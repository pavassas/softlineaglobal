
<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
  $opcion = $_POST['opcion'];
  if($opcion=="NUEVO")
  {
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
      
       <div class="form-group">
         <div class="col-xs-6"><strong>Tipo de Insumo:</strong>
         <select name="seltipoinsumo"id="seltipoinsumo" class="form-control">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysql_query("select tpi_clave_int,tpi_nombre from tipoinsumos where tpi_sw_activo = 1");
		 while($dat = mysql_fetch_array($con))
		 {
		    $id = $dat['tpi_clave_int'];
			$nom = $dat['tpi_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
         </div> 
         <div class="col-xs-6"><strong>Nombre:</strong>
         <input  name="txtnombre" id="txtnombre" class="form-control" type="text" autocomplete="off" placeholder="Ingrese nombre">
        
         </div>
    </div>
   <div class="form-group">
         <div class="col-xs-6"><strong>Unidad de medida:</strong>
         <select name="selunidad" id="selunidad" class="form-control">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysql_query("select uni_clave_int,uni_codigo,uni_nombre from unidades where uni_sw_activo = 1");
		 while($dat = mysql_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
         </select>
         </div> 
         <div class="col-xs-6"><strong>Valor Unitario:</strong>
         <input  name="txtvalor" id="txtvalor" class="form-control" type="number" step="0.1" placeholder="Ingrese valor"  onKeyPress="return NumCheck(event, this)">
        
         </div>
    </div>
    <div class="form-group">
    
    	<div class="col-xs-12"><strong>Descripción:</strong>
         <textarea id="txtdescripcion" name="txtdescripcion" class="form-control" placeholder="Escribe aqui la descripcion del material"></textarea>
    	</div>
    </div>
    <div class="form-group">
        
          <div class="col-xs-6"><strong>Estado:</strong><br>
        
                 <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" class="flat-red" checked value="1">
                 Activo</label>
                
                 <label> <input type="radio" name="radestado" id="opcion2" class="flat-red" value="0">
                Inactivo
                </label>
         </div>
    </div>
  </form>
  <?PHP  
  }
else
  if($opcion=="EDITAR" )
  {
	  $id = $_POST['id'];
	   $coninfo = mysql_query("select tpi_clave_int tpi,ins_nombre nom,uni_clave_int idu,ins_valor val,ins_descripcion des,ins_sw_activo as est from insumos   where ins_clave_int = '".$id."' limit 1");
	  $datinfo = mysql_fetch_array($coninfo);
	
	  $tpi = $datinfo['tpi'];
	  $nom = $datinfo['nom'];
	  $uni = $datinfo['idu'];
	  $val = $datinfo['val'];
	  $des = $datinfo['des'];
	  $est = $datinfo['est'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
   <div class="form-group">
         <div class="col-xs-6"><strong>Tipo de Insumo:</strong>
         <select name="seltipoinsumo"id="seltipoinsumo" class="form-control">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysql_query("select tpi_clave_int,tpi_nombre from tipoinsumos where tpi_sw_activo = 1");
		 while($dat = mysql_fetch_array($con))
		 {
		    $id = $dat['tpi_clave_int'];
			$nomt = $dat['tpi_nombre'];
			?>
            <option <?php if($tpi==$id){echo 'selected';}?> value="<?php echo $id;?>"><?php echo $nomt;?></option>
            <?Php	
		 }
		 ?>
         </select>
         </div> 
         <div class="col-xs-6"><strong>Nombre:</strong>
         <input  name="txtnombre" id="txtnombre" class="form-control" value="<?php echo $nom;?>" type="text" autocomplete="off" placeholder="Ingrese nombre">
        
         </div>
    </div>
   <div class="form-group">
         <div class="col-xs-6"><strong>Unidad de medida:</strong>
         <select name="selunidad" id="selunidad" class="form-control">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysql_query("select uni_clave_int,uni_codigo,uni_nombre from unidades where uni_sw_activo = 1");
		 while($dat = mysql_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option <?php if($uni==$idu){echo 'selected';}?> value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
         </select>
         </div> 
         <div class="col-xs-6"><strong>Valor Unitario:</strong>
         <input  name="txtvalor" id="txtvalor" class="form-control" type="number" step="0.1" value="<?php echo $val;?>"  onKeyPress="return NumCheck(event, this)" placeholder="Ingrese valor">
        
         </div>
    </div>
    <div class="form-group">
    
    	<div class="col-xs-12"><strong>Descripción:</strong>
         <textarea id="txtdescripcion"  name="txtdescripcion" class="form-control" placeholder="Escribe aqui la descripcion del material">
         <?php echo $des;?>
         </textarea>
    	</div>
    </div>
  
    <div class="form-group">
         
          <div class="col-xs-6"><strong>Estado:</strong><br>
        
    <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" class="flat-red" checked value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
    Activo</label>
    <label for="opcion2"> <input type="radio" name="radestado" id="opcion2" class="flat-red" value="0" <?php if($est==0){echo 'checked';}?>>
    Inactivo
    </label>
         </div>
    </div>
  </form>
  <?PHP  
  }
   else if($opcion=="GUARDAR")
  {
     $tipoinsumo = $_POST['tipoinsumo'];
	 $nombre  = $_POST['nombre'];
	 $unidad = $_POST['unidad'];
	 $valor = $_POST['valor'];
	 $descripcion = $_POST['descripcion'];
	 $estado  = $_POST['estado'];
	 $veri = mysql_query("select * from insumos where UPPER(ins_nombre) = UPPER('".$nombre."') and tpi_clave_int = '".$tipoinsumo."'");
	 $numv = mysql_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $ins = mysql_query("insert into insumos(ins_nombre,tpi_clave_int,uni_clave_int,ins_valor,ins_descripcion,ins_sw_activo,ins_usu_actualiz,ins_fec_actualiz) values('".$nombre."','".$tipoinsumo."','".$unidad."','".$valor."','".$descripcion."','".$estado."','".$usuario."','".$fecha."')");
		if($ins>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
 
   else if($opcion=="GUARDAREDICION" )
  {
	 $ide =$_POST['id'];
    $tipoinsumo = $_POST['tipoinsumo'];
	 $nombre  = $_POST['nombre'];
	 $unidad = $_POST['unidad'];
	 $valor = $_POST['valor'];
	 $descripcion = $_POST['descripcion'];
	 $estado  = $_POST['estado'];
	 
	 $veri = mysql_query("select * from insumos where UPPER(uni_nombre) = UPPER('".$nombre."') and tpi_clave_int  = '".$tipoinsumo."' and ins_clave_int!='".$ide."'");
	 $numv = mysql_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysql_query("update insumos set ins_nombre = '".$nombre."',tpi_clave_int='".$tipoinsumo."',uni_clave_int='".$unidad."',ins_descripcion = '".$descripcion."',ins_valor = '".$valor."',ins_sw_activo='".$estado."',ins_usu_actualiz='".$usuario."',ins_fec_actualiz='".$fecha."' where ins_clave_int = '".$ide."'");
		if($upd>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
    else if($opcion=="CARGARLISTAINSUMOS")
  {
	  ?> <script src="js/jsinsumos.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div class="table-responsive" id="no-more-tables">
      <table id="tbinsumos" class="col-md-12 table table-bordered table-hover">
                <thead>
                <tr>
                <th></th>
                <th>Codigo</th>
                <th>Tipo Insumo</th>
                <th>Tipología</th>
                <th>Nombre</th>
                <th>Unidad</th>
                <th>Valor</th>
                <th>Descripcion</th>
                <th>Estado</th>
                </tr>
                </thead>
               <?PHP
			$con = mysql_query("select ins_clave_int id,ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,ins_valor as val, ins_descripcion des,ins_sw_activo as est,tpi_tipologia as tpl from insumos i join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int   order by id asc")
			?>
                <tbody>
               <?php
			   while($dat = mysql_fetch_array($con))
			   {
				   $idc = $dat['id'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $tpl =$dat['tpl'];
				   if($tpl=="1"){$tpl="Material";}else if($tpl==2){$tpl="Equipo";}else if($tpl==3){$tpl="Mano de Obra";}
				    if($est=="0"){$est='<span class="label label-warning pull-right">Inactivo</span>';}
				   else {$est='<span class="label label-success pull-right">Activo</span>';}
				   ?>
                <tr>
               <td><a class="btn btn-block btn-warning btn-xs" onClick="CRUDINSUMOS('EDITAR',<?PHP echo $idc;?>)" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-pencil"></i></a></td>
                 <td data-title="Codigo"><?Php echo $idc;?></td>
                  <td data-title="Tipo Insumo"><?Php echo $tipo;?></td>
                  <td data-title="Tipología"><?Php echo $tpl;?></td>
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title="Unidad"><?php echo $uni."(".$cod.")";?></td>
                  <td data-title="Valor Unitario"><?php echo $val;?></td>
                  <td data-title="Descripcion"><?php echo $des;?></td>
                  <td data-title="Estado"><?Php echo $est;?></td>
                </tr>
                <?php
			   }
			   ?>
                </tbody>
                 <tfoot>
                <tr>
                <th></th>
                 <th>Codigo</th>
                <th>Tipo Insumo</th>
                <th>Tipología</th>
                <th>Nombre</th>
                <th>Unidad</th>
                <th>Valor</th>
                <th>Descripcion</th>
                <th>Estado</th>
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP

  }
?>