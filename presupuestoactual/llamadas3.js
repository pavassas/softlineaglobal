function ajaxFunction() {
    var xmlHttp;
    try {
        // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
        return xmlHttp;
    } catch (e) {
        // Internet Explorer
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            return xmlHttp;
        } catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                return xmlHttp;
            } catch (e) {
                error("Your browser does not support AJAX!");
                return false;
            }
        }
    }
}

function MOSTRARHOJA(o) {
    if (o == "VERPRESUPUESTO") {
        $('#divpreexcell').show(1000);
        $('#divapuexcell').hide(1000);
        $('#divsubexcell').hide(1000);
    } else
    if (o == "VERAPU") {
        $('#divpreexcell').hide(1000);
        $('#divapuexcell').show(1000);
        $('#divsubexcell').hide(1000);
    } else
    if (o == "VERSUB") {
        $('#divpreexcell').hide(1000);
        $('#divapuexcell').hide(1000);
        $('#divsubexcell').show(1000);
    } else if (o == "VERAPU2") {
        $('#divapuexcell').show(1000);
        $('#divsubexcell').hide(1000);
    } else
    if (o == "VERSUB2") {
        $('#divapuexcell').hide(1000);
        $('#divsubexcell').show(1000);
    }
}

function colapsemenu() {
    $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');
}

function notificacion(msn) {
    alertify.log(msn);
    return false;
}

function ok(msn) {
    alertify.success('<i class="icon fa fa-check"></i>Alerta!<br>' + msn);
    return false;
}

function error(msn) {
    alertify.error('<i class="icon fa fa-ban"></i>Alerta!<br>' + msn);
    return false;
}

//SOLO VALOR NUMERICON PERMITIENDO .
function validar_texto(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9\.]/g, "0";

    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function NumCheck(e, field) {
  key = e.keyCode ? e.keyCode : e.which
  // backspace
  if (key == 8) return true
  // 0-9
  if (key > 47 && key < 58) {
    if (field.value == "") return true
    regexp = /.[0-9]{20}$/
    return !(regexp.test(field.value))
  }
  // .
  if (key == 46) {
    if (field.value == "") return false
    regexp = /^[0-9]+$/
    return regexp.test(field.value)
  }
  // other key
  return false
 
} 
function checkDecimals(id) {
//console.log("por aca 2");
var val = $('#'+id).val();
val = val.replace("$", "");
val = val.replace(",", "").replace(",", "");
val = val.replace(",", "").replace(",", "");
decallowed = 6; // how many decimals are allowed?

	if (isNaN(val) || val == "") {
	//alert("El número no es válido. Prueba de nuevo.");
	//fieldName.select();
	//fieldName.focus();
	return false;
	}
	else 
	{
		if (val.indexOf('.') == -1) val += ".";
		dectext = val.substring(val.indexOf('.')+1, val.length);
	
			if (dectext.length > decallowed)
			{
				error("Por favor, entra un número con maximo " + decallowed + " números decimales.");
				$('#'+id).val(val.substring(val.length-1, 0));
				$('#'+id).focus();
				return true
			}
			else 
			{
				//alert ("Número validado satisfactoriamente.");
				return false;
			}
	    }
}
// End -->

//LLAMADAS PRINCIPAL
function RECUPERAR() {
    var rec = $("#recuperarcontrasena").val();
    var ajax;
    ajax = new ajaxFunction();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4) {
            document.getElementById('recu').innerHTML = ajax.responseText;
        }
    }
    jQuery("#recu").html(""); //loading gif will be overwrited when ajax have success
    ajax.open("GET", "?recuperar=si&dat=" + rec, true);
    ajax.send(null);
}

function RESTABLECER(v) {
    var con1 = $("#contrasena1").val();
    var con2 = $("#contrasena2").val();

    var ajax;
    ajax = new ajaxFunction();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4) {
            document.getElementById('recu').innerHTML = ajax.responseText;
        }
    }
    $("#recu").html(""); //loading gif will be overwrited when ajax have success
    ajax.open("GET", "?restablecer=si&cod=" + v + "&con1=" + con1 + "&con2=" + con2, true);
    ajax.send(null);
}

function ENVIARNOTIFICACION() {
    var ajax;
    ajax = new ajaxFunction();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4) {
            document.getElementById('').innerHTML = ajax.responseText;
        }
    }
    ajax.open("GET", "?enviarnotificacion=si", true);
    ajax.send(null);
}

function CORREGIRTEXTO(v) {
    var res = v.replace('#', 'REEMPLAZARNUMERAL');
    var res = res.replace('+', 'REEMPLAZARMAS');
    var res = res.replace('$', 'REEMPLAZARPESO');
    return res;
}
//script para formularios
function CRUDUSUARIOS(o, id, idd) {
    $('#msn').html('');
    if (o == "NUEVO") {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Usuario");
        $('#contenido').html('');
        $.post('funciones/fnUsuarios.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            });
    } else if (o == "EDITAR") {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Usuario");
        $('#contenido').html('');
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "GUARDAR") {
        var txtnombre = $('#txtnombre').val();
        var txtusuario = $('#txtusuario').val();
        var lu = txtusuario.length;
        var txtpass = $('#txtpass').val();
        var lc = txtpass.length
        var txtpass1 = $('#txtpass1').val();
        var selcargo = $('#selcargo').val();
        var selperfil = $('#selperfil').val();
        var selcoordinador = $('#selcoordinador').val();
        var txtemail = $('#txtemail').val();
        var ckactivo = $('input:checkbox[name=ckactivo]:checked').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre completo del usuario. Verificar');
        } else if (txtusuario == "" || txtusuario == null) {
            error('Diligenciar el usuario. Verificar');
        } else if (lu < 4) {
            error('El usuario debe ser igual o mayor a 4 digitos. Verificar');
        } else if (txtpass == "" || txtpass == null) {
            error('Diligenciar la contraseña. Verificar');
        } else if (lc < 4) {
            error('La contraseña debe ser igual o mayor a 4 digitos . Verificar');
        } else if (txtpass1 == "" || txtpass1 == null) {
            error('Diligenciar la contraseña de verificacion. Verificar');
        } else if (txtpass != txtpass1) {
            error('Las contraseñas no coinciden. Verificar');
        } else if (selcargo == "" || selcargo == null) {
            error('Seleccionar el cargo. Verificar');
        } else if (selperfil == "" || selperfil == null) {
            error('Seleccionar el perfil. Verificar');
        } else if (txtemail == "" || txtemail == null) {
            error('Diligenciar el correo electrónico. Verificar');
        } else if (txtemail.indexOf('@', 0) == -1 || txtemail.indexOf('.', 0) == -1) {
            error('Formato de correo electrónico no valido. <br>Ejemplo:example@mail.com. Verificar');
        } else {
            $.post('funciones/fnUsuarios.php', {
                    opcion: o,
                    nombre: txtnombre,
                    usuario: txtusuario,
                    pass: txtpass,
                    perfil: selperfil,
                    email: txtemail,
                    activo: ckactivo,
                    cargo: selcargo,
                    coordinador: selcoordinador
                },
                function(data) {
                    data = $.trim(data);
                    if (data == "error1") {
                        error('El usuario ingresado ya existe. Verificar');
                    } else if (data == "error2") {
                        error('El e-mail ingresado ya existe. Verificar');
                    } else if (data == "error3") {
                        error('Surgió un error al guardar el usuario. Verificar');
                    } else if (data == "ok") {
                        $('#myModal').modal('hide');
                        ok('Usuario registrado correctamente');
                        setTimeout(cargarlistadostipo('CARGARLISTAUSUARIOS'), 1000);
                    }
                });
        }
    } else if (o == "GUARDAREDICION") {
        var edi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var txtusuario = $('#txtusuario').val();
        var lu = txtusuario.length;
        var txtpass = $('#txtpass').val();
        var lc = txtpass.length
        var txtpass1 = $('#txtpass1').val();
        var selcargo = $('#selcargo').val();
        var selperfil = $('#selperfil').val();
        var txtemail = $('#txtemail').val();
        var ckactivo = $('input:checkbox[name=ckactivo]:checked').val();
        var selcoordinador = $('#selcoordinador').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre completo del usuario. Verificar');
        } else if (txtusuario == "" || txtusuario == null) {
            error('Diligenciar el usuario. Verificar');
        } else if (lu < 4) {
            error('El usuario debe ser igual o mayor a 4 digitos. Verificar');
        } else if (txtpass == "" || txtpass == null) {
            error('Diligenciar la contraseña. Verificar');
        } else if (lc < 4) {
            error('La contraseña debe ser igual o mayor a 4 digitos . Verificar');
        } else if (txtpass1 == "" || txtpass1 == null) {
            error('Diligenciar la contraseña de verificacion. Verificar');
        } else if (txtpass != txtpass1) {
            error('Las contraseñas no coinciden. Verificar');
        } else if (selcargo == "" || selcargo == null) {
            error('Seleccionar el cargo. Verificar');
        } else if (selperfil == "" || selperfil == null) {
            error('Seleccionar el perfil. Verificar');
        } else if (txtemail == "" || txtemail == null) {
            error('Diligenciar el correo electrónico. Verificar');
        } else if (txtemail.indexOf('@', 0) == -1 || txtemail.indexOf('.', 0) == -1) {
            error('Formato de correo electrónico no valido.<br> Ejemplo:example@mail.com. Verificar');
        } else {
            $.post('funciones/fnUsuarios.php', {
                    opcion: o,
                    nombre: txtnombre,
                    usuario: txtusuario,
                    pass: txtpass,
                    perfil: selperfil,
                    email: txtemail,
                    activo: ckactivo,
                    cargo: selcargo,
                    edi: edi,
                    coordinador: selcoordinador
                },
                function(data) {
                    data = $.trim(data);
                    if (data == "error1") {
                        error('El usuario ingresado ya existe. Verificar');
                    } else if (data == "error2") {
                        error('El e-mail ingresado ya existe. Verificar');
                    } else if (data == "error3") {
                        error('Surgió un error al modificar el usuario. Verificar');
                    } else if (data == "ok") {
                        $('#myModal').modal('hide');
                        ok('Usuario modificado correctamente');
                        setTimeout(cargarlistadostipo('CARGARLISTAUSUARIOS'), 1000);
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el usuario seleccionado?', 'Diálogo Confirmación LG', function(r) {
            // var conf = confirm("Realmente desea eliminar el usuario seleccionado");
            if (r == true) {
                $.post('funciones/fnUsuarios.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Usuario eliminado correctamente');
                            var table = $('#tbusuarios').DataTable();
                            table.row('#row_usu' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el Usuario. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "PASAR") {
        var can = $('#presupuestoagregar option:selected').length;
        var idu = $('#idedicion').val();
        if (can <= 0) {
            error('No hay seleccionado ninguna obra a asignar');
            return false;
        } else {

            $("#presupuestoagregar option:selected").each(function() {
                var texto = $(this).text();
                var value = $(this).val();
                $.post('funciones/fnUsuarios.php', {
                        opcion: o,
                        idu: idu,
                        presupuesto: value
                    },
                    function(data) {
                        if (data == 1) {
                            $("select#presupuestoagregar option[value='" + value + "']").remove();
                            setTimeout(CRUDUSUARIOS('REFRESCARAGREGADOS', ''), 1000);
                        } else {
                            error('Surgió Un Error al asignar presupuesto: ' + texto);
                            return false;
                        }
                    });
            });
        }
    } else if (o == "PASARTODOS") {
        var can = $('#presupuestoagregar option').length;
        if (can <= 0) {
            error("No hay presupuesto faltantes por asignar");
            return false;
        } else {
            var idu = $('#idedicion').val();
            $("#presupuestoagregar option").each(function() {
                var texto = $(this).text();
                var value = $(this).val();
                $.post('funciones/fnUsuarios.php', {
                        opcion: o,
                        idu: idu,
                        presupuesto: value
                    },
                    function(data) {
                        if (data == 1) {
                            $("select#presupuestoagregar option[value='" + value + "']").remove();
                            setTimeout(CRUDUSUARIOS('REFRESCARAGREGADOS', ''), 1000);
                        } else {
                            error('Surgió Un Error al asignar presupuesto: <strong>' + texto + '</strong>');
                            return false;
                        }
                    });
            });
        }
    } else if (o == "QUITAR") {
        var can = $('#presupuestoagregados option:selected').length;
        var idu = $('#idedicion').val();
        if (can <= 0) {
            error("No hay seleccionado ningun presupuesto a desasignar");
            return false;
        } else {
            $("#presupuestoagregados option:selected").each(function() {
                var texto = $(this).text();
                var value = $(this).val();
                $.post('funciones/fnUsuarios.php', {
                        opcion: o,
                        idu: idu,
                        presupuesto: value
                    },
                    function(data) {
                        if (data == 1) {
                            $("select#presupuestoagregados option[value='" + value + "']").remove();
                            setTimeout(CRUDUSUARIOS('REFRESCARAGREGAR', ''), 1000);
                        } else {
                            error("Surgió Un Error al desasignar presuouesto:" + texto);
                            return false;
                        }
                    });
            });
        }
    } else if (o == "QUITARTODOS") {
        var can = $('#presupuestoagregados option').length;
        if (can <= 0) {
            error("No hay presupuestos faltantes por desasignar");
            return false;
        } else {
            var idu = $('#idedicion').val();
            $("#presupuestoagregados option").each(function() {
                var texto = $(this).text();
                var value = $(this).val();
                $.post('funciones/fnUsuarios.php', {
                        opcion: o,
                        idu: idu,
                        presupuesto: value
                    },
                    function(data) {
                        if (data == 1) {
                            $("select#presupuestoagregados option[value='" + value + "']").remove();
                            setTimeout(CRUDUSUARIOS('REFRESCARAGREGAR', ''), 1000);
                        } else {
                            error("Surgió Un Error al desasignar presupuesto: " + texto);
                            return false;
                        }
                    });
            });
        }
    } else if (o == "REFRESCARAGREGADOS") {
        var idu = $('#idedicion').val();
        var ob = $('#presupuestoagregados');
        ob.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                idu: idu
            },
            function(data) {
                ob.empty();

                for (var i = 0; i < data.length; i++) {
                    ob.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                }
                setTimeout(INICIALIZARLISTAS('MODAL'), 1000);
            }, "json");

    } else if (o == "REFRESCARAGREGAR") {
        var idu = $('#idedicion').val();
        var ob = $('#presupuestoagregar');
        ob.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                idu: idu
            },
            function(data) {
                ob.empty();

                for (var i = 0; i < data.length; i++) {
                    ob.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                }
                setTimeout(INICIALIZARLISTAS('MODAL'), 1000);

            }, "json");
    } else if (o == "CAMBIOCONTRASENA") {
        var txtpass = $('#nueva').val();
        var txtpass1 = $('#confirmar').val();
        var ant = $('#anterior').val();
        var lc = txtpass.length;
        var vc = $('#vericaptcha').val();
        if (ant == "" || ant == null) {
            error('Diligenciar la contraseña anterior. Verificar');
        } else if (txtpass == "" || txtpass == null) {
            error('Diligenciar la nueva contraseña. Verificar');
        } else if (lc < 4) {
            error('La contraseña debe ser igual o mayor a 4 digitos . Verificar');
        } else if (txtpass1 == "" || txtpass1 == null) {
            error('Diligenciar la contraseña de verificación. Verificar');
        } else
        if (txtpass != txtpass1) {
            error('Las contraseñas no coinciden. Verificar');
        } else
        if (vc == "" || vc == null) {
            error('Diligenciar la palabra que se halla en la imagen. Verificar');
        } else {
            $.post('funciones/fnUsuarios.php', {
                    opcion: o,
                    ant: ant,
                    nue: txtpass,
                    conf: txtpass1,
                    captcha: vc
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == 1) {
                        $('#nueva').val('');
                        $('#confirmar').val('');
                        $('#anterior').val('');
                        $('#vericaptcha').val('');
                        ok(data[0].msn);
                        setTimeout(CRUDUSUARIOS('REFRESCARCAPTCHA', '', ''), 1000);
                    } else if (res == 2) {
                        error(data[0].msn);
                    };
                }, "json");
        }
    } else if (o == "REFRESCARCAPTCHA") {
        $('#captcha').attr('src', 'dist/img/cargando.gif');
        $('#captcha').attr('src', 'modulos/cambiarcontrasena/captcha.php');
    } else if (o == "VERICAPTCHA") {
        var vc = $('#vericaptcha').val()
        if (vc == "" || vc == null) {
            $('#msn').html('');
        } else {
            $.post('funciones/fnUsuarios.php', {
                    opcion: o,
                    captcha: vc
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == 1) {
                        // $('#btncambio').attr('disabled',false);
                    } else {
                        // $('#btncambio').attr('disabled',true);
                        error('Los caracteres de verificacion no coinciden con los de la imagen. Verificar');
                    }
                }, "json");
        }

    }

    //permisos aplicacion
    else if (o == "CARGARPERMISOS") {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Asignacion de Permisos");
        $('#contenido').html('');
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            });
    } else if (o == "AGREGARPERMISO") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                idp: ide,
                ventana: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) agregado(s) correctamente');
                } else {
                    error('Surgió un error al agregar permiso(s). Verificar');
                }
            });
    } else if (o == "AGREGARPERMISO1") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnUsuarios.php', {
                opcion: 'AGREGARPERMISO',
                idp: ide,
                ventana: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) agregado(s) correctamente');
                    setTimeout(CRUDUSUARIOS('CARGARPERMISOS', ide, ''), 1000);
                } else {
                    error('Surgió un error al agregar permiso(s). Verificar');
                }
            });
    } else if (o == "ELIMINARPERMISO") {
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                id: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) eliminado(s) correctamente');
                } else {
                    error('Surgió un error al eliminar permiso(s). Verificar');
                }
            });
    } else if (o == "ELIMINARPERMISO1") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnUsuarios.php', {
                opcion: 'ELIMINARPERMISO',
                id: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) eliminados(s) correctamente');
                    setTimeout(CRUDUSUARIOS('CARGARPERMISOS', ide, ''), 1000);
                } else {
                    error('Surgió un error al eliminar permiso(s). Verificar');
                }
            });
    } else if (o == "AGREGARTODOSP") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                id: ide
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) agregado(s) correctamente');
                    setTimeout(CRUDUSUARIOS('CARGARPERMISOS', ide, ''), 1000);
                } else {
                    error('Surgió un error al agregar permiso(s). Verificar');
                }
            });
    } else if (o == "ELIMINARTODOSP") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                id: ide
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) eliminados(s) correctamente');
                    setTimeout(CRUDUSUARIOS('CARGARPERMISOS', ide, ''), 1000);
                } else {
                    error('Surgió un error al eliminar permiso(s). Verificar');
                }
            });
    } else if (o == "MODIFICARPERMISO") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnUsuarios.php', {
                opcion: o,
                metodo: id,
                ventana: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso modificado correctamente');
                } else {
                    error('Surgió un error al modificar permiso. Verificar');
                }
            });
    }
}

function CRUDPERFILES(o, id, idd) {
    $('#msn').html('');
    if (o == "NUEVO") {
        $('.modal-dialog').removeClass('modal-lg');
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Perfil");
        $('#contenido').html('');
        $.post('funciones/fnPerfiles.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            });
    } else if (o == "EDITAR") {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Perfil");
        $('#contenido').html('');
        $.post('funciones/fnPerfiles.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "GUARDAR") {
        var txtdescripcion = $('#txtdescripcion').val();

        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción. Verificar');

        } else {
            $.post('funciones/fnPerfiles.php', {
                    opcion: o,
                    descripcion: txtdescripcion
                },
                function(data) {
                    data = $.trim(data);

                    if (data == "error1") {
                        error('La descripcion ingresada ya existe. Verificar');
                    } else if (data == "error2") {
                        error('Surgió un error al guardar el perfil. Verificar');
                    } else if (data == "ok") {
                        ok('Perfil registrado correctamente');
                        setTimeout(cargarlistadostipo('CARGARLISTAPERFILES'), 1000);
                    }
                });

        }
    } else if (o == "GUARDAREDICION") {
        var edi = $('#idedicion').val();
        var txtdescripcion = $('#txtdescripcion').val();
        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción. Verificar');
        } else {
            $.post('funciones/fnPerfiles.php', {
                    opcion: o,
                    descripcion: txtdescripcion,
                    edi: edi
                },
                function(data) {
                    data = $.trim(data);
                    if (data == "error1") {
                        error('La descripcion ingresada ya existe. Verificar');
                    } else if (data == "error2") {
                        error('Surgió un error al modificar el perfil. Verificar');
                    } else if (data == "ok") {
                        ok('Perfil modificado correctamente');
                        setTimeout(cargarlistadostipo('CARGARLISTAPERFILES'), 1000);
                    }
                });

        }
    } else if (o == "CARGARPERMISOS") {
        var ide = $('#idedicion').val();
        $('#permisos').html("");
        $.post('funciones/fnPerfiles.php', {
                opcion: o,
                id: ide
            },
            function(data) {
                $('#permisos').html(data);
            });
    } else if (o == "AGREGARPERMISO") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnPerfiles.php', {
                opcion: o,
                idp: ide,
                ventana: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) agregado(s) correctamente');
                } else {
                    error('Surgió un error al agregar permiso(s). Verificar');
                }
            });
    } else if (o == "AGREGARPERMISO1") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnPerfiles.php', {

                opcion: 'AGREGARPERMISO',
                idp: ide,
                ventana: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) agregado(s) correctamente');
                    setTimeout(CRUDPERFILES('CARGARPERMISOS', '', ''), 1000);
                } else {
                    error('Surgió un error al agregar permiso(s). Verificar');
                }
            });
    } else if (o == "ELIMINARPERMISO") {

        $.post('funciones/fnPerfiles.php', {
                opcion: o,
                id: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) eliminado(s) correctamente');
                } else {
                    error('Surgió un error al eliminar permiso(s). Verificar');
                }
            });
    } else if (o == "ELIMINARPERMISO1") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnPerfiles.php', {
                opcion: 'ELIMINARPERMISO',
                id: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) eliminados(s) correctamente');
                    setTimeout(CRUDPERFILES('CARGARPERMISOS', '', ''), 1000);
                } else {
                    error('Surgió un error al eliminar permiso(s). Verificar');
                }
            });
    } else if (o == "AGREGARTODOS") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnPerfiles.php', {
                opcion: o,
                id: ide
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) agregado(s) correctamente');
                    setTimeout(CRUDPERFILES('CARGARPERMISOS', '', ''), 1000);
                } else {
                    error('Surgió un error al agregar permiso(s). Verificar');
                }
            });
    } else if (o == "ELIMINARTODOS") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnPerfiles.php', {
                opcion: o,
                id: ide
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso(s) eliminados(s) correctamente');
                    setTimeout(CRUDPERFILES('CARGARPERMISOS', '', ''), 1000);
                } else {
                    error('Surgió un error al eliminar permiso(s). Verificar');
                }
            });
    } else if (o == "MODIFICARPERMISO") {
        var ide = $('#idedicion').val();
        $.post('funciones/fnPerfiles.php', {
                opcion: o,
                metodo: id,
                ventana: idd
            },
            function(data) {
                if (data == "ok") {
                    ok('Permiso modificado correctamente');
                } else {
                    error('Surgió un error al modificar permiso. Verificar');
                }
            });
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el perfil seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el perfil seleccionado");
            if (r == true) {
                $.post('funciones/fnPerfiles.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Perfil eliminado correctamente');
                            var table = $('#tbperfiles').DataTable();
                            table.row('#row_per' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el perfil. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDDIVISION(v, o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');
    if (v == "CIUDAD" && o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('rel', 'GUARDAR');
        $('#btnguardar').attr('name', v);
        $('#myModalLabel').html("Registro Nueva Ciudad");
        $('#contenido').html('');
        $.post('funciones/fnLocalizacion.php', {
                opcion: o,
                ventana: v
            },
            function(data) {
                $('#contenido').html(data);
            });
    } else if (v == "DEPARTAMENTO" && o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('rel', 'GUARDAR');
        $('#btnguardar').attr('name', v);
        $('#myModalLabel').html("Registro Nuevo Departamento");
        $('#contenido').html('');
        $.post('funciones/fnLocalizacion.php', {
                opcion: o,
                ventana: v
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (v == "PAIS" && o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('rel', 'GUARDAR');
        $('#btnguardar').attr('name', v);
        $('#myModalLabel').html("Registro Nuevo Pais");
        $('#contenido').html('');
        $.post('funciones/fnLocalizacion.php', {
                opcion: o,
                ventana: v
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (v == "CIUDAD" && o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('rel', 'GUARDAREDICION');
        $('#btnguardar').attr('name', v);
        $('#myModalLabel').html("Editar Ciudad");
        $('#contenido').html('');
        $.post('funciones/fnLocalizacion.php', {
                opcion: o,
                ventana: v,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (v == "DEPARTAMENTO" && o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('rel', 'GUARDAREDICION');
        $('#btnguardar').attr('name', v);
        $('#myModalLabel').html("Editar Departamento");
        $('#contenido').html('');
        $.post('funciones/fnLocalizacion.php', {
                opcion: o,
                ventana: v,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (v == "PAIS" && o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('rel', 'GUARDAREDICION');
        $('#btnguardar').attr('name', v);
        $('#myModalLabel').html("Editar Pais");
        $('#contenido').html('');
        $.post('funciones/fnLocalizacion.php', {
                opcion: o,
                ventana: v,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (v == "PAIS" && o == "GUARDAR") {
        var txtpais = $('#txtpais').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtpais == "" || txtpais == null) {
            error('Diligenciar el nombre del pais');
        } else {
            $.post('funciones/fnLocalizacion.php', {
                    opcion: o,
                    ventana: v,
                    pais: txtpais,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Pais registrado correctamente');
                        // $('#myModal').modal('hide');
                        setimeout(cargarlistadostipo('CARGARLISTAPAISES'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un pais con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al guardar el pais. Verificar');
                    }
                });
        }
    } else if (v == "DEPARTAMENTO" && o == "GUARDAR") {
        var selpais = $('#selpais').val();
        var txtdepartamento = $('#txtdepartamento').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (selpais == "" || selpais == null) {
            error('Seleccionar el pais');
        } else if (txtdepartamento == "" || txtdepartamento == null) {
            error('Diligenciar el nombre del departamento');
        } else {
            $.post('funciones/fnLocalizacion.php', {
                    opcion: o,
                    ventana: v,
                    pais: selpais,
                    departamento: txtdepartamento,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Departamento registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistados('CARGARLISTADEPARTAMENTOS', 1000))
                    } else if (data == 2) {
                        error('Ya hay un departamento con el nombre diligenciado en el pais seleccionado. Verificar');
                    } else {
                        error('Surgió un error al guardar el departamento. Verificar');
                    }
                });
        }


    } else if (v == "CIUDAD" && o == "GUARDAR") {
        var selpais = $('#selpais').val();
        var seldepartamento = $('#seldepartamento').val()
        var txtciudad = $('#txtciudad').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (selpais == "" || selpais == null) {
            error('Seleccionar el pais');
        } else
        if (seldepartamento == "" || seldepartamento == null) {
            error('Seleccionar el departamento');
        } else if (txtciudad == "" || txtciudad == null) {
            error('Diligenciar el nombre de la ciudad');
        } else {
            $.post('funciones/fnLocalizacion.php', {
                    opcion: o,
                    ventana: v,
                    pais: selpais,
                    departamento: seldepartamento,
                    ciudad: txtciudad,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Ciudad registrada correctamente');
                        // $('#myModal').modal('hide');
                        // cargarlistapaises();
                        cargarlistadostipo('CARGARLISTACIUDADES')
                    } else if (data == 2) {
                        error('Ya hay una ciudad  con el nombre diligenciado en el departamento y pais seleccionados. Verificar');
                    } else {
                        error('Surgió un error al guardar la ciudad. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (v == "PAIS" && o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtpais = $('#txtpais').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtpais == "" || txtpais == null) {
            error('Diligenciar el nombre del pais');
        } else {
            $.post('funciones/fnLocalizacion.php', {
                    opcion: o,
                    ventana: v,
                    pais: txtpais,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Pais actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTAPAISES'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un pais con el nombre ingresado. Verifica');
                    } else {
                        error('Surgió un error al actualizar el pais. Verificar');
                    }
                });
        }
    } else if (v == "DEPARTAMENTO" && o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var selpais = $('#selpais').val();
        var txtdepartamento = $('#txtdepartamento').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (selpais == "" || selpais == null) {
            error('Seleccionar el pais');
        } else if (txtdepartamento == "" || txtdepartamento == null) {
            error('Diligenciar el nombre del departamento');
        } else {
            $.post('funciones/fnLocalizacion.php', {
                    opcion: o,
                    ventana: v,
                    pais: selpais,
                    departamento: txtdepartamento,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Departamento actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTADEPARTAMENTOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un departamento con el nombre diligenciado en el pais seleccionado. Verificar');
                    } else {
                        error('Surgió un error al actualizar el departamento. Verificar');
                    }
                });
        }


    } else if (v == "CIUDAD" && o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var selpais = $('#selpais').val();
        var seldepartamento = $('#seldepartamento').val()
        var txtciudad = $('#txtciudad').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (selpais == "" || selpais == null) {
            error('Seleccionar el pais');
        } else
        if (seldepartamento == "" || seldepartamento == null) {
            error('Seleccionar el departamento');
        } else if (txtciudad == "" || txtciudad == null) {
            error('Diligenciar el nombre de la ciudad');
        } else {
            $.post('funciones/fnLocalizacion.php', {
                    opcion: o,
                    ventana: v,
                    pais: selpais,
                    departamento: seldepartamento,
                    ciudad: txtciudad,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Ciudad actualizada correctamente');
                        // $('#myModal').modal('hide');
                        // cargarlistapaises();
                        setTimeout(cargarlistadostipo('CARGARLISTACIUDADES'), 1000);
                    } else if (data == 2) {
                        error('Ya hay una ciudad  con el nombre diligenciado en el departamento y pais seleccionados. Verificar');

                    } else {
                        error('Surgió un error al actualizar la ciudad. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR" && v == "CIUDAD") {
        jConfirm('Realmente desea eliminar la ciudad seleccionada?', 'Diálogo Confirmación LG', function(r) {
            // var conf = confirm("Realmente desea eliminar la ciudad seleccionada");
            if (r == true) {
                $.post('funciones/fnLocalizacion.php', {
                        opcion: o,
                        ventana: v,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Ciudad eliminada correctamente');
                            var table = $('#tbdivision').DataTable();
                            table.row('#row_ciu' + id).remove().draw(false);

                        } else {
                            error('Surgió un error al eliminar la ciudad. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "ELIMINAR" && v == "DEPARTAMENTO") {
        jConfirm('Realmente desea eliminar el departamento seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el departamento seleccionado");
            if (r == true) {
                $.post('funciones/fnLocalizacion.php', {
                        opcion: o,
                        ventana: v,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Departamento eliminado correctamente');
                            var table = $('#tbdivision').DataTable();
                            table.row('#row_dep' + id).remove().draw(false);

                        } else {
                            error('Surgió un error al eliminar el departamento la ciudad. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "ELIMINAR" && v == "PAIS") {
        jConfirm('Realmente desea eliminar el pais seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar la pais seleccionado");
            if (r == true) {
                $.post('funciones/fnLocalizacion.php', {
                        opcion: o,
                        ventana: v,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Pais eliminado correctamente');
                            var table = $('#tbdivision').DataTable();
                            table.row('#row_pai' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el pais. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }

}


function cargardepartamento(sel1, sel2, o) {

    var pai = $('#' + sel1).val();
    var dep = $('#' + sel2);
    dep.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
    //alert(pai);
    $.post('funciones/fnLocalizacion.php', {
            opcion: 'CARGADEPARTAMENTO',
            pais: pai
        },
        function(data) {
            dep.empty();
            dep.append('<option value="">--seleccione--</option>');
            if (o == 1) { //cargarciudad('seldepartamento','selciudad','selpais');
            }
            for (var i = 0; i < data.length; i++) {
                dep.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
            }

        }, "json");

}

function CRUDCARGOS(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Cargo");
        $('#contenido').html('');
        $.post('funciones/fnCargo.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Cargo");
        $('#contenido').html('');
        $.post('funciones/fnCargo.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del cargo');
        } else {
            $.post('funciones/fnCargo.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Cargo registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTACARGOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un cargo con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al guardar el cargo. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del cargo');
        } else {
            $.post('funciones/fnCargo.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Cargo actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTACARGOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un cargo con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al actualizar el cargo. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el cargo seleccionado?', 'Diálogo Confirmación LG', function(r) {
            // var conf = confirm("Realmente desea eliminar el cargo seleccionado");
            if (r == true) {
                $.post('funciones/fnCargo.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Cargo eliminado correctamente');
                            var table = $('#tbcargos').DataTable();
                            table.row('#row_car' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el cargo. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDCLIENTES(o, id) {
    $('#msn').html('');
    if (o == "NUEVO") {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Cliente");
        $('#contenido').html('');
        $.post('funciones/fnClientes.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            });
    } else if (o == "GUARDAR") {
        var seltipopersona = $('#seltipopersona').val();
        var seltipodocumento = $('#seltipodocumento').val();
        var txtdocumento = $('#txtdocumento').val();
        var txtrazon = $('#txtrazon').val();
        var txtnombre = $('#txtnombre').val();
        var txtapellido = $('#txtapellido').val();
        var selciudad = $('#selciudad').val();
        var txttelefono = $('#txttelefono').val();
        var txtcorreo = $('#txtcorreo').val();
        var txtdescripcion = $('#txtdescripcion').val();
        if (txtdocumento == "" || txtdocumento == null) {
            error('Diligenciar Número de documento. Verificar');
        } else if (txtrazon == "") {
            error('Diligenciar el nombre del cliente. Verificar');
        } else if (selciudad == "") {
            error('Seleccionar la ciudad. Verificar');
        } else if (txtnombre == "") {
            error('Diligenciar el nombre del contacto. Verificar');
        } else if (txtapellido == "") {
            error('Diligenciar el apellido del contacto. Verificar');
        } else if (txttelefono == "") {
            error('Diligenciar el número de telefono del contacto. Verificar');
        } else if (txtcorreo == "") {
            error('Diligenciar el correo electrónico. Verificar');
        } else if (txtcorreo.indexOf('@', 0) == -1 || txtcorreo.indexOf('.', 0) == -1) {
            error('Formato de correo electrónico no valido.<br> Ejemplo:example@mail.com. Verificar');
        }
        /*else if(seltipopersona==1 && txtdescripcion=="")
	   {
		   error('Diligenciar la descripción del Cliente. Verificar');
	   }*/
        else {
            $.post('funciones/fnClientes.php', {
                opcion: o,
                tipopersona: seltipopersona,
                tipodocumento: seltipodocumento,
                documento: txtdocumento,
                razon: txtrazon,
                nombre: txtnombre,
                apellido: txtapellido,
                ciudad: selciudad,
                telefono: txttelefono,
                correo: txtcorreo,
                descripcion: txtdescripcion
            }, function(data) {
                if (data == 1) {
                    ok('Cliente Registrado correctamente');
                    setTimeout(cargarlistaclientes(), 1000);
                } else if (data == 2) {
                    error('Ya hay un Cliente registrado con el documento o correo diligenciado. Verificar');
                } else {
                    error('Surgió un error al guardar el Cliente. Verificar');
                }
            });
        }
    } else if (o == "EDITAR") {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Cliente");
        $('#contenido').html('');
        $.post('funciones/fnClientes.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "GUARDAREDICION") {
        var ide = $('#idedicion').val();
        var seltipopersona = $('#seltipopersona').val();
        var seltipodocumento = $('#seltipodocumento').val();
        var txtdocumento = $('#txtdocumento').val();
        var txtrazon = $('#txtrazon').val();
        var txtnombre = $('#txtnombre').val();
        var txtapellido = $('#txtapellido').val();
        var selciudad = $('#selciudad').val();
        var txttelefono = $('#txttelefono').val();
        var txtcorreo = $('#txtcorreo').val();
        var txtdescripcion = $('#txtdescripcion').val();
        if (txtdocumento == "" || txtdocumento == null) {
            error('Diligenciar Número de documento. Verificar');
        } else if (txtrazon == "") {
            error('Diligenciar la nombre del cliente. Verificar');
        } else if (selciudad == "") {
            error('Seleccionar la ciudad. Verificar');
        } else if (txtnombre == "") {
            error('Diligenciar el nombre del contacto. Verificar');
        } else if (txtapellido == "") {
            error('Diligenciar el apellido del contacto. Verificar');
        } else if (txttelefono == "") {
            error('Diligenciar el número de telefono del contacto. Verificar');
        } else if (txtcorreo == "") {
            error('Diligenciar el correo electrónico del contacto. Verificar');
        } else if (txtcorreo.indexOf('@', 0) == -1 || txtcorreo.indexOf('.', 0) == -1) {
            error('Formato de correo electrónico no valido.<br> Ejemplo:example@mail.com. Verificar');
        }
        /* else if(seltipopersona==1 && txtdescripcion=="")
	   {
		   error('Diligenciar la descripción del Cliente. Verificar');
	   }*/
        else {
            $.post('funciones/fnClientes.php', {
                opcion: o,
                tipopersona: seltipopersona,
                tipodocumento: seltipodocumento,
                documento: txtdocumento,
                razon: txtrazon,
                nombre: txtnombre,
                apellido: txtapellido,
                ciudad: selciudad,
                telefono: txttelefono,
                correo: txtcorreo,
                descripcion: txtdescripcion,
                id: ide
            }, function(data) {
                if (data == 1) {
                    ok('Cliente Actualizado correctamente');
                    setTimeout(cargarlistaclientes(), 1000);
                } else if (data == 2) {
                    error('Ya hay una Cliente registrada con el documento o correo diligenciado. Verificar');
                } else {
                    error('Surgió un error al actualizar el Cliente. Verificar');
                }
            });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el Cliente seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el Cliente/Coordinador seleccionado");
            if (r == true) {
                $.post('funciones/fnClientes.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Cliente eliminado correctamente');
                            var table = $('#tbclientes').DataTable();
                            table.row('#row_per' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el Cliente. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else
    if (o == "UNIFICAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('.modal-dialog').addClass('modal-lg');
        $('#btnguardar').attr('name', 'GUARDARUNIFICAR');
        $('#myModalLabel').html("Unificar Clientes");
        $('#contenido').html('');
        $.post('funciones/fnClientes.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "GUARDARUNIFICAR") {
        var clientes = $('#clienteunificar').val();
        var clientebase = $('#clientebase').val();
        if (clientes == "" || clientes == null) {
            error('Seleccionar los clientes a unificar');
        } else if (clientebase == "") {
            error('Seleccionar el cliente base');
        } else {
            jConfirm("Realmente desea unificar los clientes seleccionados por el nuevo cliente base", "Diálogo de Confirmación LG", function(r) {
                if (r == true) {
                    $.post('funciones/fnClientes.php', {
                            opcion: o,
                            clientes: clientes,
                            clientebase: clientebase
                        },
                        function(data) {
                            res = $.trim(data[0].res);
                            if (res == 1) {
                                $("#clienteunificar option:selected").each(function() {
                                    var texto = $(this).text();
                                    var value = $(this).val();
                                    $("select#clienteunificar option[value='" + value + "']").remove();
                                    if (value != clientebase) {
                                        var table = $('#tbclientes').DataTable();
                                        table.row('#row_per' + value).remove().draw(false);
                                        $("select#clientebase option[value='" + value + "']").remove();

                                    }
                                    setTimeout(INICIALIZARLISTAS('MODAL'), 1000);
                                });
                                ok(data[0].msn)
                            } else {
                                error(data[0].msn)
                            }
                        }, "json");
                } else {
                    return false;
                }
            });
        }
    }
}

function cargarlistaclientes() {
    $('#tabladatos').html("");
    $.post('funciones/fnClientes.php', {
            opcion: 'CARGARLISTACLIENTES'
        },
        function(data) {
            $('#tabladatos').html(data);
        });
}

function cargarciudad(sel1, sel2, sel3) {
    var dep = $('#' + sel1).val();
    var pai = $('#' + sel3).val();
    var ciu = $('#' + sel2);
    ciu.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
    $.post('funciones/fnClientes.php', {
            opcion: 'CARGACIUDAD',
            departamento: dep,
            pais: pai
        },
        function(data) {
            ciu.empty();
            ciu.append('<option value="">--seleccione--</option>');

            for (var i = 0; i < data.length; i++) {
                ciu.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
            }

        }, "json");
}

function validartipopersona() {
    var tp = $('#seltipopersona').val();
    if (tp == 2) {
        $('#divrazon').hide(1000);
        $('#divdescripcion').hide(1000);
        $('#txtrazon').val('');
        $('#txtdescripcion').val('');
    } else {
        $('#divrazon').show(1000);
        $('#divdescripcion').show(1000);
        $('#txtrazon').val('');
        $('#txtdescripcion').val('');
    }
}

function CRUDTIPOPROYECTO(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Tipo Proyecto");
        $('#contenido').html('');
        $.post('funciones/fnTipoproyecto.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            });
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Tipo Poyecto");
        $('#contenido').html('');
        $.post('funciones/fnTipoproyecto.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtnombre = $('#txtnombre').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del tipo proyecto. Verificar');
        } else {
            $.post('funciones/fnTipoproyecto.php', {
                    opcion: o,
                    nombre: txtnombre
                },
                function(data) {
                    if (data == 1) {
                        ok('Tipo proyecto registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTATIPOPROYECTO'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un tipo proyecto con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al guardar el tipo proyecto. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del tipo proyecto. Verificar');
        } else {
            $.post('funciones/fnTipoproyecto.php', {
                    opcion: o,
                    nombre: txtnombre,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Tipo Proyecto actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTATIPOPROYECTO'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un tipo proyecto con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al actualizar el tipo proyecto. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el tipo de proyecto seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el tipo de proyecto seleccionado");
            if (r == true) {
                $.post('funciones/fnTipoproyecto.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Tipo  de Proyecto eliminado correctamente');
                            var table = $('#tbtipoproyecto').DataTable();
                            table.row('#row_tpp' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el tipo de proyecto. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDESTADOSPROYECTO(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Estado de Presupuesto");
        $('#contenido').html('');
        $.post('funciones/fnEstadosproyecto.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Estado de Presupuesto");
        $('#contenido').html('');
        $.post('funciones/fnEstadosproyecto.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del Estado de Presupuesto. Verificar');
        } else {
            $.post('funciones/fnEstadosproyecto.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Estado de Presupuesto registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTAESTADOSPROYECTO'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un Estado de Presupuesto con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al guardar el Estado de Presupuesto. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del Estado de Presupuesto. Verificar');
        } else {
            $.post('funciones/fnEstadosproyecto.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Estado de Presupuesto actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTAESTADOSPROYECTO'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un Estado de Presupuesto con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al actualizar el Estado de Presupuesto. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el Estado de Presupuesto seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el estado de proyecto seleccionado");
            if (r == true) {
                $.post('funciones/fnEstadosproyecto.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Estado de Presupuesto eliminado correctamente');
                            var table = $('#tbestados').DataTable();
                            table.row('#row_esp' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el Estado de Presupuesto. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDTIPOCONTRATO(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Tipo Contrato");
        $('#contenido').html('');
        $.post('funciones/fnTipocontrato.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Tipo Contrato");
        $('#contenido').html('');
        $.post('funciones/fnTipocontrato.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del tipo Contrato. Verificar');
        } else {
            $.post('funciones/fnTipocontrato.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Tipo Contrato registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTATIPOCONTRATO'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un tipo Contrato con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al guardar el tipo Contrato. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del tipo Contrato. Verificar');
        } else {
            $.post('funciones/fnTipocontrato.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Tipo Contrato actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTATIPOCONTRATO'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un tipo Contrato con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al actualizar el tipo Contrato. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el tipo de contrato seleccionado?', 'Diálogo Confirmación LG', function(r) {
            // var conf = confirm("Realmente desea eliminar el tipo de contrato seleccionado");
            if (r == true) {
                $.post('funciones/fnTipocontrato.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Tipo  de Contrato eliminado correctamente');
                            var table = $('#tbtipocontrato').DataTable();
                            table.row('#row_tpc' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el tipo de contrato. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDUNIDADES(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');
    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD

    {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nueva Unidad de Medida");
        $('#contenido').html('');
        $.post('funciones/fnUnidades.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            });
    } else
    if (o == "UNIFICAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDARUNIFICAR');
        $('#myModalLabel').html("Unificar Unidades de Medida");
        $('#contenido').html('');
        $.post('funciones/fnUnidades.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "GUARDARUNIFICAR") {
        var uniu = $('#unidadunificar').val();
        var unip = $('#unidadpor').val();
        if (uniu == "" || uniu == null) {
            error('Seleccionar las unidades a unificar');

        } else if (unip == "") {
            error('Seleccionar la unidad base');
        } else {
            jConfirm("Reamente Desea unificar las unidades seleccionadas por la nueva base", "Diálogo de Confirmación LG", function(r) {
                if (r == true) {
                    $.post('funciones/fnUnidades.php', {
                            opcion: o,
                            uniu: uniu,
                            unip: unip
                        },
                        function(data) {
                            res = $.trim(data[0].res);
                            if (res == 1) {
                                $("#unidadunificar option:selected").each(function() {
                                    var texto = $(this).text();
                                    var value = $(this).val();
                                    $("select#unidadunificar option[value='" + value + "']").remove();
                                    if (value != unip) {
                                        $("select#unidadpor option[value='" + value + "']").remove();
                                    }
                                    setTimeout(INICIALIZARLISTAS('MODAL'), 1000);
                                });
                                ok(data[0].msn);
                                cargarlistadostipo('CARGARLISTAUNIDADES');
                            } else {
                                error(data[0].msn)
                            }

                        }, "json");
                } else {
                    return false;
                }
            });
        }
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Unidad de Medida");
        $('#contenido').html('');
        $.post('funciones/fnUnidades.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtcodigo = $('#txtcodigo').val();
        var txtnombre = $('#txtnombre').val();
        //var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtcodigo == "" || txtcodigo == null) {
            error('Diligenciar el codigo de la unidad de medida. Verificar');
        } else
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre de la unidad de medida. Verificar');
        } else {
            $.post('funciones/fnUnidades.php', {
                    opcion: o,
                    codigo: txtcodigo,
                    nombre: txtnombre /*,estado:radestado*/
                },
                function(data) {
                    if (data == 1) {
                        ok('Unidad de Medida registrada correctamente');
                        var table = $('#tbunidades').DataTable();
                        table.draw(false);
                        // $('#myModal').modal('hide');
                        //setTimeout(cargarlistadostipo('CARGARLISTAUNIDADES'),1000);
                    } else if (data == 2) {
                        error('Ya hay una unidad de medida con el codigo o nombre ingresados. Verificar');
                    } else {
                        error('Surgió un error al guardar la unidad de medida. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtcodigo = $('#txtcodigo').val();
        var txtnombre = $('#txtnombre').val();
        //var radestado = $('input:radio[name=radestado]:checked').val();
        if (txtcodigo == "" || txtcodigo == null) {
            error('Diligenciar el codigo de la unidad de medida. Verificar');
        } else
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre de la unidad de medida');
        } else {
            $.post('funciones/fnUnidades.php', {
                    opcion: o,
                    nombre: txtnombre,
                    codigo: txtcodigo /*,estado:radestado*/ ,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Unidad de Medida actualizado correctamente');
                        // $('#myModal').modal('hide');
                        var table = $('#tbunidades').DataTable();
                        table.row('#row_uni' + idedi).draw(false);
                    } else if (data == 2) {
                        error('Ya hay una unidad de medida con el codigo o nombre ingresados. Verificar');
                    } else {
                        error('Surgió un error al actualizar el unidad de medida. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar la unidad seleccionada?', 'Diálogo Confirmación LG', function(r) {
            // var conf = confirm("Realmente desea eliminar la unidad seleccionada");
            if (r == true) {
                $.post('funciones/fnUnidades.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Unidad eliminada correctamente');
                            var table = $('#tbunidades').DataTable();
                            table.row('#row_uni' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar la unidad. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDCAPITULOS(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Capitulo");
        $('#contenido').html('');
        $.post('funciones/fnCapitulos.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Capitulo");
        $('#contenido').html('');
        $.post('funciones/fnCapitulos.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del capitulo. Verificar');
        } else {
            $.post('funciones/fnCapitulos.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Capitulo registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTACAPITULOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un capitulo con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al guardar el capitulo. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del capitulo. Verificar');
        } else {
            $.post('funciones/fnCapitulos.php', {
                    opcion: o,
                    nombre: txtnombre,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Capitulo actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTACAPITULOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un capitulo con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al actualizar el capitulo. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el capitulo seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el capitulo seleccionado");
            if (r == true) {
                $.post('funciones/fnCapitulos.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Capitulo eliminado correctamente');
                            var table = $('#tbcapitulos').DataTable();
                            table.row('#row_cap' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el capitulo. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDGRUPOS(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').css('visibility', 'visible');
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Grupo");
        $('#contenido').html('');
        $.post('funciones/fnGrupos.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').css('visibility', 'visible');
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Grupo");
        $('#contenido').html('');
        $.post('funciones/fnGrupos.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtnombre = $('#txtnombre').val();
        var selcategoria = $('#selcategoria').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del grupo');
        } else
        if (selcategoria == "" || selcategoria == null) {
            error('Seleccionar la categoria a la cual pertenece (Informe Ejecutivo)');
        } else {
            $.post('funciones/fnGrupos.php', {
                    opcion: o,
                    nombre: txtnombre,
                    categoria: selcategoria
                },
                function(data) {
                    if (data == 1) {
                        ok('Grupo registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTAGRUPOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un Grupo con el nombre ingresado. Verificar');
                    } else {
                        error('Surgió un error al guardar el Grupo. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var selcategoria = $('#selcategoria').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del Grupo. Verificar');
        } else
        if (selcategoria == "" || selcategoria == null) {
            error('Seleccionar la categoria a la cual pertenece (Informe Ejecutivo)');
        } else {
            $.post('funciones/fnGrupos.php', {
                    opcion: o,
                    nombre: txtnombre,
                    id: idedi,
                    categoria: selcategoria
                },
                function(data) {
                    if (data == 1) {
                        ok('Grupo actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTAGRUPOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un capitulo con el nombre ingresado. Verifica');
                    } else {
                        error('Surgió un error al actualizar el capitulo. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el Grupo seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el Grupo seleccionado");
            if (r == true) {
                $.post('funciones/fnGrupos.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Grupo eliminado correctamente');
                            var table = $('#tbgrupos').DataTable();
                            table.row('#row_gru' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el grupo. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "ASOCIAR") {
        $('#btnguardar').attr('name', 'GUARDARASOCIAR');
        $('#btnguardar').css('visibility', 'hidden');
        $('#myModalLabel').html("Asociar Capitulos");
        $('#contenido').html('');
        $.post('funciones/fnGrupos.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "AGREGARCAPITULO") {
        var ide = $('#idasociar').val();
        $.post('funciones/fnGrupos.php', {
                opcion: o,
                idgrupo: ide,
                capitulo: id
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {
                    var table = $('#tbcapitulo1').DataTable();
                    table.row('#rowc' + id).remove().draw(false);
                    var table2 = $('#tbcapitulo2').DataTable();
                    table2.draw(false);
                    ok('Capitulo(s) agregado(s) correctamente');
                } else if (data == "error") {
                    error('Surgió un error al agregar capitulo(s). Verificar');
                }
            });
    } else if (o == "ELIMINARCAPITULO") {

        $.post('funciones/fnGrupos.php', {
                opcion: o,
                id: id
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {
                    var table = $('#tbcapitulo2').DataTable();
                    table.row('#rowd' + id).remove().draw(false);
                    var table2 = $('#tbcapitulo1').DataTable();
                    table2.draw(false);
                    ok('Capitulo(s) eliminado(s) correctamente');

                } else {
                    error('Surgió un error al eliminar capitulo(s). Verificar');
                }
            });
    } else if (o == "AGREGARTODOS") {
        var ide = $('#idasociar').val();
        $.post('funciones/fnGrupos.php', {
                opcion: o,
                id: ide
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {

                    var table = $('#tbcapitulo1').DataTable();
                    table.draw(false);
                    var table2 = $('#tbcapitulo2').DataTable();
                    table2.draw(false);
                    ok('Capitulos(s) agregado(s) correctamente');
                    setTimeout(cargarlistadostipo('CARGARLISTAGRUPOS'), 2000);

                } else {
                    error('Surgió un error al agregar permiso(s). Verificar');
                }
            });
    } else if (o == "ELIMINARTODOS") {
        var ide = $('#idasociar').val();
        $.post('funciones/fnGrupos.php', {
                opcion: o,
                id: ide
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {
                    var table = $('#tbcapitulo1').DataTable();
                    table.draw(false);
                    var table2 = $('#tbcapitulo2').DataTable();
                    table2.draw(false);
                    ok('Capitulos(s) eliminados(s) correctamente');

                    setTimeout(cargarlistadostipo('CARGARLISTAGRUPOS'), 2000);
                } else {
                    error('Surgió un error al eliminar capitulos(s). Verificar');
                }
            });
    }
}

function ordenargrupo(id1, id2) {
    $.post('funciones/fnGrupos.php', {
            opcion: 'CAMBIARORDEN',
            id1: id1,
            id2: id2
        },
        function(data) {
            if (data == 2) {
                jError("Surgió un error BD al cambiar el orden de las filas", "Diálogo error LG");
            }
        })
}

function ordenargrupo2(ord1, ord2, ide) {
    //alert(ord1+" - "+ord2)
    $.post('funciones/fnGrupos.php', {
            opcion: 'CAMBIARORDEN2',
            ord1: ord1,
            ord2: ord2,
            id: ide
        },
        function(data) {
            if (data == 2) {
                jError("Surgió un error BD al cambiar el orden de las filas", "Diálogo de error LG");
            }
        })
}

function CRUDTIPOINSUMOS(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nueva Tipo de Recurso");
        $('#contenido').html('');
        $.post('funciones/fnTipoinsumos.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Tipo de Recurso");
        $('#contenido').html('');
        $.post('funciones/fnTipoinsumos.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var seltipologia = $('#seltipologia').val();
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val()

        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del tipo de recurso. Verificar');
        } else
        if (seltipologia == "" || seltipologia == null) {
            error('Seleccionar la tipologia del tipo de recurso');
        } else {
            $.post('funciones/fnTipoinsumos.php', {
                    opcion: o,
                    tipologia: seltipologia,
                    nombre: txtnombre,
                    estado: radestado
                },
                function(data) {
                    if (data == 1) {
                        ok('Tipo de recurso registrado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTATIPOINSUMOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay una tipo de recurso con la tipologia y nombre ingresados. Verificar');
                    } else {
                        error('Surgió un error al guardar el tipo de recurso. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var seltipologia = $('#seltipologia').val();
        var txtnombre = $('#txtnombre').val();
        var radestado = $('input:radio[name=radestado]:checked').val();

        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del tipo de recurso');
        } else
        if (seltipologia == "" || seltipologia == null) {
            error('Seleccionar la tipologia del tipo de recurso');
        } else {
            $.post('funciones/fnTipoinsumos.php', {
                    opcion: o,
                    nombre: txtnombre,
                    tipologia: seltipologia,
                    estado: radestado,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Tipo de recurso actualizado correctamente');
                        // $('#myModal').modal('hide');
                        setTimeout(cargarlistadostipo('CARGARLISTATIPOINSUMOS'), 1000);
                    } else if (data == 2) {
                        error('Ya hay un tipo de recurso con el codigo y nombre ingresados. Verificar');
                    } else {
                        error('Surgió un error al actualizar el tipo de recurso. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el tipo de recurso seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el tipo de recurso seleccionado");
            if (r == true) {
                $.post('funciones/fnTipoinsumos.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Tipo  de Recurso eliminado correctamente');
                            var table = $('#tbtipoinsumos').DataTable();
                            table.row('#row_tpi' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el tipo de recurso. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
}

function CRUDINSUMOS(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    $('#msn').html('');

    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('.modal-dialog').removeClass('modal-lg');
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#myModalLabel').html("Registro Nuevo Recurso");
        $('#contenido').html('');
        $.post('funciones/fnInsumos.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('.modal-dialog').removeClass('modal-lg');
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#myModalLabel').html("Editar Recurso");
        $('#contenido').html('');
        $.post('funciones/fnInsumos.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtvalor = $('#txtvalor').val();
        var txtnombre = $('#txtnombre').val();
        var seltipoinsumo = $('#seltipoinsumo').val();
        var selunidad = $('#selunidad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        //var radestado = $('input:radio[name=radestado]:checked').val()

        if (seltipoinsumo == "" || seltipoinsumo == null) {
            error('Seleccionar el tipo de recurso. Verificar');
        } else
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del recurso. Verificar');
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida del recurso. Verificar');
        } else
        if (txtvalor == "" || txtvalor == null) {
            error('Diligenciar el valor unitario del recurso. Verificar');
        } else {
            $.post('funciones/fnInsumos.php', {
                    opcion: o,
                    tipoinsumo: seltipoinsumo,
                    nombre: txtnombre,
                    valor: txtvalor,
                    unidad: selunidad,
                    descripcion: txtdescripcion /*,estado:radestado*/
                },
                function(data) {
                    if (data == 1) {
                        ok('Recurso registrado correctamente');
                        var table = $('#tbinsumos').DataTable();
                        table.draw(false);
                        //setTimeout(cargarlistadostipo('CARGARLISTAINSUMOS'),1000);
                    } else if (data == 2) {
                        error('Ya hay una recurso con el nombre y tipo de recurso seleccionado. Verificar');
                    } else {
                        error('Surgió un error al guardar el recurso. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var txtvalor = $('#txtvalor').val();
        var txtnombre = $('#txtnombre').val();
        var seltipoinsumo = $('#seltipoinsumo').val();
        var selunidad = $('#selunidad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        //var radestado = $('input:radio[name=radestado]:checked').val()

        if (seltipoinsumo == "" || seltipoinsumo == null) {
            error('Seleccionar el tipo de recurso. Verificar');
        } else
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del recurso. Verificar');
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida del recurso. Verificar');
        } else
        if (txtvalor == "" || txtvalor == null) {
            error('Diligenciar el valor unitario del recurso. Verificar');
        } else {
            $.post('funciones/fnInsumos.php', {
                    opcion: o,
                    tipoinsumo: seltipoinsumo,
                    nombre: txtnombre,
                    valor: txtvalor,
                    unidad: selunidad,
                    descripcion: txtdescripcion,
                    /*estado:radestado,*/ id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Recurso actualizado correctamente');
                        var table = $('#tbinsumos').DataTable();
                        table.row('#row_' + idedi).draw(false);
                        // $('#myModal').modal('hide');
                        //setTimeout(cargarlistadostipo('CARGARLISTAINSUMOS'),1000);
                    } else if (data == 2) {
                        error('Ya hay una recurso con el nombre y tipo de recurso seleccionado. Verificar');
                    } else {
                        error('Surgió un error al actualizar el recurso. Verificar');
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar el recurso seleccionado?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar el recurso seleccionado");
            if (r == true) {
                $.post('funciones/fnInsumos.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Recurso eliminado correctamente');
                            var table = $('#tbinsumos').DataTable();
                            table.row('#row_' + id).remove().draw(false);
                        } else {
                            error('Surgió un error al eliminar el recurso. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else
    if (o == "UNIFICAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('.modal-dialog').addClass('modal-lg');
        $('#btnguardar').attr('name', 'GUARDARUNIFICAR');
        $('#myModalLabel').html("Unificar Recursos");
        $('#contenido').html('');
        $.post('funciones/fnInsumos.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "GUARDARUNIFICAR") {
        var uniu = $('#insumounificar').val();
        var unip = $('#insumopor').val();
        if (uniu == "" || uniu == null) {
            error('Seleccionar los recursos a unificar');
        } else if (unip == "") {
            error('Seleccionar el recurso base');
        } else {
            jConfirm("Realmente desea unificar los recursos seleccionados por el nuevo recurso base", "Diálogo de Confirmación LG", function(r) {
                if (r == true) {
                    $.post('funciones/fnInsumos.php', {
                            opcion: o,
                            uniu: uniu,
                            unip: unip
                        },
                        function(data) {
                            res = $.trim(data[0].res);
                            if (res == 1) {
                                $("#insumounificar option:selected").each(function() {
                                    var texto = $(this).text();
                                    var value = $(this).val();
                                    $("select#insumounificar option[value='" + value + "']").remove();
                                    if (value != unip) {
                                        $("select#insumopor option[value='" + value + "']").remove();
                                    }
                                    setTimeout(INICIALIZARLISTAS('MODAL'), 1000);
                                });
                                ok(data[0].msn)
                                var table = $('#tbinsumos').DataTable();
                                table.draw(false);
                                //cargarlistadostipo('CARGARLISTAINSUMOS');
                            } else {
                                error(data[0].msn)
                            }
                        }, "json");
                } else {
                    return false;
                }
            });
        }
    }
}

function CRUDACTIVIDADES(o, id) //v = CIUDAD, DEPARTAMENTO,PAIS
{


    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    { //$('.modal-dialog').removeClass('modal-lg');
        var tipa = $('#opcanalisis2').val();

        $('#msn').html('');
        $('#btnguardar').attr('name', 'GUARDAR');
        $('#btnguardar').attr('value', 'Guardar Cambios');
        if (tipa == "No") {
            $('#myModalLabel').html("Registro Nueva Actividad");
        } else {
            $('#myModalLabel').html("Registro Nuevo Subanalisis");
        }
        $('#contenido').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        //$('.modal-dialog').removeClass('modal-lg');
        $('#msn').html('');
        $('#btnguardar').attr('name', 'GUARDAREDICION');
        $('#btnguardar').attr('value', 'Guardar Cambios');
        $('#myModalLabel').html("Editar Actividades");
        $('#contenido').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "ACTIVIDADESPORAPROBAR") //REGISTRO DE NUEVA CIUDAD
    {
        var suba = $('#opcanalisis').val();
        $('.modal-dialog').addClass('modal-lg');
        //$('#filtroporabrobar').css('display','block');
        //$('#btnguardar').attr('name','GUARDARSELECCION');
        //$('#btnguardar').attr('value','Guardar Selección');
        if (suba == "No" || suba == null) {
            $('#myModalLabel2').html("Aprobacion Actividades");
        } else {
            $('#myModalLabel2').html("Aprobacion Actividades");
        }
        $('#contenido3').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o,
                id: id,
                suba: suba
            },
            function(data) {
                $('#contenido3').html(data);
            })
    } else if (o == "CARGARPROYECTOS") {
        var cor = $('#buscoordinador').val();
        var pro = $('#busproyecto');
        pro.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        //alert(pai);
        $.post('funciones/fnActividades.php', {
                opcion: o,
                cor: cor
            },
            function(data) {
                pro.empty();
                pro.append('<option value="">--seleccione--</option>');
                var res = data[0].res;
                if (res == "no") {} else {
                    for (var i = 0; i < data.length; i++) {
                        pro.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                    }
                }
                $('#busproyecto').selectpicker('refresh');
                setTimeout(INICIALIZARLISTAS(''), 1000);
                setTimeout(CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR', ''), 1000);

            }, "json");

    } else if (o == "APROBARACTIVIDAD") {
        var tipa = $('#opcanalisis2').val();
        $.post('funciones/fnActividades.php', {
                opcion: o,
                id: id,
                suba: tipa
            },
            function(data) {
                if (data[0].respuesta == 1) {
                    var table = $('#tbactividadesaprobar').DataTable();
                    table.row('#row_' + id).remove().draw(false);
                    $('#pendientesaprobar').html(data[0].pendientes);
                    if (tipa == "No") {
                        ok('Actividad aprobada correctamente');
                        setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'), 1000);
                    } else {
                        ok('SubAnalisis aprobado correctamente');
                        setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADESSUB'), 1000);
                    }

                } else {
                    error('Surgió un error al aprobar actividad ' + id + '. Verificar');
                }
            }, "json");
    } else if (o == "DESAPROBARACTIVIDAD") {
        var tipa = $('#opcanalisis2').val();
        $.post('funciones/fnActividades.php', {
                opcion: o,
                id: id,
                suba: tipa
            },
            function(data) {
                if (data[0].respuesta == 1) {
                    var table = $('#tbactividadesaprobar').DataTable();
                    table.row('#row_' + id).remove().draw(false);
                    $('#pendientesaprobar').html(data[0].pendientes);
                    if (tipa == "No") {
                        ok('Actividad No Aprobada');
                        setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'), 1000);
                    } else {
                        ok('SubAnalisis No aprobado');
                        setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADESSUB'), 1000);
                    }

                } else {
                    error('Surgió un error al no aprobar la actividad ' + id + '. Verificar')
                }
            }, "json");
    } else if (o == "ACTUALIZARPENDIENTE") {
        var tipa = $('#opcanalisis2').val();
        $.post('funciones/fnActividades.php', {
                opcion: o,
                suba: tipa
            },
            function(data) {
                $('#pendientesaprobar').html(data[0].pendientes);
            }, "json");
    } else if (o == "PENDIENTES") //REGISTRO DE NUEVA CIUDAD
    {
        var ida = $('#idedicion').val();
        $('#pendientes').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o,
                ida: ida
            },
            function(data) {
                $('#pendientes').html(data);
            })
    } else if (o == "PENDIENTESSUB") //ASIGNAR SUBANALISIS
    {
        $('#pendientes').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o
            },
            function(data) {
                $('#pendientes').html(data);
            })
    } else if (o == "AGREGADOS") //REGISTRO DE NUEVA CIUDAD
    {
        $('#pendientes').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#pendientes').html(data);
            })
    } else if (o == "AGREGADOSSUB") //REGISTRO DE NUEVA CIUDAD
    {
        $('#pendientes').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#pendientes').html(data);
            })
    } else if (o == "AGREGARPENDIENTE") {
        var ins = id; //$('#selagregar').val();
        // var ren = $('#rend'+id).val();// $('#rendimiento').val();
        //var val = $('#valorunitario').val();
        if (ins == "" || ins == null) {
            error('Seleccionar el recurso a agregar. Verificar')
        }
        /* else if(ren<=0 || ren==null || ren=="")
	   {
		   error('El rendimiento debe ser mayor a cero. Verificar')
	   }*/
        else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    insumo: ins,
                    rendimiento: ren
                },
                function(data) {
                    if (data == 1) {
                        //var table = $('#tbinsumosporagregar').DataTable();
                        //table.row('#rowi_'+id).remove().draw(true);
                        var table2 = $('#tbinsumospendientes').DataTable();
                        table2.draw(false);
                        $("select#selagregar option[value='" + ins + "']").remove();
                        ok('Recurso agregado correctamente. Verificar');
                        $('#selagregar').selectpicker('refresh');
                        //setTimeout(INICIALIZARLISTAS(''),1000);
                    } else {
                        error('Error al agregar recurso. Verificar')
                    }
                });
        }
    } else if (o == "DELETEPENDIENTE") {
        jConfirm('Realmente desea quitar el recurso agregado?', 'Diálogo Confirmación LG', function(r) {
            //msn = confirm("Realmente desea quitar el recurso agregado");
            if (r == true) {
                var ide = id;
                $.post('funciones/fnActividades.php', {
                        opcion: o,
                        id: ide
                    },
                    function(data) {
                        if (data == 1) {
                            //CRUDACTIVIDADES('PENDIENTES','');
                            var table = $('#tbinsumospendientes').DataTable();
                            table.row('#row_insp' + ide).remove().draw(false);
                            //var table2 = $('#tbinsumosporagregar').DataTable();
                            //table2.draw(false);
                            ok('Recurso eliminado correctamente');
                            setTimeout(CRUDACTIVIDADES('CARGARLISTAPENDIENTES', ''), 1000);


                            //setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'),1000);
                        } else {
                            error('Error al eliminar recurso. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });

    } else
    if (o == "AGREGAR") {
        var ide = $('#idedicion').val();
        var ins = id; //$('#selagregar').val();
        var ren = 0;
        //var val = $('#valorunitario').val();
        if (ins == "" || ins == null) {
            error('Seleccionar el recurso a agregar. Verificar')
        }
        /* else if(ren<=0 || ren==null || ren=="")
	   {
		   error('El rendimiento debe ser mayor a cero. Verificar')
	   }*/
        else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    insumo: ins,
                    id: ide,
                    rendimiento: ren
                },
                function(data) {
                    if (data == 1) {
                        $("select#selagregar3 option[value='" + ins + "']").remove();
                        //var table = $('#tbinsumosporagregar').DataTable();
                        //table.row('#rowi_'+id).remove().draw(true);
                        var table2 = $('#tbinsumospendientes').DataTable();
                        table2.draw(false);
                        var table3 = $('#tbactividades').DataTable();
                        table3.row('#row_' + ide).draw(false);
                        $('#selagregar3').selectpicker('refresh');
                        ok('Recurso agregado correctamente')

                        //setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'),1000);
                    } else {
                        error('Error al agregar recurso. Verificar')

                    }
                });
        }
    } else
    if (o == "DELETE") {
        jConfirm('Realmente desea quitar el recurso agregado?', 'Diálogo Confirmación LG', function(r) {
            //msn = confirm("Realmente desea quitar el recurso agregado");
            if (r == true) {
                var ide = $('#idedicion').val();
                $.post('funciones/fnActividades.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            var table = $('#tbinsumospendientes').DataTable();
                            table.row('#row_insp' + id).remove().draw(false);
                            //var table2 = $('#tbinsumosporagregar').DataTable();
                            //table2.draw(false);
                            var table3 = $('#tbactividades').DataTable();
                            table3.row('#row_' + ide).draw(false);
                            ok('Recurso eliminado correctamente.')
                            setTimeout(CRUDACTIVIDADES('CARGARLISTAAGREGADOS', ''), 1000);
                            //setTimeout("$('#selagregar3').selectpicker('refresh')",2000);
                            //setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'),1000);
                        } else {
                            error('Error al eliminar recurso. Verificar')
                        }
                    });
            } else {
                return false;
            }
        });
    }
    //SUBANALISIS
    else if (o == "AGREGARPENDIENTESUB") {
        var act = id; //$('#selagregar').val();
        var rend = $('#renda' + id).val();
        if (act == "" || act == null) {
            error('Seleccionar la actividad a agregar. Verificar')
        } else if (rend == "" || rend == null || rend <= 0) {
            error('El rendimiento no puede ser vacio,menor o igual a cero. Verificar')
        } else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    suba: act,
                    rend: rend
                },
                function(data) {
                    if (data == 1) {
                        var table = $('#tbactividadporagregar').DataTable();
                        table.row('#rowa_' + id).remove().draw(true);
                        var table2 = $('#tbactividadagregados').DataTable();
                        table2.draw(false);
                        //CRUDACTIVIDADES('PENDIENTES','');
                        ok('Actividad SubAnalisis agregada correctamente');
                    } else {
                        error('Error al agregar actividad. Verificar')
                    }
                });
        }
    } else if (o == "DELETEPENDIENTESUB") {
        jConfirm('Realmente desea quitar la actividad agregada?', 'Diálogo Confirmación LG', function(r) {
            //msn = confirm("Realmente desea quitar el recurso agregado");
            if (r == true) {
                var ide = id;
                $.post('funciones/fnActividades.php', {
                        opcion: o,
                        id: ide
                    },
                    function(data) {
                        if (data == 1) {
                            //CRUDACTIVIDADES('PENDIENTES','');
                            var table = $('#tbactividadagregados').DataTable();
                            table.row('#row_actp' + ide).remove().draw(false);
                            var table2 = $('#tbactividadporagregar').DataTable();
                            table2.draw(false);
                            ok('Actividad elimina correctamente')
                            //CRUDACTIVIDADES('CARGARLISTAPENDIENTES','');
                            setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'), 1000);
                        } else {
                            error('Error al eliminar actividad. Verificar')
                        }
                    });
            } else {
                return false;
            }
        });

    } else
    if (o == "AGREGARSUB") {
        var ide = $('#idedicion').val();
        var rend = $('#renda' + id).val();
        var act = id;
        if (act == "" || act == null) {
            error('Seleccionar la actividad a agregar. Verificar')
        } else if (rend == "" || rend == null || rend <= 0) {
            error('El rendimiento no puede ser vacio,menor o igual a cero. Verificar')
        } else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    suba: act,
                    id: ide,
                    rend: rend
                },
                function(data) {
                    var res = data[0].res;
                    if (res == 1) {
                        var table = $('#tbactividadporagregar').DataTable();
                        table.row('#rowa_' + id).remove().draw(true);
                        var table2 = $('#tbactividadagregados').DataTable();
                        table2.draw(false);
                        var table3 = $('#tbactividades').DataTable();
                        table3.row('#row_' + ide).draw(false);
                        $('#totalapu').html(data[0].valor);
                        ok('SubAnalisis agregada correctamente')
                        //setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'),1000);
                    } else {
                        error('Error al agregar subanalisis. Verificar');
                    }
                }, "json");
        }
    } else
    if (o == "DELETESUB") {
        jConfirm('Realmente desea quitar el actividad agregada?', 'Diálogo Confirmación LG', function(r) {
            //msn = confirm("Realmente desea quitar el recurso agregado");
            if (r == true) {
                var ide = $('#idedicion').val();
                $.post('funciones/fnActividades.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        var res = data[0].res

                        if (res == 1) {
                            var table = $('#tbactividadagregados').DataTable();
                            table.row('#row_actp' + id).remove().draw(false);
                            var table2 = $('#tbactividadporagregar').DataTable();
                            table2.draw(false);
                            var table3 = $('#tbactividades').DataTable();
                            table3.row('#row_' + ide).draw(false);
                            $('#totalapu').html(data[0].valor);
                            ok('Actividad eliminada correctamente')
                        } else {
                            error('Error al eliminar actividad. Verificar')
                        }
                    }, "json");
            } else {
                return false;
            }
        });
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDAR") {
        var txtdescripcion = $('#txtdescripcion').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var selunidad = $('#selunidad').val();
        var selciudad = $('#selciudad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        var radanalisis = $('#opcanalisis2').val()
        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción de la actividad. Verificar')
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto de la actividad. Verificar')
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida de la actividad. Verificar')
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad de la actividad. Verificar');
        } else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    unidad: selunidad,
                    ciudad: selciudad,
                    analisis: radanalisis
                },
                function(data) {
                    if (data == 1) {
                        $(".modal").modal("hide");

                        ok('Actividad registrada correctamente')
                        setTimeout(CRUDACTIVIDADES('ACTUALIZARPENDIENTE', ''), 1000);
                        var table = $('#tbactividades').DataTable();
                        table.draw(false);
                    } else if (data == 2) {
                        error('Ya hay una actividad con el mismo nombre diligenciado,ciudad y tipo proyecto seleccionados.Verificar')
                    } else {
                        error('Surgió un error al guardar la actividad. Verificar')
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION") {
        var idedi = $('#idedicion').val();
        var per = $('#idpermiso').val();
        var txtdescripcion = $('#txtdescripcion').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var selunidad = $('#selunidad').val();
        var selciudad = $('#selciudad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        if (per == "no") {
            error("No esta autorizado de modificar esta Actividad. Debe Crear un actividad nueva y asignarle los recursos con los rendimientos que requiera.<a role='button' title='No esta autorizado de modificar Rendimientos. Debe Crear un actividad nueva y asignarle los recursos con los rendimientos que requiera.' class='btn btn-block btn-primary' onclick=CRUDACTIVIDADES('CREARACTIVIDAD','" + idedi + "')><i class='fa fa-plus'></i>Crear</a>")
        } else
        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción de la actividad. Verificar')
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto de la actividad. Verificar')
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida de la actividad. Verificar')
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad de la actividad')
        } else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    unidad: selunidad,
                    ciudad: selciudad,
                    id: idedi
                },
                function(data) {
                    if (data == 1) {
                        ok('Actividad actualizada correctamente')
                        setTimeout(CRUDACTIVIDADES('EDITAR', idedi), 1000);
                        setTimeout(CRUDACTIVIDADES('ACTUALIZARPENDIENTE', ''), 2000);
                        var table = $('#tbactividades').DataTable();
                        table.draw(false);
                    } else if (data == 2) {
                        error('Ya hay una actividad con el mismo nombre diligenciado, ciudad y tipo proyecto seleccionados.Verificar')
                    } else {
                        error('Surgió un error al actualizar la actividad. Verificar')
                    }
                });
        }
    } else if (o == "ELIMINAR") {
        jConfirm('Realmente desea eliminar la actividad seleccionada?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar la actividad seleccionada");
            if (r == true) {
                $.post('funciones/fnActividades.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Actividad eliminada correctamente')
                            var table = $('#tbactividades').DataTable();
                            table.row('#row_' + id).remove().draw(false);
                            setTimeout(CRUDACTIVIDADES('ACTUALIZARPENDIENTE', ''), 2000);
                        } else if (data == 3) {
                            error('Surgió un error. La actividad no se puede eliminar ya que se encuentra asociada a presupuesto creados. Verificar');
                        } else {
                            error('Surgió un error al eliminar la actividad. Verificar')
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "CARGARLISTAPENDIENTES") {

        var pen = $('#selagregar');
        pen.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnActividades.php', {
                opcion: o
            },
            function(data) {
                pen.empty();
                pen.append('<option value="">--seleccione--</option>');

                for (var i = 0; i < data.length; i++) {
                    pen.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                }
                setTimeout(INICIALIZARLISTAS(''), 2000);
                setTimeout("$('#selagregar').selectpicker('refresh')", 2000);
            }, "json");
    } else if (o == "CARGARLISTAAGREGADOS" || o == "CARGARLISTAAGREGADOS2") {

        if (o == "CARGARLISTAAGREGADOS2") {
            var pen = $('#selagregar2');
        } else {
            var pen = $('#selagregar3');
        }

        pen.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnActividades.php', {
                opcion: o,
                id: id
            },
            function(data) {
                pen.empty();
                pen.append('<option value="">--seleccione--</option>');

                for (var i = 0; i < data.length; i++) {
                    pen.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                }
                setTimeout(INICIALIZARLISTAS(''), 2000);
                setTimeout("$('#selagregar').selectpicker('refresh')", 2000);
                setTimeout("$('#selagregar3').selectpicker('refresh')", 2000);

            }, "json");
    } else if (o == "VALORINSUMO" || o == "VALORINSUMO2") {
        if (o == "VALORINSUMO") {
            var ins = $('#selagregar').val();
        } else {
            var ins = $('#selagregar2').val(); // jAlert(ins,null); 
        }
        $.post('funciones/fnActividades.php', {
                opcion: 'VALORINSUMO',
                id: ins
            },
            function(data) {
                // jAlert(data[0].valor,"Valor");
                $('#valorunitario').html("$" + data[0].valor);

            }, "json");


    } else if (o == "CREARACTIVIDAD") //REGISTRO DE NUEVA CIUDAD
    {
        $('.modal-dialog').addClass('modal-lg');
        $('#msn').html('');
        $('#btnguardar').attr('name', 'GUARDARNUEVA');
        $('#btnguardar').attr('rel', id);
        $('#btnguardar').attr('value', 'Guardar Cambios');
        $('#myModalLabel').html("Crear Nueva Actividad");
        $('#contenido').html('');
        $.post('funciones/fnActividades.php', {
                opcion: o,
                actividad: id
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "GUARDARNUEVA") {
        var act = id;
        var txtdescripcion = $('#txtdescripcion').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var selunidad = $('#selunidad').val();
        var selciudad = $('#selciudad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción de la actividad. Verificar');
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto de la actividad. Verificar')
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida de la actividad. Verificar')
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad de la actividad')
        } else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    unidad: selunidad,
                    ciudad: selciudad,
                    actividad: act
                },
                function(data) {
                    if (data[0].respuesta == 2) {
                        error('Ya hay una actividad con el mismo nombre diligenciado,ciudad y tipo proyecto seleccionados.Verificar');
                    } else if (data[0].respuesta == 3) {
                        error('Surgió un error al crear la nueva actividad.Verificar')
                    } else if (data[0].respuesta == 1) {
                        var ida = data[0].actividad;
                        var table = $('#tbinsumosnuevos').DataTable();

                        var nums = table.rows().data().length;
                        $("#tbinsumosnuevos tbody tr").each(function(index) {

                            var idi = $(this).attr('id');
                            var val = $('td', this).eq(4).text();
                            var ren = $('#rend' + idi).val();
                            $.post('funciones/fnActividades.php', {
                                    opcion: 'NUEVOINSUMO',
                                    actividad: ida,
                                    insumo: idi,
                                    valor: val,
                                    rendimiento: ren
                                },
                                function(dato) {
                                    if (dato == 1) {} else {
                                        error('Surgió un error al guardar recurso ' + idi + '.Verificar');
                                    }

                                });
                        });
                        ok("Nueva actividad creada correctamente.Verificar en maestra de actividades");
                        //$('.modal').modal('hide');

                        setTimeout(cargarlistadostipo('CARGARLISTAACTIVIDADES'), 2000);
                        setTimeout(CRUDACTIVIDADES('ACTUALIZARPENDIENTE', ''), 3000);
                    }
                }, "json");
        }
    } else if (o == "GUARDARUNIFICAR") {
        var ida = $('#idedicion').val();
        var uniu = $('#unidadunificar').val();
        var unip = $('#unidadpor').val();
        if (uniu == "" || uniu == null) {
            error('Seleccionar las unidades a unificar')
        } else if (unip == "") {
            error('Seleccionar la unidad base')
        } else {
            jConfirm("Reamente Desea unificar las unidades seleccionadas por la nueva base", "Diálogo de Confirmación LG", function(r) {
                if (r == true) {
                    $.post('funciones/fnUnidades.php', {
                            opcion: o,
                            uniu: uniu,
                            unip: unip
                        },
                        function(data) {
                            res = $.trim(data[0].res);
                            if (res == 1) {
                                /*$("#unidadunificar option:selected").each(function() {			
                                	var texto = $(this).text();
                                	var value = $(this).val();							
                                	$("select#unidadunificar option[value='"+value+"']").remove();
                                	if(value!=unip)
                                	{
                                	$("select#unidadpor option[value='"+value+"']").remove();
                                	}
                                	//setTimeout(INICIALIZARLISTAS('MODAL'),1000); 							
                                });*/
                                ok(data[0].msn)
                                setTimeout(CRUDACTIVIDADES('AGREGADOS', ida), 1000);
                            } else {
                                error(data[0].msn)
                            }
                            //$('#msn').html();
                        }, "json");
                } else {
                    return false;
                }
            });
        }
    } else if (o == "VERAPU") //REGISTRO DE NUEVA CIUDAD
    {

        $('#divchil' + id).html('');
        var table1 = "<div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos" + id + "'>";
        table1 += "<thead><tr><th>CODIGO</th><th>DESCRIPCIÓN</th><th>UND</th><th>REND</th><th>V/UNITARIO($)</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";
        $.post('funciones/fnActividades.php', {
                opcion: 'LISTAINSUMOS',
                id: id
            },
            function(dato) {
                for (var i = 0; i < dato.length; i++) {
                    table1 += "<tr>" +
                        "<td data-title='Codigo'>" + dato[i].insu + "</td>" +
                        "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                        "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                        "<td data-title='Rendimiento'>" + dato[i].rendi + "</td>" +
                        "<td data-title='Valor Unitario'>$" + dato[i].valor1 + "</td>" +
                        "<td data-title='Material'> <div id='divdetallem" + dato[i].iddetalle + "'>$" + dato[i].material + "</div></td>" +
                        "<td data-title='Equipo'><div id='divdetallee" + dato[i].iddetalle + "'>$" + dato[i].equipo + "</div></td>" +
                        "<td data-title='Mano de obra'> <div id='divdetallema" + dato[i].iddetalle + "'>$" + dato[i].mano + "</div></td></tr>";
                }
                //$('#divapu'+id).html(formato_numero(APU,2,'.',','));
                table1 += "</tbody></table></div>";
                $('#divchil' + id).html(table1);
                convertirdata(id);

            }, "json");
    } else if (o == "VERSUBANALISIS") {
        //verificar si actividad tiene subana
        $('#divchil' + id).html('');
        var table2 = "<table id='tbsubanalisis" + id + "' class='table table-condensed compact' width='100%' style='font-size:11px;'>" +
            "<thead>" +
            "<tr><th class='dt-head-center' style='width:20px'></th>" +
            "<th class='dt-head-center' style='width:30px'>CÓDIGO</th>" +
            "<th class='dt-head-center' style='width:300px'>NOMBRE</th>" +
            "<th class='dt-head-center' style='width:30px'>UND</th>" +
            "<th class='dt-head-center' style='width:30px'>REND</th>" +
            "<th class='dt-head-center' style='width:40px'>V/R APU</th>" +
            "<th class='dt-head-center' style='width:40px'>TOTAL</th>" +
            "<th class='dt-head-center' style='width:120px'>TIPO PROYECTO</th>" +
            "<th class='dt-head-center' style='width:60px'>CIUDAD</th>" +
            "<th class='dt-head-center' style='width:40px'>ESTADO</th>" +
            "<th class='dt-head-center' style='width:40px'>ANALISÍS</th>" +
            "</tr>" +
            "</thead><tfoot><tr><th></th><th></th><th></th><th></th><th> </th><th> </th><th></th><th></th><th></th><th></th><th></th></tr></tfoot></table>";
        $('#divchil' + id).html(table2);
        convertirsub(id);
    }

    //CREACION RECURSOS DESDE ACTIVIDADES
    else if (o == "NUEVOINSUMOA") {
        $('#divagregarinsumo').css('display', 'none');
        $('#divnuevoinsumo').css('display', 'block');
        $.post('funciones/fnActividades.php', {
                opcion: o
            },
            function(data) {
                $('#divnuevoinsumo').html(data);
            });
    } else if (o == "CANCELARNUEVOINSUMO") {
        $('#divagregarinsumo').css('display', 'block');
        $('#divnuevoinsumo').css('display', 'none').html('');
    } else if (o == "GUARDARNUEVOINSUMO") {
        var ti = $('#tipoinsumo').val();
        var no = $('#nombreinsumo').val();
        var uni = $('#unidadinsumo').val();
        var valo = $('#valorinsumo').val();
        //var rend  = $('#rendinsumo').val();
        if (no == "") {
            error("Diligenciar el nombre del recurso. Verificar");
        } else if (ti == "") {
            error("Seleccionar el tipo de recurso. Verificar");
        } else if (uni == "") {
            error("Seleccionar la unidad del recurso. Verificar");
        } else if (valo <= 0) {
            error("El valor unitario del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }
        /*else if(rend<=0)
        {
        	error("El rendimiento del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }*/
        else {
            $.post('funciones/fnActividades.php', {
                    opcion: o,
                    ti: ti,
                    nom: no,
                    uni: uni,
                    valor: valo /*,rend:rend*/
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == "ok") {
                        $("#tipoinsumo>option[value='']").attr('selected', 'selected');
                        $('#nombreinsumo').val('');
                        $("#unidadinsumo>option[value='']").attr('selected', 'selected');
                        $('#valorinsumo').val('');
                        $('#rendinsumo').val('');
                        //ok('Recurso creado y agregado correctamente');
                        setTimeout(CRUDACTIVIDADES('AGREGARPENDIENTE', data[0].ins), 1000);
                    } else if (res == "error1") {
                        error("Ya existe el recurso con nombre,unidad y valor unitario ingresados. Verificar");
                    } else if (res == "error2") {
                        error("Surgio un error al guardar recurso. Verificar");
                    }

                }, "json");
        }
    } else if (o == "NUEVOINSUMO2A") {
        $('#divagregarinsumo2').css('display', 'none');
        $('#divnuevoinsumo2').css('display', 'block');
        $.post('funciones/fnActividades.php', {
                opcion: o
            },
            function(data) {
                $('#divnuevoinsumo2').html(data);
            });
    } else if (o == "CANCELARNUEVOINSUMO2") {
        $('#divagregarinsumo2').css('display', 'block');
        $('#divnuevoinsumo2').css('display', 'none').html('');
    } else if (o == "GUARDARNUEVOINSUMO2") {
        var pre = $('#idedicion').val();
        var ti = $('#tipoinsumo').val();
        var no = $('#nombreinsumo').val();
        var uni = $('#unidadinsumo').val();
        var valo = $('#valorinsumo').val();
        //var rend  = $('#rendinsumo').val();
        if (no == "") {
            error("Diligenciar el nombre del recurso. Verificar");
        } else if (ti == "") {
            error("Seleccionar el tipo de recurso. Verificar");
        } else if (uni == "") {
            error("Seleccionar la unidad del recurso. Verificar");
        } else if (valo <= 0) {
            error("El valor unitario del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }
        /*else if(rend<=0)
        {
        	error("El rendimiento del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }*/
        else {
            $.post('funciones/fnActividades.php', {
                    opcion: 'GUARDARNUEVOINSUMO',
                    ti: ti,
                    nom: no,
                    uni: uni,
                    valor: valo,
                    pre: pre /*,rend:rend*/
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == "ok") {
                        $("#tipoinsumo>option[value='']").attr('selected', 'selected');
                        $('#nombreinsumo').val('');
                        $("#unidadinsumo>option[value='']").attr('selected', 'selected');
                        $('#valorinsumo').val('');
                        $('#rendinsumo').val('');
                        //ok('Recurso creado y agregado correctamente');						
                        var idn = data[0].ins
                        setTimeout(CRUDACTIVIDADES('AGREGAR', idn), 1000);
                    } else if (res == "error1") {
                        error("Ya existe el recurso con nombre,unidad y valor unitario ingresados. Verificar");
                    } else if (res == "error2") {
                        error("Surgio un error al guardar recurso. Verificar");
                    }

                }, "json");
        }
    }
}

function cargarlistadostipo(o) {
    if (o == "CARGARLISTACIUDADES" || o == "CARGARLISTADEPARTAMENTOS" || o == "CARGARLISTAPAISES") {
        $('#tabladatos').html("");
        $.post('funciones/fnLocalizacion.php', {
                opcion: o,
                ventana: ''
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else
    if (o == "CARGARLISTATIPOPROYECTO") {
        $('#tabladatos').html("");
        $.post('funciones/fnTipoproyecto.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAESTADOSPROYECTO") {
        //$('#tabladatos').html("");
        $.post('funciones/fnEstadosproyecto.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTATIPOCONTRATO") {
        $('#tabladatos').html("");
        $.post('funciones/fnTipocontrato.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAUNIDADES") {
        $('#tabladatos').html("");
        $.post('funciones/fnUnidades.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTACAPITULOS") {
        $('#tabladatos').html("");
        $.post('funciones/fnCapitulos.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTATIPOINSUMOS") {
        $('#tabladatos').html("");
        $.post('funciones/fnTipoinsumos.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAINSUMOS") {
        $('#tabladatos').html("");
        $.post('funciones/fnInsumos.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAACTIVIDADES" || o == "CARGARLISTAACTIVIDADESSUB") {
        if (o == "CARGARLISTAACTIVIDADES") {
            $('#opcanalisis2').val('No');
        } else {
            $('#opcanalisis2').val('Si');
        }
        var randomnumber = Math.random() * 11;
        $('#tabladatos').html("");
        $.post('funciones/fnActividades.php', {
                opcion: o,
                randomnumber: randomnumber
            },
            function(data) {
                $('#tabladatos').html(data);
            });
        //setTimeout(CRUDACTIVIDADES("ACTUALIZARPENDIENTE",''),10000);
    } else if (o == "CARGARLISTAPRESUPUESTO") {
        $('#tabladatos').html("");
        $.post('funciones/fnPresupuesto.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAHISTORICO") {
        $('#tabladatos').html("");
        $.post('funciones/fnPresupuesto.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAHISTORICOCONTROL") {
        $('#tabladatos').html("");
        $.post('funciones/fnPresupuesto.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else
    if (o == "CARGARLISTAUSUARIOS") {
        $('#tabladatos').html("");
        $.post('funciones/fnUsuarios.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAPERFILES") {
        $('#tabladatos').html("");
        $.post('funciones/fnPerfiles.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTACARGOS") {
        $('#tabladatos').html("");
        $.post('funciones/fnCargo.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "CARGARLISTAGRUPOS") {
        $('#tabladatos').html("");
        $.post('funciones/fnGrupos.php', {
                opcion: o
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    }
}


function guardarcantidad(id, v, pre, gru, cap) {
	
	//var dec = checkDecimals('rempc' + id);
	
    if (v == "") {
        error('Ingresar Cantidad');
	}
   else {
        $.post('funciones/fnPresupuesto.php', {
                opcion: "GUARDARDETALLE",
                id: id,
                cantidad: v
            },
            function(data) {
                var res = data[0].res;
                var act = data[0].act;
                if (res == 1) {
                    setTimeout(actualizartotal(id, act, pre, gru, cap), 2000); //
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                } else {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function guardarrendimiento(id, v, ida) {
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnActividades.php', {
                opcion: "GUARDARDETALLE",
                id: id,
                rendimiento: v,
                ida: ida
            },
            function(data) {
                if (data[0].respuesta == 1) {
                    //$('#divapu'+ida).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallem'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallee'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallema'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");

                    $('#divapu' + ida).html(data[0].valor);
                    $('#divdetallem' + id).html(data[0].material);
                    $('#divdetallee' + id).html(data[0].equipo);
                    $('#divdetallema' + id).html(data[0].mano);


                } else {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function guardarrendimientop(id, v, ida) //rendimiento de recursos pendinete en maestra de actividades
{
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnActividades.php', {
                opcion: "GUARDARDETALLEP",
                idd: id,
                rend: v
            },
            function(data) {
                var res = $.trim(data[0].res)
                if (res == "ok") {

                    $('#totalapu').html(data[0].valor);

                } else {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function guardarrendimientov(id, v, ida) {
    $('#spv' + id).html("<img alt='cargando' src='dist/img/ajax-loader.gif' height='15' width='15' />");
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnActividades.php', {
                opcion: "GUARDARDETALLEV",
                id: id,
                valor: v,
                ida: ida
            },
            function(data) {
                if (data[0].respuesta == 1) {
                    //$('#divapu'+ida).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallem'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallee'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallema'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    $('#divapu' + ida).html(data[0].valor);
                    $('#divdetallem' + id).html(data[0].material);
                    $('#divdetallee' + id).html(data[0].equipo);
                    $('#divdetallema' + id).html(data[0].mano);
                } else {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function guardarrendimiento2(id, v, ida) {
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnActividades.php', {
                opcion: "GUARDARDETALLE",
                id: id,
                rendimiento: v,
                ida: ida
            },
            function(data) {
                if (data[0].respuesta == 1) {
                    //$('#divapu'+ida).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallem'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallee'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallema'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    $('#totalapu').html(data[0].valor);
                    $('#divapu' + ida).html(data[0].valor);
                    $('#divdetallem' + id).html(data[0].material);
                    $('#divdetallee' + id).html(data[0].equipo);
                    $('#divdetallema' + id).html(data[0].mano);
                } else if (data[0].respuesta == 2) {
                    error('Error BD.Verificar');
                } else if (data[0].respuesta == 3) {
                    error("<a role='button' title='No esta autorizado de modificar Rendimientos. Debe Crear un actividad nueva y asignarle los recursos con los rendimientos que requiera.' class='btn btn-block btn-primary' onclick=CRUDACTIVIDADES('CREARACTIVIDAD','" + ida + "')><i class='fa fa-plus'></i>Crear</a>");
                    //$('#divdetalle2'+id).html("");
                }
            }, "json");
    }
}

function guardarrendimientov2(id, v, ida) {
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnActividades.php', {
                opcion: "GUARDARDETALLEV",
                id: id,
                valor: v,
                ida: ida
            },
            function(data) {
                if (data[0].respuesta == 1) {
                    //$('#divapu'+ida).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallem'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallee'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    //$('#divdetallema'+id).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
                    $('#divapu' + ida).html(data[0].valor);
                    $('#divdetallem' + id).html(data[0].material);
                    $('#divdetallee' + id).html(data[0].equipo);
                    $('#divdetallema' + id).html(data[0].mano);
                } else {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function formato_numero(numero, decimales, separador_decimal, separador_miles) { // v2007-08-06
    numero = parseFloat(numero);
    if (isNaN(numero)) {
        return "";
    }
    if (decimales !== undefined) {
        // Redondeamos
        numero = numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero = numero.toString().replace(".", separador_decimal !== undefined ? separador_decimal : ",");

    if (separador_miles) {
        // Añadimos los separadores de miles
        var miles = new RegExp("(-?[0-9]+)([0-9]{3})");
        while (miles.test(numero)) {
            numero = numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}

function actualizar(id) {
    // Open this row
    $('#divchil' + id).html("");
    var table1 = "<div class='col-xs-1'><a class='btn btn-block btn-success' onclick='actualizar(" + id + ")' role='buttom'><i class='fa fa-refresh'></i></a></div><div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos" + id + "'>";
    table1 += "<thead><tr><th>CODIGO</th><th>DESCRIPCION</th><th>UND</th><th>REND</th><th>V/UNITARIO</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";


    $.post('funciones/fnActividades.php', {
            opcion: "LISTAINSUMOS",
            id: id,
            pre: '',
            gru: '',
            cap: ''
        },
        function(dato) {
            var APU = 0;
            for (var i = 0; i < dato.length; i++) {
                table1 += "<tr>" +
                    "<td data-title='Codigo'>" + dato[i].insu + "</td>" +
                    "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                    "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                    "<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput(" + dato[i].iddetalle + "," + dato[i].rendi + ")' id='divdetalle" + dato[i].iddetalle + "'>" + dato[i].rendi + "</div></td>" +
                    "<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv(" + dato[i].iddetalle + "," + dato[i].valor + ")' id='divdetallev" + dato[i].iddetalle + "'>$ " + dato[i].valor1 + "</div></td>" +
                    "<td data-title='Material'> <div id='divdetallem" + dato[i].iddetalle + "'>$" + dato[i].material + "</div></td>" +
                    "<td data-title='Equipo'><div id='divdetallee" + dato[i].iddetalle + "'>$" + dato[i].equipo + "</div></td>" +
                    "<td data-title='Mano de obra'> <div id='divdetallema" + dato[i].iddetalle + "'>$" + dato[i].mano + "</div></td></tr>";
                APU += Number(dato[i].total);
            }
            $('#divapu' + id).html(formato_numero(APU, 2, '.', ','));
            table1 += "</tbody></table></div>";
            $('#divchil' + id).html(table1);
            convertirdata(id);
        }, "json");
}

function actualizarcap(pre, gru) {

    $("#divchilg" + gru).html("");
    var table1 = "<table width='100%'   class='table table-condensed compact' id='tbcapitulosg" + gru + "' style='font-size:11px' >";
    table1 += "<thead><tr>" +
        "<th class='dt-head-center' style='width:20px' ></th>" +
        "<th class='dt-head-center' style='width:20px'></th>" +
        "<th class='dt-head-center' style='width:20px'' ></th>" +
        "<th class='dt-head-center'>CAP</th>" +
        "<th class='dt-head-center'>DESCRIPCION</th>" +
        "<th class='dt-head-center'>UN</th>" +
        "<th class='dt-head-center'>CANT</th>" +
        "<th class='dt-head-center'>VR.UNIT</th>" +
        "<th class='dt-head-center'>VR.TOTAL</th>" +
        "<th class='dt-head-center'></th>" +
        "</tr></thead><tbody>";


    $.post('funciones/fnPresupuesto.php', {
            opcion: "LISTACAPITULOS",
            pre: pre,
            gru: gru
        }, //CREAR FUNCION
        function(dato) {
            if (dato[0].res == "no") {

            } else {

                for (var i = 0; i < dato.length; i++) {
                    var cap = dato[i].capitulo;
                    var cre = dato[i].cre;

                    table1 += "<tr style='background-color:rgba(215,215,215,0.5)' id='cap" + dato[i].idd + "'>";

                    table1 += "<td class='details-control1' style='width:20px'></td>" +
                        "<td style='vertical-align:middle'><a role='button' class='btn btn-block btn-default btn-xs'  style='width:20px; height:20px; ' onClick=CRUDPRESUPUESTO('AGREGARACTIVIDAD'," + pre + ",'" + gru + "','" + cap + "','','')  ><i class='glyphicon glyphicon-plus' data-toggle='tooltip' title='Agregar Actividad'></i></a></td>" +
                        "<td style='vertical-align:middle'><a role='button' class='btn btn-block btn-danger btn-xs'  style='width:20px; height:20px; ' onClick=CRUDPRESUPUESTO('DELETEASIGNARCAPITULO'," + pre + ",'" + gru + "','" + cap + "',''," + dato[i].idd + ") ><i class='glyphicon glyphicon-trash' data-toggle='tooltip' title='Quitar Capitulo'></i></a></td>" +
                        "<td data-title='CODIGO' style='width:85px'><span id='codc" + dato[i].idd + "'>" + dato[i].codc + "</span></td>" +
                        "<td data-title='NOMBRE'>" + dato[i].nombre + "</td>" +
                        "<td data-title='UN'></td>" +
                        "<td data-title='CANT'></td>" +
                        "<td data-title=''></td>" +
                        "<td data-title='Valor Total($)'><div id='divtotcap" + dato[i].idd + "' clas='currency'>$" + dato[i].total + "</div></td>" +
                        "<td>" + dato[i].idd + "</td>" +
                        "<td>" + dato[i].capitulo + "</td>" +
                        "</tr>";

                }
            }
            table1 += "</tbody></table>";
            $("#divchilg" + gru).html(table1);
            setTimeout(convertirdata2(gru), 4000);
            setTimeout(INICIALIZARLISTAS(''), 1000);


        }, "json");
    //row.child(format(row.data()) ).show();


}

function actualizarcapactual(pre, gru) {

    $("#divchilg" + gru).html("");
    var table1 = "<table width='100%'   class='table table-condensed compact' id='tbcapitulos" + gru + "' style='font-size:11px' >";
    table1 += "<thead><tr>" +
        "<th class='dt-head-center' style='width:20px'></th>" +
        "<th class='dt-head-center'>CAP</th>" +

        "<th class='dt-head-center'>DESCRIPCIÓN</th>" +
        "<th class='dt-head-center'></th>" +
        "<th class='dt-head-center'></th>" +
        "<th class='dt-head-center'></th>" +
        "<th class='dt-head-center info'>INICIAL</th>" +
        "<th class='dt-head-center'></th>" +
        "<th class='dt-head-center'></th>" +
        "<th class='dt-head-center'></th>" +
        "<th class='dt-head-center'></th>" +
        "<th class='dt-head-center danger'>ACTUAL</th>" +
        "<th class='dt-head-center success'>CAMBIO.ALCANCE</th>" +
        "<th></th>" +
        "<th></th>" +
        "</tr></thead><tbody>";


    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "LISTACAPITULOS",
            pre: pre,
            gru: gru
        }, //CREAR FUNCION
        function(dato) {
            if (dato[0].res == "no") {

            } else {
                for (var i = 0; i < dato.length; i++) {
                    var cap = dato[i].capitulo;
                    var cre = dato[i].cre;
                    if (cre == 1) {
                        table1 += "<tr style='background-color:rgba(215,215,215,0.5)' class='trdelete' id='cap" + dato[i].idd + "'>";
                    } else {
                        table1 += "<tr style='background-color:rgba(215,215,215,0.5)' id='cap" + dato[i].idd + "'>";
                    }
                    table1 += "<td class='details-control1c' style='width:20px'></td>" +
                        "<td data-title='CODIGO' style='width:85px'><span id='codc" + dato[i].idd + "'>" + dato[i].codc + "</span></td>" +
                        "<td data-title='NOMBRE'>" + dato[i].nombre + "</td>" +
                        "<td data-title='' style='width:60px'></td>" +
                        "<td data-title='' style='width:60px'></td>" +
                        "<td data-title='' style='width:80px'></td>" +
                        "<td data-title='Valor Total($)' style='width:80px'><div id='divtotcap" + dato[i].idd + "' class='currency'>$" + dato[i].total + "</div></td>" +
                        "<td data-title='' style='width:60px'></td>" +
                        "<td data-title='' style='width:60px'></td>" +
                        "<td data-title='' style='width:60px'></td>" +
                        "<td data-title='' style='width:80px'></td>" +
                        "<td style='width:80px'><div id='divtotcapa" + dato[i].idd + "' class='currency'>$" + dato[i].totala + "</div></td>" +
                        "<td style='width:98px'><div id='divtotalcc" + dato[i].idd + "' class='currency'>$" + dato[i].totalalc + "</div></td>" +
                        "<td>" + dato[i].idd + "</td>" +
                        "<td data-title='CAP'>" + dato[i].capitulo + "</td>" +
                        "</tr>";

                }
            }
            table1 += "</tbody></table>";
            $("#divchilg" + gru).html(table1);
            setTimeout(convertircap(gru), 4000);
            setTimeout(INICIALIZARLISTAS(''), 1000);


        }, "json");
    //row.child(format(row.data()) ).show();       

}

function recalcularcapitulos(pre) {
    $.post("funciones/fnPresupuesto.php", {
            opcion: "CODIGOCAPITULOS",
            pre: pre
        },
        function(data) {
            if (data[0].res == "no") {} else {
                for (var i = 0; i < data.length; i++) {
                    var opc = data[i].opc;
                    if (opc == "cap") {
                        if ($('#codc' + data[i].idd).length) {
                            $('#codc' + data[i].idd).html(data[i].codc);
                        }
                    } else {
                        //console.log("Eventos");
                        if ($('#code' + data[i].idd).length) {
                            $('#code' + data[i].idd).html(data[i].codc);
                        }
                    }
                }
            }
        }, "json");
}

function actualizaract(pre, gru, cap) {
    // Open this row
    $("#divchila" + pre + "g" + gru + "c" + cap).html("");
    var table1 = "<table  class='table table-bordered table-condensed compact' id='tbactividadp" + pre + "g" + gru + "c" + cap + "' style='font-size:11px'>";
    table1 += "<thead><tr>" +
        "<th style='width:20px'></th>" +
        "<th class='dt-head-center' style='width:20px'></th>" +
        "<th class='dt-head-center' style='width:20px'></th>" +
        "<th class='dt-head-center' style='width:20px'></th>" +
        "<th class='dt-head-center' style='width:20px'>ITEM</th>" +
        "<th class='dt-head-center'>ACTIVIDAD</th>" +
        "<th class='dt-head-center' style='width:20px'>UN</th>" +
        "<th class='dt-head-center' width=20px>CANT</th>" +
        "<th class='dt-head-center' style='width:100px'>VR.UNIT</th>" +
        "<th class='dt-head-center' style='width:100px'>VR.TOTAL</th>" +
        "<th class='dt-head-center'>CODIGO</th>" +
        "<th class='dt-head-center'>SUBA</th>" +
        "</tr></thead><tbody>";


    $.post('funciones/fnPresupuesto.php', {
            opcion: "LISTAACTIVIDADES",
            cap: cap,
            pre: pre,
            gru: gru
        }, //CREAR FUNCION
        function(dato) {
            var res = dato[0].res;
            if (res == "no") {} else {
                for (var i = 0; i < dato.length; i++) {
                    var k = i + 1;
                    if (k < 10) {
                        k = "0" + k
                    }
                    var f = cap + "." + k;

                    var aiu = "Administración:" + dato[i].adm + "% Imprevisto:" + dato[i].imp + "% Utilidades:" + dato[i].uti + "% Iva:" + dato[i].iva + "%";

                    table1 += "<tr id='act" + dato[i].iddetalle + "'>" +
                        "<td>" + dato[i].iddetalle + "</td>" +
                        "<td class='details-control2'></td>" +
                        "<td style='vertical-align:middle'><a role='button' class='btn btn-block btn-danger btn-xs' style='width:20px; height:20px; ' onClick=CRUDPRESUPUESTO('DELETEASIGNAR'," + pre + "," + gru + "," + cap + ",''," + dato[i].iddetalle + ")  ><i class='glyphicon glyphicon-trash' data-toggle='tooltip' title='Quitar Actividad'></i></a></td>" +
                        "<td style='vertical-align:middle'><a role='button' data-toggle='modal' data-target='#myModal' class='btn btn-block btn-default btn-xs' onClick=CRUDPRESUPUESTO('DUPLICARACTIVIDAD'," + pre + "," + gru + "," + cap + "," + dato[i].idactividad + "," + dato[i].iddetalle + ")  style='width:20px; height:20px;display:none'><i class='glyphicon glyphicon-duplicate' data-toggle='tooltip' title='Duplicar Actividad'></i></a></td>" +
                        "<td data-title='ITEM' style='width:50px'>" + dato[i].items + "</td>" +
                        "<td data-title='ACTIVIDAD'>" + dato[i].actividad + /*"<span class='help-block' style='font-size:10px'>("+dato[i].ciudad+"-"+dato[i].tpp+")</span>*/ "</td>" +
                        "<td data-title='UN' style='width:80px'>" + dato[i].unidad + "</td>" +
                        "<td data-title='Cantidad' style='width:80px'>" + dato[i].cantidad + "</td>" +
                        "<td data-title='Valor Unitario($)' class='currency' title='" + dato[i].apu + "' style='width:100'>" + dato[i].apu + "</td>" +
                        "<td data-title='Valor Total($)' title='" + dato[i].total + "' style='width:80px'><div id='divtotact" + dato[i].iddetalle + "' class='currency'>" + dato[i].total + "</div></td>" +
                        "<td data-title='CODIGO'>" + dato[i].idactividad + "</td>" +
                        "<td data-title='CODIGO'>" + dato[i].suba + "</td>" +
                        "</tr>";

                }
            }
            table1 += "</tbody></table>";
            $("#divchila" + pre + "g" + gru + "c" + cap).html(table1);
            setTimeout(convertiract(pre, gru, cap), 2000);
            //setTimeout(autoTabIndex("#tbactividadp"+pre+"g"+gru+"c"+cap,11),1000);	
            setTimeout(INICIALIZARLISTAS(''), 1000);

        }, "json");
}

function actualizaractactual(pre, gru, cap)
{
    // Open this row
    //console.log("Actividades actuales" + gru);
    $("#divchila" + pre + "g" + gru + "c" + cap).html("");
    var table1 = "<table width='100%'  class='table table-bordered table-condensed compact' style='font-size:11px' id='tbactividadp" + pre + "g" + gru + "c" + cap + "'>" +
        "<thead><tr>" +
        "<th style='width:20px'></th>" +
        "<th style='width:50px'></th>" +
        //"<th style='width:20px'></th>" +
        //"<th style='width:20px'></th>" +
        "<th class='dt-head-center' >ITEM</th>" +
        "<th class='dt-head-center' style='width:200px'>DESCRIPCION</th>" +
        "<th class='dt-head-center'>UN</th>" +
        "<th class='info dt-head-center'>CANT</th>" +
        "<th class='info dt-head-center'>VR.UNIT</th>" +
        "<th class='info dt-head-center'>PPTO<br>INICIAL</th>" +
        "<th class='danger dt-head-center'>CANT<br>PROYECTADA</th>" +
        "<th class='danger dt-head-center'>CANT<br>EJECUTADA</th>" +
        "<th class='danger dt-head-center'>CANT<br>FALTANTE</th>" +
        "<th class='danger dt-head-center'>VLR.UNIT<br>ACTUAL</th>" +
        "<th class='danger dt-head-center'>PPTO<br>ACTUAL</th>" +
        "<th class='success dt-head-center'>CAMBIO<br>ALCANCE</th>" +
        "<th>SUBA</th>" +
        "</tr></thead><tbody>";


    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "LISTAACTIVIDADES",
            cap: cap,
            pre: pre,
            gru: gru
        }, //CREAR FUNCION
        function(dato) {
            var res = dato[0].res;
            if (res == "no") {} else
                {
                for (var i = 0; i < dato.length; i++) {
                    var numapu = dato[i].numapu;
                    var cre = dato[i].cre;
					var items = dato[i].items;
					var itemp = dato[i].itemp;
                    //console.log("grupo " + gru);
                           if (numapu > 0)
                           {
                               if(items=="EXT")
                               {
                                   var col = "style='color: rgba(255, 0, 0, 0.498039);font-weight:bold'";
                               }
                               else {
                                   var col = "";
                               }
                            }
                            else if (numapu <= 0 && itemp!="PRE") {
                                var col = "style='color: rgba(255, 0, 0, 0.498039)'";
                            }
							else if(itemp=="PRE")
							{
								var col = "style='background-color: rgba(16,146,55, 0.498039)'";								
							}

                            if (cre == 1 && itemp!="PRE") {
                                table1 += "<tr id='" + dato[i].iddetalle + "' " + col + " class='trdeletea'>";
                            }
							else
							if (cre == 1 && itemp=="PRE") {
                                table1 += "<tr id='" + dato[i].iddetalle + "' " + col + " class='trdeletee'>";
                            } 
							else {
                                table1 += "<tr id='" + dato[i].iddetalle + "' " + col + ">";
                            }
                            table1 += "<td>" + dato[i].iddetalle + "</td>";
							if(itemp!="PRE")
							{
								table1 += "<td class='details-control1' style='width:30px'></td>";
							}
							else
							{
							    table1 += "<td style='width:50px'></td>";
							}
							table1 +=
                                //"<td style='vertical-align:middle'><a class='btn btn-block btn-success btn-xs' onClick=CRUDPRESUPUESTOINICIAL('USARACTIVIDAD','" + dato[i].idactividad + "','" + cap + "','" + gru + "','" + cap + "') role='button'  data-toggle='modal' data-target='#myModal' title='Usar actividad' style='width:20px; height:20px'><i class='glyphicon glyphicon-share'><i/></a></td>" +
                        //"<td style='vertical-align:middle'><a class='btn btn-block btn-default btn-xs' onClick=CRUDPRESUPUESTOINICIAL('ASOCIARAPU','" + dato[i].idactividad + "','" + dato[i].iddetalle + "','" + gru + "','" + cap + "') role='button'  data-toggle='modal' data-target='#myModal' title='Asociar Apu' style='width:20px; height:20px'><i class='glyphicon glyphicon-plus'><i/></a></td>" +
                        "<td data-title='ITEM' style='width:45px'>" + dato[i].items + "</td>" +
                        "<td data-title='DESCRIPCION' style='width:280px'>" + dato[i].actividad + "</td>" +
                        "<td data-title='UN' style='width:62px'>" + dato[i].unidad + "</td>" +
                        "<td data-title='CANT.INICIAL' title='"+dato[i].cantidadt+"' style='width:60px'>" + dato[i].cantidad + "</td>" +
                        "<td data-title='VLR.UNIT.INICIAL' style='width:80px'><div id='divtotapu" + dato[i].iddetalle + "' class='currency'>" + dato[i].apu + "</div></td>" +
                        "<td data-title='PTO.INICIAL' style='width:80px' ><div id='divtoti" + dato[i].iddetalle + "' class='currency'>$" + dato[i].total + "</div></td>";
						if(itemp=="PRE")
						{
						    //console.log("grupo 2 " + gru);
						table1+=
						"<td data-title='CANT.PROYECTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempcp" + dato[i].iddetalle + "'  name=''  type='text' value='" + dato[i].cantidadp + "'  min='0'  onKeyPress='return validar_texto(event)'  onchange=guardarcantidadpen('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + gru + "','" + cap + "') disabled><br><span id='spcp" + dato[i].iddetalle + "'></span></td>" +
						
						"<td data-title='CANT.EJECUTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempce" + dato[i].iddetalle + "' name='' type='text' value='" + dato[i].cantidade + "'  min='0'  onKeyPress='return validar_texto(event)'  onchange=guardarcantidadeen('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + gru + "','" + cap + "') disabled><br><span id='spce" + dato[i].iddetalle + "'></span></td>";
						
						}
						else
						{
                            //console.log("grupo 2 " + gru);
						table1+=
						"<td data-title='CANT.PROYECTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempcp" + dato[i].iddetalle + "'  name=''  type='text' value='" + dato[i].cantidadp + "'  min='0'  onKeyPress='return validar_texto(event)'  onchange=guardarcantidadp('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + gru + "','" + cap + "') ><br><span id='spcp" + dato[i].iddetalle + "'></span></td>" +
						
						"<td data-title='CANT.EJECUTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempce" + dato[i].iddetalle + "' name='' type='text' value='" + dato[i].cantidade + "'  min='0'  onKeyPress='return validar_texto(event)'  onchange=guardarcantidade('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + gru + "','" + cap + "') ><br><span id='spce" + dato[i].iddetalle + "'></span></td>";
						}
						table1+=
                        "<td data-title='CANT.FALTANTE' style='width:60px'><div  style='cursor:pointer' id='divactf" + dato[i].iddetalle + "'>" + dato[i].cantidadf + "</div></td>" +
                        "<td data-title='VLR.UNIT.ACTUAL' style='width:80px'><div id='rempva" + dato[i].iddetalle + "' class='currency'>$" + dato[i].valoract + "</div></td>" +
                        "<td data-title='PPTO.ACTUAL' style='width:80px'><div id='divtotacta" + dato[i].iddetalle + "' class='currency'>$" + dato[i].totala + "</div></td>" +
                        "<td data-title='CAMBIOS EN EL ALCANCE' style='width:92px'><div id='divtotactc" + dato[i].iddetalle + "' class='currency'>" + dato[i].totalal + "</div></td>" +
                        "<td data-title='SUBANALISIS' style='width:92px'>" + dato[i].suba + "</td>" +
                        "</tr>";

                }
            }
            table1 += "</tbody></table>";
            $("#divchila" + pre + "g" + gru + "c" + cap).html(table1);
            setTimeout(convertirdata2(pre, gru, cap), 2000);
           // setTimeout(autoTabIndex("#tbactividadp" + pre + "g" + gru + "c" + cap, 11), 1000);
            setTimeout(INICIALIZARLISTAS(''), 1000);

        }, "json");
}

function actualizaractevento(pre, cap) {
    // Open this row
    $("#divchila" + pre + "c" + cap).html("");
    var table1 = "<table width='100%'  class='table table-bordered table-condensed compact' style='font-size:11px' id='tbactividadp" + pre + "c" + cap + "'>" +
        "<thead><tr>" +
        "<th style='width:20px'></th>" +
        "<th style='width:20px'></th>" +
        "<th class='dt-head-center' >ITEM</th>" +
        "<th class='dt-head-center' style='width:200px'>DESCRIPCION</th>" +
        "<th class='dt-head-center'>UN</th>" +
        "<th class='info dt-head-center'>CANT</th>" +
        "<th class='info dt-head-center'>VR.UNIT</th>" +
        "<th class='info dt-head-center'>PPTO<br>INICIAL</th>" +
        "<th class='danger dt-head-center'>CANT<br>PROYECTADA</th>" +
        "<th class='danger dt-head-center'>CANT<br>EJECUTADA</th>" +
        "<th class='danger dt-head-center'>CANT<br>FALTANTE</th>" +
        "<th class='danger dt-head-center'>VLR.UNIT<br>ACTUAL</th>" +
        "<th class='danger dt-head-center'>PPTO<br>ACTUAL</th>" +
        "<th class='success dt-head-center'>CAMBIO<br>ALCANCE</th>" +
        "<th>SUBA</th>" +
        "</tr></thead><tbody>";


    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "LISTAACTIVIDADESE",
            cap: cap,
            pre: pre
        }, //CREAR FUNCION
        function(dato) {
            var res = dato[0].res;
            if (res == "no") {} else {
                for (var i = 0; i < dato.length; i++) {
                    var numapu = dato[i].numapu;

                    if (numapu > 0) {
                        var col = "";
                    } else if (numapu <= 0) {
                        var col = "style='background-color: rgba(255, 0, 0, 0.498039)'";
                    }

                    table1 += "<tr id='" + dato[i].iddetalle + "' " + col + " class='trdeleteac' rel='" + dato[i].idactividad + "'>";
                    table1 += "<td>" + dato[i].iddetalle + "</td>" +
                        "<td class='details-control1'></td>" +
                        "<td data-title='ITEM' style='width:45px'>" + dato[i].items + "</td>" +
                        "<td data-title='DESCRIPCION'>" + dato[i].actividad + "</td>" +
                        "<td data-title='UN' style='width:62px'>" + dato[i].unidad + "</td>" +
                        "<td data-title='CANT.INICIAL' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='canti" + dato[i].iddetalle + "'  name=''  type='text' value='" + dato[i].cantidad + "'  min='0'  onKeyPress='return validar_texto(event)'  onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA','" + cap + "','" + dato[i].iddetalle + "','','')></td>" +
                        "<td data-title='VLR.UNIT.INICIAL' style='width:80px'><div id='divtotapue" + dato[i].iddetalle + "' class='currency'>" + dato[i].apu + "</div></td>" +
                        "<td data-title='PTO.INICIAL' style='width:80px' ><div id='divtotie" + dato[i].iddetalle + "' class='currency'>$" + dato[i].total + "</div></td>" +
                        "<td data-title='CANT.PROYECTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='cantp" + dato[i].iddetalle + "'  name=''  type='text' value='" + dato[i].cantidadp + "'  min='0'  onKeyPress='return validar_texto(event)'  onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA','" + cap + "','" + dato[i].iddetalle + "','','')></td>" +

                        "<td data-title='CANT.EJECUTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='cante" + dato[i].iddetalle + "' name='' type='text' value='" + dato[i].cantidade + "'  min='0'  onKeyPress='return validar_texto(event)'  onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA','" + cap + "','" + dato[i].iddetalle + "','','')></td>" +
                        "<td data-title='CANT.FALTANTE' style='width:60px'><div  style='cursor:pointer' id='divactfe" + dato[i].iddetalle + "'>" + dato[i].cantidadf + "</div></td>" +
                        "<td data-title='VLR.UNIT.ACTUAL' style='width:80px'><input disabled style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='vaae" + dato[i].iddetalle + "' name='' type='text' value='$" + dato[i].valoract + "'  min='0' onKeyPress='return validar_texto(event)' class='currency'></td>" +
                        "<td data-title='PPTO.ACTUAL' style='width:80px'><div id='divtotactae" + dato[i].iddetalle + "' class='currency'>$" + dato[i].totala + "</div></td>" +
                        "<td data-title='CAMBIOS EN EL ALCANCE' style='width:92px'><div id='divtotactce" + dato[i].iddetalle + "' class='currency'>" + dato[i].totalal + "</div></td>" +
                        "<td data-title='SUBANALISIS' style='width:92px'>" + dato[i].suba + "</td>" +
                        "</tr>";

                }
            }
            table1 += "</tbody></table>";
            $("#divchila" + pre + "c" + cap).html(table1);
            setTimeout(convertiracte(pre, cap), 2000);
            setTimeout(autoTabIndex("#tbactividadp" + pre + "c" + cap, 9), 1000);
            setTimeout(INICIALIZARLISTAS(''), 1000);

        }, "json");
}

function reordenaractividad(id1, id2, pre, gru, cap) {
    //alert("por aca");
    $.post('funciones/fnPresupuesto.php', {
            opcion: 'REORDENARACTIVIDAD',
            id1: id1,
            id2: id2,
            pre: pre,
            gru: gru,
            cap: cap
        },
        function(data) {
            //$("#result"+pre+"g"+gru+"c"+cap).html( 'Event result:<br>'+data );
            setTimeout(actualizaract(pre, gru, cap), 5000);
        })
}

function CRUDPRESUPUESTO(o, pre, gru, cap, act, idd) //v = CIUDAD, DEPARTAMENTO,PAIS
{
    if (o == "ENVIARAMAESTRA") {
        jConfirm('Realmente desee enviar la actividad seleccionada a la maestra.', 'Dialogo de Confirmación LG', function(r) {
            if (r == true) {
                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        idd: idd
                    },
                    function(data) {
                        var res = data[0].res;
                        if (res == "ok") {
                            ok(data[0].msn);
                        } else {
                            error(data[0].msn);
                        }
                    }, "json");
            } else {
                return false
            }
        });
    }
    if (o == "NUEVO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#msn').html('');

        $('#btnnuevo').hide();
        $('#titlevento').html("Registro Nuevo Presupuesto");
        $('#tabladatos').hide();
        $('#contenido').show();
        $('#contenido').html('');
        $('#divfiltrop').hide(1000);
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "EDITAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#msn').html('');
        $('#btnnuevo').hide();
        $('#titlevento').html("Edicion Presupuesto");
        $('#tabladatos').hide();
        $('#contenido').show();
        $('#contenido').html('');
        $('#divfiltrop').hide(1000);

        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    //fin nuevo inicio guardar
    else if (o == "GUARDARPREVIO") {
        var idp = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var seltipocontrato = $('#seltipocontrato').val();
        var selcliente = $('#selcliente').val();
        var selciudad = $('#selciudad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        var selestado = $('#selestado').val();
        var selcoordinador = $('#selcoordinador').val();
        var fecha = $('#fecha').val();
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                nombre: txtnombre,
                tipoproyecto: seltipoproyecto,
                descripcion: txtdescripcion,
                cliente: selcliente,
                ciudad: selciudad,
                estado: selestado,
                fecha: fecha,
                coordinador: selcoordinador,
                tipocontrato: seltipocontrato,
                idp: idp
            },
            function(data) {
                var res = data[0].res;
                var idpr = data[0].idp;
                if (res == "error1") {
                    error('Ya hay un presupuesto con el nombre,tipo de proyecto, ciudad y cliente diligenciados. Verificar');
                } else if (res == "error2") {
                    error('Surgió un error al guardar el presupuesto. Verificar');
                } else if (res == "ok") {
                    $('#idedicion').val(idpr);
                    $('#presupuestoseleccionado').html("/" + txtnombre).css('color', '#6E835D');
                }
            }, "json");
    } else if (o == "GUARDAR") {
        var idp = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var seltipocontrato = $('#seltipocontrato').val();
        var selcliente = $('#selcliente').val();
        var selciudad = $('#selciudad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        var selestado = $('#selestado').val();
        var selcoordinador = $('#selcoordinador').val();
        var fecha = $('#fecha').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar la nombre del proyecto. Verificar');
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto. Verificar');
        }
        /*else
		if(selestado=="" || selestado==null)
		{
			error('Seleccionar el estado de presupuesto. Verificar');
 		}*/
        else
        if (selcliente == "" || selcliente == null) {
            error('Seleccionar el cliente del presupuesto. Verificar');
        } else
        if (selcoordinador == "" || selcoordinador == null) {
            error('Seleccionar el coordinador del presupuesto. Verificar');
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad del presupuesto. Verificar');
        } else
            /*if(seltipocontrato=="" || seltipocontrato==null)
		{
			error('Seleccionar el tipo de contrato. Verificar');
 		}	
		else*/
            if (fecha == "" || fecha == null) {
                error('Seleccionar la fecha inicial del presupuesto. Verificar');
            }
        else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    nombre: txtnombre,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    cliente: selcliente,
                    ciudad: selciudad,
                    estado: selestado,
                    fecha: fecha,
                    coordinador: selcoordinador,
                    tipocontrato: seltipocontrato,
                    idp: idp
                },
                function(data) {
                    if (data != "" && data != "error1" && data != "error2") {
                        data = $.trim(data);
                        ok('Presupuesto registrado correctamente');
                        $('#presupuestoseleccionado').html("/" + txtnombre).css('color', '#6E835D');
                        setTimeout(CRUDPRESUPUESTO('EDITAR', data, '', '', '', ''), 1000);
                        setTimeout(cargarlistadostipo('CARGARLISTAPRESUPUESTO'), 1000);
                    } else if (data == "error1") {
                        error('Ya hay un presupuesto con el nombre,tipo de proyecto, ciudad y cliente diligenciados. Verificar');
                    } else if (data == "error2") {
                        error('Surgió un error al guardar el presupuesto. Verificar');
                    }
                });
        }
    }
    //fin guardar nuevo inicio guardar edicion
    else if (o == "GUARDAREDICION" || o == "GUARDAREDICION2") {
        var idedi = $('#idedicion').val();
        var txtnombre = $('#txtnombre').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var seltipocontrato = $('#seltipocontrato').val();
        var selcliente = $('#selcliente').val();
        var selciudad = $('#selciudad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        var selestado = $('#selestado').val();
        var fecha = $('#fecha').val();
        var adm = $('#administracion').val();
        var imp = $('#imprevisto').val();
        var uti = $('#utilidades').val();
        var iva = $('#iva').val();
        var selcoordinador = $('#selcoordinador').val();
        //alert(adm);

        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar el nombre del proyecto');
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto. Verificar');
        } else
            /*if(selestado=="" || selestado==null)
		{
			error('Seleccionar el estado del presupuesto. Verificar');
 		}
		else*/
            if ((selcliente == "" || selcliente == null) && o == "GUARDAREDICION") {
                error('Seleccionar el cliente del presupuesto. Verificar');
            }
        else
        if (selcoordinador == "" || selcoordinador == null) {
            error('Seleccionar el coordinador del presupuesto. Verificar');
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad del presupuesto. Verificar');
        }
        /*else
		if(seltipocontrato=="" || seltipocontrato==null)
		{
			error('Seleccionar el tipo de contrato. Verificar');
 		}*/
        else if (fecha == "" || fecha == null) {
            error('Seleccionar la fecha inicial del presupuesto. Verificar');
        } else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: 'GUARDAREDICION',
                    nombre: txtnombre,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    cliente: selcliente,
                    ciudad: selciudad,
                    estado: selestado,
                    fecha: fecha,
                    administracion: adm,
                    imprevisto: imp,
                    utilidades: uti,
                    iva: iva,
                    id: idedi,
                    coordinador: selcoordinador,
                    tipocontrato: seltipocontrato
                },
                function(data) {
                    if (data == 1) {
                        ok('Presupuesto actualizado correctamente');
                        //setTimeout(CRUDPRESUPUESTO('EDITAR',idedi,'','','',''),1000)
                        //setTimeout(cargarlistadostipo('CARGARLISTAPRESUPUESTO'),1000);
                    } else if (data == 2) {
                        error('Ya hay un presupuesto con el nombre,tipo de proyecto, ciudad y cliente diligenciados. Verificar');
                    } else {
                        error('Surgió un error al actualizar el presupuesto. Verificar');
                    }
                });
        }
    } else
    if (o == "DUPLICARACTIVIDAD") //REGISTRO DE NUEVA CIUDAD
    {
        $('.modal-dialog').removeClass('modal-lg');
        $('#myModal').modal('show');
        $('#msn2').html('');
        $('#btnguardar').attr('name', 'GUARDARDUPLICAR');
        $('#btnguardar').attr('value', 'Duplicar');
        $('#myModalLabel').html("DUPLICAR ACTIVIDAD");
        $('#contenido2').html("");
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap,
                act: act
            },
            function(data) {
                $('#contenido2').html(data);
            })
    } else
    if (o == "VERRECURSOS") //REGISTRO DE NUEVA CIUDAD
    {
        $('#divrecursos').css('display', 'block');
        $('#divsub').css('display', 'none');
    } else
    if (o == "VERSUB") //REGISTRO DE NUEVA CIUDAD
    {
        $('#divrecursos').css('display', 'none');
        $('#divsub').css('display', 'block').html('');
        $.post('funciones/fnPresupuesto.php', {
            opcion: o
        }, function(data) {
            $('#divsub').html(data);
        });

    } else if (o == "CANCELAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#msn').html('');
        $('#titlevento').html('');
        $('#presupuestoseleccionado').html("");
        $('#btnnuevo').show();
        $('#tabladatos').show();
        $('#contenido').hide();
        $('#divfiltrop').show(1000);
        cargarlistadostipo('CARGARLISTAPRESUPUESTO')
    } else if (o == "AGREGADOS") //REGISTRO DE NUEVA CIUDAD
    {
        $('#pendientes').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#pendientes').html(data);
                setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
            })
    } else if (o == "VERAGREGADOS") //REGISTRO DE NUEVA CIUDAD
    {
        $('#divagregados').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#divagregados').html(data);
            })
    } else
    if (o == "AGREGAR") {
        var ide = $('#idedicion').val();
        var gru = $('#selagregar').val();

        if (gru == "" || gru == null) {
            error('Seleccionar el grupo a agregar. Verificar');
        } else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    grupo: gru,
                    pre: ide
                },
                function(data) {
                    if (data == 1) {
                        ok('Grupo agregado correctamente');
                        setTimeout(CRUDPRESUPUESTO('CARGARLISTAGRUPOS', ide, '', '', '', '', ''), 1000);
                        setTimeout(CRUDPRESUPUESTO('VERAGREGADOS', ide, '', '', '', ''), 1000);
                    } else {
                        error('Error al agregar grupo. Verificar');
                    }
                });
        }
    } else
    if (o == "DELETE") {
        jConfirm('Realmente desea eliminar el grupo agregado a este presupuesto?', 'Diálogo Confirmación LG', function(r) {
            if (r == true) {
                var ide = $('#idedicion').val();
                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        gru: gru,
                        pre: ide,
                        ide: idd
                    },
                    function(data) {
                        if (data == 1) {
                            var table = $('#tbgruagregados').DataTable();
                            $('#divchilg' + gru).fadeOut('slow');
                            table.row('#gru' + idd).remove().draw(false);
                            ok('Grupo eliminado correctamente');
                            setTimeout(actualizartotal('', '', pre, gru, ''), 1000);
							setTimeout(ACTUALIZARENLAZADO(pre),3000);
                            setTimeout(CRUDPRESUPUESTO('CARGARLISTAGRUPOS', ide, '', '', '', '', ''), 2000);
                            setTimeout(recalcularcapitulos(ide), 3000);
                        } else {
                            error('Error al eliminar grupo. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "AGREGARCAPITULOS") {
        var val = cap;
        if (val != "") {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    pre: pre,
                    gru: gru,
                    cap: val,
                    capa: idd
                },
                function(data) {
                    data = $.trim(data);
                    if (data == 2) {
                        error('Surgió un error al agregar capitulo');
                    }
                });
        }
    } else if (o == "AGREGARACTIVIDAD" || o == "AGREGARACTIVIDADACTUAL" || o == "AGREGARACTIVIDADEVENTO") {
        /*if(idd=="eli"){}else
		{
			type = 'zoomin';
			$('.overlay-container').fadeIn(function() {
			window.setTimeout(function(){
			$('.window-container.'+type).addClass('window-container-visible');
			}, 100);
			});	
		}
	   */
        //$('#divactividades').html("");
        $('#divnueva').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                //$('#divactividades').html(data); 
                $('#divnueva').html(data);
            });
    } else if (o == "AGREGARCAP") {
        if (idd == "eli") {} else {
            type = 'zoomin';
            $('.overlay-container').fadeIn(function() {
                window.setTimeout(function() {
                    $('.window-container.' + type).addClass('window-container-visible');
                }, 100);
            });
        }

        $('#divactividades').html("");

        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                $('#divactividades').html(data);
            });
    } else if (o == "AGREGARACTIVIDAD2") {
        var ids = $('#select' + pre + 'g' + gru + 'c' + cap);
        var anc = ids.width();
        $('#select' + pre + 'g' + gru + 'c' + cap).tooltipster({
            contentAsHTML: true,
            interactive: true,
            trigger: 'click',
            theme: 'tooltipster-shadow',
            //animation: 'swing',
            side: 'bottom',
            multiple: true,
            maxWidth: anc,
            functionBefore: function(origin, continueTooltip) {
                continueTooltip();
                // when the request has finished loading, we will change the tooltip's content
                $.post('funciones/fnPresupuesto.php', {
                        opcion: 'AGREGARACTIVIDAD',
                        pre: pre,
                        gru: gru,
                        cap: cap
                    },
                    function(data) {
                        origin.tooltipster('content', '' + data);
                    });
            }
        });

    } else if (o == "CARGARLISTAGRUPOS") {

        var gru = $('#selagregar');
        gru.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                gru.empty();
                gru.append('<option value="">--seleccione--</option>');
				var res = data[0].res;
				if(res=="no"){}else
				{

					for (var i = 0; i < data.length; i++) {
						gru.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
					}
					
				}
				setTimeout(function(){ gru.selectpicker('refresh');},1000);
               

            }, "json");
    } else if (o == "ASIGNAR2") //ASIGNACION DE ACTIVIDADES A CAPITULO
    {
        $.post('funciones/fnPresupuesto.php', {
                opcion: 'ASIGNAR',
                pre: pre,
                gru: gru,
                cap: cap,
                act: act
            },
            function(data) {

                if (data == 1) {
                    $("select#select" + pre + "g" + gru + "c" + cap + " option[value='" + act + "']").remove();
                    setTimeout(actualizaract(pre, gru, cap), 1000);
                    setTimeout(INICIALIZARLISTAS('MODAL'), 1000);
                } else {
                    error('Error al asignar actividad. Verificar.');
                }
            })
    } else if (o == "ASIGNARDUPLICADO") {
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap,
                act: act,
                actant: idd
            },
            function(data) {

                if (data == 1) {
                    // $("select#select"+pre+"g"+gru+"c"+cap+" option[value='"+act+"']").remove();		  	  
                    //var table = $("#tbactividadesagregadasp"+pre+"g"+gru+"c"+cap).DataTable();
                    // table.row('#row_'+act).remove().draw( false );
                    //table.draw(false);			  
                    ok('Actividad asignada correctamente');
                    //setTimeout(CRUDPRESUPUESTO('AGREGARACTIVIDAD',pre,gru,cap,'',''),1000);
                    //if(o=="ASIGNAR")
                    //{
                    setTimeout(actualizaract(pre, gru, cap), 1000);
                    setTimeout(actualizartotal('', act, pre, gru, cap), 2000);
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                    /* else
			  if(o=="ASIGNARACTUAL")
			  {
				  setTimeout(actualizaractactual(pre,gru,cap),1000);
			  }*/
                } else if (data == 3) {
                    error('Actividad Codigo: ' + act + ' ya esta asignada. Verificar');
                } else {
                    error('Error al asignar actividad. Verificar');
                }
            });

    } else if (o == "ASIGNARACTUALEVENTO") {

        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                cap: cap,
                act: act
            },
            function(data) {

                if (data == 1) {
                    // $("select#select"+pre+"g"+gru+"c"+cap+" option[value='"+act+"']").remove();		  	  
                    var table = $("#tbactividadesagregadasp" + pre + "c" + cap).DataTable();
                    table.row('#row_' + act).remove().draw(false);
                    //table.draw(false);			  
                    ok('Actividad asignada correctamente');
                    //setTimeout(CRUDPRESUPUESTO('AGREGARACTIVIDAD',pre,gru,cap,'',''),1000);

                    setTimeout(actualizaractevento(pre, cap), 1000);

                } else if (data == 3) {
                    error('Actividad Codigo: ' + act + ' ya esta asignada. Verificar');
                } else {
                    error('Error al asignar actividad. Verificar');
                }
            });

    } else if (o == "ASIGNAR" || o == "ASIGNARACTUAL") //ASIGNACION DE ACTIVIDADES A CAPITULO
    {
        $.post('funciones/fnPresupuesto.php', {
                opcion: 'ASIGNAR',
                pre: pre,
                gru: gru,
                cap: cap,
                act: act,
                opcion2: o
            },
            function(data) {

                if (data == 1) {
                    // $("select#select"+pre+"g"+gru+"c"+cap+" option[value='"+act+"']").remove();		  	  
                    var table = $("#tbactividadesagregadasp" + pre + "g" + gru + "c" + cap).DataTable();
                    table.row('#row_' + act).remove().draw(false);
                    //table.draw(false);
                    $(".tooltipster-base").hide();
                    ok('Actividad asignada correctamente');
                    //setTimeout(CRUDPRESUPUESTO('AGREGARACTIVIDAD',pre,gru,cap,'',''),1000);
                    if (o == "ASIGNAR") {
                        setTimeout(actualizaract(pre, gru, cap), 1000);
                    } else
                    if (o == "ASIGNARACTUAL") {
                        setTimeout(actualizaractactual(pre, gru, cap), 1000);
                    }
                } else if (data == 3) {
                    error('Actividad Codigo: ' + act + ' ya esta asignada. Verificar');
                } else {
                    error('Error al asignar actividad. Verificar');
                }
            });
    } else if (o == "ASIGNAR3") //ASIGNACION DE CAPITULO A GRUPO
    {
        $.post('funciones/fnPresupuesto.php', {
                opcion: 'ASIGNARCAPITULO',
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                if (data == 1) {
                    $("select#select" + pre + "g" + gru + " option[value='" + cap + "']").remove();
                    setTimeout(actualizarcap(pre, gru), 1000);
                    setTimeout(INICIALIZARLISTAS('MODAL'), 1000);
                } else {
                    error('Error al asignar capitulos. Verificar');
                }
            });
    } else if (o == "ASIGNARCAPITULO" || o == "ASIGNARCAPITULOACTUAL") //ASIGNACION DE ACTIVIDADES A CAPITULO
    {
        $.post('funciones/fnPresupuesto.php', {
                opcion: 'ASIGNARCAPITULO',
                pre: pre,
                gru: gru,
                cap: cap,
                opcion2: o
            },
            function(data) {
                if (data == 1) {
                    ok('Capitulo(s) asignado(s) correctamente');
                    var table = $("#tbcapitulosagregadas" + gru).DataTable();
                    table.row('#row_' + cap).remove().draw(false);
                    $(".tooltipster-base").hide();
                    //setTimeout(CRUDPRESUPUESTO('AGREGARCAP',pre,gru,'','',''),1000);
                    if (o == "ASIGNARCAPITULO") {
                        setTimeout(actualizarcap(pre, gru), 1000);
                    } else
                    if (o == "ASIGNARCAPITULOACTUAL") {
                        setTimeout(actualizarcapactual(pre, gru), 1000);
                    }


                    setTimeout(recalcularcapitulos(pre), 2000);
                } else {
                    error('Error al asignar capitulos. Verificar');
                }
            });
    } else if (o == "DELETEASIGNAR" || o == "DELETEASIGNARACTUAL") //ASIGNACION DE ACTIVIDADES A CAPITULO
    {
        //preguntar antes de eliminar
        jConfirm('Realmente desea quitar la actividad asignada?', 'Diálogo Confirmación LG', function(r) {
            //var msn = confirm("Realmente desea quitar la actividad asignada")
            if (r == true) {
                $.post('funciones/fnPresupuesto.php', {
                        opcion: 'DELETEASIGNAR',
                        id: idd
                    },
                    function(data) {
                        var res = data[0].res;
                        var act = data[0].act;
                        if (res == 1) {
                            ok('Actividad eliminada correctamente');

                            var table = $("#tbactividadp" + pre + "g" + gru + "c" + cap).DataTable();
                            table.draw(false);
                            if (o == "DELETEASIGNAR") {
                                setTimeout(actualizartotal('', act, pre, gru, cap), 2000);
								setTimeout(ACTUALIZARENLAZADO(pre),3000);
                                setTimeout(actualizaract(pre, gru, cap), 3000);
                            } else {
                                setTimeout(actualizartotala(pre, gru, cap, act, '', '', ''), 2000);
                                setTimeout(actualizaractactual(pre, gru, cap), 3000);
								setTimeout(ACTUALIZARENLAZADO(pre),3000);
                            }
                        } else {
                            error('Error al eliminar actividad. Verificar');
                        }
                    }, "json");
            } else {
                return false;
            }
        });
    } else if (o == "DELETEASIGNARCAPITULO" || o == "DELETEASIGNARCAPITULOACTUAL") //ASIGNACION DE ACTIVIDADES A CAPITULO
    {
        //preguntar antes de eliminar
        //console.log("Pr aqui de nuevo 2")
        jConfirm('Realmente desea quitar la capitulo asignado?', 'Diálogo Confirmación LG', function(r) {
            //var msn = confirm("Realmente desea quitar la capitulo asignado")
            if (r == true) {
                $.post('funciones/fnPresupuesto.php', {
                        opcion: 'DELETEASIGNARCAPITULO',
                        id: idd,
                        pre: pre,
                        gru: gru,
                        cap: cap
                    },
                    function(data) {

                        if (data == 1) {
                            ok('Capitulo eliminado correctamente');
                            //setTimeout(CRUDPRESUPUESTO('AGREGARCAP',pre,gru,cap,'','eli'),1000);
                            if (o == "DELETEASIGNARCAPITULO") {
                                var table = $('#tbcapitulosg' + gru).DataTable();
                                table.row('#cap' + idd).remove().draw(false);
                                setTimeout(actualizartotal('', '', pre, gru, ''), 2000);
								setTimeout(ACTUALIZARENLAZADO(pre),3000);
                            } else {
                                var table = $('#tbcapitulos' + gru).DataTable();
                                table.row('#cap' + idd).remove().draw(false);
                                setTimeout(actualizartotala(pre, gru, cap, '', '', '', ''), 2000);
								setTimeout(ACTUALIZARENLAZADO(pre),3000);
                            }

                            setTimeout(recalcularcapitulos(pre), 2000);
                        } else {
                            error('Error al eliminar actividad. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "ASIGNARCAPITULOEVENTO" || o == "GUARDARCAPITULOEVENTO") {
        var nue = $('#selectcapitulo').val();
        if (o == "GUARDARCAPITULOEVENTO" && (nue == "" || nue == null)) {
            error("Diligenciar la descripcion del capitulo extra. Verificar");
            $('#selectcapitulo').focus();
        } else {
            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: o,
                    pre: pre,
                    cap: cap,
                    nue: nue
                },
                function(data) {
                    var res = data[0].res;
                    if (res == "ok") {
                        $('#selectcapitulo').val('');
                        var table = $('#tbeventos').DataTable();
                        table.draw(false);
                        var table = $('#tbcapituloseventos').DataTable();
                        table.row('#cap' + cap).remove().draw(false);
                        setTimeout(recalcularcapitulos(pre), 2000);
                    } else if (res == "error1") {
                        error("Ya hay un capitulo extra agregado con la misma descripcion. Verificar");
                    } else if (res == "error2") {
                        error("Surgio un error al agregar capitulo extra.Verificar");
                    }

                }, "json");
        }
    } else if (o == "DISTRIBUIR") {
        var idedi = $('#idedicion').val();
        var adm = $('#administracion').val();
        var imp = $('#imprevisto').val();
        var uti = $('#utilidades').val();
        var iva = $('#iva').val();
        jConfirm("Realmente desear distribuir el AIU a todas las actividades?", "Diálogo Confirmación LG", function(r) {
            if (r == true) {

                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        administracion: adm,
                        imprevisto: imp,
                        utilidades: uti,
                        iva: iva,
                        id: idedi
                    },
                    function(data) {
                        if (data == 1) {
                            ok('AIU distribuido correctamente');
                            setTimeout(CRUDPRESUPUESTO('EDITAR', idedi, '', '', '', ''), 1000);
                        } else if (data == 2) {
                            error('Surgió un error al distribuir AIU. Verificar');
                        } else if (data == 3) {
                            error('Surgió un actualizar sw_distribuido Presupuesto. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "DESHACERDISTRIBUIR") {
        var idedi = $('#idedicion').val();
        jConfirm("Realmente desear deshacer distribucion del AIU ?", "Diálogo Confirmación LG", function(r) {
            if (r == true) {

                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        id: idedi
                    },
                    function(data) {
                        if (data == 1) {
                            ok('Proceso Realizado correctamente');
                            setTimeout(CRUDPRESUPUESTO('EDITAR', idedi, '', '', '', ''), 1000);
                        } else if (data == 2) {
                            error('Surgió un error al deshacer distribución de  AIU. Verificar');
                        } else if (data == 3) {
                            error('Surgió un actualizar sw_distribuido Presupuesto. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "LLENARACTIVIDADAGREGAR") {
        var sel = $('#select' + pre + 'g' + gru + 'c' + cap);

        sel.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                var res = data[0].res;
                if (res == "no") {
                    sel.empty();
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 1000);
                } else {
                    sel.empty();
                    sel.append('<option value="">--seleccione--</option>');
                    for (var i = 0; i < data.length; i++) {
                        sel.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                    }
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 1000);
                }
            }, "json");

    } else if (o == "LLENARCAPITULOSAGREGAR") {
        var sel = $('#select' + pre + 'g' + gru);

        sel.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru
            },
            function(data) {
                var res = data[0].res;
                if (res == "no") {
                    sel.empty();
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 1000);
                } else {
                    sel.empty();
                    sel.append('<option value="">--seleccione--</option>');
                    for (var i = 0; i < data.length; i++) {
                        sel.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                    }
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 1000);
                }
            }, "json");

    } else if (o == "GUARDARDUPLICAR") {

        var pre = $('#idedicion').val();
        var act = $('#actedicion').val();
        var cap = $('#capedicion').val();
        var gru = $('#gruedicion').val();
        var txtdescripcion = $('#txtdescripcionA').val();
        var seltipoproyecto = $('#seltipoproyectoA').val();
        var selunidad = $('#selunidadA').val();
        var selciudad = $('#selciudadA').val();
        var radestado = $('input:radio[name=radestadoA]:checked').val();
        var radanalisis = 'No'; // $('input:radio[name=radanalisisA]:checked').val();
        var table = $('table.insumos').DataTable();
        var nums = table.rows().data().length;
        var err = 0;
        var err2 = 0;
        var cans = $('#cantidadsub').val();
        if (cans > 0) {
            //console.log("Por aca sub");
            $("table.subanalisis tbody tr").each(function(index) {
                var del = "-";
                var n = $('td', this).eq(0).text().split(del);
                var act = n[1];
                // console.log(act);
                var ren = $('#rends' + act).val();
                if (ren <= 0) {
                    err2++;
                }
            });
        } else {
            err2 = 0;
        }     

        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción de la actividad. Verificar');
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto de la actividad. Verificar');
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida de la actividad. Verificar');
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad de la actividad. Verificar');
        } else if (nums <= 0) {
            error('La actividad a duplicar no tiene recursos. Verificar');
        } else if (err2 > 0) {
            error('Hay Subanalisis con rendimientos menor o iguales a cero. Verificar');
        } else {

            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    actividad: act,
                    presupuesto: pre,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    unidad: selunidad,
                    ciudad: selciudad,
                    estado: radestado,
                    analisis: radanalisis,
                    grupo: gru,
                    capitulo: cap
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == 2) {
                        error('Ya hay una actividad con el mismo nombre diligenciado,ciudad y tipo proyecto seleccionados.Verificar');
                    } else if (res == 3) {
                        error('Surgió un error al crear la nueva actividad.Verificar');
                    } else if (res == 4) {
                        error('Hay Recursos con rendimientos menor o iguales a cero. Verificar');
                    } else if (res == 1) {
                        var idca = data[0].ida;
                        //insercion de insumos
                        /*$.post('funciones/fnPresupuesto.php', {
                                opcion: 'GUARDARINSUMOAGREGADO',
                                id: idca,
                                act: act,
                                pre: pre,
                                gru: gru,
                                cap: cap
                            },
                            function(data) {
                                if (data == 1) {} else {
                                    error("Error al agregar recursos a la actividad duplicada");
                                }
                            });*/


                        //insercion de subanalisis
                        if (cans > 0) {
                            $("table.subanalisis tbody tr").each(function(index) {
                                var del = "-";
                                var n = $('td', this).eq(0).text().split(del);
                                var acts = n[1];


                                var ren = $('#rends' + acts).val();
                                //console.log(val);
                                $.post('funciones/fnPresupuesto.php', {
                                        opcion: 'GUARDARSUBANALISISAGREGADO',
                                        id: idca,
                                        act: act,
                                        ren: ren,
                                        pre: pre,
                                        gru: gru,
                                        cap: cap,
                                        acts: acts
                                    },
                                    function(data) {
                                        if (data == 1) {
                                            //console.log("Guardo Sub-Analisis");
                                        } else {
                                            error("Error al agregar subanalisis al la actividad duplicada");
                                        }
                                    });

                            });
                        }
                        //fin insercion de subanalisis

                        jConfirm("Actividad Duplicada correctamente. Desea Reemplazar la actividad seleccionada por la nueva", "Diálogo de Confirmación LG", function(r) {
                            if (r == true) {
                                setTimeout(CRUDPRESUPUESTO('REEMPLAZARACTIVIDAD', pre, gru, cap, act, idca), 2000);
                            } else {
                                setTimeout(CRUDPRESUPUESTO('ASIGNARDUPLICADO', pre, gru, cap, idca, act), 2000);
                                // var table = $("#tbactividadesagregadasp"+pre+"g"+gru+"c"+cap).DataTable();
                                //table.draw(false);
                                $('.modal').modal('hide');
                            }
                        });
                    }
                }, "json")
        }

    } else if (o == "REEMPLAZARACTIVIDAD") {
        $.post('funciones/fnPresupuesto.php', {
            opcion: o,
            pre: pre,
            gru: gru,
            cap: cap,
            act: act,
            acta: idd
        }, function(data) {
            var res = data[0].res;
            if (res == 1) {
                ok("Actividad Reemplazada Correctamente");
                //var table = $('#tbactividadp'+pre+'g'+gru+'c'+cap).DataTable();
                //table.draw(false);
                setTimeout(actualizaract(pre, gru, cap), 1000);
                setTimeout(actualizartotal('', '', pre, gru, cap), 2000);
				setTimeout(ACTUALIZARENLAZADO(pre),3000);
                //setTimeout(CRUDPRESUPUESTO('LLENARACTIVIDADAGREGAR',pre,gru,cap,'',''),1000);
                $('.modal').modal('hide');
            } else if (res == 2) {
                error('Surgió un error al desasignar actividad asociada anteriormente.Verificar');

            } else if (res == 3) {
                error('Surgió un error al asociar actividad nueva.Verificar');
            } else {
                error('Surgió un error al reemplazar la actividad.Verificar');
            }
        }, "json");

    } else if (o == "NUEVOINSUMO") {
        $('#divagregarinsumo').css('display', 'none');
        $('#divnuevoinsumo').css('display', 'block');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: '',
                gru: '',
                cap: '',
                act: ''
            },
            function(data) {
                $('#divnuevoinsumo').html(data);
            });
    } else if (o == "CANCELARNUEVOINSUMO") {
        $('#divagregarinsumo').css('display', 'block');
        $('#divnuevoinsumo').css('display', 'none').html('');
    } else if (o == "GUARDARNUEVOINSUMO") {
        var ti = $('#tipoinsumo').val();
        var no = $('#nombreinsumo').val();
        var uni = $('#unidadinsumo').val();
        var valo = $('#valorinsumo').val();
        var pre = $('#idedicion').val();
        var gru = $('#gruedicion').val();
        var cap = $('#capedicion').val();
        var act = $('#actedicion').val();
        //var rend  = $('#rendinsumo').val();
        if (no == "") {
            error("Diligenciar el nombre del recurso. Verificar");
        } else if (ti == "") {
            error("Seleccionar el tipo de recurso. Verificar");
        } else if (uni == "") {
            error("Seleccionar la unidad del recurso. Verificar");
        } else if (valo <= 0) {
            error("El valor unitario del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }
        /*else if(rend<=0)
        {
        	error("El rendimiento del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }*/
        else {
            //var t = $('table.insumos').DataTable();
            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    ti: ti,
                    nom: no,
                    uni: uni,
                    valor: valo,
                    pre: pre,
                    gru: gru,
                    cap: cap,
                    act: act
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == "ok") {
                        $("#tipoinsumo>option[value='']").attr('selected', 'selected');
                        $('#nombreinsumo').val('');
                        $("#unidadinsumo>option[value='']").attr('selected', 'selected');
                        $('#valorinsumo').val('');
                        $('#rendinsumo').val('');
                        ok('Recurso creado y agregado correctamente');
                        setTimeout(CRUDPRESUPUESTO('LISTAINSUMOSDUPLICAR', '', '', '', '', ''), 1000);
                        setTimeout(INICIALIZARLISTAS(''), 2000);
                    } else if (res == "error1") {
                        error("Ya existe el recurso con nombre,unidad y valor unitario ingresados. Verificar");
                    } else if (res == "error2") {
                        error("Surgio un error al guardar recurso. Verificar");
                    }

                }, "json");
        }
    } else if (o == "NUEVOINSUMO2") {
        $('#divagregarinsumo2').css('display', 'none');
        $('#divnuevoinsumo2').css('display', 'block');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: '',
                gru: '',
                cap: '',
                act: ''
            },
            function(data) {
                $('#divnuevoinsumo2').html(data);
            });
    } else if (o == "CANCELARNUEVOINSUMO2") {
        $('#divagregarinsumo2').css('display', 'block');
        $('#divnuevoinsumo2').css('display', 'none').html('');
    } else if (o == "GUARDARNUEVOINSUMO2") {
        var ti = $('#tipoinsumo').val();
        var no = $('#nombreinsumo').val();
        var uni = $('#unidadinsumo').val();
        var valo = $('#valorinsumo').val();
        //var rend  = $('#rendinsumo').val();
        if (no == "") {
            error("Diligenciar el nombre del recurso. Verificar");
        } else if (ti == "") {
            error("Seleccionar el tipo de recurso. Verificar");
        } else if (uni == "") {
            error("Seleccionar la unidad del recurso. Verificar");
        } else if (valo <= 0) {
            error("El valor unitario del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }
        /*else if(rend<=0)
        {
        	error("El rendimiento del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }*/
        else {
            //var t = $('table.insumos').DataTable();
            $.post('funciones/fnPresupuesto.php', {
                    opcion: 'GUARDARNUEVOINSUMO',
                    ti: ti,
                    nom: no,
                    uni: uni,
                    valor: valo /*,rend:rend*/
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == "ok") {
                        $("#tipoinsumo>option[value='']").attr('selected', 'selected');
                        $('#nombreinsumo').val('');
                        $("#unidadinsumo>option[value='']").attr('selected', 'selected');
                        $('#valorinsumo').val('');
                        $('#rendinsumo').val('');
                        ok('Recurso creado y agregado correctamente');
                        var idn = data[0].ins
                        setTimeout(CRUDPRESUPUESTO('AGREGARPENDIENTEPRESUPUESTO', '', '', '', '', idn), 1000);
                    } else if (res == "error1") {
                        error("Ya existe el recurso con nombre,unidad y valor unitario ingresados. Verificar");
                    } else if (res == "error2") {
                        error("Surgio un error al guardar recurso. Verificar");
                    }

                }, "json");
        }
    } else if (o == "NUEVOINSUMO3" || o == "NUEVOINSUMO4") {
        //$('#divagregarinsumo2').css('display','none');
        var pre = $('#presupuesto').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var id = $('#idact').val();

        $('#divnuevorecurso').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap,
                act: id
            },
            function(data) {
                $('#divnuevorecurso').html(data);

            });
    } else if (o == "CANCELARNUEVOINSUMO3" || o == "CANCELARNUEVOINSUMO4") {
        if (o == "CANCELARNUEVOINSUMO3") {
            var op = 'AGREGARRECURSO';
        } else {
            var op = 'AGREGARRECURSO2';
        }
        var pre = $('#presupuesto').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var id = $('#idact').val();
        $('#divnuevorecurso').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

        $.post('funciones/fnPresupuesto.php', {
                opcion: op,
                pre: pre,
                gru: gru,
                cap: cap,
                act: id
            },
            function(data) {
                setTimeout($('#divnuevorecurso').html(data), 2000);
            });
    } else if (o == "GUARDARNUEVOINSUMO3" || o == "GUARDARNUEVOINSUMO4") {
        if (o == "GUARDARNUEVOINSUMO3") {
            var op = 'ASIGNARRECURSO';
        } else if (o == "GUARDARNUEVOINSUMO4") {
            var op = 'ASIGNARRECURSO';
        }
        var ti = $('#tipoinsumo').val();
        var no = $('#nombreinsumo').val();
        var uni = $('#unidadinsumo').val();
        var valo = $('#valorinsumo').val();
        var pre = $('#presupuesto').val();
        //var rend  = $('#rendinsumo').val();
        if (no == "") {
            error("Diligenciar el nombre del recurso. Verificar");
        } else if (ti == "") {
            error("Seleccionar el tipo de recurso. Verificar");
        } else if (uni == "") {
            error("Seleccionar la unidad del recurso. Verificar");
        } else if (valo <= 0) {
            error("El valor unitario del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }
        /*else if(rend<=0)
        {
        	error("El rendimiento del recurso no puede ser vacio o menor o igual a cero. Verificar");
        }*/
        else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: 'GUARDARNUEVOINSUMO',
                    ti: ti,
                    nom: no,
                    uni: uni,
                    valor: valo,
                    pre: pre /*,rend:rend*/
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == "ok") {
                        $("#tipoinsumo>option[value='']").attr('selected', 'selected');
                        $('#nombreinsumo').val('');
                        $("#unidadinsumo>option[value='']").attr('selected', 'selected');
                        $('#valorinsumo').val('');
                        $('#rendinsumo').val('');

                        ok('Recurso creado y agregado correctamente');
                        var idn = data[0].ins;

                        setTimeout(CRUDPRESUPUESTO(op, '', '', '', '', idn), 1000);
                    } else if (res == "error1") {
                        error("Ya existe el recurso con nombre,unidad y valor unitario ingresados. Verificar");
                    } else if (res == "error2") {
                        error("Surgio un error al guardar recurso. Verificar");
                    }

                }, "json");
        }
    } else if (o == "AGREGARINSUMO") {
        var pre = $('#idedicion').val();
        var gru = $('#gruedicion').val();
        var cap = $('#capedicion').val();
        var act = $('#actedicion').val();
        var ins = $('#selagregar2').val();
        var re = 0; // $('#rendimiento').val();
        if (ins == "" || ins == null) {
            error('Seleccionar el recurso a agregar. Verificar')
        } else
        /*	if(re<=0)
		{
			error('El rendimiento debe ser mayor a cero. Verificar');
		}
		else*/
        {
            var t = $('table.insumos').DataTable();
            $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                ins: ins,
                pre: pre,
                gru: gru,
                cap: cap,
                act: act
            }, function(data) {

                $("select#selagregar2 option[value='" + ins + "']").remove();
                CRUDPRESUPUESTO('LISTAINSUMOSDUPLICAR', '', '', '', '', '');
                setTimeout(INICIALIZARLISTAS(''), 1000);
            }, "json")
        }
    } else if (o == "ELIMINAR") {
        jConfirm("Realmente desear eliminar el Presupuesto seleccionado?", "Diálogo Confirmación LG", function(r) {
            if (r == true) {

                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        pre: pre
                    },
                    function(data) {
                        data = $.trim(data);
                        if (data == 1) {
                            ok('Presupuesto Eliminado Correctamente');
                            var table = $('#tbpresupuesto').DataTable();
                            table.row('#row_' + pre).remove().draw(false);
                            $("select#presupuesto option[value='" + pre + "']").remove();
                        } else {
                            error('Surgió un Error al eliminar Presupuesto');
                            return false;
                        }
                    });
            } else {
                return false;
            }
        });

    } else if (o == "ELIMINARPERMANENTE") {
        jConfirm("Realmente desear eliminar el Presupuesto de Forma Permanente?", "Diálogo Confirmación LG", function(r) {
            if (r == true) {

                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        pre: pre
                    },
                    function(data) {
                        data = $.trim(data);
                        if (data == 1) {
                            ok('Presupuesto Eliminado Correctamente');
                            var table = $('#tbpresupuesto').DataTable();
                            table.row('#row_' + pre).remove().draw(false);
                        } else {
                            error('Surgió un Error al eliminar Presupuesto');
                            return false;
                        }
                    });
            } else {
                return false;
            }
        });

    } else if (o == "VERSUBANALISIS") {
        //verificar si actividad tiene subana
        $('#divchilp' + pre + 'g' + gru + 'c' + cap + 'a' + act).html('');
        var table2 = "<table id='tbsubanalisisp" + pre + "g" + gru + "c" + cap + "a" + idd + "'  class='table table-condensed compact' width='100%' style='font-size:11px;'>" +
            "<thead>" +
            "<tr><th class='dt-head-center' style='width:20px'></th>" +
            "<th class='dt-head-center' style='width:30px'>CÓDIGO</th>" +
            "<th class='dt-head-center' style='width:300px'>NOMBRE</th>" +
            "<th class='dt-head-center' style='width:30px'>UND</th>" +
            "<th class='dt-head-center' style='width:30px'>REND</th>" +
            "<th class='dt-head-center' style='width:40px'>V/R APU</th>" +
            "<th class='dt-head-center' style='width:40px'>TOTAL</th>" +
            "<th class='dt-head-center' style='width:120px'>TIPO PROYECTO</th>" +
            "<th class='dt-head-center' style='width:60px'>CIUDAD</th>" +
            "<th class='dt-head-center' style='width:40px'>ESTADO</th>" +
            "<th class='dt-head-center' style='width:40px'>ANALISÍS</th>" +
            "</tr>" +
            "</thead><tfoot><tr><th></th><th></th><th></th><th></th><th> </th><th> </th><th></th><th></th><th></th><th></th><th></th></tr></tfoot></table>";
        $('#divchilp' + pre + 'g' + gru + 'c' + cap + 'a' + act).html(table2);
        convertirsub(pre, gru, cap, idd);
    } 
	else if (o == "VERAPU") //VERAPUPRESUPUESTO
    {
		
        $('#divchilp' + pre + 'g' + gru + 'c' + cap + 'a' + act).html('');
        var table2 = "<div id='no-more-tables'><table  class='table table-condensed compact' id='tbinsumosp" + pre + "g" + gru + "c" + cap + "a" + act + "'>";
        table2 += "<thead><tr>" +
            "<th class='dt-head-center'>CODIGO</th>" +
            "<th class='dt-head-center'>DESCRIPCION</th>" +
            "<th class='dt-head-center'>UND</th>" +
            "<th class='dt-head-center'>REND</th>" +
            "<th class='dt-head-center'>V/UNITARIO</th>" +
            "<th class='dt-head-center'>MATERIAL</th>" +
            "<th class='dt-head-center'>EQUIPO</th>" +
            "<th class='dt-head-center'>M.DE.O</th>" +
            "</tr></thead><tbody>";
        $.post('funciones/fnPresupuesto.php', {
                opcion: "LISTAINSUMOS",
                id: act,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(dato) {
                var res = dato[0].res;
                if (res == "no") {} else {
                    for (var i = 0; i < dato.length; i++) {
                        var cre = dato[i].cre;
                        if (cre == 1) {
                            table2 += "<tr class='delete1'>";
                        } else {
                            table2 += "<tr>";
                        }
                        table2 += "<td data-title='Insumo'>" + dato[i].insu + "</td>" +
                            "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                            "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                            "<td data-title='Rendimiento' class='dt-center'>" + dato[i].rendi + "</td>" +
                            "<td data-title='Valor Unitario' class='dt-right'> $" + dato[i].valor + "</td>" +
                            "<td data-title='Material' class='dt-right'> $" + dato[i].material + "</td>" +
                            "<td data-title='Equipo' class='dt-right'>$" + dato[i].equipo + "</td>" +
                            "<td data-title='Mano de obra' class='dt-right'>$" + dato[i].mano + "</td></tr>";
                    }
                }
                table2 += "</tbody></table></div>";
                $("#divchilp" + pre + "g" + gru + "c" + cap + "a" + act).html(table2);
                setTimeout(convertirlistainsumos(pre, gru, cap, act), 1000);
                setTimeout(INICIALIZARTOOLTIP(), 2000)
            }, "json");
    } else if(o=="VERAPUBK"){
		var inf = idd;
        $('#divchilp' + pre + 'g' + gru + 'c' + cap + 'a' + act).html('');
        var table2 = "<div id='no-more-tables'><table  class='table table-condensed compact' id='tbinsumosp" + pre + "g" + gru + "c" + cap + "a" + act + "'>";
        table2 += "<thead><tr>" +
            "<th class='dt-head-center'>CODIGO</th>" +
            "<th class='dt-head-center'>DESCRIPCION</th>" +
            "<th class='dt-head-center'>UND</th>" +
            "<th class='dt-head-center'>REND</th>" +
            "<th class='dt-head-center'>V/UNITARIO</th>" +
            "<th class='dt-head-center'>MATERIAL</th>" +
            "<th class='dt-head-center'>EQUIPO</th>" +
            "<th class='dt-head-center'>M.DE.O</th>" +
            "</tr></thead><tbody>";
        $.post('funciones/fnPresupuesto.php', {
                opcion: "LISTAINSUMOSBK",
                id: act,
                pre: pre,
                gru: gru,
                cap: cap,
				inf:inf
            },
            function(dato) {
                var res = dato[0].res;
                if (res == "no") {} else {
                    for (var i = 0; i < dato.length; i++) {
                        var cre = dato[i].cre;
                        if (cre == 1) {
                            table2 += "<tr class='delete1'>";
                        } else {
                            table2 += "<tr>";
                        }
                        table2 += "<td data-title='Insumo'>" + dato[i].insu + "</td>" +
                            "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                            "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                            "<td data-title='Rendimiento' class='dt-center'>" + dato[i].rendi + "</td>" +
                            "<td data-title='Valor Unitario' class='dt-right'> $" + dato[i].valor + "</td>" +
                            "<td data-title='Material' class='dt-right'> $" + dato[i].material + "</td>" +
                            "<td data-title='Equipo' class='dt-right'>$" + dato[i].equipo + "</td>" +
                            "<td data-title='Mano de obra' class='dt-right'>$" + dato[i].mano + "</td></tr>";
                    }
                }
                table2 += "</tbody></table></div>";
                $("#divchilp" + pre + "g" + gru + "c" + cap + "a" + act).html(table2);
                setTimeout(convertirlistainsumos(pre, gru, cap, act,inf), 1000);
                setTimeout(INICIALIZARTOOLTIP(), 2000)
            }, "json");
    
	} else if (o == "FILTRARACTIVIDAD" || o == "FILTRARACTIVIDADACTUAL") {
        $('#divfiltroa').html("");

        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                $('#divfiltroa').html(data);
            });
    } else
    if (o == "DUPLICARPRESUPUESTO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#myModal').modal('show');
        $('#btnguardar').attr('name', 'GUARDARAGREGADOSDUPLICAR');
        $('#btnguardar').attr('rel', pre);
        $('#myModalLabel').html("Duplicar Presupuesto");
        $('#contenido2').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#contenido2').html(data);
            })
    } else if (o == "AGREGADOSDUPLICAR") //REGISTRO DE NUEVA CIUDAD
    {
        $('#contenidoduplicar').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#contenidoduplicar').html(data);
                //setTimeout(INICIALIZARLISTAS('PRESUPUESTO'),2000);
            })
    } else if (o == "GUARDARAGREGADOSDUPLICAR") {

        var txtnombre = $('#txtnombre').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var selcliente = $('#selcliente').val();
        var selciudad = $('#selciudad').val();
        var txtdescripcion = $('#txtdescripcion').val();
        var selestado = 2;
        var selcoordinador = $('#selcoordinador').val();
        var fecha = $('#fecha').val();
        var adm = $('#administracion').val();
        var imp = $('#imprevisto').val();
        var uti = $('#utilidades').val();
        var iva = $('#iva').val();
        if (txtnombre == "" || txtnombre == null) {
            error('Diligenciar la nombre del proyecto. Verificar');
        } else
        if (selcoordinador == "" || selcoordinador == null) {
            error('Seleccionar el coordinador del presupuesto. Verificar');
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto. Verificar');
        } else
        if (selestado == "" || selestado == null) {
            error('Seleccionar el estado de presupuesto. Verificar');
        } else
        if (selcliente == "" || selcliente == null) {
            error('Seleccionar el cliente del presupuesto. Verificar');
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad del presupuesto. Verificar');
        } else if (fecha == "" || fecha == null) {
            error('Seleccionar la fecha inicial del presupuesto. Verificar');
        } else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    nombre: txtnombre,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    cliente: selcliente,
                    ciudad: selciudad,
                    estado: selestado,
                    fecha: fecha,
                    coordinador: selcoordinador,
                    pre: pre,
                    adm: adm,
                    imp: imp,
                    uti: uti,
                    iva: iva
                },
                function(data) {
                    if (data == "ok") {
                        data = $.trim(data);
                        ok('Presupuesto Duplicado correctamente<br>Verificar en listado de presupuesto');
                        $('#myModal').modal('hide');
                        var table = $('#tbpresupuesto').DataTable();
                        table.draw(false);
                        //setTimeout(CRUDPRESUPUESTO('EDITAR',data,'','','',''),1000);
                        //setTimeout(cargarlistadostipo('CARGARLISTAPRESUPUESTO'),1000);
                    } else if (data == "error1") {
                        error('Ya hay un presupuesto con el nombre,tipo de proyecto, ciudad y cliente diligenciados. Verificar');
                    } else if (data == "error2") {
                        error('Surgió un error al guardar el presupuesto. Verificar');
                    }
                });
        }
    } else if (o == "APLICAIVA") {
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                ck: idd
            },
            function(data) {
                data = $.trim(data);
                if (data == 1) {
                    actualizarcosto();
                } else {
                    error("Surgio un error al actualizar calculo de aplicacion de IVA. Verificar");
                }
            });
    } else if (o == "RESTAURARPRESUPUESTO") {
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                data = $.trim(data);
                if (data == 1) {
                    ok("Presupuesto Restaurado correctamente.");
                    var table = $('#tbpresupuesto').DataTable();
                    table.row('#row_' + pre).remove().draw(false);
                } else {
                    error("Surgio un error al restaura presupuesto. Verificar");
                }
            });
    } else
    if (o == "NUEVAACTIVIDAD" || o == "NUEVAACTIVIDADACTUAL" || o == "NUEVAACTIVIDADEVENTO") //REGISTRO DE NUEVA CIUDAD
    { //$('.modal-dialog').removeClass('modal-lg');

        // $('#btnguardar').attr('name','GUARDARNUEVAACTIVIDAD');
        // $('#btnguardar').attr('value','Guardar Cambios');
        // $('#myModalLabel').html("Registro Nueva Actividad");
        $('#divnueva').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                $('#divnueva').html(data);
            })
    } else if (o == "PENDIENTESACTIVIDAD") //REGISTRO DE NUEVA CIUDAD
    {
        var pre = $('#idedicion').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        $('#li1').addClass('active');
        $('#li2').removeClass('active');
        $('#pendientesactividad').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                $('#pendientesactividad').html(data);
            })
    } else if (o == "PENDIENTESSUBACTIVIDAD") //REGISTRO DE NUEVA CIUDAD
    {
        $('#li2').addClass('active');
        $('#li1').removeClass('active');
        $('#pendientesactividad').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o
            },
            function(data) {
                $('#pendientesactividad').html(data);
            })
    } else if (o == "AGREGARPENDIENTEPRESUPUESTO") {
        var pre = $('#idedicion').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var ins = idd; //$('#selagregar').val();
        var ren =  $('#rend'+idd).val();// $('#rendimiento').val();
        //var val = $('#valorunitario').val();
        if (ins == "" || ins == null) {
            error('Seleccionar el recurso a agregar. Verificar')
        }
        /*else if(ren<=0 || ren=="")
	   {
		   error('El rendimiento no puede ser menor o igual a cero o vacio. Verificar');
	   }*/
        else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    insumo: ins,
                    rendimiento: ren,
                    pre: pre,
                    gru: gru,
                    cap: cap
                },
                function(data) {
					var res = data[0].res;
					$('#totalapu').html(data[0].valor);
                    if (res == 1) {
                        var table = $('#tbinsumosporagregar').DataTable();
                        table.row('#rowi_'+ins).remove().draw(true);
                       // $("select#selagregar3 option[value='" + ins + "']").remove();
                        var table2 = $('#tbinsumospendientes').DataTable();
                        table2.draw(false);
                        //CRUDACTIVIDADES('PENDIENTES','');
                        ok('Recurso agregado correctamente. Verificar')
                    } else {
                        error('Error al agregar recurso. Verificar')
                    }
                },"json");
        }
    } else if (o == "DELETEPENDIENTEPRESUPUESTO") {
		
		var pre = $('#idedicion').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        /*jConfirm('Realmente desea quitar el recurso agregado?', 'Diálogo Confirmación LG', function(r) 
	    {
		//msn = confirm("Realmente desea quitar el recurso agregado");
			if(r==true)
			{*/
        var ide = idd;
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                id: ide,
				pre:pre,
				gru:gru,
				cap:cap
            },
            function(data) {
				var res = data[0].res;
				$('#totalapu').html(data[0].valor);
                if (res == 1) {
                    //CRUDACTIVIDADES('PENDIENTES','');
                    var table = $('#tbinsumospendientes').DataTable();
                    table.row('#row_insp' + ide).remove().draw(false);
                    //var table2 = $('#tbinsumosporagregar').DataTable();
                    //table2.draw(false);
                    ok('Recurso eliminado correctamente');
                    setTimeout(CRUDPRESUPUESTO('CARGARPORAGREGAR', '', '', '', '', ''), 1000);
                } else {
                    error('Error al eliminar recurso. Verificar');
                }
            },"json");
        /*	}
        	else
        	{
        	   return false;
        	}
        });	*/
    } else if (o == "AGREGARPENDIENTESUBPRESUPUESTO") {
        var act = act; //$('#selagregar').val();
        var pre = $('#idedicion').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var rend = $('#renda' + act).val();
        if (act == "" || act == null) {
            error('Seleccionar la actividad a agregar. Verificar')
        } else if (rend == "" || rend == null || rend <= 0) {
            error('El rendimiento no puede ser vacio,menor o igual a cero. Verificar')
        } else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: o,
                    suba: act,
                    rend: rend,
                    pre: pre,
                    gru: gru,
                    cap: cap
                },
                function(data) {
					var res = data[0].res;
					$('#totalapu').html(data[0].valor);
					
                    if (res == 1) {
                        var table = $('#tbactividadporagregar').DataTable();
                        table.row('#rowa_' + act).remove().draw(true);
                        var table2 = $('#tbactividadagregados').DataTable();
                        table2.draw(false);
                        //CRUDACTIVIDADES('PENDIENTES','');
                        ok('Actividad SubAnalisis agregada correctamente');
                    } else {
                        error('Error al agregar actividad. Verificar')
                    }
                },"json");
        }
    } else if (o == "DELETEPENDIENTESUBPRESUPUESTO") {
        /*jConfirm('Realmente desea quitar la actividad agregada?', 'Diálogo Confirmación LG', function(r) 
	    {
		//msn = confirm("Realmente desea quitar el recurso agregado");
			if(r==true)
			{*/
	       var pre = $('#idedicion').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var ide = idd;
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                id: ide,
				pre:pre,
				gru:gru,
				cap:cap
            },
            function(data) {
				var res = data[0].res;
					$('#totalapu').html(data[0].valor);
                if (res == 1) {
                    //CRUDACTIVIDADES('PENDIENTES','');
                    var table = $('#tbactividadagregados').DataTable();
                    table.row('#row_actp' + ide).remove().draw(false);
                    var table2 = $('#tbactividadporagregar').DataTable();
                    table2.draw(false);
                    ok('Actividad elimina correctamente')

                } else {
                    error('Error al eliminar actividad. Verificar');

                }
            },"json");
        /*}
			else
			{
			   return false;
			}
		});
		*/
    } else if (o == "GUARDARNUEVAACTIVIDAD" || o == "GUARDARNUEVAACTIVIDADACTUAL" | o == "GUARDARNUEVAACTIVIDADEVENTO") {
        var txtdescripcion = $('#txtdescripcion2').val();
        var seltipoproyecto = $('#seltipoproyecto2').val();
        var selunidad = $('#selunidad').val();
        var selciudad = $('#selciudad2').val();

        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción de la actividad. Verificar');
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto de la actividad. Verificar');
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida de la actividad. Verificar');
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad de la actividad. Verificar');
        } else {
            $.post('funciones/fnPresupuesto.php', {
                    opcion: 'GUARDARNUEVAACTIVIDAD',
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    unidad: selunidad,
                    ciudad: selciudad,
                    pre: pre,
                    gru: gru,
                    cap: cap,
                    opcion2: o
                },
                function(data) {
                    if (data[0].res == 1) {
                        /*var table = $('#tbinsumospendientes').DataTable();
                        table.draw(false);
                        var table2 = $('#tbinsumosporagregar').DataTable();
                        table2.draw(false);
                        var table3 = $('#tbactividadporagregar').DataTable();
                        table3.draw(false);
                        var table4 = $('#tbactividadagregados').DataTable();
                        table4.draw(false);
					
                         $('#txtdescripcion2').val('');
                        $('#seltipoproyecto2>option[value=""]').attr('selected',true);
                        $('#selunidad>option[value=""]').attr('selected',true);
                        $('#selciudad2>option[value=""]').attr('selected',true);*/


                        ok('Actividad registrada y agregada correctamente al presupuesto');
                        if (o == "GUARDARNUEVAACTIVIDAD") {
                            $('.actagregar').tooltipster('hide');
                            setTimeout(actualizaract(pre, gru, cap), 1000);
                        } else if (o == "GUARDARNUEVAACTIVIDADACTUAL") {
                            $('.actagregar').tooltipster('hide');
                            setTimeout(actualizaractactual(pre, gru, cap), 1000);
                        } else if (o == "GUARDARNUEVAACTIVIDADEVENTO") {
                            $("#select" + pre + "c" + cap).tooltipster('hide');
                            setTimeout(actualizaractevento(pre, cap), 1000);
                        }
                    } else if (data[0].res == 2) {
                        error('Ya hay una actividad con el mismo nombre diligenciado,ciudad y tipo proyecto seleccionados.Verificar');
                    } else if (data[0].res == 3) {
                        error('Surgió un error al guardar la actividad. Verificar');
                    } else if (data[0].res == 4) {
                        error('No ha asignado ningun APU a la actividad. Verificar');
                    } else if (data[0].res == 5) {
                        error('Hay recursos con rendimientos menor o igual a cero. Verificar');
                    }
                }, "json");
        }
    }
    //2016 - 09  - Nuevos Retroceso
    else if (o == "CARGARPORAGREGAR") {
        var pre = $('#idedicion').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var pen = $('#selagregar3');
        pen.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                pen.empty();
                pen.append('<option value="">--seleccione--</option>');

                for (var i = 0; i < data.length; i++) {
                    pen.append('<option value="' + data[i].ins + '">' + data[i].nom + ' - ' + data[i].cod + ' - (' + data[i].val + ')</option>');
                }
                setTimeout(selectpick(),2000);
            }, "json");
    } else if (o == "EDITARPENDIENTEPRESUPUESTO") {
        var pre = $('#idedicion').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var rend = $('#detp' + idd).val();
        var v = $('#detv' + idd).val();
        var v1 = v.replace("$", "");
        var v2 = v1.replace(",", "").replace(",", "");
        v2 = v2.replace(",", "").replace(",", "");
        v = v2;
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                id: idd,
                rend: rend,
                val: v,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(data) {
                var res = data[0].res;
                if (res == 1) {
                    $('#totalapu').html(data[0].valor);
                } else {
                    error("Surgio un error al editar recurso. Verificar");
                }
            }, "json");
    } else if (o == "ASIGNARRECURSO" || o == "ASIGNARRECURSO2") {
        var pre = $('#presupuesto').val();
        var gru = $('#idgru').val();
        var cap = $('#idca').val();
        var act = $('#idact').val();
        var acti = $('#idacta').val();
        var ins = idd;
        var ren = $('#rend' + ins).val();
        var er = 0;
        if ((ren == "" || ren == null || ren == 0) && o == "ASIGNARRECURSO2") {
            error("Diligenciar el rendimiento del recurso a agregar. Verificar");
            $('#rend' + ins).focus();
            er++;
        } else
        if ((ren == "" || ren == null) && o == "ASIGNARRECURSO") {
            //error("Diligenciar el rendimiento del recurso a agregar. Verificar");
            ren = 0;
        }
        if (er <= 0) {

            $.post('funciones/fnPresupuesto.php', {
                    opcion: 'ASIGNARRECURSO',
                    pre: pre,
                    gru: gru,
                    cap: cap,
                    act: act,
                    ins: ins,
                    ren: ren
                },
                function(data) {
                    var res = data[0].res;
                    var nomi = data[0].nomi;
                    var uni = data[0].uni;
                    var vali = data[0].vali;
                    var material = data[0].material;
                    var equipo = data[0].equipo;
                    var mano = data[0].mano;
                    var acti = data[0].acti;

                    if (res == "ok") {
                        var table = $('#tbrecursosagregadosp' + pre + 'g' + gru + 'c' + cap + 'a' + act).DataTable();
                        table.row('#rowi_' + ins).remove().draw(false);
                        //$(".tooltipster-base").hide();
                        if (data[0].idc == 0) {} else {
                            $("<tr id='rowr_" + pre + "_" + gru + "_" + cap + "_" + acti + "_" + data[0].idc + " '  class='deleterecurso'><td>" + ins + "</td><td>" + nomi + "</td><td class='dt-center'>" + uni + "</td><td><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' title='" + ren + "' id='remren" + data[0].idc + "' name='remren_" + data[0].idc + "_" + pre + "_" + gru + "_" + cap + "_" + acti + "_" + ins + "'  type='text' value='" + ren + "'  min='0' onchange=guardarcantidadrenact('" + data[0].idc + "','" + pre + "','" + gru + "','" + cap + "',this.value,'" + acti + "','" + ins + "') onKeyPress='return validar_texto(event)' /></td><td><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remval" + data[0].idc + "' name='remval_" + data[0].idc + "_" + pre + "_" + gru + "_" + cap + "_" + acti + "_" + ins + "'  type='text' value='" + vali + "' min=''  onKeyPress='return validar_texto(event)' onchange=guardarvaloract('" + data[0].idc + "','" + pre + "','" + gru + "','" + cap + "',this.value,'" + acti + "','" + ins + "') class='currency'/></td><td style='text-align:right'><div id='material" + data[0].idc + "' class='currency'>" + material + "</div></td><td style='text-align:right'><div id='equipo" + data[0].idc + "' class='currency'>" + equipo + "</div></td><td style='text-align:right'><div id='mano" + data[0].idc + "' class='currency'>" + mano + "</div></td></tr>").insertBefore("#filaact" + act);
                        }
                        //$('#tbApu').draw(false);
                        //ok('Recurso Asignado') ;
                        if (o == "ASIGNARRECURSO") {
                            setTimeout(CRUDPRESUPUESTOINICIAL('VERAPU', act, '', gru, cap), 1000);
                        }
                        setTimeout(actualizartotala(pre, gru, cap, acti, act, act, ''), 2000);
						setTimeout(ACTUALIZARENLAZADO(pre),3000);
                    }
                }, "json");
        }
    } else if (o == "DELETERECURSOACT") {
        jConfirm('Realmente desea quitar el recurso asignado?', 'Diálogo Confirmación LG', function(r) {
            if (r == true) {
                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        pre: pre,
                        gru: gru,
                        cap: cap,
                        act: act,
                        idd: idd
                    },
                    function(data) {
                        var res = data[0].res;

                        if (res == "ok") {
                            var table = $('#tbinsumos' + act + 'p' + pre + 'g' + gru + 'c' + cap).DataTable();
                            table.row('#' + idd).remove().draw(false);
                            setTimeout(actualizartotala(pre, gru, cap, '', act, act, ''), 2000);
							setTimeout(ACTUALIZARENLAZADO(pre),3000);
                        } else {
                            error("Surgio un error al eliminar recurso. Verificar");
                        }
                    }, "json");
            } else {
                return false;
            }
        });

    } else if (o == "DELETERECURSOACTAPU") {
        jConfirm('Realmente desea quitar el recurso asignado?', 'Diálogo Confirmación LG', function(r) {
            if (r == true) {
                $.post('funciones/fnPresupuestoInicial.php', {
                        opcion: o,
                        pre: pre,
                        gru: gru,
                        cap: cap,
                        act: act,
                        idd: idd
                    },
                    function(data) {
                        var res = data[0].res;
                        var idda = data[0].idda;
                        if (res == "ok") {
                            $('#rowr_' + pre + '_' + gru + '_' + cap + '_' + act + '_' + idd).remove();
                            setTimeout(actualizartotala(pre, gru, cap, act, idda, idda, ''), 2000);
							setTimeout(ACTUALIZARENLAZADO(pre),3000);
                        } else {
                            error("Surgio un error al eliminar recurso. Verificar");
                        }
                    }, "json");
            } else {
                return false;
            }
        });
    } else if (o == "ACTUALIZARVALORES") {
        var pre = $('#idedicion').val();
        jConfirm('Realmente desea actualizar los valores de todo el presupuesto con los valores de hoy?', 'Diálogo Confirmación LG', function(r) {
            if (r == true) {
                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        pre: pre
                    },
                    function(data) {
                        var res = data[0].res;
                        var act = data[0].actualizados;
                        if (res == "ok") {
                            ok("Valores Actualizados Correctamente");
                            setTimeout(CRUDPRESUPUESTO('EDITAR', pre, '', '', '', ''), 1000);
                        } else if (res == "error1") {
                            error("Surgio un error al buscar valores a actualizar");
                        } else if (res == "error2") {
                            error("No se actualizaron los valores");
                        }

                    }, "json");

            } else {
                return false;
            }
        });
    } else if (o == "GENERARINFORME") {
        var pre = $('#presupuesto').val();
        jConfirm('Realmente desea generar informe de este presupuesto?', 'Diálogo Confirmación LG', function(r) {

            if (r == true) {
                notificacion("Generando informe. Espera un momento...");
                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        pre: pre
                    },
                    function(data) {
                        var res = data[0].res;
                        if (res == "ok") {
                            $('#docproyecto').val(data[0].numeroinforme);
                            ok("Informe Generado correctamente")
                            window.open('modulos/presupuesto/informeexportar.php?edi=' + pre);
                        } else {
                            error("Surgió un error al generar informe. Verificar");
                        }
                    }, "json");
            } else {
                return false;
            }
        });
    } 
	else if(o=="RESTAURARINFORME")
	{
        var presupuesto = $('#presupuesto').val();
		var inf = pre; 
        jConfirm('Realmente desea restaurar el presupuesto a la fecha seleccionada?', 'Diálogo Confirmación LG', function(r) {
            if (r == true) {
                $.post('funciones/fnPresupuesto.php', {
                        opcion: o,
                        pre: presupuesto,
						inf:inf
                    },
                    function(data) {
                        var res = data[0].res;
                        if (res == "ok") {
                            $('#docproyecto').val(data[0].numeroinforme);
							$('#tbpresupuesto').DataTable().draw();
                            ok("Presupuesto Restaurado Correctamente");
							setTimeout(CRUDPRESUPUESTOINICIAL('INFORMES',presupuesto,'','',''),1000);
							setTimeout(ACTUALIZARENLAZADO(pre),3000);
                           // window.open('modulos/presupuesto/presupuestoinfexc.php');
                        } else {
                            error("Surgió un error al restaurar presupuesto. Verificar");
                        }
                    }, "json");
            } else {
                return false;
            }
        });
    
	}
	else if (o == "CARGARLISTARESTAURAR") {
        $('#myModal').modal('show');
        $('#myModalLabel').html("Informes Generados");
        var pre = $('#presupuesto').val();
        $('#btnguardar').css('display', 'none');
        $('#contenido2').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#contenido2').html(data);
                //setTimeout(INICIALIZARLISTAS('PRESUPUESTO'),2000);
            })
    } else if (o == "DELETEINSUMODUPLICAR") {
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                idd: idd
            },
            function(data) {
                var res = data[0].res;
                if (res == 1) {
                    var table = $('table.insumos').DataTable();
                    table.row('#row_ins' + idd).remove().draw();
                    CRUDPRESUPUESTO('TOTALAPUDUPLICARE', '', '', '', '', '');
                } else {
                    error("Surgio un error al eliminar recurso seleccionado. Verificar");
                }
            }, "json");
    } else if (o == "LISTAINSUMOSDUPLICAR") {
        var pre = $('#idedicion').val();
        var act = $('#actedicion').val();
        var cap = $('#capedicion').val();
        var gru = $('#gruedicion').val();
        $('#divlistainsumos').html('');
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap,
                act: act
            },
            function(data) {
                $('#divlistainsumos').html(data);
            });
    } else if (o == "ACTUALIZARINSUMODUPLICAR") {
        var rend = $('#rend' + idd).val();
        var v = $('#val' + idd).val();
        var v1 = v.replace("$", "");
        var v2 = v1.replace(",", "").replace(",", "");
        v2 = v2.replace(",", "").replace(",", "");
        v = v2;
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                rend: rend,
                valor: v,
                idd: idd
            },
            function(data) {
                var res = data[0].res;
                if (res == 1) {
                    //funcion de actualizar total apu
                    CRUDPRESUPUESTO('TOTALAPUDUPLICARE', '', '', '', '', '');


                } else {
                    error('Surgio un error al modificar recurso. Verificar');
                }
            }, "json");
    } else if (o == "TOTALAPUDUPLICARE") {
        var pre = $('#idedicion').val();
        var act = $('#actedicion').val();
        var cap = $('#capedicion').val();
        var gru = $('#gruedicion').val();
        $.post('funciones/fnPresupuesto.php', {
                opcion: o,
                pre: pre,
                gru: gru,
                cap: cap,
                act: act
            },
            function(data) {
                $('#totalapu').html(data[0].totalapu);
                setTimeout("$('.currency').formatCurrency({colorize: true})", 1000);
            }, "json");
    }
	else if(o=="ENLAZAR")
	{
		var presupuesto = $('#presupuesto').val();
		var preenlazado = pre;
		var gruenlazar = $('#gruenlazar').val();
		var capenlazar = $('#capenlazar').val();
		$.post('funciones/fnPresupuestoInicial.php',{opcion:o,presupuesto:presupuesto,preenlazado:preenlazado,gruenlazar:gruenlazar,capenlazar:capenlazar}, function(data)
		{
			var res = data[0].res;
			if(res=="ok")
			{	  
			      var table = $('#tbpresupuestoenlazar').DataTable();
				  table.row('#rowen_'+preenlazado).remove().draw();
				  
			      setTimeout(actualizartotala(presupuesto, gruenlazar, capenlazar, '', '', '', ''), 2000);
			      setTimeout(actualizaractactual(presupuesto, gruenlazar, capenlazar), 1000); 
                           
			}
			else 
			{
				error("Surgio error al enlazar presupuesto seleccionado. Verficar");
			}
	      },"json");
	}
         else if(o=="DELETEENLAZAR")
		 {
			 
        //preguntar antes de eliminar
        jConfirm('Realmente desea quitar el presupuesto enlazado?', 'Diálogo Confirmación LG', function(r) {
            //var msn = confirm("Realmente desea quitar la actividad asignada")
            if (r == true) {
                $.post('funciones/fnPresupuestoInicial.php', {
                        opcion: o,
                        ide: idd
                    },
                    function(data) {
                        var res = data[0].res;
                        if (res == 1) {
                            ok('Presupuesto desenlado correctamente');
                            //var table = $("#tbactividadp" + pre + "g" + gru + "c" + cap).DataTable();
                           // table.draw(false);                          
                            setTimeout(actualizartotala(pre, gru, cap, '', '', '', ''), 2000);
                            setTimeout(actualizaractactual(pre, gru, cap), 3000);
							setTimeout(ACTUALIZARENLAZADO(pre),3000);
                           
                        } else {
                            error('Error al desenlazar presupuesto. Verificar');
                        }
                    }, "json");
            } else {
                return false;
            }
        });
    
    }
	else if(o=="ELIMINARINFORME")
	{
		var inf = idd;
		var pre = $('#presupuesto').val()
		jConfirm('Realmente desea eliminar el informe seleccionado?', 'Diálogo Confirmación LG', function(r) {
            // var conf = confirm("Realmente desea eliminar el usuario seleccionado");
            if (r == true) {
				  $.post('funciones/fnPresupuesto.php',{opcion:o,inf:inf,pre:pre },
				  function(data)
				  {
					   var res = data[0].res;
					   if(res=="ok")
					   {
						   var table = $('#tbpresupuesto').DataTable();
						   table.row('#rowin_'+inf).remove().draw();
						  ok("Informe seleccionado eliminado correctamente");
					   }
					   else
					   {
						  error("Surgieron errores al eliminar el informe seleccionado");
					   }
				  },"json");
						  
			}else{
				return false;
			}
		});
	}
}

function actualizartotal(idact, act, pre, gru, cap) {

    $.post('funciones/fnPresupuesto.php', {
            opcion: 'CALCULOTOTALES',
            idpresupuesto: pre,
            idcapitulo: cap,
            idgrupo: gru,
            idactividad: idact,
            act: act
        },
        function(data) {
            var idcap = data[0].idca;
            //$('#divtotcap'+idcap).html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />")
            $('#totcos').html(data[0].totalneto);
            $('#totpre').html(data[0].totpre);
            $('#totadm').html(data[0].totadm);
            $('#totimp').html(data[0].totimp);
            $('#totuti').html(data[0].totuti);
            $('#totiva').html(data[0].totiva);
            if (gru == "" || gru == null) {} else {
                $('#divtotgru' + gru).html(data[0].totalgru);
            }

            if (cap == "" || cap == null) {} else {
                $('#divtotcap' + idcap).html(data[0].totalcap);
            }

            if (idact == "" || idact == null) {} else {
                $('#divtotact' + idact).html(data[0].totalact);
            }

        }, "json");
}

function actualizarcosto(id) {
    var idpre = $('#idedicion').val();
	//var dec = checkDecimals(id);
    //$('#totcos').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
    //$('#totpre').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
    //$('#totadm').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
    //$('#totimp').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
    //$('#totuti').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
    //$('#totiva').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
    var adm = $('#administracion').val();
    var imp = $('#imprevisto').val();
    var uti = $('#utilidades').val();
    var iva = $('#iva').val();
	
    $.post('funciones/fnPresupuesto.php', {
            opcion: 'ACTUALIZARCOSTOS',
            idpresupuesto: idpre,
            adm: adm,
            imp: imp,
            uti: uti,
            iva: iva
        },
        function(data) {
            $('#totcos').html(data[0].totalneto);
            $('#totpre').html(data[0].totpre);
            $('#totadm').html(data[0].totadm);
            $('#totimp').html(data[0].totimp);
            $('#totuti').html(data[0].totuti);
            $('#totiva').html(data[0].totiva);
            //setTimeout(cargarlistadostipo('CARGARLISTAPRESUPUESTO'),1000);
        }, "json");
	

}
$(document).ready(function(e) {
    $('#mostrar').on('click', function() {
        type = 'zoomin';
        $('.overlay-container').fadeIn(function() {
            window.setTimeout(function() {
                $('.window-container.' + type).addClass('window-container-visible');
            }, 100);
        });
    });
});

function CRUDPRESUPUESTOINICIAL(o, id, idd, gru, cap) {
    if (o == "ACTUALIZARRECURSO") {
        var pre = $('#presupuesto').val()
        var deti = gru;
        var ins = idd;
        var act = id;
        var ren = $('#rendac' + cap + 'a' + act + 'i' + ins).val();
        var vala = $('#valac' + cap + 'a' + act + 'i' + ins).val();
        var vala2 = vala.replace(",", "").replace(",", "");
        vala2 = vala2.replace(",", "").replace(",", "");
        vala = vala2.replace("$", "");
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                pre: pre,
                cap: cap,
                act: act,
                ins: ins,
                rend: ren,
                val: vala
            },
            function(data) {
                var res = data[0].res;
                if (res == "ok") {
                    console.log(deti);
                    $('#materiale' + deti).html(data[0].mate);
                    $('#equipoe' + deti).html(data[0].equi);
                    $('#manoe' + deti).html(data[0].man);
                    $('#cambioe' + deti).html(data[0].alc);
                    setTimeout(CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA', cap, data[0].idda, '', ''), 1000);
                } else {
                    error("surgio un error al actualizar valores");
                }
            }, "json");
    }
    else
    if (o == "ACTUALIZARRECURSOAIUE") {
        var pre = $('#presupuesto').val();
        var deti = gru;
        var ins = idd;
        var act = id;

        var adm = $('#adme' + pre + '_' + gru + '_' + cap + '_' + act + '_' + ins).val();
        var imp = $('#impe' + pre + '_' + gru + '_' + cap + '_' + act + '_' + ins).val();
        var uti = $('#utie' + pre + '_' + gru + '_' + cap + '_' + act + '_' + ins).val();
        var iva = $('#ivae' + pre + '_' + gru + '_' + cap + '_' + act + '_' + ins).val();
        //if(adm=="" || adm==null){adm=0;}
        if (adm == "" || adm == null) {
            adm = 0;
        }
        if (imp == "" || imp == null) {
            imp = 0;
        }
        if (uti == "" || uti == null) {
            uti = 0;
        }
        if (iva == "" || iva == null) {
            iva = 0;
        }


        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                pre: pre,
                cap: cap,
                act: act,
                ins: ins,
                adm: adm,
                imp: imp,
                uti:uti,
                iva:iva
            },
            function(data) {
                var res = data[0].res;
                if (res == "ok") {
                    $('#materiale' + deti).html(data[0].mate);
                    $('#equipoe' + deti).html(data[0].equi);
                    $('#manoe' + deti).html(data[0].man);
                    $('#cambioe' + deti).html(data[0].alc);
                    $('#incpe' + deti).html(data[0].incpe);
                    setTimeout(CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA', cap, data[0].idda, '', ''), 1000);
                } else {
                    error("surgio un error al actualizar valores");
                }
            }, "json");
    }
    if (o == "CALCULARTOTALES") {
        var pre = $('#presupuesto').val();
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                id: id,
                pre: pre
            },
            function(data) {
                if (id > 0) {
                    $('#divcapi' + id).html(data[0].divcapi);
                    $('#divcapa' + id).html(data[0].divcapa);
                    $('#divcapal' + id).html(data[0].divcapal);
                }
                $('#totaleventoi').html(data[0].totaleventoi);
                $('#totaleventoa').html(data[0].totaleventoa);
                $('#totaleventoal').html(data[0].totaleventoal);

                $('#totalobraei').html(data[0].totalobraei);
                $('#totalobraea').html(data[0].totalobraea);
                $('#totalobraeal').html(data[0].totalobraeal);
                setTimeout("$('.currency').formatCurrency({colorize: true})", 1000);
            }, "json");
    }
    if (o == "ACTUALIZARACTEXTRA") {
        var pre = $('#presupuesto').val();
        var canti = $('#canti' + idd).val();
        var cantp = $('#cantp' + idd).val();
        var cante = $('#cante' + idd).val();
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                id: id,
                idd: idd,
                pre: pre,
                canti: canti,
                cantp: cantp,
                cante: cante
            },
            function(data) {
                var res = data[0].res;
                if (res == "ok") {
                    $('#divtotie' + idd).html(data[0].preini);
                    $('#divactfe' + idd).html(data[0].cantf);
                    $('#divtotactae' + idd).html(data[0].preact);
                    $('#divtotactce' + idd).html(data[0].alcanca);
                    $('#vaae' + idd).val(data[0].apua);
                    //CALCULARTOTALES
                    setTimeout(CRUDPRESUPUESTOINICIAL('CALCULARTOTALES', id, '', '', ''), 2000)
                } else {
                    error("Surgio un error al actualizar actividad. Verificar");
                }
            }, "json");
    }
    if (o == "DELETEACTEVENTO") {
        var pre = $('#presupuesto').val();
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                id: id,
                idd: idd,
                pre: pre
            },
            function(data) {
                var res = data[0].res;
                if (res == "ok") {
                    var table = $('#tbactividadp' + pre + 'c' + id).DataTable();
                    table.row('#' + idd).remove().draw(false);
                    setTimeout(CRUDPRESUPUESTOINICIAL('CALCULARTOTALES', id, '', '', ''), 2000)
                } else {
                    error("Surgio un error al eliminar capitul extra");
                }
            }, "json");
    }
    if (o == "DELETEASIGNAREVENTO") {
        var pre = $('#presupuesto').val();
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                id: id,
                pre: pre
            },
            function(data) {
                var res = data[0].res;
                if (res == "ok") {
                    var table = $('#tbeventos').DataTable();
                    table.row('#rowe_' + id).remove().draw(false);
                    setTimeout(CRUDPRESUPUESTOINICIAL('CALCULARTOTALES', '', '', '', ''), 2000)
                } else {
                    error("Surgio un error al eliminar capitul extra");
                }
            }, "json");

    } else
    if (o == "ACTUALIZARCAPEXTRA") {
        var apro = $('#aproe' + id).val();
        var fec = $('#feche' + id).val();
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                apro: apro,
                fec: fec,
                id: id
            },
            function(data) {
                var res = data[0].res;
                if (res == "ok") {

                } else {
                    error("surgio un error al actualizar información. Verificar");
                }
            }, "json");
    } else
    if (o == "EVENTOS") {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o
            },
            function(data) {
                $('#diveventos').html(data);
            })
    } else
    if (o == "OTROTIPROYECTO") {
        $('#otrotipoproyecto').val('');
        var ti = $('#seltipoproyecto').val();
        if (ti == "") {
            $('#otrotipoproyecto').attr('disabled', false);
        } else {
            $('#otrotipoproyecto').attr('disabled', true);
        }

    } else
    if (o == "CARGARLISTAPRESUPUESTO") 
	{
        var tableg = $('#tbgruagregados').DataTable();
        tableg.destroy();
        var tabler = $('#tbrecursos').DataTable();
        tabler.destroy();
        var tablea = $('#tbApu').DataTable();
        tablea.destroy();
       var tablep = $('#tbPartidas').DataTable();
        tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        var tablef = $('#tbfacturas').DataTable();
        tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        var tablec = $('#tbEstadoscontrato').DataTable();
        tablec.destroy();
        $('#presupuestoseleccionado').html("");
        $('#divfiltrop').show(1000);
        $('#tabladatos').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: 'CARGARLISTAPRESUPUESTO'
            },
            function(data) {
                $('#tabladatos').html(data);
            })

    } else if (o == "INFORMES") //REGISTRO DE NUEVA CIUDAD
    {
        var pre = $('#presupuesto').val();
        $('#divfiltrop').hide(1000);
        $('#tabladatos').html("");
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#tabladatos').html(data);
            });
    } else if (o == "ALCANCEPRESUPUESTADO") //REGISTRO DE NUEVA CIUDAD
    {
        $('#tablainformes').css('display','none');
        $('#tablainformes2').css('display','none');
        $('#tablainformesrecursos').css('display','none');
        $('#tablainformesalcance').css('display','');
        $('#tablainformescontratos').css('display','none');
        $('#tablainformesegreso').css('display','none');
        var alcv = $('#ALCSELECCIONADO').val();
        //var tableg = $('#tbgruagregados').DataTable();
        //tableg.destroy();
        //var tabler = $('#tbrecursos').DataTable();
        //tabler.destroy();
        //var tablea = $('#tbApu').DataTable();
        //tablea.destroy();
        //var tablep = $('#tbPartidas').DataTable();
        //tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        //var tablef = $('#tbfacturas').DataTable();
        //tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        //var tablec = $('#tbEstadoscontrato').DataTable();
        //tablec.destroy();
        var pre = $('#presupuesto').val();
        if(alcv==1){}
        else
        {
            $('#ALCSELECCIONADO').val(1);
            $('#tablainformesalcance').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');
            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: o,
                    id: pre
                },
                function (data) {
                    $('#tablainformesalcance').html(data);
                })
        }

    } else if (o == "USARACTIVIDAD") {
        var idpre = $('#presupuesto').val();
        $('#btnguardar').css('display', 'none');
        $('#myModalLabel').html("Datos Actividad a usar");
        $('#contenido2').html('');
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                actividad: id,
                iddetalle: idd,
                presupuesto: idpre,
                grupo: gru,
                capitulo: cap
            },
            function(data) {
                $('#contenido2').html(data);
            })
    }
    else
    if (o == "GUARDARACTIVIDADUSAR" || o == "GUARDARACTIVIDADUSARINICIAL")
    {

        var txtdescripcion = $('#txtdescripcion').val();
        var seltipoproyecto = $('#seltipoproyecto').val();
        var selunidad = $('#selunidad').val();
        var selciudad = $('#selciudad').val();
        var radestado = $('input:radio[name=radestado]:checked').val();
        var radanalisis = $('input:radio[name=radanalisis]:checked').val();

        if (txtdescripcion == "" || txtdescripcion == null) {
            error('Diligenciar la descripción de la actividad. Verificar');
        } else
        if (seltipoproyecto == "" || seltipoproyecto == null) {
            error('Seleccionar el tipo de proyecto de la actividad. Verificar');
        } else
        if (selunidad == "" || selunidad == null) {
            error('Seleccionar la unidad de medida de la actividad. Verificar');
        } else
        if (selciudad == "" || selciudad == null) {
            error('Seleccionar la ciudad de la actividad. Verificar');
        } else {
            var idpre = $('#presupuesto').val();
            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: o,
                    actividad: id,
                    presupuesto: idpre,
                    iddetalle: idd,
                    tipoproyecto: seltipoproyecto,
                    descripcion: txtdescripcion,
                    unidad: selunidad,
                    ciudad: selciudad,
                    estado: radestado,
                    analisis: radanalisis,
                    grupo: gru,
                    capitulo: cap
                },
                function(data) {
                    data = $.trim(data);
                    if (data == "error1") {
                        error('Ya hay una actividad con el mismo nombre diligenciado,ciudad y tipo proyecto seleccionados.Verificar');
                    } else if (data == "error2") {
                        error('Surgió un error al crear la nueva actividad.Verificar');
                    } else if (data == "ok") {
                        ok('Nueva actividad creada correctamente.Verificar en maestra de actividades');
                        $('.modal').modal('hide');
                    }
                })
        }
    }

    //INICIO FUNCIONES CONTROL EGRESO
    else if (o == "CONTROLEGRESO")
    {
        $('#tablainformes').css('display','none');
        $('#tablainformes2').css('display','none');
        $('#tablainformesrecursos').css('display','none');
        $('#tablainformesalcance').css('display','none');
        $('#tablainformescontratos').css('display','none');
        $('#tablainformesegreso').css('display','');
        var este = $('#EGRESELECCIONADO').val();

        //var tableg = $('#tbgruagregados').DataTable();
        //tableg.destroy();
        //var tabler = $('#tbrecursos').DataTable();
        //tabler.destroy();
       // var tablea = $('#tbApu').DataTable();
       // tablea.destroy();
        //var tablep = $('#tbPartidas').DataTable();
        //tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
       // var tablef = $('#tbfacturas').DataTable();
      //  tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        //var tablec = $('#tbEstadoscontrato').DataTable();
        //tablec.destroy();
       /* if(este==1) {
            var tablee = $('#tbfacturas').DataTable();
            // tabler.page( 'current' ).draw( 'page' );

            tablee.draw('full-hold');
        }
        else {*/
            $('#EGRESELECCIONADO').val(1);
            var pre = $('#presupuesto').val();
            $('#tablainformesegreso').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div><div class="loader-section section-right"></div></div>');

            $.post('funciones/fnPresupuestoEgreso.php', {
                    opcion: o,
                    id: pre
                },
                function (data) {
                    $('#tablainformesegreso').html(data);
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
                })
       // }
    }
    //INICIO FUNCIONES CONTROL EGRESO
    else if (o == "INFORMEEJECUTIVO")
    {
        $('#tablainformes').css('display','none');
        $('#tablainformes2').css('display','');
        $('#tablainformesrecursos').css('display','none');
        $('#tablainformesalcance').css('display','none');
        $('#tablainformescontratos').css('display','none');
        $('#tablainformesegreso').css('display','none');
        //var tableg = $('#tbgruagregados').DataTable();
       // tableg.destroy();

        //var tabler = $('#tbrecursos').DataTable();
        //tabler.destroy();
        //var tablea = $('#tbApu').DataTable();
        //tablea.destroy();
      //  var tablep = $('#tbPartidas').DataTable();
       // tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        //var tablef = $('#tbfacturas').DataTable();
        //tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        // var tablec = $('#tbEstadoscontrato').DataTable();
        //tablec.destroy();
        var pre = $('#presupuesto').val();
        $('#tablainformes2').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

        $.post('funciones/fnPresupuestoEjecutivo.php', {
                opcion: o,
                id: pre
            },
            function(data) {
                $('#tablainformes2').html(data);
            })
    }
    //INICIO FUNCIONES ESTADOS CONTRATOS
    else if (o == "ESTADOSCONTRATOS") {
        $('#tablainformes').css('display','none');
        $('#tablainformes2').css('display','none');
        $('#tablainformesrecursos').css('display','none');
        $('#tablainformesalcance').css('display','none');
        $('#tablainformescontratos').css('display','');
        $('#tablainformesegreso').css('display','none');
        var estc = $('#ESTCONTSELECCIONADO').val();
        //var tableg = $('#tbgruagregados').DataTable();
        //tableg.destroy();
        //var tabler = $('#tbrecursos').DataTable();
        //tabler.destroy();
        //var tablea = $('#tbApu').DataTable();
        //tablea.destroy();
       // var tablep = $('#tbPartidas').DataTable();
       // tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        //var tablef = $('#tbfacturas').DataTable();
        //tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        //var tablec = $('#tbEstadoscontrato').DataTable();
        //tablec.destroy();
        if(estc==1) {
            var tablee = $('#tbEstadoscontrato').DataTable();
            // tabler.page( 'current' ).draw( 'page' );
            /*  tabler
                  .order( [[ 1, 'asc' ]] )
                  .draw( false );*/
            tablee.draw('full-hold');
        }
        else
        {
            $('#ESTCONTSELECCIONADO').val(1);
            var pre = $('#presupuesto').val();
            $('#tablainformescontratos').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

            $.post('funciones/fnEstadoscontrato.php', {
                    opcion: o,
                    id: pre
                },
                function (data) {
                    $('#tablainformescontratos').html(data);
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
                });
        }
    } else if (o == "PAGOSACTAS") {
        $('#tablainformes').css('display','none');
        $('#tablainformes2').css('display','');
        $('#tablainformesrecursos').css('display','none');
        $('#tablainformesalcance').css('display','none');
        $('#tablainformescontratos').css('display','none');
        $('#tablainformesegreso').css('display','none');

        //var tableg = $('#tbgruagregados').DataTable();
        //tableg.destroy();
        //var tabler = $('#tbrecursos').DataTable();
        //tabler.destroy();
       // var tablea = $('#tbApu').DataTable();
       // tablea.destroy();
        //var tablep = $('#tbPartidas').DataTable();
       /// tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        //var tablef = $('#tbfacturas').DataTable();
       //tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        ////var tablec = $('#tbEstadoscontrato').DataTable();
        //tablec.destroy();
        var pre = $('#presupuesto').val();
        $('#tablainformes2').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

        $.post('funciones/fnEstadoscontrato.php', {
                opcion: o,
                id: pre
            },
            function(data) {
                $('#tablainformes2').html(data);
            })
    }
    //INICIO CONTROL
    else if (o == "INFORMERECURSOS") {
        $('#tablainformes').css('display','none');
        $('#tablainformes2').css('display','none');
        $('#tablainformesrecursos').css('display','');
        $('#tablainformesalcance').css('display','none');
        $('#tablainformescontratos').css('display','none');
        $('#tablainformesegreso').css('display','none');
        var recv = $('#RECSELECCIONADO').val();
        //var tableg = $('#tbgruagregados').DataTable();
        //tableg.destroy();

        //tabler.destroy();
        //var tablea = $('#tbApu').DataTable();
        //tablea.destroy();
        //var tablep = $('#tbPartidas').DataTable();
       // tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        //var tablef = $('#tbfacturas').DataTable();
        //tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
       // var tablec = $('#tbEstadoscontrato').DataTable();
        //tablec.destroy();
        if(recv==1)
        {
            var tabler = $('#tbrecursos2').DataTable();
           // tabler.page( 'current' ).draw( 'page' );
          /*  tabler
                .order( [[ 1, 'asc' ]] )
                .draw( false );*/
            tabler.draw('full-hold');
            //setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
            setTimeout(CRUDPRESUPUESTOINICIAL('TOTALINFORMERECURSOS', '', '', '', ''), 2000)
            /*    var clonedHeaderRow;

           $(".persist-area").each(function() {
                clonedHeaderRow = $(".persist-header", this);
                clonedHeaderRow
                    .before(clonedHeaderRow.clone())
                    .css("width", clonedHeaderRow.width())
                    .addClass("floatingHeader");

            });

            $(window)
                .scroll(UpdateTableHeaders)
                .trigger("scroll");*/
        }
        else
        {
            $('#RECSELECCIONADO').val(1);
            var pre = $('#presupuesto').val();
            $('#tablainformesrecursos').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

            $.post('funciones/fnRecursos.php', {
                    opcion: o,
                    pre: pre
                },
                function (data) {
                    $('#tablainformesrecursos').html(data);
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
                })
        }
    }
    else if(o=="TOTALINFORMERECURSOS"){
        var pre = $('#presupuesto').val();
      $.post('funciones/fnRecursos.php',{opcion:o,pre:pre},
            function(data){
        $('#totaladjr').html(data[0].totaladj);
        $('#totalcor').html(data[0].totalcor);
        $('#totalprer').html(data[0].totalpre);
        $('#totaldisr').html(data[0].totaldispo);
            setTimeout(function(){
                $('.currency').formatCurrency({
                    colorize: true
                });

            },1000);
        },"json");
    }
    else if (o == "INFORMEAPU") {
        var tableg = $('#tbgruagregados').DataTable();
        tableg.destroy();
        var tabler = $('#tbrecursos').DataTable();
        tabler.destroy();
        var tablea = $('#tbApu').DataTable();
        tablea.destroy();
      //  var tablep = $('#tbPartidas').DataTable();
     //   tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        var tablef = $('#tbfacturas').DataTable();
        tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        var tablec = $('#tbEstadoscontrato').DataTable();
        tablec.destroy();
        var pre = $('#presupuesto').val();
        $('#tablainformes').html('');

        $.post('funciones/fnRecursos.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#tablainformes').html(data);
            })
    } else if (o == "INFORMEAPUINICIAL" || o == "INFORMEAPUACTUAL") {
        var pre = $('#presupuesto').val();
        $('#informesapu').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

        $.post('funciones/fnRecursos.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#informesapu').html(data);
                setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
            })
    } else if (o == "INFORMESUB") {
        var tableg = $('#tbgruagregados').DataTable();
        tableg.destroy();
        var tabler = $('#tbrecursos').DataTable();
        tabler.destroy();
        var tablea = $('#tbApu').DataTable();
        tablea.destroy();
        var tablep = $('#tbPartidas').DataTable();
        tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        var tablef = $('#tbfacturas').DataTable();
        tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        var tablec = $('#tbEstadoscontrato').DataTable();
        tablec.destroy();
        var pre = $('#presupuesto').val();
        $('#tablainformes').html("");
        $.post('funciones/fnRecursos.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#tablainformes').html(data);
            })
    } else if (o == "INFORMESUBINICIAL" || o == "INFORMESUBACTUAL") {
        var pre = $('#presupuesto').val();
        $('#informessub').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

        $.post('funciones/fnRecursos.php', {
                opcion: o,

                pre: pre
            },
            function(data) {
                $('#informessub').html(data);
                setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
            })
    } else if (o == "INFORMEPAR") {
        var recv = $('#PARSELECCIONADO').val();
        var pars = $('#PARVERSELECCIONADO').val();
        $('#tablainformes').css('display','');
        $('#tablainformes2').css('display','none');
        $('#tablainformesrecursos').css('display','none');
        $('#tablainformesalcance').css('display','none');
        $('#tablainformescontratos').css('display','none');
        $('#tablainformesegreso').css('display','none');

        //var tableg = $('#tbgruagregados').DataTable();
        //tableg.destroy();
        //var tabler = $('#tbrecursos').DataTable();
        //tabler.destroy();
        //tabler.fixedHeader.disable();
        //var tablea = $('#tbApu').DataTable();
        //tablea.destroy();
        if(recv==1)
            {
                  var par = $('#selpartida').val();
                  if(pars==0)
                  {
                      $('#divpartida').css('display','');
                      $('#divconsolidado').css('display','none');
                      var tabler = $('#tbPartidas').DataTable();
                      // tabler.page( 'current' ).draw( 'page' );
                      /*  tabler
                       .order( [[ 1, 'asc' ]] )
                       .draw( false );*/
                      tabler.draw('full-hold');
                     // CRUDPARTIDAS('VERPARTIDA', par, '', '', '', '', '', '');
                  }
                  else
                  {
                      CRUDPRESUPUESTOINICIAL('INFORMECONSOLIDADOPAR','','','','','','','');
                  }
            }
            else {

            $('#PARSELECCIONADO').val(1);
          //  var tablep = $('#tbPartidas').DataTable();
           // tablep.destroy();
            var tablep2 = $('#tbPartidas2').DataTable();
            tablep2.destroy();
            //var tablef = $('#tbfacturas').DataTable();
            //tablef.destroy();
            var tablepa = $('#tbPagosActas').DataTable();
            tablepa.destroy();
            ////var tablec = $('#tbEstadoscontrato').DataTable();
            //tablec.destroy();
            //console.log("Por aca");

            var pre = $('#presupuesto').val();
            $('#tablainformes').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div>          <div class="loader-section section-right"></div></div>');

            $.post('funciones/fnPartidas.php', {
                    opcion: o,
                    pre: pre
                },
                function (data) {
                    $('#tablainformes').html(data);
                    setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
                });
        }
    } else if (o == "INFORMECONSOLIDADOPAR") {
        $('#PARVERSELECCIONADO').val(1);
        $('#divverpatida').show();
        $('#divnuepartida').hide();
        $('#divdelpartida').hide();
        $('#divselpartida').hide();

        $('#divconsolidado').css('display','');
        $('#divpartida').css('display','none');
        $('#tablainformes').css('display','');
        $('#tablainformes2').css('display','none');
        $('#tablainformesrecursos').css('display','none');
        $('#tablainformesalcance').css('display','none');
        $('#tablainformescontratos').css('display','none');
        $('#tablainformesegreso').css('display','none');

        // var tableg = $('#tbgruagregados').DataTable();
       // tableg.destroy();
       // var tabler = $('#tbrecursos').DataTable();
       // tabler.destroy();
       // var tablea = $('#tbApu').DataTable();
       // tablea.destroy();
       // var tablep = $('#tbPartidas').DataTable();
       // tablep.destroy();
        var tablep2 = $('#tbPartidas2').DataTable();
        tablep2.destroy();
        //var tablef = $('#tbfacturas').DataTable();
        //tablef.destroy();
        var tablepa = $('#tbPagosActas').DataTable();
        tablepa.destroy();
        //var tablec = $('#tbEstadoscontrato').DataTable();
        //tablec.destroy();
        var pre = $('#presupuesto').val();
        $('#divconsolidado').html('<div id="loader-wrapper"><div id="loader"></div><div class="loader-section section-left"></div><div class="loader-section section-right"></div></div>');

        $.post('funciones/fnPartidas.php', {
                opcion: o,
                pre: pre
            },
            function(data) {
                $('#divconsolidado').html(data);
            })
    } else if (o == "ASOCIARAPU") {
        var pre = $('#presupuesto').val();
        $('#msn').html('');
        $('#btnguardar').attr('name', 'GUARDARASOCIAR');
        $('#btnguardar').attr('value', 'Guardar Cambios');
        $('#myModalLabel').html("Asociación de Apu Actividades");
        $('#contenido2').html('');
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                id: id,
                gru: gru,
                cap: cap,
                pre: pre,
                idd: idd
            },
            function(data) {
                $('#contenido2').html(data);
            })
    } else if (o == "AGREGADOS") //REGISTRO DE NUEVA CIUDAD
    {
        $('#pendientes').html('');
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#pendientes').html(data);
            })
    } else if (o == "AGREGADOSSUB") //REGISTRO DE NUEVA CIUDAD
    {
        $('#pendientes').html('');
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#pendientes').html(data);
            })
    } else
    if (o == "AGREGARAPU") {
        var ide = $('#idedicion').val();
        var gru = $('#gruedicion').val();
        var cap = $('#capedicion').val();
        var pre = $('#presupuesto').val();
        var idd = $('#iddedicion').val();
        var ins = id; //$('#selagregar').val();
        var ren = $('#rend' + id).val();
        //var val = $('#valorunitario').val();
        if (ins == "" || ins == null) {
            error('Seleccionar el recurso a agregar. Verificar');
        } else {
            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: o,
                    insumo: ins,
                    id: ide,
                    rendimiento: ren,
                    pre: pre,
                    gru: gru,
                    cap: cap
                },
                function(data) {
                    if (data == 1) {
                        var table = $('#tbinsumosporagregar').DataTable();
                        table.row('#rowi_' + id).remove().draw(true);
                        var table2 = $('#tbinsumospendientes').DataTable();
                        table2.draw(false);
                        //CRUDACTIVIDADES('AGREGADOS',ide);
                        ok('Recurso agregado correctamente');
                        setTimeout(actualizartotala(pre, gru, cap, ide, idd, '', ''), 2000);
						setTimeout(ACTUALIZARENLAZADO(pre),3000);

                        //setTimeout(CRUDPRESUPUESTOINICIAL('ALCANCEPRESUPUESTADO','','','',''),2000);
                    } else {
                        error('Error al agregar recurso. Verificar');
                    }
                });
        }
    } else
    if (o == "DELETEAPU") {
        var ide = $('#idedicion').val();
        var gru = $('#gruedicion').val();
        var cap = $('#capedicion').val();
        var pre = $('#presupuesto').val();
        var idd = $('#iddedicion').val();
        jConfirm('Realmente desea quitar el recurso agregado?', 'Diálogo Confirmación LG', function(r) {
            //msn = confirm("Realmente desea quitar el recurso agregado");
            if (r == true) {
                var ide = $('#idedicion').val();
                $.post('funciones/fnPresupuestoInicial.php', {
                        opcion: o,
                        id: id,
                        pre: pre,
                        gru: gru,
                        cap: cap
                    },
                    function(data) {
                        if (data == 1) {
                            var table = $('#tbinsumospendientes').DataTable();
                            table.row('#row_insp' + ide).remove().draw(false);
                            var table2 = $('#tbinsumosporagregar').DataTable();
                            table2.draw(false);
                            ok('Recurso eliminado correctamente');
                            setTimeout(actualizartotala(pre, gru, cap, ide, idd, '', ''), 2000);
							setTimeout(ACTUALIZARENLAZADO(pre),3000);
                            //setTimeout(CRUDPRESUPUESTOINICIAL('ALCANCEPRESUPUESTADO','','','',''),2000);
                        } else {
                            error('Error al eliminar actividad. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "VERAPU") //REGISTRO DE NUEVA CIUDAD
    {
        var pre = $('#presupuesto').val();

        $('#divchil' + id).html('');
        var table2 = "<div id='no-more-tables'><table  class='table table-condensed compact table-hover' id='tbinsumos" + id + "p" + pre + "g" + gru + "c" + cap + "'>";
        table2 += "<thead><tr>" +
            "<th class='dt-head-center'>CODIGO</th>" +
            "<th class='info dt-head-center' width='20%'>RECURSO</th>" +
            "<th class='info dt-head-center' width='7%'>UND</th>" +
            "<th class='info dt-head-center 'width='6%'>REND</th>" +
            "<th class='info dt-head-center' width='6%'>V/UNITARIO</th>" +
            "<th class='info dt-head-center' width='6%'>MATERIAL</th>" +
            "<th class='info dt-head-center' width='6%'>EQUIPO</th>" +
            "<th class='info dt-head-center' width='6%'>M.DE.O</th>" +
            "<th class='danger dt-head-center' width='6%'>REND ACTUAL.</th>" +
            "<th class='danger dt-head-center' width='12%'>VR.UNIT ACTUAL</th>" +
            "<th class='danger dt-head-center' width='6%'>AIU</th>" +
            "<th class='danger dt-head-center' width='6%'>MATERIAL</th>" +
            "<th class='danger dt-head-center' width='6%'>EQUIPO</th>" +
            "<th class='danger dt-head-center' width='6%'>M.DE.O</th>" +
            "<th class='success dt-head-center' width='6%'>C.ALCANCE</th>" +
            "</tr></thead><tbody>";
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "LISTAINSUMOS",
                id: id,
                pre: pre,
                gru: gru,
                cap: cap
            },
            function(dato) {
                var res = dato[0].res;
                if (res == "no") {} else {
                    for (var i = 0; i < dato.length; i++) {
                        var cre = dato[i].cre;
                        if (cre == 1) {
                            table2 += "<tr class='delete1' id='" + dato[i].iddetalle + "'>";
                        } else {
                            table2 += "<tr>";
                        }
                        table2 +=
                            "<td data-title='Codigo'>" + dato[i].insu + "</td>" +
                            "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                            "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                            "<td data-title='Rendimiento' class='dt-center'>" + dato[i].rendi + "</td>" +
                            "<td data-title='Valor Unitario' class='dt-right currency'>$" + dato[i].valor + "</td>" +
                            "<td data-title='Material' class='dt-right currency'>$" + dato[i].material + "</td>" +
                            "<td data-title='Equipo' class='dt-right currency'>$" + dato[i].equipo + "</td>" +
                            "<td data-title='Mano de obra' class='dt-right currency'>$" + dato[i].mano + "</td>" +
                            "<td data-title='Rendimiento Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].REA + "</td>" +
                            "<td data-title='Valor Unitario Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].VAA + "</td>" +
                            "<td title='Hola'><a class='ivasactual tooltipstered' style='cursor:pointer'  title='This is bad content' id='" + dato[i].iddetalle + "_" + pre + "_" + gru + "_" + cap + "_" + id + "_" + dato[i].insu + "'>AIU<span id='incp"+dato[i].iddetalle+"'>"+dato[i].incp+"</span></a></td>" +
                            "<td data-title='Material' class='dt-right'>" + dato[i].materiala + "</td>" +
                            "<td data-title='equipo' class='dt-right'>" + dato[i].equipoa + "</td>" +
                            "<td data-title='mano' class='dt-right'>" + dato[i].manoa + "</td>" +
                            "<td  data-title='C.Alcance' class='dt-right'>" + dato[i].totalact + "</td></tr>";

                    }
                }
                table2 += "</tbody></table></div>";
                $('#divchil' + id).html(table2);
                setTimeout(convertirdata4(id,pre,gru,cap),1000);
                //setTimeout(INICIALIZARTOOLTIP(),1000)
                //setTimeout(autoTabIndex2("#tbinsumos"+id+"p"+pre+"g"+gru+"c"+cap,14),1000);
            }, "json");
    } else if (o == "VERAPUE") {

        var pre = $('#presupuesto').val();

        $('#divchil' + id).html('');
        var table2 = "<div id='no-more-tables'><table  class='table table-condensed compact' id='tbinsumos" + id + "p" + pre + "c" + cap + "'>";
        table2 += "<thead><tr>" +
            "<th class='dt-head-center'>CODIGO</th>" +
            "<th class='info dt-head-center' width='20%'>RECURSO</th>" +
            "<th class='info dt-head-center' width='7%'>UND</th>" +
            "<th class='info dt-head-center 'width='6%'>REND</th>" +
            "<th class='info dt-head-center' width='6%'>V/UNITARIO</th>" +
            "<th class='info dt-head-center' width='6%'>MATERIAL</th>" +
            "<th class='info dt-head-center' width='6%'>EQUIPO</th>" +
            "<th class='info dt-head-center' width='6%'>M.DE.O</th>" +
            "<th class='danger dt-head-center' width='6%'>REND ACTUAL.</th>" +
            "<th class='danger dt-head-center' width='12%'>VR.UNIT ACTUAL</th>" +
            "<th class='danger dt-head-center' width='6%'>AIU</th>" +
            "<th class='danger dt-head-center' width='6%'>MATERIAL</th>" +
            "<th class='danger dt-head-center' width='6%'>EQUIPO</th>" +
            "<th class='danger dt-head-center' width='6%'>M.DE.O</th>" +
            "<th class='success dt-head-center' width='6%'>C.ALCANCE</th>" +
            "</tr></thead><tbody>";
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "LISTAINSUMOSE",
                id: id,
                pre: pre,
                cap: cap,
                act: idd
            },
            function(dato) {
                var res = dato[0].res;
                if (res == "no") {} else {
                    for (var i = 0; i < dato.length; i++) {
                        table2 += "<tr>" +
                            "<td data-title='Codigo'>" + dato[i].insu + "</td>" +
                            "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                            "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                            "<td data-title='Rendimiento' class='dt-center'>" + dato[i].rendi + "</td>" +
                            "<td data-title='Valor Unitario' class='dt-right currency'>$" + dato[i].valor + "</td>" +
                            "<td data-title='Material' class='dt-right currency'>$" + dato[i].material + "</td>" +
                            "<td data-title='Equipo' class='dt-right currency'>$" + dato[i].equipo + "</td>" +
                            "<td data-title='Mano de obra' class='dt-right currency'>$" + dato[i].mano + "</td>" +
                            "<td data-title='Rendimiento Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].REA + "</td>" +
                            "<td data-title='Valor Unitario Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].VAA + "</td>" +
                            "<td title='Hola'><a class='ivasactuale' style='cursor:pointer' onmouseover='INICIALIZARTOOLTIP()' id='" + dato[i].iddetalle + "_" + pre + "_" + cap + "_" + id +"_"+idd+ "_" + dato[i].insu + "'>AIU<span id='incpe"+dato[i].deti+"'>"+dato[i].incpe+"</span></a></td>" +
                            "<td data-title='Material' class='dt-right'>" + dato[i].materiala + "</td>" +
                            "<td data-title='eqipo' class='dt-right'>" + dato[i].equipoa + "</td>" +
                            "<td data-title='mano' class='dt-right'>" + dato[i].manoa + "</td>" +
                            "<td  data-title='C.Alcance' class='dt-right'>" + dato[i].totalact + "</td></tr>";

                    }
                }
                table2 += "</tbody></table></div>";
                $('#divchil' + id).html(table2);
                convertirinsumos(id, pre, cap, idd);
                setTimeout(INICIALIZARTOOLTIP(), 1000)
                setTimeout(autoTabIndex2("#tbinsumos" + id + "p" + pre + "c" + cap, 14), 1000);
            }, "json");

    } else if (o == "VERSUBANALISIS") {
        var pre = $('#presupuesto').val();
        $('#divchil' + id).html('');
        var table2 = "<table id='tbsubanalisis" + id + "p" + pre + "g" + gru + "c" + cap + "' class='table table-condensed compact' width='100%' style='font-size:11px;'>" +
            "<thead>" +
            "<tr>" +
            "<th class='dt-head-center' style='width:20px'></th>" +
            "<th class='dt-head-center' style='width:30px'>CÓDIGO</th>" +
            "<th class='dt-head-center'>NOMBRE</th>" +
            "<th class='dt-head-center' style='width:30px'>UND</th>" +
            "<th class='dt-head-center info' style='width:100px'>APU INICIAL</th>" +
            "<th class='dt-head-center danger' style='width:100px'>APU ACTUAL</th>" +
            "<th class='dt-head-center success' style='width:100px'>CAMBIO.ALC</th>" +

            "</tr>" +
            "</thead><tfoot><tr><th colspan='7'></th></tr></tfoot></table>";
        $('#divchil' + id).html(table2);
        convertirsub(id, pre, gru, cap);
    } else if (o == "VERSUBANALISISE") {
        var pre = $('#presupuesto').val();
        $('#divchil' + id).html('');
        var table2 = "<table id='tbsubanalisis" + id + "p" + pre + "c" + cap + "' class='table table-condensed compact' width='100%' style='font-size:11px;'>" +
            "<thead>" +
            "<tr>" +
            "<th class='dt-head-center' style='width:20px'></th>" +
            "<th class='dt-head-center' style='width:30px'>CÓDIGO</th>" +
            "<th class='dt-head-center'>NOMBRE</th>" +
            "<th class='dt-head-center' style='width:30px'>UND</th>" +
            "<th class='dt-head-center info' style='width:100px'>APU INICIAL</th>" +
            "<th class='dt-head-center danger' style='width:100px'>APU ACTUAL</th>" +
            "<th class='dt-head-center success' style='width:100px'>CAMBIO.ALC</th>" +

            "</tr>" +
            "</thead><tfoot><tr><th colspan='7'></th></tr></tfoot></table>";
        $('#divchil' + id).html(table2);
        convertirsube(id, pre, cap, idd);

    }
}

function guardarcantidadp(id, v, pre, gru, cap) {
    if (v == "") {
        error('Ingresar Cantidad');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLEP",
                id: id,
                cantidad: v
            },
            function(data) {
                var res = data[0].res;
                var act = data[0].act;
                if (res == 1) {
                    setTimeout(actualizartotala(pre, gru, cap, act, id, '', ''), 2000);
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                    setTimeout(actualizarcostoi(), 1000);
                    setTimeout(ACTUALIZARPARTIDAS(pre, gru, cap, act,''),2000);
                } else {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}
//FUNCION PARA ACTUALIZAR ITEM SELECCIONAR EN LA ACTIVIDAD MODIFICADA
function ACTUALIZARPARTIDAS(pre, gru, cap, act,ins){
    if(act>0){
        $.post('funciones/fnPresupuestoInicial.php',{opcion:"ACTUALIZARPARTIDAS",pre:pre,gru:gru,cap:cap,act:act,ins:ins},
        function(data){
            var cana = data[0].cana;
            //console.log("Cantidad Partidas Actualizadas:" + cana);
        },"json");
    }
}
function guardarcantidade(id, v, pre, gru, cap) {
	if(v==""){v = 0;}
	
    if (v == "") {
        error('Ingresar Cantidad');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLEE",
                id: id,
                cantidad: v
            },
            function(data) {
                var res = data[0].res;
                var act = data[0].act;
                if (res == 1) {
                    setTimeout(actualizartotala(pre, gru, cap, act, id, '', ''), 2000);
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                } else {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function guardarvalora(id, v, pre, gru, cap) {
    var v1 = v.replace("$", "");
    var v2 = v1.replace(",", "").replace(",", "");
    v2 = v2.replace(",", "").replace(",", "");
    v = v2;
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLEV",
                id: id,
                valor: v
            },
            function(data) {
                if (data == 1) {
                    setTimeout(actualizartotala(pre, gru, cap, id, '', '', ''), 2000);
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                } else {
                    error('Error BD.Verificar');
                }
            });
    }
}

function actualizarcostoi() {
    var idpre = $('#presupuesto').val();
    /*$('#totcosp').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
	$('#totprep').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
	$('#totadmp').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
	$('#totimpp').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
	$('#totutip').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
	$('#totivap').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
	$('#alccos').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
   $('#alcpre').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
   $('#alcadm').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
   $('#alcimp').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
   $('#alcuti').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");
   $('#alciva').html("<img alt='cargando' src='dist/img/cargando.gif' height='14' width='25' />");*/
    var adm = $('#administracionp').val();
    var imp = $('#imprevistop').val();
    var uti = $('#utilidadesp').val();
    var iva = $('#ivap').val();
    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: 'ACTUALIZARCOSTOS',
            idpresupuesto: idpre,
            adm: adm,
            imp: imp,
            uti: uti,
            iva: iva
        },
        function(data) {
            $('#totcosp').html(data[0].totalneto);
            $('#totprep').html(data[0].totpre);
            $('#totadmp').html(data[0].totadm);
            $('#totimpp').html(data[0].totimp);
            $('#totutip').html(data[0].totuti);
            $('#totivap').html(data[0].totiva);
            $('#alccos').html(data[0].alccos);
            $('#alcpre').html(data[0].alcpre);
            $('#alcadm').html(data[0].alcadm);
            $('#alcimp').html(data[0].alcimp);
            $('#alcuti').html(data[0].alcuti);
            $('#alciva').html(data[0].alciva);
            setTimeout(CRUDPRESUPUESTOINICIAL('CALCULARTOTALES', '', '', '', ''), 2000);
			setTimeout(ACTUALIZARENLAZADO(idpre),3000);
            setTimeout("$('.currency').formatCurrency({colorize: true})", 1000);

        }, "json");

}

function actualizartotala(pre, gru, cap, acti, act, idda, idsu) {
    var idpre = $('#presupuesto').val();
    $('#totcosp').html("");
    $('#totprep').html("");
    $('#totadmp').html("");
    $('#totimpp').html("");
    $('#totutip').html("");
    $('#totivap').html("");

    $('#divactf' + act).html("");

    $('#divtotacta' + act).html("");
    $('#divtotactc' + act).html("");


    $('#alccos').html("");
    $('#alcpre').html("");
    $('#alcadm').html("");
    $('#alcimp').html("");
    $('#alcuti').html("");
    $('#alciva').html("");

    $('#divtotgrua' + gru).html("");
    $('#divtotgrual' + gru).html("");



    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: 'CALCULOTOTALES',
            pre: pre,
            gru: gru,
            cap: cap,
            act: act,
            idda: idda,
            acti: acti,
            idsu: idsu
        },
        function(data) {
            //  alert('ok'+data[0].totalact);
            var idcap = data[0].idca;
            $('#divtotcapa' + idcap).html("");
            $('#divtotalcc' + idcap).html("");
            //actuales
            $('#totcosp').html(data[0].totalnetoa);
            $('#totaladjr').html(data[0].totalnetoa);// ADJUDICADO ES IGULA AL TOTAL ACTUAL NETO DEL PRESUPUESTO
            $('#totaldisr').html(data[0].totaldispo);
            $('#totprep').html(data[0].totprea);
            $('#totadmp').html(data[0].totadma);
            $('#totimpp').html(data[0].totimpa);
            $('#totutip').html(data[0].totutia);
            $('#totivap').html(data[0].totivaa);
			//iniciales
			$('#totcos').html(data[0].totalneto);
            $('#totpre').html(data[0].totpre);
            $('#totadm').html(data[0].totadm);
            $('#totimp').html(data[0].totimp);
            $('#totuti').html(data[0].totuti);
            $('#totiva').html(data[0].totiva);

            $('#alccos').html(data[0].alccos);
            $('#alcpre').html(data[0].alcpre);
            $('#alcadm').html(data[0].alcadm);
            $('#alcimp').html(data[0].alcimp);
            $('#alcuti').html(data[0].alcuti);
            $('#alciva').html(data[0].alciva);

            if (idcap == "" || idcap == null) {

            } else {
                $('#divtotcap' + idcap).html(data[0].totalcap);
                $('#divtotcapa' + idcap).html(data[0].totalcapa);
                $('#divtotalcc' + idcap).html(data[0].totalalcc);
            }
            //  jAlert("IddA:"+idda+" Act:"+act+"TotAct:"+data[0].totalact,null);
            if (act == "" || act == null) {

            } else {
                $('#divtotapu' + act).html(data[0].totalapu);
                $('#rempva' + act).html(data[0].totalapua);
                $('#rempvam' + act).html(data[0].totalapuam);
                $('#rempvae' + act).html(data[0].totalapuae);
                $('#rempvama' + act).html(data[0].totalapuamo);
                //console.log(act);
                $('#divtoti' + act).html(data[0].totalact);
                $('#divactf' + act).html(data[0].faltante);
                $('#divtotacta' + act).html(data[0].totalacta);
                $('#divtotactc' + act).html(data[0].totalalca);
            }

            if (idda != "") {
                // alert(data[0].totpreact);
                //$('#rempva'+idda).val(data[0].totalidda);

                //$('#divtotacta'+idda).html("$"+data[0].totpreact);
            }
            if (idsu != "") {
                $('#divapus' + idsu).html(data[0].totalsui);
                $('#divapusa' + idsu).html(data[0].totalsua);
                $('#divalcancesub' + idsu).html(data[0].alcancesub);
            }
            $('#divtotgrui' + gru).html(data[0].totgrui);
            $('#divtotgrua' + gru).html(data[0].totgruact);
            $('#divtotgrual' + gru).html(data[0].totgrualc);

            setTimeout(CRUDPRESUPUESTOINICIAL('CALCULARTOTALES', '', '', '', ''), 2000)
            //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);


        }, "json");
}

function cambiartamano(v) {
    var wt = (v * 100) / 12;
    $('.table').css('font-size', v + 'pt');
    $('.table').css('width', wt + '%');


}

function convertircheck() {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
}

function selectpick() {
    $('.selectpicker').selectpicker({
	liveSearch: true
	});
}

function INICIALIZARLISTAS(o) {
    if (o == "PRESUPUESTO") {
        $(".select2").select2();
        $(".singular2").select2();
        $("#datemask").inputmask("yyyy-mm-dd", {
            "placeholder": "yyyy-mm-dd"
        });
        $("[data-mask]").inputmask();
    } else if (o == "MODAL") {
        $(".select2").select2({
            closeOnSelect: false
        });
        $(".singular2").select2({
            placeholder: "--Seleccione--",
        });
    }
    $('.selectpicker').selectpicker({
        liveSearch: true,
        dropupAuto: false

    });

    $('.currency').formatCurrency({
        colorize: true
    });
    $('.ui.dropdown').dropdown({
        on: 'click'
    });
}

function inicializarid(id) {
    $('#' + id).selectpicker({
        liveSearch: true,
    });
}

function INICIALIZARTOOLTIPADJ() {
  /*  $(document).keyup(function(event) {
        if (event.which == 27) {

            $(".tooltipster-base").hide();
        }
    });*/
    $('.ivaadjudicado').tooltipster({
        content: "Click para modificar AIU",
        //contentAsHTML: true,				

        functionBefore: function(origin, continueTooltip) {
            continueTooltip();
            var id = $(this).attr('id');
            var n = id.split("_");
            var pre = n[1];
            var ins = n[3];
          

            //if (origin.data('ajax') !== 'cached') {
                $.ajax({
                    type: 'POST',
                    url: 'funciones/fnRecursos.php',
                    data: {
                        opcion: "DATOSAIU",                       
                        pre: pre,
                        ins: ins
                    },
                    success: function(data) {
                        // update our tooltip content with our returned data and cache it
                        origin.tooltipster({
                            content: data,
                            multiple: true,
                            contentAsHTML: true,
                            trigger: 'click',
                            theme: 'tooltipster-shadow',
                            contentCloning: false,
                            interactive: true,
                            animation: 'slide',
                            updateAnimation: 'scale',
                            position: 'bottom',
                            positionTracker: false,
                            //multiple:true,	
                            distance: 1,
                            offsetY: -12,
                            arrow: false,
                        });

                        //origin.tooltipster('update','VALORES AIU : ' + data).data('ajax', 'cached');
                    }
                });
            //}
            // when the request has finished loading, we will change the tooltip's content
            /*$.post('funciones/fnPresupuestoInicial.php',{opcion:"DATOSAIU",idd:idd,pre:pre,gru:gru,cap:cap,act:act,ins:ins},
            function(data)
            {
            	origin.tooltipster('content', 'VALORES AIU : ' + data);
            });	*/
        },
        functionAfter: function(origin) {
            //alert('The tooltip has closed!');	
            //origin.tooltipster();
        }
    });
}
function INICIALIZARTOOLTIP() {
    $(document).keyup(function(event) {
        if (event.which == 27) {

          //  $(".tooltipster-base").hide();
        }
    });

}

function ACTUALIZARFACTURAS() {
    var tof = $('#totfacturas'); //facturas y clase
    var ltof = $('#litotfacturas'); //textfactura
    var lfac = $('#listfacturas'); //lista append
    $('#listfacturas li').remove();
    tof.removeClass('label-danger').removeClass('label-default');
    $.post("funciones/fnPresupuestoInicial.php", {
            opcion: 'ACTUALIZARFACTURAS'
        },
        function(data) {
            tof.addClass(data[0].clase);
            tof.html(data[0].facturas);
            ltof.html(data[0].textfactura);
            lfac.append(data[0].lista);
        }, "json");
}
//codigo brayan
function guardarcantidadrenacts(id, pre, gru, cap, v, ida, ins) //rendimiento de subanalisis
{
    var v = $('#remrens' + id).val();
    if (v == "") {
        error('Ingresar Rendimiento');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLERENACTS",
                id: id,
                pre: pre,
                gru: gru,
                cap: cap,
                cantidad: v,
                act: ida,
                ins: ins
            },
            function(data) {
                if (data[0].res == 1) {
                    setTimeout(actualizarcambio2(id, pre, gru, cap, ida, ins), 2000);
                    setTimeout(actualizartotala(pre, gru, cap, ida, data[0].idda, data[0].idda, data[0].idsu), 2000);
                    setTimeout(actualizarcostoi(), 1000);
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
					//setTimeout(ACTUALIZARPARTIDAS(pre,gru,cap,ida,ins),3000);
                    //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
                } else {
                    error('Error BD.Verificar');
                }
            }, "json")
    }
}

function guardarcantidadrenactsu(id, pre, gru, cap, v, ida, ins) //rendimiento de subanalisis
{
    var v = $('#remren' + pre + 'g' + gru + 'c' + cap + 'a' + ida + 's' + id).val();
    if (v == "") {
        error('Ingresar Rendimiento');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLERENACTSU",
                id: id,
                pre: pre,
                gru: gru,
                cap: cap,
                cantidad: v,
                act: ida
            },
            function(data) {
                if (data[0].res == 1) {
                    setTimeout(actualizarcambio3(id, pre, gru, cap, ida, ins), 2000);
                    setTimeout(actualizartotala(pre, gru, cap, ida, data[0].idda, data[0].idda, ''), 2000);
                    setTimeout(actualizarcostoi(), 1000);
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                    //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
                } else {
                    error('Error BD.Verificar');
                }
            }, "json")
    }
}

function guardarcantidadrenact(id, pre, gru, cap, v, ida, ins) {
    var v = $('#remren' + id).val();
    if (v == "") {
        error('Ingresar Rendimiento');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLERENACT",
                id: id,
                pre: pre,
                gru: gru,
                cap: cap,
                cantidad: v,
                act: ida,
                ins: ins
            },
            function(data) {
                if (data[0].res == 1) {
                    setTimeout(actualizarcambio(id, pre, gru, cap, ida, ins), 2000);
                    setTimeout(actualizartotala(pre, gru, cap, ida, data[0].idda, data[0].idda, ''), 2000);
                    setTimeout(actualizarcostoi(), 3000)
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                    setTimeout(ACTUALIZARPARTIDAS(pre,gru,cap,ida,ins),3000);
                    //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
                } else if (data[0].res == 2) {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function guardarvaloract(id, pre, gru, cap, v, ida, ins) {
    var v1 = v.replace("$", "");
    var v2 = v1.replace(",", "").replace(",", "");
    v2 = v2.replace(",", "").replace(",", "");
    v = v2;
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLEVALACT",
                id: id,
                pre: pre,
                gru: gru,
                cap: cap,
                cantidad: v,
                act: ida,
                ins: ins
            },
            function(data) {
                if (data[0].res == 1) {
                    setTimeout(actualizarcambio(id, pre, gru, cap, ida, ins), 2000);
                    setTimeout(actualizartotala(pre, gru, cap, ida, data[0].idda, data[0].idda, ''), 2000);
                    setTimeout(actualizarcostoi(), 1000)
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                    setTimeout(ACTUALIZARPARTIDAS(pre,gru,cap,ida,ins),3000);
                    //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);

                } else if (data[0].res == 2) {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}

function guardarvaloracts(id, pre, gru, cap, v, ida, ins) {
    var v1 = v.replace("$", "");
    var v2 = v1.replace(",", "").replace(",", "");
    v2 = v2.replace(",", "").replace(",", "");
    v = v2;
    if (v == "") {
        error('Ingresar Valor');
    } else {
        $.post('funciones/fnPresupuestoInicial.php', {
                opcion: "GUARDARDETALLEVALACTS",
                id: id,
                pre: pre,
                gru: gru,
                cap: cap,
                valor: v,
                act: ida,
                ins: ins
            },
            function(data) {
                if (data[0].res == 1) {
                    setTimeout(actualizarcambio2(id, pre, gru, cap, ida, ins), 2000);
                    setTimeout(actualizartotala(pre, gru, cap, ida, data[0].idda, data[0].idda, data[0].idsu), 2000);
                    setTimeout(actualizarcostoi(), 1000)
					setTimeout(ACTUALIZARENLAZADO(pre),3000);
                    //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);				
                } else if (data[0].res == 2) {
                    error('Error BD.Verificar');
                }
            }, "json");
    }
}


function guardarsubactual(pre, act, sub, ins) //rendimiento de subanalisis
{
    var re = $('#remrens' + act + '_' + sub + '_' + ins).val();
    var v = $('#remvals' + act + '_' + sub + '_' + ins).val();
    var v1 = v.replace("$", "");
    var v2 = v1.replace(",", "").replace(",", "");
    v2 = v2.replace(",", "").replace(",", "");
    v = v2;

    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "GUARDARSUBACTUAL",
            pre: pre,
            cantidad: re,
            valor: v,
            act: act,
            sub: sub,
            ins: ins
        },
        function(data) {
            if (data[0].res == "ok") {

                $('#materials' + act + '_' + sub + '_' + ins).html(data[0].material);
                $('#equipos' + act + '_' + sub + '_' + ins).html(data[0].equipo);
                $('#manos' + act + '_' + sub + '_' + ins).html(data[0].mano);
                setTimeout("$('.currency').formatCurrency({colorize: true})", 1000);
            } else {
                error('Error BD.Verificar');
            }
        }, "json")

}

function actualizarcambio(id, pre, gru, cap, act, ins) {
    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "CAMBIOALCANCE",
            id: id,
            pre: pre,
            gru: gru,
            cap: cap,
            act: act,
            ins: ins
        },
        function(data) {
            $('#cambio' + id).html("$" + data[0].totales);
            $('#equipo' + id).html("$" + data[0].equi);
            $('#mano' + id).html("$" + data[0].mano);
            $('#material' + id).html("$" + data[0].mate);
            $('#incp' + id).html(data[0].incp);
        }, "json");
}

function actualizarcambio2(id, pre, gru, cap, act, ins) {
    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "CAMBIOALCANCESUB",
            id: id,
            pre: pre,
            gru: gru,
            cap: cap,
            act: act,
            ins: ins
        },
        function(data) {
            $('#cambios' + id).html("$" + data[0].totales);
            $('#equipos' + id).html("$" + data[0].equi);
            $('#manos' + id).html("$" + data[0].mano);
            $('#materials' + id).html("$" + data[0].mate);
        }, "json");
}

function actualizarcambio3(id, pre, gru, cap, act, ins) {
    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "CAMBIOALCANCESUBA",
            id: id,
            pre: pre,
            gru: gru,
            cap: cap,
            act: act
        },
        function(data) {
            $('#cambio' + pre + 'g' + gru + 'c' + cap + 'a' + act + 's' + id).html("$" + data[0].totales);
            $('#equipo' + pre + 'g' + gru + 'c' + cap + 'a' + act + 's' + id).html("$" + data[0].equi);
            $('#mano' + pre + 'g' + gru + 'c' + cap + 'a' + act + 's' + id).html("$" + data[0].mano);
            $('#material' + pre + 'g' + gru + 'c' + cap + 'a' + act + 's' + id).html("$" + data[0].mate);
        }, "json");
}


//CALCULOS DE EDICION DE VALORES ACTUALES DE SUBANALISIS EN ALCANCE PRESUPUESTADO
function LLENARGRUPO(gru,idg)
{
    var sel = $('#' + gru);
    sel.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
    $.post('funciones/fnPresupuestoEgreso.php', {
            opcion: 'LLENARGRUPO',
            idg: idg
        },
        function(data) {
            sel.empty();
            sel.append('<option value=""></option>');

            for (var i = 0; i < data.length; i++) {
                if(data[i].id==idg) {
                    sel.append('<option value="' + data[i].id + '" selected >' + data[i].id + '</option>');

                }
                else{
                    sel.append('<option value="' + data[i].id + '"  >' + data[i].id + '</option>');
                }
            }
         //   setTimeout(function(){ sel.selectpicker('refresh');},1000);

        }, "json");
}
function CRUDCONTROLEGRESO(o, id) {

    if (o == "AGREGARFACTURA")
    {
        var pre = $('#presupuesto').val();
        if (pre == "" || pre == null)
        {
            error('Seleccionar el presupuesto agregar factura. Verificar');
        }
        else
        {
            $.post('funciones/fnPresupuestoEgreso.php', {
                    opcion: 'AGREGARFACTURA',
                    pre: pre
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == 1)
                    {
                        //ok("Factura agregada correctamente");
                        var table = $('#tbfacturas').DataTable();
                        table.draw(false);
                        //setTimeout(ACTUALIZARFACTURAS(), 1000);
                       // CRUDCONTROLEGRESO('ANADIRFILA', data[0].id);
                    }
                }, "json");
        }
    }
    else
    if (o == "ANADIRFILA")
    {
        var totc = $('#totalceldas').val();
        var pre = $('#presupuesto').val();
        $.post('funciones/fnPresupuestoEgreso.php', {
                opcion: 'ANADIRFILA',
                pre: pre,
                totc: totc,
                id: id
            },
            function(data) {
                //data = $.trim(data);
                $("#tbfacturas tbody").append(data);

                //var totce = parseInt($('#totalceldas').val())+19;
                //$('#totalceldas').val(totce);
                var table = $('#tbfacturas').DataTable();


                //setTimeout(recorrer('#tbfacturas tbody tr'),1000);			  
                //setTimeout(recorrer('#tbfacturas tbody tr'),1000);
                //table.row('#'+id).draw( false );
                setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);

                //if(data==1)
                //{

                //  setTimeout(CRUDPRESUPUESTOINICIAL('CONTROLEGRESO','','','',''),1000);				 
                //}
            });
    }
    else
    if (o == "SELECCIONAR")
    {
        $('#myModalLabel').html("Seleccion de Presupuesto");
        $('#contenido').html('');
        $.post('funciones/fnPresupuestoEgreso.php', {
                opcion: 'CARGARLISTAPRESUPUESTO'
            },
            function(data) {
                $('#contenido').html(data);
            })
    }
    else
    if (o == "AGREGADOS") //REGISTRO DE NUEVA CIUDAD
    {

        $('#tabladatos').html("");
        $.post('funciones/fnPresupuestoEgreso.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#tabladatos').html(data);
            })

    }
    else
    if (o == "AGREGAR")
    {
        var idp = $('#presupuesto').val();
        var grupo = $('#grupo').val();
        var factura = $('#factura').val();
        var acta = $('#acta').val();
        var fecha = $('#fecha').val();
        var neto = $('#neto').val();
        var iva = $('#iva').val();
        var ret = $('#retencion').val();
        var amo = $('#amortizacion').val();
        var ord = $('#orden').val();
        var apro = $('#aprobado').val();
        var obs = $('#observacion').val();
        var doc = $('#documento').val();
        var ben = $('#beneficiario').val();
        var fechaf = $('#fechaf').val();

        if (idp == "" || idp == null) {
            error('Seleccionar el presupuesto');
        } else if (grupo == "" || grupo == null) {
            error('Seleccionar el grupo');
        } else if (factura == "" || factura == null) {
            error('Diligenciar el numero de la factura');
        } else if (fechaf == "" || fechaf == null) {
            error('Diligenciar la fecha de la factura');
        } else if (neto == "" || neto == null) {
            error('Diligenciar el valor neto');
        } else {
            $('#msn').html('');
            $.post('funciones/fnPresupuestoEgreso.php', {
                    opcion: o,
                    idp: idp,
                    grupo: grupo,
                    factura: factura,
                    acta: acta,
                    fechar: fecha,
                    neto: neto,
                    iva: iva,
                    ret: ret,
                    amo: amo,
                    orden: ord,
                    apro: apro,
                    obs: obs,
                    doc: doc,
                    ben: ben,
                    fechaf: fechaf
                },
                function(data) {
                    if (data[0].res == 1) {
                        ok('Factura Agregada Correctamente');
                        setTimeout(CRUDCONTROLEGRESO('AGREGADOS', idp), 1000);
                    } else
                    if (data[0].res == 2) {
                        error('Ya esta el grupo seleccionado con la misma factura en presupuesto actual. Verificar');
                    } else
                    if (data[0].res == 3) {
                        error('Ocurrio un error al agregar factura. Verificar');
                    } else
                    if (data[0].res == 4) {
                        error('El Total del saldo entre el total del valor bruto menos el total valor aprobado no puede ser menor a cero. Total Saldo:' + data[0].totalsaldo + '. Verificar');
                    }
                }, "json");
        }
    }
    else
    if (o == "CARGARGRUPOS")
    {
        var doc = $('#doc' + id).val();
        var gru = $('#gru' + id);
        var idp = $('#presupuesto').val();
        gru.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        $.post('funciones/fnPresupuestoEgreso.php', {
                opcion: o,
                doc: doc,
                pre: idp
            },
            function(data) {
                gru.empty();
                gru.append('<option value="" selected>--seleccione--</option>');
                //jAlert(data.length,null);
                for (var i = 0; i < data.length; i++) {
                    if (data.length == 1) {
                        gru.append('<option selected="selected" value="' + data[i].id + '">' + data[i].literal + '</option>');
                    } else {
                        gru.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                    }
                }
                INICIALIZARLISTAS('PRESUPUESTO');
                setTimeout(CRUDCONTROLEGRESO('ACTUALIZAR', id), 1000);

            }, "json");
    }
    else
    if (o == "ACTUALIZAR")
    {
    var idp = $('#presupuesto').val();
    var grupo = $('#gru' + id).val();
    var factura = $('#fac' + id).val();
    var acta = $('#act' + id).val();
    var fecha = $('#fec' + id).val();
    var neto = $('#net' + id).val();
    var neto1 = neto.replace("$", "");
    var neto2 = neto1.replace(",", "").replace(",", "");

    neto2 = neto2.replace(",", "").replace(",", "");
    neto = neto2;
    var iva = $('#iva' + id).val();
    var iva1 = iva.replace("$", "");
    var iva2 = iva1.replace(",", "").replace(",", "");
    iva2 = iva2.replace(",", "").replace(",", "");
    iva = iva2;
    var ret = $('#ret' + id).val();
    var ret1 = ret.replace("$", "");
    var ret2 = ret1.replace(",", "").replace(",", "");
    ret2 = ret2.replace(",", "").replace(",", "");
    ret = ret2;
    var amo = $('#amo' + id).val();
    var amo1 = amo.replace("$", "");
    var amo2 = amo1.replace(",", "").replace(",", "");
    amo2 = amo2.replace(",", "").replace(",", "");
    amo = amo2;
    var ord = $('#ord' + id).val();
    var apro = $('#apro' + id).val();
    var apro1 = apro.replace("$", "");
    var apro2 = apro1.replace(",", "").replace(",", "");
    apro2 = apro2.replace(",", "").replace(",", "");
    apro = apro2;
    var obs = $('#obs' + id).val();
    var doc = $('#doc' + id).val();
    var ben = $('#ben' + id).val();
    var fechaf = $('#fecf' + id).val();
    var anti = $('#anti' + id).val();
    var anti1 = anti.replace("$", "");
    var anti2 = anti1.replace(",", "").replace(",", "");
    anti2 = anti2.replace(",", "").replace(",", "");
    anti = anti2;
    // var ap = $('input:radio[name=apli'+id+']:checked').val();
    if (ord <= 0) {
        apro = 0;
        var ap = "no";
        $('#apro' + id).val(0);
    } else {
        var ap = "si";
    }
    //$('#msn2').html("");
    if (idp == "" || idp == null) {
        error('Seleccionar el presupuesto');
    }
    /* else if(grupo=="" || grupo==null)
   {
        $('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Seleccionar el grupo</div>');
   }
    else if(ben=="" || ben==null)
   {
       $('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Dilgenciar el beneficiario</div>');
   }
   else if(doc=="" || doc==null)
   {
       $('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Diligenciar el documento del beneficiario</div>');
   }
   else if(factura=="" || factura==null)
   {
        $('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Diligenciar el numero de la factura</div>');
   }
   else if(fechaf=="" || fechaf==null)
   {
        $('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Diligenciar la fecha de la factura</div>');
   }
   else if(neto=="" || neto==null)
   {
        $('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Diligenciar el valor neto</div>');
   }
   else if(ap=="1" && (ord==0 || ord=="" || ord==null ))
   {
      $('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Dilgenciar el número de orden. Verificar</div>');
   }*/
    /*|| apro==0 || apro=="" || apro==null*/
    else {
        $('#msn1').html('');
        $.post('funciones/fnPresupuestoEgreso.php', {
                opcion: o,
                idp: idp,
                grupo: grupo,
                factura: factura,
                acta: acta,
                fechar: fecha,
                neto: neto,
                iva: iva,
                ret: ret,
                amo: amo,
                orden: ord,
                apro: apro,
                obs: obs,
                doc: doc,
                ben: ben,
                fechaf: fechaf,
                anti: anti,
                apli: ap,
                cpe: id
            },
            function(data) {
                if (data[0].res == 1) {

                    $('#msn2').html('');
                    $('#saldo').html("$" + data[0].saldo);
                    $('#totneto').html("$" + data[0].totalneto);
                    $('#totiva').html("$" + data[0].totaliva);
                    $('#totbruto').html("$" + data[0].totalbruto);

                    $('#totretencion').html("$" + data[0].totalretencion);
                    $('#totanticipo').html("$" + data[0].totalanticipo);
                    $('#totamortizacion').html("$" + data[0].totalamo);
                    $('#totaprobado').html("$" + data[0].totalaprobado);
                    $('#totsaldo').html("$" + data[0].totalsaldo);
                    $('#est' + id).html(data[0].estado);
                    $('#bru' + id).html("$" + data[0].bruto);
                    $('#totinvertido').html("$" + data[0].totalinvertido);
                    var bor = data[0].borde;
                    if (bor == "si") {
                        $('#numorder' + id).css('border-color', 'rgba(255, 0, 0, 0.498039)').attr('title', 'Se excediendo del saldo presupuestal ');//.css('border-width', '2px').css('border-style', 'solid')
                        $('#saldo' + id).css('border-color', 'rgba(255, 0, 0, 0.498039)').attr('title', 'Se excediendo del saldo presupuestal ');//.css('border-width', '2px').css('border-style', 'solid')

                    }

                    var rep = data[0].rep;
                    if (rep == "si") {
                        $('#ord' + id).css('border-color', 'red').css('border-width', '2px').css('border-style', 'solid').attr('title', 'Esta orden de compra esta repetida verificar');
                        //actualizar saldo del anterior y va lor aprobado anterior
                        var sa = data[0].saldoa;
                        var ap = data[0].aproa;

                        if (sa < 0) {
                            $('#est' + data[0].cpea).html('<strong>Cerrada</strong>');
                        } else if (sa > 0) {
                            $('#est' + data[0].cpea).html('<strong>Abierta</strong>');
                        } else if (sa == 0) {
                            $('#est' + data[0].cpea).html('<strong>Cerrada</strong>');
                        }


                        $('#sal' + data[0].cpea).html(sa);
                        $('#sal' + data[0].cpea).css('color', 'black');

                        //}
                        $('#apro' + data[0].cpea).val(data[0].valoraa);
                        var rep1 = data[0].rep1;
                        if (rep1 == "si") {
                            error('El saldo de la factura con la misma orden diligenciada es negativo. Verificar');
                        }
                    } else {
                        $('#ord' + id).css('border-color', 'FFFFFF').css('border-width', 'none').css('border-style', 'none').attr('title', '');
                    }
                    //	alert(rep);
                    $('#apro' + id).val("$" + data[0].aproa);
                    var s = data[0].sal;
                    //if(s<0){ s = s*(-1); $('#sal'+id).html("("+s+")"); $('#sal'+id).css('color','red');}else{
                    $('#sal' + id).html(s);
                    //$('#sal'+id).css('color','black');}
                    //setTimeout(CRUDCONTROLEGRESO('AGREGADOS',idp),1000);
                    var colo = data[0].colo;
                    $('#doc' + id).parent('td').css('background-color', colo);
                    $('#ben' + id).parent('td').css('background-color', colo);
                    $('#ben' + id).val(data[0].ben);
                    $('#doc' + id).val(data[0].doc);

                    var idcpea = data[0].idcpea;
                    var valoaproa = data[0].valoaproa;
                    var salan = data[0].salan;
                    if (idcpea > 0) {
                        //console.log(salan+"-"+idcpea);
                        $('#apro' + idcpea).val("$" + valoaproa);
                        $('#sal' + idcpea).html(salan);
                        if (salan < 0) {
                            $('#est' + idcpea).html('<strong>Cerrada</strong>');
                        } else if (salan > 0) {
                            $('#est' + idcpea).html('<strong>Abierta</strong>');
                        } else if (salan == 0) {
                            $('#est' + idcpea).html('<strong>Cerrada</strong>');
                        }

                    }
                    if (ord <= 0 || ord == null || ord == '') {
                        $('#est' + id).html('<strong></strong>');
                    }
                    if (ord <= 0 || ord == null || ord == '') {
                        $('#sal' + id).html('<strong></strong>');
                    }
                    $('#gru' + id + '>option[value="' + data[0].gru + '"]').attr('selected', 'selected');
                    $('#gru' + id).selectpicker('refresh')
                    var ss = data[0].sals;
                    //console.log(ss);
                    var numfac = data[0].numfac;
                    var aprom = data[0].aprom;
                    if(ss<0 && numfac==1 && aprom==1 )
                    {
                        setTimeout(function () {
                            error("Orden de compra no tiene fondo disponible, generar ampliación 2");
                        },1000);
                    }
                    else
                    if(ss<0 && ord != "" && numfac>1){

                        setTimeout(function () {
                            error("Orden de compra no tiene fondo disponible, generar ampliación");
                        },1000);
                    }
                    setTimeout(CRUDCONTROLEGRESO('ACTUALIZARDATOS', ''), 2000);
                    setTimeout("$('.currency').formatCurrency({colorize: true})", 1000);
                    setTimeout(ACTUALIZARFACTURAS(), 1000);
                } else
                if (data[0].res == 2) {
                    error('Ya esta el grupo seleccionado con la misma factura en presupuesto actual. Verificar');
                    $('#msn2').html('');
                } else
                if (data[0].res == 3) {
                    error('Ocurrio un error al agregar factura. Verificar');
                    $('#msn2').html('');
                } else
                if (data[0].res == 4) {
                    error('El saldo entre el valor bruto menos el valor aprobado no puede ser menor a cero. Saldo:' + data[0].sal + '. Verificar');
                    $('#msn2').html('');
                }
            }, "json");
    }
}
    else
    if (o == "ELIMINAR")
    {
        var idp = $('#presupuesto').val();
        jConfirm('Realmente desea eliminar la factura de la lista?', 'Diálogo Confirmación LG', function(r) {
            //var conf = confirm("Realmente desea eliminar la factura de la lista");
            if (r == true) {
                $.post('funciones/fnPresupuestoEgreso.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        data = $.trim(data);
                        if (data == 1)
                        {
                            ok('Factura eliminada correctamente');
                            var table = $('#tbfacturas').DataTable();
                            table.row('#rowce_' + id).remove().draw(false);
                            //setTimeout(recorrer('#tbfacturas tbody tr'), 1000);
                            //$('#totalceldas').val($('#totalceldas').val()-18);
                            setTimeout(CRUDCONTROLEGRESO('ACTUALIZARDATOS', ''), 2000);
                            setTimeout(ACTUALIZARFACTURAS(), 1000);
                        }
                        else
                        {
                            error('Surgió un error a eliminar factura. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    }
    else
    if (o == "ACTUALIZARDATOS")
    {
        var idp = $('#presupuesto').val();
        $.post('funciones/fnPresupuestoEgreso.php', {
                opcion: o,
                idp: idp
            },
            function(data) {
                // jAlert(
                $('#saldo').html("$" + data[0].saldo);
                $('#totnetoe').html("$" + data[0].totalneto);
                $('#totivae').html("$" + data[0].totaliva);
                $('#totbrutoe').html("$" + data[0].totalbruto);
                $('#totretencione').html("$" + data[0].totalretencion);
                $('#totanticipoe').html("$" + data[0].totalanticipo);
                $('#totamortizacione').html("$" + data[0].totalamo);
                $('#totaprobadoe').html("$" + data[0].totalaprobado);
                $('#totsaldoe').html("$" + data[0].totalsaldo);
                $('#totinvertidoe').html("$" + data[0].totalinvertido);
            }, "json");

    }
    else
    if (o == "APLICA")
    {
        var ap = $('input:radio[name=apli' + id + ']:checked').val();
        if (ap == "0" || ap == "") {
            $('#ord' + id).val(0);
            $('#apro' + id).val(0);
            $('#ord' + id).attr('disabled', true);
            $('#apro' + id).attr('disabled', true);
        } else {
            $('#ord' + id).attr('disabled', false);
            $('#apro' + id).attr('disabled', false);
        }
        setTimeout(CRUDCONTROLEGRESO('ACTUALIZAR', id), 2000);
    }
}

function recorrer(id) {
    var cantidadfilas = $("" + id + "").length;
    //console.log("Cantidad Filas = " + cantidadfilas);
    var ind1 = 1;
    var ind2 = parseInt(cantidadfilas) * 19;
    $('#totalceldas').val(ind2)
    $("#tbfacturas tbody tr").each(function(index) {

        $(this).children("td").each(function(index2) {
            $(this).attr('tabindex', ind1);
            //console.log("Tab Index Actual: " + $(this).attr('tabindex'));
            // console.log("Index Td: " + $(this).index());
            $('#totalceldas').val(ind1)
            ind1++;

            /*switch (index2) 
            {
            	
            	case 0: campo1 = $(this).text();
            			break;
            	case 1: campo2 = $(this).text();
            			break;
            	case 2: campo3 = $(this).text();
            			break;
            }*/
            //$(this).css("background-color", "#ECF8E0");
        });

        //alert(campo1 + ' - ' + campo2 + ' - ' + campo3);
    })
}

function autoTabIndex(id, col) {
    //'#tbfacturas tbody tr'
    id += " tbody tr";
    var cantidadfilas = $("" + id + "").length;
    //console.log("Cantidad Filas = " + cantidadfilas);
    var ind1 = 1;
    var ind2 = parseInt(cantidadfilas) * col;
    $(id).each(function(index) {
        $(this).children("td").each(function(index2) {
            $(this).attr('tabindex', ind1);
            //console.log("Tab Index Actual: " + $(this).attr('tabindex'));
            //console.log("Index Td: " + $(this).index());
            ind1++;
        });
    })
}

function autoTabIndex2(id, col) {
    //'#tbfacturas tbody tr'
    id += " tbody tr";
    var cantidadfilas = $("" + id + "").length;
    //console.log("Cantidad Filas = " + cantidadfilas);
    var ind1 = 1;
    var ind2 = parseInt(cantidadfilas) * col;
    $(id).each(function(index) {
        $(this).children("td").each(function(index2) {
            $(this).attr('tabindex', 1000 + ind1);
            //console.log("Tab Index Actual: " + $(this).attr('tabindex'));
            //console.log("Index Td: " + $(this).index());
            ind1++;
        });
    })
}

function CONTROLESTADOS(o, id) {
    var idp = $('#presupuesto').val();
    if (o == "ELIMINAR") {

        jConfirm('Realmente desea eliminar el contrato de la lista?', 'Diálogo Confirmación LG', function(r) {
            if (r == true) {
                $.post('funciones/fnEstadoscontrato.php', {
                        opcion: o,
                        id: id
                    },
                    function(data) {
                        data = $.trim(data);
                        if (data == 1) {
                            ok('Contrato eliminado correctamente');
                            var table = $('#tbEstadoscontrato').DataTable();
                            table.row('#rowec_' + id).remove().draw(false);
                        } else {
                            error('Surgió un error a eliminar contrato. Verificar');
                        }
                    });
            } else {
                return false;
            }
        });
    } else if (o == "ACTUALIZAR") {

        var gru = $('#grue' + id).val();
        var ord = $('#ordc' + id).val();
        var cont = $('#cont' + id).val();
        var contra = $('#contra' + id).val();
        var nit = $('#nite' + id).val();
        var obje = $('#obje' + id).val();
        var vali = $('#vali' + id).val();
        var vali1 = vali.replace("$", "");
        var vali2 = vali1.replace(",", "").replace(",", "");
        vali2 = vali2.replace(",", "").replace(",", "");
        vali = vali2;
        var iv = $('#iv' + id).val();
        var iv1 = iv.replace("$", "");
        var iv2 = iv1.replace(",", "").replace(",", "");
        iv2 = iv2.replace(",", "").replace(",", "");
        iv = iv2;
        var valos = $('#valos' + id).val();
        var valos1 = valos.replace("$", "");
        var valos2 = valos1.replace(",", "").replace(",", "");
        valos2 = valos2.replace(",", "").replace(",", "");
        valos = valos2;
        var ivos = $('#ivos' + id).val();
        var ivos1 = ivos.replace("$", "");
        var ivos2 = ivos1.replace(",", "").replace(",", "");
        ivos2 = ivos2.replace(",", "").replace(",", "");
        ivos = ivos2;
        var dedu = $('#dedu' + id).val();
        var dedu1 = dedu.replace("$", "");
        var dedu2 = dedu1.replace(",", "").replace(",", "");
        dedu2 = dedu2.replace(",", "").replace(",", "");
        dedu = dedu2;
        var fei = $('#fei' + id).val();
        var fef = $('#fef' + id).val()
        var aseguc = $('#aseguc' + id).val();
        var valorc = $('#valorc' + id).val();
        var valorc1 = valorc.replace("$", "");
        var valorc2 = valorc1.replace(",", "").replace(",", "");
        valorc2 = valorc2.replace(",", "").replace(",", "");
        valorc = valorc2;
        var inicioc = $('#inicioc' + id).val();
        var finc = $('#finc' + id).val();

        var asegus = $('#asegus' + id).val();
        var valors = $('#valors' + id).val();
        var valors1 = valors.replace("$", "");
        var valors2 = valors1.replace(",", "").replace(",", "");
        valors2 = valors2.replace(",", "").replace(",", "");
        valors = valors2;
        var inicios = $('#inicios' + id).val();
        var fins = $('#fins' + id).val();

        var asegub = $('#asegub' + id).val();
        var valorb = $('#valorb' + id).val();
        var valorb1 = valorb.replace("$", "");
        var valorb2 = valorb1.replace(",", "").replace(",", "");
        valorb2 = valorb2.replace(",", "").replace(",", "");
        valorb = valorb2;
        var iniciob = $('#iniciob' + id).val();
        var finb = $('#finb' + id).val();

        var asegur = $('#asegur' + id).val();
        var valorr = $('#valorr' + id).val();
        var valorr1 = valorr.replace("$", "");
        var valorr2 = valorr1.replace(",", "").replace(",", "");
        valorr2 = valorr2.replace(",", "").replace(",", "");
        valorr = valorr2;
        var inicior = $('#inicior' + id).val();
        var finr = $('#finr' + id).val();

        var asegue = $('#asegue' + id).val();
        var valore = $('#valore' + id).val();
        var valore1 = valore.replace("$", "");
        var valore2 = valore1.replace(",", "").replace(",", "");
        valore2 = valore2.replace(",", "").replace(",", "");
        valore = valore2;
        var inicioe = $('#inicioe' + id).val();
        var fine = $('#fine' + id).val();

        var fechal = $('#fechal' + id).val();
        //jAlert(vali+" - "+iv);

       // $('#msn2').html("");

        $.post('funciones/fnEstadoscontrato.php', {
                ord: ord,
                cont: cont,
                contra: contra,
                nit: nit,
                obje: obje,
                vali: vali,
                iv: iv,
                valos: valos,
                ivos: ivos,
                dedu: dedu,
                fei: fei,
                fef: fef,
                aseguc: aseguc,
                valorc: valorc,
                inicioc: inicioc,
                finc: finc,
                asegus: asegus,
                valors: valors,
                inicios: inicios,
                fins: fins,
                asegub: asegub,
                valorb: valorb,
                iniciob: iniciob,
                finb: finb,
                asegur: asegur,
                valorr: valorr,
                inicior: inicior,
                finr: finr,
                asegue: asegue,
                valore: valore,
                inicioe: inicioe,
                fine: fine,
                fechal: fechal,
                id: id,
                pre: idp,
                gru: gru,
                opcion: o
            },
            function(data) {
                //jAlert(gru,null);
                if (data[0].res == "error1") {
                    error('El nit ingresado ya esta asociado a otro contratista. Verificar');
                } else if (data[0].res == "error2") {
                    error('Ya hay un contratista ingresado con el mismo grupo y contrato diligenciados. Verificar');
                } else if (data[0].res == "error3") {
                    error('Ocurrio un error en BD. No se guardaron los datos. Verificar');
                } else if (data[0].res == "error4") {
                    error('Las fechas iniciales no pueden ser mayores a las finales. Verificar');
                } else if (data[0].res == "ok") {
                    $('#nett' + id).html("$" + data[0].nett);
                    $('#salc' + id).html("$" + data[0].salc);
                    $('#vf' + id).html("$" + data[0].vf);
                    setTimeout("$('.currency').formatCurrency({colorize: true})", 1000);
                    setTimeout(ACTUALIZARFACTURAS(), 1000);
                }
            }, "json");
    }
}


function CRUDEJECUTIVO(o, id) {
    if (o == "SELECCIONAR") {
        $('#myModalLabel').html("Seleccion de Presupuesto");
        $('#contenido').html('');
        $.post('funciones/fnPresupuestoEjecutivo.php', {
                opcion: 'CARGARLISTAPRESUPUESTO'
            },
            function(data) {
                $('#contenido').html(data);
            })
    } else if (o == "AGREGADOS") //REGISTRO DE NUEVA CIUDAD
    {
        $('#tabladatos').html("");
        $.post('funciones/fnPresupuestoEjecutivo.php', {
                opcion: o,
                id: id
            },
            function(data) {
                $('#tabladatos').html(data);
            })

    } else if (o == "ACTUALIZAR") {
        var obs = $('#obs' + id).val();
        var idpr = $('#presupuesto').val();
        if (idpr == "" || idpr == null) {
            error('Debe seleccionar el presupuesto. Verificar')
        } else {
            $.post('funciones/fnPresupuestoEjecutivo.php', {
                    opcion: o,
                    idgrupo: id,
                    presupuesto: idpr,
                    obs: obs
                },
                function(data) {})
        }

    } else if (o == "ACTUALIZARGENERAL2") {
        var pre = $('#presupuesto').val();
        var avp = $('#avplan').val();
        var avr = $('#avreal').val();
        var codp = $('#codproyecto').val();
        var docp = $('#docproyecto').val();
        if (pre == "" || pre == null) {
            error('Debe seleccionar el presupuesto. Verificar');
        } else {
            $.post('funciones/fnPresupuestoEjecutivo.php', {
                    opcion: o,
                    codp: codp,
                    docp: docp,
                    avp: avp,
                    avr: avr,
                    pre: pre
                },
                function(data) {
                    var res = $.trim(data[0].res);
                    if (res == "ok") {
                        $('#atraso').html(data[0].atraso);
                    } else {
                        error("Surgio un error al actualizar porcentaje de avance. Verificar");
                    }

                }, "json");
        }

    } else if (o == "ACTUALIZARGENERAL") {

        var cdr = $('#cdreal').val();
        var cdr1 = cdr.replace("$", "");
        var cdr2 = cdr1.replace(",", "").replace(",", "");
        cdr2 = cdr2.replace(",", "").replace(",", "");
        cdr = cdr2;
        var cdc = $('#cdcomprometido').val();
        var cdc1 = cdc.replace("$", "");
        var cdc2 = cdc1.replace(",", "").replace(",", "");
        cdc2 = cdc2.replace(",", "").replace(",", "");
        cdc = cdc2;
        var eer = $('#eereal').val();
        var eer1 = eer.replace("$", "");
        var eer2 = eer1.replace(",", "").replace(",", "");
        eer2 = eer2.replace(",", "").replace(",", "");
        eer = eer2;
        var eec = $('#eecomprometido').val();
        var eec1 = eec.replace("$", "");
        var eec2 = eec1.replace(",", "").replace(",", "");
        eec2 = eec2.replace(",", "").replace(",", "");
        eec = eec2;
        var ier = $('#iereal').val();
        var ier1 = ier.replace("$", "");
        var ier2 = ier1.replace(",", "").replace(",", "");
        ier2 = ier2.replace(",", "").replace(",", "");
        ier = ier2;
        var iec = $('#iecomprometido').val();
        var iec1 = iec.replace("$", "");
        var iec2 = iec1.replace(",", "").replace(",", "");
        iec2 = iec2.replace(",", "").replace(",", "");
        iec = iec2;
        var ecr = $('#ecreal').val();
        var ecr1 = ecr.replace("$", "");
        var ecr2 = ecr1.replace(",", "").replace(",", "");
        ecr2 = ecr2.replace(",", "").replace(",", "");
        ecr = ecr2;
        var ecc = $('#eccomprometido').val();
        var ecc1 = ecc.replace("$", "");
        var ecc2 = ecc1.replace(",", "").replace(",", "");
        ecc2 = ecc2.replace(",", "").replace(",", "");
        ecc = ecc2;

        var avp = $('#avplan').val();
        var avr = $('#avreal').val();

        var codp = $('#codproyecto').val();
        var docp = $('#docproyecto').val();



        if (pre == "" || pre == null) {
            error('Debe seleccionar el presupuesto. Verificar');
        } else {
            $.post('funciones/fnPresupuestoEjecutivo.php', {
                    opcion: o,
                    cdr: cdr,
                    cdc: cdc,
                    eer: eer,
                    eec: eec,
                    ier: ier,
                    iec: iec,
                    ecr: ecr,
                    ecc: ecc,
                    codp: codp,
                    docp: docp,
                    avp: avp,
                    avr: avr,
                    pre: pre
                },
                function(data) {
                    $('#atraso').html(data[0].atraso);
                    $('#cdasignado').html(data[0].cdasignado);
                    $('#eeasignado').html(data[0].eeasignado);
                    $('#ieasignado').html(data[0].ieasignado);
                    $('#ecasignado').html(data[0].ecasignado);
                    $('#cddisponible').html(data[0].cddisponible);
                    $('#eedisponible').html(data[0].eedisponible);
                    $('#iedisponible').html(data[0].iedisponible);
                    $('#ecdisponible').html(data[0].ecdisponible);
                    $('#cddesviacion').html(data[0].cddesviacion);
                    $('#eedesviacion').html(data[0].eedesviacion);
                    $('#iedesviacion').html(data[0].iedesviacion);
                    $('#ecdesviacion').html(data[0].ecdesviacion);
                    $('#tplan').html(data[0].tplan);
                    $('#treal').html(data[0].treal);
                    $('#tcomprometido').html(data[0].tcomprometido);
                    $('#tasignado').html(data[0].tasignado);
                    $('#tdisponible').html(data[0].tdisponible);
                    $('#tvalidado').html(data[0].tvalidado);
                    $('#tactualizado').html(data[0].tactualizado);
                    $('#tdesviacion').html(data[0].tdesviacion);

                }, "json");
        }
    }
}

function guardaraiu(id, pre, gru, cap, ida, ins) {
    var adm = $('#adm' + pre + '_' + gru + '_' + cap + '_' + ida + '_' + ins).val();
    var imp = $('#imp' + pre + '_' + gru + '_' + cap + '_' + ida + '_' + ins).val();
    var uti = $('#uti' + pre + '_' + gru + '_' + cap + '_' + ida + '_' + ins).val();
    var iva = $('#iva' + pre + '_' + gru + '_' + cap + '_' + ida + '_' + ins).val();
    //if(adm=="" || adm==null){adm=0;}
    if (imp == "" || imp == null) {
        imp = 0;
    }
    if (uti == "" || uti == null) {
        uti = 0;
    }
    if (iva == "" || iva == null) {
        iva = 0;
    }
    //$('#span'+pre+'_'+gru+'_'+cap+'_'+ida+'_'+ins).html("<img alt='cargando' src='dist/img/ajax-loader.gif' height='15' width='15' />");

    $.post('funciones/fnPresupuestoInicial.php', {
            opcion: "GUARDARDETALLEAIU",
            id: id,
            pre: pre,
            gru: gru,
            cap: cap,
            adm: adm,
            imp: imp,
            uti: uti,
            iva: iva,
            act: ida,
            ins: ins
        },
        function(data) {
            if (data[0].res == 1) {
                //setTimeout(actualizarcambio(id,pre,gru,cap,ida,ins),2000);	
                $('#cambio' + id).html("$" + data[0].alcance);
                $('#equipo' + id).html("$" + data[0].equi);
                $('#mano' + id).html("$" + data[0].mano);
                $('#material' + id).html("$" + data[0].mate);
                $('#incp' + id).html(data[0].incp);
                setTimeout(actualizartotala(pre, gru, cap, data[0].acti, data[0].idda, ''), 2000);
                setTimeout(INICIALIZARTOOLTIP(), 1000);
				setTimeout(ACTUALIZARENLAZADO(pre),3000);
            } else if (data[0].res == 2) {}
        }, "json");
}
function guardaraiua( pre, ins) {
	 var tot = $('#total' + ins).html();
        var tot1 = tot.replace("$", "");
        var tot2 = tot1.replace(",", "").replace(",", "");
        tot2 = tot2.replace(",", "").replace(",", "");
        tot = tot2;
        var vra = $('#vra' + ins).val();
        var vra1 = vra.replace("$", "");
        var vra2 = vra1.replace(",", "").replace(",", "");
        vra2 = vra2.replace(",", "").replace(",", "");
        vra = vra2;
        var canta = $('#canta' + ins).val();
    var adm = $('#adm' + pre + '_' + ins).val();
    var imp = $('#imp' + pre + '_' + ins).val();
    var uti = $('#uti' + pre + '_' + ins).val();
    var iva = $('#iva' + pre + '_' + ins).val();
    //if(adm=="" || adm==null){adm=0;}
    if (imp == "" || imp == null) {
        imp = 0;
    }
    if (uti == "" || uti == null) {
        uti = 0;
    }
    if (iva == "" || iva == null) {
        iva = 0;
    }
    //$('#span'+pre+'_'+gru+'_'+cap+'_'+ida+'_'+ins).html("<img alt='cargando' src='dist/img/ajax-loader.gif' height='15' width='15' />");

    $.post('funciones/fnRecursos.php', {
            opcion: "GUARDARDETALLEAIU",           
            pre: pre,           
            adm: adm,
            imp: imp,
            uti: uti,
            iva: iva,           
            ins: ins,
			tot: tot,
            vra: vra,
            canta: canta,
        },
        function(data) {
            if (data[0].res == 1) {
                //setTimeout(actualizarcambio(id,pre,gru,cap,ida,ins),2000);	
                $('#subta' + ins).html("$" + data[0].totala);//totaladjudicador
                $('#ad' + ins).html("$" + data[0].totadm);
                $('#im' + ins).html("$" + data[0].totimp);
                $('#ut' + ins).html("$" + data[0].totuti);
				$('#iv' + ins).html("$" + data[0].totiva);
                //setTimeout(actualizartotala(pre, gru, cap, data[0].acti, data[0].idda, ''), 2000);
                setTimeout(INICIALIZARTOOLTIPADJ(), 1000);
				setTimeout(ACTUALIZARENLAZADO(pre),3000);
            } else if (data[0].res == 2) {}
        }, "json");
}

function CRUDPARTIDAS(o, id, gru, cap, act, ins, idd, por) {
    if (o == "SELECCIONARITEM") {
        var pre = $('#presupuesto').val();
        $('#btnseleccion').attr('disabled', false);
        $('#btnguardar').css('display', 'none');
        $('#myModalLabel').html("Indicar # de Item del cual se esta comprometiendo el recurso");
        $('#contenido2').html('');
        $.post('funciones/fnPartidas.php', {
                opcion: o,
                pre: pre,
                ins: ins,
                idp: id
            },
            function(data) {
                $('#contenido2').html(data);
            })
    } else
    if (o == "NUEVAPARTIDA") {
        var pre = $('#presupuesto').val();
        $.post("funciones/fnPartidas.php", {
                opcion: o,
                pre: pre
            },
            function(data) {
                var res = $.trim(data[0].res);
                var idp = $.trim(data[0].idp);
                if (res == 1) {
                    CRUDPARTIDAS('VERPARTIDA', idp, '', '', '', '', '');
                    setTimeout(CRUDPARTIDAS('ACTUALIZARPARTIDAS', idp, '', '', '', '', ''), 1000);
                } else {
                    error('Surgió un Error al Crear Nueva Partida Presupuestal. Verificar');
                    //jError("","Diálogo Error LG");
                    //return false;
                }
            }, "json");
    } else if (o == "VERPARTIDA") {
        $('#PARVERSELECCIONADO').val(0);
        var pre = $('#presupuesto').val();
        $('#divpartida').html('');
        $('#btnpartida').attr('title', id);
        $('#numpartida').html($('div.item.active.selected').html());
        $.post("funciones/fnPartidas.php", {
                opcion: o,
                pre: pre,
                idp: id
            },
            function(data) {
                $('#divpartida').html(data);
            });
    }
    else if(o=="MOSTRARPARTIDA")
    {
        var idp = $('#selpartida').val();
        $('#divverpatida').hide();
        $('#divnuepartida').show();
        $('#divdelpartida').show();
        $('#divselpartida').show();
        $('#divpartida').css('display','');
        $('#divconsolidado').css('display','none');
        $('#PARVERSELECCIONADO').val(0);

        var tabler = $('#tbPartidas').DataTable();
        tabler.draw('full-hold');
       // CRUDPARTIDAS('VERPARTIDA', idp, '', '', '', '', '');
    }
    else if (o == "ACTUALIZARPARTIDAS") {
        var lpar = $('#selpartida');
      //  $("#navpartidas div").remove(".item");
        //$('#numpartida').html($('div.item.active.selected').html());
		lpar.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
        var pre = $('#presupuesto').val();
        $.post("funciones/fnPartidas.php", {
                opcion: o,
                pre: pre,
                idp: id
            },
            function(data) {
				 lpar.empty();
				 var res = data[0].res;
				 if(res=="no"){

                 }
                 else {

                     for (var i = 0; i < data.length; i++) {
                         if (data[i].id == id) {
                             lpar.append('<option value="' + data[i].id + '" selected>' + data[i].literal + '</option>');
                         }
                         else {
                             lpar.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
                         }
                     }
                 }
               // $('#numpartida').html($('div.item.active.selected').html());
                setTimeout(function(){ lpar.selectpicker('refresh');}, 1000);
            }, "json");
    } else if (o == "SELECCIONARACTIVIDAD") {
        var pre = $('#presupuesto').val();
        var por = por;
        var ck = $('input:checkbox[name=ckseleccion' + idd + ']:checked').val();
        if (ck == 1) {
            ck = 1;
        } else {
            ck = 0;
        }
        if(id=="" || id<=0 || id==null)
        {
          error("Surgio un error. No hay partida seleccionada");
        }
        else {

            //console.log(ck);
            $.post('funciones/fnPartidas.php', {
                    opcion: o,
                    idp: id,
                    pre: pre,
                    gru: gru,
                    cap: cap,
                    act: act,
                    ins: ins,
                    idd: idd,
                    ck: ck,
                    por: por
                },
                function (data) {
                    var res = $.trim(data[0].res);
                    /* var ni = data[0].numite;
                     if (ni > 0) {
                     $('#check' + ins).addClass('fa-check');
                     $('#val' + ins).attr('disabled', false);
                     } else {
                     $('#check' + ins).removeClass('fa-check');
                     $('#val' + ins).attr('disabled', true);
                     }*/
                    //  $('#item' + ins).html(data[0].items);
                    //  $('#val' + ins).val(data[0].total);
                    //$('#val' + ins).attr('title', data[0].total);
                    $('.totales').html(data[0].totales);
                    if (res == "ok") {
                        $('#porcentaje').val(data[0].porcentaje);
                        var table = $('#tbPartidas').DataTable();
                        table.row('#rowpa_' + ins).draw(false);
                        //$('#cant_' + id + '_' + ins).html(data[0].cantdis);
                        var tabler = $('#tbrecursos2').DataTable();
                        //tabler.row("#row_rec"+ins).draw(false);
                        $('#totaladjr').html(data[0].totaladj);
                        $('#totalcor').html(data[0].totalcor);
                        $('#totalprer').html(data[0].totalpre);
                        $('#totaldisr').html(data[0].totaldispo);
                        setTimeout(INICIALIZARLISTAS('PRESUPUESTO'), 2000);
                    } else if (res == "error1") {
                        $('input:checkbox[name=ckseleccion' + idd + ']').attr('checked', false);
                        error("El total disponible del recurso ya fue comprometido");
                    } else if (res == "error2") {
                        error("ERROR AL ACTUALIZAR VALOR COMPROMETIDO. Verificar")
                    } else if (res == "error3") {
                        error("ERROR AL INSERTAR VALOR COMPROMETIDO. Verificar")
                    } else if (res == "error4") {
                        error("ERROR AL ASIGNAR ITEM SELECCIONADO. Verificar")
                    } else if (res == "error5") {
                        error("ERROR AL ELIMINAR VALOR COMPROMETIDO. Verificar")
                    } else if (res == "error6") {
                        error("ERROR AL ACTUALIZAR VALOR COMPROMETIDO. Verificar")
                    }
                    else if (res == "error7") {
                        error("EL ITEM YA FUE SELECCIONADO EN OTRA PARTIDA. Verificar")
                    }
                    else {
                        error('Surgió un error al seleccionar actividad. Verificar');
                    }
                }, "json");
        }
    }
    else if (o == "ACTUALIZARDATOS") {
        var nom = $('#nombrepartida').val();
        $.post('funciones/fnPartidas.php', {
                opcion: o,
                nom: nom,
                idp: id
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {} else {
                    error('Surgió un error al actualizar el nombre de la partida');
                }
            });
    } else if (o == "ACTUALIZAROBSERVACION") {
        var obs = $('#obs' + ins).val();
        $.post('funciones/fnPartidas.php', {
                opcion: o,
                obs: obs,
                idp: id,
                ins: ins
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {

                } else {
                    error('Surgió un error al actualizar el observación del recurso en la partida seleccionada');
                }
            });
    } else if (o == "ACTUALIZARVALOR") {
        var pre = $('#presupuesto').val();
        var valor = $('#val' + ins).val();
        var v1 = valor.replace("$", "");
        var v2 = v1.replace(",", "").replace(",", "");
        v2 = v2.replace(",", "").replace(",", "");
        valor = v2;

        $.post('funciones/fnPartidas.php', {
                opcion: o,
                val: valor,
                idp: id,
                ins: ins,
                pre: pre
            },
            function(data) {
                res = $.trim(data[0].res);
                if (res == "ok") {
                    // $('#tot'+ins).html(data[0].total);
                    $('#val' + ins).attr('title', valor);
                    $('#cant_' + id + '_' + ins).html(data[0].sumca);
                    $('.totales').html(data[0].totales);
                    setTimeout(function() { $('.currency').formatCurrency({ colorize: true }) } ,1000);
                } else if (res == "error1") {
                    error("El valor a comprometer no puede exceder el total del recurso");
                } else {
                    error('Surgió un error al actualizar el valor del recurso en la partida seleccionada');
                }
            }, "json");
    } else if (o == "ACTUALIZAROBSERVACIONCONSOLIDADO") {
        var obs = $('#obs' + id).val();
        $.post('funciones/fnPartidas.php', {
                opcion: o,
                obs: obs,
                idp: id
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {} else {
                    error('Surgió un error al actualizar el observación de la partida');
                }
            });
    } else if (o == "DELPARTIDA") {
        var pre = $('#presupuesto').val();
        jConfirm('Realmente desea eliminar la partida presupuestal seleccionada?', 'Diálogo Confirmación LG', function(r) {
            // var conf = confirm("Realmente desea eliminar el usuario seleccionado");
            if (r == true) {
                $.post('funciones/fnPartidas.php', {
                        opcion: o,
                        idp: id,
                        pre: pre
                    },
                    function(data) {
                        var res = $.trim(data[0].res);
                        if (res == "ok") {

                            ok("Partida eliminada correctamente");
                            if (data[0].idpa == 0) {
                                $('#divpartida').html('');
                                setTimeout(CRUDPARTIDAS('ACTUALIZARPARTIDAS', data[0].idpa, '', '', '', '', '', ''), 1000);
                            } else {
                                CRUDPARTIDAS('VERPARTIDA', data[0].idpa, '', '', '', '', '');
                                setTimeout(CRUDPARTIDAS('ACTUALIZARPARTIDAS', data[0].idpa, '', '', '', '', '', ''), 1000);
                            }
                        } else {
                            error('Surgió un error al eliminar la partida seleccionada. Verificar');
                        }
                    }, "json");
            } else {
                return false;
            }
        });
    } else if (o == "ACTUALIZARADJUDICADO") {
        var tot = $('#total' + ins).html();
        var tot1 = tot.replace("$", "");
        var tot2 = tot1.replace(",", "").replace(",", "");
        tot2 = tot2.replace(",", "").replace(",", "");
        tot = tot2;
        var vra = $('#vra' + ins).val();
        var vra1 = vra.replace("$", "");
        var vra2 = vra1.replace(",", "").replace(",", "");
        vra2 = vra2.replace(",", "").replace(",", "");
        vra = vra2;
        var canta = $('#canta' + ins).val();

        var pre = $('#presupuesto').val();
        //if(valo<=0){valo=0;}
        $.post('funciones/fnRecursos.php', {
                opcion: o,
                pre: pre,
                ins: ins,
                tot: tot,
                vra: vra,
                canta: canta,
                valor: 0
            },
            function(data) {
                var res = $.trim(data[0].res);
                if (res == 1) {} else {
                    error("Surgio un error al actualizar valor adjudicado. Verificar");
                }
                //var table = $('#tbrecursos2').DataTable();
                //table.row('#row_rec'+ins).draw(false);
                $('#dispo' + ins).html(data[0].dispo); //disponible
                $('#subta' + ins).html(data[0].subta); //subtotaladjudicado
                $('#dispop' + ins).html(data[0].dispop); //disponible
                //var totrec = $('.totalrec').attr('title');
                //var totrec2 = totrec.replace("$", "");
                //totrec = totrec2.replace(",","").replace(",","").replace(",","").replace(",",""); 		
                //$('.totaladj').html(data[0].totaladj);
                //$('.totalcom').html(data[0].totalcom);
                //console.log(data[0].totaladj);
                //if(data[0].totaladj>0)
                //{
                //	var di = totrec - data[0].totaladj;
                //$('.totaldis').html(di);
                //}
                //else
                //{
                //var di = totrec - data[0].totalcom;
                //$('.totaldis').html(di);
                //}
                //var dip = totrec - data[0].totalp;
                //$('.totalp').html(data[0].totalp);
                //$('.totaldisp').html(dip);
                $('#totaladjr').html(data[0].totaladj);
                $('#totalcor').html(data[0].totalco);
                $('#totalprer').html(data[0].totalpre);
                $('#totaldisr').html(data[0].totaldispo);
                setTimeout(function() { $('.currency').formatCurrency({ colorize: true }) } ,1000);
            }, "json");
    } else if (o == "ABRIRPARTIDA") {

        var pre = $('#presupuesto').val();
        $.post('funciones/fnPartidas.php', {
                opcion: o,
                idp: id
            },
            function(data) {
                data = $.trim(data);
                if (data == "ok") {
                    ok("Partida abierta correctamente");
                    setTimeout(CRUDPARTIDAS('VERPARTIDA', id, '', '', '', '', '', ''), 1000);
                } else {
                    error("Surgio un error al abrir partida . Verificar");
                }
            });
    } else if (o == "CERRARPARTIDA") {
        var nom = $('#nombrepartida').val();
        if (nom == "" || nom == null) {
            error("Diligenciar el nombre de la partida antes de cerrarla. Verificar");
        } else {
            $.post('funciones/fnPartidas.php', {
                    opcion: o,
                    idp: id
                },
                function(data) {
                    data = $.trim(data);
                    if (data == "ok") {
                        ok("Partida cerrada correctamente");
                        setTimeout(CRUDPARTIDAS('VERPARTIDA', id, '', '', '', '', '', ''), 1000);
                    } else {
                        error("Surgio un error al cerrar partida. Verificar");
                    }
                });
        }
    }
}


function ACTUALIZARENLAZADO(pre)
{
	$.post('funciones/fnPresupuestoInicial.php',{opcion:"ACTUALIZARPRESUPUESTOSENLAZADOS",preenlazado:pre},
	function(data){
			var res = data[0].res;
			if(res=="error1")
			{
				//console.log("Este presupuesto no esta enlazado a ninguno");
			}
			else if(res=="error2")
			{
				error("Surgio un error al actualizar valores enlazado")
			}
			else if(res=="ok")
			{
				//console.log("OK. Valores Enlazado a otros presupuestos enlazados");
			}		
		
		},"json");
	}
	
	
	
	function UpdateTableHeaders() {
       $(".persist-area").each(function() {
       
           var el             = $(this),
               offset         = el.offset(),
               scrollTop      = $(window).scrollTop(),
               floatingHeader = $(".floatingHeader", this)
           
           if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
               floatingHeader.css({
                "visibility": "visible"
               });
           } else {
               floatingHeader.css({
                "visibility": "hidden"
               });      
           };
       });
    }
	
	  function ajustar(id) {
        var texto=$('#'+id);
        var txt=texto.val();
        var tamano=txt.length;
        tamano*=7; //el valor multiplicativo debe cambiarse dependiendo del tamaño de la fuenteo
		if(tamano<100){tamano=100;}
        texto.css('width',tamano+"px");
    }