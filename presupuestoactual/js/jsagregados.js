$(document).ready(function(e) {
    
    var table = $('#tbgruagregados').DataTable( {       
         "columnDefs": 
		 [ 
		   { "targets": [7], "className": "dt-right" },
           { "targets": [8], "visible": false },
		   { "targets": [9], "visible": false },
		   { "targets": [10], "visible": false }          
         ],
		"ordering": false,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10,20,30 ], ["Todos",10,20,30 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Grupo Agregados",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		},
		"paging":false,
		"searching":false,
		fixedHeader: true	
    } );
    /* $('#tbgruagregados tfoot th').each( function () {
	var title = $('#tbgruagregados tfoot th').eq( $(this).index() ).text();
	if(title==""){$(this).html('');}else{
	$(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span></div>' );}
	} );*/
 
  
    // Apply the search
    /*table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );*/
	//
	$('#tbgruagregados tbody tr')
        .on( 'mouseover', function () {			
			$(this).css('background-color','');
			$(this).addClass('highlight');
        } )
        .on( 'mouseleave', function () {
			$(this).css('background-color','rgba(215,215,215,1.00);');
			$(this).removeClass('highlight');           
        } );
	
 
  // Add event listener for opening and closing details
    $('#tbgruagregados tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[8];
		var pre = $('#idedicion').val();
		//alert(pre +"-"+id)
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
		var table1 = "<div id='divchilg"+id+"' style='margin-left:20px;' ><table  class='table table-condensed compact' id='tbcapitulosg"+id+"' width='100%'>";
		table1+="<thead><tr>"+
		"<th class='dt-head-center' style='width:10px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'>CAP</th>"+
		"<th class='dt-head-center'>DESCRIPCIÓN</th>"+
		"<th class='dt-head-center'>UN</th>"+
		"<th class='dt-head-center'>CANT</th>"+
		"<th class='dt-head-center'>VR.UNIT</th>"+
		"<th class='dt-head-center'>VR.TOTAL</th>"+
		"<th class='dt-head-center'></th>"+
		"<th class='dt-head-center'></th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTACAPITULOS",pre:pre,gru:id},//CREAR FUNCION
			function(dato)
			{
				if(dato[0].res=="no")
				{
				
				}
				else
				{
				
					for(var i=0; i<dato.length; i++)
					{
						var cap = dato[i].capitulo;
						var cre = dato[i].cre;
						
				table1+="<tr class='trcapitulo' style='background-color:rgba(215,215,215,0.5)' id='cap"+dato[i].idd+"'>";
				table1+="<td class='details-control1' style='width:20px'></td>"+
				"<td style='vertical-align:middle'><a role='button' class='btn btn-block btn-default btn-xs' onClick=CRUDPRESUPUESTO('AGREGARACTIVIDAD',"+pre+",'"+id+"','"+cap+"','','')  style='width:20px; height:20px; '><i class='glyphicon glyphicon-plus' data-toggle='tooltip' title='Agregar Actividad'></i></a></td>"+
				"<td style='vertical-align:middle'><a role='button' class='btn btn-block btn-danger btn-xs' onClick=CRUDPRESUPUESTO('DELETEASIGNARCAPITULO',"+pre+",'"+id+"','"+cap+"','',"+dato[i].idd+")  style='width:20px; height:20px; '><i class='glyphicon glyphicon-trash' data-toggle='tooltip' title='Quitar Capitulo'></i></a></td>"+
				"<td data-title='CODIGO' style='width:85px'><span id='codc"+dato[i].idd+"'>"+dato[i].codc+"</span></td>"+
				"<td data-title='NOMBRE'>"+dato[i].nombre+"</td>"+
				"<td data-title='UN'></td>"+
				"<td data-title='CANT'></td>"+
				"<td data-title=''></td>"+
				"<td data-title='Valor Total($)'><div id='divtotcap"+dato[i].idd+"'>$"+dato[i].total+"</div></td>"+
				"<td>"+dato[i].idd+"</td>"+
				"<td>"+dato[i].capitulo+"</td>"+
				"</tr>";
		
					}
				}
				table1+="</tbody></table></div>";
				table1+="<div class='col-md-12'><input type='text' id='select"+pre+"g"+id+"' placeholder='CAPITULOS A AGREGAR'   onkeyup=filterGlobal('tbcapitulosagregadas"+id+"','select"+pre+"g"+id+"') name='pre_"+pre+"_gru_"+id+"'  class='form-control input-sm  capagregar'  style='width:100%'/></div>";
				row.child(table1).show();
				convertirdata2(id);
				//setTimeout(CRUDPRESUPUESTO('LLENARCAPITULOSAGREGAR',pre,id,'','',''),1000);
				/*<select id='select"+pre+"g"+id+"' data-placeholder='CAPITULOS A AGREGAR' onchange=CRUDPRESUPUESTO('ASIGNAR3','"+pre+"','"+id+"',this.value,'','')  class='form-control select2' style='width:100%' ></select>*/;
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
         }
    } );	

	
	   $("#tbgruagregados tbody").contextMenu({
        selector: 'tr', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idg = dat[8];
			var idd = dat[9];
			var pre  = dat[10];			
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTO('DELETE',pre,idg,'','',idd),1000);
			}	
			/*else if(key=="add")
			{
				setTimeout(CRUDPRESUPUESTO('AGREGARACTIVIDAD',pre,d,idc,'',''),1000);
			}*/
           // window.console && console.log(m) || alert(m); 
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            //"copy": {name: "Duplicar", icon: "copy"},
            //"paste": {name: "Paste", icon: "paste"},
			//"add": {name: "Agregar Actividad", icon: "add"},
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
/*});*/
       $(document).keyup(function(event){
        if(event.which==27)
        {
			
            $(".tooltipster-base").hide();
			// $("input").blur(); 
        }
    });
       $('.veraplica').tooltipster({
		        content:"",
				contentAsHTML: true,
				theme:'tooltipster-shadow',
				contentCloning: false,
				interactive:true,				
				position: 'top',
				multiple: true,
				restoration:'current',				
				maxWidth:$(this).width(),
				trigger: 'click',
				distance:1,
				offsetY:-12,
				arrow:false,				
				triggerClose: {
					click: true,
					tap: true
				},				
				functionBefore: function(origin, continueTooltip) {					
				continueTooltip();	
				var pre = $('#idedicion').val();
					// when the request has finished loading, we will change the tooltip's content
				$.post('funciones/fnPresupuesto.php',{opcion:'VERAPLICAIVA',pre:pre},
					function(data)
					{
						origin.tooltipster('content', 'APLICA IVA A:' + data);
					});		 
				
				   },				   
					functionAfter: function(origin) {
						//alert('The tooltip has closed!');
					}
				});

});
function agregargrupo()
{
	$('#panelgrupo').css('display','block');
	INICIALIZARLISTAS('PRESUPUESTO');
}
function ocultargrupos()
{
    $('#panelgrupo').css('display','none');
}
function filterGlobal (table,fil) {
	//console.log("Si entro"+table);
	$('#'+fil).keypress(function(e){
	    if(e.which==13)
		{
			$('#'+table).DataTable().search(
			$('#'+fil).val(),'',''
			).draw();
			setTimeout(function(){ $('#'+fil).val('');},3000);
			//$('#'+fil).val('')
		}
	});
}

function convertirdata2(d)
{
	var pre = $('#idedicion').val();
	
	$( "#select"+pre+"g"+d + " input" ).focus(function() {
	$( this ).parent( "td" ).addClass('focus');	
	var nam = $(this).attr('class');
			
		if(nam=="currency")
		{
			var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			var val = $(this).val()
			$(this).val(res1)
			if($(this).val()<0)
			{
			  $(this).val('');
			}
		}
		else
		{
		   if($(this).val()<0)
			{
			  $(this).val('');
			}
		}
	});

	$( "#select"+pre+"g"+ d + " input" ).focusout(function() {
	$( this ).parent( "td" ).removeClass('focus');
	 var nam = $(this).attr('class');
		 
		if(nam=="currency")
		{
			 if($(this).val()<0 || $(this).val()=="")
			 {
				$(this).val(0.00);
			 }		
		}
		else
		{
			$(this).val('');			 
		}	
	});
   
   var table = $('#tbcapitulosg'+d).DataTable(
   {   
       "columnDefs": 
		 [ 
		    { "targets": [0], "orderable" : true },
			{ "targets": [1], "orderable" : true, "visible" : false},
			{ "targets": [2], "orderable" : true, "visible" : false},
			{ "targets": [3], "visible" : true,"className" : "dt-left" , "className" : 'reorder'},
			{ "targets": [6], "className" : "dt-center" },
			{ "targets": [7], "className" : "dt-right" },
			{ "targets": [8], "className" : "dt-right" }, 
			{ "targets": [9], "visible" : false,} ,
			{ "targets": [10], "visible" : false} ,
			
			        
         ],
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Capitulos ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false,
		//"order": [[3, 'asc']],
        rowReorder: {
           selector: 'td:nth-child(2)'
        },
		fnDrawCallback: function() {
			$('#tbcapitulosg'+d+' thead').remove();
		}
    } );

    var table = $('#tbcapitulosg'+d).DataTable();
    table.on( 'row-reorder', function ( e, diff, edit ) {
        var ide = edit.triggerRow.data()[9]
        var result = 'Reorder started on row: '+edit.triggerRow.data()[0]+'<br>';
        var id1 = edit.triggerRow.data()[9];
        var id2 = 0;
        var id3 = 0;
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();
            if(i==0){id3 =diff[i].newData }
            id2 = diff[i].newData;
            //id1 =  diff[i].oldData;
            result += rowData[9]+' actualizo en posicion '+
                diff[i].newData +' (fue '+diff[i].oldData+')<br>';
        }
        if(id1>id3){id2=id3;}
        result+="ID1: "+id1 + "ID2:" + id2;
        //console.log(result);


        // $("#result"+pre+"g"+g+"c"+c).html( 'Event result:<br>'+result );
    } );
	 $('#tbcapitulosg'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var idc = dat[10];
		//console.log(idc);
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
		var table1 = "<span id='result"+pre+"g"+d+"c"+idc+"'></span>"+
		"<div id='divchila"+pre+"g"+d+"c"+idc+"' class='col-md-12' style='padding-left:30px'>"+		
		"<table width='100%'  class='table table-bordered table-condensed compact' id='tbactividadp"+pre+"g"+d+"c"+idc+"' style='font-size:11px'>";
		table1+="<thead><tr >"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px' >ITEM</th>"+
		"<th class='dt-head-center'>ACTIVIDAD</th>"+
		"<th class='dt-head-center' style='width:20px'>UN</th>"+
		"<th class='dt-head-center' style='width:20px'>CANT</th>"+
		"<th class='dt-head-center' style='width:100px'>VR.UNIT</th>"+
		"<th class='dt-head-center' style='width:100px'>VR.TOTAL</th>"+
		"<th class='dt-head-center'>CODIGO</th>"+
		"<th class='dt-head-center'>SUBA</th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAACTIVIDADES",cap:idc,pre:pre,gru:d},//CREAR FUNCION
			function(dato)
			{
				var res = dato[0].res;
				if(res=="no")
				{
				}
				else
				{					
					for(var i=0; i<dato.length; i++)
					{
					   var k = i+1;
					   if(k<10){ k = "0"+k}
					   var f = idc +"."+k;
						
					table1+="<tr id='"+dato[i].iddetalle+"'>"+
					"<td>"+dato[i].iddetalle+"</td>"+
					"<td class='details-control2'></td>"+
					"<td style='vertical-align:middle'>"+dato[i].DA+"</td>"+
					"<td  style='vertical-align:middle'>"+dato[i].DU+"</td>"+
					"<td  data-title='ITEM' style='width:50px'>"+dato[i].items+"</td>"+
					"<td data-title='ACTIVIDAD'>"+dato[i].actividad+"</td>"+
					"<td data-title='UN' style='width:80px'>"+dato[i].unidad+"</td>"+
					"<td  data-title='Cantidad' style='width:80px'>"+dato[i].cantidad+"</td>"+
					"<td data-title='Valor Unitario($)' class='currency' title='"+dato[i].apu+"' style='width:100px'>"+dato[i].apu+"</td>"+
					"<td data-title='Valor Total($)' title='"+dato[i].total+"'  style='width:80px'><div id='divtotact"+dato[i].iddetalle+"' class='currency'>"+dato[i].total+"</div></td>"+			
					"<td data-title='CODIGO'>"+dato[i].idactividad+"</td>"+	
					"<td data-title='SUBA'>"+dato[i].suba+"</td>"+	
					
					"</tr>";
		             
					}
				}
				table1+="</tbody></table>"+
				"</div>"+
				"<div class='col-md-12'><input type='text' id='select"+pre+"g"+d+"c"+idc+"' placeholder='ACTIVIDADES A AGREGAR'    name='pre_"+pre+"_gru_"+d+"_cap_"+idc+"'  class='form-control input-sm ' onkeyup=filterGlobal('tbactividadesagregadasp"+pre+"g"+d+"c"+idc+"','select"+pre+"g"+d+"c"+idc+"')  style='width:100%'/></div>";//actagregar
				//<select id='select"+pre+"g"+d+"c"+idc+"' data-placeholder='ACTIVIDADES A AGREGAR' onchange=CRUDPRESUPUESTO('ASIGNAR2','"+pre+"','"+d+"','"+idc+"',this.value,'')  class='form-control select2' style='width:100%' ></select>
				
				row.child(table1).show();
				convertiract(pre,d,idc);
				//setTimeout(autoTabIndex("#tbactividadp"+pre+"g"+d+"c"+idc,11),1000);
							
				//setTimeout(CRUDPRESUPUESTO('LLENARACTIVIDADAGREGAR',pre,d,idc,'',''),1000);
				//setTimeout(CRUDPRESUPUESTO('AGREGARACTIVIDAD2',pre,d,idc,'',''),2000);				
		
				setTimeout(INICIALIZARLISTAS(''),1000);
			    //setTimeout(CARGARACTIVIDADES('select'+pre+'g'+d+'c'+id),1000);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
         }
    });
	
	
		$('#tbcapitulosg'+d+' tbody tr')
        .on( 'mouseover', function () {			
			$(this).css('background-color','');
			$(this).addClass('highlight');
        } )
        .on( 'mouseleave', function () {
			$(this).css('background-color','rgba(215,215,215,0.5);');
			$(this).removeClass('highlight');           
        } );
		
		 $('.capagregar').tooltipster({	
		        content:"",
				contentAsHTML: true,
				theme:'tooltipster-shadow',
				contentCloning: false,
				interactive:true,
				offsetX: 0,
				offsetY: 0,				
				position: 'bottom',
				positionTracker: false,
				updateAnimation:'fade',
				multiple: true,							
				maxWidth:$(this).width(),
				trigger: 'click',
				distance:1,
				offsetY:-12,
				arrow:false,								
				functionBefore: function(origin, continueTooltip) {					
				continueTooltip();					
				
				var del = "_";
				var n = $(this).attr('name').split(del);
				var pre = n[1];
				var gru = n[3];	
				
				if (origin.data('ajax') !== 'cached') {
				 $.ajax({
					type: 'POST',
					url: 'funciones/fnPresupuesto.php',
					data: {opcion:'AGREGARCAP',pre:pre,gru:gru},
					success: function(data) {
					   // update our tooltip content with our returned data and cache it
					   origin.tooltipster('update', data ).data('ajax', 'cached');
					}
				 });
				}
					// when the request has finished loading, we will change the tooltip's content
				/*$.post('funciones/fnPresupuesto.php',{opcion:'AGREGARCAP',pre:pre,gru:gru},
					function(data)
					{						
						origin.tooltipster('content', '' + data);
					});	*/
				
				   },				   
				functionAfter: function(origin) {
					//content:""
					//alert('The tooltip has closed!');
					}
				});

	/*$(function(){*/
    $("#tbcapitulosg"+d+" tbody").contextMenu({
        selector: 'tr.trcapitulo', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idd = dat[9];
			var idc = dat[10];			
			if(key=="delete")
			{
				//console.log("Por aqui de nuevo");
				setTimeout(CRUDPRESUPUESTO('DELETEASIGNARCAPITULO',pre,d,idc,'',idd),1000);
			}	
			/*else if(key=="add")
			{
				setTimeout(CRUDPRESUPUESTO('AGREGARACTIVIDAD',pre,d,idc,'',''),1000);
			}*/
           // window.console && console.log(m) || alert(m); 
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            //"copy": {name: "Duplicar", icon: "copy"},
            //"paste": {name: "Paste", icon: "paste"},
			//"add": {name: "Agregar Actividad", icon: "add"},
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
/*});*/
}
// JavaScript Document
function convertiract(pre,g,c)
{

	
	$( "#tbactividadp"+pre+"g"+g+"c"+c+" input" ).focus(function() {
	$( this ).parent( "td" ).addClass('focus');
	var nam = $(this).attr('class');
	var tit = $(this).attr('name');	
		if($(this).val()<0 && tit!="busqueda")
		{
		$(this).val('');
		}
	});

$( "#tbactividadp"+pre+"g"+g+"c"+c+" input" ).focusout(function() {
$( this ).parent( "td" ).removeClass('focus');
 var nam = $(this).attr('class'); 
var tit = $(this).attr('name');
		 if(($(this).val()<0 || $(this).val()=="") && tit!="busqueda" )
		 {
			$(this).val('0.00');
		 }
		 else
		 {
			 
		 }
});
	//alert("Pre:"+pre+" Gru:"+g+" Cap:"+c);
   var table = $("#tbactividadp"+pre+"g"+g+"c"+c).DataTable(
   {   
       "columnDefs": 
		 [ 
		 { "targets":[0],"visible":false},
			{ "targets":[1],"orderable":false},
			{ "targets":[2],"orderable":false,"visible":false},
			{ "targets":[3],"orderable":false,"visible":false},
		 	{ "orderable" : true, "className" : 'reorder', "targets": [4], },           
			{ "targets": [7 ], "className" : "dt-center" },
			{ "targets": [8 ], "className" : "dt-right" },
			{ "targets": [9 ], "className" : "dt-right" } ,
			{ "targets": [10], "visible": false,"className": "dt-center" },
			{ "targets": [11], "visible": false,"className": "dt-center" },
		  ],
		 "searching":false,
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"paging":false,
		rowReorder: {
			selector: 'td:nth-child(2)'
			},
		fnDrawCallback: function() {
			$("#tbactividadp"+pre+"g"+g+"c"+c+" thead").remove();
			var k = 1;
			$("#tbactividadp"+pre+"g"+g+"c"+c+" tbody td").each(function () {
				$(this).attr('tabindex', k);
				k++;
			})
		}
    } );
	var table = $("#tbactividadp"+pre+"g"+g+"c"+c).DataTable();
	table.on( 'row-reorder', function ( e, diff, edit ) {
		 var ide = edit.triggerRow.data()[4]
        var result = 'Reorder started on row: '+edit.triggerRow.data()[0]+'<br>';
 		var id1 = edit.triggerRow.data()[0];
		var id2 = 0;
		var id3 = 0;
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();
			if(i==0){id3 =diff[i].newData } 
			id2 = diff[i].newData;			
			//id1 =  diff[i].oldData; 
			result += rowData[4]+' actualizo en posicion '+
			        diff[i].newData +' (fue '+diff[i].oldData+')<br>';
        }
		if(id1>id3){id2=id3;}
        result+="ID1: "+id1 + "ID2:" + id2;
		if(id1>0 && id2>0)
		{
		reordenaractividad(id1,id2,pre,g,c);
		}
		
     // $("#result"+pre+"g"+g+"c"+c).html( 'Event result:<br>'+result );
    } );
	//funci
	/*$("#select"+pre+"g"+g+"c"+c).keyup(function(e){
		//console.log("Por aca");
	    if(e.keyCode==13)
		{
			$('#tbactividadesagregadasp'+pre+'g'+g+'c'+c).DataTable().search($(this).val(),'','').draw();
			setTimeout(function() { $(this).val(''); },1000)
		}
	}).change(function(){
	       $('#tbactividadesagregadasp'+pre+'g'+g+'c'+c).DataTable().search($(this).val(),'','').draw();
		   setTimeout(function() { $(this).val(''); },1000)
	});*/
	
	 $('#tbactividadp'+pre+'g'+g+'c'+c+' tbody').on('click', 'td.details-control2', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var ida = dat[10];
		var idd = dat[0];
		var suba = dat[11];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown2');
        }
        else 
		{
			var id = tr.attr('id');
            // Open this row
			var table2 = "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+ida+"' title='ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDPRESUPUESTO('VERAPU','"+pre+"','"+g+"','"+c+"','"+ida+"','') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>";
			if(suba==1)
		   {
			table2 +="<li>"+
			"<a onclick=CRUDPRESUPUESTO('VERSUBANALISIS','"+pre+"','"+g+"','"+c+"','"+ida+"','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>";
		   }
		   table2+="</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body'><div id='divchilp"+pre+"g"+g+"c"+c+"a"+ida+"' class='col-md-12'><table style='font-size:11px'  class='table table-condensed compact' id='tbinsumosp"+pre+"g"+g+"c"+c+"a"+ida+"'>";
			table2+="<thead><tr>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"<th class='dt-head-center'>DESCRIPCION</th>"+
			"<th class='dt-head-center'>UND</th>"+
			"<th class='dt-head-center'>REND</th>"+
			"<th class='dt-head-center'>V/UNITARIO</th>"+
			"<th class='dt-head-center'>MATERIAL</th>"+
			"<th class='dt-head-center'>EQUIPO</th>"+
			"<th class='dt-head-center'>M.DE.O</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAINSUMOS",id:ida,pre:pre,gru:g,cap:c},
			function(dato)
			{
		       
				for(var i=0; i<dato.length; i++)
				{
					var cre = dato[i].cre;
					if(cre==1)
					{
						table2+="<tr class='delete1'>";
					}
					else
					{
					 	table2+="<tr>";
					}
					table2+="<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'> $"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right'> $"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td></tr>";
				}
				 
			table2+="</tbody></table></div>"+
			"<div class='col-md-12' style='display:none'><input type='text' id='selectp"+pre+"g"+g+"c"+c+"a"+ida+"' name='pre_"+pre+"_gru_"+g+"_cap_"+c+"_act_"+ida+"' class='form-control input-sm' style='width:100%' placeholder='AGREGAR RECURSOS'/></div>"+
			"</div></div>";
			row.child(table2).show();
			//setTimeout(convertirlistainsumos(pre,g,c,ida),1000);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown2');
        }
    });
	var currCell = $('#tbactividadp'+pre+'g'+g+'c'+c+' tbody td').first();
	var editing = false;
	
	// User clicks on a cell
	$('#tbactividadp'+pre+'g'+g+'c'+c+' td').click(function() {
	currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbactividadp'+pre+'g'+g+'c'+c+' tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell.closest('tr').prev().find('td:eq(' + 
		  currCell.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell.closest('tr').next().find('td:eq(' + 
		  currCell.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell = c;
		currCell.focus();
		currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
	}
	});
	
	$('#tbactividadp'+pre+'g'+g+'c'+c+' tbody tr')
        .on( 'mouseover', function () {			
			//$(this).css('background-color','');
			$(this).addClass('highlight');
        } )
        .on( 'mouseleave', function () {
			//$(this).css('background-color','rgba(215,215,215,0.5);');
			$(this).removeClass('highlight');           
        } );		

	     $("#select"+pre+"g"+g+"c"+c).tooltipster({
		        content:"",
				contentAsHTML: true,
				theme:'tooltipster-shadow',
				contentCloning: false,
				interactive:true,
				animation:'slide',	
				updateAnimation:'scale',								
				position: 'bottom',
				positionTracker: false,
				onlyOne:true,						
				maxWidth:1100,
				minWidth:1100,
				distance:1,
				offsetY:-12,
				arrow:false,
				trigger: 'click',						
				functionBefore: function(origin, continueTooltip) {					
				continueTooltip();	
				
				var del = "_";
				var n = $(this).attr('name').split(del);
				var pre = n[1];
				var gru = n[3];
				var cap = n[5];
				
				 // next, we want to check if our data has already been cached
				//if (origin.data('ajax') !== 'cached') {
				 $.ajax({
					type: 'POST',
					url: 'funciones/fnPresupuesto.php',
					data: {opcion:'AGREGARACTIVIDAD',pre:pre,gru:gru,cap:cap},
					success: function(data) {
						//console.log('Hola2');
						//var tabler = $('#tbactividadesagregadasp'+pre+'g'+g+'c'+c).DataTable();
					//tabler.destroy();
					   // update our tooltip content with our returned data and cache it
					   origin.tooltipster('update','<div id="divnueva">' +  data + '</div>').data('ajax', 'cached');
					}
				 });
				//}				
				
				   },				   
				functionAfter: function(origin) {
					//alert('The tooltip has closed!');
					//console.log("Hola");
					//$('#divnueva').html('');
					
					//origin.tooltipster();
				}
			});
		
	/*$(function(){*/
    $("#tbactividadp"+pre+"g"+g+"c"+c+" tbody").contextMenu({
        selector: 'tr', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idd = dat[0];
			var ida = dat[10];
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTO('DELETEASIGNAR',pre,g,c,'',idd),1000);
			}
			else
			if(key=="copy")
			{
				setTimeout(CRUDPRESUPUESTO('DUPLICARACTIVIDAD',pre,g,c,ida,idd),1000);
               
			}
           // window.console && console.log(m) || alert(m); 
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Duplicar", icon: "copy"},
            //"paste": {name: "Paste", icon: "paste"},
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
/*});*/
}

function convertirlistainsumos(pre,gru,cap,act)
{
   var table = $("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act).DataTable(
   {   
		"searching":false,
		"ordering": false,
		"info": false,
		"autoWidth": true,
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		},
		"order": [[1, 'asc']],
        "paging":         false,
		fnDrawCallback: function() {
			var k = 1;
			$("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act+" tbody td").each(function () {
				$(this).attr('tabindex', k);
				k++;
			})
		}
    } );
	var currCell = $("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act+" tbody td").first();
	var editing = false;
	var val = 0;
	
	// User clicks on a cell
	$("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act+" td").click(function() {
	currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	val = currCell.children('input').val();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act+" tbody").keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell.closest('tr').prev().find('td:eq(' + 
		  currCell.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell.closest('tr').next().find('td:eq(' + 
		  currCell.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell = c;
		currCell.focus();
		val = currCell.children('input').val();
		//currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {   
		   if(e.which==27)
		   {
			 currCell.children('input').val(val); 
			 //currCelli.focus();
		   }
		   else
		   {  
		      currCell.children('input').focus();
		         
		   }
		  // currCelli.children('input').val(String.fromCharCode(e.which))
	   }
	});
	
	  $("#selectp"+pre+"g"+gru+"c"+cap+"a"+act).tooltipster({
		content:"",
		contentAsHTML: true,
		theme:'tooltipster-shadow',
		contentCloning: false,
		interactive:true,
		animation:'slide',	
		updateAnimation:'scale',								
		position: 'bottom',
		positionTracker: false,
		onlyOne:true,						
		maxWidth:1100,
		minWidth:1100,
		distance:1,
		offsetY:-12,
		arrow:false,
		trigger: 'click',						
		functionBefore: function(origin, continueTooltip) {					
		continueTooltip();
				
		var del = "_";
		var n = $(this).attr('name').split(del);
		var pre = n[1];
		var gru = n[3];
		var cap = n[5];
		var act = n[7];
				
				 // next, we want to check if our data has already been cached
				if (origin.data('ajax') !== 'cached') {
				 $.ajax({
					type: 'POST',
					url: 'funciones/fnPresupuesto.php',
					data: {opcion:'AGREGARRECURSO',pre:pre,gru:gru,cap:cap,act:act},
					success: function(data) {
						var tabler = $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable();
					    tabler.destroy();
					   // update our tooltip content with our returned data and cache it
					   origin.tooltipster('update','<div id="divnuevorecurso">' +  data + '</div>').data('ajax', 'cached');
					}
				 });
				}				
				
				   },				   
				functionAfter: function(origin) {
					//alert('The tooltip has closed!');
					//console.log("Hola");
					
					//origin.tooltipster();
				}
			});
		
	/*$(function(){*/
    $("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act+" tbody").contextMenu({
        selector: 'tr.delete1', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idd = dat[0];
			var ida = dat[10];
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTO('DELETEASIGNAR',pre,g,c,'',idd),1000);
			}
			else
			if(key=="copy")
			{
				setTimeout(CRUDPRESUPUESTO('DUPLICARACTIVIDAD',pre,g,c,ida,idd),1000);
               
			}
           // window.console && console.log(m) || alert(m); 
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Duplicar", icon: "copy"},
            //"paste": {name: "Paste", icon: "paste"},
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
	
}


function convertirsub(pre,gru,cap,act)
{
	//console.log("Hola3");
    var table = $('#tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable({
		"columnDefs": 
		 [ 
			{ "targets": [2], "className": "dt-left" },
			{ "targets": [3], "className": "dt-center" },
			//{ "targets": [4], "className": "dt-center" },
			{ "targets": [5], "className": "dt-right" },
			//{ "targets": [6], "className": "dt-right" },			
			{ "targets": [4,6,7,8,9,10], "visible": false }  
			      
         ],
		"searching": false,
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,	
		"ajax": {
                    "url": "modulos/presupuesto/subanalisis_actividades.php",
                    "data": {id:act,pre:pre,gru:gru,cap:cap}
				},
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},				
			{ "data": "Codigo" },
			{ "data": "Nombre" },			
			{ "data": "Unidad" },
			{ "data": "Rendimiento" },
			{ "data": "Total",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data": "Totales",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data":  null,"defaultContent": ""},
			{ "data":  null,"defaultContent": ""},
			{ "data":  null,"defaultContent": ""},
			{ "data":  null,"defaultContent": ""}
		],
		"order": [[2, 'asc']]		
    } );
     
	
	$('table #tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act+' thead tr th').each(function(index,element){
	index += 1;
	$('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
    // DataTable
    var tables= $('#tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable();
 
    // Apply the search
   /* tables.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
*/
	 var detailRows1 = [];
 
    $('#tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act+' tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1s= "<div class='nav-tabs-custom' style='display:none'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDACTIVIDADES('VERAPU','"+id+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table1s+="<thead><tr><th>CODIGO</th><th>DESCRIPCION</th><th>UND</th><th>REND</th><th>V/UNITARIO</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAINSUMOSSUB",id:id,pre:pre,gru:gru,cap:cap,act:act},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table1s+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1s+="</tbody></table></div></div></div>";
				row.child(table1s).show();
				convertirdata(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
		tables.on( 'draw', function () {
			$.each( detailRows1, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
 

}

