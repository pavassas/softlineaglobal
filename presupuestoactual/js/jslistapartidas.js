/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {
    $('.currency').formatCurrency({colorize: true});

    $("#tbPartidas tbody input").focus(function() {
        //$( this ).parent( "td" ).addClass('focus');
        var nam = $(this).attr('class');
        var valo = $(this).attr('title');
        //console.log(valo);
        //sessionStorage.setItem('valor',valo);
        //console.log(sessionStorage.getItem('valor'));

        if(nam=="currency")
        {
			/*var str = $(this).val()
			 var res = str.replace("$", "");
			 var res1 = res.replace(",","").replace(",","");
			 res1 = res1.replace(",","").replace(",","");
			 var val = $(this).val()*/

            $(this).val(valo);


            if($(this).val()<0)
            {
                $(this).val('');
            }
        }
    });

$( "input" ).focusout(function() {
		var nam = $(this).attr('class');

		if(nam=="currency")
		{
			if($(this).val()<=0 || $(this).val()=="")
			{
				$(this).val(0.00);
			}
			$('.currency').formatCurrency({colorize: true});
		}

});


		var pre = $('#presupuesto').val();
		var par = $('#codpartida').val();
		var table2 = $('#tbPartidas').DataTable( {
            "columnDefs":
                [
                    { "targets": [0], "visible": false }
                ],
		"dom": '<"top"pl>rt<"bottom"><"clear">',		
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[15,20,30,-1 ], [15,20,30,"Todos" ]],

		//pageResize: true, 
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"decimal": ".",
        "thousands": ","
		},
		"processing": true,
        "serverSide": true,		
        "ajax":{
			"url":"modulos/presupuesto/partidasjson.php",
			"data":{pre:pre,par:par}
		},		
		"columns": [
            { "data" : "Creacion" },
			{ "data" : "Codigo" },
			{ "data" : "Nombre" },	
			{ "data" : "Unidad", "className": "dt-center" },	
			{ "data" : "CantP",  "className": "dt-center" },
			{ "data" : "CantC",  "className": "dt-center" },
			{ "data" : "Valor",  "className": "dt-right"  },
			{ "data" : "Totalc", "className": "dt-right"  },
            { "data" : "Items", "className": "dt-center"  },
            { "data" : "Ck", "className": "dt-center"  },
            { "data" : "Observacion", "className": "dt-right"  },


		],
		"order": [[0, 'asc'],[2, 'asc']],
		//fixedHeader: true,
		fnDrawCallback: function() {
			var k = 1;
			
			$("#tbPartidas tbody td").each(function () {
							
				$(this).attr('tabindex', k);				
				k++;
			})
		},
		 "fnFooterCallback": function ( row, data, start, end, display ) {
             /*var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
					//console.log(intVal(b));
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
					//console.log(b);
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                '$'+pageTotal +' ( $'+ total +' total)'
            );*/
        }
    } );
     
	 
	 
 	/*$('#tbfacturas tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );*/

	   var table2 = $('#tbPartidas').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );




    var currCell = $('#tbPartidas tbody td').first();
    var editing = false;

// User clicks on a cell
    $('#tbPartidas td').click(function() {
        currCell = $(this);
        //currCell.toggleClass("editing");
        //currCell.focus();
        currCell.children('input[type=text]').focus();
        //currCell.children('select').focus();
        //currCell.children('.select2').focus();
    });
// User navigates table using keyboard
    $('#tbPartidas tbody').keydown(function (e) {
        //console.log("Cantidad Celdas:"+tds);
        var c = "";
        if (e.which == 39) {
            // Right Arrow
            c = currCell.next();
        } else if (e.which == 37) {
            // Left Arrow
            c = currCell.prev();
        } else if (e.which == 38) {
            // Up Arrow
            c = currCell.closest('tr').prev().find('td:eq(' +
                currCell.index() + ')');
        } else if (e.which == 40) {
            // Down Arrow
            c = currCell.closest('tr').next().find('td:eq(' +
                currCell.index() + ')');
        }
        else if (!editing && (e.which == 9 && !e.shiftKey)) {
            // Tab
            e.preventDefault();
            c = currCell.next();
        } else if (!editing && (e.which == 9 && e.shiftKey)) {
            // Shift + Tab
            e.preventDefault();
            c = currCell.prev();
        }
        else if(!editing &&(e.which== 46))
        {
            var ide =  currCell.parents('tr').attr('id');
            //console.log("Eliminar:" + ide);

        }

        // If we didn't hit a boundary, update the current cell
        if (c.length > 0) {
            //console.log("Estamos en la celda: "+c.attr('tabindex'));

            currCell = c;
            //currCell.focus();
            //currCell.children('input[type=text]').focus();

            var allInputs = currCell.children(":input");
            var allSelects = currCell.children("select");
            if(allInputs.length>0)
            {
                currCell.children('input').focus();
                if(currCell.children('input').is(':disabled'))
                {
                    currCell.focus();
                }
                else
                {
                    currCell.children('input').focus();
                }
                //currCell.children(":input").val(sessionStorage.getItem('valor'))
            }
            else if(allSelects.length>0)
            {
                currCell.children('select').focus();
            }
            else
            {
                currCell.focus();
            }
            //currCell.children('select').focus();
            //INICIALIZARLISTAS('PRESUPUESTO');
        }
    });

});


function iniciar(cu,o)
{

    $('#tbPartidas tbody').on('keydown',function (e) {
        var c = cu;
        if (e.which == 39) {
            // Right Arrow
            o = 1;
            c = currCell.next();
        } else if (e.which == 37) {
            // Left Arrow
            o = 1;
            c = currCell.prev();
        } else if (e.which == 38) {
            // Up Arrow
            o = 1;
            c = currCell.closest('tr').prev().find('td:eq(' +
                currCell.index() + ')');
        } else if (e.which == 40) {
            // Down Arrow
            o = 1;
            c = currCell.closest('tr').next().find('td:eq(' +
                currCell.index() + ')');
        }
        else if (e.which == 9 && !e.shiftKey) {
            // Tab
            o = 1;
            e.preventDefault();
            c = currCell.next();
        } else if (!editing && (e.which == 9 && e.shiftKey)) {
            // Shift + Tab
            o = 1;
            e.preventDefault();
            c = currCell.prev();
        }
        // If we didn't hit a boundary, update the current cell
        if (c.length > 0) {
            //console.log("Estamos en la celda"+c.attr('tabindex'));
            var tab = c.attr('tabindex');
            currCell = c;
            var allInputs = c.children("input:enabled");
            if(allInputs.length>0 && o==1)
            {
                //currCell.children('input').focus();
               // console.log("Por aca");
                detener(c);
            }
            else
            {
                currCell.focus();
            }
        }
    });
}
function detener(cu)
{
    cu.children('input').focus();
    $('#tbPartidas tbody').off('keydown');
    currCell = cu;
    cu.children('input').on('keydown',function(e){
        if(e.keyCode==13)
        {
            var nam = $(this).attr('name');
            var del = "_";
            var n = nam.split(del);
            var ele = n[0];
            var idp = n[1];
            var ins = n[2];
            //
            cu = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
            if(cu.length>0)
            {
                currCell = cu;

            }
            // currCell.focus();
            //console.log(ins);
            if(ele=="val")
            {
                CRUDPARTIDAS('ACTUALIZARVALOR',idp,'','','',ins,'','');
            }
            else if(ele=="obs")
            {
                CRUDPARTIDAS('ACTUALIZAROBSERVACION',idp,'','','',ins,'','');
            }
            setTimeout(iniciar(cu,1),1000);
        }
        else if(e.keyCode==27)
        {
            currCell = cu;
            // currCell.focus();

            setTimeout(iniciar(cu,2),1000);

        }

    });
}
function filterColumnp (id, i ) {
	
	//console.log("Si entro"+table);
	$('#'+id).keypress(function(e){
	    if(e.which==13)
		{
			$('#tbPartidas').DataTable().column( i ).search( $('#'+id).val() ).draw();
			
			//$('#'+fil).val('')
		}
	});
	
    

    //$('#tbrecursos2').DataTable().column( i ).search( $('#'+id).val() ).draw();
	//console.log( $('#'+id).val());
}


