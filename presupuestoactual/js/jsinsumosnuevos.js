// JavaScript Documen
$(document).ready(function(e) {
      var table = $('#tbinsumosnuevos').DataTable(
      {  
	   "columnDefs": 
		 [ 		 
           { "targets": [5 ], "visible": false }          
         ],
	  "ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"order": [[1, 'asc']],
		"scrollY":        '50vh',
        "scrollCollapse": true,
        "paging": false
	  });
	  
	    $('#tbinsumosnuevos tfoot th').each( function () {
	var title = $('#tbinsumosnuevos tfoot th').eq( $(this).index() ).text();
	if(title==""){$(this).html('');}else{
	$(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></span></div>' );}
	} );
	
	  table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});