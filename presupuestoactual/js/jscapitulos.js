// JavaScript Document
$(document).ready(function(e) {
    

    var table = $('#tbcapitulos').DataTable( {       
     	"columnDefs": 
		 [ 
			{ "targets": [0], "orderable":false },
			{ "targets": [1], "orderable":false },
			       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10,20,30 ], ["Todos",10,20,30]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
        },
		"order":[[2,'asc']]
		
    } );
     
	  $('#tbcapitulos tfoot th').each( function () {
        var title = $('#tbcapitulos thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
 
    // DataTable
    var table = $('#tbcapitulos').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
    $('#tbcapitulos tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	// Add event listener for opening and closing details
 
});