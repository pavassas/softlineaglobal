
$(document).ready(function(e) {
    
    var insumo = $('#idinsumo').val();
	var por = $('#porcentaje').val();
	
    var table = $('#tbgruagregadosp').DataTable( {       
         "columnDefs": 
		 [ 
		  { "targets": [5 ], "className": "dt-right" },
           { "targets": [6 ], "visible": false }          
         ],
		"ordering": false,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10,20,30 ], ["Todos",10,20,30 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Grupo Agregados",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false		
    } );
    /* $('#tbgruagregados tfoot th').each( function () {
	var title = $('#tbgruagregados tfoot th').eq( $(this).index() ).text();
	if(title==""){$(this).html('');}else{
	$(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span></div>' );}
	} );*/
 
  
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	//
	
 
  // Add event listener for opening and closing details
    $('#tbgruagregadosp tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[6];
		var pre = $('#presupuesto').val();
		//alert(pre +"-"+id)
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
		var table1 = "<div id='divchilg"+id+"' class='col-xs-12'><table  class='table table-condensed compact' id='tbcapitulosgp"+id+"'>";
		table1+="<thead><tr>"+
		"<th class='dt-head-center' style='width:20px'></th>"+	
		"<th class='dt-head-center'>CAP</th>"+
		"<th class='dt-head-center'>DESCRIPCIÓN</th>"+
		"<th class='dt-head-center'>UN</th>"+
		"<th class='dt-head-center'>CANT</th>"+
		"<th class='dt-head-center'>VR.UNIT</th>"+
		"<th class='dt-head-center'>VR.TOTAL</th>"+
		"<th class='dt-head-center'></th>"+
		"<th class='dt-head-center'></th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPartidas.php',{opcion:"LISTACAPITULOS",pre:pre,gru:id,ins:insumo,porcentaje:por},//CREAR FUNCION
			function(dato)
			{
				if(dato[0].res=="no")
				{
				
				}
				else
				{
				
					for(var i=0; i<dato.length; i++)
					{
						var cap = dato[i].capitulo;
					table1+="<tr style='background-color:rgba(215,215,215,0.5)' id='cap"+dato[i].idd+"'>"+
					"<td class='details-control1'></td>"+					
					"<td data-title='CODIGO'>"+dato[i].codc+"</td>"+
					"<td data-title='NOMBRE'>"+dato[i].nombre+"</td>"+
					"<td data-title='UN'></td>"+
					"<td data-title='CANT'></td>"+
					"<td data-title=''></td>"+
					"<td data-title='Valor Total($)'><div id='divtotcap"+dato[i].idd+"'>$"+dato[i].total+"</div></td>"+
					"<td>"+dato[i].idd+"</td>"+
					"<td>"+dato[i].capitulo+"</td>"+
					"</tr>";
		
					}
				}
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata2(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
         }
    } );	

});


function convertirdata2(d)
{
	var por = $('#porcentaje').val();
   var pre = $('#presupuesto').val();
    var insumo = $('#idinsumo').val();
	var idpartida = $('#idpartida').val();
   var table = $('#tbcapitulosgp'+d).DataTable(
   {   
       "columnDefs": 
		 [ 
		    { "targets": [0], "className": "details-control1","orderable":false },
			{ "targets": [4], "className": "dt-center" },
			{ "targets": [5], "className": "dt-right" },
			{ "targets": [6 ], "className": "dt-right" }, 
			{ "targets": [7 ], "visible": false} ,
			{ "targets": [8 ], "visible": false} ,
			{ "targets": [1 ], "visible": true,"className": "dt-center" },
			         
         ],
		"ordering": false,
		"info": false,
		"autoWidth": true,
		"searching":false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Capitulos ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false,
		"order": [[1, 'asc']]
    } );
	
 
	 $('#tbcapitulosgp'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[8];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
		var table1 = "<div id='divchila"+pre+"g"+d+"c"+id+"' class='col-md-12'><table  class='display table table-condensed compact' id='tbactividadpa"+pre+"g"+d+"c"+id+"' style='font-size:11px'>";
		table1+="<thead><tr>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' >ITEM</th>"+
		"<th class='dt-head-center'>ACTIVIDAD</th>"+
		"<th class='dt-head-center' style='width:20px'>UN</th>"+
		"<th class='dt-head-center' style='width:20px'>CANT</th>"+
		"<th class='dt-head-center'>VR.UNIT</th>"+
		"<th class='dt-head-center'>VR.TOTAL</th>"+
		"<th class='dt-head-center'>CODIGO</th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPartidas.php',{opcion:"LISTAACTIVIDADES",cap:id,pre:pre,gru:d,ins:insumo,idp:idpartida,porcentaje:por},//CREAR FUNCION
			function(dato)
			{
				var res = dato[0].res;
				if(res=="no")
				{
				}
				else
				{				
					for(var i=0; i<dato.length; i++)
					{
					   var k = i+1;
					   if(k<10){ k = "0"+k}
					   var f = id +"."+k;
						
						table1+="<tr id='act"+dato[i].iddetalle+"'>"+
						"<td  class='details-control1'></td>"+
						"<td style='vertical-align:middle'>"+dato[i].ck+"</td>"+					
						"<td  data-title='ITEM'>"+dato[i].items+"</td>"+
						"<td  data-title='ACTIVIDAD'>"+dato[i].actividad+"</td>"+
						"<td  data-title='UN'>"+dato[i].unidad+"</td>"+
						"<td  data-title='Cantidad'>"+dato[i].cantidad+"</td>"+
						"<td  data-title='Valor Unitario($)'>$"+dato[i].apu+"</td>"+
						"<td  data-title='Valor Total($)'>$"+dato[i].total+"</td>"+			
						"<td data-title='CODIGO'>"+dato[i].idactividad+"</td>"+							
						"</tr>";             
						
					}
				}
				table1+="</tbody><tfoot></tfoot><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tfoot></table>"+
				"</div>";	
				row.child(table1).show();
				convertiract(pre,d,id);
				
			},"json");
            tr.addClass('shown1');
         }
    });
}
// JavaScript Document
function convertiract(pre,g,c)
{
	//alert("Pre:"+pre+" Gru:"+g+" Cap:"+c);
	 var insumo = $('#idinsumo').val();
	 var idpartida = $('#idpartida').val();
   var table = $("#tbactividadpa"+pre+"g"+g+"c"+c).DataTable(
   {   
       "columnDefs": 
		 [ 
			{ "targets": [5], "className": "dt-center" },
			{ "targets": [6 ], "className": "dt-right" },
			{ "targets": [7 ], "className": "dt-right" } ,
			{ "targets": [2], "visible": true,"className": "dt-center"} ,
			{ "targets": [8], "visible": false,"className": "dt-center" },
			{ "targets":[0],"orderable":false},
			{ "targets":[1],"orderable":false}
			
   
         ],
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"searching":false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"paging":false
    } );
	
	 $('#tbactividadpa'+pre+'g'+g+'c'+c+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var ida = dat[8];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
			var table2 = "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+ida+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDPRESUPUESTO('VERAPU','"+pre+"','"+g+"','"+c+"','"+ida+"','') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+ida+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+ida+"' ><div><table style='font-size:11px'  class='table table-condensed table-striped compact' id='tbinsumos"+ida+"'>";
			table2+="<thead><tr>"+
			"<th class='dt-head-center'>RECURSO</th>"+
			"<th class='dt-head-center'>UND</th>"+
			"<th class='dt-head-center info ' >REND</th>"+
			"<th class='dt-head-center info  '>V/UNIT</th>"+
			"<th class='dt-head-center info  '>MATERIAL</th>"+
			"<th class='dt-head-center info  '>EQUIPO</th>"+
			"<th class='dt-head-center info  '>M.DE.O</th>"+
                "<th class='dt-head-center danger' >REND.ACT</th>"+
                "<th class='dt-head-center danger'>V/UNIT.ACT</th>"+
                "<th class='dt-head-center danger'>MATERIAL</th>"+
                "<th class='dt-head-center danger'>EQUIPO</th>"+
                "<th class='dt-head-center danger'>M.DE.O</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAINSUMOS",id:ida,pre:pre,gru:g,cap:c,ins:insumo},
			function(dato)
			{
		       
				for(var i=0; i<dato.length; i++)
				{
					table2+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'> $"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right'> $"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].renact+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'> $"+dato[i].valoract+"</td>"+
					"<td data-title='Material' class='dt-right'> $"+dato[i].materiala+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipoa+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].manoa+"</td>"+
					"</tr>";
				}
				 
			table2+="</tbody></table></div></div></div>";
			row.child(table2).show();
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
        }
    });
	
}



function convertirsub(id)
{
    var table = $('#tbsubanalisis'+id).DataTable({
		
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,	
		"ajax": {
                    "url": "modulos/actividades/subanalisis_actividades.php",
                    "data": {id:id}
				},
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},				
			{ "data": "Codigo" },
			{ "data": "Nombre" },
			{ "data": "Tipo_Proyecto" },
			{ "data": "Unidad","className": "dt-center", },
			{ "data": "Ciudad" },
			{ "data": "Total", "className": "dt-right",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data": "Estado","className": "dt-center", "orderable": false,
			  "render": function ( data, type, full, meta ) {
					 if(data=="Inactivo")
					 {
					return '<span class="label label-warning ">'+data+'</span>';
					 }
					 else if(data=="Por Aprobar")
					 {
					 return '<span class="label label-info">'+data+'</span>';						 
					 }
					 else
					 {
					return '<span class="label label-success">'+data+'</span>'; 
					 }
   				 }
			},
			{ "data": "Analisis", "className": "dt-center", "orderable": false,
			   "render": function ( data, type, full, meta ) {
					 if(data=="No")
					 {
					return '<span class="label label-warning">No</span>';
					 }
					 else
					 {
					return '<span class="label label-success">Si</span>'; 
					 }
   				 } 
		    }
		],
		"order": [[2, 'asc']]		
    } );
     
	
	$('table #tbsubanalisis'+id+' thead tr th').each(function(index,element){
	index += 1;
	$('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
    // DataTable
    var tables= $('#tbsubanalisis'+id).DataTable();
 
    // Apply the search
    tables.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	 var detailRows1 = [];
 
    $('#tbsubanalisis'+id+' tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1s= "<div class='nav-tabs-custom' style='display:none'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDACTIVIDADES('VERAPU','"+id+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table1s+="<thead><tr><th>Nombre Recurso</th><th>Unidad Medida</th><th>Rendimiento</th><th>Valor Unitario($)</th><th>Material($)</th><th>Equipo($)</th><th>Mano de obra($)</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table1s+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1s+="</tbody></table></div></div></div>";
				row.child(table1s).show();
				convertirdata(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
		tables.on( 'draw', function () {
			$.each( detailRows1, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} ); 

}