function convertirdata3(d)
{
   var table = $('#tbactividad'+d).DataTable(
   {   
        
         "columnDefs": 
		 [ 
		   { "targets": [ 4 ], "className": "dt-center" },
		   { "targets": [ 5 ], "className": "dt-right" },
		   { "targets": [ 6 ], "className": "dt-right"},
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,-1 ], [10,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"order": [[2, 'asc']]
    } );
	
	 $('#tbactividad'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[1];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
			var table2 = "<div class='col-xs-1'></div><div id='divchil"+id+"' class='col-xs-11'><div class='table-responsive'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table2+="<thead><tr><th>Nombre Recurso</th><th>Unidad\nMedida</th><th>Rendimiento</th><th>Valor Unitario($)</th><th>Material($)</th><th>Equipo($)</th><th>Mano de obra($)</th></tr></thead><tbody>";
	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table2+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'>$"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right'>$ "+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td></tr>";
			   		APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
			table2+="</tbody></table></div></div>";
			row.child(table2).show();
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
        }
    });
}


function convertirdata(d)
{
   var table = $('#tbcapitulos'+d).DataTable(
   {   
         "columnDefs": 
		 [ 
		   { "targets": [ 3 ], "className": "dt-center" },
		   { "targets": [ 4 ], "className": "dt-right" },
		   { "targets": [ 5 ], "className": "dt-right"},
           { "targets": [ 6 ], "visible": false },
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,-1 ], [10,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Capitulos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		}
		
    } );
	
	  $('#tbcapitulos'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[6];
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
		var table1 = "<div class='col-xs-1'></div><div id='divchila"+id+"'  class='col-xs-11'><div class='table-responsive'><table  class='table table-condensed table-striped' id='tbactividad"+id+"'>";
		table1+="<thead><tr><th></th><th>Codigo</th><th>Actividad</th><th>Unidad</th><th>Cantidad</th><th>Valor Unitario($)</th><th>Valor Total</th></tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAACTIVIDADES",id:id},//CREAR FUNCION
			function(dato)
			{
				
				var sumapuc = 0;//sumade apu de capitulo
				for(var i=0; i<dato.length; i++)
				{
				table1+="<tr><td class='details-control1'></td>"+				
				"<td data-title='Codigo'>"+dato[i].idactividad+"</td>"+
				"<td data-title='ACTIVIDAD'>"+dato[i].actividad+"</td>"+
				"<td data-title='UN'>"+dato[i].unidad+"</td>"+
				"<td data-title='Cantidad'>"+dato[i].cantidad+"</td>"+
				"<td data-title='Valor Unitario($)'>$"+dato[i].apu+"</td>"+
				"<td data-title='Valor Total($)'>$"+dato[i].total+"</td></tr>";
	
				}
				table1+="</tbody></table></div></div>";
				row.child(table1).show();
				convertirdata3(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
         }
    } );
}
// JavaScript Document
$(document).ready(function(e) {
    
    $('.currency').formatCurrency({colorize: true});
    var table = $('#tbpresupuesto').DataTable( { 	      
          
		"columnDefs": 
		 [ 
		   { "targets": [ 6 ], "className": "dt-center" },
		   { "targets": [ 7 ], "className": "dt-center" },
		   { "targets": [ 8 ], "className": "dt-center" },
		   { "targets": [ 9 ], "className": "dt-center" },
		   { "targets": [ 10 ], "className": "dt-right" },
           { "targets": [ 11 ], "visible": false },
		    { "targets": [0 ], "visible": false },
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		}
		
    } );
     
	  $('#tbpresupuesto tfoot th').each( function () {
        var title = $('#tbpresupuesto thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
     
	 
    // DataTable
    var table = $('#tbpresupuesto').DataTable();

    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	//seleccion
   $('#tbpresupuesto tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
			var id = this.id;
			$('#presupuesto>option[value="'+id+'"]').attr('selected','selected');
			CRUDEJECUTIVO('AGREGADOS',id);
			setTimeout(INICIALIZARLISTAS('PRESUPUESTO'),1);
			 $('#myModal').modal('hide');
			
        }
    } );
  // Add event listener for opening and closing details
    $('#tbpresupuesto tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[11];
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
			var table1 = "<div class='col-xs-1'></div><div id='divchil"+id+"'  class='col-xs-11'><div class='table-responsive'><table  class='table table-condensed table-striped' id='tbcapitulos"+id+"'>";
			table1+="<thead><tr><th></th><th>CAP</th><th>UN</th><th>Cantidad</th><th>Valor Unitario($)</th><th>Valor Total</th><th>Codigo</th></tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTACAPITULOS",id:id},//CREAR FUNCION
			function(dato)
			{				
				for(var i=0; i<dato.length; i++)
				{
					table1+="<tr><td class='details-control1'></td>"+
					"<td data-title='CAP'>"+dato[i].capitulo+"</td>"+
					"<td data-title='UN'></td>"+
					"<td data-title='Cantidad'></td>"+
					"<td data-title='Valor Unitario($)'></td>"+
					"<td data-title='Valor Total($)'>$"+dato[i].total+"</td>"+
					
					"<td data-title='Codigo'>"+dato[i].iddetalle+"</td></tr>";
				}
				table1+="</tbody></table></div></div>";
				row.child(table1).show();
				convertirdata(id);
				},"json");
				//row.child(format(row.data()) ).show();
				tr.addClass('shown');
			}
	} );
	
	// Add event listener for opening and closing details
 
});
