// JavaScript Document
$(document).ready(function(e) {
	var val = "";
$( "input" ).focus(function() {
$( this ).parent( "td" ).addClass('focus');
	var nam = $(this).attr('class');
	if($(this).val()<0)
	{
		$(this).val('');
	}
});

$( "input" ).focusout(function() {
	$( this ).parent( "td" ).removeClass('focus');
	var nam = $(this).attr('class');
	if($(this).val()<=0 || $(this).val()=="")
	{
		$(this).val(0.00);
	}
});

    var table = $('#tbinsumospendientes').DataTable( {       
         "columnDefs": 
		 [ 
				
				{ "targets": [3 ], "className": "dt-center" },
				{ "targets": [4], "className": "dt-center" },
				{ "targets": [5 ], "className": "dt-right" },				
				          
         ],
		 "searching":false,
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Agregados",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,		
        "ajax": "modulos/actividades/insumopendientesjson.php",		
		"columns": [ 
			
			{   "orderable":      false,
				"data":           "Delete"
				
			},	
			{ "data": "Codigo" },
			{ "data": "Nombre" },
			{ "data": "Unidad" },
			{ "data": "Rendimiento"},
			{ "data": "Valor1" },	
			{ "data": "Tipo" }	
			
		],
		"order": [[2, 'asc']],
		fnDrawCallback: function() {
			var k = 1;
			$("#tbinsumospendientes tbody td").each(function () {
				$(this).attr('tabindex', k);
				k++;
			})

		}
		
    } );
     
 
    // DataTable
    var table = $('#tbinsumospendientes').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	// Add event listener for opening and closing details
var currCell = $('#tbinsumospendientes tbody td').first();
	var editing = false;
	var val = 0;
	
	// User clicks on a cell
	$('#tbinsumospendientes td').click(function() {
	currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	val = currCell.children('input').val();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbinsumospendientes tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell.closest('tr').prev().find('td:eq(' + 
		  currCell.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell.closest('tr').next().find('td:eq(' + 
		  currCell.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell = c;
		currCell.focus();
		val = currCell.children('input').val();
		//currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {   
		   if(e.which==27)
		   {
			 currCell.children('input').val(val); 
			 //currCelli.focus();
		   }
		   else
		   {   
		     currCell.children('input').focus();
		   }
	   }
	}); 
});