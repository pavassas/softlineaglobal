
function convertirdata(d)
{
   var table = $('#tbinsumos'+d).DataTable(
   {   
        "columnDefs": 
		 [ 
		 		
				{ "targets": [2 ], "className": "dt-right" },
				{ "targets": [3 ], "className": "dt-right" },
				{ "targets": [4], "className": "dt-right" },
				{ "targets": [5 ], "className": "dt-right" },
				{ "targets": [6 ], "className": "dt-right" },          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,-1 ], [10,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Recursos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		}
		
    } );
}
// JavaScript Document
$(document).ready(function(e) {
    
	var suba = $('#opcanalisis').val();
	var coor = $('#buscoordinador').val();
	var acti = $('#busactividad').val();
	var proy = $('#busproyecto').val();
    var selected = [];
    var table = $('#tbactividadesaprobar').DataTable({
		 "columnDefs": 
		 [ 
		 		{ "targets": [1,5 ], "visible":false },
				{ "targets": [11,12 ], "visible": false },
				       
         ],
		"dom": '<"top">rt<"bottom"pl><"clear">',
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"processing": true,
        "serverSide": true,
        "ajax": { 
		  	"url":"modulos/actividades/actividadesaprobarjson.php",
			"data":{suba:suba,coor:coor,acti:acti,proy:proy}  
			    },		
		"columns": [ 
			{
				"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},
			{
				
				"orderable":      false,
				"data":           "Editar",
				"render": function ( data, type, full, meta ) { var edi = "'DESAPROBARACTIVIDAD'";
      					return '<a class="btn btn-block btn-danger btn-xs" onClick="CRUDACTIVIDADES('+edi+','+data+')" style="width:20px; height:20px" title="No Aprobar Actividad"><i class="fa fa-ban"></i></a>';
   				 }
			},	
			{
				
				"orderable":      false,
				"data":           "Editar",
				"render": function ( data, type, full, meta ) { var edi = "'APROBARACTIVIDAD'";
      					return '<a class="btn btn-block btn-success btn-xs" onClick="CRUDACTIVIDADES('+edi+','+data+')" style="width:20px; height:20px" title="Aprobar Actividad"><i class="glyphicon glyphicon-check"  ></i></a>';
   				 }
			},		
			{ "data":"Coordinador"},
			{ "data":"Presupuesto" }, 
			{ "data": "Codigo" },
			{ "data": "Nombre" },			
			{ "data": "Unidad" },			
			{
				"data":           "Total", "className": "dt-right",
				"render": function (data, type, full, meta) {
					return data;	
   				 }
			},
			
			{ "data": "Tipo_Proyecto" },
			{ "data": "Ciudad" },
			{ "data": "Estado",
			   "render": function ( data, type, full, meta ) {
				         if(data=="Inactivo")
						 {
      					return '<span class="label label-warning pull-center">'+data+'</span>';
						 }
						 else if(data=="Por Aprobar")
						 {
						 return '<span class="label label-info pull-center">'+data+'</span>';						 
						 }
						 else
						 {
						return '<span class="label label-success pull-center">'+data+'</span>'; 
						 }
   				 }
			},
			{ "data": "Analisis",
			   "render": function ( data, type, full, meta ) {
				         if(data=="No")
						 {
      					return '<span class="label label-warning pull-right">No</span>';
						 }
						 else
						 {
						return '<span class="label label-success pull-right">Si</span>'; 
						 }
   				 } 
		    },
			{ "data": "Usuario"},
			{ "data": "Fecha"}
			
		],
		"order": [[4, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
		
		
		
    } );
     
	 $('#tbactividadesaprobar tfoot th').each( function () {
        var title = $('#tbactividadesaprobar thead th').eq( $(this).index() ).text();
		$(this).html('');
		//if(title==""){}else{
        //$(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
 
 $('table #tbactividadesaprobar thead tr th').each(function(index,element){
    index += 1;
    $('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
});
    // DataTable
    var table = $('#tbactividadesaprobar').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	 $('#tbactividadesaprobar tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	$('#btnguardar').click( function() 
	{
		var nam = $('#btnguardar').attr('name');
		if(nam=="GUARDARSELECCION")
		{
			if(selected == '')
			{
				$('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>No ha seleccionado ninguna actividad a aprobar.</div>'); 
				return false;
			}
			else{
			
				for(var i = 0; i<selected.length;i++)
				{
					
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
					  
						CRUDACTIVIDADES('APROBARACTIVIDAD',n[1]);	
					}
					
				
				}
				
				
			}
		}
		else
	    {
		    $('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Surgio un Error. Verificar</div>'); 
		}
	
    } ); 
	
	 var detailRows = [];
 
    $('#tbactividadesaprobar tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;
		
       // var idx = $.inArray( tr.attr('id'), detailRows );
		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide();
 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else {
			
            // Open this row
	var table1 = "<div id='divchil"+id+"'><div class='col-xs-1'></div><div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
	table1+="<thead><tr><th>Nombre Recurso</th><th>Unidad Medida</th><th>Rendimiento</th><th>Valor Unitario($)</th><th>Material($)</th><th>Equipo($)</th><th>Mano de obra($)</th></tr></thead><tbody>";
	      
	              
	$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
	function(dato)
	{
		var APU = 0;
		
			for(var i=0; i<dato.length; i++)
			{
			table1+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
			"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
			/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
			"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
			"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
			"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
			"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
			"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
			"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
			   APU+= Number(dato[i].total);
			}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
			table1+="</tbody></table></div></div>";
			row.child(table1).show();
			convertirdata(id);
		},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
        
            
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
	table.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
	
	$('#busactividad').keyup(function(e){
	     if(e.keyCode==13)
		 {
			 CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','');
		 }
	})
 
});
