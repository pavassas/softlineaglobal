// JavaScript Document
$(document).ready(function(e) {
	$('.currency').formatCurrency({colorize: true});

		var idp = $('#presupuesto').val()
		var table2 = $('#tbPagosActas').DataTable( {       
         "searching":true,
		"paging":false,
		//"ordering": false,
		"info": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10], [10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		
    } );
     
	var table2 = $('#tbPagosActas').DataTable(); 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	
	var currCell = $('#tbPagosActas tbody td').first();
var editing = false;

// User clicks on a cell
$('#tbPagosActas td').click(function() {
    currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
});
// User navigates table using keyboard
$('#tbPagosActas tbody').keydown(function (e) {
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
        e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
        e.preventDefault();
        c = currCell.prev();
    } 
    
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
        currCell = c;
        currCell.focus();
		currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
    }
});

 var clonedHeaderRow;
    
       $("#tbPagosActas .persist-area").each(function() {
           clonedHeaderRow = $(".persist-header", this);
           clonedHeaderRow
             .before(clonedHeaderRow.clone())
             .css("width", clonedHeaderRow.width())
             .addClass("floatingHeader");
             
       });
       
       $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");

});


