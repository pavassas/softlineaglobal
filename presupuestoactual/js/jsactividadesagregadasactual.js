/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {
  /*$('.close1').click(function() {
	$('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible');
	}); */
		var selected = [];
		var idca = $('#idcap').val();
		var gru  = $('#idgrup').val();
		var pre =  $('#presupuesto').val();
		/*var cod = $('#buscodigo').val();
		var des = $('#busdescripcion').val();
		var tpp = $('#bustipo').val();
		var uni = $('#busunidad').val();
		var ciu = $('#busciudad').val()*/
		var table2 = $('#tbactividadesagregadasp'+pre+'g'+gru+'c'+idca).DataTable( {       
          
		 "columnDefs": 
		 [ 		
           { "targets": [1], "visible": false } ,
		   { "targets": [2], "visible": false }        
         ],
		"dom": '<"top"i>rt<"bottom"p><"clear">',
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10], [10]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"sProcessing":'Buscando Datos'
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/act_agregadasjson.php",
                    "data": {idc: idca,gru:gru,pre:pre,opc:2/*,cod:cod,des:des,tpp:tpp,uni:uni,ciu:ciu*/}
				},
		"columns": [
			{
				
				"orderable":      false,
				"data":           "Editar",
				"render": function ( data, type, full, meta ) {  
      					return "<a class='btn btn-block btn-info btn-xs' onClick=CRUDPRESUPUESTO('ASIGNARACTUAL','"+pre+"','"+gru+"','"+idca+"','"+data+"','') style='width:20px; height:20px' title='Asignar Actividad'><i class='glyphicon glyphicon-plus'   ></i></a>";
   				 }
			},		
			{
				
				"orderable":      false,
				"data":           "Editar",
				"render": function ( data, type, full, meta ) {  
      					return "<a class='btn btn-block btn-warning btn-xs' onClick=CRUDPRESUPUESTO('DUPLICARACTIVIDAD','"+pre+"','"+gru+"','"+idca+"','"+data+"','') style='width:20px; height:20px' role='button' data-toggle='modal' data-target='#myModal' title='Duplicar Actividad' ><i class='glyphicon glyphicon-duplicate'   ></i></a>";
   				 }
			},			
			{ "data": "Codigo" },
			{ "data": "Nombre" },			
			{ "data": "Unidad","className":"dt-center" },
			{ "data":"Total", "className": "dt-right"},
			{ "data": "Tipo","className":"dt-center"  },
			{ "data": "Ciudad" }			
		],
		"order": [[3, 'asc'],[4, 'ASC']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        },
		//scrollY:        '35vh',
        //scrollCollapse: true,
        //paging:         false
		
    } );
     
	 $('#tbactividadesagregadasp'+pre+'g'+gru+'c'+idca+' tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	//filtros de busqueda
	
	 $('#tbactividadesagregadasp'+pre+'g'+gru+'c'+idca+' tfoot th').each( function () {
        var title = $('#tbactividadesagregadasp'+pre+'g'+gru+'c'+idca+' tfoot th').eq( $(this).index() ).text();
		//var width = 0;
		//if(title=="CODIGO"){width=40;}
		//console.log(title);
		if(title=="" || title=="VALOR APU"){$(this).html('');}else if($.trim(title)!="Nueva"){
        $(this).html( '<input type="text" style="width:100%"  placeholder="'+title+'" />' );}
    } );
	var table2 = $('#tbactividadesagregadasp'+pre+'g'+gru+'c'+idca).DataTable();
 
    // Apply the search
   table2.columns().every( function () {
        var that = this;
          $( 'input', this.footer() ).keyup( function (e) {
				if (e.keyCode == 13) {
							that
								.search( this.value )
								.draw();
								setTimeout(function() { this.value = ''; } ,1000);
								
				}
        } ).change( function () {				
							that
								.search( this.value )
								.draw();
								setTimeout(function() { this.value = '';  } ,1000);
												
        } );
      
        /*$( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );*/
    } );
	
	
	$('#asignaciones').click( function() 
	{
		var table = $('#tbactividadesagregadasp'+pre+'g'+gru+'c'+idca).DataTable();		
		var nums = table.rows('.selected').data().length;
		if(nums<=0)
		{
			error("No ha seleccionado ninguna actividad");
			return false;
		}
		else{
		
			for(var i = 0; i<selected.length;i++)
			{	
				if(selected[i]=="" || selected[i]==null)
				{
					
				}
				else
				{
					var id = selected[i];
					var n=id.split("row_");			      
					CRUDPRESUPUESTO('ASIGNARACTUAL',pre,gru,idca,n[1],'');	
				}
			}
			//selected.length = 0;
			selected.splice(0,selected.length)
			//setTimeout(actualizartotal('',idca),1000);	
			
		}
    } ); 
	
	/*$('body').keyup(function(e) {
       if(e.which===27){$('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible'); } // 13 is the keycode for the Escape key
	});*/	
	
});

function filterColumn (id, i ) {
    $('#tbactividadesagregadas').DataTable().column( i ).search( $('#'+id).val() ).draw();
	setTimeout(function() { $('#'+id).val(''); } ,1000);
	//console.log( $('#'+id).val());
}
	


