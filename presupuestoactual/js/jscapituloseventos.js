/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selectede = [];
	    var pre =  $('#presupuesto').val();	
		var table3 = $('#tbcapituloseventos').DataTable( { 
		"dom": '<"top"i>rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[4], [4 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"sProcessing":'Buscando Datos...'
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/cap_eventosjson.php",
                    "data": {pre:pre}
				},
		"columns": [
			{
				
				"orderable":      false,
				"data":           "Codigo",
				"render": function ( data, type, full, meta ) {
      					return "<a class='btn btn-block btn-default btn-xs' onClick=CRUDPRESUPUESTO('ASIGNARCAPITULOEVENTO','"+pre+"','','"+data+"','','') style='width:20px; height:20px' title='Asignar Actividad'><i class='glyphicon glyphicon-plus'></i></a>";
   				 }
			},			
			{ "data": "Codigo" },
			{ "data": "Nombre" }
			
		],
		"order": [[2, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selectede) !== -1 ) {
                $(row).addClass('selected');
            }
        },
		scrollY:        '30vh',
        scrollCollapse: true,
        paging:         false
		
    } );
     
	 $('#tbcapituloseventos tbody').on('click', 'tr', function () {
        var id = this.id;
		
        var index = $.inArray(id, selectede);
 
        if ( index === -1 ) {
            selectede.push( id );
        } else {
            selectede.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );	
	var table3 = $('#tbcapituloseventos').DataTable();
 
    // Apply the search
    table3.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	$('#asignacionescape').click( function() 
	{
		var table = $('#tbcapituloseventos').DataTable();		
		var nums = table.rows('.selected').data().length;
		if(nums<=0)
		{
			error("No ha seleccionado ningun capitulo");		
		return false;
		}
		else{
		
			for(var i = 0; i<selectede.length;i++)
			{	
				if(selectede[i]=="" || selectede[i]==null)
				{
					
				}
				else
				{
					var id = selectede[i];
					var n=id.split("row_");			      
					CRUDPRESUPUESTO('ASIGNARCAPITULOEVENTO',pre,'',n[1],'','');	
				}
			}
			//setTimeout(recalcularcapitulos(pre),1000);
			//setTimeout(actualizarcap(pre,gru),2000);		
			
		}
    } ); 
	
});
function filterColumne (id, i ) {
    $('#tbcapituloseventos').DataTable().column( i ).search(
        $('#'+id).val()
    ).draw();
}

