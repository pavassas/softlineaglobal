// JavaScript Document
$(document).ready(function(e) {
  
    var table2 = $('#tbpermisos2').DataTable( { 
	      "columnDefs": 
		 [ 		 
           { "targets": [2], "visible": false },		         
         ], 
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,5,10], ['Todos',5,10]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		}		
    } );
     
	  $('#tbpermisos2 tfoot th').each( function () {
        var title = $('#tbpermisos2 thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control input-sm" placeholder="'+title+'" /></div>' );}
    } );
 	$('#tbpermisos2 tbody').on('click','tr', function () {
        $(this).toggleClass('selected');
    } );
    // DataTable
    var table2 = $('#tbpermisos2').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	 $('#quitar').click( function() 
	{
		var idu = $('#idedicion').val();
        var table = $('#tbpermisos2').DataTable();
		
		var nums = table.rows('.selected').data().length;
		if(nums<=0)
		{
			error('No a seleccionado ninguna ventana a quitar');
			//alert("")
		}
		else
		{
			$("#tbpermisos2 tbody tr").each(function (index) 
			{
				if($(this).hasClass('selected'))
				{					
					var id = this.id
					
					CRUDPERFILES('ELIMINARPERMISO','',id);
					
				}
			});
			setTimeout(CRUDPERFILES('CARGARPERMISOS',idu,''),1000);
		
		}
	});
	$('#quitartodos').click( function() 
	{
		CRUDPERFILES('ELIMINARTODOS','','');
	})
	// Add event listener for opening and closing details
 
});