/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
	
	
 
  
}

function convertirdata(d)
{
   var table = $('#tbinsumos'+d).DataTable(
   {   
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,-1 ], [10,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Insumos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "Anterior","next":"siguiente"}
		}
		
    } );
}
// JavaScript Document
$(document).ready(function(e) {
    

    var table = $('#tbactividades').DataTable( {       
          
		
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": ""
		}
		
    } );
     
	  $('#tbactividades tfoot th').each( function () {
        var title = $('#tbactividades thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
 
    // DataTable
    var table = $('#tbactividades').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
    $('#tbactividades tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[2];
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
	var table1 = "<div id='divchil"+id+"'><div class='col-xs-1'><a class='btn btn-block btn-success' onclick='actualizar("+id+")' role='buttom'><i class='fa fa-refresh'></i></a></div><div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
	table1+="<thead><tr><th>Nombre Insumo</th><th>Unidad Medida</th><th>Rendimiento</th><th>Valor Unitario($)</th><th>Material($)</th><th>Equipo($)</th><th>Mano de obra($)</th></tr></thead><tbody>";
	      
	              
	$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id,pre:'',gru:'',cap:''},
	function(dato)
	{
		var APU = 0;
		
			for(var i=0; i<dato.length; i++)
			{
			table1+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
			"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
			"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
			"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' > "+dato[i].valor+"</div></td>"+
			"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>"+dato[i].material+"</div></td>"+
			"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>"+dato[i].equipo+"</div></td>"+
			"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>"+dato[i].mano+"</div></td></tr>";
			   APU+= Number(dato[i].total);
			}
			$('#divapu'+id).html(formato_numero(APU,2,'.',','));
			table1+="</tbody></table></div></div>";
			row.child(table1).show();
			convertirdata(id);
		},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	// Add event listener for opening and closing details
 
});
