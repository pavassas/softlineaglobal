/* Formatting function for row details - modify as you need */
var currCell = $('#tbApu tbody td').first();
var editing = false;
// JavaScript Document
$(document).ready(function(e) {
	
	$('.currency').formatCurrency({colorize: true});
	$( "#tbSubanalisis input" ).focus(function() {
		$( this ).parent( "td" ).addClass('focus');
		var nam = $(this).attr('class');
		if(nam=="currency")
		{
			var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			var val = $(this).val()
			$(this).val(res1)
			if($(this).val()<0)
			{
			  $(this).val('');
			}
		}
		else
		{
		   if($(this).val()<=0)
			{
			  $(this).val('');
			}
		}
	});

	$( "#tbSubanalisis input" ).focusout(function() {
		$( this ).parent( "td" ).removeClass('focus');
 		var nam = $(this).attr('class');
		if(nam=="currency")
		{
			 if(($(this).val()<=0 || $(this).val()==""))
			 {
				$(this).val(0.00);
			 }
			$('.currency').formatCurrency({colorize: true});
		}
		else
		{
			 if($(this).val()<=0 || $(this).val()=="")
			 {
				$(this).val(0.00);
			 }
		}	
	});
		var table2 = $('#tbSubanalisis').DataTable( {  
		"autoWidth": true,     
        "searching":false,
		"paging":false,
		"ordering": false,
		"info": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,15], ["Todos",15 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		//fixedHeader: true,
		fnDrawCallback: function() {
			var k = 1;
			$("#tbSubanalisis tbody td").each(function () {				
				$(this).attr('tabindex', k);				
				k++;
			})
		}		
    } );
     
	var table2 = $('#tbSubanalisis').DataTable();
	
	// User clicks on a cell
	$('#tbSubanalisis td').on('click',function() 
	{
		$('#tbSubanalisis tbody').on('keydown');
		currCell = $(this);		
		currCell.focus();
		//console.log("Estamos en la celda"+currCell.attr('tabindex'));		         

	});
	$('#tbSubanalisis td').on('dblclick',function() 
	{
		$('#tbSubanalisis tbody').on('keydown');
		currCell = $(this);
		//currCell.toggleClass("editing");
		//currCell.children('input').focus();		
		var allInputs = currCell.children(":input");
		if(allInputs.length>0)
		{				
			detener(currCell);		
		}
		else
		{
			currCell.focus();				
		}	
	});
	// User navigates table using keyboard

	$('#tbSubanalisis tbody').on('keydown',function (e) {
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (e.which == 9 && !e.shiftKey) { 
        // Tab
       // e.preventDefault();
        c = currCell.next();
    } else if (e.which == 9 && e.shiftKey) { 
        // Shift + Tab
       // e.preventDefault();
        c = currCell.prev();
    } 	    
    // If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
	
		currCell = c;			
		currCell.focus();
		//console.log("Estamos en la celda 1 - "+currCell.attr('tabindex'));
	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {   
		   if(e.keyCode==13)
	       {
			  //console.log("Estamos en la celda ENTER1 - "+currCell.attr('tabindex'));	
			  var allInputs = currCell.children(":input");
			  if(allInputs.length>0)
			  {
				  detener(currCell);
			  }
			  else
			  {
				  currCell.focus();
			  }
			  	
		   }
		   else
		   {   
		     
			  	         
		   }
	   }
	}); 
	 var clonedHeaderRow;
    
       $(".persist-area").each(function() {
           clonedHeaderRow = $(".persist-header", this);
           clonedHeaderRow
             .before(clonedHeaderRow.clone())
             .css("width", clonedHeaderRow.width())
             .addClass("floatingHeader");
             
       });
       
       $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
});

function iniciar(cu)
{
	$('#tbSubanalisis td').on('click',function() 
	{
		$('#tbSubanalisis tbody').on('keydown');
		currCell = $(this);		
		currCell.focus();
		//console.log("Estamos en la celda"+currCell.attr('tabindex'));		         

	});
	
	$('#tbSubanalisis tbody').on('keydown',function (e) {
    var c = cu;
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
       // e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
       // e.preventDefault();
        c = currCell.prev();
    }     
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
		//console.log("Estamos en la celda"+c.attr('tabindex'));
			currCell = c;
			currCell.focus();
			//console.log("Estamos en la celda 2 - "+currCell.attr('tabindex'));
    	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {   
		   if(e.keyCode==13)
	       {
			  detener(currCell);
			  //console.log("Estamos en la celda ENTER2 - "+currCell.attr('tabindex'));		
		   }
		   else
		   {   
		      //currCell.focus();
			 // console.log("Estamos en la celda 3- "+currCell.attr('tabindex'));		         
		   }
		  // currCelli.children('input').val(String.fromCharCode(e.which))
	   }
	});
}
function detener(cur)
{	
	cur.children('input').focus();
	$('#tbSubanalisis tbody').off('keydown');
	cur.children('input').on('keydown',function(e){
		var vala = $(this).attr('title');
		
		if(e.keyCode==27)
		{ 
			cur.children('input').val(vala);
			cur.children('input').off('keydown');
			currCell = cur;
			//currCell.focus();
			iniciar(currCell);
		}
		else
	    if(e.keyCode==13)
		{
		  var nam = $(this).attr('name');
		  var del = "_";
		  var n = nam.split(del);
		  var ele = n[0];
		  var idc = n[1]
		  var pre = n[2];
		  var gru = n[3];
		  var cap = n[4];
		  var act = n[5];
		  var ins = n[6];
		  //
		  cur.children('input').off('keydown');
		  cur = cur.closest('tr').next().find('td:eq(' + cur.index() + ')');
		  if(cur.length>0)
		  {
			  currCell = cur;
			  
		  }
		  //currCell.focus();
		  /*if(ele=="remren")
		  {
			  // console.log($('#remren'+idc).val());
			   guardarcantidadrenact(idc,pre,gru,cap,$('#remren'+idc).val(),act,ins);
		  }		  
		  else if(ele=="remval")
		  {
			 guardarvaloract(idc,pre,gru,cap,$('#remval'+idc).val(),act,ins)
		  }
		  else if(ele=="remrens")
		  {
			 guardarcantidadrenactsu(idc,pre,gru,cap,$('#remren'+pre+'g'+gru+'c'+cap+'a'+act+'s'+idc).val(),act,'')
		  }	*/	  
		  setTimeout(iniciar(cur),1000);
		}
		
		else if (e.keyCode == 38) { 
        // Up Arrow
          //cur = currCell.closest('tr').prev().find('td:eq(' +  currCell.index() + ')');
		  //currCell = cur;
		   setTimeout(iniciar(cur),1000);
    	} else if (e.keyCode == 40) { 

        // Down Arrow
          //cur= currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		  //currCell = cur;
		  setTimeout(iniciar(cur),1000);
   		} 
	});		
}