// JavaScript Document
$(document).ready(function(e) {
    

    var table = $('#tbgrupos').DataTable( {       
     
		"info": true,
		"paging":false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[ -1 ,10,20,30], ["Todos",10,20,30]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
        },
	    rowReorder: true
		
    } );
     
	  $('#tbgrupos tfoot th').each( function () {
        var title = $('#tbgrupos tfoot th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
 
    // DataTable
    var table = $('#tbgrupos').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
   $('#tbgrupos tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
	
	 table.on( 'row-reorder', function ( e, diff, edit ) {
		 var ide = edit.triggerRow.data()[3]
        var result = 'Reordenacion Comenzo en : '+edit.triggerRow.data()[4]+'id:'+ide+'<br>';
		
 
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();
 			ordenargrupo2(diff[i].oldData,diff[i].newData,ide);
            result += rowData[4]+' actualizo a posición '+
                diff[i].newData+' (fue '+diff[i].oldData+')<br>';
				//alert(diff[i].oldData+""+diff[i].newData)
				
        }
 
      $('#result').html( 'Event result:<br>'+result );
    } );
	//funcion para subir y bajar filas
	 $(".up,.down").click(function () {
               
         var $element = this;
         var tr = $($element).parents("tr:first");
         // var arr = $('#tbgrupos tbody tr.selected'); 
		  // for(var i=0; i<arr.length; i++) 
		   //{            
				//var tr = arr[i]; 
				var row = $(tr); 
			
				var prevRow;
				
				var id1 = "";
				var id2 = "";
				if($(this).is('.up')){
				prevRow = $(tr).prev();
				var n = row.attr('id').split("row_gru");
				id1 = n[1];
				var n2 = prevRow.attr('id').split("row_gru");
				id2 = n2[1];
				row.insertBefore(row.prev());
				
				}				
				else{
				
				prevRow = $(tr).next();
				var n = row.attr('id').split("row_gru");
				id1 = n[1];
				var n2 = prevRow.attr('id').split("row_gru");
				id2 = n2[1];
				row.insertAfter(row.next());
				}
				ordenargrupo(id1,id2);
				//if(prevRow.length==0){  break; } 
				
		// }
       
    });
 
});