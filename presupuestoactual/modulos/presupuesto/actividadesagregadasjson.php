<?php
include("../../data/Conexion.php");
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$table = 'actividades';
$pre = $_GET['pre'];
$gru = $_GET['gru'];
$cap = $_GET['cap'];
// Table's primary key
$primaryKey = 'd.asp_clave_int';//'act_clave_int'

$columns = array(
	array(
		'db' => 'd.asp_clave_int',
		'dt' => 'DT_RowId', 'field' => 'asp_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_actp'.$d;
		}
	),
	array( 'db' => 'd.asp_clave_int', 'dt' => 'Delete', 'field' => 'asp_clave_int','formatter'=>function($d,$row){
	   return "<a role='button' class='btn btn-danger btn-xs' style='width:20px; height:20px' onClick=CRUDPRESUPUESTO('DELETEPENDIENTESUBPRESUPUESTO','','','','','".$d."')><i class='fa fa-trash'></i></a>";	
	}),
	array( 'db' => 'd.asp_rendimiento', 'dt' => 'Rendimiento', 'field' => 'asp_rendimiento' ),
	array( 'db' => 'a.act_nombre', 'dt' => 'Nombre', 'field' => 'act_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => 't.tpp_nombre ','dt'=>'Tipo', 'field' => 'tpp_nombre'),
	array( 'db'  => 'c.ciu_nombre','dt' => 'Ciudad', 'field' => 'ciu_nombre' )
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = 'd.asp_clave_int';
 $joinQuery = " FROM  actividades a join act_sub_pen_presupuesto d on d.act_clave_int = a.act_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join unidades  u on u.uni_clave_int  = a.uni_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int";
$extraWhere = " d.usu_clave_int = '".$idUsuario."' and d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."'";
   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

