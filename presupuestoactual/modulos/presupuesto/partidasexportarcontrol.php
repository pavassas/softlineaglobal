<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idpresupuesto = $_GET['edi'];
include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('safe_mode', 0);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}
//DATOS DEL PRESUPUESTO
$con  = mysql_query("select UPPER(per_nombre) nom,UPPER(per_apellido)  ape,UPPER(per_razon) cli, per_documento,pre_nombre,pre_fecha,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_usu_creacion,pre_cliente,tpp_clave_int,date_format(pre_fecha,'%Y%m%d') as fec,pre_apli_iva,pre_coordinador as cor,pre_av_plan,pre_av_real,pre_documento  from presupuesto pr left outer join persona p on p.per_clave_int = pr.per_clave_int where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$fech = $dat['fec'];
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$nomo = $dat['pre_nombre'];
$fechai  =$dat['pre_fecha'];
$apli = $dat['pre_apli_iva'];
$tpp = $dat['tpp_clave_int'];
$precliente = $dat['pre_cliente'];
$avplan = $dat['pre_av_plan'];
$avreal = $dat['pre_av_real'];
$atraso = $avplan - $avreal;
$codproyecto = $dat['pre_codigo'];
$docproyecto = $dat['pre_documento'];
$nomcliente = $dat['cli'];
$cor = $dat['cor'];
$cont = mysql_query("select tpp_nombre from tipoproyecto where tpp_clave_int = '".$tpp."'");
$datt =  mysql_fetch_array($cont);
$tppr =  strtoupper($datt['tpp_nombre']);

if($precliente!=""){$cliente=$precliente;}else { $cliente = $nomcliente." - NIT: ".$dat['per_documento'];}
$creado = $dat['pre_usu_creacion'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$creado."' limit 1");
$dat = mysql_fetch_array($con);
$creadopor = $dat['usu'];
$car  = $dat['car_clave_int'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$cor."' limit 1");
$dat = mysql_fetch_array($con);
$aprobadopor = $dat['usu'];
$carc  = $dat['car_clave_int'];

$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$car."'");
$datc = mysql_fetch_array($conc);
$cargo = $datc['car'];
$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$carc."'");
$datc = mysql_fetch_array($conc);
$cargoc = $datc['car'];

$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysql_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

$totadm = ($totalc * $adm)/100;
$totimp = ($totalc * $imp)/100;
$totuti = ($totalc * $uti)/100;
if($apli==0){ $totiva = ($totuti * $iva)/100; }else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100; }
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$adma = $dat['pri_administracion'];
$ivaa = $dat['pri_iva'];
$impa = $dat['pri_imprevisto'];
$utia = $dat['pri_utilidades'];

$totadma = ($totalca * $adma)/100;
$totimpa = ($totalca * $impa)/100;
$totutia = ($totalca * $utia)/100;
if($apli==0) { $totivaa = ($totutia * $ivaa)/100; } else { $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;

$alccos = $totalc - $totalca;
$alcpre = $totpre - $totprea;
$alcadm = $totadm - $totadma;
$alcimp = $totimp - $totimpa;
$alcuti = $totuti - $totutia;
$alciva = $totiva - $totivaa;



/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);

// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => array('argb' => 'FF993300'),
        ),
    ),
);

// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
    ->setLastModifiedBy("Pavas.co")
    ->setTitle("Informe Presupuesto")
    ->setSubject("Informe Presupuesto")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Informes");
$conpartidas = mysql_query("select par_clave_int,par_nombre,par_estado,par_tipo from partidas where pre_clave_int = '".$idpresupuesto."' order by par_tipo ASC,par_clave_int ASC");
$numpartidas = mysql_num_rows($conpartidas);
for($np=0;$np<$numpartidas;$np++) {
    $datp = mysql_fetch_array($conpartidas);
    $codpartida = $datp['par_clave_int'];
    $partida = $datp['par_nombre'];
    $estp = $datp['par_estado'];
    $tippar = $datp['par_tipo'];
    if($tippar==1){ $text = $partida; }else{$text = $np+1;}
    if ($np > 0) {
        $objPHPExcel->createSheet();
    }

    $objPHPExcel->setActiveSheetIndex($np);
    $objPHPExcel->getActiveSheet()
        ->setCellValue('B1', "")
        ->setCellValue('E1', "PARTIDA PRESUPUESTAL\r\n".$partida)
        ->setCellValue('I1', "CUADRO COMPARATIVO N°\r\n".$text)
        ->setCellValue('E2', "VERSIÓN: 05")
        ->setCellValue('G2', "FECHA VERSION: 30/03/2016")
        ->setCellValue('I2', "APROBADO POR: NATALÍ PINZÓN")

        ->setCellValue('B3', "INTERVENTOR: LINEA GLOBAL")
        ->setCellValue('B4', "CONTRATISTA: ")
        ->setCellValue('B5', "CLIENTE: " . $cliente)
        ->setCellValue('B6', "NOMBRE DE LA OBRA: " . $nomo)

        ->setCellValue('B8', "COD.")
        ->setCellValue('C8', "DESCRIPCIÓN")
        ->setCellValue('D8', "UN")
        ->setCellValue('E8', "INICIAL")
        ->setCellValue('F8', "COMPROMETIDA")
        ->setCellValue('G8', "VR.UNIT")
        ->setCellValue('H8', "VR.COMPROMETIDO")
        ->setCellValue('I8', "ITEM")
        ->setCellValue('J8', "OBSERVACIONES")

        ->mergeCells('B1:D2')
        ->mergeCells('E1:H1')
        ->mergeCells('E2:F2')
        ->mergeCells('G2:H2')
        ->mergeCells('I1:J1')
        ->mergeCells('I2:J2')
        ->mergeCells('B3:J3')
        ->mergeCells('B4:J4')
        ->mergeCells('B5:J5')
        ->mergeCells('B6:J6')
        ->mergeCells('B7:J7')
       // ->mergeCells('B8:A9')
        ->mergeCells('B8:B9')
        ->mergeCells('C8:C9')
        ->mergeCells('D8:D9')
        ->mergeCells('E8:E9')
        ->mergeCells('F8:F9')
        ->mergeCells('G8:G9')
        ->mergeCells('H8:H9')
        ->mergeCells('I8:I9')
        ->mergeCells('J8:J9')
        //->mergeCells('K8:K9')
      ;

    cellColor('B1:J2', 'FFFFFF');
    cellColor('B8:J8', '92D050');
    $objPHPExcel->getActiveSheet()->getStyle('B8:J9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle("B1:J2")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B8:J8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle("B8:J8")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('E1:J2')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B3:J6')->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->getStyle('E1:J1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('B8:J8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('E1:J1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('E2:J2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('B1:J6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $objPHPExcel->getActiveSheet()->getStyle('A8:J9')->getFont()->setSize(10);
    $objPHPExcel->getActiveSheet()->getStyle('A8:J8')->getAlignment()->setWrapText(true);
//echo date('H:i:s') , " Set column height" , EOL;
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
    $objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
   // $objPHPExcel->getActiveSheet()->getRowDimension('10')->setRowHeight(6);
    $objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
    $objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(12.75);
    $objPHPExcel->getActiveSheet()->getStyle('B8:J9')->getFont()->setSize(10);
    //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
    //$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");

    $con = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva from presupuesto where pre_clave_int = '" . $idpresupuesto . "' limit 1");
    $dat = mysql_fetch_array($con);
    $adm = $dat['pre_administracion'];
    $iva = $dat['pre_iva'];
    $imp = $dat['pre_imprevisto'];
    $uti = $dat['pre_utilidades'];
    $apli = $dat['pre_apli_iva'];
    $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='" . $idpresupuesto . "'");
    $datsum = mysql_fetch_array($consu);
    if ($datsum['totc'] == "" || $datsum['totc'] == NULL) {
        $totalc = 0;
    } else {
        $totalc = $datsum['totc'];
    }
    if ($datsum['totca'] == "" || $datsum['totca'] == NULL) {
        $totalca = 0;
    } else {
        $totalca = $datsum['totca'];
    }

    $totadm = ($totalc * $adm) / 100;
    $totimp = ($totalc * $imp) / 100;
    $totuti = ($totalc * $uti) / 100;
    if ($apli == 0) {
        $totiva = ($totuti * $iva) / 100;
    } else {
        $totiva = (($totadm + $totimp + $totuti) * $iva) / 100;
    }
    $totpre = $totalc + $totadm + $totimp + $totuti + $totiva;

    if($estp==1){$wp = "AND i.ins_clave_int IN (SELECT DISTINCT ins_clave_int FROM partida_item
								WHERE pre_clave_int = '".$idpresupuesto."')";}else{$wp="";}
    $conpre = mysql_query("SELECT IFNULL(Id,Id1) Id,IFNULL(Ins,Ins1) Ins,IFNULL(Cod,Cod1) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                
								sum(d.pgi_rend_ini * pgi_cant_ini) AS Cant,sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' ".$wp."
								GROUP BY Id,Ins,Val,Cod
							) T1
							LEFT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_ini*d.pgi_rend_ini * pgi_cant_ini) AS Cant1,sum(pgi_rend_sub_ini*d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' ".$wp." 
								GROUP BY Id1,Ins1, Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1
							UNION
							SELECT IFNULL(Id1,Id) Id,IFNULL(Ins1,Ins) Ins,IFNULL(Cod1,Cod) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                
								sum(d.pgi_rend_ini * pgi_cant_ini) AS Cant,sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' ".$wp."
								GROUP BY Id,Ins,Val,Cod
							) T1
							RIGHT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_ini*d.pgi_rend_ini * pgi_cant_ini) AS Cant1,sum(pgi_rend_sub_ini*d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' ".$wp."
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1
							ORDER BY Ins");
    $numpre   = mysql_num_rows($conpre);
    $totales = 0;

    if ($numpre <= 0) {
        $numpre = 1;
    }
    $hasta = $numpre + 10;

    $acum = $hasta;
    $filc = 10;
    $totalvalor = 0;
    for ($i = 10; $i < $hasta; $i++) {
        $dat = mysql_fetch_array($conpre);

        $cod = $dat['Id'];
        $ins = $dat['Ins'];
        $val = $dat['Val'];
        $cant = $dat['Suma'];
        $uni = $dat['Cod'];
        $comprometido = $dat['Suma2'];
        $totalr = $val * $cant;
        $conop = mysql_query("select sum(pao_val_comprometido) totc from partida_item po join partidas pa on pa.par_clave_int = po.par_clave_int  where pa.pre_clave_int = '".$idpresupuesto."' and po.ins_clave_int = '".$cod."'");
        $datcp = mysql_fetch_array($conop);
        $comp = $datcp['totc']; if($comp=="" || $comp==NULL){$comp=0;}

        $concanc = mysql_query("select sum(pai_cant_comprometida) as canc from partida_item where par_clave_int = '".$codpartida."' and ins_clave_int = '".$cod."'");
        $datca = mysql_fetch_array($concanc);
        if($datca['canc']=="" || $datca['canc']==NULL){$cantc = 0;}else{$cantc = $datca['canc'];}

        $cono = mysql_query("select pao_observacion,pao_valor from partida_observacion where par_clave_int = '".$codpartida."' and ins_clave_int = '".$cod."'");
        $dato = mysql_fetch_array($cono);
        $numo = mysql_num_rows($cono);
        if($numo>0)
        {
            $obs = $dato['pao_observacion'];
            $total = $dato['pao_valor'];
        }
        else
        {
            $obs = "";
            $total = 0;
        }
        $totales = $totales + $total;

        $conitem = mysql_query("select d.pre_clave_int pre,d.gru_clave_int gru,d.cap_clave_int cap,g.gru_orden ord from pre_gru_cap_actividad d join partida_item p on p.pre_clave_int = d.pre_clave_int and p.gru_clave_int = d.gru_clave_int and p.cap_clave_int = d.cap_clave_int  and p.act_clave_int = d.act_clave_int join grupos g on g.gru_clave_int = d.gru_clave_int where p.par_clave_int = '".$codpartida."' and p.ins_clave_int = '".$cod."' group by pre,gru,cap order by ord,gru,cap");
        $numit = mysql_num_rows($conitem);
        if($numit>0)
        {
            $item  ="";
            for($s=0;$s<$numit;$s++)
            {
                $dat2 = mysql_fetch_array($conitem);
                $pre = $dat2['pre'];
                $gru = $dat2['gru'];
                $cap = $dat2['cap'];
                $conc = mysql_query("select pgc_codigo from pre_gru_capitulo d  where d.pre_clave_int = '".$pre."' and d.gru_clave_int ='".$gru."' and d.cap_clave_int = '".$cap."'");
                $datc = mysql_fetch_array($conc);
                $codc = $datc['pgc_codigo'];
                $conitem2 = mysql_query("select d.pgca_clave_int idd,d.act_clave_int act from pre_gru_cap_actividad d where d.pre_clave_int = '".$pre."' and d.gru_clave_int ='".$gru."' and d.cap_clave_int = '".$cap."' order by idd");
                $numit2 = mysql_num_rows($conitem2);
                if($numit2>0)
                {
                    for($l=1;$l<=$numit2;$l++)
                    {
                        $datl2 = mysql_fetch_array($conitem2);
                        $acti2 = $datl2['act'];
                        $convitem = mysql_query("select * from pre_gru_cap_actividad d join partida_item p on p.pre_clave_int = d.pre_clave_int and p.gru_clave_int = d.gru_clave_int and p.cap_clave_int = d.cap_clave_int  and p.act_clave_int = d.act_clave_int where p.par_clave_int = '".$codpartida."' and d.act_clave_int = '".$acti2."' and p.ins_clave_int = '".$cod."'");
                        $numitv = mysql_num_rows($convitem);
                        if($l<10){$it="".$codc.".0".$l;}else{$it="".$codc.".".$l;}
                        if($numitv>0)
                        {
                            $item .="".$it."-";
                        }
                    }
                }
                else
                {
                    $item = "";
                }
            }
        }
        else
        {
            $item = "";
        }

        $compro = $totalr - $comp;
        $objPHPExcel->getActiveSheet()
            ->setCellValue('B' . $filc, $cod)
            ->setCellValue('C' . $filc, $ins)
            ->setCellValue('D' . $filc, $uni)
            ->setCellValue('E' . $filc, $cant)
            ->setCellValue('F' . $filc, $cantc)
            ->setCellValue('G' . $filc, $val)//valor moneda
            ->setCellValue('H' . $filc, $total)//Valor moneda
            ->setCellValue('I' . $filc, $item)
            ->setCellValue('J' . $filc, $obs);
             cellColor('B' . $filc . ':J' . $filc, 'FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('C' . $filc)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('D'.$filc.':F'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->getStyle('G' . $filc . ":H" . $filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('E'.$filc.':F'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':J'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':J'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $filc . ':J' . $filc)->getFont()->setSize(10);
        //FIN CONSULTAS
        if($total<=0 and $estp==1)
        {
            $mostrafila = "false";
            $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setVisible(false);
        }
        else
        if($cant>0)
        {
            $mostrafila = "true";
            $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setVisible(true);
        }


        $filc = $filc + 1;

    }
    $filc2 = $filc + 1;
    if ($numpre <= 0) {
        $filcs = $filc;
    } else {
        $filcs = $filc - 1;
    }

    $objPHPExcel->getActiveSheet()
        ->setCellValue('B'.$filc2, "TOTAL PARTIDA PRESUPUESTAL")
        ->setCellValue('H'.$filc2, "=SUM(H10:H".($filc - 1).")")
        ->mergeCells('B'.$filc2.':G'.$filc2)
        ->mergeCells('G'.$filc.':J'.$filc)
    ;
    $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(6);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':H'.$filc2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':H'.$filc2)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('H'.$filc2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10.57);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10.57);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14.29);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14.29);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14.29);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14.29);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14.29);


    $objPHPExcel->getActiveSheet()->getStyle('B10:B' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('C10:C' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
    $objPHPExcel->getActiveSheet()->getStyle('D10:D' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('E10:E' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('F10:F' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('I10:I' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

    $objPHPExcel->getActiveSheet()->getStyle('G' . $filc . ':H' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
    $objPHPExcel->getActiveSheet()->getStyle('B'. $filc2 . ':F' . $filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

//añadir comentarios a celldas
    $objPHPExcel->getActiveSheet()->getComment('G'.$filc2)->setAuthor('Linea Global');
    $objPHPExcel->getActiveSheet()->getComment('G'.$filc2)->getText()->createTextRun('Monto que se llevará al "consolidado de partidas presupuestales"')->getFont()->setSize(7.5);

    $objPHPExcel->getActiveSheet()->getComment('F8')->setAuthor('Linea Global');
    $objPHPExcel->getActiveSheet()->getComment('F8')->getText()->createTextRun('Producto de la multiplicación del rendimiento del insumo por la cantidad del Item. Puede provenir del APU o del Subanálisis. En caso que provenga del Subanálisis, se multiplicará el rendimiento del insumo en el subanálisis x el rendimiento del subanálisis en el ítem x la cant total del item')->getFont()->setBold(true)->setSize(7.5);

    $objPHPExcel->getActiveSheet()->getComment('H8')->setAuthor('Linea Global');
    $objPHPExcel->getActiveSheet()->getComment('H8')->getText()->createTextRun('Producto de la multiplicación del valor (afectado por el rendimiento) del insumo por la cantidad del Item. Puede provenir del APU o del Subanálisis. En caso que provenga del Subanálisis, se multiplicará el valor (afectado por el rendimiento) del insumo en el subanálisis x el rendimiento del subanálisis en el ítem x la cant total del item')->getFont()->setBold(true)->setSize(7.5);

    $objPHPExcel->getActiveSheet()->getComment('I8')->setAuthor('Linea Global');
    $objPHPExcel->getActiveSheet()->getComment('I8')->getText()->createTextRun('Indicar # del ítem del cual se está comprometiendo el insumo')->getFont()->setBold(true)->setSize(7.5);

    $objPHPExcel->getActiveSheet()->getComment('J8')->setAuthor('Linea Global');
    $objPHPExcel->getActiveSheet()->getComment('J8')->getText()->createTextRun('Nombre de ítem y otros datos relevantes para tener claridad de la partida.')->getFont()->setBold(true)->setSize(7.5);

    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('logo');
    $objDrawing->setDescription('logo');
    $objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
    $objDrawing->setHeight(60);
    $objDrawing->setCoordinates('B1');
    $objDrawing->setOffsetX(0);
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(100);
//titulos de la hoja
    $objPHPExcel->getActiveSheet()->setTitle("PARTIDA ".($text));

}



$np = $numpartidas;
if ($np > 0) {
    $objPHPExcel->createSheet();
}

$objPHPExcel->setActiveSheetIndex($np);
$objPHPExcel->getActiveSheet()
    ->setCellValue('B1', "")
    ->setCellValue('D1', "LISTADO PARTIDAS PRESUPUESTALES")
    ->setCellValue('D2', "VERSIÓN: 05")
    ->setCellValue('F2', "FECHA VERSION: 30/03/2016")
    ->setCellValue('G2', "APROBADO POR: NATALÍ PINZÓN")

    ->setCellValue('B3', "INTERVENTOR: LINEA GLOBAL")
    ->setCellValue('B4', "CONTRATISTA: ")
    ->setCellValue('B5', "CLIENTE: " . $cliente)
    ->setCellValue('B6', "NOMBRE DE LA OBRA: " . $nomo)

    ->setCellValue('B8', "N°.")
    ->setCellValue('C8', "DESCRIPCIÓN")
    ->setCellValue('F8', "VALOR")
    ->setCellValue('G8', "OBSERVACIONES")

    ->mergeCells('B1:C2')
    ->mergeCells('D1:G1')
    ->mergeCells('D2:E2')
    ->mergeCells('B3:G3')
    ->mergeCells('B4:G4')
    ->mergeCells('B5:G5')
    ->mergeCells('B6:G6')
    ->mergeCells('B7:G7')
    // ->mergeCells('B8:A9')
    ->mergeCells('B8:B9')
    ->mergeCells('C8:E9')

    ->mergeCells('F8:F9')
    ->mergeCells('G8:G9');

cellColor('B1:G2', 'FFFFFF');
cellColor('B8:G8', '92D050');
$objPHPExcel->getActiveSheet()->getStyle('B8:G9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("B1:G2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B8:G8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle("B8:G8")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E1:G2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B3:G6')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('D1:G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B8:J8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D1:G1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('D2:G2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B1:G6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('B8:G9')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('B8:G8')->getAlignment()->setWrapText(true);
//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
// $objPHPExcel->getActiveSheet()->getRowDimension('10')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getStyle('B8:G9')->getFont()->setSize(10);
//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");

$con = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva from presupuesto where pre_clave_int = '" . $idpresupuesto . "' limit 1");
$dat = mysql_fetch_array($con);
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$apli = $dat['pre_apli_iva'];
$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='" . $idpresupuesto . "'");
$datsum = mysql_fetch_array($consu);
if ($datsum['totc'] == "" || $datsum['totc'] == NULL) {
    $totalc = 0;
} else {
    $totalc = $datsum['totc'];
}
if ($datsum['totca'] == "" || $datsum['totca'] == NULL) {
    $totalca = 0;
} else {
    $totalca = $datsum['totca'];
}

$totadm = ($totalc * $adm) / 100;
$totimp = ($totalc * $imp) / 100;
$totuti = ($totalc * $uti) / 100;
if ($apli == 0) {
    $totiva = ($totuti * $iva) / 100;
} else {
    $totiva = (($totadm + $totimp + $totuti) * $iva) / 100;
}
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$conpre = mysql_query("select par_clave_int,par_observacion,par_nombre,par_tipo from partidas where pre_clave_int = '".$idpresupuesto."' order by par_tipo ASC,par_clave_int ASC");
$numpre   = mysql_num_rows($conpre);
$totales = 0;

if ($numpre <= 0) {
    $numpre = 1;
}
$hasta = $numpre + 10;

$acum = $hasta;
$filc = 10;
$totalvalor = 0;
$np = 1;
for ($i = 10; $i < $hasta; $i++) {
    $dat = mysql_fetch_array($conpre);

    $obs = $dat['par_observacion'];
    $idp = $dat['par_clave_int'];
    $nom = $dat['par_nombre'];

    $conv = mysql_query("select sum(po.pai_val_comprometido) as tot from  partidas p  join partida_item po on p.par_clave_int =  po.par_clave_int  WHERE p.pre_clave_int = '".$pre."' and p.par_clave_int = '".$idp."'");
    $datv = mysql_fetch_array($conv);
    $total = $datv['tot'];
    $objPHPExcel->getActiveSheet()
        ->setCellValue('B' . $filc, $np)
        ->setCellValue('C' . $filc, $nom)
        ->setCellValue('F' . $filc, $total)//valor moneda
        ->setCellValue('G' . $filc, $obs)
        ->mergeCells('C'.$filc.':E'.$filc)
    ;
    cellColor('B' . $filc . ':G' . $filc, 'FFFFFF');
    $objPHPExcel->getActiveSheet()->getStyle('C' . $filc)->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('F' . $filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':G'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':G'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('B' . $filc . ':G' . $filc)->getFont()->setSize(10);
    //FIN CONSULTAS
    $filc = $filc + 1;
    $np++;
}
$filc2 = $filc + 1;
if ($numpre <= 0) {  $filcs = $filc;} else {   $filcs = $filc - 1; }

$objPHPExcel->getActiveSheet()
    ->setCellValue('B'.$filc2, "TOTAL PARTIDA PRESUPUESTAL")
    ->setCellValue('F'.$filc2, "=SUM(F10:F".($filc - 1).")")
    ->mergeCells('B'.$filc2.':E'.$filc2)
    //->mergeCells('G'.$filc.':J'.$filc)
;
$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':F'.$filc2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':F'.$filc2)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('F'.$filc2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
//$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10.57);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(26);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);



$objPHPExcel->getActiveSheet()->getStyle('B10:B' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('C10:C' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('F10:F' . $filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

$objPHPExcel->getActiveSheet()->getStyle('G' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('B'. $filc2 . ':E' . $filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

//añadir comentarios a celldas

$objPHPExcel->getActiveSheet()->getComment('B8')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('B8')->getText()->createTextRun('Nümero del proceso de cuadro comparativo. Debe ser el mismo indicado en la partida')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('F8')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('F8')->getText()->createTextRun('Se enlaza del total de cada partida.')->getFont()->setBold(true)->setSize(7.5);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('B1');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(100);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle("CONSOLIDADO PARTIDAS PTALES ");

//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
    $callStartTime = microtime(true);
    $archivo =  date('Ymd') . ' PARTIDAS PRESUPUESTALES ' . $tppr . ' ' . $nomo . '.xlsx';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//$objWriter->save(str_replace(__FILE__,'descargas/'.$archivo,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
// Set password for readonly activesheet
/*
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("P4v4s2017.*");*/
// Set password for readonly data


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');