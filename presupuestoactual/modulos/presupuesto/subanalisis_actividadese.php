<?php
include("../../data/Conexion.php");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//JOIN actividadinsumos a on i.act_clave_int = a.act_clave_int
// DB table to use
$table = 'actividades';

// Table's primary key
$primaryKey = 'd.act_subanalisis';//'act_clave_int'
$id = $_GET['id'];
$act = $_GET['act'];
$pre = $_GET['pre'];
$cap = $_GET['cap'];

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'd.act_subanalisis',
		'dt' => 'DT_RowId', 'field' => 'act_subanalisis',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
	array( 'db' => 'd.act_subanalisis', 'dt' => 'Codigo', 'field' => 'act_subanalisis' ),
	array( 'db' => 'd.pre_clave_int', 'dt' => 'Pre', 'field' => 'pre_clave_int' ),
	array( 'db' => 'd.pev_clave_int', 'dt' => 'Cap', 'field' => 'cap_clave_int' ),
	array( 'db' => 'i.act_nombre', 'dt' => 'Nombre', 'field' => 'act_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array('db'  => 'd.act_clave_int','dt' => 'Total', 'field' => 'act_clave_int', 'formatter' => function( $d, $row ) {

		$cona = mysql_query("select pre_apli_iva from presupuesto where pre_clave_int = '".$row[2]."'");
		$data = mysql_fetch_array($cona);
		$apli = $data['pre_apli_iva'];
		if($apli==0)
		{
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."'  and pa.pev_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);	
		}
		else
		{
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."' ' and pa.pev_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);	
		}
		
		return '<div id="divapus'.$row[0].'" class="currency">$'.number_format($totals,2,'.',',').'</div>';
        }),
		array('db'  => 'd.act_clave_int','dt' => 'TotalA', 'field' => 'act_clave_int', 'formatter' => function( $d, $row ) {

		$cona = mysql_query("select pre_apli_iva from presupuesto where pre_clave_int = '".$row[2]."'");
		$data = mysql_fetch_array($cona);
		$apli = $data['pre_apli_iva'];
		if($apli==0)
		{
			
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."  and pa.pev_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");		
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
		}
		else
		{
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."'  and pa.pev_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");		
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
		}
		
            return '<div id="divapusa'.$row[0].'" class="currency">$'.number_format($totals,2,'.',',').'</div>';
       }),	   
		array('db'  => 'd.act_clave_int','dt' => 'Alcance', 'field' => 'act_clave_int', 'formatter' => function( $d, $row ) {

		$cona = mysql_query("select pre_apli_iva from presupuesto where pre_clave_int = '".$row[2]."'");
		$data = mysql_fetch_array($cona);
		$apli = $data['pre_apli_iva'];
		if($apli==0)
		{
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."'  and pa.pev_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsui = $totals + ($totads+$totims+$totuts+$totivs);	
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."'  and pa.pev_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");		
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsua = $totals + ($totads+$totims+$totuts+$totivs);
		}
		else
		{
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."'  and pa.cap_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsui = $totals + ($totads+$totims+$totuts+$totivs);	
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$row[2]."' and pa.pev_clave_int= '".$row[3]."' and a.act_clave_int = '".$d."' and pa.act_subanalisis = '".$row[1]."'");		
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsua = $totals + ($totads+$totims+$totuts+$totivs);
		}
		$alcancesu = $totalsui - $totalsua;
            return '<div id="divalcancesub'.$row[0].'" class="currency">$'.number_format($alcancesu,2,'.',',').'</div>';
       })
    
	
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = ' d.act_subanalisis ';
 
 $joinQuery = "FROM  actividades AS i join pre_cap_act_sub_insumo d on d.act_subanalisis = i.act_clave_int JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int";
$extraWhere = " d.act_clave_int = '".$act."' and d.pre_clave_int = '".$pre."' and d.pev_clave_int ='".$cap."'";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

