<?php
session_start();
include ("../../data/Conexion.php");
error_reporting(0);
date_default_timezone_set('America/Bogota');

// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario = $_COOKIE["usuario"];
$idUsuario = $_COOKIE["usIdentificacion"];
$con = mysql_query("select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysql_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$percla = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$hi = $_GET['hi'];
$usuarios = 0;
$con = mysql_query("select usu_clave_int from usuario where usu_coordinador = '".$idUsuario."'");
$num = mysql_num_rows($con);
if($num>0)
{
	$idu = array();
    for($u=0;$u<$num;$u++)
	{
		$dat = mysql_fetch_array($con);
		$usu = $dat['usu_clave_int'];
		$idu[] = $usu;
	}
	$usuarios = implode(",",$idu);
}

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
$tipoproyecto  =$_GET['tipoproyecto'];
$nombre = $_GET['nombre'];
$fec = $_GET['fec'];
$cliente = $_GET['cliente'];
		
// DB table to use
$table = 'presupuesto';
// Table's primary key
$primaryKey = 'pr.pre_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
	array(
		'db' => 'pr.pre_clave_int',
		'dt' => 'DT_RowId', 'field' => 'pre_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
	array(
		'db' => 'pr.pre_clave_int',
		'dt' => 'UD_Id', 'field' => 'pre_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return $d;
		}
	),
		array( 'db' => 'pr.pre_clave_int', 'dt' => 'Presupuesto', 'field' => 'pre_clave_int' ),
		array( 'db' => 'pr.pre_nombre', 'dt' => 'Nombre', 'field' => 'pre_nombre' ),
		array( 'db' => 'pr.pre_descripcion', 'dt' => 'Descripcion', 'field' => 'pre_descripcion' ),
		array( 'db' => 'pr.pre_fecha', 'dt' => 'Creacion', 'field' => 'pre_fecha' ),
		array( 'db' => "pr.tpp_clave_int", 'dt' => 'Tipop', 'field' => 'tpp_clave_int' ,'formatter' => function( $d, $row ) {
					
			$cont = mysql_query("select tpp_nombre from tipoproyecto t  where tpp_clave_int = '".$d."'");
			$dat  = mysql_fetch_array($cont);
			$tipo = $dat['tpp_nombre'];
			return $tipo;	
        })  ,
		array( 'db' => "pr.esp_clave_int", 'dt' => 'Estadop', 'field' => 'esp_clave_int' ,'formatter' => function( $d, $row ) {
			
			$cont = mysql_query("select esp_nombre  from estadosproyecto where esp_clave_int = '".$d."'");
			$datt = mysql_fetch_array($cont);
			$esp = $datt['esp_nombre'];				
			return $esp;			
			
        })  ,	
		array( 'db' => 'pr.pre_administracion','dt' => 'Adm', 'field' => 'pre_administracion' ),
		array( 'db' => 'pr.pre_imprevisto','dt' => 'Imp', 'field' => 'pre_imprevisto' ),
		array( 'db' => 'pr.pre_utilidades','dt' => 'Uti', 'field' => 'pre_utilidades' ),
		array( 'db' => 'pr.pre_iva','dt' => 'Iva', 'field' => 'pre_iva' ),
		array('db'  => 'pr.pre_clave_int','dt' => 'Total', 'field' => 'pre_clave_int' ,'formatter' => function( $d, $row ) {
		
			$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$d."'");		
			$datsum = mysql_fetch_array($consu);
			if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}
		    
			/*$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
			",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."'");
			$datsu = mysql_fetch_array($consu);
			//DATOS DISTRIBUCION
			
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			
			//
			
			if($datsu['tot']=="" || $datsu['tot']==NULL){$total = 0;}else {$total  = $datsu['tot'];}
			$total = $total + ($totad+$totim+$totut+$totiv);*/
			$totadm = ($total * $row[8])/100;
			$totimp = ($total * $row[9])/100;
			$totuti = ($total * $row[10])/100;
			$totiva = ($totuti *$row[11])/100;
			$total = $total + $totadm + $totimp + $totuti + $totiva;			
			return number_format($totalc,2,'.',',');	  			
        } ),		
		array( 'db'  => 'pr.pre_cliente','dt' => 'Cliente', 'field' => 'pre_cliente' ),
		array( 'db' => 'pe.per_razon', 'dt' => 'Persona', 'field' => 'per_razon','formatter' => function( $d, $row  ) {
			if($row[13]=="")
			{
			return $d;
			}
			else
			{
			return $row[13];
			}
        }
		 ),		
		array( 'db' => 'pr.est_clave_int', 'dt' => 'Estado', 'field' => 'est_clave_int', 'formatter' => function( $d, $row ) {
			return "";
        }),
		array( 'db' => 'pr.esp_clave_int', 'dt' => 'Estadop', 'field' => 'esp_clave_int', 'formatter' => function( $d, $row ) {
			$con = mysql_query("select esp_nombre from estadosproyecto where esp_clave_int = '".$d."' limit 1");
			$dat = mysql_fetch_array($con);
			return $dat['esp_nombre'];
        })
	
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = ' pr.pre_clave_int';

$joinQuery = "FROM  presupuesto AS pr LEFT OUTER JOIN persona pe on pr.per_clave_int = pe.per_clave_int";
			//t.tic_estado NOT IN (3,4) and
if(strtoupper($perfil)=="ADMINISTRADOR")
{
			if($hi==0)
			{
				$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(2) and pr.pre_est_anterior!=5 ";//or t.tic_usuario = '".$usuario."'
			}
			else
			{
				$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(2) and pr.pre_est_anterior=5";//or t.tic_usuario = '".$usuario."'
			}
}else
if(strtoupper($perfil)=="COORDINADOR")
{
	if($hi==0)
			{
				$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(2) and pr.pre_est_anterior!=5  and (pr.pre_clave_int in(select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$idUsuario."') or pr.pre_coordinador ='".$idUsuario."')";//or t.tic_usuario = '".$usuario."'
			}
			else
			{
				$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(2) and pr.pre_est_anterior=5  and (pr.pre_clave_int in(select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$idUsuario."') or pr.pre_coordinador ='".$idUsuario."')";//or t.tic_usuario = '".$usuario."'
			}
	
}
else
{
	if($hi==0)
			{
				$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(2) and pr.pre_est_anterior!=5  and pr.pre_clave_int in(select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$idUsuario."')";//or t.tic_usuario = '".$usuario."'
			}
			else
			{
				$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(2) and pr.pre_est_anterior=5  and pr.pre_clave_int in(select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$idUsuario."')";//or t.tic_usuario = '".$usuario."'
			}

}


echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
);

