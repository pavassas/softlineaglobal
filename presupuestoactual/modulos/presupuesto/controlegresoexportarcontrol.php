<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idpresupuesto = $_GET['edi'];
include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('safe_mode', 0);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}
//DATOS DEL PRESUPUESTO
$con  = mysql_query("select UPPER(per_nombre) nom,UPPER(per_apellido)  ape,UPPER(per_razon) cli, per_documento,pre_nombre,pre_fecha,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_usu_creacion,pre_cliente,tpp_clave_int,date_format(pre_fecha,'%Y%m%d') as fec,pre_apli_iva,pre_coordinador as cor,pre_av_plan,pre_av_real,pre_documento  from presupuesto pr left outer join persona p on p.per_clave_int = pr.per_clave_int where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$fech = $dat['fec'];
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$nomo = $dat['pre_nombre'];
$fechai  =$dat['pre_fecha'];
$apli = $dat['pre_apli_iva'];
$tpp = $dat['tpp_clave_int'];
$precliente = $dat['pre_cliente'];
$avplan = $dat['pre_av_plan'];
$avreal = $dat['pre_av_real'];
$atraso = $avplan - $avreal;
$codproyecto = $dat['pre_codigo'];
$docproyecto = $dat['pre_documento'];
$nomcliente = $dat['cli'];
$cor = $dat['cor'];
$cont = mysql_query("select tpp_nombre from tipoproyecto where tpp_clave_int = '".$tpp."'");
$datt =  mysql_fetch_array($cont);
$tppr =  strtoupper($datt['tpp_nombre']);

if($precliente!=""){$cliente=$precliente;}else { $cliente = $nomcliente." - NIT: ".$dat['per_documento'];}
$creado = $dat['pre_usu_creacion'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$creado."' limit 1");
$dat = mysql_fetch_array($con);
$creadopor = $dat['usu'];
$car  = $dat['car_clave_int'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$cor."' limit 1");
$dat = mysql_fetch_array($con);
$aprobadopor = $dat['usu'];
$carc  = $dat['car_clave_int'];

$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$car."'");
$datc = mysql_fetch_array($conc);
$cargo = $datc['car'];
$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$carc."'");
$datc = mysql_fetch_array($conc);
$cargoc = $datc['car'];

$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysql_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

$totadm = ($totalc * $adm)/100;
$totimp = ($totalc * $imp)/100;
$totuti = ($totalc * $uti)/100;
if($apli==0){ $totiva = ($totuti * $iva)/100; }else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100; }
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$adma = $dat['pri_administracion'];
$ivaa = $dat['pri_iva'];
$impa = $dat['pri_imprevisto'];
$utia = $dat['pri_utilidades'];

$totadma = ($totalca * $adma)/100;
$totimpa = ($totalca * $impa)/100;
$totutia = ($totalca * $utia)/100;
if($apli==0) { $totivaa = ($totutia * $ivaa)/100; } else { $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;

$alccos = $totalc - $totalca;
$alcpre = $totpre - $totprea;
$alcadm = $totadm - $totadma;
$alcimp = $totimp - $totimpa;
$alcuti = $totuti - $totutia;
$alciva = $totiva - $totivaa;

$cons = mysql_query("select sum(cpe_valor_neto) net,sum(cpe_iva) iv, sum(cpe_amortizacion)  as am,sum(cpe_ret_garantia) gar,sum(cpe_valor_aprobado) as ap,sum(cpe_anticipo) as anti from control_egreso where pre_clave_int = '".$idpresupuesto."'");
$dat = mysql_fetch_array($cons);
if($dat['net']=="" || $dat['net']==NULL){$totalneto = 0;}else{$totalneto = $dat['net'];}
if($dat['iv']=="" || $dat['iv']==NULL){$totaliva = 0;}else{$totaliva = $dat['iv'];}
$totalbru = $totalneto + $totaliva;
if($dat['am']=="" || $dat['am']==NULL){$totalamo = 0;}else{$totalamo = $dat['am'];}
if($dat['anti']=="" || $dat['anti']==NULL){$totalanti = 0;}else{$totalanti = $dat['anti'];}
if($dat['gar']=="" || $dat['gar']==NULL){$totalgarantia = 0;}else{$totalgarantia = $dat['gar'];}
if($dat['ap']=="" || $dat['ap']==NULL){$totalaprobado = 0;}else{$totalaprobado = $dat['ap'];}

$saldo = $totpre - ($totalbru + $totalanti - $totalamo);

$totalsaldo = $totalaprobado - $totalbru ;

if($totalsaldo<0){$totalsaldo = "(". number_format($totalsaldo*(-1),2,'.',',').")"; $col1 = "red";}
else{$totalsaldo = number_format($totalsaldo,2,'.',','); $col1="black";}

/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);

// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => array('argb' => 'FF993300'),
        ),
    ),
);

// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
    ->setLastModifiedBy("Pavas.co")
    ->setTitle("Informe Presupuesto")
    ->setSubject("Informe Presupuesto")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Informes");

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A1' , "")
    ->setCellValue('E1' , "CONTROL PRESUPUESTAL POR EGRESOS")
    ->setCellValue('E2' , "VERSIÓN: 05")
    ->setCellValue('J2' , "FECHA VERSION: 30/03/2016")
    ->setCellValue('O2' , "APROBADO POR: NATALÍ PINZÓN")
    ->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL")
    ->setCellValue('A4' , "CONTRATISTA: ")
    ->setCellValue('A5' , "CLIENTE: ".$cliente)
    ->setCellValue('A6' , "NOMBRE DE LA OBRA: ".$nomo)

    ->setCellValue('A11' , "GRUPO")
    ->setCellValue('B11' , "FECHA")
    ->setCellValue('C11' , "NIT O CEDULA")
    ->setCellValue('D11' , "BENEFICIARIO")
    ->setCellValue('E11' , "N° DE FRA")
    ->setCellValue('F11' , "N° DE ACTA")
    ->setCellValue('G11' , "FECHA FACTURA")
    ->setCellValue('H11' , "VALOR NETO")
    ->setCellValue('I11' , "IVA")
    ->setCellValue('J11' , "VALOR BRUTO")
    ->setCellValue('K11' , "RETEGARANTIA")
    ->setCellValue('L11' , "ANTICIPO")
    ->setCellValue('M11' , "AMORTIZACION ANTICIPO")
    ->setCellValue('N11' , "N° ORDEN DE COMP.")
    ->setCellValue('O11' , "VALOR APROBADO ORDEN DE COMPRA DE OBRA")
    ->setCellValue('P11' , "SALDO O.C")
    ->setCellValue('Q11' , "ESTADO DE ORDEN DE COMPRA")
    ->setCellValue('R11' , "OBSERVACIONES")
    ->mergeCells('A1:D2')
    ->mergeCells('E1:R1')
    ->mergeCells('E2:I2')
    ->mergeCells('J2:N2')
    ->mergeCells('O2:R2')

    ->mergeCells('A3:R3')
    ->mergeCells('A4:R4')
    ->mergeCells('A5:R5')
    ->mergeCells('A6:R6')
    ->mergeCells('A7:R7')

    ->mergeCells('A11:A12')
    ->mergeCells('B11:B12')
    ->mergeCells('C11:C12')
    ->mergeCells('D11:D12')
    ->mergeCells('E11:E12')
    ->mergeCells('F11:F12')
    ->mergeCells('G11:G12')
    ->mergeCells('H11:H12')
    ->mergeCells('I11:I12')
    ->mergeCells('J11:J12')
    ->mergeCells('K11:K12')
    ->mergeCells('L11:L12')
    ->mergeCells('M11:M12')
    ->mergeCells('N11:N12')
    ->mergeCells('O11:O12')
    ->mergeCells('P11:P12')
    ->mergeCells('Q11:Q12')
    ->mergeCells('R11:R12')
;

cellColor('A1:R2', 'FFFFFF');
cellColor('A11:R11', '92D050');
$objPHPExcel->getActiveSheet()->getStyle('A11:R12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("A1:R2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A11:R11')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle("A11:R11")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E1:R2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:R6')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('E1:R1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A11:R11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('E2:R2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A1:R6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('A11:R12')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A11:R11')->getAlignment()->setWrapText(true);
//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('10')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getStyle('A8:R9')->getFont()->setSize(10);
//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");

$con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva from presupuesto where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$apli = $dat['pre_apli_iva'];
$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysql_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

/* if($apli==0)
 {
       $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".
       ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
       ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
       ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
       ",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
       " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
       $datsu = mysql_fetch_array($consu);
       $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
       if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
       $totals = $totals + ($totads+$totims+$totuts+$totivs);

       $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".
       ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
       ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
       ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
       ",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
       " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
       $datsu = mysql_fetch_array($consu);
       $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
       if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}
       $totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);
 }
 else
 {
   $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".
   ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
   ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
   ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
   ",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
   "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
   "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
   " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
   $datsu = mysql_fetch_array($consu);
   $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
   if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
   $totals = $totals + ($totads+$totims+$totuts+$totivs);

   $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".
   ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
   ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
   ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
   ",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
   "((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
   "((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
   " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
   $datsu = mysql_fetch_array($consu);
   $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
   if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}
   $totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);
 }
   */
$totadm = ($totalc * $adm)/100;
$totimp = ($totalc * $imp)/100;
$totuti = ($totalc * $uti)/100;
if($apli==0) { $totiva = ($totuti * $iva)/100; } else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100;  }
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$conpre = mysql_query("select cpe_clave_int,pre_clave_int,gru_clave_int,cpe_num_factura,cpe_num_acta,cpe_fec_factura,cpe_valor_neto,cpe_iva,cpe_ret_garantia,cpe_amortizacion,cpe_num_orden,cpe_valor_aprobado,cpe_observaciones,date_format(cpe_fec_factura,'%d-%b-%y') ff,cpe_documento,cpe_beneficiario,cpe_fecha,cpe_anticipo  from control_egreso where pre_clave_int = '".$idpresupuesto."'");

$numpre = mysql_num_rows($conpre); if($numpre<=0){$numpre=1;}
$hasta = $numpre + 13;

$acum = $hasta;
$filc = 13;
$totalvalor  = 0;
for ($i = 13; $i < $hasta; $i++)
{
    $dat = mysql_fetch_array($conpre);
    $cpe = $dat['cpe_clave_int'];
    $pre = $dat['pre_clave_int'];
    $gru = $dat['gru_clave_int'];
    $fac = $dat['cpe_num_factura'];
    $act = $dat['cpe_num_acta'];
    $fec = $dat['cpe_fecha'];
    $doc = $dat['cpe_documento'];
    $ben = $dat['cpe_beneficiario'];
    $fecf = $dat['cpe_fec_factura'];
    $net = $dat['cpe_valor_neto'];
    $iva = $dat['cpe_iva'];
    $ret = $dat['cpe_ret_garantia'];
    $anti = $dat['cpe_anticipo'];
    $amo = $dat['cpe_amortizacion'];
    $ord = $dat['cpe_num_orden'];
    $apro = $dat['cpe_valor_aprobado'];
    $obs = $dat['cpe_observaciones'];

    $ff = $dat['ff'];
    $bru = $net + $iva;
    $sal = $apro-$bru;
    if($sal>0){$est="Abierta";}else if($sal==0){$est = "Cerrada";}else{$est="Sobregirada";}
    if($sal<0){$sal = "(".$sal*(-1).")"; $col = "red";}else{$col="black";}

    $objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $gru)
        ->setCellValue('B' . $filc, $fec)
        ->setCellValue('C' . $filc, $doc)
        ->setCellValue('D' . $filc, $ben)
        ->setCellValue('E' . $filc, $fac)
        ->setCellValue('F' . $filc, $act)
        ->setCellValue('G' . $filc, $ff)
        ->setCellValue('H' . $filc, $net)
        ->setCellValue('I' . $filc, $iva)
        ->setCellValue('J' . $filc, '=+H'.$filc.'+I'.$filc)
        ->setCellValue('K' . $filc, $ret)
        ->setCellValue('L' . $filc, $anti)
        ->setCellValue('M' . $filc, $amo)
        ->setCellValue('N' . $filc, $ord)
        ->setCellValue('O' . $filc, $apro)
        ->setCellValue('P' . $filc, '=+O'.$filc.'-J'.$filc)
        ->setCellValue('Q' . $filc, '=IF(P' .$filc.'>0,"Abierta",IF(P'.$filc.'<0,"Sobregirada","Cerrada"))')
        ->setCellValue('R' . $filc, $obs);
    cellColor('A' . $filc.':R'.$filc, 'FFFFFF');

    $objPHPExcel->getActiveSheet()->getStyle('H'.$filc.":M".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('O'.$filc.":P".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':R'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':R'.$filc)->getFont()->setSize(10);


    //FIN CONSULTAS

    $filc = $filc+ 1;

}
$filc2 = $filc +1;
if($numpre<=0){ $filcs = $filc; }else { $filcs = $filc-1;}

$objPHPExcel->getActiveSheet()
    ->setCellValue('A8' , "PARTIDA PRESUPUESTAL")
    ->setCellValue('A9' , "SALDO PRESUPUESTAL")
    ->setCellValue('E8' , $totpre)
    ->setCellValue('E9' , '=+E8-H'.$filc2)
    ->setCellValue('A'.$filc , 'TOTALES')
    ->setCellValue('H'.$filc , '=SUM(H13:H'.$filcs.')')
    ->setCellValue('I'.$filc , '=SUM(I13:I'.$filcs.')')
    ->setCellValue('J'.$filc , '=SUM(J13:J'.$filcs.')')
    ->setCellValue('K'.$filc , '=SUM(K13:K'.$filcs.')')
    ->setCellValue('L'.$filc , '=SUM(L13:L'.$filcs.')')
    ->setCellValue('M'.$filc , '=SUM(M13:M'.$filcs.')')
    ->setCellValue('O'.$filc , '=SUM(O13:O'.$filcs.')')
    ->setCellValue('P'.$filc , '=SUM(P13:P'.$filcs.')')
    ->setCellValue('A'.$filc2 ,'TOTAL INVERTIDO')
    ->setCellValue('H'.$filc2 , '=+J'.$filc.'+L'.$filc.'-M'.$filc)
    ->setCellValue('I'.$filc2 , '')
    ->setCellValue('J'.$filc2 , '')
    ->setCellValue('K'.$filc2 , '')
    ->setCellValue('L'.$filc2 , '')
    ->setCellValue('M'.$filc2 , '')
    ->setCellValue('O'.$filc2 , '')
    ->setCellValue('P'.$filc2 , '')
    ->mergeCells('A8:D8')
    ->mergeCells('A9:D9')
    ->mergeCells('E8:G8')
    ->mergeCells('E9:G9')
    ->mergeCells('H8:R9')
    ->mergeCells('A10:R10')
    ->mergeCells('A'.$filc.':G'.$filc)
    ->mergeCells('A'.$filc2.':G'.$filc2)
    ///->mergeCells('Q'.$filc.':R'.$filc)
;
$objPHPExcel->getActiveSheet()->getStyle('A8:G9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("A8:G9")->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('E8:G9')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('E8:G9')->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':R'.$filc)->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('H'.$filc.':P'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('H'.$filc.":M".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('O'.$filc.":P".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');


$objPHPExcel->getActiveSheet()->getStyle('A'.$filc2.':R'.$filc2)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H'.$filc2.':P'.$filc2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

//$objPHPExcel->getActiveSheet()->getStyle('H'.$filc2.':P'.$filc2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);


//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10.57);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10.57);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10.14);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20.14);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11.43);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11.43);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(14.86);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(19.43);
$objPHPExcel->getActiveSheet()->getStyle('A13:A'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B13:B'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('C13:C'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('D13:D'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('E13:E'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('F13:F'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('G13:G'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('L13:L'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('M13:M'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('N13:N'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('O13:O'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('P13:P'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
//$objPHPExcel->getActiveSheet()->getStyle('H13:M'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
//$objPHPExcel->getActiveSheet()->getStyle('O13:P'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('A13:R'.$filc2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc2.':G'.$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

//añadir comentarios a celldas


$objPHPExcel->getActiveSheet()->getComment('Q11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('P11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
//$objPHPExcel->getActiveSheet()->getComment('P11')->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()->getComment('Q11')->getText()->createTextRun('Si el saldo es mayor a 0 entonces la OC esta abierta y si es igual a Cero entonces esta cerrada')->getFont()->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('P11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('O11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
//$objPHPExcel->getActiveSheet()->getComment('O11')->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()->getComment('P11')->getText()->createTextRun('Diferencia entre el valor aprobado en la OC y el valor bruto de la factura')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('O11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('N11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('O11')->getText()->createTextRun('Monto Aprobado para la OC. Si la OC se repite en varias facturas, relacionar el valor aprobado una sola vez')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('B11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('B11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('B11')->getText()->createTextRun('Fecha de recepción del gasto/Factura en Interventoria')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('A11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('A11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('A11')->getText()->createTextRun('Grupo al cual corresponde el gasto')->getFont()->setBold(true)->setSize(7.5);
/*$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");*/

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(100);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('CONTROL PRESUPUESTAL POR EGRESO');
//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
$callStartTime = microtime(true);
$archivo =  date('Ymd').' CONTROL EGRESO '.$tppr.' '.$nomo.'.xlsx';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//$objWriter->save(str_replace(__FILE__,'descargas/'.$archivo,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
// Set password for readonly activesheet
/*
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("P4v4s2017.*");*/
// Set password for readonly data


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');