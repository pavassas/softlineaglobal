<?php
include('../../data/Conexion.php');
?>
  <script src="js/jsupload.js"></script>

<section>
<div class="row" style="background-color:#222d32">
<div class="col-md-11">
<h4><a class="text-muted"><i class="fa fa-dashboard"></i> MAESTRAS/</a><a ui-sref="Actividades" ui-sref-opts="{reload: true}">ACTIVIDADES</a> <small class="active">/ <a ui-sref="Importacion" ui-sref-opts="{reload: true}">IMPORTACIÓN</a></small></h4>
</div>
</div>
<div class="row" ng-controller="ProgressCtrl">
    <div class="col-xs-12">
    
        <div class="box">
            <div class="box-body">
            <div id="mensaje"></div>
            <div id="respuesta" class="alert"></div>
       <form class="formulario1" enctype="multipart/form-data">
          
          <div class="row">
             <div class="col-lg-1"> 
                <label> Archivo: </label> 
             </div>             
             <div class="col-lg-4">
                <input type="file" name="archivoactividad" id="archivoactividad" class="form-control" />
             </div> 
             <div class="col-lg-1">
                <a id="boton_subir" role="button" class="btn btn-success"><i class="glyphicon glyphicon-upload"></i></a>
             </div>             
          </div>
          <ul class="list-inline">
          <li><a class="link-blue text-sm"  href="modulos/formatos/FORMATOIMPORTACIONAPU.xls">Archivo Importacion Actividades <i class="fa fa-file-excel-o"></i>Descargar</a> </li>
          <li><a class="link-black text-sm" ><i class="fa fa-file-excel-o"></i>Formato de Archivo: Excel 97-2003 Extension .xls</a></li>
          </ul>
          <hr />
         
       
       <hr />
       <div id="archivos_subidos"></div>
        <div class="form-group" id="progreso" style="display:none"><uib-progressbar  class="progress-striped active"  max="max" value="100"><span style="color:white; white-space:nowrap;">Analizando Archivo Por favor Espere...</span></uib-progressbar></div>
       <div id="publishform1" class="form-group"></div>
       </form>

            </div>
        </div>
    </div>
 </div>
 </section>
<!-- Modal -->

