<?php
include('../../data/Conexion.php');
?>
<input type="hidden" name="tipo" id="tipo" value="Todos">
<section>
<div class="row" style="background-color:#222d32">
<div class="col-md-11">
<h4><a class="text-muted"><i class="fa fa-dashboard"></i> CAMBIAR CONTRASEÑA/</a> <small class="active"></small></h4>
</div>
</div>
 <div class="row">
<div class="col-xs-12">
	<div class="box">
            <div class="box-body">
            <form id="form1" class="form-horizontal">
              <span id="msn"></span>
             <div class="col-md-4">
                    <div class="form-group">
                    <div class="col-md-12"><strong>Contraseña Anterior:</strong>
					<input name="anterior" id="anterior" type="password" class="form-control input-sm" data-clear-btn="true">
                    </div>
                    </div>
				<div class="form-group">
                    <div class="col-md-12"><strong>Nueva Contraseña:</strong>
					<input name="nueva" id="nueva" type="password" class="form-control input-sm" data-clear-btn="true">
                	</div>
                </div>
				<div class="form-group">
                    <div class="col-md-12"><strong>Confirmar Contraseña:</strong>
					<input name="confirmar" id="confirmar" type="password" class="form-control input-sm" data-clear-btn="true">
                    </div>
                </div>
                <div class="form-group">
                <div class="col-md-12">
                <div class="input-group">                
                <img src="modulos/cambiarcontrasena/captcha.php" id="captcha" style="width:100%"/>
                <span class="input-group-addon"><a class="btn btn-success" onClick="CRUDUSUARIOS('REFRESCARCAPTCHA','')"><i class="glyphicon glyphicon-refresh"></i></a></span>
                </div>
               </div>
               <div class="col-md-12">
                  <strong>Ingresa la palabra:</strong>
                  <input type="text" name="vericaptcha" id="vericaptcha" class="form-control input-sm"  onChange="CRUDUSUARIOS('VERICAPTCHA','','')">
                  </div>              
                 
               
                </div>
                <div class="form-group">
                    <div class="col-md-12">
					<input type="button" id="btncambio"  class="btn btn-default" onclick="CRUDUSUARIOS('CAMBIOCONTRASENA','')" style="width: 257px; cursor:pointer" value="Guardar"/>
                    </div>
                </div>
                </div>
				 </form>                
                   

             </div>
        <!-- /.box-body -->
            </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>  
</div>

         
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
  
        <div id="contenido"></div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a role="button" name="" class="btn btn-primary" id="btnguardar" rel="" onClick="CRUDUSUARIOS(this.name,'')">Guardar Cambios</a>
      </div>
    </div>
  </div>
</div>
