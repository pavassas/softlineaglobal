<?php
include('../../data/Conexion.php');
echo "<style onload=CRUDPRESUPUESTOINICIAL('CARGARLISTACIUDADES','','')></style>";
?>
<section>
<div class="row" style="background-color:#222d32">
<div class="col-md-11">
<h4><a class="text-muted"><i class="fa fa-dashboard"></i> MAESTRAS/</a> <small class="active"> LOCALIZACIÓN</small></h4>
</div>
</div>
 <div class="row">
<div class="col-xs-12">
	<div class="box">
            <div class="box-body">
                <div ng-controller="TabsDemoCtrl">
                  <uib-tabset>
                    <uib-tab heading="Ciudades" select="alertMe('CARGARLISTACIUDADES')">
                     <div class="form-group">
                     <div class="col-md-2">
                    <a class="btn btn-block btn-primary" onClick="CRUDDIVISION('CIUDAD','NUEVO','')" role="button" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    </div>
           
                    </uib-tab>
                    <uib-tab heading="Departamento" select="alertMe('CARGARLISTADEPARTAMENTOS')">
                    <div class="form-group">
                     <div class="col-xs-2">
                    <a class="btn btn-block btn-primary" onClick="CRUDDIVISION('DEPARTAMENTO','NUEVO','')" role="button" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    </div>
                     
                    </uib-tab>
                     <uib-tab heading="Paises" select="alertMe('CARGARLISTAPAISES')">
                     <div class="form-group">
                     <div class="col-xs-2">
                    <a class="btn btn-block btn-primary" onClick="CRUDDIVISION('PAIS','NUEVO','')" role="button" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    </div>
                      
                    </uib-tab>
                  </uib-tabset>

                </div>
        <!-- /.box-body -->
            </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                 <span id="msn1"></span>
                <div id="tabladatos" style="padding:10px"></div>
            </div>
        </div>
    </div>
</div>

         
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <div id="contenido"></div>
        <span id="msn"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a role="button" name="" class="btn btn-primary" id="btnguardar" rel="" onClick="CRUDDIVISION(this.name,this.rel,'')">Guardar Cambios</a>
      </div>
    </div>
  </div>
</div>
