<?php
	include ("../../data/Conexion.php");
	error_reporting(0);
	session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$percla = $dato['prf_clave_int'];
	
	$con = mysqli_query($conectar,"select * from permiso pr where pr.usu_clave_int = '".$idUsuario."' and pr.ven_clave_int = 12");
	$dato = mysqli_fetch_array($con);
	$metact = $dato['per_metodo'];

	//Como?
	//$sub = $_GET['suba']; // Aca cambiar por $_POST
	$sub = $_POST['suba']; // Aca cambiar por $_POST si

	$actividades = 0;
	$cona = mysqli_query($conectar,"select act_clave_int from actividades where est_clave_int in(1,3) and act_usu_creacion = '".$idUsuario."' and act_analisis = '".$sub."'");
	$numa = mysqli_num_rows($cona);
	if($numa>0)
	{
	   $ids = array();
	   for($a=0;$a<$numa;$a++)
	   {
		   $data = mysqli_fetch_array($cona);
		   $ida = $data['act_clave_int'];
		   $ids[] = $ida;
	   }
	   $actividades = implode(",",$ids);
	}
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//JOIN actividadinsumos a on i.act_clave_int = a.act_clave_int
// DB table to use
$table = 'actividades';
// Table's primary key
$primaryKey = 'i.act_clave_int';//'act_clave_int'

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'i.act_clave_int',
		'dt' => 'DT_RowId', 'field' => 'act_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
	array( 'db' => 'i.act_clave_int', 'dt' => 'Editar', 'field' => 'act_clave_int' ,'formatter'=>function($d,$row){
		$idUsuario= $_COOKIE["usIdentificacion"];
		if(strtoupper($row[12])=="ADMINISTRADOR" || $row[13]==$idUsuario)
		{
		return "<a class='btn btn-block btn-default btn-xs' onClick=CRUDACTIVIDADES('EDITAR','".$d."') data-toggle = 'modal' data-target='#myModal'  style='width:20px; height:20px; vertical-align:middle'><i class='glyphicon glyphicon-pencil'></i></a>";
		}
		else
		{
			return "";
		}
	}),
	array( 'db' => 'i.act_clave_int', 'dt' => 'Codigo', 'field' => 'act_clave_int' ),
	array( 'db' => 'i.act_nombre', 'dt' => 'Nombre', 'field' => 'act_nombre' ),
	array( 'db' => 't.tpp_nombre', 'dt' => 'Tipo_Proyecto', 'field' => 'tpp_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => "c.ciu_nombre", 'dt' => 'Ciudad', 'field' => 'ciu_nombre'  ),	
	array( 'db'  => 'e.est_nombre','dt' => 'Estado', 'field' => 'est_nombre' ),
	array( 'db'  => 'i.act_analisis','dt' => 'Analisis', 'field' => 'act_analisis' ),
	array( 'db'  => 'i.act_clave_int','dt' => 'Total', 'field' => 'act_clave_int', 'formatter' => function( $d, $row ) {
		global $conectar;
		$cons = mysqli_query($conectar,"SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int join insumos r on r.ins_clave_int  = a.ins_clave_int where i.act_clave_int = '".$d."'");
		$dats = mysqli_fetch_array($cons);
		$total = $dats['tot'];
		
		$cons2 = mysqli_query($conectar,"SELECT sum((a.ati_rendimiento*a.ati_valor)*ats_rendimiento) as tot FROM  act_subanalisis AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_subanalisis join insumos r on r.ins_clave_int = a.ins_clave_int where i.act_clave_int = '".$d."'");
		$dats2 = mysqli_fetch_array($cons2);
		$total2 = $dats2['tot'];
		$total = $total + $total2;		
		
            return '<div id="divapu'.$row[0].'">$'.number_format($total,2,'.',',').'</div>';
        }),
	array( 'db'  => 'i.act_clave_int','dt' => 'Suba', 'field' => 'act_clave_int','formatter'=>function($d,$row){
		global $conectar;
		    $con = mysqli_query($conectar,"select * from act_subanalisis where act_clave_int = '".$d."'");
			$num =  mysqli_num_rows($con);
			return $num;	
		} ),
	array( 'db' => 'i.act_clave_int', 'dt' => 'Eliminar', 'field' => 'act_clave_int' ,'formatter'=>function($d,$row){	
	        $idUsuario= $_COOKIE["usIdentificacion"];
			if(strtoupper($row[12])=="ADMINISTRADOR" || $row[13]==$idUsuario)
			{	
				return "<a class='btn btn-block btn-danger btn-xs' onClick=CRUDACTIVIDADES('ELIMINAR','".$d."')  style='width:20px; height:20px; vertical-align:middle'><i class='glyphicon glyphicon-trash'></i></a>";
			}
			else
			{
				return "";
			}
		}),
		array( 'db'  => "'".$perfil."'",'dt' => 'Perfil' ,'field' => 'Perfil','as'=>"Perfil"),
		array( 'db'  => 'i.act_usu_creacion','dt' => 'Creacion' ,'field' => 'act_usu_creacion'),
		array( 'db' => 'i.act_clave_int', 'dt' => 'Exportar', 'field' => 'act_clave_int' ,'formatter'=>function($d,$row){		
			return "<a class='btn btn-block btn-success btn-xs' href='modulos/actividades/actividadesexportar.php?edi=".$d."' style='width:20px; height:20px; vertical-align:middle' target='_blank' title='Exportar Apu'><i class='fa fa-file-excel-o'></i></a>";		
	})
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = ' i.act_clave_int ';
 
 if($metact==2 and $percla!=1)
	{
		$joinQuery = "FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int  LEFTJOIN departamento AS d on d.dep_clave_int = c.dep_clave_int LEFT JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int JOIN estados AS e on e.est_clave_int  = i.est_clave_int";
		$extraWhere = " (e.est_clave_int = 1 or i.act_clave_int in(".$actividades.")) and i.act_analisis = '".$sub."'";
	}
	else if($percla!=1)
	{
		$joinQuery = "FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int LEFT JOIN departamento AS d on d.dep_clave_int = c.dep_clave_int   LEFT JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int JOIN estados AS e on e.est_clave_int  = i.est_clave_int";
		$extraWhere = " (e.est_clave_int in(1) or i.act_clave_int in(".$actividades.")) and i.act_analisis = '".$sub."'";
	}
	else
	{ 
		$joinQuery = "FROM  actividades AS i  JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int  JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int  JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int  LEFT  JOIN departamento AS d on d.dep_clave_int = c.dep_clave_int  LEFT JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int  JOIN estados AS e on e.est_clave_int  = i.est_clave_int";
		$extraWhere = " e.est_clave_int not in('2','4','5') and i.act_analisis = '".$sub."'";   
	}
 
echo json_encode(
	SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
	//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

