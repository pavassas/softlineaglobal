<?php
include ("../../data/Conexion.php");
error_reporting(0);
session_start();
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario = $_COOKIE["usIdentificacion"];
$sistema = $_COOKIE['sistema'];
$clave= $_COOKIE["clave"];
date_default_timezone_set('America/Bogota');
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$perfil = strtoupper($dato['prf_descripcion']);
$percla = $dato['prf_clave_int'];
$nombreusuario = $dato['usu_nombre'];
$cla = $dato['usu_clave_int'];
$claveusuario = $dato['usu_clave_int'];
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//JOIN actividadinsumos a on i.act_clave_int = a.act_clave_int
// DB table to use
$table = 'actividades';
$sub = $_GET['suba'];
$coor = $_GET['coor'];
$acti = $_GET['acti'];
$proyecto = $_GET['proy'];

// Table's primary key
$primaryKey = 'i.act_clave_int';//'act_clave_int'

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'i.act_clave_int',
		'dt' => 'DT_RowId', 'field' => 'act_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
	array( 'db' => 'i.act_clave_int', 'dt' => 'Editar', 'field' => 'act_clave_int' ),
	array( 'db' => 'i.act_clave_int', 'dt' => 'Codigo', 'field' => 'act_clave_int' ),
	array( 'db' => 'i.act_nombre', 'dt' => 'Nombre', 'field' => 'act_nombre' ),
	array( 'db' => 't.tpp_nombre', 'dt' => 'Tipo_Proyecto', 'field' => 'tpp_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => "c.ciu_nombre", 'dt' => 'Ciudad', 'field' => 'ciu_nombre'  ),	
	array('db'  => 'e.est_nombre','dt' => 'Estado', 'field' => 'est_nombre' ),
	array('db'  => 'i.act_analisis','dt' => 'Analisis', 'field' => 'act_analisis' ),
	array('db'  => 'i.act_clave_int','dt' => 'Total', 'field' => 'act_clave_int', 'formatter' => function( $d, $row ) {
		global $conectar;
		$cons = mysqli_query($conectar,"SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int where i.act_clave_int = '".$d."'");
		$dats = mysqli_fetch_array($cons);
		$total = $dats['tot'];
		
		$cons2 = mysqli_query($conectar,"SELECT sum((a.ati_rendimiento*a.ati_valor)*ats_rendimiento) as tot FROM  act_subanalisis AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_subanalisis where i.act_clave_int = '".$d."'");
		$dats2 = mysqli_fetch_array($cons2);
		$total2 = $dats2['tot'];
		$total = $total + $total2;		
            return '<div id="divapu'.$row[0].'">$'.number_format($total,2,',','.').'</div>';
        }),
	array('db'  => 'us.usu_nombre','dt' => 'Usuario', 'field' => 'usu_nombre' ),
	array('db'  => 'i.act_fec_creacion','dt' => 'Fecha', 'field' => 'act_fec_creacion' ),
	array('db'  => 'pr.pre_coordinador','dt' => 'Coordinador', 'field' => 'pre_coordinador','formatter'=>function($d,$row){	
		global $conectar;
	     if($d!="" and $d!=NULL and $d>0)
		 {			 
	     	$consu = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.usu_clave_int = '".$d."' LIMIT 1");
		 	$datsu = mysqli_fetch_array($consu);
			$nom = $datsu['usu_nombre'];
			return $nom;
		 }
		 else
		 {
			return  "No tiene";
		 }
	} ),
	array('db'  => 'pr.pre_nombre','dt' => 'Presupuesto', 'field' => 'pre_nombre' ),
	
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = ' i.act_clave_int ';
 
 	$joinQuery = "FROM  actividades AS i  LEFT JOIN presupuesto pr on pr.pre_clave_int = i.pre_clave_int JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int  JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int LEFT JOIN departamento AS d on d.dep_clave_int = c.dep_clave_int  LEFT JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int join estados AS e on e.est_clave_int  = i.est_clave_int join usuario AS us on us.usu_clave_int = i.act_usu_creacion";
if($perfil=="ADMINISTRADOR")
{
	$extraWhere = " e.est_clave_int in('3') and i.act_analisis = '".$sub."' and (pr.pre_coordinador = '".$coor."' OR '".$coor."' IS NULL or '".$coor."' = '') and (i.act_nombre LIKE REPLACE('%".$acti."%',' ','%')  OR '".$acti."' IS NULL OR '".$acti."' = '') and (pr.pre_clave_int = '".$proyecto."' OR '".$proyecto."' IS NULL or '".$proyecto."' = '')";   
}
else if($perfil=="COORDINADOR")
{
	$extraWhere = " e.est_clave_int in('3') and i.act_analisis = '".$sub."' and (pr.pre_coordinador = '".$coor."' OR '".$coor."' IS NULL or '".$coor."' = '') and (i.act_nombre LIKE REPLACE('%".$acti."%',' ','%')  OR '".$acti."' IS NULL OR '".$acti."' = '') and (pr.pre_clave_int = '".$proyecto."' OR '".$proyecto."' IS NULL or '".$proyecto."' = '') and( i.act_usu_creacion = '".$idUsuario."' or i.act_usu_creacion in(SELECT uu.usu_clave_int FROM usuario uu WHERE uu.usu_coordinador = '".$idUsuario."'))";  
}

 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

