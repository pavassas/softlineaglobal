<?php
include("../../data/Conexion.php");

$table = 'actividades';

// Table's primary key
$primaryKey = 'd.ats_clave_int';//'act_clave_int'
$id = $_GET['id'];
$columns = array(
	array(
		'db' => 'd.ats_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ats_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_actp'.$d;
		}
	),
	array( 'db' => 'd.ats_clave_int', 'dt' => 'Delete', 'field' => 'ats_clave_int','formatter'=>function($d,$row){
	   return "<a role='button' class='btn btn-danger btn-xs' style='width:20px; height:20px' onClick=CRUDACTIVIDADES('DELETESUB','".$d."')><i class='fa fa-trash'></i></a>";	
	}),
	array( 'db' => 'a.act_clave_int', 'dt' => 'Actividad', 'field' => 'act_clave_int' ),
	array( 'db' => 'a.act_nombre', 'dt' => 'Nombre', 'field' => 'act_nombre' ),
	array( 'db' => 'd.ats_rendimiento', 'dt' => 'Rendimiento', 'field' => 'ats_rendimiento','formatter'=>function($d,$row){
	   return $d;	
	}),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => 't.tpp_nombre ','dt'=>'Tipo', 'field' => 'tpp_nombre'),
	array( 'db'  => 'c.ciu_nombre','dt' => 'Ciudad', 'field' => 'ciu_nombre')
	);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = ' d.ats_clave_int ';
 $joinQuery = " FROM  actividades a join act_subanalisis d on a.act_clave_int = d.act_subanalisis join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join unidades  u on u.uni_clave_int  = a.uni_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int";
$extraWhere =  " d.act_clave_int = '".$id."'";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

