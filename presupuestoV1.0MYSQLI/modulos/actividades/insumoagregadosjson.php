<?php
include("../../data/Conexion.php");

$table = 'insumos';

// Table's primary key
$primaryKey = 'd.ati_clave_int';//'act_clave_int'
$id = $_GET['id'];
$columns = array(
	array(
		'db' => 'd.ati_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ati_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_insp'.$d;
		}
	),
	array( 'db' => 'd.ati_clave_int', 'dt' => 'Delete', 'field' => 'ati_clave_int','formatter'=>function($d,$row){
	   return "<a role='button' class='btn btn-danger btn-xs' style='width:20px; height:20px' onClick=CRUDACTIVIDADES('DELETE','".$d."')><i class='fa fa-trash'></i></a>";	
	}),
	array( 'db' => 'a.act_clave_int', 'dt' => 'Actividad', 'field' => 'act_clave_int' ),
	array( 'db' => 'i.ins_nombre', 'dt' => 'Nombre', 'field' => 'ins_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => 't.tpi_nombre ','dt'=>'Tipo', 'field' => 'tpi_nombre'),
	array( 'db'  => 'i.ins_valor','dt' => 'Valor', 'field' => 'ins_valor','formatter'=>function($d,$row){
		return "<span class='currency'>$ ".number_format($d,2,'.',',')."</span>";
		} ),
	array( 'db'  => 'd.ati_valor','dt' => 'Valor1', 'field' => 'ati_valor','formatter'=>function($d,$row){
		return "<span class='currency'>$ ".number_format($d,2,'.',',')."</span>";
		} ),
	array( 'db' => 'd.ati_rendimiento', 'dt' => 'Rendimiento', 'field' => 'ati_rendimiento','formatter'=>function($d,$row){
		if(round($d,2)<=0){ $d = rtrim($d,'0');} else { $d = number_format($d,2,'.',','); } 
		
		return "<input  style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remp2r".$row[0]."' type='text' value='".$d."'   onKeyPress='return NumCheck(event, this)' min='0' onkeyup=guardarrendimiento2('".$row[0]."',this.value,'".$row[2]."') onchange=guardarrendimiento2('".$row[0]."',this.value,'".$row[2]."')><br><span id='sp2r".$row[0]."'></span>";
		 //return $d;
		} ),
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = 'i.ins_clave_int';
 $joinQuery = " FROM  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int";
$extraWhere =  " a.act_clave_int = '".$id."'";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

