<?php
include("../../data/Conexion.php");
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];

$table = 'actividades';

// Table's primary key
$primaryKey = 'a.act_clave_int';//'act_clave_int'

$columns = array(
	array(
		'db' => 'a.act_clave_int',
		'dt' => 'DT_RowId', 'field' => 'act_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowa_'.$d;
		}
	),
	array( 'db' => 'a.act_clave_int', 'dt' => 'Agregar', 'field' => 'act_clave_int','formatter'=>function($d,$row){
	   return "<a role='button' title='Agregar' class='btn btn-default btn-xs' style='width:20px; height:20px' onClick=CRUDACTIVIDADES('AGREGARPENDIENTESUB','".$d."')><i class='fa fa-plus'></i></a>";	
	}),
	array( 'db' => 'a.act_clave_int', 'dt' => 'Rendimiento', 'field' => 'act_clave_int','formatter'=>function($d,$row){
	   return "<input type='text' id='renda".$d."' onKeyPress='return NumCheck(event, this)' style='width:80px'/>";	
	}),
	array( 'db' => 'a.act_nombre', 'dt' => 'Nombre', 'field' => 'act_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => 't.tpp_nombre ','dt'=>'Tipo', 'field' => 'tpp_nombre'),
	array( 'db' => 'c.ciu_nombre','dt' => 'Ciudad', 'field' => 'ciu_nombre')
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = ' a.act_clave_int ';
 $joinQuery = " FROM actividades a  JOIN actividadinsumos AS i on a.act_clave_int = i.act_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join unidades  u on u.uni_clave_int  = a.uni_clave_int join ciudad c on c.ciu_clave_int =  a.ciu_clave_int";
$extraWhere = " (a.est_clave_int not in('2','3','4','5') or a.act_clave_int in( select act_clave_int from actividades where  act_usu_creacion ='".$idUsuario."' and est_clave_int  not in('2','4','5') and act_analisis ='Si')) and a.act_analisis = 'Si' and a.act_clave_int not in(select act_clave_int  from act_sub_pendientes where usu_clave_int = '".$idUsuario."')";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

