<?php
session_start();
include('../../data/Conexion.php');
$idUsuario = $_COOKIE["usIdentificacion"];
$con = mysqli_query($conectar,"select * from permiso pr where pr.usu_clave_int = '".$idUsuario."' and pr.ven_clave_int = 3");
$dato = mysqli_fetch_array($con);
$metact = $dato['per_metodo'];

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$cla = $dato['usu_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$nombreusuario = $dato['usu_nombre'];
$percla = $dato['prf_clave_int'];
$perfil = strtoupper($dato['prf_descripcion']);
echo "<style onload=CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')></style>";
?>

<section>
<div class="row" style="background-color:#222d32">
<div class="col-md-12">
<h4><a class="text-muted"><i class="fa fa-dashboard"></i> MAESTRAS <small class="active">/ ACTIVIDADES</small><small class="active" id="titleactividad">/ POR APROBAR</small></a></h4>
</div>
</div>
   
        <div class="box">
            <div class="box-body">
                <span id="msn1"></span>
                <div class="row" id="filtroporabrobar">
                    <div class="col-md-2">
                        <label>Tipo:</label>
                        <select id="opcanalisis" class="form-control input-sm" onChange="CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')">
                            <option value="No">Actividades</option>
                            <option value="Si">Sub Analisis</option>
                        </select></div>
                    <div class="col-md-3">
                        <label>Coordinador:</label>
                        <select  name="buscoordinador" id="buscoordinador" class="form-control input-sm selectpicker" onChange="CRUDACTIVIDADES('CARGARPROYECTOS','')" data-live-search="true" data-live-search-placeholder="Buscar Coordinador" data-actions-box="true">
                            <option value="">--seleccione--</option>
                            <?php
                            if($perfil=="COORDINADOR")
                            {
                                $con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR') and usu_clave_int ='".$idUsuario."'");
                            }
                            else
                            {
                                $con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
                            }
                            while($dat = mysqli_fetch_array($con))
                            {
                                $idp = $dat['usu_clave_int'];
                                $nom = $dat['usu_nombre'];
                                $ema = $dat['usu_email'];

                                ?>
                                <option <?php if($idp==$sel){ echo "selected"; }?> value="<?php echo $idp;?>"><?Php echo $nom." - ".$ema;?></option>
                                <?php

                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Proyecto:</label>
                        <select name="busproyecto" id="busproyecto" class="form-control input-sm selectpicker" onChange="CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')" data-live-search="true" data-live-search-placeholder="Buscar Proyecto" data-actions-box="true">
                            <option value="">--seleccione--</option>
                            <?php
                            $con = mysqli_query($conectar,"select p.pre_clave_int,pre_nombre,tpp_nombre,ciu_nombre from presupuesto p join actividades a on a.pre_clave_int = p.pre_clave_int join tipoproyecto t on t.tpp_clave_int = p.tpp_clave_int join ciudad c on p.ciu_clave_int = c.ciu_clave_int where a.est_clave_int = 3 GROUP BY p.pre_clave_int ORDER BY p.pre_nombre");
                            while($dat = mysqli_fetch_array($con))
                            {
                                $idpr = $dat['pre_clave_int'];
                                $nom = $dat['pre_nombre']." - ".$dat['tpp_nombre']." - ".$dat['ciu_nombre'];

                                ?>
                                <option value="<?php echo $idpr;?>"><?Php echo $nom;?></option>
                                <?php

                            }
                            ?>
                        </select>
                    </div>


                    <div class="col-md-3">
                        <label>Actividad:</label>
                        <input type="text" id="busactividad"  onChange="CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')" value="" class="form-control input-sm"/>
                    </div>
                </div>
                <div id="tabladatos" class="col-md-12"></div>
            </div>
        </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
     
        <div id="contenido"></div>
         <span id="msn"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a role="button" name="" class="btn btn-primary" id="btnguardar" rel="" onClick="CRUDACTIVIDADES(this.name,this.rel)">Guardar Cambios</a>
      </div>
    </div>
  </div>
</div>
