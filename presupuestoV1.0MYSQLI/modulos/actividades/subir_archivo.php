<?php
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
include ("../../data/Conexion.php");
require_once '../../Classes/PHPExcel/IOFactory.php';
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

if (isset($_FILES['archivo'])) 
{
	$time = time();
	$tempPath = $_FILES[ 'archivo' ][ 'tmp_name' ];
	$nameEXC = $_FILES['archivo']['name'];
	$dir = "xls/".$nameEXC;
    $uploadPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'xls' . DIRECTORY_SEPARATOR . $_FILES[ 'archivo' ][ 'name' ];

	if(move_uploaded_file( $tempPath, $uploadPath ))
	{
	
		$objPHPEXCEL=PHPExcel_IOFactory::load($dir);
		$objHoja=$objPHPEXCEL->getActiveSheet()->toArray(null,true,true,true,true,true,true,true,true,true);
		$act =  ""; $ida = 0;
		$totalregistros = $objHoja->sheets[0]['numRows']; 
		foreach($objHoja as $iIndidce => $objCelda)
		{
			$COLA = $objCelda['A']; $COLB = $objCelda['B']; $COLC = $objCelda['C']; 
			$COLD = $objCelda['D']; $COLE = $objCelda['E']; $COLI = $objCelda['I'];
			$COLJ = $objCelda['J']; 
			
			$COLF = $objCelda['F']; $COLG = $objCelda['G']; $COLH = $objCelda['H'];
			 
			if($COLA=="ID" || $COLA=="" || $COLA==NULL || $COLB=="DESCRIPCIÓN" || $COLC=="UNIDAD" || $COLD=="RENDIM" || $COLE=="PRECIO" || $COLI=="CIUDAD" || $COLJ=="TIPO PROYECTO"  || $COLC=="" || $COLB=="")
			{
				
			}
			else
			{
				$COLA = number_format($COLA,0,'','');
				if($COLD=="" || $COLD==NULL || $COLD<=0){$act = $COLB; $ins="";}else {$ins = $COLA;}
				//VERIFICAR UNIDADES 
				$veru = mysqli_query($conectar,"select uni_clave_int from unidades where UPPER(uni_codigo)=UPPER('".$COLC."') limit 1");
				$numu = mysqli_num_rows($veru);
				if($numu>0){$datu = mysqli_fetch_array($veru); $idu = $datu['uni_clave_int'];}
				else 
				{
				$ins = mysqli_query($conectar,"insert into unidades (uni_codigo,uni_nombre,est_clave_int,uni_usu_actualiz,uni_fec_actualiz) values('".$COLC."','".$COLC."',1,'".$usuario."','".$fecha."')"); 
				$idu = mysqli_insert_id($conectar);
				}
			
				//VERIFICAR CIUDAD //DETARMENTO COD 36 PARA LAS CIUDADES QUE NO EXISTEN
				if($COLD=="" || $COLD==NULL || $COLD<=0)
				{
					$verc = mysqli_query($conectar,"select ciu_clave_int from ciudad where UPPER(ciu_nombre)=UPPER('".$COLI."') limit 1");
					$numc = mysqli_num_rows($verc);
					if($numc>0){$datc = mysqli_fetch_array($verc); $idc = $datc['ciu_clave_int'];}
					else 
					{		
					$insc = mysqli_query($conectar,"insert into ciudad (ciu_nombre,dep_clave_int,est_clave_int,ciu_usu_actualiz,ciu_fec_actualiz) values('".$COLI."','36',1,'".$usuario."','".$fecha."')"); 
					$idc = mysqli_insert_id($conectar);
					}
					//ACTUALIZA O INSERTA TIPO PROYECTO IF NO EXISTEN
					$vert = mysqli_query($conectar,"select tpp_clave_int from tipoproyecto where UPPER(tpp_nombre)=UPPER('".$COLJ."') limit 1");
					$numt = mysqli_num_rows($vert);
					if($numt>0){$datt = mysqli_fetch_array($vert); $tpp = $datt['tpp_clave_int'];}
					else 
					{		
					$inst = mysqli_query($conectar,"insert into tipoproyecto (tpp_nombre,est_clave_int,tpp_usu_actualiz,tpp_fec_actualiz) values('".$COLJ."',1,'".$usuario."','".$fecha."')"); 
					$tpp = mysqli_insert_id($conectar);
					}
					//ACTUALIZA O INSERTA ACTIVIDADES IF NO EXISTEN
					$vera = mysqli_query($conectar,"select * from actividades where act_nombre='".$COLB."'");
					$numa = mysqli_num_rows($vera);
					if($numa>0)
					{
					   if($numa==1){$act = "".$act."-".$numa."";}else {$act = "".$act."-".($numa+1)."";}    	
					    $inserta = mysqli_query($conectar,"insert into actividades (act_clave_int,act_nombre,tpp_clave_int,uni_clave_int,act_analisis,est_clave_int,ciu_clave_int,act_usu_actualiz,act_fec_actualiz) values('NULL','".$act."','".$tpp."','".$idu."','No','1','".$idc."','".$usuario."','".$fecha."')");
						$ida = mysqli_insert_id($conectar);
						
					}
					else
					{
					   $inserta = mysqli_query($conectar,"insert into actividades (act_clave_int,act_nombre,tpp_clave_int,uni_clave_int,act_analisis,est_clave_int,ciu_clave_int,act_usu_actualiz,act_fec_actualiz) values('NULL','".$COLB."','".$tpp."','".$idu."','No','1','".$idc."','".$usuario."','".$fecha."')");
					   $ida = mysqli_insert_id($conectar);
					}
				}
				else 
				{
					$idc = ""; $tpp="";
					if($COLF>0){$tpi=2;}else if($COLG>0){$tpi=1;}else if($COLH>0){$tpi=3;}else {$tpi=4;}
				//insercion de insumos y actividad asociadas
					$veri = mysqli_query($conectar,"select ins_clave_int from insumos where ins_clave_int = '".$COLA."' limit 1");
					$numi  = mysqli_num_rows($veri); 
					if($numi>0)
					{
						$dati = mysqli_fetch_array($veri);
						$idi = $dati['ins_clave_int'];
					}
					else
					{
						$ins = mysqli_query($conectar,"insert into insumos(ins_clave_int,ins_nombre,tpi_clave_int,uni_clave_int,ins_valor,est_clave_int,ins_usu_actualiz,ins_fec_actualiz) value('".$COLA."','".$COLB."','".$tpi."','".$idu."','".$COLE."','1','".$usuario."','".$fecha."')");
					$idi = mysqli_insert_id($conectar);
					
					}
			  //verificar detalle
					$verif = mysqli_query($conectar,"select ati_clave_int from actividadinsumos where act_clave_int ='".$ida."' and ins_clave_int ='".$idi."'");
				  $numvf = mysqli_num_rows($verif);
				  if($numvf>0)
				  {
					  $datf = mysqli_fetch_array($verif);
					  $idd = $datf['ati_clave_int'];
					  $updateai = mysqli_query($conectar,"update actividadinsumos set ati_rendimiento= '".$COLD."',act_valor='".$COLE."' where ati_clave_int = '".$idd."'");
					  
				  }
			  else{
				  $insertai = mysqli_query($conectar,"insert into actividadinsumos(act_clave_int,ins_clave_int,ati_rendimiento,ati_valor,ati_usu_actualiz,ati_fec_actualiz) values('".$ida."','".$idi."','".$COLD."','".$COLE."','".$usuario."','".$fecha."')");
				  $idd = mysqli_insert_id($conectar);			 
				 }
			  }		
			
			/*$veri = mysqli_query($conectar,"select * from pruebas where COLA='".$COLA."'");
			$numv = mysqli_num_rows($veri);
				if($numv<=0)
				{
				  
				$ins = mysqli_query($conectar,"INSERT INTO pruebas(COLA,COLB,COLC,COLD,COLE,COLI,COLJ) values('".$COLA."','".$COLB."','".$idu."','".$COLD."','".$COLE."','".$COLI."','".$COLJ."')");
				}*/
		   }
		}
		echo 1;
	}
	else
	{	
	   echo 0;
	}
}
?>
