<?php
include("../../data/Conexion.php");

$table = 'insumos';

// Table's primary key
$primaryKey = 'i.ins_clave_int';//'act_clave_int'
$id = $_GET['id'];
$pre = $_GET['pre'];
$gru = $_GET['gru'];
$cap = $_GET['cap'];
$columns = array(
	array(
		'db' => 'i.ins_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ins_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowi_'.$d;
		}
	),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Agregar', 'field' => 'ins_clave_int','formatter'=>function($d,$row){
	   return "<a role='button' title='Agregrar' class='btn btn-default btn-xs' style='width:20px; height:20px' onClick=CRUDPRESUPUESTOINICIAL('AGREGARAPU','".$d."','','','')><i class='fa fa-plus'></i></a>";	
	}),
	array( 'db' => 'i.ins_nombre', 'dt' => 'Nombre', 'field' => 'ins_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array('db' => 't.tpi_nombre ','dt'=>'Tipo', 'field' => 'tpi_nombre'),
	array('db'  => 'i.ins_valor','dt' => 'Valor', 'field' => 'ins_valor','formatter'=>function($d,$row){
		return "<span class='currency'>$ ".number_format($d,2,'.',',')."</span>";
		} ),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Rendimiento', 'field' => 'ins_clave_int','formatter'=>function($d,$row){
		 return "<input type='text' id='rend".$d."' onKeyPress='return NumCheck(event, this)' style='width:80px'/>";
		} ),
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = ' i.ins_clave_int ';
 $joinQuery = " FROM insumos i join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int";
$extraWhere = " i.est_clave_int = 1 and i.ins_clave_int not in(select distinct ins_clave_int from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$id."')";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

