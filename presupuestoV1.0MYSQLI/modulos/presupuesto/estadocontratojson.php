<?php
include("../../data/Conexion.php");
error_reporting(0);
$table = 'estados_contrato';

// Table's primary key
$primaryKey = 'e.esc_clave_int';//'act_clave_int'

$pre = $_POST['pre'];

$columns = array(
	array(
		'db' => 'e.esc_clave_int',
		'dt' => 'DT_RowId', 'field' => 'esc_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowec_'.$d;
		}
	),//0
	array( 'db' => 'e.esc_clave_int', 'dt' => 'Delete', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
	   return "<a title='' role='button' class='btn btn-danger btn-xs' style='width:20px; height:20px' onClick=CONTROLESTADOS('ELIMINAR','".$d."')><i class='fa fa-trash'></i></a>";
	}),//1
    array( 'db' => 'e.esc_orden', 'dt' => 'Orden', 'field' => 'esc_orden','formatter'=>function($d,$row){
       $input = "<input onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."')  type='text' name='ordc".$row[1]."' id='ordc".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:left' value='".$d."'/>";
       return $input;
    } ),//2
    array( 'db' => 'e.esc_contrato', 'dt' => 'Contrato', 'field' => 'esc_contrato','formatter'=>function($d,$row){
       $input = "<input onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."')  type='text' name='cont".$row[1]."' id='cont".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:left' value='".$d."'/>";
       $list = "";
        return $input;
    }  ),//3
	array( 'db' => 'e.gru_clave_int', 'dt' => 'Grupo', 'field' => 'gru_clave_int','formatter'=>function($d,$row){
        global $conectar;
	    $select = "<select  onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') id='grue".$row[1]."' style='background-color:transparent; border:thin; width:80px; cursor:pointer'  data-width='80px' class='form-control input-sm selectpicker'>
      <option value=''></option>";

        $cong = mysqli_query($conectar,"select distinct g.gru_clave_int gru, g.gru_nombre nog from grupos g where est_clave_int = 1 order by g.gru_nombre");
        $maxg = 0;
        while($datg = mysqli_fetch_array($cong))
        {
            $idg = $datg['gru'];
            $nom = $datg['nog'];
            if($idg>$maxg){$maxg=$idg;}
            if ($idg==$d){ $sel =  'selected';}else{$sel = "";}
            $select.="<option title='".$idg."' ".$sel."   value='".$idg."'>".$nom."</option>";
        }
        $select.="</select>";
        return $select;

    }  ),//4
	array( 'db' => 'e.esc_contratista', 'dt' => 'Contratista', 'field' => 'esc_contratista','formatter'=>function($d,$row){
        $len = strlen($d); $len*=7; if($len<=100){$len=100;}
        $input ="<input onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='contra".$row[1]."' id='contra".$row[1]."'
         style='background-color:transparent; border:thin; width:".$len."px; cursor:pointer; text-align:left' value='".$d."' onKeyUp= ajustar(this.id) >";
        return $input;

    }  ),//5
	array( 'db' => 'e.esc_nit','dt'=>'Nit', 'field' => 'esc_nit','formatter'=>function($d,$row){
        $len = strlen($d); $len*=7; if($len<=100){$len=100;}
        $input ="<input onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='nite".$row[1]."' id='nite".$row[1]."'
         style='background-color:transparent; border:thin; width:".$len."px; cursor:pointer; text-align:left' value='".$d."' onKeyUp= ajustar(this.id) >";
        return $input;

    } ),//6
	array( 'db'  => 'e.esc_obj_contrato','dt' => 'Objeto', 'field' => 'esc_obj_contrato','formatter'=>function($d,$row){
        $len = strlen($d); $len*=7; if($len<=100){$len=100;}
        $input ="<input onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='obje".$row[1]."' id='obje".$row[1]."'
         style='background-color:transparent; border:thin; width:".$len."px; cursor:pointer; text-align:left' value='".$d."' onKeyUp= ajustar(this.id) >";
        return $input;
		} ),//7
    array( 'db' => 'e.esc_val_inicial','dt'=>'ValorInicial', 'field' => 'esc_val_inicial','formatter'=>function($d,$row){
        $input = "<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='vali".$row[1]."' id='vali".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  value='$".number_format($d,2,'.',',')."' class='currency'>";
        return $input;
    } ),//8
    array( 'db' => 'e.esc_iva','dt'=>'Iva', 'field' => 'esc_iva','formatter'=>function($d,$row){
        $input  ="<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='iv".$row[1]."' id='iv".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='$".number_format($d,2,'.',',')."'' class='currency'>";
        return $input;
    } ),//9
    array( 'db' => 'e.esc_vr_otrosi','dt'=>'VrOtro', 'field' => 'esc_vr_otrosi','formatter'=>function($d,$row){
        $input  ="<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='valos".$row[1]."' id='valos".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='$".number_format($d,2,'.',',')."'' class='currency'>";
        return $input;
    } ),//10
    array( 'db' => 'e.esc_iva_otrosi ','dt'=>'IvaOtro', 'field' => 'esc_iva_otrosi','formatter'=>function($d,$row){
        $input  ="<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='ivos".$row[1]."' id='ivos".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='$".number_format($d,2,'.',',')."' class='currency'>";
        return $input;
    } ),//11
    array( 'db' => 'e.esc_clave_int', 'dt' => 'VrFinal', 'field' => 'esc_clave_int','formatter'=>function($d,$row){

        $vf = $row[8] + $row[9] + $row[10]+ $row[11];
        return  "<span id='vf".$row[1]."' class='currency'>".number_format($vf,2,'.',',')."</span>";

    }),//12
    array( 'db' => 'e.esc_clave_int', 'dt' => 'Anticipo', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_anticipo) as ant from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $ant = $dato['ant'];
        $sql = "select sum(cpe_anticipo) as ant from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))";

        return  "<span title='".$sql."' id='ant".$row[1]."' class='currency'>".number_format($ant,2,'.',',')."</span>";


    }),//13
    array( 'db' => 'e.esc_clave_int', 'dt' => 'AnticipoAmo', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_amortizacion) as amo from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $amo = $dato['amo'];
        return  "<span id='amo".$row[1]."' class='currency'>".number_format($amo,2,'.',',')."</span>";

    }),//14
    array( 'db' => 'e.esc_clave_int', 'dt' => 'SaldoAnticipo', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $ant = $dato['ant'];
        $amo = $dato['amo'];
        $sala = $ant - $amo;
        return  "<span id='sala".$row[1]."' class='currency'>".number_format($sala,2,'.',',')."</span>";

    }),//15
    array( 'db' => 'e.esc_clave_int', 'dt' => 'RetGarantia', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_ret_garantia) gar from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $ret = $dato['gar'];
         return  "<span id='ret".$row[1]."' class='currency'>".number_format($ret,2,'.',',')."</span>";

    }),//16
    array( 'db' => 'e.esc_deducciones','dt'=>'Deducciones', 'field' => 'esc_deducciones','formatter'=>function($d,$row){
        $input  ="<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='dedu".$row[1]."' id='dedu".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='$".number_format($d,2,'.',',')."'' class='currency'>";
        return $input;
    }),//17
    array( 'db' => 'e.esc_deducciones', 'dt' => 'Neto', 'field' => 'esc_deducciones','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo,sum(cpe_iva) iv,sum(cpe_ret_garantia) gar,sum(cpe_valor_neto) net from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $ant = $dato['ant'];
        $amo = $dato['amo'];
        $iva = $dato['iv'];
        $ret = $dato['gar'];
        $net = $dato['net'];
        $bru = $net + $iva;
        $sala = $ant - $amo;
        $nett = $bru - $amo - $ret - $d;
        return  "<span id='nett".$row[1]."' class='currency'>".number_format($nett,2,'.',',')."</span>";

    }),//18
    array( 'db' => 'e.esc_clave_int', 'dt' => 'Facturado', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_valor_neto) net from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $net = $dato['net'];
        return  "<span id='neto".$row[1]."' class='currency'>".number_format($net,2,'.',',')."</span>";

    }),//19

    array( 'db' => 'e.esc_clave_int', 'dt' => 'IvaFacturador', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_iva) iv from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $iva = $dato['iv'];
        return  "<span id='iva".$row[1]."' class='currency'>".number_format($iva,2,'.',',')."</span>";

    }),//20
    array( 'db' => 'e.esc_clave_int', 'dt' => 'TotalFacturado', 'field' => 'esc_clave_int','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_iva) iv,sum(cpe_valor_neto) net from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $iva = $dato['iv'];
        $net = $dato['net'];
        $bru = $net + $iva;
        return  "<span id='tot".$row[1]."' class='currency'>".number_format($bru,2,'.',',')."</span>";

    }),//21
    array( 'db' => 'e.esc_deducciones', 'dt' => 'SaldoContrato', 'field' => 'esc_deducciones','formatter'=>function($d,$row){
        global $conectar;
        $condatos = mysqli_query($conectar,"select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo,sum(cpe_iva) iv,sum(cpe_ret_garantia) gar,sum(cpe_valor_neto) net from control_egreso where pre_clave_int = '".$_POST['pre']."'  and (cpe_documento = '".$row[6]."' and UPPER(cpe_beneficiario) = UPPER('".$row[5]."-".$row[3]."'))");//and gru_clave_int = '".$gru."'
        $dato = mysqli_fetch_array($condatos);
        $ant = $dato['ant'];
        $amo = $dato['amo'];
        $iva = $dato['iv'];
        $ret = $dato['gar'];
        $net = $dato['net'];
        $bru = $net + $iva;
        $sala = $ant - $amo;
        $vf = $row[8] + $row[9] + $row[10]+ $row[11];
        $nett = $bru - $amo - $ret - $d;
        $salc = $vf - $bru;
        return  "<span id='salc".$row[1]."' class='currency'>".number_format($salc,2,'.',',')."</span>";

    }),//12

    array( 'db' => 'e.esc_fec_inicio','dt'=>'fei', 'field' => 'esc_fec_inicio','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='fei".$row[1]."' id='fei".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_fec_fin','dt'=>'fef', 'field' => 'esc_fec_fin','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='fef".$row[1]."' id='fef".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'esc_cum_asegu','dt'=>'aseguc', 'field' => 'esc_cum_asegu','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' name='aseguc".$row[1]."' type='text' id='aseguc".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') value='".$d."' maxlength='6'>";

    }),
    array( 'db' => 'e.esc_cum_valor','dt'=>'valorc', 'field' => 'esc_cum_valor','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='valorc".$row[1]."' id='valorc".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  value='$".number_format($d,2,'.',',')."' class='currency'>";
    }),
    array( 'db' => 'e.esc_cum_inicio','dt'=>'inicioc', 'field' => 'esc_cum_inicio','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='inicioc".$row[1]."' id='inicioc".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_cum_fin','dt'=>'finc', 'field' => 'esc_cum_fin','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='finc".$row[1]."' id='finc".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_sal_asegu','dt'=>'asegus', 'field' => 'esc_sal_asegu','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' name='asegus".$row[1]."' type='text' id='asegus".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') value='".$d."' maxlength='6'>";

    }),
    array( 'db' => 'e.esc_sal_valor','dt'=>'valors', 'field' => 'esc_sal_valor','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='valors".$row[1]."' id='valors".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  value='$".number_format($d,2,'.',',')."' class='currency'>";
    }),
    array( 'db' => 'e.esc_sal_inicio','dt'=>'inicios', 'field' => 'esc_sal_inicio','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='inicios".$row[1]."' id='inicios".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_sal_fin','dt'=>'fins', 'field' => 'esc_sal_fin','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='fins".$row[1]."' id='fins".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_bue_asegu','dt'=>'asegub', 'field' => 'esc_bue_asegu','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' name='asegub".$row[1]."' type='text' id='asegub".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') value='".$d."' maxlength='6'>";

    }),
    array( 'db' => 'e.esc_bue_valor','dt'=>'valorb', 'field' => 'esc_bue_valor','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='valorb".$row[1]."' id='valorb".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  value='$".number_format($d,2,'.',',')."' class='currency'>";
    }),
    array( 'db' => 'e.esc_bue_inicio','dt'=>'iniciob', 'field' => 'esc_bue_inicio','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='iniciob".$row[1]."' id='iniciob".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_bue_fin','dt'=>'finb', 'field' => 'esc_bue_fin','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='finb".$row[1]."' id='finb".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_res_asegu','dt'=>'asegur', 'field' => 'esc_res_asegu','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' name='asegur".$row[1]."' type='text' id='asegur".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') value='".$d."' maxlength='6'>";

    }),
    array( 'db' => 'e.esc_res_valor','dt'=>'valorr', 'field' => 'esc_res_valor','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='valorr".$row[1]."' id='valorr".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  value='$".number_format($d,2,'.',',')."' class='currency'>";
    }),
    array( 'db' => 'e.esc_res_inicio','dt'=>'inicior', 'field' => 'esc_res_inicio','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='inicior".$row[1]."' id='inicior".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_res_fin','dt'=>'finr', 'field' => 'esc_res_fin','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='finr".$row[1]."' id='finr".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_est_asegu','dt'=>'asegue', 'field' => 'esc_est_asegu','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' name='asegue".$row[1]."' type='text' id='asegue".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') value='".$d."' maxlength='6'>";

    }),
    array( 'db' => 'e.esc_est_valor','dt'=>'valore', 'field' => 'esc_est_valor','formatter'=>function($d,$row){
        return "<input onKeyPress='return NumCheck(event, this)' onchange=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='text' name='valore".$row[1]."' id='valore".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center'  value='$".number_format($d,2,'.',',')."' class='currency'>";
    }),
    array( 'db' => 'e.esc_est_inicio','dt'=>'inicioe', 'field' => 'esc_est_inicio','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='inicioe".$row[1]."' id='inicioe".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_est_fin','dt'=>'fine', 'field' => 'esc_est_fin','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='fine".$row[1]."' id='fine".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";

    }),
    array( 'db' => 'e.esc_fec_acta','dt'=>'fechal', 'field' => 'esc_fec_acta','formatter'=>function($d,$row){
        if($d=="0000-00-00"){$d="";}
        return "<input onblur=CONTROLESTADOS('ACTUALIZAR','".$row[1]."') type='date' class='fecha' name='fechal".$row[1]."' id='fechal".$row[1]."' style='background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center' value='".$d."' />";
    }),
    array( 'db' => 'e.esc_clave_int', 'dt' => 'Clave', 'field' => 'esc_clave_int')
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = 'e.esc_clave_int';
 $joinQuery = " FROM  estados_contrato e";
 $extraWhere =  " e.pre_clave_int = '".$_POST['pre']."'";
 
echo json_encode(
	SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

