<?php
session_start();
include ("../../data/Conexion.php");
error_reporting(0);
date_default_timezone_set('America/Bogota');

// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario = $_COOKIE["usuario"];
$idUsuario = $_COOKIE["usIdentificacion"];
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$percla = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$usuarios = 0;
$con = mysqli_query($conectar,"select usu_clave_int from usuario where usu_coordinador = '".$idUsuario."'");
$num = mysqli_num_rows($con);
if($num>0)
{
	$idu = array();
    for($u=0;$u<$num;$u++)
	{
		$dat = mysqli_fetch_array($con);
		$usu = $dat['usu_clave_int'];
		$idu[] = $usu;
	}
	$usuarios = implode(",",$idu);
}

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
$tipoproyecto  =$_GET['tipoproyecto'];
$nombre = $_GET['nombre'];
$fec = $_GET['fec'];
$cliente = $_GET['cliente'];
$pre = $_GET['pre'];
		
// DB table to use
$table = 'informebk';
// Table's primary key
$primaryKey = 'i.inf_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
	array(
		'db' => 'i.inf_clave_int',
		'dt' => 'DT_RowId', 'field' => 'inf_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowin_'.$d;
		}
	),
	array(
		'db' => 'i.inf_clave_int',
		'dt' => 'UD_Id', 'field' => 'inf_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return $d;
		}
	),
		array( 'db' => 'pr.pre_clave_int', 'dt' => 'Presupuesto', 'field' => 'pre_clave_int' ),
		array( 'db' => 'pr.pre_nombre', 'dt' => 'Nombre', 'field' => 'pre_nombre' ),
		array( 'db' => 'pr.pre_descripcion', 'dt' => 'Descripcion', 'field' => 'pre_descripcion' ),
		array( 'db' => 'pr.pre_fecha', 'dt' => 'Creacion', 'field' => 'pre_fecha' ),
		array( 'db' => "pr.tpp_clave_int", 'dt' => 'Tipop', 'field' => 'tpp_clave_int' ,'formatter' => function( $d, $row ) {
			global $conectar;		
			$cont = mysqli_query($conectar,"select tpp_nombre from tipoproyecto t  where tpp_clave_int = '".$d."'");
			$dat  = mysqli_fetch_array($cont);
			$tipo = $dat['tpp_nombre'];
			return $tipo;	
        })  ,
		array( 'db' => "pr.esp_clave_int", 'dt' => 'Estadop', 'field' => 'esp_clave_int' ,'formatter' => function( $d, $row ) {
			global $conectar;
			$cont = mysqli_query($conectar,"select esp_nombre  from estadosproyecto where esp_clave_int = '".$d."'");
			$datt = mysqli_fetch_array($cont);
			$esp = $datt['esp_nombre'];				
			return $esp;			
			
        })  ,	
		array( 'db' => 'pr.pre_administracion','dt' => 'Adm', 'field' => 'pre_administracion' ,'formatter'=> function($d, $row){
			   return number_format($d,2,'.',',');
			}),
		array( 'db' => 'pr.pre_imprevisto','dt' => 'Imp', 'field' => 'pre_imprevisto','formatter'=> function($d, $row){
			   return number_format($d,2,'.',',');
			} ),
		array( 'db' => 'pr.pre_utilidades','dt' => 'Uti', 'field' => 'pre_utilidades','formatter'=> function($d, $row){
			   return number_format($d,2,'.',',');
			} ),
		array( 'db' => 'pr.pre_iva','dt' => 'Iva', 'field' => 'pre_iva','formatter'=> function($d, $row){
			   return number_format($d,2,'.',',');
			} ),
		array('db'  => 'pr.pre_clave_int','dt' => 'Total', 'field' => 'pre_clave_int' ,'formatter' => function( $d, $row ) {
		global $conectar;
		if($row[17]==0)
		{	  
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100)*pa.pgi_cant_ini) as totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100)*pa.pgi_cant_ini) as totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100)*pa.pgi_cant_ini) as totut".
			",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from pre_gru_cap_act_sub_insumobk pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.inf_clave_int= '".$row[18]."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)*pa.pgi_cant_ini) as totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)*pa.pgi_cant_ini) as totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_cant_ini) as totut".
			",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from pre_gru_cap_act_insumobk pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.inf_clave_int = '".$row[18]."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];		
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}
			$totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);
	  }
		else
		{	  
		$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100)*pa.pgi_cant_ini) as totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100)*pa.pgi_cant_ini) as totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100)*pa.pgi_cant_ini) as totut".
			",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from pre_gru_cap_act_sub_insumobk pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.inf_clave_int = '".$row[18]."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)*pa.pgi_cant_ini) as totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)*pa.pgi_cant_ini) as totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_cant_ini) as totut".
			",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from pre_gru_cap_act_insumobk pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.inf_clave_int = '".$row[18]."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];		
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}
			$totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);	  
	  }
			$totadm = ($totalc * $row[8])/100;
			$totimp = ($totalc * $row[9])/100;
			$totuti = ($totalc* $row[10])/100;
			if($row[17]==0){$totiva = ($totuti *$row[11])/100;}else{$totiva = (($totadm+$totimp+$totuti) *$row[11])/100;}
			$total = $totalc + $totadm + $totimp + $totuti + $totiva;			
			return "$".number_format($total,2,'.',',');	  			
        } ),		
		array( 'db'  => 'pr.pre_cliente','dt' => 'Cliente', 'field' => 'pre_cliente' ),
		array( 'db' => 'pe.per_razon', 'dt' => 'Persona', 'field' => 'per_razon', 'formatter' => function( $d, $row  ) {
			if($row[13]=="")
			{
			return $d;
			}
			else
			{
			return $row[13];
			}
        }
		 ),		
		array( 'db' => 'pr.est_clave_int', 'dt' => 'Estado', 'field' => 'est_clave_int', 'formatter' => function( $d, $row ) {
			return "";
        }),
		array( 'db' => 'pr.esp_clave_int', 'dt' => 'Estadop', 'field' => 'esp_clave_int', 'formatter' => function( $d, $row ) {
			global $conectar;
			$con = mysqli_query($conectar,"select esp_nombre from estadosproyecto where esp_clave_int = '".$d."' limit 1");
			$dat = mysqli_fetch_array($con);
			return $dat['esp_nombre'];
        }),
		array( 'db' => 'pr.pre_apli_iva', 'dt' => 'Apli', 'field' => 'pre_apli_iva' ),//row[17]
		array( 'db' => 'i.inf_clave_int', 'dt' => 'Inf', 'field' => 'inf_clave_int' ),//$row[18]
		array( 'db' => 'i.inf_fecha', 'dt' => 'FechaInforme', 'field' => 'inf_fecha' )//$row[18]	
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = ' i.inf_clave_int';

$joinQuery = "FROM informesbk i join presupuestobk AS pr on pr.pre_clave_int = i.pre_clave_int LEFT OUTER JOIN persona pe on pr.per_clave_int = pe.per_clave_int";
			//t.tic_estado NOT IN (3,4) and
if(strtoupper($perfil)=="ADMINISTRADOR")
{	
	$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(0,1,'',5) and i.pre_clave_int = '".$pre."'";//or t.tic_usuario = '".$usuario."'
}
else if(strtoupper($perfil)=="COORDINADOR")
{
	$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(0,1,'',5) and (pr.pre_clave_int in(select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$idUsuario."') or pr.pre_coordinador =".$idUsuario.")  and i.pre_clave_int = '".$pre."'";//or 
}
else
{
	$extraWhere = "  (pr.pre_nombre LIKE '".$nombre."%' OR '".$nombre."' IS NULL OR '".$nombre."' = '')  and (pr.pre_fecha LIKE '".$fec."%' OR '".$fec."' IS NULL OR '".$fec."' = '') and (pr.pre_cliente LIKE REPLACE('%".$cliente."%',' ','%') OR CONCAT(pe.per_nombre,' ',pe.per_apellido) LIKE REPLACE('%".$cliente."%',' ','%')  OR '".$cliente."' IS NULL OR '".$cliente."' = '' ) and (pr.tpp_clave_int = '".$tipoproyecto."' OR '".$tipoproyecto."' IS NULL	OR '".$tipoproyecto."' = '' ) and pr.est_clave_int in(0,1,'',5) and pr.pre_clave_int in(select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$idUsuario."')  and i.pre_clave_int = '".$pre."'";//or 
}


echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
);

