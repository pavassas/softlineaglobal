<?php
include ("../../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$percla = $dato['prf_clave_int'];

$pre = $_GET['pre'];
// DB table to use
$table = 'eventoscambio';

$primaryKey = 'ca.pev_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'ca.pev_clave_int',
		'dt' => 'DT_RowId', 'field' => 'pev_clave_int',
		'formatter' => function( $d, $row ) {
			return 'rowe_'.$d;
		}
	),
	array( 'db' => 'ca.pev_clave_int', 'dt' => 'Codigo', 'field' => 'pev_clave_int' ),	
	array( 'db' => 'ca.pev_descripcion', 'dt' => 'Nombre', 'field' => 'pev_descripcion' ),
	array( 'db' => 'ca.pev_aprobo', 'dt' => 'Aprobo', 'field' => 'pev_aprobo','formatter'=>function($d,$row){
	  return  "<input style='width:100%; border:thin; background-color:transparent; text-align: left; cursor:pointer' id='aproe".$row[1]."'  type='text' value='".$d."' onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARCAPEXTRA','".$row[1]."','','','')>";
	} ),
	array( 'db' => 'ca.pev_fec_aprobo', 'dt' => 'Fecha', 'field' => 'pev_fec_aprobo','formatter'=>function($d,$row){
	  return  "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='feche".$row[1]."'  type='date' value='".$d."' min='0' onKeyPress='return NumCheck(event, this)' onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARCAPEXTRA','".$row[1]."','','','')>";
	} ),
	array( 'db' => 'ca.pev_codigo', 'dt' => 'Code', 'field' => 'pev_codigo','formatter'=>function($d,$row){
	   return "<div id='code".$row[1]."'>".number_format($d,2,'.','')."</div>";
	} ),
	array( 'db' => 'ca.pre_clave_int', 'dt' => 'TotalI', 'field' => 'pre_clave_int','formatter'=>function($d,$row){
		global $conectar;
	      $cona = mysqli_query($conectar,"select pre_apli_iva from presupuesto where pre_clave_int = '".$d."'");
		  $data  = mysqli_fetch_array($cona);
		  $apli = $data['pre_apli_iva'];
		  $cap = $row[1];
		  $pre = $d;
			if($apli==0)
			{  
			//SUBANALISIS
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			}
			else
			{						   
			//SUBANALISIS
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
							 
			}	
			return "<div class='currency' id='divcapi".$cap."'>$".number_format($apu,2,'.',',')."</div>";	
	}),
		array( 'db' => 'ca.pre_clave_int', 'dt' => 'TotalA', 'field' => 'pre_clave_int','formatter'=>function($d,$row){
			global $conectar;
	      $cona = mysqli_query($conectar,"select pre_apli_iva from presupuesto where pre_clave_int = '".$d."'");
		  $data  = mysqli_fetch_array($cona);
		  $apli = $data['pre_apli_iva'];
		  $cap = $row[1];
		  $pre = $d;
			if($apli==0)
			{  		
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
			}
			else
			{		
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);					 
			}	
			return "<div class='currency' id='divcapa".$cap."'>$".number_format($apua,2,'.',',')."</div>";	
	}),
		array( 'db' => 'ca.pre_clave_int', 'dt' => 'AlcanceA', 'field' => 'pre_clave_int','formatter'=>function($d,$row){
			global $conectar;
	      $cona = mysqli_query($conectar,"select pre_apli_iva from presupuesto where pre_clave_int = '".$d."'");
		  $data  = mysqli_fetch_array($cona);
		  $apli = $data['pre_apli_iva'];
		  $cap = $row[1];
		  $pre = $d;
			if($apli==0)
			{  
			//SUBANALISIS
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
			}
			else
			{						   
			//SUBANALISIS
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysqli_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);					 
			}	
			$alcance = $apu -$apua;
			return "<div class='currency' id='divcapal".$cap."'>$".number_format($alcance,2,'.',',')."</div>";	
	})


	
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

 require( '../../data/ssp.class.php' );
 $whereAll = "";
 $groupBy = ' ca.pev_clave_int ';
 $joinQuery = "FROM  eventoscambio AS ca";
 $extraWhere = "pre_clave_int='".$pre."'";  

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

