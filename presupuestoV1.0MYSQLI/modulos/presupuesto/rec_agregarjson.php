<?php

include ("../../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$percla = $dato['prf_clave_int'];
$insumos = 0;
	$con = mysqli_query($conectar,"select ins_clave_int from insumos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['ins_clave_int'];
		   $idsu[] = $ins;
	   }
	   $insumos = implode(',',$idsu);
	}

$pre = $_GET['pre'];
$gru = $_GET['gru'];
$cap = $_GET['cap'];
$id = $_GET['act'];
$opcr = $_GET['opcr'];

$conact = mysqli_query($conectar,"select act_clave_int from pre_gru_cap_actividad where pgca_clave_int = '".$id."'");
$datact = mysqli_fetch_array($conact);
$act = $datact['act_clave_int'];

$noinsumos = 0;
$cono = mysqli_query($conectar,"select ins_clave_int from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."'");
$numo = mysqli_num_rows($cono);
if($numo>0)
{
	$insun = array();
    for($n = 0;$n<$numo;$n++)
	{
       $dato = mysqli_fetch_array($cono);
	   $insu = $dato['ins_clave_int'];
	   $insun[] = $insu;
    }
	$noinsumos = implode(",", $insun);
}

// DB table to use
$table = 'insumos';

// Table's primary key
$primaryKey = 'i.ins_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'i.ins_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ins_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowi_'.$d;
		}
	),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Asignar' ,'field' => 'ins_clave_int','formatter'=>function($d,$row){
	     $idUsuario= $_COOKIE["usIdentificacion"];
		     if($row[11]==1)
			 {
				return "<a class='btn btn-block btn-success btn-xs' onClick=CRUDPRESUPUESTO('ASIGNARRECURSO','','','','','".$d."')  style='width:20px; height:20px'><i class='glyphicon glyphicon-plus'></i></a>";
			 }
			 else
			 {
			    return "<a class='btn btn-block btn-success btn-xs' onClick=CRUDPRESUPUESTO('ASIGNARRECURSO2','','','','','".$d."')  style='width:20px; height:20px'><i class='glyphicon glyphicon-plus'></i></a>";
			 }
	} ),	
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Codigo','field' => 'ins_clave_int' ),
	array( 'db' => 't.tpi_nombre', 'dt' => 'Tipo' ,'field' => 'tpi_nombre'),
	array( 'db' => "t.tpi_tipologia", 'dt' => 'Tipologia' ,'field' => 'tpi_tipologia','formatter'=>function($d,$row){
		if($d==1){ return 'Material'; }else if($d==2){ return 'Equipo'; }else if($d==3){ return 'Mano de Obra';	}
	}),
	array( 'db' => 'i.ins_nombre', 'dt' => 'Nombre','field' => 'ins_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Cod' ,'field' => 'uni_codigo' ),
	array( 'db' => 'u.uni_nombre', 'dt' => 'Unidad' ,'field' => 'uni_nombre'),
	array( 'db' => 'i.ins_valor', 'dt' => 'Valor' ,'field' => 'ins_valor','formatter' => function( $d, $row ) {
            return '$'.number_format($d,2,',','.');}
        ),	
	array( 'db'  => 'i.ins_descripcion','dt' => 'Descripcion' ,'field' => 'ins_descripcion'),
	array( 'db'=> 'i.ins_clave_int' ,'dt'=>"Rendimiento","field"=>'ins_clave_int','formatter'=>function($d,$row){
	   return "<input type='text' id='rend".$d."' style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' onKeyPress='return NumCheck(event, this)'>";
	}),
	array( 'db' => "'".$opcr."'", 'dt' => 'Opcr' ,'field' => 'Opcr','as'=>'Opcr'),
);


$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

$whereAll = "";
 $groupBy = ' i.ins_clave_int ';
// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$joinQuery =  " FROM insumos AS i join tipoinsumos AS t on t.tpi_clave_int = i.tpi_clave_int join unidades AS  u on u.uni_clave_int  = i.uni_clave_int ";
$extraWhere = " (i.est_clave_int in(0,1)  or i.ins_clave_int in(".$insumos.")) and i.ins_clave_int not in(".$noinsumos.")";  //u.salary >= 90000";  
echo json_encode(
//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

