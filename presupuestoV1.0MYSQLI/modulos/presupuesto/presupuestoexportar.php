<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idpresupuesto = $_GET['edi'];


include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");
//DATOS DEL PRESUPUESTO
$con  = mysqli_query($conectar,"select UPPER(per_nombre) nom,UPPER(per_apellido)  ape,UPPER(per_razon) cli, per_documento,pre_nombre,pre_fecha,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_usu_creacion,pre_cliente,tpp_clave_int,date_format(pre_fecha,'%Y%m%d') as fec,pre_apli_iva,pre_coordinador as cor  from presupuesto pr left outer join persona p on p.per_clave_int = pr.per_clave_int where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysqli_fetch_array($con);   
$fech = $dat['fec'];
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$nomo = $dat['pre_nombre'];
$fechai = $dat['pre_fecha'];
$apli = $dat['pre_apli_iva'];
$nomcliente = $dat['cli'];
$cont = mysqli_query($conectar,"select tpp_nombre from tipoproyecto where tpp_clave_int = '".$dat['tpp_clave_int']."'");
$datt =  mysqli_fetch_array($cont);
$tppr =  strtoupper($datt['tpp_nombre']);
$cor = $dat['cor'];
if($precliente!=""){$cliente=$precliente;}else { $cliente = $nomcliente." - NIT: ".$dat['per_documento']; }

$creado = $dat['pre_usu_creacion'];
$con = mysqli_query($conectar,"select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$creado."' limit 1");
$dat = mysqli_fetch_array($con);
$creadopor = $dat['usu'];
$car  = $dat['car_clave_int'];
$con = mysqli_query($conectar,"select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$cor."' limit 1");
$dat = mysqli_fetch_array($con);
$aprobadopor = $dat['usu'];
$carc  = $dat['car_clave_int'];

$conc = mysqli_query($conectar,"select UPPER(car_nombre) car from cargos where car_clave_int = '".$car."'");
$datc = mysqli_fetch_array($conc);
$cargo = $datc['car'];
$conc = mysqli_query($conectar,"select UPPER(car_nombre) car from cargos where car_clave_int = '".$carc."'");
$datc = mysqli_fetch_array($conc);
$cargoc = $datc['car'];

/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
   // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}

			
PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );			

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

function convert_htmlentities($data)
	{
		//$result = str_replace(
		//array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
		//"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data); 
		$result = str_replace("á",htmlentities("á"),$data);
		$result = str_replace("é",htmlentities("é"),$result);
		$result = str_replace("í",htmlentities("í"),$result);
		$result = str_replace("ó",htmlentities("ó"),$result);
		$result = str_replace("ú",htmlentities("ú"),$result);
		$result = str_replace("Á",htmlentities("Á"),$result);
		$result = str_replace("É",htmlentities("É"),$result);
		$result = str_replace("Í",htmlentities("Í"),$result);
		$result = str_replace("Ó",htmlentities("Ó"),$result);
		$result = str_replace("Ú",htmlentities("Ú"),$result);
		$result = str_replace("ñ",htmlentities("ñ"),$result);
		$result = str_replace("Ñ",htmlentities("Ñ"),$result);
		$result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
		return $result;
	}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		),
	),
);

// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THICK,
			'color' => array('argb' => 'FF993300'),
		),
	),
);



// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
							 ->setLastModifiedBy("Pavas.co")
							 ->setTitle("PRESUPUESTO DE OBRA Y CONTROL DE COSTOS")
							 ->setSubject("Informe Presupuesto")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Informes");
// Create a first sheet
//echo date('H:i:s') , " Agregar datos" , EOL;
// Add data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1' , "")
							  ->setCellValue('D1' , "PRESUPUESTO INICIAL DE OBRA")
							  ->setCellValue('D2' , "VERSIÓN: 05")
							  ->setCellValue('F2' , "FECHA VERSION: 30/03/2016")
							  ->setCellValue('G2' , "APROBADO POR: NATALÍ PINZÓN")
							  ->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL INGENIERIA S.A.S ")
							  ->setCellValue('A4' , "CLIENTE: ".$cliente)
							  ->setCellValue('A5' , "NOMBRE DE LA OBRA: ".$nomo)
							  ->setCellValue('A6' , "FECHA INICIAL: ".$fechai)
							  ->setCellValue('A8' , "CAP.")
							  ->setCellValue('B8' , "ITEM")
							  ->setCellValue('C8' , "DESCRIPCIÓN")
							  ->setCellValue('D8' , "UN")
							  ->setCellValue('E8' , "CANT")
							  ->setCellValue('F8' , "VR.UNIT")
							  ->setCellValue('G8' , "VR.TOTAL")
							  ->mergeCells('A1:C2')
							  ->mergeCells('D1:G1')
							  ->mergeCells('D2:E2')
							  ->mergeCells('A3:G3')
							  ->mergeCells('A4:G4')
							  ->mergeCells('A5:G5')
							  ->mergeCells('A6:G6')
							  ->mergeCells('A7:G7')
							  ;
							cellColor('A1:G1', 'FFFFFF');
							cellColor('E8:G8', 'B8CCE4');
							$objPHPExcel->getActiveSheet()->getStyle("A8:G8")->getFont()->setBold(true)->setName('Calibri');
							$objPHPExcel->getActiveSheet()->getStyle('D1:G2')->getFont()->setBold(true)->setName('Calibri');
							$objPHPExcel->getActiveSheet()->getStyle('A3:G6')->getFont()->setBold(true)->setName('Calibri');

							$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);
							$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setWrapText(true);


//CICLO DE GRUPO

$congru = mysqli_query($conectar,"select distinct g.gru_nombre as nom,g.gru_clave_int as cla from grupos g join pre_gru_cap_actividad p on p.gru_clave_int = g.gru_clave_int where p.pre_clave_int = '".$idpresupuesto."' and pgca_cantidad>0 order by g.gru_orden ASC");
$numg = mysqli_num_rows($congru);
$filc = 9;
$hasta = $filc+$numg;
$totaldirecto  = 0;
$linea1G = "=";

for($g=9; $g<$hasta;$g++)
{
	$datg = mysqli_fetch_array($congru);
	$grupo = $datg['nom'];
	$idgrupo = $datg['cla'];

    $consu = mysqli_query($conectar,"SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idgrupo."' and pgca_creacion!=1");
    $datsum = mysqli_fetch_array($consu);
    if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}
	/*
	if($apli==0)
	{	
	
		$consu = mysqli_query($conectar,"SELECT sum(((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*
		pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) tot ,
		sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_adm_ini) / 100) totad ,
		sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_imp_ini) / 100) totim ,
		sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_uti_ini) / 100) totut ,
		sum((((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."'   and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_uti_ini) / 100) * pa.pgi_iva_ini) / 100 totiv
		FROM pre_gru_cap_act_sub_insumo pa JOIN actividades a ON a.act_clave_int = pa.act_clave_int
		WHERE pa.pre_clave_int = '".$idpresupuesto."' AND pa.gru_clave_int = '".$gru."' AND a.act_clave_int IN (
		SELECT DISTINCT act_subanalisis	FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."')");// and pa.act_subanalisis = '".$idc."'//							
		$datsu = mysqli_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}
		$totalss = $totalss + ($totads+$totims+$totuts+$totivs);
		
		$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".			
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
		",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
		" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int= '".$idgrupo."'");
		
		$datsu = mysqli_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totals = $totals + $totalss + ($totads+$totims+$totuts+$totivs);
		
		
		$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".			
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
		",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
		" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int= '".$idgrupo."'");
		$datsu = mysqli_fetch_array($consu);
		$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$total = 0;}else {$total  = $datsu['tot'];}	
		$total = $total  +($totad+$totim+$totut+$totiv) + ($totals);
	}
	else
	{
		$consu = mysqli_query($conectar,"select sum(((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".			
					",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
					",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
					",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
					",sum(((((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100)+".
					"(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."'   and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100)+".
					"(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."'   and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
					" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and a.act_clave_int IN (
						SELECT DISTINCT act_subanalisis	FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$gru."'	)");
					$datsu = mysqli_fetch_array($consu);
					$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
					if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}
					$totalss = $totalss + ($totads+$totims+$totuts+$totivs);	
			
		$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".			
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
		",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
		" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int= '".$idgrupo."'");
		
		$datsu = mysqli_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totals = $totals + $totalss + ($totads+$totims+$totuts+$totivs);		
		
		$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".			
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
		",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
		" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int= '".$idgrupo."'");
		$datsu = mysqli_fetch_array($consu);
		$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$total = 0;}else {$total  = $datsu['tot'];}	
		$total = $total  +($totad+$totim+$totut+$totiv) + ($totals);
	
	}*/
	//AÑADIR FILA
	$objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $grupo)
	->setCellValue('B' . $filc, "")
	->setCellValue('C' . $filc, "")
	->setCellValue('D' . $filc, "")
	->setCellValue('E' . $filc, "")
	->setCellValue('F' . $filc, "")
	//->setCellValue('G' . $filc, number_format($total,2,'.',''))
	->mergeCells('A' . $filc.':F' . $filc);						 								
	cellColor('A' . $filc.':G'.$filc, '92D050');
	$linea1G .="+G".$filc;	
	$filaaG  = $filc;
	
	$objPHPExcel->getActiveSheet()->getStyle('A' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
	$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getFont()->setBold(true);	
	$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getFont()->setSize(10)->setName('Calibri');	
    //BORDES GRUPOS
	$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	
	$objPHPExcel->getActiveSheet()->getStyle('G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');



   $filc = $filc + 1;//INCREMENTA 1 PARA EMPEZAR LOS CAPITULOS
	$conc = mysqli_query($conectar,"select distinct c.cap_nombre cap, c.cap_clave_int as cla,c.cap_codigo as codc,d.pgc_codigo as co from presupuesto p join pre_gru_capitulo d on d.pre_clave_int = p.pre_clave_int join capitulos c on d.cap_clave_int = c.cap_clave_int  where d.pre_clave_int = '".$idpresupuesto."' and d.gru_clave_int = '".$idgrupo."' and d.cap_clave_int in(select cap_clave_int from pre_gru_cap_actividad where pre_clave_int = '".$idpresupuesto."' and gru_clave_int = '".$idgrupo."' and pgca_cantidad>0 ) ORDER BY co");
	$numc = mysqli_num_rows($conc);
   $hasta1 = $filc + $numc;
   $linea1C = "=";
   for($c=$filc;$c<$hasta1;$c++)
   {
	   
		$linea1C.= "+G".$filc;		
		$datc = mysqli_fetch_array($conc);
		$capitulo = utf8_encode(convert_htmlentities($datc['cap']));
		$idcapitulo  = $datc['cla'];
		
		/*$conco = mysqli_query($conectar,"select pgc_codigo from pre_gru_capitulo where pre_clave_int = '".$idpresupuesto."' and gru_clave_int ='".$idgrupo."' and cap_clave_int = '".$idcapitulo."'");
		$datco = mysqli_fetch_array($conco);*/
		$co = $datc['co'];
		//if($k<10){$item="".$codc.".0".$k;}else{$item="".$codc.".".$k;}
		
		$codc = number_format($co,2,'.','');

       $consu = mysqli_query($conectar,"SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$idcapitulo."' and pgca_creacion!=1");
       $datsum = mysqli_fetch_array($consu);
       if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
		
		/*if($apli==0)
		{
			$consu = mysqli_query($conectar,"SELECT sum(((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) tot ,
			sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_adm_ini) / 100) totad ,
			sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_imp_ini) / 100) totim ,
			sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_uti_ini) / 100) totut ,
			sum((((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_uti_ini) / 100) * pa.pgi_iva_ini) / 100 totiv
			FROM pre_gru_cap_act_sub_insumo pa JOIN actividades a ON a.act_clave_int = pa.act_clave_int
			WHERE pa.pre_clave_int = '".$idpresupuesto."' AND pa.gru_clave_int = '".$idgrupo."' AND pa.cap_clave_int = '".$idcapitulo."' AND a.act_clave_int IN (
			SELECT DISTINCT act_subanalisis	FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."'	AND cap_clave_int = '".$idcapitulo."')");// and pa.act_subanalisis = '".$idc."'//							
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}
			$totalss = $totalss + ($totads+$totims+$totuts+$totivs);
			
							
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
			",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + $totalss + ($totads+$totims+$totuts+$totivs);
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
			",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}		
			$totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);	
		}
		else
		{	
			$consu = mysqli_query($conectar,"select sum(((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".			
			",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
			",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
			",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100)+".
			"(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100)+".
			"(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int IN (SELECT DISTINCT act_subanalisis	FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."'	AND cap_clave_int = '".$idcapitulo."')");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}
			$totalss = $totalss + ($totads+$totims+$totuts+$totivs);
						
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
			",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + $totalss + ($totads+$totims+$totuts+$totivs);
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
			",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}		
			$totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);	
			
			
		}*/
		//AÑADIR FILA
		$objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $codc)
		->setCellValue('B' . $filc, $capitulo)
		->setCellValue('C' . $filc, "")
		->setCellValue('D' . $filc, "")
		->setCellValue('E' . $filc, "")
		->setCellValue('F' . $filc, "")
		//->setCellValue('G' . $filc, number_format($totalc,2,'.',''))
		->mergeCells('B' . $filc.':F' . $filc);						 								
		cellColor('A' . $filc.':G'.$filc, 'C4D79B');
		$filaaC = $filc;
		$objPHPExcel->getActiveSheet()->getStyle('A' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getFont()->setBold(true)->setName('Calibri');	
		$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);
		$objPHPExcel->getActiveSheet()->getStyle('A' . $filc.':G' . $filc)->getFont()->setSize(10)->setName('Calibri');
		$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setOutlineLevel(1);
		$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setVisible(false);
		//FORMATO CODIGO
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
		//FORMATO MONEDA
		$objPHPExcel->getActiveSheet()->getStyle('G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		//BORDES CAPITULOS
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
		$filc = $filc + 1;//SE INCREMENTA 1 PARA EMPEZAR LAS ACTIVIDAD
		$cona = mysqli_query($conectar,"select d.pgca_clave_int idd, d.cap_clave_int idc,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,d.pgca_cantidad cant,ciu_nombre,tpp_nombre,d.pgca_item as item from  pre_gru_cap_actividad  d join actividades a on a.act_clave_int = d.act_clave_int join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where d.pre_clave_int = '".$idpresupuesto."' and d.gru_clave_int = '".$idgrupo."' and d.cap_clave_int = '".$idcapitulo."' and d.pgca_cantidad>0 and d.pgca_creacion = 0 order by idd asc");
		$numa = mysqli_num_rows($cona);		
		$hasta2 = $filc + $numa;
		$linea1A = "=";
		$la = $filc;
		$ac = 1;
          for($a=$filc;$a<$hasta2;$a++)
		  {
			$data = mysqli_fetch_array($cona);
	        //AÑADIR FILA 			
			$linea1A.="+G".$filc;
			
			$idd = $data['idd'];
			$idca = $data['idc'];
			$actividad = utf8_encode(convert_htmlentities($data['nom']));
			$uni = $data['uni'];
			$ida = $data['ida'];
			$cant= $data['cant'];
			$cantp = $data['cantp']; if($cantp<=0){$cantp= $cant;}
			$cante = $data['cante'];
			$cantf = $cantp - $cante;
			$ciu = $data['ciu_nombre'];
			$tpp = $data['tpp_nombre'];
			if($ac<10){$item="".$co.".0".$ac;}else{$item="".$co.".".$ac;}


			//$item = $data['item'];				  
			//consulta del total de la suma de las actividades asignadas a este capitulo
			if($apli==0)
			{
		
				/*$consu = mysqli_query($conectar,"SELECT sum(((select distinct pgi_rend_sub_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' AND act_clave_int = '".$ida."' and act_subanalisis = pa.act_clave_int)*
				pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) tot ,
				sum(((((select distinct pgi_rend_sub_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' AND act_clave_int = '".$ida."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_adm_ini) / 100) totad ,
				sum(((((select distinct pgi_rend_sub_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' AND act_clave_int = '".$ida."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_imp_ini) / 100) totim ,
				sum(((((select distinct pgi_rend_sub_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' AND act_clave_int = '".$ida."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_uti_ini) / 100) totut ,
				sum((((((select distinct pgi_rend_sub_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' AND act_clave_int = '".$ida."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) * pa.pgi_uti_ini) / 100) * pa.pgi_iva_ini) / 100 totiv
				FROM pre_gru_cap_act_sub_insumo pa JOIN actividades a ON a.act_clave_int = pa.act_clave_int
				WHERE pa.pre_clave_int = '".$idpresupuesto."' AND pa.gru_clave_int = '".$idgrupo."' AND pa.cap_clave_int = '".$idcapitulo."' AND a.act_clave_int IN (
				SELECT DISTINCT act_subanalisis	FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."'	AND cap_clave_int = '".$idcapitulo."'	AND act_clave_int = '".$ida."')");// and pa.act_subanalisis = '".$idc."'//							
				$datsu = mysqli_fetch_array($consu);
				$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
				if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}*/
				$totalss = 0;// $totalss + ($totads+$totims+$totuts+$totivs);
						
				$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".			
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
				",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
				" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
				$datsu = mysqli_fetch_array($consu);
				$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
				if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
				$totals = $totals + $totalss + ($totads+$totims+$totuts+$totivs);				   
				   
				$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
				",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
				" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
				$datsu = mysqli_fetch_array($consu);
				$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
				if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}			
				$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			}
			else
			{
				/*$consu = mysqli_query($conectar,"select sum(((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".
			",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
			",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
			",sum(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."' and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100)+".
			"(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100)+".
			"(((((select distinct pgi_rend_sub_ini*pgi_cant_ini FROM pre_gru_cap_act_sub_insumo WHERE 	pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."' AND cap_clave_int = '".$idcapitulo."'  and act_subanalisis = pa.act_clave_int)*pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int IN (SELECT DISTINCT act_subanalisis	FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND gru_clave_int = '".$idgrupo."'	AND cap_clave_int = '".$idcapitulo."')");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}*/
			$totalss = 0;//$totalss + ($totads+$totims+$totuts+$totivs);
			
			
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
			$datsu = mysqli_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + $totalss + ($totads+$totims+$totuts+$totivs);				   
				   
			$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
			$datsu = mysqli_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}			
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			}
			$totalact = $apu * $cant;
			$totaldirecto = $totaldirecto + $totalact;
			
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, "")
			->setCellValue('B' . $filc, $item)
			->setCellValue('C' . $filc, $actividad)
			//->setCellValue('D' . $filc, $uni)
			//->setCellValue('E' . $filc, $cant)
			->setCellValue('F' . $filc, $apu)
			->setCellValue('G' . $filc, "=E".$filc."*F".$filc);
			cellColor('A' . $filc.':G'.$filc, 'FFFFFF');
			$objPHPExcel->getActiveSheet()->getCell('D'. $filc)->setValueExplicit($uni,PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->getCell('E' . $filc)->setValueExplicit($cant,PHPExcel_Cell_DataType::TYPE_NUMERIC);

              //FORMATO MONEDA
             // $objPHPExcel->getActiveSheet()->getStyle('G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

			//$objPHPExcel->getActiveSheet()->getCell('G' . $filc)->setValueExplicit($totalact,PHPExcel_Cell_DataType::TYPE_NUMERIC);

			$objPHPExcel->getActiveSheet()->getStyle('E'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			
			$objPHPExcel->getActiveSheet()->getStyle('A' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('D'.$filc.':E'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));						
			$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			//$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);
			$objPHPExcel->getActiveSheet()->getStyle('A' . $filc.':G' . $filc)->getFont()->setSize(10)->setName('Calibri');
			$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setOutlineLevel(2);
			$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setVisible(false);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':G'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			//formato moneda 
			$objPHPExcel->getActiveSheet()->getStyle('F'.$filc.':G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

			//AJUSTE AUTOMATICO
			$objPHPExcel->getActiveSheet()->getStyle('C'.$filc)->getAlignment()->setWrapText(true);

			
			 if($a==($hasta2-1)){}else{$filc = $filc + 1;}//INCREMENTO DE FILA EN ACTIVIDAD
			 $ac++;
		    
		  }
		$objPHPExcel->getActiveSheet()->setCellValue('G' .$filaaC, $linea1A);
		$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setCollapsed(true);
		$objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);
		
		if($c==($hasta1-1)){}else{$filc = $filc + 1;}//INCREMENTO DE FILA EN CAPITULO
       
   }
    $objPHPExcel->getActiveSheet()->setCellValue('G' .$filaaG, $linea1C);
	$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setCollapsed(true);
	$objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);
	if($g==($hasta-1)){}else{$filc = $filc + 1;};//INCREMENTO DE FILA EN GRUPO
}				  

$filc1 = $filc + 1;
$filc2 = $filc + 2;
$filc3 = $filc + 3;
$filc4 = $filc + 4;
$filc5 = $filc + 5;
$filc6 = $filc + 6;
$filc7 = $filc + 7;
$filc8 = $filc + 8;
$filc9 = $filc + 9;
$filc10 = $filc + 10;
if($apli==1)
{
	$totalaiu = (($uti*$totaldirecto)/100) + (($adm*$totaldirecto)/100) + (($imp*$totaldirecto)/100);
	$totaliva = ($iva* $totalaiu)/100;
	//$calculoiva = "=((G".$filc5."+G".$filc3."+G".$filc4.") * F".$filc6.")/100";
	$calculoiva = "=((G".$filc5."+G".$filc3."+G".$filc4.") * F".$filc6.")";

}
else
{
	$totaliva = ($iva*(($uti*$totaldirecto)/100))/100;
	//$calculoiva = "=(F".$filc6."*G".$filc5.")/100";
	$calculoiva = "=(F".$filc6."*G".$filc5.")";
}

$objPHPExcel->getActiveSheet()->setCellValue('E'.$filc3, "ADMINISTRACIÓN")
							  ->setCellValue('E'.$filc4, "IMPREVISTO")
							  ->setCellValue('E'.$filc5, "UTILIDADES")
							  ->setCellValue('E'.$filc6, "IVA")
							  ->setCellValue('F'.$filc2, "SUBTOTAL COSTO DIRECTO")
							  ->setCellValue('F'.$filc3, $adm."%")
							  ->setCellValue('F'.$filc4, $imp."%")
							  ->setCellValue('F'.$filc5, $uti."%")
							  ->setCellValue('F'.$filc6, $iva."%")
							  ->setCellValue('F'.$filc7, "TOTAL PRESUPUESTO")							
							  ->setCellValue('G'.$filc2, $linea1G)
							  ->setCellValue('G'.$filc3, "=F".$filc3."*G".$filc2)//($adm*$totaldirecto)/100
							  ->setCellValue('G'.$filc4, "=F".$filc4."*G".$filc2)//($imp*$totaldirecto)/100
							  ->setCellValue('G'.$filc5, "=F".$filc5."*G".$filc2)//($uti*$totaldirecto)/100
							  ->setCellValue('G'.$filc6, $calculoiva)//$totaliva
							  ->setCellValue('G'.$filc7, '=SUM(G'.$filc2.':G'.$filc6.')')
							  ->setCellValue('A'.$filc9, "DILIGENCIADO POR: ".$creadopor." - ".$cargo)
							  ->setCellValue('A'.$filc10, "APROBADO POR: ".$aprobadopor." - ".$cargoc)
							  ->mergeCells('A' .$filc8.':G' .$filc8)
							  ->mergeCells('A' .$filc9.':G' .$filc9)
							  ->mergeCells('A' .$filc10.':G' .$filc10)
							  ->mergeCells('A' .$filc1.':G' .$filc1)
							  ->mergeCells('A' .$filc2.':E' .$filc2)
							  ->mergeCells('A' .$filc3.':D' .$filc3)
							  ->mergeCells('A' .$filc4.':D' .$filc4)
							  ->mergeCells('A' .$filc5.':D' .$filc5)
							  ->mergeCells('A' .$filc6.':D' .$filc6)
							  ->mergeCells('A' .$filc7.':E' .$filc7);	
							  cellColor('A' .$filc2.':G'.$filc7, 'FFFFFF');
	
	//formato moneda en totales
	$objPHPExcel->getActiveSheet()->getStyle('G'.$filc2.':G'.$filc7)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
						  
		 
//$objPHPExcel->getActiveSheet()->getStyle('G' .$filc2.':G' .$filc7)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(23);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(23);
//echo date('H:i:s') , " Set column align" , EOL;
$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('D2:G2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A8:G8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('F' .$filc3.':F' .$filc6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('E' .$filc3.':E' .$filc6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('F' .$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('F' .$filc7)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('D1:G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc1)->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension($filc8)->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension($filc2)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc3)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc4)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc5)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc6)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc7)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc9)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc10)->setRowHeight(12.75);

//echo date('H:i:s') , " Set column number format" , EOL;
//$objPHPExcel->getActiveSheet()->getStyle('G9:G'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE );
//$objPHPExcel->getActiveSheet()->getStyle('F9:F'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
					 										
$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A8:G8')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('G' .$filc2.':G' .$filc7)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
 cellColor('G' .$filc2.':G' .$filc7, 'B8CCE4');
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':F' .$filc2)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':F' .$filc2)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc3.':F' .$filc3)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc3.':F' .$filc3)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc4.':F' .$filc4)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc4.':F' .$filc4)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc5.':F' .$filc5)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc5.':F' .$filc5)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc6.':F' .$filc6)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc6.':F' .$filc6)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc7.':F' .$filc7)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc7.':F' .$filc7)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc9.':G' .$filc10)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':G' .$filc10)->getFont()->setSize(10)->setName('Calibri');	
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':G' .$filc10)->getFont()->setBold(true)->setName('Calibri');
// Añadir una imagen al informe
//echo date('H:i:s') , " Añadir una imagen al informe" , EOL;
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL1.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(30);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('PRESUPUESTO'); 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
// JavaScript Document
//2DA hoja
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()
	->setCellValue('A1' , "")
	->setCellValue('D1' , "ANÁLISIS DE PRECIOS UNITARIOS")
	->setCellValue('D2' , "VERSIÓN: 05")
	->setCellValue('F2' , "FECHA VERSION: 30/03/2016")
	->setCellValue('G2' , "APROBADO POR: NATALÍ PINZÓN")
	->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL INGENIERIA S.A.S ")
	->setCellValue('A4' , "CLIENTE: ".$cliente)
	->setCellValue('A5' , "NOMBRE DE LA OBRA: ".$nomo)
	->setCellValue('A6' , "FECHA INICIAL: ".$fechai)
	->setCellValue('A8' , "ÍTEM")
	->setCellValue('B8' , "DESCRIPCIÓN")
	->setCellValue('C8' , "UND")
	->setCellValue('D8' , "REND")
	->setCellValue('E8' , "V/UNITARIO")
	->setCellValue('F8' , "MATERIAL")
	->setCellValue('G8' , "EQUIPO")
	->setCellValue('H8' , "MANO DE OBRA")
	->mergeCells('A1:C2')
	->mergeCells('D1:H1')
	->mergeCells('D2:E2')
	->mergeCells('G2:H2')
	->mergeCells('A3:H3')
	->mergeCells('A4:H4')
	->mergeCells('A5:H5')
	->mergeCells('A6:H6')
	->mergeCells('A7:H7')
	; 

	cellColor('A1:H2', 'FFFFFF');
	cellColor('A8:H8', '92D050');
	$objPHPExcel->getActiveSheet()->getStyle('A8:H8')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle("A1:H2")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A8:H8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));		
	$objPHPExcel->getActiveSheet()->getStyle("A8:H8")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('D1:H2')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A3:H6')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getStyle('D1:H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
	$objPHPExcel->getActiveSheet()->getStyle('D2:H2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
	$objPHPExcel->getActiveSheet()->getStyle('A1:H6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

	//echo date('H:i:s') , " Set column height" , EOL;
	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
	$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
	$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(6);
	$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);

	$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setWrapText(true);
							  
$conpre = mysqli_query($conectar,"select d.pgca_clave_int idd,d.gru_clave_int as gru, d.cap_clave_int as cap,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,d.pgca_cantidad cant,ciu_nombre,tpp_nombre,d.pgca_item as Item,
  dc.pgc_clave_int as dcap from  pre_gru_cap_actividad  d join actividades a on a.act_clave_int = d.act_clave_int join pre_gru_capitulo dc ON dc.pre_clave_int = d.pre_clave_int AND dc.gru_clave_int = d.gru_clave_int
AND dc.cap_clave_int = d.cap_clave_int join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where d.pre_clave_int = '".$idpresupuesto."' and d.pgca_cantidad>0 and d.pgca_creacion = 0 GROUP BY idd ORDER BY  gru,dcap,idd ASC");

$numpre = mysqli_num_rows($conpre);
$hasta = $numpre + 9;

$acum = $hasta;
$filc = 10;
$totalvalor  = 0;
$totalequipo = 0;
$totalmaterial = 0;
$totalmano = 0;
$ic=1;
$codca = 0;
for ($i = 10; $i <= $hasta; $i++) 
{
	$dat = mysqli_fetch_array($conpre);
	$noma = utf8_encode(convert_htmlentities($dat['nom']));
	$gru = $dat['gru'];
	$cap = $dat['cap'];
	$uni = $dat['uni'];
	$ida = $dat['ida'];
	$cant= $dat['cant'];
	$cantp = $dat['cantp']; //if($cantp<=0){$cantp= $cant;}
	$cante = $dat['cante']; 
	//$item = $dat['Item'];
	$conc = mysqli_query($conectar,"select pgc_codigo from pre_gru_capitulo where pre_clave_int = '".$idpresupuesto."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."'");
	$datc = mysqli_fetch_array($conc);
	$codc = $datc['pgc_codigo'];
	if($codc!=$codca){$ic=1;}
	$codca = $codc;
	// if($codca==0){$codca=$cap;}elseif($codca==$cap){}else{$ic=1;$codca=$cap;}
	if($ic<10){ $item = "".$codc.".0".$ic; }else{ $item = "".$codc.".".$ic; }
	
	$conact = mysqli_query($conectar,"select d.pgi_clave_int idd, i.ins_clave_int idi,i.ins_nombre AS nom,t.tpi_nombre tip,u.uni_codigo AS cod,u.uni_nombre nomu,i.ins_valor AS val,d.pgi_rend_ini,d.pgi_vr_ini,d.pgi_adm_ini,d.pgi_imp_ini,d.pgi_uti_ini,d.pgi_iva_ini,d.pgi_rend_act,d.pgi_vr_act,d.pgi_adm_act,d.pgi_imp_act,d.pgi_uti_act,d.pgi_iva_act,t.tpi_tipologia AS tpl from  insumos i JOIN pre_gru_cap_act_insumo d ON (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where d.pre_clave_int = '".$idpresupuesto."' AND d.gru_clave_int = '".$gru."' AND d.cap_clave_int = '".$cap."' AND d.act_clave_int = '".$ida."' and d.pgi_creacion = 0   order by idd");
	$numa = mysqli_num_rows($conact);
	if($numa>0)
	{
		$objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $item)
		  ->setCellValue('B' . $filc, $noma)
		  //->setCellValue('C' . $filc, $uni)
		  ->setCellValue('D' . $filc, "")
		  ->setCellValue('E' . $filc, "")
		  ->setCellValue('F' . $filc, "")
		  ->setCellValue('G' . $filc, "")
		  ->setCellValue('H' . $filc, "") ;						 								
		cellColor('A' . $filc.':C'.$filc, '92D050');
		$objPHPExcel->getActiveSheet()->getCell('C' . $filc)->setValueExplicit($uni,PHPExcel_Cell_DataType::TYPE_STRING);
		$sum1 = $filc+1; $sum2 = $filc + $numa;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filc.':D'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':A'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

		//BORDES DE LAS ACTIVIDADES
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':C'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc)->getAlignment()->setWrapText(true);
	  
	     $tvalor = 0;
		 $tmaterial = 0;
		 $tequipo = 0;
		 $tmano = 0;
		 $totalm = 0; $totale = 0; $totalma = 0; $apu = 0;
		 $fil = 0;
		 $filamano = "=0";
		 $filaequipo = "=0";
		 $filamaterial = "=0";
		for($k=	1;$k<=$numa;$k++)
		{
			$dati = mysqli_fetch_array($conact);
			
			$idc = $dati['idd'];
			$nom = $dati['nom'];
			//$tipo = $dati['tip'];
			$unii = $dati['nomu'];
			$val = $dati['val']; 
			//$val1 = $dati['val1'];
			//$des = $dati['des'];
			$cod = $dati['cod'];
			//$est = $dati['est'];
			$ren = $dati['ren'];
			$tpl = $dati['tpl'];
			$insu = $dati['idi'];
			
			//$con1 = mysqli_query($conectar,"select IFNULL(pgi_rend_ini,0) AS ri,IFNULL(pgi_vr_ini,0) AS vri,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini from pre_gru_cap_act_insumo where pre_clave_int = '".$idpresupuesto."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$ida."' and ins_clave_int = '".$insu."'");
			//	   $dato1 = mysqli_fetch_array($con1);
			$ren = $dati['pgi_rend_ini']; 
			$valor = $dati['pgi_vr_ini']; 	
			$admini = $dati['pgi_adm_ini'];
			$impini = $dati['pgi_imp_ini'];
			$utiini = $dati['pgi_uti_ini'];			
			$ivaini = $dati['pgi_iva_ini'];

			$tadmini = ($valor*$admini)/100;
			$timpini = ($valor*$impini)/100;
			$tutiini = ($valor*$utiini)/100;
			if($apli==0) { $tivaini = ($tutiini*$ivaini)/100; } else{ $tivaini = (($tutiini+$tadmini+$timpini)*$ivaini)/100; }
			$valor = $valor +($tadmini+$timpini+$tutiini+$tivaini);
			$totalcini = $ren*$valor;

			$fil = $filc  + $k;
			$unidad = $cod;
			$material=0; $equipo = 0; $mano = 0; 
			$manocalculo = "=0";  $equipocalculo = "=0";  $materialcalculo = "=0";
			$mate = 0; $equi = 0; $man = 0;
			$apu = $apu + $totalcini;
			if($tpl==1){$material=$totalcini; $totalm = $totalm + $material; $materialcalculo = "=D".$fil."*E".$fil; }
			else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo; $equipocalculo = "=D".$fil."*E".$fil; }
			else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano; $manocalculo = "=D".$fil."*E".$fil; }
			$total = $material + $equipo + $mano;
			$rent = $ren;
			//if(trim($cod)=="%"){$rent = $ren*100;}else{$rent = $ren;}
			// if(round($rent,2)<=0){ $rent = rtrim($rent,'0');} else { $rent = $rent; } 		   
			$filamaterial.="+F".$fil;
			$filaequipo.="+G".$fil;
			$filamano.="+H".$fil;
				   
			
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $fil, $insu)
			->setCellValue('B' . $fil, $nom)
			->setCellValue('C' . $fil, $cod)
			->setCellValue('D' . $fil, $rent)
			->setCellValue('E' . $fil, $valor)
			->setCellValue('F' . $fil, $materialcalculo)
			->setCellValue('G' . $fil, $equipocalculo)
			->setCellValue('H' . $fil, $manocalculo)
			//->setCellValue('I' . $fil, $ubicacion)
			//->setCellValue('J' . $fil, $tipo)
			;	
			if(round($rent,2)<=0){} else {
				$objPHPExcel->getActiveSheet()->getStyle('D'.$fil)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			} 
			$objPHPExcel->getActiveSheet()->getCell('C' . $fil)->setValueExplicit($cod,PHPExcel_Cell_DataType::TYPE_STRING);					 								
			cellColor('A' . $fil.':H'.$fil, 'FFFFFF');
			//cellColor('I' . $fil.':J'.$fil, '92D050');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
			$objPHPExcel->getActiveSheet()->getStyle('C'.$fil.':D'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':A'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H' . $fil)->getFont()->setSize(10);	
			//$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(12.75);	
			
			//bordes de los insumos
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$objPHPExcel->getActiveSheet()->getStyle('E'.$fil.':H'.$fil)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

			$objPHPExcel->getActiveSheet()->getStyle('B'.$fil)->getAlignment()->setWrapText(true);

		}
		//SUBANALISIS
				$consulta = mysqli_query($conectar,"SELECT i.act_clave_int idc,i.act_nombre nom,	u.uni_codigo cod,s.pgi_rend_sub_ini ri,s.pgi_rend_sub_act ra FROM	actividades AS i JOIN pre_gru_cap_act_sub_insumo s ON s.act_subanalisis = i.act_clave_int JOIN tipoproyecto AS t ON t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS u ON u.uni_clave_int = i.uni_clave_int WHERE s.pre_clave_int = '".$idpresupuesto."' AND s.gru_clave_int = '".$gru."' AND s.cap_clave_int = '".$cap."' AND s.act_clave_int = '".$ida."' GROUP BY s.act_subanalisis ORDER BY i.act_nombre ASC");
			   $nums = mysqli_num_rows($consulta);
			   if($nums>0)
			   {
				   while($dats = mysqli_fetch_array($consulta))
				   {
					   $idc = $dats['idc'];
					   $insu = "S-".$idc;
					   $nom = $dats['nom'];
					   $tipo = "";
					   $unidad = $dats['cod'];
					   $ren = $dats['ri'];
					   $renact = $dats['ra'];
					   $tpl = 1;					  
					   if($apli==0)
					   {
						   
						   $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."' and pa.act_subanalisis = '".$idc."'");// //		
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}
							$totalss = $totalss + ($totads+$totims+$totuts+$totivs); 
											
							$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
							",sum((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum(((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$idc."'");// and pa.act_subanalisis = '".$idc."'//							
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$valor = $totals +  $totalss + ($totads+$totims+$totuts+$totivs);
					   	
							
					   		/*$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."' and pa.act_subanalisis = '".$idc."'");
							
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$valor = $totals +  ($totads+$totims+$totuts+$totivs);	*/						
					   }
					   else
					   {	
					   
					       $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
							"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
							"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."' and pa.act_subanalisis = '".$idc."'");
							// 							
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totalss = 0;}else {$totalss  = $datsu['tot'];}
							$totalss = $totalss + ($totads+$totims+$totuts+$totivs);	
												  
							$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
							",sum((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum((((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
							"((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
							"((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$idc."' ");
							// and pa.act_subanalisis = '".$idc."'							
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$valor = $totals + $totalss + ($totads+$totims+$totuts+$totivs);
					        
												  
							/*$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
							"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
							"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."' and pa.act_subanalisis = '".$idc."'");
							
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$valor = $totals +  ($totads+$totims+$totuts+$totivs);*/
							
					   }
					    $fil = $fil+1;

						$valor = $valor;						
						$totalcini = $ren*$valor;						
						
						$material=0; $equipo = 0; $mano = 0;
						$manocalculo = "=0";  $equipocalculo = "=0";  $materialcalculo = "=0";
						$mate = 0; $equi = 0; $man = 0;
						$apu = $apu + $totalcini;
						if($tpl==1){$material=$totalcini; $totalm = $totalm + $material; $materialcalculo = "=D".$fil."*E".$fil; }
						else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo; $equipocalculo = "=D".$fil."*E".$fil; }
						else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano; $manocalculo = "=D".$fil."*E".$fil;  }
						$total = $material + $equipo + $mano;	
						if($unidad=="%"){$rent = $ren*100;}else{$rent = $ren;}

						$filamaterial.="+F".$fil;
						$filaequipo.="+G".$fil;
						$filamano.="+H".$fil;
						
						//if(round($rent,2)<=0){ $rent = rtrim($rent,'0');} else { $rent = number_format($rent,2,',','.'); } 		   
						
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $fil, $insu)
			->setCellValue('B' . $fil, $nom)
			->setCellValue('C' . $fil, $unidad)
			->setCellValue('D' . $fil, $rent)
			->setCellValue('E' . $fil, $valor)
			->setCellValue('F' . $fil, $materialcalculo)
			->setCellValue('G' . $fil, $equipocalculo)
			->setCellValue('H' . $fil, $manocalculo)
			//->setCellValue('I' . $fil, $ubicacion)
			//->setCellValue('J' . $fil, $tipo)
			;	
			if(round($rent,2)<=0){} else {
				$objPHPExcel->getActiveSheet()->getStyle('D'.$fil)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			} 
			$objPHPExcel->getActiveSheet()->getStyle('B'.$fil)->getAlignment()->setWrapText(true);

			$objPHPExcel->getActiveSheet()->getCell('C' . $fil)->setValueExplicit($unidad,PHPExcel_Cell_DataType::TYPE_STRING);					 								
			cellColor('A' . $fil.':H'.$fil, 'FFFFFF');
			//cellColor('I' . $fil.':J'.$fil, '92D050');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
			$objPHPExcel->getActiveSheet()->getStyle('C'.$fil.':D'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':A'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H' . $fil)->getFont()->setSize(10);	
			//$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(12.75);			
			//bordes de los insumos
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$objPHPExcel->getActiveSheet()->getStyle('E'.$fil.':H'.$fil)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

				   }
			   }
		
		
		$filt = $filc + $numa + $nums + 1; 
		$tvalor = $tmaterial + $tequipo + $tmano;
		$totalvalor = $totalm + $totale + $totalma;
		$objPHPExcel->getActiveSheet()->setCellValue('A' . $filt, "TOTAL")
			->setCellValue('B' . $filt, "")
			->setCellValue('C' . $filt, "")
			->setCellValue('D' . $filt, "")
			->setCellValue('E' . $filt, "=SUM(F".$filt.":H".$filt.")")
			->setCellValue('F' . $filt, $filamaterial)
			->setCellValue('G' . $filt, $filaequipo)
			->setCellValue('H' . $filt, $filamano)
			->mergeCells('A'.$filt.':D'.$filt);
			cellColor('A'.$filt.':H'.$filt, 'C4D79B');
			
			//FORMATO MONEDA EN TOTALES
		$objPHPExcel->getActiveSheet()->getStyle('E'.$filt.':H'.$filt)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

		$objPHPExcel->getActiveSheet()->getStyle('A' .$filt.':D' .$filt)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filt.':H' . $filt)->getFont()->setSize(10)->setBold(true);;	
		$objPHPExcel->getActiveSheet()->getRowDimension($filt)->setRowHeight(12.75);		
		//FIN CONSULTAS
		//BORDES TOTALES
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filt.':H'.$filt)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
	
	 	$filc = $filc + $numa + $nums + 2;
	}
	$ic++;							
}
$filc2 = $filc +1;
//$objPHPExcel->getActiveSheet()->setCellValue('E'.$filc2,$totalvalor)
//		->setCellValue('F'.$filc2, $totalmaterial)
//		->setCellValue('G'.$filc2, $totalequipo)
//		->setCellValue('H'.$filc2, $totalmano);	
//		cellColor('E' .$filc2.':H'.$filc2, '92D050');
		
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(63);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);

//$objPHPExcel->getActiveSheet()->getStyle('D10:D'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
//$objPHPExcel->getActiveSheet()->getStyle('E10:H'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL1.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(30);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle("APU'S");

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()
	->setCellValue('A1' , "")
	->setCellValue('D1' , "SUB-ANÁLISIS DE PRECIOS UNITARIOS")
	->setCellValue('D2' , "VERSIÓN: 05")
	->setCellValue('F2' , "FECHA VERSION: 30/03/2016")
	->setCellValue('G2' , "APROBADO POR: NATALÍ PINZÓN")
	->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL INGENIERIA S.A.S ")
	->setCellValue('A4' , "CLIENTE: ".$cliente)
	->setCellValue('A5' , "NOMBRE DE LA OBRA: ".$nomo)
	->setCellValue('A6' , "FECHA INICIAL: ".$fechai)
	->setCellValue('A8' , "ÍTEM")
	->setCellValue('B8' , "DESCRIPCIÓN")
	->setCellValue('C8' , "UND")
	->setCellValue('D8' , "REND")
	->setCellValue('E8' , "V/UNITARIO")
	->setCellValue('F8' , "MATERIAL")
	->setCellValue('G8' , "EQUIPO")
	->setCellValue('H8' , "MANO DE OBRA")
	->mergeCells('A1:C2')
	->mergeCells('D1:H1')
	->mergeCells('D2:E2')
	->mergeCells('G2:H2')
	->mergeCells('A3:H3')
	->mergeCells('A4:H4')
	->mergeCells('A5:H5')
	->mergeCells('A6:H6')
	->mergeCells('A7:H7'); 

	cellColor('A1:H2', 'FFFFFF');
	cellColor('A8:H8', '92D050');
	$objPHPExcel->getActiveSheet()->getStyle('A8:H8')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle("A1:H2")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A8:H8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));		
	$objPHPExcel->getActiveSheet()->getStyle("A8:H8")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('D1:H2')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A3:H6')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getStyle('D1:H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
	$objPHPExcel->getActiveSheet()->getStyle('D2:H2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
	$objPHPExcel->getActiveSheet()->getStyle('A1:H6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

	$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setWrapText(true);

	//echo date('H:i:s') , " Set column height" , EOL;
	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
	$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
	$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(6);
	$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
							  
$conpre = mysqli_query($conectar,"select d.gru_clave_int as gru, d.cap_clave_int as cap,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,ciu_nombre,tpp_nombre from  pre_gru_cap_act_sub_insumo  d join actividades a on a.act_clave_int = d.act_subanalisis join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where d.pre_clave_int = '".$idpresupuesto."' and d.pgi_rend_ini>0 group by gru,cap,ida order by gru,cap,ida asc");

$numpre = mysqli_num_rows($conpre);
$hasta = $numpre + 9;

$acum = $hasta;
$filc = 10;
$totalvalor  = 0;
$totalequipo = 0;
$totalmaterial = 0;
$totalmano = 0;
$ic=1;
$codca = 0;
for ($i = 10; $i <= $hasta; $i++) 
{
	$dat = mysqli_fetch_array($conpre);
	$noma = utf8_encode(convert_htmlentities($dat['nom']));
	$gru = $dat['gru'];
	$cap = $dat['cap'];
	$uni = $dat['uni'];
	$ida = $dat['ida'];
	$item = "S-".$dat['ida'];
	$conc = mysqli_query($conectar,"select pgc_codigo from pre_gru_capitulo where pre_clave_int = '".$idpresupuesto."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."'");
	$datc = mysqli_fetch_array($conc);
	$codc = $datc['pgc_codigo'];
	
	$conact = mysqli_query($conectar,"select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,IFNULL(d.ati_rendimiento,0) as ren,IFNULL(i.ins_valor,0) as val,IFNULL(d.ati_valor,0) as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$ida."'   order by idd");
	$numa = mysqli_num_rows($conact);
	if($numa>0)
	{
		$objPHPExcel->getActiveSheet()
			->setCellValue('A' . $filc, $item)
		  	->setCellValue('B' . $filc, $noma)
		  //->setCellValue('C' . $filc, $uni)
		  	->setCellValue('D' . $filc, "")
		  	->setCellValue('E' . $filc, "")
		  	->setCellValue('F' . $filc, "")
		  	->setCellValue('G' . $filc, "")
		  	->setCellValue('H' . $filc, "") ;						 								
		  	cellColor('A' . $filc.':C'.$filc, '92D050');
		$objPHPExcel->getActiveSheet()->getCell('C' . $filc)->setValueExplicit($uni,PHPExcel_Cell_DataType::TYPE_STRING);
		$sum1 = $filc+1; $sum2 = $filc + $numa;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filc.':D'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':A'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

         //BORDES DE LAS ACTIVIDADES
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':C'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc)->getAlignment()->setWrapText(true);

	    $tvalor = 0;
		$tmaterial = 0;
		$tequipo = 0;
		$tmano = 0;
		$totalm = 0; $totale = 0; $totalma = 0; $apu = 0;
		$fil = 0;
		$filamano = "=0";
		$filaequipo = "=0";
		$filamaterial = "=0";

		for($k=	1;$k<=$numa;$k++)
		{
			$dati = mysqli_fetch_array($conact);
			
			$idc = $dati['idd'];
			$nom = utf8_encode(convert_htmlentities($dati['nom']));
			$tipo = $dati['tip'];
			$unii = $dati['nomu'];
			$val = $dati['val']; 
			$val1 = $dati['val1'];
			$des = $dati['des'];
			$cod = $dati['cod'];
			$est = $dati['est'];
			$ren = $dati['ren'];
			$tpl = $dati['tpl'];
			$insu = $dati['id'];
			
			$con1 = mysqli_query($conectar,"select IFNULL(pgi_rend_ini,0) AS ri,IFNULL(pgi_vr_ini,0) AS vri,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini from pre_gru_cap_act_sub_insumo where pre_clave_int = '".$idpresupuesto."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_subanalisis = '".$ida."' and ins_clave_int = '".$insu."'");
			$dato1 = mysqli_fetch_array($con1);
			$ren = $dato1['ri']; 
			$valor = $dato1['vri']; 	
			$admini = $dato1['pgi_adm_ini'];
			$impini = $dato1['pgi_imp_ini'];
			$utiini = $dato1['pgi_uti_ini'];			
			$ivaini = $dato1['pgi_iva_ini'];


			$tadmini = ($valor*$admini)/100;
			$timpini = ($valor*$impini)/100;
			$tutiini = ($valor*$utiini)/100;
			if($apli==0) { $tivaini = ($tutiini*$ivaini)/100; } else{ $tivaini = (($tutiini+$tadmini+$timpini)*$ivaini)/100; }
			$valor = $valor +($tadmini+$timpini+$tutiini+$tivaini);

			$fil = $filc  + $k;
			$totalcini = $ren*$valor;
			$unidad = $cod;
			$material=0; $equipo = 0; $mano = 0;
			/*$mate = 0; $equi = 0; $man = 0;
			$apu = $apu + $totalcini;
			if($tpl==1){$material=$totalcini; $totalm = $totalm + $material;}else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo; }else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano;}
			$total = $material + $equipo + $mano;
			if($cod=="%"){$rent = $ren*100;}else{$rent = $ren;}
			if(round($rent,2)<=0){ $rent = rtrim($rent,'0');} else { $rent = number_format($rent,2,',','.'); }*/


			$manocalculo = "=0";  $equipocalculo = "=0";  $materialcalculo = "=0";
			$mate = 0; $equi = 0; $man = 0;
			$apu = $apu + $totalcini;
			if($tpl==1){$material=$totalcini; $totalm = $totalm + $material; $materialcalculo = "=D".$fil."*E".$fil; }
			else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo; $equipocalculo = "=D".$fil."*E".$fil; }
			else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano; $manocalculo = "=D".$fil."*E".$fil;  }
			$total = $material + $equipo + $mano;
			$rent = $ren;
			//if(trim($cod)=="%"){$rent = $ren*100;}else{$rent = $ren;}
			if(round($rent,2)<=0){ $rent = rtrim($rent,'0');} else { $rent = number_format($rent,2,',','.'); } 	

			$filamaterial.="+F".$fil;
			$filaequipo.="+G".$fil;
			$filamano.="+H".$fil;
				   
		
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $fil, $insu)
			->setCellValue('B' . $fil, $nom)
			->setCellValue('C' . $fil, $cod)
			->setCellValue('D' . $fil, $rent)
			->setCellValue('E' . $fil, $valor)
			->setCellValue('F' . $fil, $materialcalculo)
			->setCellValue('G' . $fil, $equipocalculo)
			->setCellValue('H' . $fil, $manocalculo)
			//->setCellValue('I' . $fil, $ubicacion)
			//->setCellValue('J' . $fil, $tipo)
			;	
			if(round($rent,2)<=0){} else {
				$objPHPExcel->getActiveSheet()->getStyle('D'.$fil)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			} 	

			$objPHPExcel->getActiveSheet()->getStyle('B'.$fil)->getAlignment()->setWrapText(true);

			$objPHPExcel->getActiveSheet()->getCell('C' . $fil)->setValueExplicit($cod,PHPExcel_Cell_DataType::TYPE_STRING);					 								
			cellColor('A' . $fil.':H'.$fil, 'FFFFFF');
			//cellColor('I' . $fil.':J'.$fil, '92D050');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
			$objPHPExcel->getActiveSheet()->getStyle('C'.$fil.':D'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':A'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H' . $fil)->getFont()->setSize(10);	
			//$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(12.75);	
			
			//bordes de los insumos
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$objPHPExcel->getActiveSheet()->getStyle('E'.$fil.':H'.$fil)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		}
		//SUBANALISIS
				$consulta = mysqli_query($conectar,"SELECT i.act_clave_int idc,i.act_nombre nom,	u.uni_codigo cod,s.pgi_rend_sub_ini ri,s.pgi_rend_sub_act ra FROM	actividades AS i JOIN pre_gru_cap_act_sub_insumo s ON s.act_subanalisis = i.act_clave_int JOIN tipoproyecto AS t ON t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS u ON u.uni_clave_int = i.uni_clave_int WHERE s.pre_clave_int = '".$idpresupuesto."' AND s.gru_clave_int = '".$gru."' AND s.cap_clave_int = '".$cap."' AND s.act_clave_int = '".$ida."' GROUP BY s.act_subanalisis ORDER BY i.act_nombre ASC");
			   $nums = mysqli_num_rows($consulta);
			   if($nums>0)
			   {
				   while($dats = mysqli_fetch_array($consulta))
				   {
					   $idc = $dats['idc'];
					   $insu = "S-".$idc;
					   $nom = utf8_encode(convert_htmlentities($dats['nom']));
					   $tipo = "";
					   $unidad = $dats['cod'];
					   $ren = $dats['ri'];
					   $renact = $dats['ra'];
					   $tpl = 1;					  
					   if($apli==0)
					   {	
					   		$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".			
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."' and pa.act_subanalisis = '".$idc."'");
							
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totals + ($totads+$totims+$totuts+$totivs);						
					   }
					   else
					   {						  
							$consu = mysqli_query($conectar,"select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".		
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
							",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
							"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
							"((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$id."' and pa.act_subanalisis = '".$idc."'");
							
							$datsu = mysqli_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totals + ($totads+$totims+$totuts+$totivs);							
					   }
					   
						$valor = $totalcini;						
						$totalcini = $ren*$valor;						
						$fil = $fil+1;

						$material=0; $equipo = 0; $mano = 0;
						$mate = 0; $equi = 0; $man = 0;
						$apu = $apu + $totalcini;
						$manocalculo = "=0";  $equipocalculo = "=0";  $materialcalculo = "=0";
						if($tpl==1){$material=$totalcini; $totalm = $totalm + $material;  $materialcalculo = "=D".$fil."*E".$fil;}
						else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo;  $equipocalculo = "=D".$fil."*E".$fil; }
						else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano;  $manocalculo = "=D".$fil."*E".$fil;}
						$total = $material + $equipo + $mano;
						$rent = $ren;
						//if(trim($unidad)=="%"){$rent = $ren*100;}else{$rent = $ren;}
						if(round($rent,2)<=0){ $rent = rtrim($rent,'0');} else { $rent = $rent; } 			   
						
						$filamaterial.="+F".$fil;
						$filaequipo.="+G".$fil;
						$filamano.="+H".$fil;

						$objPHPExcel->getActiveSheet()->setCellValue('A' . $fil, $insu)
						->setCellValue('B' . $fil, $nom)
						->setCellValue('C' . $fil, $unidad)
						->setCellValue('D' . $fil, $rent)
						->setCellValue('E' . $fil, $valor)
						->setCellValue('F' . $fil, $materialcalculo)
						->setCellValue('G' . $fil, $equipocalculo)
						->setCellValue('H' . $fil, $manocalculo)
						//->setCellValue('I' . $fil, $ubicacion)
						//->setCellValue('J' . $fil, $tipo)
						;
						$objPHPExcel->getActiveSheet()->getStyle('B'.$fil)->getAlignment()->setWrapText(true);

						if(round($rent,2)<=0){} else {
							$objPHPExcel->getActiveSheet()->getStyle('D'.$fil)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
						} 	
						$objPHPExcel->getActiveSheet()->getCell('C' . $fil)->setValueExplicit($unidad,PHPExcel_Cell_DataType::TYPE_STRING);					 								
						cellColor('A' . $fil.':H'.$fil, 'FFFFFF');
						//cellColor('I' . $fil.':J'.$fil, '92D050');
						$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
						$objPHPExcel->getActiveSheet()->getStyle('C'.$fil.':D'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
						$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':A'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
						$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H' . $fil)->getFont()->setSize(10);	
						$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(12.75);			
						//bordes de los insumos
						$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						
						$objPHPExcel->getActiveSheet()->getStyle('E'.$fil.':H'.$fil)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

				   }
			   }
		
		
		$filt = $filc + $numa + $nums + 1; 
		$tvalor = $tmaterial + $tequipo + $tmano;
		$totalvalor = $totalm + $totale + $totalma;
		$objPHPExcel->getActiveSheet()->setCellValue('A' . $filt, "TOTAL")
			->setCellValue('B' . $filt, "")
			->setCellValue('C' . $filt, "")
			->setCellValue('D' . $filt, "")
			->setCellValue('E' . $filt, "=SUM(F".$filt.":H".$filt.")")
			->setCellValue('F' . $filt, $filamaterial)
			->setCellValue('G' . $filt, $filaequipo)
			->setCellValue('H' . $filt, $filamano)
			->mergeCells('A'.$filt.':D'.$filt)
			;
			cellColor('A'.$filt.':H'.$filt, 'C4D79B');
			
			//FORMATO MONEDA EN TOTALES
			$objPHPExcel->getActiveSheet()->getStyle('E'.$filt.':H'.$filt)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

		$objPHPExcel->getActiveSheet()->getStyle('A' .$filt.':D' .$filt)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filt.':H' . $filt)->getFont()->setSize(10)->setBold(true);;	
		$objPHPExcel->getActiveSheet()->getRowDimension($filt)->setRowHeight(12.75);		
		//FIN CONSULTAS
		//BORDES TOTALES
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filt.':H'.$filt)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);	
	
	 	$filc = $filc + $numa + $nums + 2;
	}		
	$ic++;							
}
$filc2 = $filc +1;
//$objPHPExcel->getActiveSheet()->setCellValue('E'.$filc2,$totalvalor)
//		->setCellValue('F'.$filc2, $totalmaterial)
//		->setCellValue('G'.$filc2, $totalequipo)
//		->setCellValue('H'.$filc2, $totalmano);	
//		cellColor('E' .$filc2.':H'.$filc2, '92D050');
		
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(63);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);

//$objPHPExcel->getActiveSheet()->getStyle('D10:D'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
//$objPHPExcel->getActiveSheet()->getStyle('E10:H'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL1.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(30);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle("SUBANALISIS");
//3RA HOJA
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(3);
$objPHPExcel->getActiveSheet()
	->setCellValue('A1' , "")
	->setCellValue('D1' , "LISTADO DE RECURSOS")
	->setCellValue('D2' , "VERSIÓN: 05")
	->setCellValue('F2' , "FECHA VERSION: 30/03/2016")
	->setCellValue('G2' , "APROBADO POR: NATALÍ PINZÓN")
	->setCellValue('B3' , "INTERVENTOR: LINEA GLOBAL INGENIERIA S.A.S ")
	->setCellValue('B4' , "CLIENTE: ".$cliente)
	->setCellValue('B5' , "NOMBRE DE LA OBRA: ".$nomo)
	->setCellValue('B6' , "FECHA INICIAL: ".$fechai)
	->setCellValue('A8' , "")
	->setCellValue('B8' , "CÓDIGO")
	->setCellValue('C8' , "DESCRIPCIÓN")
	->setCellValue('D8' , "UND")
	->setCellValue('E8' , "CANTIDAD")
	->setCellValue('F8' , "V/UNITARIO")
	->setCellValue('G8' , "V/TOTAL")
	->mergeCells('B1:C2')
	->mergeCells('D1:G1')
	->mergeCells('D2:E2')
	->mergeCells('B3:G3')
	->mergeCells('B4:G4')
	->mergeCells('B5:G5')
	->mergeCells('B6:G6')
	->mergeCells('B7:G7');
	cellColor('B1:G1', 'FFFFFF');
	cellColor('B8:G8', '92D050');
	$objPHPExcel->getActiveSheet()->getStyle('B1:G1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle("B1:G2")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('B8:G8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));		
	$objPHPExcel->getActiveSheet()->getStyle("B8:G8")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('D1:G2')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('B3:G6')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getStyle('D1:G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
	$objPHPExcel->getActiveSheet()->getStyle('D2:G2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
	$objPHPExcel->getActiveSheet()->getStyle('B1:G6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

	//echo date('H:i:s') , " Set column height" , EOL;
	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
	$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
	$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);

	$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setWrapText(true);
	
	$conpre = mysqli_query($conectar,"select i.ins_clave_int Id,i.ins_nombre Ins,u.uni_codigo Cod,d.pgi_vr_ini FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_insumo d on d.ins_clave_int = i.ins_clave_int  where d.pre_clave_int = '".$idpresupuesto."' group by i.ins_clave_int,d.pgi_vr_ini 
	UNION
	select i.ins_clave_int Id,i.ins_nombre Ins,u.uni_codigo Cod,d.pgi_vr_ini FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_sub_insumo d on d.ins_clave_int = i.ins_clave_int  where d.pre_clave_int = '".$idpresupuesto."' group by i.ins_clave_int,d.pgi_vr_ini order by Ins ASC");
/*
if($apli==0)
{
	    $conpre = mysqli_query($conectar,"SELECT IFNULL(IFNULL(Id, Id1),Id2) Id,IFNULL(IFNULL(Ins, Ins1),Ins2) Ins,IFNULL(IFNULL(Cod, Cod1),Cod2) Cod,IFNULL(IFNULL(Val1, Val),Val2) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0)+IFNULL(Cant2,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0)+IFNULL(Cant2,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0)+IFNULL(tot3,0) as Sumat,IFNULL(totad1,0)+IFNULL(totad2,0)+IFNULL(totad3,0) as Sumad,IFNULL(totim1,0)+IFNULL(totim2,0)+IFNULL(totim3,0) as Sumai,IFNULL(totut1,0)+IFNULL(totut2,0)+IFNULL(totut3,0) as Sumau,IFNULL(totiv1,0)+IFNULL(totiv2,0)+IFNULL(totiv3,0) as Sumaiv
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini*d.pgi_cant_ini) AS Cant,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad1,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim1,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut1,
								((((d.pgi_vr_ini*d.pgi_uti_ini)/100)*d.pgi_iva_ini)/100) totiv1 
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id,Ins,Val,Cod
							) T1
							LEFT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini*pgi_rend_sub_ini) as tot2,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad2,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim2,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut2,
								((((d.pgi_vr_ini*d.pgi_uti_ini)/100)*d.pgi_iva_ini)/100) totiv2 
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1
							LEFT OUTER JOIN
							(
								SELECT i.ins_clave_int AS Id2,i.ins_nombre AS Ins2,pa.pgi_vr_ini AS Val2,u.uni_codigo AS Cod2,
								sum(((SELECT DISTINCT pgi_rend_sub_ini * pgi_cant_ini FROM pre_gru_cap_act_sub_insumo							
								WHERE pre_clave_int = '".$idpresupuesto."'	AND act_subanalisis = pa.act_clave_int) * 
								pa.pgi_rend_sub_ini *pa.pgi_rend_ini)) Cant2,
								sum(((SELECT DISTINCT pgi_rend_sub_ini * pgi_cant_ini FROM 
								pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND act_subanalisis = 
								pa.act_clave_int) *pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) tot3,
								((pa.pgi_vr_ini * pa.pgi_adm_ini) / 100) totad3,
								((pa.pgi_vr_ini * pa.pgi_imp_ini) / 100) totim3,
								(( pa.pgi_vr_ini * pa.pgi_uti_ini) / 100) totut3,
								((((pa.pgi_vr_ini * pa.pgi_uti_ini) / 100) * pa.pgi_iva_ini) / 100) totiv3
								FROM
								pre_gru_cap_act_sub_insumo pa JOIN actividades a ON a.act_clave_int = pa.act_clave_int
								JOIN insumos i ON i.ins_clave_int = pa.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE pa.pre_clave_int = '".$idpresupuesto."' AND a.act_clave_int IN (SELECT DISTINCT act_subanalisis
								FROM pre_gru_cap_act_sub_insumo	WHERE pre_clave_int = '".$idpresupuesto."')
								GROUP BY Id2,Ins2,Val2,Cod2
						    )T3
							ON T1.Id = T3.Id2 OR T2.Id1 = T3.Id2
							
							UNION
							SELECT IFNULL(IFNULL(Id, Id1),Id2) Id,IFNULL(IFNULL(Ins, Ins1),Ins2) Ins,IFNULL(IFNULL(Cod, Cod1),Cod2) Cod,IFNULL(IFNULL(Val1, Val),Val2) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0)+IFNULL(Cant2,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0)+IFNULL(Cant2,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0)+IFNULL(tot3,0) as Sumat,IFNULL(totad1,0)+IFNULL(totad2,0)+IFNULL(totad3,0) as Sumad,IFNULL(totim1,0)+IFNULL(totim2,0)+IFNULL(totim3,0) as Sumai,IFNULL(totut1,0)+IFNULL(totut2,0)+IFNULL(totut3,0) as Sumau,IFNULL(totiv1,0)+IFNULL(totiv2,0)+IFNULL(totiv3,0) as Sumaiv
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini*d.pgi_cant_ini) AS Cant,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad1,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim1,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut1,
								((((d.pgi_vr_ini*d.pgi_uti_ini)/100)*d.pgi_iva_ini)/100) totiv1 
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id,Ins,Val,Cod
							) T1
							RIGHT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini*pgi_rend_sub_ini) as tot2,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad2,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim2,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut2,
								((((d.pgi_vr_ini*d.pgi_uti_ini)/100)*d.pgi_iva_ini)/100) totiv2 
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1
							RIGHT OUTER JOIN
							(
								SELECT i.ins_clave_int AS Id2,i.ins_nombre AS Ins2,pa.pgi_vr_ini AS Val2,u.uni_codigo AS Cod2,
								sum(((SELECT DISTINCT pgi_rend_sub_ini * pgi_cant_ini FROM pre_gru_cap_act_sub_insumo							
								WHERE pre_clave_int = '".$idpresupuesto."'	AND act_subanalisis = pa.act_clave_int) * 
								pa.pgi_rend_sub_ini *pa.pgi_rend_ini)) Cant2,
								sum(((SELECT DISTINCT pgi_rend_sub_ini * pgi_cant_ini FROM 
								pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' AND act_subanalisis = 
								pa.act_clave_int) *pa.pgi_rend_sub_ini * pa.pgi_rend_ini * pa.pgi_vr_ini)) tot3,
								((pa.pgi_vr_ini * pa.pgi_adm_ini) / 100) totad3,
								((pa.pgi_vr_ini * pa.pgi_imp_ini) / 100) totim3,
								(( pa.pgi_vr_ini * pa.pgi_uti_ini) / 100) totut3,
								((((pa.pgi_vr_ini * pa.pgi_uti_ini) / 100) * pa.pgi_iva_ini) / 100) totiv3
								FROM
								pre_gru_cap_act_sub_insumo pa JOIN actividades a ON a.act_clave_int = pa.act_clave_int
								JOIN insumos i ON i.ins_clave_int = pa.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE pa.pre_clave_int = '".$idpresupuesto."' AND a.act_clave_int IN (SELECT DISTINCT act_subanalisis
								FROM pre_gru_cap_act_sub_insumo	WHERE pre_clave_int = '".$idpresupuesto."')
								GROUP BY Id2,Ins2,Val2,Cod2
						    )T3
							ON T1.Id = T3.Id2 OR T2.Id1 = T3.Id2													
							ORDER BY Ins");
		}
else
{
			
	    $conpre = mysqli_query($conectar,"SELECT IFNULL(Id,Id1) Id,IFNULL(Ins,Ins1) Ins,IFNULL(Cod,Cod1) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Sumat,IFNULL(totad1,0)+IFNULL(totad2,0) as Sumad,IFNULL(totim1,0)+IFNULL(totim2,0) as Sumai,IFNULL(totut1,0)+IFNULL(totut2,0) as Sumau,IFNULL(totiv1,0)+IFNULL(totiv2,0) as Sumaiv
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad1,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim1,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut1,
								(((((d.pgi_vr_ini*d.pgi_adm_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_imp_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_uti_ini)/100))*d.pgi_iva_ini)/100) totiv1 
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id,Ins,Val,Cod
							) T1
							LEFT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini*pgi_rend_sub_ini) as tot2,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad2,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim2,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut2,
								(((((d.pgi_vr_ini*d.pgi_adm_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_imp_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_uti_ini)/100))*d.pgi_iva_ini)/100) totiv2 
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1
							UNION
							SELECT IFNULL(Id1,Id) Id,IFNULL(Ins1,Ins) Ins,IFNULL(Cod1,Cod) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Sumat,IFNULL(totad1,0)+IFNULL(totad2,0) as Sumad,IFNULL(totim1,0)+IFNULL(totim2,0) as Sumai,IFNULL(totut1,0)+IFNULL(totut2,0) as Sumau,IFNULL(totiv1,0)+IFNULL(totiv2,0) as Sumaiv							
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini * pgi_cant_ini) AS Cant,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad1,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim1,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut1,
								(((((d.pgi_vr_ini*d.pgi_adm_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_imp_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_uti_ini)/100))*d.pgi_iva_ini)/100) totiv1 
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id,Ins,Val,Cod
							) T1
							RIGHT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1,
								sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini *pgi_rend_sub_ini) as tot2,
								((d.pgi_vr_ini*d.pgi_adm_ini)/100) totad2,
								((d.pgi_vr_ini*d.pgi_imp_ini)/100) totim2,
								((d.pgi_vr_ini*d.pgi_uti_ini)/100) totut2,
								(((((d.pgi_vr_ini*d.pgi_adm_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_imp_ini)/100)+".
								"((d.pgi_vr_ini*d.pgi_uti_ini)/100))*d.pgi_iva_ini)/100) totiv2 
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$idpresupuesto."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1													
							ORDER BY Ins");
			
		}
*/
							
$numpre = mysqli_num_rows($conpre);
$hasta = $numpre + 8;

$acum = $hasta;
$filc = 9;
$subtotal  = 0;
$filatotal = "=SUM(G";
for ($i = 9; $i <= $hasta; $i++) 
{
	$dat = mysqli_fetch_array($conpre);
	
	$cod = $dat['Id'];
	$ins = utf8_encode(convert_htmlentities($dat['Ins']));
	$uni = $dat['Cod'];
	$vri = $dat['pgi_vr_ini'];

    $con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant								 
    FROM insumos i
    JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
    WHERE d.pre_clave_int = '".$idpresupuesto."' and d.ins_clave_int = '".$cod."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0 and d.pgi_vr_ini = '".$vri."'");
    $dati = mysqli_fetch_array($con); $can = $dati['Cant']; if($can=="" || $can==NULL){$can = 0;}

    $con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1	
    FROM insumos i
    JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
    WHERE d.pre_clave_int = '".$idpresupuesto."' and d.ins_clave_int = '".$cod."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0 and d.pgi_vr_ini = '".$vri."' ");
    $dati = mysqli_fetch_array($con2);  $can1 = $dati['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}
    $cantidad = $can + $can1;

    $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr ,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."' and pgi_vr_ini = '".$vri."'");
    $datad = mysqli_fetch_array($conad);
    $valor = $vri;
    $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
    if($apli==0)
    {
        $totadm = ($valor*$adm)/100;
        $totimp = ($valor*$imp)/100;
        $totuti = ($valor*$uti)/100;
        $totiva = ($totuti*$iva)/100;
    }
    else
    {
        $totadm = ($valor*$adm)/100;
        $totimp = ($valor*$imp)/100;
        $totuti = ($valor*$uti)/100;
        $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
    }

    if($datad['vr']=="" || $datad['vr']==NULL)
    {
        //seeleccionar el aiu en subanalisis
        $conad2 = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr ,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."' and pgi_vr_ini = '".$vri."'");
        $datad2 = mysqli_fetch_array($conad2);
        $adm = $datad2['adm']; 
        $imp = $datad2['imp']; 
        $uti = $datad2['uti']; 
        $iva = $datad2['iva'];
        if($apli==0)
        {
            $totadm = ($valor*$adm)/100;
            $totimp = ($valor*$imp)/100;
            $totuti = ($valor*$uti)/100;
            $totiva = ($totuti*$iva)/100;
        }
        else
        {
            $totadm = ($valor*$adm)/100;
            $totimp = ($valor*$imp)/100;
            $totuti = ($valor*$uti)/100;
            $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
        }
    }
    $valor = $valor + $totadm + $totimp + $totuti + $totiva;
    $totale = $valor * $cantidad;
    //cantidad comprometidad
    $conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'");
    $datco = mysqli_fetch_array($conco);
    $cantco = $datco['canc'];
    if($cantco=="" || $cantco==NULL){$cantco=0;}
    //TOTAL COMPROMETIDO
    $totalc = $valor * $cantco;

    //CANTIDAD ADJUDICADA
    $conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit from adjudicado where pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'");
    $datad = mysqli_fetch_array($conad);
    $canta = $datad['adj_cantidad'];
    if($canta=="" || $canta==NULL){$canta=0;} if($canta==0){$canta=$cantco;}
    $vra = $datad['adj_vr_unit'];
    if($vra=="" || $vra==NULL){$vra=0;}

    //valor adju
    /* $conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit from adjudicado where pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'");
     $datad = mysqli_fetch_array($conad);*/

    //totaladjudicado
    $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_act) vr,AVG(pgi_adm_act) adm,AVG(pgi_imp_act) imp,AVG(pgi_uti_act) uti,AVG(pgi_iva_act) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'  and pgi_vr_ini = '".$vri."' LIMIT 1");
    $datad = mysqli_fetch_array($conad);
    $valora = $datad['vr'];
    $adma = $datad['adm']; $impa = $datad['imp']; $utia = $datad['uti']; $ivaa = $datad['iva'];

    $con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_act * d.pgi_cant_act) AS Cant								 
			FROM insumos i JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$idpresupuesto."' and d.ins_clave_int = '".$cod."' and d. and pgi_vr_ini = '".$vri."'");
    $dat = mysqli_fetch_array($con); $cana = $dat['Cant']; if($cana=="" || $cana==NULL){$cana = 0;}

    $con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_act * pgi_cant_act*pgi_rend_sub_act) AS Cant1 FROM insumos i  JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int WHERE d.pre_clave_int = '".$idpresupuesto."' and d.ins_clave_int = '".$cod."'  and d.pgi_vr_ini = '".$vri."'");
    $dat = mysqli_fetch_array($con2);  $cana1 = $dat['Cant1']; if($cana1=="" || $cana1==NULL){$cana1 = 0;}

    $cantidada = $cana + $cana1;

    if($apli==0)
    {
        $totadma = ($valora*$adma)/100;
        $totimpa = ($valora*$impa)/100;
        $totutia = ($valora*$utia)/100;
        $totivaa = ($totutia*$ivaa)/100;
    }
    else
    {
        $totadma = ($valora*$adma)/100;
        $totimpa = ($valora*$impa)/100;
        $totutia = ($valora*$utia)/100;
        $totivaa = (($totadma + $totimpa + $totutia)*$ivaa)/100;
    }
   	if($datad['vr']=="" || $datad['vr']==NULL)
   	{
		$conad2 = mysqli_query($conectar,"SELECT AVG(pgi_vr_act) vr,AVG(pgi_adm_act) adm,AVG(pgi_imp_act) imp,AVG(pgi_uti_act) uti,AVG(pgi_iva_act) iva FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'  and pgi_vr_ini = '".$vri."' LIMIT 1");
		$datad2 = mysqli_fetch_array($conad2);
		$adma = $datad2['adm']; $impa = $datad2['imp']; $utia = $datad2['uti']; $ivaa = $datad2['iva'];
		$valora = $datad2['vr'];
		if($apli==0)
	    {
	        $totadma = ($valora*$adma)/100;
	        $totimpa = ($valora*$impa)/100;
	        $totutia = ($valora*$utia)/100;
	        $totivaa = ($totutia*$ivaa)/100;
	    }
	    else
	    {
	        $totadma = ($valora*$adma)/100;
	        $totimpa = ($valora*$impa)/100;
	        $totutia = ($valora*$utia)/100;
	        $totivaa = (($totadma + $totimpa + $totutia)*$ivaa)/100;
	    }

   	}
    $valora = $valora + $totadma + $totimpa + $totutia + $totivaa;
    $totaladj = $valora * $cantidada;

    if((int)$totaladj>0)
    {
        $dispo = (int)$totale-(int)$totaladj;
        $form = "OPCION 1 : ".$totale." - ".$totaladj."=".$dispo;
    }
    else
    {
        $dispo = (int)$totale-(int)$totalc;
        $form = "OPCION 2: ".$totale." - ".$totalc."=".$dispo;
    }
    /*$val = $dat['Val'];
	$cant = $dat['Suma'];
	$tot = $dat['Sumat'];
	$totad = $dat['Sumad']; $totim = $dat['Sumai']; $totut = $dat['Sumau']; $totiv = $dat['Sumaiv'];
	
	$val = $val + $totad + $totim + $totut + $totiv;
	$total = $cant * $val;// $tot + $totad + $totim + $totut + $totiv;
	
	*/
	
	$subtotal = $subtotal + ($totale);
	/*$to = $val;

	$objPHPExcel->getActiveSheet()->setCellValue('B' . $filc, $cod)
	->setCellValue('C' . $filc, $ins)
	//->setCellValue('D' . $filc, $uni)
	//->setCellValue('E' . $filc, $cant)
	->setCellValue('F' . $filc, $to)
	->setCellValue('G' . $filc, $total);
	cellColor('A' . $filc.':G'.$filc, 'FFFFFF');*/
	//$filatotal.="+G".$filc;
	if($i==9)
	{
		$filatotal.=$filc.":G";
	}
	if($i==$hasta)
	{
		$filatotal.=$filc.")";
	}

    $objPHPExcel->getActiveSheet()->setCellValue('B' . $filc, $cod)
        ->setCellValue('C' . $filc, $ins)
        //->setCellValue('D' . $filc, $uni)
        //->setCellValue('E' . $filc, $cant)
        ->setCellValue('F' . $filc, $valor)
        ->setCellValue('G' . $filc,"=E".$filc."*F".$filc);//$totale
    cellColor('A' . $filc.':G'.$filc, 'FFFFFF');
	//FORMATO MONEDA
	$objPHPExcel->getActiveSheet()->getStyle('F'.$filc.':G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

	
	$objPHPExcel->getActiveSheet()->getCell('D' . $filc)->setValueExplicit($uni,PHPExcel_Cell_DataType::TYPE_STRING);
	$objPHPExcel->getActiveSheet()->getCell('E' . $filc)->setValueExplicit($cantidad,PHPExcel_Cell_DataType::TYPE_NUMERIC);
	//$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':E'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
	$objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':G'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle('B'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));	
	$objPHPExcel->getActiveSheet()->getStyle('D'.$filc.':E'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));		
	
	//$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);

	$objPHPExcel->getActiveSheet()->getStyle('C'.$filc)->getAlignment()->setWrapText(true);

	
	//FIN CONSULTAS

	 $filc = $filc + 1; 
}

 $filc1 = $filc;
// $filc2 = $filc + 2;
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2.43);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(65);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$filc1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
//$objPHPExcel->getActiveSheet()->getStyle('F2:G'.$filc1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
 $objPHPExcel->getActiveSheet()->getStyle('B8:G'.$filc1)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

 $objPHPExcel->getActiveSheet()->setCellValue('B'.$filc1 , "TOTAL")
                               ->setCellValue('G'.$filc1 , $filatotal)
                               ->mergeCells('A1:A'.$filc1)->mergeCells('B'.$filc1.':F'.$filc1);
 $objPHPExcel->getActiveSheet()->getStyle('B'.$filc1.':G'.$filc1)->getFont()->setBold(true);
 $objPHPExcel->getActiveSheet()->getStyle('B' .$filc1)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
 cellColor('B'.$filc1.':G'.$filc1, 'C4D79B');
 //FORMTATO MONEDA TOTALES
 $objPHPExcel->getActiveSheet()->getStyle('G'.$filc1)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

$objPHPExcel->getActiveSheet()->getStyle('B8:G' . $filc1)->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getRowDimension($filc1)->setRowHeight(12.75);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL1.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('B1');
$objDrawing->setOffsetX(12);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('RECURSOS');

$callStartTime = microtime(true);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
$archivo = date('Ymd').' PPTO DE OBRA '.$tppr.' '.$nomo.'.xlsx';

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');