<?php
include("../../data/Conexion.php");
$table = 'insumos';
// Table's primary key
$primaryKey = 'i.ins_clave_int';//'act_clave_int'
$pre = $_GET['pre'];
$columns = array(
	array(
		'db' => 'i.ins_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ins_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			$usuario= $_COOKIE['usuario'];
			$idUsuario = $_COOKIE["usIdentificacion"];			
			$fecha=date("Y/m/d H:i:s");
			global $conectar;
			$conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$row[8]."' and ins_clave_int = '".$d."'");
			$datco = mysqli_fetch_array($conco);
			$cantco = $datco['canc'];
			if($cantco=="" || $cantco==NULL){$cantco=0;}			
			$veri = mysqli_query($conectar,"select * from adjudicado where pre_clave_int = '".$row[8]."' and ins_clave_int = '".$d."'");
			$numv = mysqli_num_rows($veri);
			if($numv>0)
			{			
				//$sql = mysqli_query($conectar,"update adjudicado set adj_cantidad = '".$cantco."' where pre_clave_int = '".$pre."' and ins_clave_int = '".$cod."'");			
			}
			else
			{ 			
				$sql = mysqli_query($conectar,"insert into adjudicado (pre_clave_int,ins_clave_int,adj_cantidad,adj_usu_actualiz,adj_fec_actualiz) VALUES('".$row[8]."','".$d."','".$cantco."','".$usuario."','".$fecha."')");			
			}
			
			return 'row_rec'.$d;
		}// r0
	),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Codigo', 'field' => 'ins_clave_int' ), //r1
	array( 'db' => 'i.ins_nombre', 'dt' => 'Nombre', 'field' => 'ins_nombre' ),  //r2
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),  //r3
	array( 'db' => 'd.pre_clave_int', 'dt' => 'CantP','field' =>'pre_clave_int','formatter'=>function($d,$row){
		global $conectar;
			$con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant								 
			FROM insumos i
			JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysqli_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}
			
			$con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1								 
			FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysqli_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}
			$totc = $can + $can1;
			return  "<span title='".$totc."'>".number_format($totc,2,'.','')."</span>";
	}), //r5
	array( 'db' => 'd.pre_clave_int', 'dt' => 'TotalP','field' =>'pre_clave_int','formatter'=>function($d,$row){
		global $conectar;
		     $cona = mysqli_query($conectar,"select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
	 		$data = mysqli_fetch_array($cona);
	         $apli = $data['apli'];
			 
			 $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			 $datad = mysqli_fetch_array($conad);
			 $valor = $datad['vr'];
			 $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
			 
			$con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant							 
			FROM insumos i JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysqli_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}
			
			$con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1 FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysqli_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}
			
			$cantidad = $can + $can1;
			if($apli==0)
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = ($totuti*$iva)/100;
			}
			else
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = (($totadm + $totimp + $totuti)*$iva)/100;
			}
			//SI ES SUBANALISIS
			if($datad['vr']=="" || $datad['vr']==NULL)
			{
				$conad2 = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
				$datad2 = mysqli_fetch_array($conad2);
				$valor = $datad2['vr'];
				$adm = $datad2['adm']; $imp = $datad2['imp']; $uti = $datad2['uti']; $iva = $datad2['iva'];
			}
			$valor = $valor + $totadm + $totimp + $totuti + $totiva;
			$totale = $valor * $cantidad;
			
			return "<span id='total".$row[0]."' class='currency' title='".$totale."'>$".number_format($totale,2,'.',',')."</span>";
			
	}), //r6
	array( 'db' => 'd.pre_clave_int','dt'=>'Valor','field'=>'pre_clave_int','formatter'=>function($d,$row){
		global $conectar;
			$cona = mysqli_query($conectar,"select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
	 		$data = mysqli_fetch_array($cona);
	        $apli = $data['apli'];
			 
			 $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."' LIMIT 1");
			 $datad = mysqli_fetch_array($conad);
			 $valor = $datad['vr'];
			 $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
			 if($apli==0)
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = ($totuti*$iva)/100;
			}
			else
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = (($totadm + $totimp + $totuti)*$iva)/100;
			}
			$valor = $valor + $totadm + $totimp + $totuti + $totiva;
		
	   return "<span class='currency'>$".number_format($valor,2,'.',',') ."</span>";
	}), //r7
	array('db'=> 'd.pre_clave_int', 'dt'=> 'CantC', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
		global $conectar;
			$conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datco = mysqli_fetch_array($conco);
			$cantco = $datco['canc'];
			if($cantco=="" || $cantco==NULL){$cantco=0;}
			//$totalc = $row[7] * $cantco;
			return "<span title='".$cantco."'>".number_format($cantco,2,'.',',')."</span>";
	} ),//r8
	array('db'=> 'd.pre_clave_int', 'dt'=> 'TotalC', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
		global $conectar;
		   	$cona = mysqli_query($conectar,"select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
	 		$data = mysqli_fetch_array($cona);
	        $apli = $data['apli'];
			 
			 $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."' LIMIT 1");
			 $datad = mysqli_fetch_array($conad);
			 $valor = $datad['vr'];
			 $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
			 if($apli==0)
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = ($totuti*$iva)/100;
			}
			else
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = (($totadm + $totimp + $totuti)*$iva)/100;
			}
			$valor = $valor + $totadm + $totimp + $totuti + $totiva;
			
	   $conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datco = mysqli_fetch_array($conco);
			$cantco = $datco['canc'];
			if($cantco=="" || $cantco==NULL){$cantco=0;}
			$totalc = $valor * $cantco;
			return "<span class='currency'>$".number_format($totalc,2,'.',',')."</span>";
	} ),//r9
	array('db'=> 'd.pre_clave_int', 'dt'=> 'CantA', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
			global $conectar;
		    $conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datco = mysqli_fetch_array($conco);
			$cantco = $datco['canc'];
			if($cantco=="" || $cantco==NULL){$cantco=0;}
			
	  		$conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$canta = $datad['adj_cantidad'];		
			if($canta=="" || $canta==NULL){$canta=0;} if($canta==0){$canta=$cantco;}
			
			return "<input type='text' name='canta".$row[0]."' id='canta".$row[0]."' onKeyPress='return NumCheck(event, this)' value='". $canta."' title='".$canta."' class='form-control input-sm' onChange=CRUDPARTIDAS('ACTUALIZARADJUDICADO','','','','','".$row[0]."','') style='width:100%; border:thin; text-align:right'/>";
	} ),//r10
	array('db'=> 'd.pre_clave_int', 'dt'=> 'ValorA', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
		global $conectar;
		$conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$vra = $datad['adj_vr_unit'];			
			if($vra=="" || $vra==NULL){$vra=0;}		
				return "<input type='text' name='vra".$row[0]."' id='vra".$row[0]."' onKeyPress='return NumCheck(event, this)' class='form-control input-sm' value='".$vra."' onChange=CRUDPARTIDAS('ACTUALIZARADJUDICADO','','','','','".$row[0]."','') style='width:100%; border:thin; text-align:right' class='currency'/>";//		
			
				
	}),
	array('db'=> 'd.pre_clave_int', 'dt'=> 'AdmA', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
		global $conectar;
		$conad = mysqli_query($conectar,"select adj_adm,adj_vr_unit from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$adma = $datad['adj_adm'];
			$vra = $datad['adj_vr_unit'];			
			if($vra=="" || $vra==NULL){$vra=0;}		
			$totadm = ($vra*$adma)/100;
				return "<input type='text' name='adm". $d."_".$row[0]."' id='adm". $d."_".$row[0]."' onKeyPress='return NumCheck(event, this)' class='form-control input-sm' value='".$adma."' onChange=guardaraiua('".$d."','".$row[0]."') style='width:100%; border:thin; text-align:right' class='currency'/>
				<br><span class='currency'id='ad". $row[0]."'>".number_format($totadm,2,'.',',')."</span>";//		
				
	}),
	array('db'=> 'd.pre_clave_int', 'dt'=> 'ImpA', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
		global $conectar;
			$conad = mysqli_query($conectar,"select adj_imp,adj_vr_unit from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$impa = $datad['adj_imp'];
			$vra = $datad['adj_vr_unit'];			
			if($vra=="" || $vra==NULL){$vra=0;}		
			$totimp = ($vra*$impa)/100;
			return "<input type='text' name='imp". $d."_".$row[0]."' id='imp". $d."_".$row[0]."' onKeyPress='return NumCheck(event, this)' class='form-control input-sm' value='".$impa."' onChange=guardaraiua('".$d."','".$row[0]."') style='width:100%; border:thin; text-align:right' class='currency'/>
			<br><span class='currency'id='im". $row[0]."'>".number_format($totimp,2,'.',',')."</span>";//		
				
	}),
	array('db'=> 'd.pre_clave_int', 'dt'=> 'UtiA', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
		global $conectar;
		$conad = mysqli_query($conectar,"select adj_uti,adj_vr_unit from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$utia = $datad['adj_uti'];
			$vra = $datad['adj_vr_unit'];			
			if($vra=="" || $vra==NULL){$vra=0;}		
			$totuti = ($vra*$utia)/100;
			return "<input type='text' name='uti". $d."_".$row[0]."' id='uti". $d."_".$row[0]."' onKeyPress='return NumCheck(event, this)' class='form-control input-sm' value='".$utia."' onChange=guardaraiua('".$d."','".$row[0]."') style='width:100%; border:thin; text-align:right' class='currency'/>
			<br><span class='currency'id='ut". $row[0]."'>".number_format($totuti,2,'.',',')."</span>";//		
				
	}),
	array('db'=> 'd.pre_clave_int', 'dt'=> 'IvaA', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
		global $conectar;
		$conad = mysqli_query($conectar,"select adj_uti,adj_iva,adj_vr_unit from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$utia = $datad['adj_uti'];
			$ivaa = $datad['adj_iva'];
			$vra = $datad['adj_vr_unit'];			
			if($vra=="" || $vra==NULL){$vra=0;}		
			$totuti = ($vra*$utia)/100;
			$totiva = ($totuti*$ivaa)/100;
			
			return "<input type='text' name='iva". $d."_".$row[0]."' id='iva". $d."_".$row[0]."' onKeyPress='return NumCheck(event, this)' class='form-control input-sm' value='".$ivaa."' onChange=guardaraiua('".$d."','".$row[0]."') style='width:100%; border:thin; text-align:right' class='currency'/>
			<br><span class='currency'id='iv". $row[0]."'>".number_format($totiva,2,'.',',')."</span>";//		
				
	}),
	 array('db'=> 'd.pre_clave_int', 'dt'=> 'TotalA', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
	 		global $conectar;
	        /*
		
			$conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datco = mysqli_fetch_array($conco);
			$cantco = $datco['canc'];
			if($cantco=="" || $cantco==NULL){$cantco=0;}
			
			$conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit,adj_adm,adj_imp,adj_uti,adj_iva from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$adju = $datad['adj_valor']; 
			$canta = $datad['adj_cantidad'];
			$vra = $datad['adj_vr_unit'];
			$adma = $datad['adj_adm'];
			$impa = $datad['adj_imp'];
			$utia = $datad['adj_uti'];
			$ivaa = $datad['adj_iva'];
			if($adju=="" || $adju==NULL){$adju=0;} 
			if($canta=="" || $canta==NULL){$canta=0;} if($canta==0){$canta=$cantco;}
			if($vra=="" || $vra==NULL){$vra=0;}
			$totadm = ($vra*$adma)/100;
			$totimp = ($vra*$impa)/100;
			$totuti = ($vra*$utia)/100;
			$totiva = ($totuti*$ivaa)/100;
			$vra = $vra + $totadm + $totimp + $totuti + $totiva;
			$subta = $canta * $vra;	*/
         $cona = mysqli_query($conectar,"select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
         $data = mysqli_fetch_array($cona);
         $apli = $data['apli'];


         //totales iniciales
         $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."' LIMIT 1");
         $datad = mysqli_fetch_array($conad);
         $valor = $datad['vr'];
         $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];

         $con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant								 
			FROM insumos i JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."'");
         $dat = mysqli_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}

         $con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1 FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."'");
         $dat = mysqli_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}

         $cantidadi = $can + $can1;
         if($apli==0)
         {
             $totadm = ($valor*$adm)/100;
             $totimp = ($valor*$imp)/100;
             $totuti = ($valor*$uti)/100;
             $totiva = ($totuti*$iva)/100;
         }
         else
         {
             $totadm = ($valor*$adm)/100;
             $totimp = ($valor*$imp)/100;
             $totuti = ($valor*$uti)/100;
             $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
         }
         $valori = $valor + $totadm + $totimp + $totuti + $totiva;
         $totalinicial = $valori * $cantidadi;
         //totales actuales

       /*  $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_act) vr,AVG(pgi_adm_act) adm,AVG(pgi_imp_act) imp,AVG(pgi_uti_act) uti,AVG(pgi_iva_act) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."' AND (pgi_rend_act != pgi_rend_ini OR pgi_cant_act != pgi_cant_ini OR pgi_vr_act != pgi_vr_ini) LIMIT 1");
         $datad = mysqli_fetch_array($conad);
         $valor = $datad['vr'];
         $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];

         $con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_act * d.pgi_cant_act) AS Cant								 
			FROM insumos i JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' AND (d.pgi_rend_act != d.pgi_rend_ini OR d.pgi_cant_act != d.pgi_cant_ini OR d.pgi_vr_act != d.pgi_vr_ini
)");
         $dat = mysqli_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}

         $con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_act * pgi_cant_act*pgi_rend_sub_act) AS Cant1 FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' AND (d.pgi_rend_act != d.pgi_rend_ini
	OR d.pgi_cant_act != d.pgi_cant_ini OR d.pgi_vr_act != d.pgi_vr_ini OR d.pgi_rend_sub_act!=d.pgi_rend_sub_ini)");
         $dat = mysqli_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}

         $cantidad = $can + $can1;
         if($apli==0)
         {
             $totadm = ($valor*$adm)/100;
             $totimp = ($valor*$imp)/100;
             $totuti = ($valor*$uti)/100;
             $totiva = ($totuti*$iva)/100;
         }
         else
         {
             $totadm = ($valor*$adm)/100;
             $totimp = ($valor*$imp)/100;
             $totuti = ($valor*$uti)/100;
             $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
         }
         $valor = $valor + $totadm + $totimp + $totuti + $totiva;
         $totaladj = $valor * $cantidad;*/
         //totaladjudicado
         if($apli==0)
         {
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."'  and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act OR pa.pgi_rend_sub_act!=pa.pgi_rend_sub_ini)  ");

             $datsu = mysqli_fetch_array($consu);
             $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
             $totals = $totals + ($totads+$totims+$totuts+$totivs);

             //datos actual presupuesto
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act)");
             $datsu = mysqli_fetch_array($consu);
             $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totala = 0;}else {$totala  = $datsu['tot'];}
             $totala = $totala + ($totada+$totima+$totuta+$totiva)+ $totals;
         }
         else
         {
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini
	OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini OR pa.pgi_rend_sub_act!=pa.pgi_rend_sub_ini)");

             $datsu = mysqli_fetch_array($consu);
             $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
             $totals = $totals + ($totads+$totims+$totuts+$totivs);

             //datos actual presupuesto
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act) ");
             $datsu = mysqli_fetch_array($consu);
             $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totala = 0;}else {$totala  = $datsu['tot'];}
             $totala = $totala + ($totada+$totima+$totuta+$totiva)+ $totals;
         }
         $totaladj = $totala;

         //if($totaladj!=$totalinicial){}else{ $totaladj = 0;}

			return "<span id='subta".$row[0]."' class='currency'>$".number_format($totaladj,2,'.',',')."</span>";
	}),
	 array('db'=> 'd.pre_clave_int', 'dt'=> 'Disponible', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
	     //TOTSL PRESUPUUESTADO
	 	global $conectar;
		     $cona = mysqli_query($conectar,"select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
	 		 $data = mysqli_fetch_array($cona);
	         $apli = $data['apli'];
			 
			 $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			 $datad = mysqli_fetch_array($conad);
			 $valor = $datad['vr'];
			 $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
			 
			 $con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant								 
			FROM insumos i
			JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysqli_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}
			
			$con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1								 
			FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysqli_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}
			
			$cantidad = $can + $can1;
			if($apli==0)
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = ($totuti*$iva)/100;
			}
			else
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = (($totadm + $totimp + $totuti)*$iva)/100;
			}
			$valor = $valor + $totadm + $totimp + $totuti + $totiva;
			$totale = $valor * $cantidad;

           //TOTAL ADJUDICADO
			$conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datco = mysqli_fetch_array($conco);
			$cantco = $datco['canc'];
			if($cantco=="" || $cantco==NULL){$cantco=0;}
			$totalc = $valor * $cantco;
         /*
            $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_act) vr,AVG(pgi_adm_act) adm,AVG(pgi_imp_act) imp,AVG(pgi_uti_act) uti,AVG(pgi_iva_act) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
            $datad = mysqli_fetch_array($conad);
            $valor = $datad['vr'];
            $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];

            $con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_act * d.pgi_cant_act) AS Cant								 
            FROM insumos i JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
            WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' AND (d.pgi_rend_act != d.pgi_rend_ini OR d.pgi_cant_act != d.pgi_cant_ini OR d.pgi_vr_act != d.pgi_vr_ini
)");
            $dat = mysqli_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}

            $con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_act * pgi_cant_act*pgi_rend_sub_act) AS Cant1 FROM insumos i
            JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
            WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' AND (d.pgi_rend_act != d.pgi_rend_ini
	OR d.pgi_cant_act != d.pgi_cant_ini OR d.pgi_vr_act != d.pgi_vr_ini OR d.pgi_rend_sub_act!=d.pgi_rend_sub_ini)");
            $dat = mysqli_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}

            $cantidad = $can + $can1;
            if($apli==0)
            {
             $totadm = ($valor*$adm)/100;
             $totimp = ($valor*$imp)/100;
             $totuti = ($valor*$uti)/100;
             $totiva = ($totuti*$iva)/100;
            }
            else
            {
             $totadm = ($valor*$adm)/100;
             $totimp = ($valor*$imp)/100;
             $totuti = ($valor*$uti)/100;
             $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
            }
            $valor = $valor + $totadm + $totimp + $totuti + $totiva;
            $totaladj = $valor * $cantidad;
            //if($totale!=$totaladj){}else{$totaladj = 0;}*/
         //totaladjudicado
         if($apli==0)
         {
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."'  and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act OR pa.pgi_rend_sub_act!=pa.pgi_rend_sub_ini)  ");

             $datsu = mysqli_fetch_array($consu);
             $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
             $totals = $totals + ($totads+$totims+$totuts+$totivs);

             //datos actual presupuesto
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act)");
             $datsu = mysqli_fetch_array($consu);
             $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totala = 0;}else {$totala  = $datsu['tot'];}
             $totala = $totala + ($totada+$totima+$totuta+$totiva)+ $totals;
         }
         else
         {
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini
	OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini OR pa.pgi_rend_sub_act!=pa.pgi_rend_sub_ini)");

             $datsu = mysqli_fetch_array($consu);
             $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
             $totals = $totals + ($totads+$totims+$totuts+$totivs);

             //datos actual presupuesto
             $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
                 ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
                 ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
                 "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                 " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$d."' and pa.ins_clave_int = '".$row[0]."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act) ");
             $datsu = mysqli_fetch_array($consu);
             $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
             if($datsu['tot']=="" || $datsu['tot']==NULL){$totala = 0;}else {$totala  = $datsu['tot'];}
             $totala = $totala + ($totada+$totima+$totuta+$totiva)+ $totals;
         }
         $totaladj = $totala;

         /*$conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit from adjudicado where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			$datad = mysqli_fetch_array($conad);			
			$adju = $datad['adj_valor']; 
			$canta = $datad['adj_cantidad'];
			$vra = $datad['adj_vr_unit'];
			if($adju=="" || $adju==NULL){$adju=0;} 
			if($canta=="" || $canta==NULL){$canta=0;}
			if($canta==0){$canta=$cantco;}
			if($vra=="" || $vra==NULL){$vra=0;}
			$subta = $canta * $vra;		*/
			
			if($totaladj>0)
		    {
			   $dispo = floatval($totale)-floatval($totaladj);

			   $form = "OPCION 1 : ".$totale." - ".$totaladj."=".$dispo;
		    }
			else
			{
			   $dispo = floatval($totale)-floatval($totalc);
                $form = "OPCION 2: ".$totale." - ".$totalc."=".$dispo;
			}
			if($dispo<0){
			    $dispo = $dispo * (-1);
			    $dispo = "($".number_format($dispo,2,'.',',').")";
			    $col = "red";
            }else{
			    $col = "inherit";
			    $dispo= "$".number_format($dispo,2,'.',',');

            }
							
			return "<span style='color:".$col."' title='".$form."' id='dispo".$row[0]."' class='currency'>".$dispo."</span>";
	}),
    array( 'db' => 'd.pgi_creacion', 'dt' => 'Creacion', 'field' => 'pgi_creacion' ), //r1
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( '../../data/ssp.class.php' );

 $groupBy = 'i.ins_clave_int';
 $joinQuery = " FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_insumo d on d.ins_clave_int = i.ins_clave_int   ";

 //$unionQuery2 = "FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_sub_insumo ds on ds.ins_clave_int = i.ins_clave_int"

$extraWhere =  " d.pre_clave_int = '".$pre."'";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
);

