<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idpresupuesto = $_GET['edi'];
include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('safe_mode', 0);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}
//DATOS DEL PRESUPUESTO
$con  = mysqli_query($conectar,"select UPPER(per_nombre) nom,UPPER(per_apellido)  ape,UPPER(per_razon) cli, per_documento,pre_nombre,pre_fecha,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_usu_creacion,pre_cliente,tpp_clave_int,date_format(pre_fecha,'%Y%m%d') as fec,pre_apli_iva,pre_coordinador as cor,pre_av_plan,pre_av_real,pre_documento  from presupuesto pr left outer join persona p on p.per_clave_int = pr.per_clave_int where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysqli_fetch_array($con);
$fech = $dat['fec'];
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$nomo = $dat['pre_nombre'];
$fechai  =$dat['pre_fecha'];
$apli = $dat['pre_apli_iva'];
$tpp = $dat['tpp_clave_int'];
$precliente = $dat['pre_cliente'];
$avplan = $dat['pre_av_plan'];
$avreal = $dat['pre_av_real'];
$atraso = $avplan - $avreal;
$codproyecto = $dat['pre_codigo'];
$docproyecto = $dat['pre_documento'];
$nomcliente = $dat['cli'];
$cor = $dat['cor'];
$cont = mysqli_query($conectar,"select tpp_nombre from tipoproyecto where tpp_clave_int = '".$tpp."'");
$datt =  mysqli_fetch_array($cont);
$tppr =  strtoupper($datt['tpp_nombre']);

if($precliente!=""){$cliente=$precliente;}else { $cliente = $nomcliente." - NIT: ".$dat['per_documento'];}
$creado = $dat['pre_usu_creacion'];
$con = mysqli_query($conectar,"select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$creado."' limit 1");
$dat = mysqli_fetch_array($con);
$creadopor = $dat['usu'];
$car  = $dat['car_clave_int'];
$con = mysqli_query($conectar,"select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$cor."' limit 1");
$dat = mysqli_fetch_array($con);
$aprobadopor = $dat['usu'];
$carc  = $dat['car_clave_int'];

$conc = mysqli_query($conectar,"select UPPER(car_nombre) car from cargos where car_clave_int = '".$car."'");
$datc = mysqli_fetch_array($conc);
$cargo = $datc['car'];
$conc = mysqli_query($conectar,"select UPPER(car_nombre) car from cargos where car_clave_int = '".$carc."'");
$datc = mysqli_fetch_array($conc);
$cargoc = $datc['car'];

$consu = mysqli_query($conectar,"SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysqli_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

$totadm = ($totalc * $adm)/100;
$totimp = ($totalc * $imp)/100;
$totuti = ($totalc * $uti)/100;
if($apli==0){ $totiva = ($totuti * $iva)/100; }else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100; }
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$con  = mysqli_query($conectar,"select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysqli_fetch_array($con);
$adma = $dat['pri_administracion'];
$ivaa = $dat['pri_iva'];
$impa = $dat['pri_imprevisto'];
$utia = $dat['pri_utilidades'];

$totadma = ($totalca * $adma)/100;
$totimpa = ($totalca * $impa)/100;
$totutia = ($totalca * $utia)/100;
if($apli==0) { $totivaa = ($totutia * $ivaa)/100; } else { $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;

$alccos = $totalc - $totalca;
$alcpre = $totpre - $totprea;
$alcadm = $totadm - $totadma;
$alcimp = $totimp - $totimpa;
$alcuti = $totuti - $totutia;
$alciva = $totiva - $totivaa;

$cons = mysqli_query($conectar,"select sum(cpe_valor_neto) net,sum(cpe_iva) iv, sum(cpe_amortizacion)  as am,sum(cpe_ret_garantia) gar,sum(cpe_valor_aprobado) as ap,sum(cpe_anticipo) as anti from control_egreso where pre_clave_int = '".$idpresupuesto."'");
$dat = mysqli_fetch_array($cons);
if($dat['net']=="" || $dat['net']==NULL){$totalneto = 0;}else{$totalneto = $dat['net'];}
if($dat['iv']=="" || $dat['iv']==NULL){$totaliva = 0;}else{$totaliva = $dat['iv'];}
$totalbru = $totalneto + $totaliva;
if($dat['am']=="" || $dat['am']==NULL){$totalamo = 0;}else{$totalamo = $dat['am'];}
if($dat['anti']=="" || $dat['anti']==NULL){$totalanti = 0;}else{$totalanti = $dat['anti'];}
if($dat['gar']=="" || $dat['gar']==NULL){$totalgarantia = 0;}else{$totalgarantia = $dat['gar'];}
if($dat['ap']=="" || $dat['ap']==NULL){$totalaprobado = 0;}else{$totalaprobado = $dat['ap'];}

$saldo = $totpre - ($totalbru + $totalanti - $totalamo);

$totalsaldo = $totalaprobado - $totalbru ;

if($totalsaldo<0){$totalsaldo = "(". number_format($totalsaldo*(-1),2,'.',',').")"; $col1 = "red";}
else{$totalsaldo = number_format($totalsaldo,2,'.',','); $col1="black";}

/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);

// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => array('argb' => 'FF993300'),
        ),
    ),
);



// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
    ->setLastModifiedBy("Pavas.co")
    ->setTitle("Informe Presupuesto")
    ->setSubject("Informe Presupuesto")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Informes");



$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1' , "")
    ->setCellValue('D1' , "LISTADO DE RECURSOS")
    ->setCellValue('D2' , "VERSIÓN: 05")
    ->setCellValue('G2' , "FECHA VERSION: 30/03/2016")
    ->setCellValue('J2' , "APROBADO POR: NATALÍ PINZÓN")
    ->setCellValue('B3' , "INTERVENTOR: LINEA GLOBAL INGENIERIA S.A.S ")
    ->setCellValue('B4' , "CLIENTE: ".$cliente)
    ->setCellValue('B5' , "NOMBRE DE LA OBRA: ".$nomo)
    ->setCellValue('B6' , "FECHA INICIAL: ".$fechai)
    ->setCellValue('A8' , "")
    ->setCellValue('B8' , "CÓDIGO")
    ->setCellValue('C8' , "DESCRIPCIÓN")
    ->setCellValue('D8' , "UND")
    ->setCellValue('E8' , "PRESUPUESTADA")
    ->setCellValue('E9' , "CANTIDAD")
    ->setCellValue('F9' , "V/UNITARIO")
    ->setCellValue('G9' , "V/TOTAL")
    ->setCellValue('H8' , "COMPROMETIDO")
    ->setCellValue('H9' , "CANT")
    ->setCellValue('I9' , "V/UNITARIO")
    ->setCellValue('J9' , "V/TOTAL")
    ->setCellValue('K8' , "ADJUDICADO")
    ->setCellValue('L8' , "DISPONIBLE")
    ->mergeCells('B1:C2')
    ->mergeCells('D1:L1')
    ->mergeCells('D2:F2')
    ->mergeCells('G2:I2')
    ->mergeCells('J2:L2')
    ->mergeCells('B3:L3')
    ->mergeCells('B4:L4')
    ->mergeCells('B5:L5')
    ->mergeCells('B6:L6')
    ->mergeCells('B7:L7')

    ->mergeCells('B8:B9')
    ->mergeCells('C8:C9')
    ->mergeCells('D8:D9')
    ->mergeCells('E8:G8')
    ->mergeCells('H8:J8')
    ->mergeCells('K8:K9')
    ->mergeCells('L8:L9')
;
cellColor('B1:L1', 'FFFFFF');
cellColor('B8:L9', '92D050');
$objPHPExcel->getActiveSheet()->getStyle('B1:L1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("B1:L2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B8:L9')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle("B8:L9")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D1:L2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B3:L6')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('D1:L1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('D2:L2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B1:L6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
/*$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");*/
//$conpre = mysqli_query($conectar,"select i.ins_clave_int Id,i.ins_nombre Ins,u.uni_codigo Cod,d.pgi_vr_ini FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_insumo d on d.ins_clave_int = i.ins_clave_int left outer join pre_gru_cap_act_sub_insumo ds on ds.pre_clave_int = d.pre_clave_int where d.pre_clave_int = '".$idpresupuesto."' group by i.ins_clave_int,d.pgi_vr_ini order by Ins ASC");

$conpre = mysqli_query($conectar,"select i.ins_clave_int Id,i.ins_nombre Ins,u.uni_codigo Cod,d.pgi_vr_ini FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_insumo d on d.ins_clave_int = i.ins_clave_int  where d.pre_clave_int = '".$idpresupuesto."' group by i.ins_clave_int,d.pgi_vr_ini 
UNION
select i.ins_clave_int Id,i.ins_nombre Ins,u.uni_codigo Cod,d.pgi_vr_ini FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_sub_insumo d on d.ins_clave_int = i.ins_clave_int  where d.pre_clave_int = '".$idpresupuesto."' group by i.ins_clave_int,d.pgi_vr_ini order by Ins ASC");


$numpre = mysqli_num_rows($conpre);
$hasta = $numpre + 9;

$acum = $hasta;
$filc = 10;
$subtotal  = 0;
for ($i = 10; $i <= $hasta; $i++)
{
    $dat = mysqli_fetch_array($conpre);

    $cod = $dat['Id'];
    $ins = utf8_encode(convert_htmlentities($dat['Ins']));
    $uni = $dat['Cod'];
    $vri = $dat['pgi_vr_ini'];

    $con = mysqli_query($conectar,"SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant 							 
    FROM insumos i
    JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
    WHERE d.pre_clave_int = '".$idpresupuesto."' and d.ins_clave_int = '".$cod."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0 and d.pgi_vr_ini = '".$vri."'");
    $dati = mysqli_fetch_array($con); $can = $dati['Cant']; if($can=="" || $can==NULL){$can = 0;}

    $con2 = mysqli_query($conectar,"SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1								 
    FROM insumos i
    JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
    WHERE d.pre_clave_int = '".$idpresupuesto."' and d.ins_clave_int = '".$cod."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0 and d.pgi_vr_ini = '".$vri."'");
    $dati = mysqli_fetch_array($con2);  $can1 = $dati['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}
    $cantidad = $can + $can1; //cantidad inicial

    $conad = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr ,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."' and pgi_vr_ini = '".$vri."'");
    $datad = mysqli_fetch_array($conad);
    $numaiu = mysqli_num_rows($conad);
    $valor = $vri;
    $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
    if($apli==0)
    {
        $totadm = ($valor*$adm)/100;
        $totimp = ($valor*$imp)/100;
        $totuti = ($valor*$uti)/100;
        $totiva = ($totuti*$iva)/100;
    }
    else
    {
        $totadm = ($valor*$adm)/100;
        $totimp = ($valor*$imp)/100;
        $totuti = ($valor*$uti)/100;
        $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
    }
    if($datad['vr']=="" || $datad['vr']==NULL)
    {
        //seeleccionar el aiu en subanalisis
        $conad2 = mysqli_query($conectar,"SELECT AVG(pgi_vr_ini) vr ,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_sub_insumo WHERE pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."' and pgi_vr_ini = '".$vri."'");
        $datad2 = mysqli_fetch_array($conad2);
        $adm = $datad2['adm']; 
        $imp = $datad2['imp']; 
        $uti = $datad2['uti']; 
        $iva = $datad2['iva'];
        if($apli==0)
        {
            $totadm = ($valor*$adm)/100;
            $totimp = ($valor*$imp)/100;
            $totuti = ($valor*$uti)/100;
            $totiva = ($totuti*$iva)/100;
        }
        else
        {
            $totadm = ($valor*$adm)/100;
            $totimp = ($valor*$imp)/100;
            $totuti = ($valor*$uti)/100;
            $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
        }
    }
    $valor = $valor + $totadm + $totimp + $totuti + $totiva;//valor unitario inicial
    $totale = $valor * $cantidad; //total presupuestado inicial
    //cantidad comprometidad
    $conco = mysqli_query($conectar,"select sum(pai_cant_comprometida) canc from partida_item  where pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'");
    $datco = mysqli_fetch_array($conco);
    $cantco = $datco['canc'];
    if($cantco=="" || $cantco==NULL){$cantco=0;}
    //TOTAL COMPROMETIDO
    $totalc = $valor * $cantco;


    //CANTIDAD ADJUDICADA
   /* $conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit from adjudicado where pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'");
    $datad = mysqli_fetch_array($conad);
    $canta = $datad['adj_cantidad'];
    if($canta=="" || $canta==NULL){$canta=0;} if($canta==0){$canta=$cantco;}
    $vra = $datad['adj_vr_unit'];
    if($vra=="" || $vra==NULL){$vra=0;}*/

    //valor adju
    /* $conad = mysqli_query($conectar,"select adj_valor,adj_cantidad,adj_vr_unit from adjudicado where pre_clave_int = '".$idpresupuesto."' and ins_clave_int = '".$cod."'");
     $datad = mysqli_fetch_array($conad);*/



    //totaladjudicado
    if($apli==0)
    {
        $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
            ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
            " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'  and pa.ins_clave_int = '".$cod."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act OR pa.pgi_rend_sub_act!=pa.pgi_rend_sub_ini) and pa.pgi_vr_ini= '".$vri."' ");

        $datsu = mysqli_fetch_array($consu);
        $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
        if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
        $totals = $totals + ($totads+$totims+$totuts+$totivs);

        //datos actual presupuesto
        $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
            ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
            " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.ins_clave_int = '".$cod."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act) and pa.pgi_vr_ini= '".$vri."' ");
        $datsu = mysqli_fetch_array($consu);
        $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
        if($datsu['tot']=="" || $datsu['tot']==NULL){$totala = 0;}else {$totala  = $datsu['tot'];}
        $totala = $totala + ($totada+$totima+$totuta+$totiva)+ $totals;
    }
    else
    {
        $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
            ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
            "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
            "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
            " from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.ins_clave_int = '".$cod."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act OR pa.pgi_rend_sub_act!=pa.pgi_rend_sub_ini) and pa.pgi_vr_ini= '".$vri."' ");

        $datsu = mysqli_fetch_array($consu);
        $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
        if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
        $totals = $totals + ($totads+$totims+$totuts+$totivs);

        //datos actual presupuesto
        $consu = mysqli_query($conectar,"select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
            ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
            "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
            "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
            " from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.ins_clave_int = '".$cod."' AND (pa.pgi_rend_act != pa.pgi_rend_ini OR pa.pgi_cant_act != pa.pgi_cant_ini OR pa.pgi_vr_act != pa.pgi_vr_ini  OR pa.pgi_adm_ini !=pa.pgi_adm_act  OR pa. pgi_imp_ini !=pa.pgi_adm_act OR pa.pgi_uti_ini !=pa.pgi_uti_act OR pa.pgi_iva_ini !=pa.pgi_iva_act) and pa.pgi_vr_ini= '".$vri."' ");
        $datsu = mysqli_fetch_array($consu);
        $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
        if($datsu['tot']=="" || $datsu['tot']==NULL){$totala = 0;}else {$totala  = $datsu['tot'];}
        $totala = $totala + ($totada+$totima+$totuta+$totiva)+ $totals;
    }
    $totaladj = $totala;

    if((int)$totaladj>0)
    {
        $dispo = (int)$totale-(int)$totaladj;
        $form = "OPCION 1 : ".$totale." - ".$totaladj."=".$dispo;
    }
    else
    {
        $dispo = (int)$totale-(int)$totalc;
        $form = "OPCION 2: ".$totale." - ".$totalc."=".$dispo;
    }

    /* $val = $dat['Val'];
     $cant = $dat['Suma'];

     $tot = $dat['Sumat'];
     $totad = $datsu['Sumad']; $totim = $datsu['Sumai']; $totut = $datsu['Sumau']; $totiv = $datsu['Sumaiv'];
     $total = $tot + $totad + $totim + $totut + $totiv;*/

    /*$codinsumo = $dat['codi'];
    $insumo = $dat['ins'];
    $unidad = $dat['uni'];

    //$totad = $dat['totad']; $dat = $dat['totim']; $totut = $dat['totut']; $totiv = $dat['totiv'];
    $cant = $dat['cant'];
    $to = $dat['tot']; //+ ($totad+$totim+$totut+$totiv);
    $total = $cant * $to;*/

    $subtotal = $subtotal + ($totale);
    //$totale = number_format($totale,2,'.','');
    //$cantidad = number_format($cantidad,2,'.','');
    //$valor = number_format($valor,2,'.','');

    $objPHPExcel->getActiveSheet()->setCellValue('B' . $filc, $cod)
        ->setCellValue('C' . $filc, $ins)
        //->setCellValue('D' . $filc, $uni)
        //->setCellValue('E' . $filc, $cant)
        ->setCellValue('F' . $filc, $valor)
        ->setCellValue('G' . $filc, "=E".$filc."*F".$filc)
        ->setCellValue('I' . $filc, $valor)
        ->setCellValue('J' . $filc, "=H".$filc."*I".$filc)
        ->setCellValue('K' . $filc, $totaladj)
        ->setCellValue('L' . $filc, '=IF(K'.$filc.'>0,G'.$filc.'-K'.$filc.',G'.$filc.'-J'.$filc.')')
    ;
    cellColor('A' . $filc.':L'.$filc, 'FFFFFF');
    //FORMATO MONEDA
    $objPHPExcel->getActiveSheet()->getStyle('F'.$filc.':G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

    $objPHPExcel->getActiveSheet()->getStyle('I'.$filc.':L'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');


    $objPHPExcel->getActiveSheet()->getCell('D' . $filc)->setValueExplicit($uni,PHPExcel_Cell_DataType::TYPE_STRING);
    $objPHPExcel->getActiveSheet()->getCell('E' . $filc)->setValueExplicit($cantidad,PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $objPHPExcel->getActiveSheet()->getCell('H' . $filc)->setValueExplicit($cantco,PHPExcel_Cell_DataType::TYPE_NUMERIC);
    //$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':E'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':L'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('B' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('D' . $filc.':E' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle('H' . $filc.':H' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

    $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);

    //FIN CONSULTAS

    $filc = $filc + 1;
}

$filc1 = $filc;
$filcu = $filc-1;
// $filc2 = $filc + 2;
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2.43);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(65);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(17);
$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$filc1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$filc1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
//$objPHPExcel->getActiveSheet()->getStyle('F2:G'.$filc1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('B8:L'.$filc1)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->setCellValue('B'.$filc1 , "TOTAL")
    ->setCellValue('G'.$filc1 ,"=SUM(G10:G".$filcu.")")
    ->setCellValue('J'.$filc1 ,"=SUM(J10:J".$filcu.")")
    ->setCellValue('K'.$filc1 ,"=SUM(K10:K".$filcu.")")
    ->setCellValue('L'.$filc1 ,'=IF(K'.$filc1.'>0,G'.$filc1.'-K'.$filc1.',G'.$filc1.'-J'.$filc1.')')
    ->mergeCells('A1:A'.$filc1)->mergeCells('B'.$filc1.':F'.$filc1);
$objPHPExcel->getActiveSheet()->getStyle('B'.$filc1.':L'.$filc1)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B' .$filc1)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
cellColor('B'.$filc1.':L'.$filc1, 'C4D79B');
//FORMTATO MONEDA TOTALES
$objPHPExcel->getActiveSheet()->getStyle('G'.$filc1)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

$objPHPExcel->getActiveSheet()->getStyle('J'.$filc1.":L".$filc1)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');


$objPHPExcel->getActiveSheet()->getStyle('B8:L' . $filc1)->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getRowDimension($filc1)->setRowHeight(12.75);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('B1');
$objDrawing->setOffsetX(12);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('RECURSOS');
//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
$callStartTime = microtime(true);
$archivo =  date('Ymd').' RECURSOS '.$tppr.' '.$nomo.'.xlsx';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//$objWriter->save(str_replace(__FILE__,'descargas/'.$archivo,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
// Set password for readonly activesheet
/*
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("P4v4s2017.*");*/
// Set password for readonly data

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');