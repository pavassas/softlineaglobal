<?php
include('../../data/Conexion.php');
error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = strtoupper($dato['prf_descripcion']);
	$percla = $dato['prf_clave_int'];
	$coordi = $dato['usu_coordinador'];
	
	$con = mysqli_query($conectar,"select * from permiso pr  where pr.usu_clave_int = '".$idUsuario."' and pr.ven_clave_int = 12");
	$dato = mysqli_fetch_array($con);
	$metact = $dato['per_metodo'];
	
	$unidades = 0;
	$con = mysqli_query($conectar,"select uni_clave_int from unidades where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $uni = $dat['uni_clave_int'];
		   $idsu[] = $uni;
	   }
	   $unidades = implode(',',$idsu);
	}
	$insumos = 0;
	$con = mysqli_query($conectar,"select ins_clave_int from insumos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['ins_clave_int'];
		   $idsu[] = $ins;
	   }
	   $insumos = implode(',',$idsu);
	}
	
	$clientes = 0;
	$con = mysqli_query($conectar,"select per_clave_int from persona where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['per_clave_int'];
		   $idsu[] = $ins;
	   }
	   $clientes = implode(',',$idsu);
	}
$tipoproyectos = 0;
	$con = mysqli_query($conectar,"select tpp_clave_int from tipoproyecto where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['tpp_clave_int'];
		   $idsu[] = $ins;
	   }
	   $tipoproyectos = implode(',',$idsu);
	}
	$tipocontratos = 0;
	$con = mysqli_query($conectar,"select tpc_clave_int from tipocontrato where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['tpc_clave_int'];
		   $idsu[] = $ins;
	   }
	   $tipocontratos = implode(',',$idsu);
	}
	
	
?>
  <script src="js/jsuploadpresupuesto.js"></script>

<section>
<div class="row" style="background-color:#222d32">
<div class="col-md-11">
<h5><a ui-sref="Presupuesto"><i class="fa fa-dashboard"></i>CONTROL PRESUPUESTO</a><small class="active">/ <a ui-sref="Importacionpresupuesto" ui-sref-opts="{reload: true}">IMPORTACIÓN</a></small></h5></div><div class="col-md-1"></div>
  
</div>
<div class="row" ng-controller="ProgressCtrl">
    <div class="col-xs-12">
    
        <div class="box">
            <div class="box-body">
            <span id="msn"></span>
            <div id="mensaje"></div>
            <div id="respuesta" class="alert"></div>
       <form class="formulario1" enctype="multipart/form-data">
        <div class="form-group">  
        <input type="hidden" id="opcestado" name="opcestado" value="5">
        <input type="hidden" id="opccontrol" name="opccontrol" value="1">
     <div class="col-md-4"><strong>Nombre:</strong>
     <div class="ui corner labeled input">
    <input type="text" id="txtnombre" name="txtnombre" class="form-control input-sm" placeholder="Escribe aqui el nombre">
     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
    </div>  
    <div class="col-md-4"><strong>Descripcion:</span></strong>
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" placeholder="Escribe aqui la descripcion">
    </div>
    <div class="col-md-4"><strong>Coordinador:</strong>
    <div class="ui corner labeled input">
          <select name="selcoordinador" id="selcoordinador" class="form-control input-sm">
          <option value="">--seleccione--</option>
          <?php
		   $con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
		   while($dat = mysqli_fetch_array($con))
		   {
			  $idp = $dat['usu_clave_int'];
			  $nom = $dat['usu_nombre'];
			  $ema = $dat['usu_email'];
		     ?>
             <option value="<?php echo $idp;?>" <?php if($coordi==$idp){ echo 'selected';}?>><?Php echo $nom." - ".$ema;?></option>
             <?php
	
		  }
		  ?>
          </select>
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
          </div>
    </div>
       <div class="form-group">
         <div class="col-md-4"><strong>Tipo de Proyecto:</strong>
         <div class="ui corner labeled input">
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control input-sm" onchange="CRUDPRESUPUESTOINICIAL('OTROTIPROYECTO','','','','')">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where est_clave_int = 1 or tpp_clave_int in(".$tipoproyectos.")");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
        <div class="col-md-4"> Otro:<input type="text" class="form-control input-sm" name="otrotipoproyecto" id="otrotipoproyecto" placeholder="Nuevo Tipo de Proyecto"/>
        
         </div> 
          <div class="col-md-4"><strong>Cliente:</strong>
          <div class="ui corner labeled input">
          <input name="selcliente" id="selcliente" class="form-control input-sm" type="text"> 
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>    
          </div>
    </div>    
   
    <div class="form-group">
         <div class="col-md-4"><strong>Pais:</strong>
         <div class="ui corner labeled input">
          <select name="selpais" id="selpais" class="form-control input-sm" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Departamento:</strong>
         <div class="ui corner labeled input">
          <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-4"><strong>Ciudad:</strong>
         <div class="ui corner labeled input">
          <select name="selciudad" id="selciudad" class="form-control input-sm">
         <option value="">--seleccione--</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    
    <div class="form-group">
    <div class="col-md-4"><strong>Tipo de Contrato:</strong>
         <div class="ui corner labeled input">
         <select name="seltipocontrato"id="seltipocontrato" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpc_clave_int,tpc_nombre from tipocontrato where est_clave_int = 1 or tpc_clave_int in(".$tipocontratos.")");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpc_clave_int'];
			$nom = $dat['tpc_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    
         <div class="col-md-4"><strong>Fecha Inicial:</strong>
         <div class="ui corner labeled input">
         <input type="date" name="fecha" id="fecha" class="form-control input-sm" value="<?php echo date('Y-m-d');?>"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        </div>
        <div class="col-md-4">
        <strong>Archivo:</strong>
        <div class="ui corner labeled input">
         <input type="file" name="archivo" id="archivo" class="form-control input-sm" />
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        </div>
        <div class="col-md-2"> <br><a  onClick="subirArchivos()" role="button" class="btn btn-success"><i class="glyphicon glyphicon-upload"></i>Importar</a></div>
     </div>
       <ul class="list-inline">
          <li><a class="link-blue text-sm"  href="modulos/formatos/FORMATOIMPORTACIONPRESUPUESTO.xls?<?php echo time(); ?>">Archivo Importacion Presupuesto <i class="fa fa-file-excel-o"></i>Descargar</a> </li>
          <li><a class="link-black text-sm" ><i class="fa fa-file-excel-o"></i>Formato de Archivo: Excel 97-2003 Extension .xls</a></li>
          </ul>
     
       <div id="archivos_subidos"></div>
       <div class="form-group" id="progreso" style="display:none"><uib-progressbar  class="progress-striped active"  max="max" value="100"><span style="color:white; white-space:nowrap;">Analizando Archivo Por favor Espere...</span></uib-progressbar></div>
       <div id="publishform" class="form-group"></div>
       </form>

            </div>
        </div>
    </div>
 </div>
 </section>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <div id="contenido2"></div>
        <span id="msn1"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a role="button" name="" class="btn btn-primary" id="btnguardar"  rel="">Guardar Cambios</a>
      </div>
    </div>
  </div>
</div>
