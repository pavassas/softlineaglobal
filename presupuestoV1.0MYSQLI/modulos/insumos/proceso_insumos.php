<?php

include ("../../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$percla = $dato['prf_clave_int'];
$insumos = 0;
	$con = mysqli_query($conectar,"select ins_clave_int from insumos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['ins_clave_int'];
		   $idsu[] = $ins;
	   }
	   $insumos = implode(',',$idsu);
	}

// DB table to use
$table = 'insumos';

// Table's primary key
$primaryKey = 'i.ins_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'i.ins_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ins_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Editar' ,'field' => 'ins_clave_int','formatter'=>function($d,$row){
	     $idUsuario= $_COOKIE["usIdentificacion"]; 
		   if(strtoupper($row[12])=="ADMINISTRADOR" || ($row[13]==$idUsuario))
		   {
			return "<a class='btn btn-block btn-default btn-xs' onClick=CRUDINSUMOS('EDITAR','".$d."') data-toggle='modal' data-target='#myModal' style='width:20px; height:20px'><i class='glyphicon glyphicon-pencil'></i></a>";
		   }
		   else 
		   {
			   return "";
		   }
	} ),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Eliminar' ,'field' => 'ins_clave_int','formatter'=>function($d,$row){
	      $idUsuario= $_COOKIE["usIdentificacion"]; 
		   if(strtoupper($row[12])=="ADMINISTRADOR" || ($row[13]==$idUsuario))
		   {
			return "<a class='btn btn-block btn-danger btn-xs' onClick=CRUDINSUMOS('ELIMINAR','".$d."') style='width:20px; height:20px'><i class='glyphicon glyphicon-trash'></i></a>";
		   }
		   else 
		   {
			   return "";
		   }
	} ),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Codigo','field' => 'ins_clave_int' ),
	array( 'db' => 't.tpi_nombre', 'dt' => 'Tipo' ,'field' => 'tpi_nombre'),
	array( 'db' => "t.tpi_tipologia", 'dt' => 'Tipologia' ,'field' => 'tpi_tipologia'),
	array( 'db' => 'i.ins_nombre', 'dt' => 'Nombre','field' => 'ins_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Cod' ,'field' => 'uni_codigo' ),
	array( 'db' => 'u.uni_nombre', 'dt' => 'Unidad' ,'field' => 'uni_nombre'),
	array( 'db' => 'i.ins_valor', 'dt' => 'Valor' ,'field' => 'ins_valor','formatter' => function( $d, $row ) {
            return '$'.number_format($d,2,',','.');}
        ),	
	array( 'db'  => 'e.est_nombre','dt' => 'Estado' ,'field' => 'est_nombre'),
	array( 'db'  => 'i.ins_descripcion','dt' => 'Descripcion' ,'field' => 'ins_descripcion'),
	array( 'db'  => "'".$perfil."'",'dt' => 'Perfil' ,'field' => 'Perfil','as'=>"Perfil"),
	array( 'db'  => 'i.usu_clave_int','dt' => 'Usuario' ,'field' => 'usu_clave_int'),
    array( 'db'  => 'c.ciu_nombre','dt' => 'Ciudad' ,'field' => 'ciu_nombre'),
    array( 'db'  => 'tp.tpp_nombre','dt' => 'TipoProyecto' ,'field' => 'tpp_nombre')
);


$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

$whereAll = "";
 $groupBy = ' i.ins_clave_int ';
// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$joinQuery =  " FROM insumos AS i join tipoinsumos AS t on t.tpi_clave_int = i.tpi_clave_int join unidades AS  u on u.uni_clave_int  = i.uni_clave_int join estados AS e on e.est_clave_int = i.est_clave_int  LEFT OUTER JOIN ciudad c ON c.ciu_clave_int  = i.ciu_clave_int LEFT OUTER JOIN tipoproyecto tp ON tp.tpp_clave_int = i.tpp_clave_int";
$extraWhere = " e.est_clave_int in(0,1)  or i.ins_clave_int in(".$insumos.") ";  //u.salary >= 90000";  
echo json_encode(
//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

