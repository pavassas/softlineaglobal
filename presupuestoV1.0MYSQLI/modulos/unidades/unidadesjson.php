<?php
include ("../../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$percla = $dato['prf_clave_int'];
// DB table to use
$table = 'unidades';

// Table's primary key
$primaryKey = 'u.uni_clave_int';
$unidades = 0;
$con = mysqli_query($conectar,"select uni_clave_int from unidades where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
$num = mysqli_num_rows($con);
if($num>0)
{
	$idsu = array();
   for($u=0;$u<$num;$u++)
   {
	   $dat = mysqli_fetch_array($con);
	   $uni = $dat['uni_clave_int'];
	   $idsu[] = $uni;
   }
   $unidades = implode(',',$idsu);
}

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'u.uni_clave_int',
		'dt' => 'DT_RowId', 'field' => 'uni_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_uni'.$d;
		}
	),
	array( 'db' => 'u.uni_clave_int', 'dt' => 'Editar' ,'field' => 'uni_clave_int','formatter'=>function($d,$row){
		   
		   $idUsuario= $_COOKIE["usIdentificacion"]; 
		   if(strtoupper($row[8])=="ADMINISTRADOR" || ($row[9]==$idUsuario))
		   {
			return "<a class='btn btn-block btn-default btn-xs' onClick=CRUDUNIDADES('EDITAR','".$d."') data-toggle='modal' data-target='#myModal' style='width:20px; height:20px'><i class='glyphicon glyphicon-pencil'></i></a>";
		   }
		   else 
		   {
			   return "";
		   }
		} ),
	array( 'db' => 'u.uni_clave_int', 'dt' => 'Eliminar' ,'field' => 'uni_clave_int','formatter'=>function($d,$row){
		 $idUsuario= $_COOKIE["usIdentificacion"]; 
		   if(strtoupper($row[8])=="ADMINISTRADOR" || ($row[9]==$idUsuario))
		   {
				return 	"<a class='btn btn-block btn-danger btn-xs' onClick=CRUDUNIDADES('ELIMINAR','".$d."') style='width:20px; height:20px'><i class='glyphicon glyphicon-trash'></i></a>";
		   }
		   else
		   {
			   return "";
		   }
		 } ),
	array( 'db' => 'u.uni_clave_int', 'dt' => 'Codigo','field' => 'uni_clave_int' ),
	array( 'db' => 'u.uni_nombre', 'dt' => 'Nombre','field' => 'uni_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Cod' ,'field' => 'uni_codigo' ),
	array( 'db' => 'u.uni_nombre', 'dt' => 'Unidad' ,'field' => 'uni_nombre'),
	array( 'db'  => 'e.est_nombre','dt' => 'Estado' ,'field' => 'est_nombre'),
	array( 'db'  => "'".$perfil."'",'dt' => 'Perfil' ,'field' => 'Perfil','as'=>"Perfil"),
	array( 'db'  => 'u.usu_clave_int','dt' => 'Usuario' ,'field' => 'usu_clave_int'),
);


$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuestov1.1',
	'host' => '127.0.0.1'
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

$whereAll = "";
$groupBy = ' u.uni_clave_int ';
$joinQuery =  " FROM unidades u join estados AS e on e.est_clave_int = u.est_clave_int ";
$extraWhere = " e.est_clave_int in(0,1) or u.uni_clave_int in(".$unidades.") ";  
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

