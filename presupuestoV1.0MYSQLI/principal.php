<?php
	error_reporting(0);
	include('data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario = $_COOKIE["usIdentificacion"];
	$sistema = $_COOKIE['sistema'];
	$clave= $_COOKIE["clave"];
	
	// verifica si no se ha loggeado
	if(!isset($_COOKIE["usIdentificacion"]))
	{
		header("LOCATION:data/logout.php");
	}
	else
	{
	
	}
  include('data/permisos.php');
	date_default_timezone_set('America/Bogota');
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = strtoupper($dato['prf_descripcion']);
	$percla = $dato['prf_clave_int'];
	$nombreusuario = $dato['usu_nombre'];
	$cla = $dato['usu_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
  $imgperfil = $dato['usu_imagen'];
  $fontsize = $dato['usu_tam_fuente'];
  if($imgperfil=="" || $imgperfil==NULL || !file_exists($imgperfil))
  {
    $imgperfil = "dist/img/default-user.png";
  }
	
	
	$con = mysqli_query($conectar,"select * from permiso pr where pr.usu_clave_int = '".$idUsuario."' and pr.ven_clave_int = 12");
	$dato = mysqli_fetch_array($con);
	$metact = $dato['per_metodo'];

	
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="dist/img/favicon.ico" rel="shortcut icon">
    <title>Presupuestos - Sistema de Información para el manejo de presupuestos(<?php echo $sistema;?>)</title>
    <script type="text/javascript" src="dist/cdn/jquery.min.js?<?php echo time(); ?>"></script>
    <!-- Instalo angular -->
    <script src="dist/cdn/angular.min.js?<?php echo time(); ?>"></script>
    <!-- Rutas angular -->
    <script src="dist/js/angular-ui-router.js?<?php echo time(); ?>"></script> 
    <script src="dist/js/ui-bootstrap-tpls-0.14.3.min.js?<?php echo time(); ?>"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js?<?php echo time(); ?>"></script>
    <script src="dist/js/ocLazyLoad.js?<?php echo time(); ?>"></script>  
	<script src="rutas1.js?<?php echo time(); ?>"></script>
    <script src="js/upload.js?<?php echo time(); ?>"></script>
    <script src="dist/js/jquery.reveal.js?<?php echo time(); ?>"></script>
    <link rel="stylesheet" type="text/css" href="dist/css/reveal.css?<?php echo time(); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css?<?php echo time(); ?>">
    
    <link rel="stylesheet" href="bootstrap/bootstrapselect/dist/css/bootstrap-select.css?<?php echo time(); ?>">

     <link rel="stylesheet" href="dist/dropify/css/dropify.css?<?php echo time();?>"/>
  
<!--    <link rel="stylesheet" href="dist/css/bootstrap-select.css">
-->  <!-- Estilos de tablas GRID -->
  <link rel="stylesheet" href="dist/css/tablas.css?<?php echo time(); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/font-awesome-4.7.0/css/font-awesome.css?<?php echo time();?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="dist/ionicons-master/css/ionicons.css?<?php echo time(); ?>">
  <!-- Select2 -->
  <link rel="stylesheet" href="dist/select2-4.0.2/dist/css/select2.css?<?php echo time(); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css?<?php echo time(); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css?<?php echo time(); ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css?<?php echo time(); ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css?<?php echo time(); ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css?<?php echo time(); ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css?<?php echo time(); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css?<?php echo time(); ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css?<?php echo time(); ?>">
    <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css?<?php echo time(); ?>">
  <link rel="stylesheet" type="text/css" href="plugins/datatables/css/keyTable.bootstrap.css?<?php echo time(); ?>">


  <link rel="stylesheet" type="text/css" href="plugins/datatables/extensions/Responsive/css/dataTables.responsive.css?<?php echo time();?>">
  <!-- Theme style -->
<link rel="stylesheet" type="text/css" href="dist/css/estilos.css?<?php echo time(); ?>">
<link rel="stylesheet" href="dist/autocompletar/jquery.autoSuggest.css?<?php echo time();?>"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
 /*
 *  STYLE 14
 */

::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.6);
	background-color: #ffffff;
}

::-webkit-scrollbar
{
	width: 8px;
	background-color: #9eb66f;
  cursor: pointer;
}

::-webkit-scrollbar-thumb
{
	background-color: #FFF;
	background-image: -webkit-linear-gradient(90deg,
	                                          rgba(0, 0, 0, 1) 0%,
											  rgba(0, 0, 0, 1) 25%,
											  transparent 100%,
											  rgba(0, 0, 0, 1) 75%,
											  transparent)
}

  .symbol.required:before {
    content: "*";
    display: inline;
    color: #E6674A;
}
  </style>
</head>
<body class="hold-transition skin-black sidebar-mini" ng-app="myApp">
  <input type="hidden" id="opcanalisis2" value="No"/>
  <input type="hidden" id="opctiporecurso" value="1"/>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b> P</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Control</b> Presupuesto</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <a class="sidebar-toggle1" role="button">
        <img src="dist/img/LOGOGLOBAL.jpg" class="Img-rounded" width="160" height="50"  alt="User Image">
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu" style="display:none">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#" >
                      <div class="pull-left">
                        <img id="imgperfil" src="<?php echo $imgperfil."?".time();?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img id="imgperfil1" src="<?php echo $imgperfil."?".time();?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img id= src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu" style="display:none">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined 
					today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long 
					description here that may not fit into the page and may 
					cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales 
					made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your 
					username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
			<?PHP
			$consulta = mysqli_query($conectar,"select * from control_egreso where cpe_clave_int not in(SELECT c.cpe_clave_int FROM control_egreso c JOIN estados_contrato e ON UPPER(c.cpe_beneficiario) = UPPER(CONCAT_WS('-',e.esc_contratista,CAST(e.esc_contrato AS CHAR))) AND c.cpe_documento = e.esc_nit  GROUP BY cpe_clave_int) and pre_clave_int in(select pre_clave_int from usuario_presupuesto where  usu_clave_int = '".$idUsuario."')");
		$numero = mysqli_num_rows($consulta);
            $consulta2 = mysqli_query($conectar,"select count(cpe_clave_int) as cant,pre_clave_int as pre from control_egreso where cpe_clave_int not in(SELECT c.cpe_clave_int FROM control_egreso c JOIN estados_contrato e ON UPPER(c.cpe_beneficiario) = UPPER(CONCAT_WS('-',e.esc_contratista,CAST(e.esc_contrato AS CHAR))) AND c.cpe_documento = e.esc_nit GROUP BY cpe_clave_int) and pre_clave_int in(select pre_clave_int from usuario_presupuesto where  usu_clave_int = '".$idUsuario."') group by pre");
            $numero2 = mysqli_num_rows($consulta2);
			if($numero>0){ $pc1 = "danger";}else{$pc1 = "default";}
			
			if($perfil=="ADMINISTRADOR")
			{
			   $cona = mysqli_query($conectar,"select count(distinct a.act_clave_int) as cant from actividades a JOIN actividadinsumos AS i on a.act_clave_int = i.act_clave_int  where est_clave_int = '3'");
			   $data = mysqli_fetch_array($cona);
			   $pendi = $data['cant'];
			}
      else if($perfil=="COORDINADOR")
      {
        $cona = mysqli_query($conectar,"select count(distinct a.act_clave_int) as cant from actividades a JOIN actividadinsumos AS i on a.act_clave_int = i.act_clave_int  where est_clave_int = '3' and( a.act_usu_creacion = '".$idUsuario."' or a.act_usu_creacion in(SELECT uu.usu_clave_int FROM usuario uu WHERE uu.usu_coordinador = '".$idUsuario."'))");
         $data = mysqli_fetch_array($cona);
         $pendi = $data['cant'];
      }
			else
			{
				$pendi = 0;
			}
			if($pendi>0 and $sistema=="presupuesto")
			{
            ?>  
            <!--         
		   <li class="tasks-menu"><a onClick="CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')" role="button"  data-toggle="modal" data-target="#myModal2" title="Actividades por aprobar">
              <i class="fa fa-warning"></i>
              <span id="pendientesaprobar" class="label label-warning"><?php echo $pendi;?></span>
            </a></li>-->
            <?php
			}
			?>
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Facturas Sin Relacionar">
              <i class="fa fa-flag-o"></i>
              <span id="totfacturas" class="label label-<?php echo $pc1;?>"><?php echo $numero;?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" id="litotfacturas">Hay <?php echo $numero; ?> Facturas sin relacionar</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu" id="listfacturas">
                <?php
				if($numero2>0)
				{
					for($l=0;$l<$numero2;$l++)
					{
						$dat2 = mysqli_fetch_array($consulta2);
						$pre = $dat2['pre'];
						$cant = $dat2['cant'];
				//DATOS PRESUPUESTO
						$conp = mysqli_query($conectar,"SELECT	p.pre_nombre nomp,c2.per_correo AS Dest,concat(c2.per_nombre,' ',c2.per_apellido) as Cor,CONCAT(c.per_nombre,' ',c.per_apellido) as Cli,  ci.ciu_nombre as Ciu,p.pre_cliente as Cli2,p.pre_coordinador as Idc FROM	presupuesto p LEFT OUTER JOIN persona c ON c.per_clave_int = p.per_clave_int LEFT OUTER JOIN persona c2 ON c2.per_clave_int = p.pre_coordinador JOIN ciudad ci ON ci.ciu_clave_int = p.ciu_clave_int WHERE pre_clave_int = '".$pre."'");
						$datp = mysqli_fetch_array($conp);
						$nomp = $datp['nomp'];
						$dest = $datp['Dest'];
						$cor = $datp['Cor'];
						$cli = $datp['Cli'];
						$cli2 = $datp['Cli2']; if($cli=="" || $cli==NULL || $cli=="null" || $cli==0){$cli=$cli2;}
						$ciudad = $datp['Ciu'];
						$idc = $datp['Idc'];
						$percent = ($cant*100)/$numero;
						if($percent<=25){ $pc = "green";}else if($percent<=50){$pc="aqua";}else 
						if($percent<=75){$pc="yellow";}else if($percent<=100){$pc = "red";}
						$convp = mysqli_query($conectar,"select pre_clave_int from usuario_presupuesto where pre_clave_int = '".$pre."' and usu_clave_int = '".$idUsuario."'");
						$numvp = mysqli_num_rows($convp);
						if($numvp>0)
						{
						//progress-bar-green red yellow( 1 % 5 green, 26 a 50 aqua, 51 as 75 yellow, 76 as 100 red)
					?>
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        <?php echo $nomp;?>
                        <small class="pull-right"><?php echo $cant;?></small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-<?php echo $pc; ?>" style="width: <?Php echo $percent;?>%" role="progressbar" aria-valuenow="<?php echo $percent;?>" aria-valuemin="0" aria-valuemax="100>">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <?php
					}
			}
		}
				?>
                 
                </ul>
              </li>
              <li class="footer" style="display:none">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img id="imgperfil2" src="<?php echo $imgperfil."?".time();?>" class="user-image" alt="User Image">
              <span class="hidden-xs" id="nomperfil2"><?php echo $nombreusuario; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img id="imgperfil3" src="<?php echo $imgperfil."?".time();?>" class="img-circle" alt="User Image">

                <p id="nomperfil3">
                  <?php echo substr($nombreusuario,0,23); ?>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body" style="display:none">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a ui-sref="Ajustes({Op:'Cue'})" ui-sref-opts="{reload: true}" class="btn btn-default btn-flat">Mi Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="data/logout.php" class="btn btn-default btn-flat">
					Cerrar sesion</a>
                </div>
                
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li style="display:none">
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">        
        <?php
		
    $con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 14");
    $dato = mysqli_fetch_array($con);
    $metdas = $dato['per_metodo'];
		if($metdas <> '' and ($metdas == 0 || $metdas == 1))
		{
		?>
        <li class="active treeview" style="left: 0px; top: 0px">
          <a ui-sref="Dashboard" ui-sref-opts="{reload: true}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <?php
        }
        $con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 1");
		$dato = mysqli_fetch_array($con);
		$metpre = $dato['per_metodo'];
		
		if($metpre <> '' and ($metpre== 0 || $metpre == 1) and $sistema=="presupuesto")
		{
		?>
        <li class="treeview">
          <a style="cursor:pointer; left: 0px; top: 0px;" ui-sref="Presupuesto" ui-sref-opts="{reload: true}">
            <i class="fa fa-dollar"></i>
            <span>Presupuesto</span>
          </a>
        </li>
        
         <?php
        }
    if($methis <> '' and ($methis== 0 || $methis == 1) and $sistema=="presupuesto")
    {
      ?>
      <li class="treeview">
          <a style="cursor:pointer; left: 0px; top: 0px;" ui-sref="Historico" ui-sref-opts="{reload: true}">
            <i class="fa fa-dollar"></i>
            <span>Historico Presupuesto</span>
          </a>
        </li>
      <?php
    }
		
		
		if($sistema=="control")
		{			
				
				if($metini <> '' and ($metini == 0 || $metini == 1) )
				{
        ?>
        <li class="treeview">
        <a style="cursor:pointer; left: 0px; top: 0px;" ui-sref="Presupuestoinicial" ui-sref-opts="{reload: true}"><i class="fa fa-calculator"></i><span>Control Presupuesto</span>
        </a>
        </li>
                
        <?PHP
				}		

        if($methiscon <> '' and ($methiscon == 0 || $methiscon == 1) )
        {
        ?>
        <li class="treeview">
          <a style="cursor:pointer; left: 0px; top: 0px;" ui-sref="HistoricoControl" ui-sref-opts="{reload: true}">
          <i class="fa fa-dollar"></i>
          <span>Historico Presupuesto</span>
          </a>
        </li>
        <?php
        }

		}
    /*
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 3");
		$dato = mysqli_fetch_array($con);
		$metconf = $dato['per_metodo'];
		
    $con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 4");
		$dato = mysqli_fetch_array($con);
		$metdiv = $dato['per_metodo'];
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 5");
		$dato = mysqli_fetch_array($con);
		$metcli = $dato['per_metodo'];
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 6");
		$dato = mysqli_fetch_array($con);
		$metuni = $dato['per_metodo'];
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 7");
		$dato = mysqli_fetch_array($con);
		$mettippro = $dato['per_metodo'];
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 8");
		$dato = mysqli_fetch_array($con);
		$metestpro = $dato['per_metodo'];
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 9");
		$dato = mysqli_fetch_array($con);
		$mettipcon = $dato['per_metodo'];
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 10");
		$dato = mysqli_fetch_array($con);
		$mettipins = $dato['per_metodo'];
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 11");
		$dato = mysqli_fetch_array($con);
		$metper = $dato['per_metodo'];		
		
		
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 12");
		$dato = mysqli_fetch_array($con);
		$metcar = $dato['per_metodo'];	
			
		$con = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 13");
		$dato = mysqli_fetch_array($con);
		$metusu = $dato['per_metodo'];*/
		
		if($metconf <> '' and ($metconf == 0 || $metconf == 1) and $sistema=="presupuesto")
		{
        ?>
        <li class="treeview">
          <a style="cursor:pointer">
            <i class="fa fa-wrench"></i> <span>Maestras</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
      			<li><a ui-sref="Grupos" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Grupos</a></li>
      			<li><a ui-sref="Capitulos" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Capitulos</a></li>

      			<li  class="treeview"><a style="cursor:pointer"><i class="fa fa-circle-o"></i>Actividades<i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                    <li><a ui-sref="Actividades({Opc:'No'})" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o" ></i>APU</a></li>
                    <li><a ui-sref="Actividades({Opc:'Si'})" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>SubAnalisis</a></li>
                    <?php
                if($pendi>0 and $sistema=="presupuesto")
                {
                    ?>
                    <li><a ui-sref="ActividadesPorAprobar({Opc:'Por'})" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Por Aprobar
                            <span id="pendientesaprobar" class="label label-warning"><?php echo $pendi;?></span>
                        </a></li>
                    <?php
                }

                ?>
                </ul>
            </li>

      			<li><a ui-sref="Insumos" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Recursos</a></li>
                  <li class="treeview">
                  <a style="cursor:pointer">
                  <i class="fa fa-wrench"></i> <span>Otros</span>
                  <i class="fa fa-angle-left pull-right"></i>
                  </a>
                      <ul class="treeview-menu">
                      <?php if($metdiv <> '' and ($metdiv == 0 || $metdiv == 1)){ ?><li><a ui-sref="Division" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Localización</a></li><?php } ?>
                      
                      <?php if($metcli <> '' and ($metcli == 0 || $metcli == 1)){ ?><li><a ui-sref="Clientes" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Cliente</a></li>		<?php } ?>   
                      <?php if($metuni <> '' and ($metuni == 0 || $metuni == 1)){ ?><li><a ui-sref="Unidades" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Unidades de Medida</a></li><?php } ?>
                      <?php if($mettippro <> '' and ($mettippro == 0 || $mettippro == 1)){ ?><li><a ui-sref="Tipoproyecto" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Tipo de Proyecto</a></li><?php } ?>
                      <?php if($metestpro <> '' and ($metestpro == 0 || $metestpro == 1)  and $perfil=="ADMINISTRADOR"){ ?><li style="display:none"><a ui-sref="Estadoproyecto" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Estados de Proyecto</a></li><?php } ?>
                      <?php if($mettipcon <> '' and ($mettipcon == 0 || $mettipcon == 1)){ ?><li><a ui-sref="Tipocontrato" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Tipo de Contrato</a></li><?php } ?>
                      <?php if($mettipins <> '' and ($mettipins == 0 || $mettipins == 1) and $perfil=="ADMINISTRADOR"){ ?><li><a ui-sref="Tipoinsumo" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Tipos de Insumo</a></li><?php } ?>
                      <?php if($metper <> '' and ($metper == 0 || $metper == 1) and $perfil=="ADMINISTRADOR"){ ?><li><a ui-sref="Perfiles" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Perfiles</a></li><?php } ?>
                      <?php if($metcar <> '' and ($metcar == 0 || $metcar == 1)and ($perfil=="ADMINISTRADOR")){ ?><li class="hide"><a ui-sref="Cargos" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Cargos</a></li>		<?php } ?> 
                      <?php if($metusu <> '' and ($metusu == 0 || $metusu == 1)){ ?><li><a ui-sref="Usuarios" ui-sref-opts="{reload: true}"><i class="fa fa-circle-o"></i>Usuarios</a></li><?php } ?>
                      </ul>
                  </li>
              </ul>
        </li>
        <?php
        }
          $veri = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 1");
          $dato = mysqli_fetch_array($veri);
          $metpre = $dato['per_metodo'];
          $veri = mysqli_query($conectar,"select * from permiso where usu_clave_int = '".$idUsuario."' and ven_clave_int = 2");
          $dato = mysqli_fetch_array($veri);
          $metcont = $dato['per_metodo'];
        if($sistema=="presupuesto" && $metcont <> '' and ($metcont== 0 || $metcont == 1))
        {
          ?>
           <li class="treeview">
            <a style="cursor:pointer; left: 0px; top: 0px;" onclick="cambiosistema('IRACONTROL')" class="text-red"><i class="fa fa-send"></i><span>Ir a Control Presupuesto</span>
             </a>
          </li>
          <?php
        }
        if($sistema=="control" && $metpre <> '' and ($metpre== 0 || $metpre == 1))
        {
          ?>
           <li class="treeview">
            <a style="cursor:pointer; left: 0px; top: 0px;" onclick="cambiosistema('IRAPRESUPUESTO')" class="text-red" ><i class="fa fa-send"></i><span>Ir a Presupuesto</span>
             </a>
          </li>
          <?php
        }
        if($idUsuario>0)
        {
          ?>
          <li class="treeview">
            <a class="text-warning" href="data/logout.php" >
              <i class="fa fa-lock"></i>
           <span>Cerrar Sesión</span></a>
          </li>
          <?php
        }
        ?>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content" ui-view>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
   
    <strong><a  id="prueba" data-pk="1" data-type="text" title="Enter username">PAVAS S.A.S.</a><br>Copyright © <?php echo date('Y'); ?> 
	  Todos los derechos reservados</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Modal title</h4>
      </div>
      <div class="modal-body">
      <div class="row" id="filtroporabrobar">
      	<div class="col-md-2">
        <label>Tipo:</label>
        <select id="opcanalisis" class="form-control input-sm" onChange="CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')">
        <option value="No">Actividades</option>
        <option value="Si">Sub Analisis</option>
        </select></div>
        <div class="col-md-3">
      <label>Coordinador:</label>
       <select name="buscoordinador" id="buscoordinador" class="form-control input-sm selectpicker" onChange="CRUDACTIVIDADES('CARGARPROYECTOS','')" data-live-search="true" data-live-search-placeholder="Buscar Coordinador" data-actions-box="true">
          <option value="">--seleccione--</option>
          <?php
		  $con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
		  while($dat = mysqli_fetch_array($con))
		  {
			  $idp = $dat['usu_clave_int'];	
			  $nom = $dat['usu_nombre'];
			  $ema = $dat['usu_email'];
		     ?>
             <option value="<?php echo $idp;?>"><?Php echo $nom." - ".$ema;?></option>
             <?php
	
		  }
		  ?>
          </select>
      </div>
        <div class="col-md-3">
      <label>Proyecto:</label>
       <select name="busproyecto" id="busproyecto" class="form-control input-sm selectpicker" onChange="CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')" data-live-search="true" data-live-search-placeholder="Buscar Proyecto" data-actions-box="true">
          <option value="">--seleccione--</option>
          <?php
		  $con = mysqli_query($conectar,"select p.pre_clave_int,pre_nombre,tpp_nombre,ciu_nombre from presupuesto p join actividades a on a.pre_clave_int = p.pre_clave_int join tipoproyecto t on t.tpp_clave_int = p.tpp_clave_int join ciudad c on p.ciu_clave_int = c.ciu_clave_int where a.est_clave_int = 3 GROUP BY p.pre_clave_int ORDER BY p.pre_nombre");
		  while($dat = mysqli_fetch_array($con))
		  {
			  $idpr = $dat['pre_clave_int'];	
			  $nom = $dat['pre_nombre']." - ".$dat['tpp_nombre']." - ".$dat['ciu_nombre'];
		
		     ?>
             <option value="<?php echo $idpr;?>"><?Php echo $nom;?></option>
             <?php
	
		  }
		  ?>
          </select>
      </div>
     
       
      <div class="col-md-3">
      <label>Actividad:</label>
      <input type="text" id="busactividad"  onChange="CRUDACTIVIDADES('ACTIVIDADESPORAPROBAR','')" value="" class="form-control input-sm"/>
      </div>
      </div>
        <div id="contenido3"></div>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>       
      </div>
    </div>
  </div>
</div>
<div class="modal-overlay"></div>
<script>
  $('.modal-overlay').on('click',function(){
    $(this).hide();
    //$('.modal-overlay').hide();
    $('.modal-text').hide().html('');
    $('.modal-text2').hide().html('');
    $('.modal-text3').hide().html('');
    $('.modal-input').removeClass('modal-input');
  })
</script>

<!-- ./wrapper -->
<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js?<?php echo time(); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="dist/cdn/jquery-ui-11-4.min.js?<?php echo time(); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js?<?php echo time(); ?>"></script>
<script src="bootstrap/bootstrapselect/dist/js/bootstrap-select.js?<?php echo time(); ?>"></script>

<!--<script src="dist/js/bootstrap-select.js"></script>
--><!-- Select2 -->
<script src="dist/select2-4.0.2/dist/js/select2.full.js?<?php echo time(); ?>"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js?<?php echo time(); ?>"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js?<?php echo time(); ?>"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js?<?php echo time(); ?>"></script>
<!-- DataTables -->
<script src="plugins/datatables/js/jquery.dataTables.js?<?php echo time(); ?>"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js?<?php echo time(); ?>"></script>

<script src="plugins/datatables/js/dataTables.keyTable.js?<?php echo time(); ?>"></script>
<script src="plugins/datatables/js/dataTables.rowReorder.js?<?php echo time(); ?>"></script>
<link href="plugins/datatables/css/rowReorder.dataTables.css?<?php echo time(); ?>" rel="stylesheet"/>
<script src="plugins/datatables/js/dataTables.fixedHeader.js?<?php echo time(); ?>"></script>
<link href="plugins/datatables/css/fixedHeader.dataTables.css?<?php echo time(); ?>" rel="stylesheet"/>
<script src="plugins/datatables/dataTables.select.js?<?php echo time(); ?>"></script>
<script src="plugins/datatables/dataTables.buttons.js?<?php echo time(); ?>"></script>
<script src="plugins/datatables/js/dataTables.pageResize.min.js?<?php echo time(); ?>"/>
<script type="text/javascript" language="javascript" src="plugins/datatables/extensions/Responsive/js/dataTables.responsive.js?<?php echo time();?>"></script>


<!-- Morris.js charts -->
<script src="dist/cdn/raphael-min.js?<?php echo time(); ?>"></script>
<script src="plugins/morris/morris.min.js?<?php echo time(); ?>"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js?<?php echo time(); ?>"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js?<?php echo time(); ?>"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js?<?php echo time(); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js?<?php echo time(); ?>"></script>
<!-- daterangepicker -->
<script src="dist/cdn/moment.min.js?<?php echo time(); ?>"></script>
<script src="plugins/daterangepicker/daterangepicker.js?<?php echo time(); ?>"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js?<?php echo time(); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js?<?php echo time(); ?>"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js?<?php echo time(); ?>"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js?<?php echo time(); ?>"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js?<?php echo time(); ?>"></script>

<!-- AdminLTE App -->
<script src="dist/js/app.js?<?php echo time(); ?>"></script>
<!-- AdminLTE dashboard demo ***COMENTARIADO POR PROBLEMAS CON MULTISELECT*** (This is only for demo purposes) <script src="dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js?<?php echo time(); ?>"></script>
<script type="text/javascript" src="llamadas3.js?<?php echo time(); ?>"></script>

<link href="dist/css/jquery.alerts.css?<?php echo time(); ?>" rel="StyleSheet" type="text/css" />
<script src="dist/js/jquery.ui.draggable.js?<?php echo time(); ?>" type="text/javascript"></script>
<script src="dist/js/jquery.alerts.mod.js?<?php echo time(); ?>" type="text/javascript"></script>

<script type="text/javascript" src="dist/alertify.js/lib/alertify.js?<?php echo time(); ?>"></script>
<link rel="stylesheet" href="dist/alertify.js/themes/alertify.core.css?<?php echo time(); ?>" />
<link rel="stylesheet" href="dist/alertify.js/themes/alertify.default.css?<?php echo time(); ?>" />

<script src="dist/js/jquery.formatCurrency-1.4.0/jquery.formatCurrency-1.4.0.min.js?<?php echo time(); ?>"></script>
<script src="dist/js/jquery.formatCurrency-1.4.0/i18n/jquery.formatCurrency.all.js?<?php echo time(); ?>"></script>

<script src="dist/js/jquery.contextMenu.js?<?php echo time(); ?>"></script>
<script src="dist/js/jquery.ui.position.js?<?php echo time(); ?>"></script>
<link href="dist/js/jquery.contextMenu.css?<?php echo time(); ?>" rel="stylesheet" type="text/css"/>

<!--PACE-->
<link rel="stylesheet" href="plugins/pace/pace.css?<?php echo time();?>"/>
<script src="plugins/pace/pace.js?<?php echo time();?>"></script>

<link href="dist/tooltipster-master/css/tooltipster.css?<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-light.css?<?php echo time(); ?>" />
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-noir.css?<?php echo time(); ?>" />
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-punk.css?<?php echo time(); ?>" />
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-shadow.css?<?php echo time(); ?>" />
<script src="dist/tooltipster-master/js/jquery.tooltipster.js?<?php echo time(); ?>"></script>

<script src="dist/dropify/js/dropify.js?<?php echo time();?>"></script>
<script src="dist/autocompletar/jquery.autoSuggest.js?<?php echo time();?>"></script>

<script src="dist/js/socket/fancywebsocket.js?<?php echo time();?>"></script>
<!--
<link href="dist/tooltister-v4/css/tooltipster.bundle.css?<?php echo time?>" rel="stylesheet" type="text/css"/>
<link href="dist/tooltister-v4/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css?<?php echo time();?>" rel="stylesheet" type="text/css"/>
<link href="dist/tooltister-v4/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css?<?php echo time();?>" rel="stylesheet" type="text/css"/>
<link href="dist/tooltister-v4/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-noir.min.css?<?php echo time();?>" rel="stylesheet" type="text/css"/>
<link href="dist/tooltister-v4/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css?<?php echo time();?>" rel="stylesheet" type="text/css"/>
<link href="dist/tooltister-v4/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css?<?php echo time();?>" rel="stylesheet" type="text/css"/>

<script src="dist/tooltister-v4/js/tooltipster.bundle.min.js" type="text/javascript"></script>
-->
<!--
  <script type="text/javascript" src="dist2/components/popup.js"></script>
  <script type="text/javascript" src="dist2/components/dropdown.js"></script>
  <script type="text/javascript" src="dist2/components/transition.js"></script>
-->

<!-- Archivos para seleccionar multiples elementos de una lista 
<script type="text/javascript" src="dist/js/prettify.js"></script>
<link rel="stylesheet" href="dist/css/bootstrap-multiselect.css" />
<script src="dist/js/bootstrap-multiselect.js"></script>-->
<style>
  .form-control{
    /*font-size: <?php echo $fontsize;?>px !important;*/
  }
</style>
<script>
$(document).ready(function(e)
{
	$('.currency').on('blur keyup',function()
	{
		$('.currency').formatCurrency({colorize: true})
	});
	
$('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    })
   var fontsize = localStorage.getItem('fontSize');
   //if(fontsize=="")
   //{
      localStorage.setItem('fontSize','<?php echo $fontsize?>');
   //}
});
 
</script>

</body>
</html>