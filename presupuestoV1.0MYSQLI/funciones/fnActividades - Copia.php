
<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
  $opcion = $_POST['opcion'];
  if($opcion=="NUEVO")
  {
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group">    
    <div class="col-xs-12"><strong>Descripción:<span class="symbol required"></span></strong>
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control" placeholder="Escribe aqui la descripcion">
    </div>
    </div>
       <div class="form-group">
         <div class="col-xs-6"><strong>Tipo de Proyecto:<span class="symbol required"></span></strong>
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where tpp_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
         </div> 
        <div class="col-xs-6"><strong>Unidad de medida:<span class="symbol required"></span></strong>
         <select name="selunidad" id="selunidad" class="form-control">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where uni_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
         </select>
         </div> 
    </div>    
    <div class="form-group">
        <div class="col-xs-6"><strong>SubAnalisis:</strong><br>
        <label for="opcion3"> <input type="radio" name="radanalisis" id="opcion3" class="flat-red"  value="1">Si</label>
        <label for="opcion4"> <input type="radio" name="radanalisis" id="opcion4" class="flat-red" value="0" checked>No</label>        
        </div>
        <div class="col-xs-6"><strong>Estado:</strong><br>        
        <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" class="flat-red" checked value="1">Activo</label>        
        <label> <input type="radio" name="radestado" id="opcion2" class="flat-red" value="0">Inactivo</label>
        </div>
    </div>
    
    <div class="form-group">
         <div class="col-xs-6"><strong>Pais:<span class="symbol required"></span></strong>
          <select name="selpais" id="selpais" class="form-control" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where pai_sw_activo = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
         </div>
         <div class="col-xs-6"><strong>Departamento:<span class="symbol required"></span></strong>
           <select name="seldepartamento" id="seldepartamento" class="form-control" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
         </select>
         </div>
    </div>
     <div class="form-group">
         <div class="col-xs-6"><strong>Ciudad:<span class="symbol required"></span></strong>
             <select name="selciudad" id="selciudad" class="form-control">
         <option value="">--seleccione--</option>
         </select>
         </div>
    </div>
    <?php $var ="'PENDIENTES'"; echo '<style onload="CRUDACTIVIDADES('.$var.')"></style>'?>
    <div class="form-group">
    	<div class="col-xs-12">
           <div id="pendientes"></div>
        </div>
    </div>
  </form>
  <?PHP  
  }
else
  if($opcion=="EDITAR" )
  {
	  $ida = $_POST['id'];
	   $coninfo = mysqli_query($conectar,"select act_nombre as des,tpp_clave_int as tpp,uni_clave_int uni,act_analisis as ana,act_sw_activo as est,c.ciu_clave_int ciu,d.dep_clave_int dep,p.pai_clave_int pai from actividades a join ciudad c on c.ciu_clave_int = a.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where act_clave_int = '".$ida."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $tpp = $datinfo['tpp'];
	  $uni = $datinfo['uni'];
	  $ana = $datinfo['ana'];
	  $des = $datinfo['des'];
	  $est = $datinfo['est'];
	  $ciu = $datinfo['ciu'];
	  $pai = $datinfo['pai'];
	  $dep = $datinfo['dep'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $ida;?>">
<div class="form-group">    
    <div class="col-xs-12"><strong>Descripción:<span class="symbol required"></span></strong>
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control" value="<?php echo $des;?>" placeholder="Escribe aqui la descripcion">
    </div>
    </div>
       <div class="form-group">
         <div class="col-xs-6"><strong>Tipo de Proyecto:<span class="symbol required"></span></strong>
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where tpp_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $idp = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option <?php if($tpp==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
         </div> 
        <div class="col-xs-6"><strong>Unidad de medida:<span class="symbol required"></span></strong>
         <select name="selunidad" id="selunidad" class="form-control">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where uni_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option <?php if($uni==$idu){echo 'selected';}?> value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
         </select>
         </div> 
    </div>    
    <div class="form-group">
        <div class="col-xs-6"><strong>SubAnalisis:</strong>
        <label for="opcion3"> <input type="radio" name="radanalisis" id="opcion3" class="flat-red"  value="1" <?php if($ana==1 || $ana==""){echo 'checked';}?>>Si</label>
        <label for="opcion4"> <input type="radio" name="radanalisis" id="opcion4" class="flat-red" value="0" <?php if($ana==0){echo 'checked';}?>>No</label>        
        </div>
        <div class="col-xs-6"><strong>Estado:</strong><br>        
       <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" class="flat-red" checked value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
    Activo</label>
    <label for="opcion2"> <input type="radio" name="radestado" id="opcion2" class="flat-red" value="0" <?php if($est==0){echo 'checked';}?>>
    Inactivo
    </label>
        </div>
    </div>
  <div class="form-group">
         <div class="col-xs-6"><strong>Pais:<span class="symbol required"></span></strong>
          <select name="selpais" id="selpais" class="form-control" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where pai_sw_activo = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
         </div>
         <div class="col-xs-6"><strong>Departamento:<span class="symbol required"></span></strong>
           <select name="seldepartamento" id="seldepartamento" class="form-control" onChange="cargarciudad('seldepartamento','selciudad,'selpais')">
         <option value="">--seleccione--</option>
			<?php
            $con = mysqli_query($conectar,"select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and dep_sw_activo = 1");
            while($dat = mysqli_fetch_array($con))
            {
            ?>
                <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
            <?php
            }
            ?>
         </select>
         </div>
    </div>
    
    <div class="form-group">
         <div class="col-xs-6"><strong>Ciudad:<span class="symbol required"></span></strong>
             <select name="selciudad" id="selciudad" class="form-control">
         <option value="">--seleccione--</option>
       <?php
	     $con = mysqli_query($conectar,"select ciu_clave_int,ciu_nombre from ciudad where dep_clave_int ='".$dep."' and ciu_sw_activo = 1");
	 while($dat = mysqli_fetch_array($con))
	 {
     ?>
     <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
     <?php
		}
	 ?>
         </select>
         </div>
     </div>
      <?php  $var ="'AGREGADOS'"; echo '<style onload="CRUDACTIVIDADES('.$var.','.$ida.')"></style>'?>
    <div class="form-group">
    	<div class="col-xs-12">
           <div id="pendientes"></div>
        </div>
    </div>
  </form>
  <?PHP  
  }
   else if($opcion=="GUARDAR")
  {
     $tipoproyecto = $_POST['tipoproyecto'];
	 $unidad = $_POST['unidad'];
	 $ciudad = $_POST['ciudad'];
	 $descripcion = $_POST['descripcion'];
	 $estado  = $_POST['estado'];
	 $analisis  = $_POST['analisis'];
	 
	 $veri = mysqli_query($conectar,"select * from actividades where UPPER(act_nombre) = UPPER('".$descripcion."')");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $ins = mysqli_query($conectar,"insert into actividades(act_nombre,tpp_clave_int,uni_clave_int,act_analisis,act_sw_activo,ciu_clave_int,act_usu_actualiz,act_fec_actualiz) values('".$descripcion."','".$tipoproyecto."','".$unidad."','".$analisis."','".$estado."','".$ciudad."','".$usuario."','".$fecha."')");
		if($ins>0)
		{
		   $ida = mysqli_insert_id($conectar);
		   $con = mysqli_query($conectar,"select * from insumospendientes");
		   $num = mysqli_num_rows($con);
		   if($num>0 and $ida>0)
		   {
$inse = mysqli_query($conectar,"insert into actividadinsumos select '','".$ida."',ins_clave_int,inp_rendimiento,inp_valor,'".$usuario."','".$fecha."' from insumospendientes");
			   if($inse>0)
			   {
			      $vaciar = mysqli_query($conectar,"TRUNCATE TABLE insumospendientes");
			   }
		   }
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
 
   else if($opcion=="GUARDAREDICION" )
  {
	 $ide =$_POST['id'];
    $tipoproyecto = $_POST['tipoproyecto'];
	 $unidad = $_POST['unidad'];
	 $ciudad = $_POST['ciudad'];
	 $descripcion = $_POST['descripcion'];
	 $estado  = $_POST['estado'];
	 $analisis  = $_POST['analisis'];
	 
	 $veri = mysqli_query($conectar,"select * from actividades where UPPER(act_nombre) = UPPER('".$descripcion."') and act_clave_int!='".$ide."'");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update actividades set act_nombre='".$descripcion."',tpp_clave_int='".$tipoproyecto."',uni_clave_int='".$unidad."',act_analisis='".$analisis."',act_sw_activo='".$estado."',ciu_clave_int='".$ciudad."',act_usu_actualiz='".$usuario."',act_fec_actualiz='".$fecha."' where act_clave_int = '".$ide."'");
		if($upd>0)
		{
		  
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
    else if($opcion=="CARGARLISTAACTIVIDADES")
  {
	  ?> <script src="js/jsactividades.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div class="table-responsive" id="no-more-tables">
      <table id="tbactividades" class="col-md-12 table table-bordered table-hover">
                <thead>
                <tr>
                <th></th>
                <th></th>
                
                <th>Codigo</th>               
                <th>Descripción</th>
                <th>Tipo Proyecto</th>
                <th>Unidad</th>
                <th>Ubicacion</th>
                <th>Valor APU($)</th>
                <th>Analisis</th>
                <th>Estado</th>
                
                </tr>
                </thead>
               <?PHP
			$con = mysqli_query($conectar,"select act_clave_int id,act_nombre as nom,t.tpp_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,act_analisis as ana, act_sw_activo as est,p.pai_nombre as pai,d.dep_nombre as depa,c.ciu_nombre as ciu from actividades i join tipoproyecto t on t.tpp_clave_int = i.tpp_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int join ciudad c on c.ciu_clave_int = i.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int order by tip,nom,nomu")
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['id'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $valor = 0;//suma de insumos de todas las actividades 
				   $ubi = $dat['pai']."/".$dat['depa']."/".$dat['ciu'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ana = $dat['ana'];
				   
				   $cons = mysqli_query($conectar,"select i.ins_valor as val,d.ati_valor as val1,d.ati_rendimiento as ren from insumos i join actividadinsumos d on d.ins_clave_int = i.ins_clave_int where d.act_clave_int = '".$idc."'");
				   $sumvalor = 0;
				   while($dats = mysqli_fetch_array($cons))
				   {
					   $ren = $dats['ren'];
					   $sv = $dats['val']*$ren;
					   $sv1 = $dats['val1']*$ren;
					   if($sv1<=0){$sumvalor+=$sv;}else{$sumvalor+=$sv1;}
				   }
				   if($ana=="0"){$ana='<span class="label label-warning pull-right">No</span>';}
				   else {$ana='<span class="label label-success pull-right">Si</span>';}
				    if($est=="0"){$est='<span class="label label-warning pull-center">Inactivo</span>';}
				   else {$est='<span class="label label-success pull-center">Activo</span>';}
				   
				   $var = "'EDITAR',".$idc;
				   $edi = '<a class="btn btn-block btn-warning btn-xs" onClick="CRUDACTIVIDADES('.$var.')" data-toggle = "modal" data-target="#myModal"><i class="glyphicon glyphicon-pencil"></i></a>';
				   
				    
				$apu = "<div id='divapu".$idc."'>".number_format($sumvalor,2,'.',',')."</div>";	
				   ?>
                   
                <td  class="details-control"></td>
               <td><?php echo $edi?></td>
                  <td data-title="Codigo"><?Php echo $idc;?></td>
                  <td data-title="Descripcion"><?Php echo $nom;?></td>
                  <td data-title="Tipo Proyecto"><?Php echo $tipo;?></td>
                  <td data-title="Unidad"><?php echo $uni."(".$cod.")";?></td>
                  <td data-title="Ubicacion"><?php echo $ubi;?></td>
                  <td data-title="Valor APU($)"><div id="divapu<?php echo $idc;?>"><?php echo number_format($sumvalor,2,'.',',');?></div></td>
                  <td data-title="Analisis"><?Php echo $ana;?></td> 
                  <td data-title="Estado"><?Php echo $est;?></td>
                 
                 
                </tr>
                <?php
			   }
			   ?>
                </tbody>
                 <tfoot>
                <tr>
                <th></th>
                <th></th>
                <th>Codigo</th>
                <th>Descripción</th>
                <th>Tipo Proyecto</th>
                <th>Unidad</th>
                <th>Ubicacion</th>
                <th>Valor APU($)</th>
                <th>Analisis</th>
                <th>Estado</th>
               
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP

  }
  else if($opcion=="AGREGARPENDIENTE")
  {
      $insumo = $_POST['insumo'];
	  $rendimiento = $_POST['rendimiento'];
	  $valor = $_POST['valor'];
	  $ins = mysqli_query($conectar,"insert into insumospendientes(ins_clave_int,inp_rendimiento,inp_valor) values ('".$insumo."','".$rendimiento."','".$valor."')");
	  if($ins>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
 
  else if($opcion=="DELETEPENDIENTE")
  {
      $ide = $_POST['id'];
	  $dele = mysqli_query($conectar,"delete from insumospendientes where inp_clave_int = '".$ide."'");
	  if($dele>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  } 
   else if($opcion=="AGREGAR")
  {
	  $ida = $_POST['id'];
      $insumo = $_POST['insumo'];
	  $rendimiento = $_POST['rendimiento'];
	  $valor = $_POST['valor'];
	  $ins = mysqli_query($conectar,"insert into actividadinsumos(act_clave_int,ins_clave_int,ati_rendimiento,ati_valor) values ('".$ida."','".$insumo."','".$rendimiento."','".$valor."')");
	  if($ins>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
   else if($opcion=="DELETE")
  {
	  $ide= $_POST['id'];
	  $ins = mysqli_query($conectar,"delete from actividadinsumos where ati_clave_int = '".$ide."'");
	  if($ins>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
  else
   if($opcion=="PENDIENTES")
  {
	  ?>
      <script src="js/jsinsumospendientes.js"></script>
     <div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
        <div class="form-group">
        	<div class="col-xs-12">
        		<div class="input-group"><span class="input-group-addon">Insumos:</span> 
            	<select name="selagregar" id="selagregar" class="form-control">
            	<option value="">--seleccione--</option>
            <?php
			   $con = mysqli_query($conectar,"select ins_clave_int, ins_nombre from insumos where ins_sw_activo = 1 and ins_clave_int not in(select ins_clave_int from insumospendientes)");
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idi = $dat['ins_clave_int'];
				   $nomi = $dat['ins_nombre'];
			   ?>
               <option value="<?Php echo $idi?>"><?php echo $nomi;?></option>
               <?php
			   }
			?>
           		 </select>
           
            	</div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-5">
            	<div class="input-group"><span class="input-group-addon">Rendimiento:</span> 
            <input type="number" name="rendimiento" id="rendimiento" min="0"  onKeyPress="return NumCheck(event, this)" value="0" class="form-control"/></div>
            </div>
            <div class="col-xs-7">
            	<div class="input-group"><span class="input-group-addon">Valor Unitario:</span> 
            <input type="number" name="valorunitario" id="valorunitario" min="0"  onKeyPress="return NumCheck(event, this)" value="0" class="form-control"/> <span class="input-group-addon"><a onClick="CRUDACTIVIDADES('AGREGARPENDIENTE','')" role="button" class="btn btn-block btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></a></span>
            	</div>
            </div>
        </div>
            </h4>
             </div>
  <div class="panel-body">
       <div class="table-responsive" id="no-more-tables">
      <table id="tbinsumospendientes" class="col-md-12 table table-bordered table-hover">
                <thead>
                <tr>
                <th></th>
                
                <th>Tipo</th>
                <th>Nombre</th>
                <th>Unidad</th>
                <th>Rendimiento</th>
                <th>Valor</th>
                <th>Descripcion</th>
                </tr>
                </thead>
               <?PHP
		$con = mysqli_query($conectar,"select d.inp_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,i.ins_valor as val,d.inp_valor val1,i.ins_descripcion des,d.inp_rendimiento as ren from  insumos i join insumospendientes d on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int   order by tip,nom,nomu")
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];//origina
				   $val1 = $dat['val1'];//detalle
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				    
				   ?>
                <tr>
               <td><a role="button" class="btn btn-block btn-danger btn-xs" onClick="CRUDACTIVIDADES('DELETEPENDIENTE',<?PHP echo $idc;?>)"><i class="glyphicon glyphicon-trash"></i></a></td>
                 
                  <td data-title="Tipo"><?Php echo $tipo;?></td>
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title="Unidad"><?php echo $uni."(".$cod.")";?></td>
                  <td data-title="Rendimiento"><?php echo $ren;?></td>
                  <td data-title="Valor Unitario">$ <?php echo number_format($valor,2,'.',',');?></td>
                  <td data-title="Descripcion"><?php echo $des;?></td>
                 
                </tr>
                <?php
			   }
			   ?>
                </tbody>
             <tfoot>
                <tr>
                <th></th>
                
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                </tr>
                </tfoot>
                </table>
                </div>
                  </div>
</div>
           
      <?PHP
  }else
   if($opcion=="AGREGADOS")
  {
	  $id = $_POST['id'];
	  ?>
      <script src="js/jsinsumospendientes.js"></script>
   <div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"> 
    	<div class="form-group">
            <div class="col-xs-12">
            	<div class="input-group"><span class="input-group-addon">Insumos:</span> 
            	<select name="selagregar" id="selagregar" class="form-control">
            	<option value="">--seleccione--</option>
            <?php
			   $con = mysqli_query($conectar,"select ins_clave_int, ins_nombre from insumos where ins_sw_activo = 1 and ins_clave_int not in(select ins_clave_int from actividadinsumos where act_clave_int = '".$id."')");
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idi = $dat['ins_clave_int'];
				   $nomi = $dat['ins_nombre'];
			   ?>
               <option value="<?Php echo $idi?>"><?php echo $nomi;?></option>
               <?php
			   }
			?>
            	</select>
           		</div>
            </div>
          </div>
          <div class="form-group">
             <div class="col-xs-5">
           		<div class="input-group"><span class="input-group-addon">Rendimiento:</span> 
            	<input type="number" name="rendimiento" id="rendimiento" min="0"  onKeyPress="return NumCheck(event, this)" value="0" class="form-control"/> </div>
            </div>
             <div class="col-xs-7">
           		<div class="input-group"><span class="input-group-addon">Valor Unitario:</span> 
            	<input type="number" name="valorunitario" id="valorunitario" min="0"  onKeyPress="return NumCheck(event, this)" value="0" class="form-control"/> <span class="input-group-addon"><a onClick="CRUDACTIVIDADES('AGREGAR','<?php echo $id;?>')" role="button" class="btn btn-block btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></a></span>
            	</div>
            </div>
         </div>
      </h4>
  </div>
  <div class="panel-body">
             
       <div class="table-responsive" id="no-more-tables">
      <table id="tbinsumospendientes" class="col-md-12 table table-bordered table-hover">
                <thead>
                <tr>
                <th></th>
                
                <th>Tipo</th>
                <th>Nombre</th>
                <th>Unidad</th>
                <th>Rendimiento</th>
                <th>Valor</th>
                <th>Descripcion</th>
                </tr>
                </thead>
               <?PHP
		$con = mysqli_query($conectar,"select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where a.act_clave_int = '".$id."'   order by tip,nom,nomu")
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				    
				   ?>
                <tr>
               <td><a role="button" class="btn btn-block btn-danger btn-xs" onClick="CRUDACTIVIDADES('DELETE',<?PHP echo $idc;?>)"><i class="glyphicon glyphicon-trash"></i></a></td>
                 
                  <td data-title="Tipo"><?Php echo $tipo;?></td>
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title="Unidad"><?php echo $uni."(".$cod.")";?></td>
                  <td data-title="Rendimiento"><?php echo $ren;?></td>
                  <td data-title="Valor Unitario">$ <?php echo number_format($valor,2,',','.');?></td>
                  <td data-title="Descripcion"><?php echo $des;?></td>
                 
                </tr>
                <?php
			   }
			   ?>
                </tbody>
               <tfoot>
                <tr>
                <th></th>
                
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                </tr>
                </tfoot>
                </table>
                </div>
               </div>
</div>
           
      <?PHP
  }
  else if($opcion=="LISTAINSUMOS")
  {
     $id=$_POST['id'];
	$con = mysqli_query($conectar,"select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where a.act_clave_int = '".$id."'   order by tip,tpl,nom,nomu");
	  while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   $tpl = $dat['tpl'];
				  
				   $unidad = $uni."(".$cod.")";
				   $material=0; $equipo = 0; $mano = 0;
				  
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				    if($tpl==1){$material=$valor*$ren;}else if($tpl==2){$equipo = $valor*$ren;}else if($tpl==3){$mano = $valor*$ren;}
					$total = $material + $equipo + $mano;
				   $datos[] =  array("iddetalle"=>$idc,"insumo"=>$nom,"tipo"=>$tipo,"unidad"=>$unidad,"rendi"=>$ren,"valor"=>$valor,"material"=>number_format($material,2,',','.'),"equipo"=>number_format($equipo,2,',','.'),"mano"=>number_format($mano,2,',','.'),"total"=>$total);
			   }
			   
			   
	echo json_encode($datos);
  
  }
  else if($opcion=="GUARDARDETALLE")
  {
     $id = $_POST['id'];
	 $rendimiento = $_POST['rendimiento'];
	 $upd = mysqli_query($conectar,"update actividadinsumos set ati_rendimiento ='".$rendimiento."',ati_usu_actualiz='".$usuario."',ati_fec_actualiz='".$fecha."' where ati_clave_int ='".$id."'");
	 if($upd>0)
	 {
        echo 1;
	 }
	 else
	 {
	    echo 2;
	 }
  }
   else if($opcion=="GUARDARDETALLEV")
  {
     $id = $_POST['id'];
	 $valor = $_POST['valor'];
	 $upd = mysqli_query($conectar,"update actividadinsumos set ati_valor ='".$valor."',ati_usu_actualiz='".$usuario."',ati_fec_actualiz='".$fecha."' where ati_clave_int ='".$id."'");
	 if($upd>0)
	 {
        echo 1;
	 }
	 else
	 {
	    echo 2;
	 }
  }
?>