
<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
  $con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
  $dato = mysqli_fetch_array($con);
  $perfil = strtoupper($dato['prf_descripcion']);
  $percla = $dato['prf_clave_int'];
  $coordi = $dato['usu_coordinador'];
  $fontsize = $dato['usu_tam_fuente'];
	$fecha=date("Y/m/d H:i:s");

     //LIVERA LOS INGRESO A TAL PRESUPUESTO
  $update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");
  
  $opcion = $_POST['opcion'];
  if($opcion=="NUEVO")
  {
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group">
         
         <div class="col-md-6"><strong>Nombre:</strong>
         <div class="ui corner labeled input">
         <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre">
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
        <div class="col-md-6"><strong>Tipologia:</strong>
        <div class="ui corner labeled input">
         <select   placeholder="Ingrese el código" name="seltipologia" id="seltipologia" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <option value="1">Material</option>
         <option value="2">Equipo</option>
         <option value="3">Mano de Obra</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         
    </div>
  
    <div class="form-group">
        
          <div class="col-md-6"><strong>Estado:</strong><br>
        
                 <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" class="flat-red" checked value="1">
                 Activo</label>
                
                 <label> <input type="radio" name="radestado" id="opcion2" class="flat-red" value="0">
                Inactivo
                </label>
         </div>
    </div>
  </form>
  <?PHP  
  }
else
  if($opcion=="EDITAR" )
  {
	  $id = $_POST['id'];
	   $coninfo = mysqli_query($conectar,"select u.tpi_clave_int idi,u.tpi_tipologia as tip,u.tpi_nombre as nom,u.est_clave_int as est from tipoinsumos u   where u.tpi_clave_int = '".$id."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $idi = $datinfo['idi'];
	  $tip = $datinfo['tip'];
	  $nom  = $datinfo['nom'];
	  $est = $datinfo['est'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    <div class="form-group">
    <div class="col-md-6"><strong>Nombre:</strong>
    <div class="ui corner labeled input">
    <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese el nombre" value="<?php echo $nom;?>">  
     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>  
    </div>
    <div class="col-md-6"><strong>Tipologia:</strong>
    <div class="ui corner labeled input">
    <select  name="seltipologia" id="seltipologia" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <option <?php if($tip==1){ echo 'selected';}?> value="1">Material</option>
         <option <?php if($tip==2){ echo 'selected';}?> value="2">Equipo</option>
         <option <?php if($tip==3){ echo 'selected';}?> value="3">Mano de Obra</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
    </div>
    </div>
  
    <div class="form-group">
         
          <div class="col-md-6"><strong>Estado:</strong><br>
        
    <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" class="flat-red" checked value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
    Activo</label>
    <label for="opcion2"> <input type="radio" name="radestado" id="opcion2" class="flat-red" value="0" <?php if($est==0){echo 'checked';}?>>
    Inactivo
    </label>
         </div>
    </div>
  </form>
  <?PHP  
  }
   else if($opcion=="GUARDAR")
  {
     $tipo = $_POST['tipologia'];
	 $nombre  = $_POST['nombre'];
	 $estado  = $_POST['estado'];
	 $veri = mysqli_query($conectar,"select * from tipoinsumos where UPPER(tpi_nombre) = UPPER('".$nombre."') and tpi_tipologia='".$tipo."' and est_clave_int!=2");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $ins = mysqli_query($conectar,"insert into tipoinsumos(tpi_nombre,tpi_tipologia,est_clave_int,tpi_usu_actualiz,tpi_fec_actualiz) values('".$nombre."','".$tipo."','".$estado."','".$usuario."','".$fecha."')");
		if($ins>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
 
   else if($opcion=="GUARDAREDICION" )
  {
	 $ide =$_POST['id'];
     $tipo = $_POST['tipologia'];
	 $nombre  = $_POST['nombre'];
	 $estado  = $_POST['estado'];
	 $veri = mysqli_query($conectar,"select * from tipoinsumos where UPPER(tpi_nombre) = UPPER('".$nombre."') and tpi_tipologia  = '".$tipo."' and tpi_clave_int!='".$ide."' and est_clave_int!=2");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update tipoinsumos set tpi_tipologia='".$tipo."',tpi_nombre='".$nombre."',est_clave_int='".$estado."',tpi_usu_actualiz='".$usuario."',tpi_fec_actualiz='".$fecha."' where tpi_clave_int = '".$ide."'");
		if($upd>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
    else if($opcion=="CARGARLISTATIPOINSUMOS")
  {
	  ?> <script src="js/jstipoinsumos.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div>
      <table id="tbtipoinsumos" class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                  <th class="dt-head-center" style="width:20px"></th>                
                  <th class="dt-head-center" style="width:20px"></th>
                  <th class="dt-head-center">NOMBRE</th>
                  <th class="dt-head-center">TIPOLOGÍA</th>
                  <th class="dt-head-center" style="width:40px">ESTADO</th>
                </tr>
                </thead>
               <?PHP
			$con = mysqli_query($conectar,"select u.tpi_clave_int as di,u.tpi_nombre nom,u.tpi_tipologia tip,u.est_clave_int as est,e.est_nombre as estn from tipoinsumos u join estados e on e.est_clave_int = u.est_clave_int where e.est_clave_int not in(2) order by tip,nom")
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['di'];
				   $estnom = $dat['estn'];
				   $tip = $dat['tip'];
				   $nom = $dat['nom'];
				   $est = $dat['est'];
				   if($tip==1){ $tip="Material";}else if($tip==2){ $tip = "Equipo"; }else if($tip==3){ $tip = "Mano de Obra";}
				    if($est=="0"){$est='<span class="label label-warning pull-right">'.$estnom.'</span>';}
				   else {$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
				   ?>
                <tr id="row_tpi<?php echo $idc;?>">
               <td><a class="btn btn-block btn-default btn-xs" onClick="CRUDTIPOINSUMOS('EDITAR',<?PHP echo $idc;?>)" data-toggle="modal" data-target="#myModal" style="width:20px; height:20px"><i class="glyphicon glyphicon-pencil"></i></a></td>
                  <td><a class="btn btn-block btn-danger btn-xs" onClick="CRUDTIPOINSUMOS('ELIMINAR',<?PHP echo $idc;?>)" style="width:20px; height:20px"><i class="glyphicon glyphicon-trash" ></i></a></td>                 
                  <td><?Php echo $nom;?></td>
                  <td><?Php echo $tip;?></td>
                  <td><?Php echo $est;?></td>
                </tr>
                <?php
			   }
			   ?>
                </tbody>
                 <tfoot>
                <tr>
                <th></th>
                <th></th>                  
                <th>NOMBRE</th>
                <th>TIPOLOGÍA</th>
                <th>ESTADO</th>                  
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP

  }
  else if($opcion=="ELIMINAR")
  {
	   $id = $_POST['id'];
	$update = mysqli_query($conectar,"update tipoinsumos set est_clave_int = 2 where tpi_clave_int = '".$id."'");
	if($update>0){  echo 1;}else {echo 2;}
  }
?>