
<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
  $opcion = $_POST['opcion'];
  if($opcion=="NUEVO")
  {
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group">  
     <div class="col-xs-6"><strong>Nombre:<span class="symbol required"></span></strong>
    <input type="text" id="txtnombre" name="txtnombre" class="form-control" placeholder="Escribe aqui el nombre">
    </div>  
    <div class="col-xs-6"><strong>Descripcion:</span></strong>
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control" placeholder="Escribe aqui la descripcion">
    </div>
    </div>
       <div class="form-group">
         <div class="col-xs-4"><strong>Tipo de Proyecto:<span class="symbol required"></span></strong>
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where tpp_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
         </div> 
        <div class="col-xs-4"><strong>Estado Proyecto:<span class="symbol required"></span></strong>
         <select name="selestado" id="selestado" class="form-control">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select esp_clave_int,esp_nombre from estadosproyecto where esp_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['esp_clave_int'];
			 $nom = $dat['esp_nombre'];
			
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom;?></option>
            <?php
		 }
		 ?>		 
         </select>
         </div> 
          <div class="col-xs-4"><strong>Cliente:<span class="symbol required"></span></strong>
          <select name="selcliente" id="selcliente" class="form-control">
          <option value="">--seleccione--</option>
          <?php
		  $con = mysqli_query($conectar,"select per_clave_int,per_documento,per_nombre,per_apellido from persona where per_tipo_per = 1 and per_sw_activo = 1");
		  while($dat = mysqli_fetch_array($con))
		  {
			  $idp = $dat['per_clave_int'];
			  $doc = $dat['per_documento'];
			  $nom = $dat['per_nombre'];
			  $ape = $dat['per_apellido'];
		     ?>
             <option value="<?php echo $idp;?>"><?Php echo $nom." ".$ape." - ".$doc;?></option>
             <?php
	
		  }
		  ?>
          </select>
          </div>
    </div>    
   
    <div class="form-group">
         <div class="col-xs-4"><strong>Pais:<span class="symbol required"></span></strong>
          <select name="selpais" id="selpais" class="form-control" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where pai_sw_activo = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
         </div>
         <div class="col-xs-4"><strong>Departamento:<span class="symbol required"></span></strong>
           <select name="seldepartamento" id="seldepartamento" class="form-control" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
         </select>
         </div> 
         <div class="col-xs-4"><strong>Ciudad:<span class="symbol required"></span></strong>
             <select name="selciudad" id="selciudad" class="form-control">
         <option value="">--seleccione--</option>
         </select>
         </div>
    </div>
    
    <div class="form-group">
         <div class="col-xs-4"><strong>Fecha:<span class="symbol required"></span></strong>
         <input type="date" name="fecha" id="fecha" class="form-control">
        </div>
     </div>   
  </form>
  <?PHP  
  }
else
  if($opcion=="EDITAR" )
  {
	  $idpre = $_POST['id'];
	   $coninfo = mysqli_query($conectar,"select pr.pre_nombre nom,pr.pre_descripcion des,t.tpp_clave_int as tpp,e.esp_clave_int est,pe.per_clave_int cli,c.ciu_clave_int ciu,d.dep_clave_int dep,p.pai_clave_int pai,pr.pre_administracion as adm,pr.pre_imprevisto imp,pr.pre_utilidades uti,pr.pre_iva as iv,pr.pre_fecha as fe from presupuesto pr join persona pe on pe.per_clave_int = pr.per_clave_int join tipoproyecto t on t.tpp_clave_int = pr.tpp_clave_int join estadosproyecto e on e.esp_clave_int = pr.esp_clave_int join ciudad c on c.ciu_clave_int = pr.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where pr.pre_clave_int = '".$idpre."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $tpp = $datinfo['tpp'];
	  $nom = $datinfo['nom'];
	  $des = $datinfo['des'];
	  $ciu = $datinfo['ciu'];
	  $pai = $datinfo['pai'];
	  $dep = $datinfo['dep'];
	  $est = $datinfo['est'];
	  $cli = $datinfo['cli'];
	  $adm = $datinfo['adm'];
	  $imp = $datinfo['imp'];
	  $uti = $datinfo['uti'];
	  $iva = $datinfo['iva'];
	  $fechap = $datinfo['fe'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">

  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $idpre?>">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title text-primary">Informacion Presupuesto</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>           
          </div>
        </div>
        <div class="box-body" id="micolass">
  <input type="hidden" id="idedicion" value="<?php echo $idpre;?>">
<div class="form-group">   
 <div class="col-xs-6"><strong>Nombre:<span class="symbol required"></span></strong>
    <input type="text" id="txtnombre" name="txtnombre" value="<?php echo $nom;?>" class="form-control" placeholder="Escribe aqui el nombre">
    </div>   
    <div class="col-xs-6"><strong>Descripción:</span></strong>
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control" value="<?php echo $des;?>" placeholder="Escribe aqui la descripcion">
    </div>
    </div>
       <div class="form-group">
         <div class="col-xs-4"><strong>Tipo de Proyecto:<span class="symbol required"></span></strong>
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where tpp_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $idp = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option <?php if($tpp==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
         </div> 
        <div class="col-xs-4"><strong>Estado Proyecto:<span class="symbol required"></span></strong>
         <select name="selestado" id="selestado" class="form-control">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select esp_clave_int,esp_nombre from estadosproyecto where esp_sw_activo = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $ides = $dat['esp_clave_int'];
			 $nom = $dat['esp_nombre'];
			
		    ?>
            <option <?php if($ides==$est){echo 'selected';}?>  value="<?Php echo $ides;?>"><?php echo $nom;?></option>
            <?php
		 }
		 ?>		 
         </select>
         </div> 
          <div class="col-xs-4"><strong>Cliente:<span class="symbol required"></span></strong>
          <select name="selcliente" id="selcliente" class="form-control">
          <option value="">--seleccione--</option>
          <?php
		  $con = mysqli_query($conectar,"select per_clave_int,per_documento,per_nombre,per_apellido from persona where per_tipo_per = 1 and per_sw_activo = 1");
		  while($dat = mysqli_fetch_array($con))
		  {
			  $idc = $dat['per_clave_int'];
			  $doc = $dat['per_documento'];
			  $nom = $dat['per_nombre'];
			  $ape = $dat['per_apellido'];
		     ?>
             <option <?PHP if($idc==$cli){echo 'selected';}?> value="<?php echo $idc;?>"><?Php echo $nom." ".$ape." - ".$doc;?></option>
             <?php
	
		  }
		  ?>
          </select>
          </div>
    </div>    
   
  <div class="form-group">
         <div class="col-xs-4"><strong>Pais:<span class="symbol required"></span></strong>
          <select name="selpais" id="selpais" class="form-control" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where pai_sw_activo = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
         </div>
         <div class="col-xs-4"><strong>Departamento:<span class="symbol required"></span></strong>
           <select name="seldepartamento" id="seldepartamento" class="form-control" onChange="cargarciudad('seldepartamento','selciudad,'selpais')">
         <option value="">--seleccione--</option>
			<?php
            $con = mysqli_query($conectar,"select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and dep_sw_activo = 1");
            while($dat = mysqli_fetch_array($con))
            {
            ?>
                <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
            <?php
            }
            ?>
         </select>
         </div><div class="col-xs-4"><strong>Ciudad:<span class="symbol required"></span></strong>
             <select name="selciudad" id="selciudad" class="form-control">
         <option value="">--seleccione--</option>
       <?php
	     $con = mysqli_query($conectar,"select ciu_clave_int,ciu_nombre from ciudad where dep_clave_int ='".$dep."' and ciu_sw_activo = 1");
	 while($dat = mysqli_fetch_array($con))
	 {
     ?>
     <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
     <?php
		}
	 ?>
         </select>
         </div>
    </div>
    
    <div class="form-group">
         <div class="col-xs-4"><strong>Fecha:<span class="symbol required"></span></strong>
         <input type="date" name="fecha" id="fecha" class="form-control" value="<?php echo $fechap;?>">
     </div>
     </div>
        <!-- /.box-body -->
      </div>
      
      <?php  $var ="'AGREGADOS'"; $v = "''"; echo '<style onload="CRUDPRESUPUESTO('.$var.','.$idpre.','.$v.')"></style>';?>
      
    <div class="form-group">
    	<div class="col-xs-12">
           <div id="pendientes"></div>
        </div>
    </div>

  
  </form>
  <?PHP  
  }
   else if($opcion=="GUARDAR")
  {
	 $nombre = $_POST['nombre'];
	 $estado = $_POST['estado'];
	 $cliente  = $_POST['cliente'];
     $tipoproyecto = $_POST['tipoproyecto'];
	 $ciudad = $_POST['ciudad'];
	 $descripcion = $_POST['descripcion'];
	 $fechap = $_POST['fecha'];
	
	 
	 $veri = mysqli_query($conectar,"select * from presupuesto where UPPER(pre_nombre) = UPPER('".$nombre."') and tpp_clave_int = '".$tipoproyecto."' and per_clave_int = '".$cliente."' and ciu_clave_int = '".$ciudad."'");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 'error1';
	 }
	 else 
	 {
        $ins = mysqli_query($conectar,"insert into presupuesto(pre_nombre,pre_descripcion,tpp_clave_int,esp_clave_int,per_clave_int,ciu_clave_int,pre_fecha,pre_usu_actualiz,pre_fec_actualiz) values('".$nombre."','".$descripcion."','".$tipoproyecto."','".$estado."','".$cliente."','".$ciudad."','".$fechap."','".$usuario."','".$fecha."')"); 
		$idp = mysqli_insert_id($conectar);
		if($ins>0)
		{
		  
		   $con = mysqli_query($conectar,"select * from capitulos where cap_sw_activo = 1");
		   $num = mysqli_num_rows($con);
		   if($num>0 and $idp>0)
		   {
$inse = mysqli_query($conectar,"insert into presupuestocapitulos select '','".$idp."',cap_clave_int,'".$usuario."','".$fecha."' from capitulos");
			   
		   }
		   echo $idp;
		}
		else
	    {
		  echo 'error2';
		}	
	 }	 
  }
 
   else if($opcion=="GUARDAREDICION" )
  {
		$ide =$_POST['id'];
		$nombre = $_POST['nombre'];
		$estado = $_POST['estado'];
		$cliente  = $_POST['cliente'];
		$tipoproyecto = $_POST['tipoproyecto'];
		$ciudad = $_POST['ciudad'];
		$descripcion = $_POST['descripcion'];
		$administracion = $_POST['administracion'];
		$imprevisto = $_POST['imprevisto'];
		$utilidades = $_POST['utilidades'];
		$iva = $_POST['iva'];
		$fechap = $_POST['fecha'];
	 
	 $veri = mysqli_query($conectar,"select * from presupuesto where UPPER(pre_nombre) = UPPER('".$nombre."') and tpp_clave_int = '".$tipoproyecto."' and per_clave_int = '".$cliente."' and ciu_clave_int = '".$ciudad."' and  pre_clave_int!='".$ide."'");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update presupuesto set pre_nombre='".$nombre."',pre_descripcion='".$descripcion."',tpp_clave_int='".$tipoproyecto."',esp_clave_int='".$estado."',per_clave_int='".$cliente."',ciu_clave_int='".$ciudad."',pre_administracion='".$administracion."',pre_imprevisto = '".$imprevisto."',pre_utilidades='".$utilidades."',pre_iva = '".$iva."',pre_fecha='".$fechap."',pre_usu_actualiz='".$usuario."',pre_fec_actualiz='".$fecha."' where pre_clave_int = '".$ide."'");
		if($upd>0)
		{
		  
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
    else if($opcion=="CARGARLISTAPRESUPUESTO")
  {
	  ?> <script src="js/jspresupuesto.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div class="table-responsive" id="no-more-tables">
      <table id="tbpresupuesto" class="col-md-12 table table-bordered table-hover">
                <thead>
                <tr>
                <th></th>
                <th></th>
                
                <th>Nombre</th>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>T.Proyecto</th>
                <th>Estado</th>
                <th>% Admin</th>
                <th>% Impr</th>
                <th>% Util</th>
                <th>% Iva</th>
                <th>Total</th>
                <th>Codigo</th>
                
                </tr>
                </thead>
               <?PHP
			$con = mysqli_query($conectar,"select pre_clave_int ,pre_nombre,pre_descripcion,esp_nombre,tpp_nombre,ciu_nombre,per_nombre,per_apellido,per_documento,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_fecha,pai_nombre,dep_nombre,ciu_nombre from presupuesto pr join persona pe on pr.per_clave_int = pe.per_clave_int join tipoproyecto t on t.tpp_clave_int = pr.tpp_clave_int join estadosproyecto e on e.esp_clave_int = pr.esp_clave_int join ciudad c on c.ciu_clave_int = pr.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int order by pre_nombre,per_nombre,per_apellido");
			
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['pre_clave_int'];
				   $nom = $dat['pre_nombre'];
				   $des = $dat['pre_descripcion'];
				   $tipo = $dat['tpp_nombre'];
				   
				   $total = 0;//suma de insumos de todas las actividades 
				   $ubi = $dat['pai_nombre']."/".$dat['dep_nombre']."/".$dat['ciu_nombre'];
				  
				   $est = $dat['esp_nombre'];
				   $adm = $dat['pre_administracion'];
				   $imp = $dat['pre_imprevisto'];
				   $uti = $dat['pre_utilidades'];
				   $iva = $dat['pre_iva'];
				   $fechap = $dat['pre_fecha'];
				   
				   
				   /*$cons = mysqli_query($conectar,"select i.ins_valor as val,d.ati_valor as val1,d.ati_rendimiento as ren from insumos i join actividadinsumos d on d.ins_clave_int = i.ins_clave_int where d.act_clave_int = '".$idc."'");
				   $sumvalor = 0;
				   while($dats = mysqli_fetch_array($cons))
				   {
					   $ren = $dats['ren'];
					   $sv = $dats['val']*$ren;
					   $sv1 = $dats['val1']*$ren;
					   if($sv1<=0){$sumvalor+=$sv;}else{$sumvalor+=$sv1;}
				   }*/
				 
				   ?>
                <tr>
                <td  class="details-control"></td>
               <td><a class="btn btn-block btn-warning btn-xs" onClick="CRUDPRESUPUESTO('EDITAR',<?PHP echo $idc;?>,'')" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-pencil"></i></a></td>
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title="Fecha"><?php echo $fechap;?></td>
                  <td data-title="Descripcion"><?Php echo $des?></td>
                  <td data-title="Tipo Proyecto"><?Php echo $tipo;?></td>
                  <td data-title="Estado"><?php echo $est;?></td>
                  <td data-title="% Administracion"><?php echo $adm;?></td>
                  <td data-title="% Imprevisto"><?php echo $imp; ?></td>
                  <td data-title="% Utilidades"><?Php echo $uti;?></td> 
                  <td data-title="% Iva"><?Php echo $iva;?></td>
                  <td data-title="Total"><?Php echo $total;?></td>
                  <td data-title="Codigo"><?Php echo $idc;?></td>
                 
                 
                </tr>
                <?php
			   }
			   ?>
                </tbody>
                 <tfoot>
                <tr>
                <th></th>
                <th></th>
                 
                <th>Nombre</th>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>Tipo Proyecto</th>
                <th>Estado</th>
                <th>% Admin</th>
                <th>% Impre</th>
                <th>% Util</th>
                <th>% IVa</th>
                <th>Total</th>
                <th>Codigo</th>
                
               
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP

  }
   else if($opcion=="AGREGAR")
  {
	  $ida = $_POST['id'];
      $capitulo = $_POST['capitulos'];
	  $ins = mysqli_query($conectar,"insert into presupuestocapitulos(pre_clave_int,cap_clave_int,prc_usu_actualiz,prc_fec_actualiz) values ('".$ida."','".$capitulo."','".$usuario."','".$fecha."')");
	  if($ins>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
   else if($opcion=="DELETE")
  {
	  $ide= $_POST['id'];
	  $veri = mysqli_query($conectar,"select * from pre_cap_actividad where prc_clave_int = '".$ide."'");
	  $numv = mysqli_num_rows($veri);
	  if($numv>0){$delete = mysqli_query($conectar,"delete from pre_cap_actividad where prc_clave_int = '".$ide."'");}
	  $ins = mysqli_query($conectar,"delete from presupuestocapitulos where prc_clave_int = '".$ide."'");
	  if($ins>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
  else
   if($opcion=="AGREGADOS")
  {
	  $id = $_POST['id'];
	  $con  = mysqli_query($conectar,"select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva from presupuesto where pre_clave_int = '".$id."' limit 1");
	  $dat = mysqli_fetch_array($con);   
	  $adm = $dat['pre_administracion'];
	  $iva = $dat['pre_iva'];
	  $imp = $dat['pre_imprevisto'];
	  $uti = $dat['pre_utilidades'];
	  
	 
	  ?>

   <div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"> 
    	<div class="form-group">
            <div class="col-xs-8">
            	<div class="input-group"><span class="input-group-addon">Capitulos:</span> 
            	<select name="selagregar" id="selagregar" class="form-control">
            	<option value="">--seleccione--</option>
            <?php
			   $con = mysqli_query($conectar,"select cap_clave_int, cap_nombre from capitulos where cap_sw_activo = 1 and cap_clave_int not in(select cap_clave_int from presupuestocapitulos where pre_clave_int = '".$id."')");
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idi = $dat['cap_clave_int'];
				   $nomi = $dat['cap_nombre'];
			   ?>
               <option value="<?Php echo $idi?>"><?php echo $nomi;?></option>
               <?php
			   }
			?>
            	</select>
           		<span class="input-group-addon"><a onClick="CRUDPRESUPUESTO('AGREGAR','<?php echo $id;?>','')" role="button" class="btn btn-block btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></a></span>
            	</div>
            </div>
         </div>
      </h4>
  </div>
  <div class="panel-body">
  <div class="form-group">
  <div class="col-xs-12">
         <script src="js/jsagregados.js"></script>    
       <div class="table-responsive" id="no-more-tables">
      <table id="tbagregados" class="col-md-12 table table-bordered table-hover">
                <thead>
                <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>Nombre</th>
                <th>Unidad</th>
                <th>Cantidad</th>
                <th>Valor</th>
                <th>Valor Total</th>
                <th></th>
                </tr>
                </thead>
               <?PHP
		$con = mysqli_query($conectar,"select d.prc_clave_int idd, c.cap_clave_int id,c.cap_nombre as nom from  presupuesto p join presupuestocapitulos d on p.pre_clave_int = d.pre_clave_int join capitulos c on d.cap_clave_int = c.cap_clave_int  where p.pre_clave_int = '".$id."'   order by nom")
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $total = 0;
				    
				   ?>
                <tr>
                <td class="details-control"></td>
                <td><a role="button" class="btn btn-block btn-success btn-xs" onClick="CRUDPRESUPUESTO('AGREGARACTIVIDAD',<?PHP echo $idc;?>,'')"><i class="glyphicon glyphicon-plus"></i></a></td>
               <td><a role="button" class="btn btn-block btn-danger btn-xs" onClick="CRUDPRESUPUESTO('DELETE',<?PHP echo $idc;?>,'')"><i class="glyphicon glyphicon-trash"></i></a></td>
                 
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title=""></td>
                  <td data-title=""></td>
                  <td data-title=""></td>
                  <td data-title="Valor Total">$ <?php echo number_format($total,2,',','.');?></td>
                  <td data-title=""><?php echo $idc;?></td>
                
                 
                </tr>
                <?php
			   }
			   ?>
                </tbody>
               <tfoot>
                <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>Nombre</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                </tr>
                </tfoot>
                </table>
                </div>
              </div>
             </div>
             <div class="form-group">
               <div class="col-xs-6"></div>
               <div class="col-xs-6">
                  
                      <ul class="list-group">
                      <li class="list-group-item"><span class="badge bg-green pull-right">$ 0</span>Total Costo Directo:</li>
                      <li class="list-group-item"><span class="badge bg-green pull-right">$ <?php echo $adm?></span>
                           <div class="row">
                               <div class="col-xs-9">
                               <label class="col-xs-6 control-label">Administraciòn:</label>
                               <div class="input-group input-group-sm"><input type="number" id="administracion" class="form-control" value="<?php echo $adm?>"></div>
                               </div>
                               <div class="col-xs-3"></div>
                           </div>
                      </li>
                      <li class="list-group-item"><span class="badge bg-green pull-right">$ <?php echo $imp?></span>
                           <div class="row">
                               <div class="col-xs-9">
                               <label class="col-xs-6 control-label">Imprevisto:</label>
                               <div class="input-group input-group-sm"><input type="number" id="imprevisto" class="form-control" value="<?php echo $imp?>"></div>
                               </div>
                               <div class="col-xs-3"></div>
                           </div>
                       </li>
                      <li class="list-group-item"><span class="badge bg-green pull-right">$ <?php echo $uti?></span>
                           <div class="row">
                               <div class="col-xs-9">
                               <label class="col-xs-6 control-label">Utilidades:</label>
                               <div class="input-group input-group-sm"><input type="number" id="utilidades" class="form-control" value="<?php echo $uti;?>"></div>
                               </div>
                               <div class="col-xs-3"></div>
                           </div>
                        </li>
                      <li class="list-group-item"><span class="badge bg-green pull-right">$ <?php echo $iva;?></span>
                           <div class="row">
                               <div class="col-xs-9">
                               <label class="col-xs-6 control-label">Iva:</label>
                               <div class="input-group input-group-sm"><input type="number" id="iva" class="form-control" value="<?php echo $iva;?>"></div>
                               </div>
                               <div class="col-xs-3"></div>
                           </div></li>
                      <li class="list-group-item "><span class="badge bg-green pull-right">$ 0</span>Total Presupuesto :</li>
                      </ul>
                      
                      
               </div>
             </div>
              
               </div>
</div>
 <!-- Default box -->
      <div class="box" id="divagregaractividad">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>Actividades por agregar</strong></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div id="divactividades" ></div> 
        </div>
        <!-- /.box-body -->
       
      </div>
           
      <?PHP
  }
  else if($opcion=="LISTACAPITULOS")
  {
     $id=$_POST['id'];
	$con = mysqli_query($conectar,"select d.prc_clave_int idd, c.cap_clave_int id,c.cap_nombre as nom from  presupuesto p join presupuestocapitulos d on d.pre_clave_int = p.pre_clave_int join capitulos c on d.cap_clave_int = c.cap_clave_int  where p.pre_clave_int = '".$id."'   order by nom");
	  while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $idca = $dat['id'];
				   $nom = $dat['nom'];
			       //consulta del total de la suma de las actividades asignadas a este capitulo
				 
				   $datos[] =  array("iddetalle"=>$idc,"capitulo"=>$nom,"total"=>0);
			   }
			   
			   
	echo json_encode($datos);
  
  }
   else if($opcion=="LISTAACTIVIDADES")
  {
     $id=$_POST['id'];
	 //$con = mysqli_query($conectar,"")
	$con = mysqli_query($conectar,"select d.pca_clave_int idd, d.prc_clave_int idc,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,d.pca_cantidad cant from  pre_cap_actividad  d join actividades a on a.act_clave_int = d.act_clave_int join unidades u on u.uni_clave_int = a.uni_clave_int where d.prc_clave_int = '".$id."' order by nom");
	  while($dat = mysqli_fetch_array($con))
			   {
				   $idd = $dat['idd'];
				   $idca = $dat['id'];
				   $nom = $dat['nom'];
				   $uni = $dat['uni'];
				   $ida = $dat['ida'];
				   $cant= $dat['cant'];
			       //consulta del total de la suma de las actividades asignadas a este capitulo
				 
				   $datos[] =  array("iddetalle"=>$idd,"actividad"=>$nom,"total"=>0,"unidad"=>$uni,"idactividad"=>$ida,"apu"=>0,"cantidad"=>$cant);
			   }
			   
			   
	echo json_encode($datos);
  
  }
  else if($opcion=="GUARDARDETALLE")
  {
     $id = $_POST['id'];
	 $rendimiento = $_POST['rendimiento'];
	 $upd = mysqli_query($conectar,"update actividadinsumos set ati_rendimiento ='".$rendimiento."',ati_usu_actualiz='".$usuario."',ati_fec_actualiz='".$fecha."' where ati_clave_int ='".$id."'");
	 if($upd>0)
	 {
        echo 1;
	 }
	 else
	 {
	    echo 2;
	 }
  }
   else if($opcion=="GUARDARDETALLEV")
  {
     $id = $_POST['id'];
	 $valor = $_POST['valor'];
	 $upd = mysqli_query($conectar,"update actividadinsumos set ati_valor ='".$valor."',ati_usu_actualiz='".$usuario."',ati_fec_actualiz='".$fecha."' where ati_clave_int ='".$id."'");
	 if($upd>0)
	 {
        echo 1;
	 }
	 else
	 {
	    echo 2;
	 }
  }
  else  if($opcion=="AGREGARACTIVIDAD")
  {
	  $id = $_POST['id'];
	  
	  ?> <script src="js/jsactividadesagregadas.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
      <input type="hidden" id="idca" value="<?php echo $id;?>">
        <div class="table-responsive" id="no-more-tables">
      <table id="tbactividadesagregadas" class="col-md-12 table table-bordered table-hover">
                <thead>
                <tr>
                <th></th>
                <th>Codigo</th>               
                <th>Descripción</th>
                <th>Tipo Proyecto</th>
                <th>Unidad</th>
                <th>Ubicacion</th>
                <th>Valor APU($)</th>
                </tr>
                </thead>
               <?PHP
			$con = mysqli_query($conectar,"select act_clave_int id,act_nombre as nom,t.tpp_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,act_analisis as ana, act_sw_activo as est,p.pai_nombre as pai,d.dep_nombre as depa,c.ciu_nombre as ciu from actividades i join tipoproyecto t on t.tpp_clave_int = i.tpp_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int join ciudad c on c.ciu_clave_int = i.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int where i.act_clave_int not in(select act_clave_int from pre_cap_actividad where prc_clave_int='".$id."') order by tip,nom,nomu")
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['id'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $valor = 0;//suma de insumos de todas las actividades 
				   $ubi = $dat['pai']."/".$dat['depa']."/".$dat['ciu'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ana = $dat['ana'];
				   
				   $cons = mysqli_query($conectar,"select i.ins_valor as val,d.ati_valor as val1,d.ati_rendimiento as ren from insumos i join actividadinsumos d on d.ins_clave_int = i.ins_clave_int where d.act_clave_int = '".$idc."'");
				   $sumvalor = 0;
				   while($dats = mysqli_fetch_array($cons))
				   {
					   $ren = $dats['ren'];
					   $sv = $dats['val']*$ren;
					   $sv1 = $dats['val1']*$ren;
					   if($sv1<=0){$sumvalor+=$sv;}else{$sumvalor+=$sv1;}
				   }
				   if($ana=="0"){$ana='<span class="label label-warning pull-right">No</span>';}
				   else {$ana='<span class="label label-success pull-right">Si</span>';}
				    if($est=="0"){$est='<span class="label label-warning pull-center">Inactivo</span>';}
				   else {$est='<span class="label label-success pull-center">Activo</span>';}
				   ?>
                <tr>
                
               <td><a title="" class="btn btn-block btn-info btn-xs" onClick="CRUDPRESUPUESTO('ASIGNAR',<?PHP echo $idc;?>,<?php echo $id;?>)"><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="Asignar Actividad"></i></a></td>
                  <td data-title="Codigo"><?Php echo $idc;?></td>
                  <td data-title="Descripcion"><?Php echo $nom;?></td>
                  <td data-title="Tipo Proyecto"><?Php echo $tipo;?></td>
                  <td data-title="Unidad"><?php echo $uni."(".$cod.")";?></td>
                  <td data-title="Ubicacion"><?php echo $ubi;?></td>
                  <td data-title="Valor APU($)"><div id="divapu<?php echo $idc;?>"><?php echo number_format($sumvalor,2,'.',',');?></div></td>
                
                 
                 
                </tr>
                <?php
			   }
			   ?>
                </tbody>
                 <tfoot>
                <tr>
                <th></th>
               
                <th>Codigo</th>
                <th>Descripción</th>
                <th>Tipo Proyecto</th>
                <th>Unidad</th>
                <th>Ubicacion</th>
                <th>Valor APU($)</th>
       
               
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP

  }
  else if($opcion=="ASIGNAR")
  {
	$id = $_POST['id'];
	$idd = $_POST['idd'];//id del presupuesto,
	$ins = mysqli_query($conectar,"insert into pre_cap_actividad(prc_clave_int,act_clave_int,pca_usu_actualiz,pca_fec_actualiz) values ('".$idd."','".$id."','".$usuario."','".$fecha."') ");
	if($ins>0)
	{
		echo 1;
	}
	else
	{
		echo 2;
	}
  }
?>