<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
  $con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
  $dato = mysqli_fetch_array($con);
  $perfil = strtoupper($dato['prf_descripcion']);
  $percla = $dato['prf_clave_int'];
  $coordi = $dato['usu_coordinador'];
  $fontsize = $dato['usu_tam_fuente'];
	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
  $opcion = $_POST['opcion'];
	
	//CONSULTA DE UNIDADES CREADAS POR ESE USUARIO
	$unidades = 0;
	$con = mysqli_query($conectar,"select uni_clave_int from unidades where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $uni = $dat['uni_clave_int'];
		   $idsu[] = $uni;
	   }
	   $unidades = implode(',',$idsu);
	}
  if(strtoupper($perfil)=="ADMINISTRADOR"){ $estado=1;}else{$estado=3;}

     //LIVERA LOS INGRESO A TAL PRESUPUESTO
  $update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");

  
  if($opcion=="NUEVO")
  {
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group">
         <div class="col-md-6"><strong>Código:</strong>
         <div class="ui corner labeled input">
         <input type="text" placeholder="Ingrese el código" name="txtcodigo" id="txtcodigo" class="form-control input-sm">
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-6"><strong>Nombre:</strong>
         <div class="ui corner labeled input">
         <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre">
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        
         </div>
    </div>
 
  </form>
  <?PHP  
  }
  else if($opcion=="UNIFICAR")
  {
	 ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
    <div class="row">
        <div class="col-md-8">
        <strong>Unidades a Unificar:</strong>
        <select name="unidadunificar" id="unidadunificar" class="form-control input-sm selectpicker" multiple="multiple" data-live-search="true" data-live-search-placeholder="Buscar Unidad" style="width:100%">
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int in(".$unidades.") order by uni_codigo");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
        </select>
        </div>
        <div class="col-md-4">
         <strong>Unidad Base:</strong>
        <select name="unidadpor" id="unidadpor" class="form-control input-sm selectpicker" style="width:100%">
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int in(".$unidades.") order by uni_codigo");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
        </select>
        </div>
    </div>
    </form>
      
      <?php
	  echo "<style onload=INICIALIZARLISTAS('MODAL')></style>";
  }
else
if($opcion=="GUARDARUNIFICAR")
{
  $uniu = $_POST['uniu']; $uniu = implode(', ', (array)$uniu);
  $unip = $_POST['unip'];
  
  $conu = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where uni_clave_int in(".$uniu.")");
  $cantu = mysqli_num_rows($conu);
  if($cantu>0)
  {
	  for($k=0;$k<$cantu;$k++)
	  {
		  $datu = mysqli_fetch_array($conu);
		  $idu = $datu['uni_clave_int'];
		  $update = mysqli_query($conectar,"update actividades set uni_clave_int = '".$unip."' where uni_clave_int ='".$idu."'");
		  $update2 = mysqli_query($conectar,"update insumos set uni_clave_int = '".$unip."' where uni_clave_int = '".$idu."'");
		  if($idu!=$unip)
		  {
		  	$update3 = mysqli_query($conectar,"update unidades set est_clave_int = 2 where uni_clave_int = '".$idu."'");
		  }
	  }
	  $res = 1;
	  $msn = 'Unidades Unificadas correctamente';
  }
  else
  {
	 $res = 2;
	$msn = 'No ha seleccionado ninguna unidad a unificar';
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else
  if($opcion=="EDITAR" )
  {
	  $id = $_POST['id'];
	   $coninfo = mysqli_query($conectar,"select u.uni_clave_int idu,u.uni_codigo as cod,u.uni_nombre as nom,u.est_clave_int as est from unidades u   where u.uni_clave_int = '".$id."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $idu = $datinfo['idu'];
	  $cod = $datinfo['cod'];
	  $nom  = $datinfo['nom'];
	//  $est = $datinfo['est'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    <div class="form-group">
    <div class="col-md-6"><strong>Código:</strong>
    <div class="ui corner labeled input">
    <input name="txtcodigo" id="txtcodigo" type="text" class="form-control input-sm" value="<?php echo $cod;?>" placeholder="Ingrese el código">
    <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
    </div><div class="col-md-6"><strong>Nombre:</strong>
    <div class="ui corner labeled input">
    <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese el nombre" value="<?php echo $nom;?>">
    <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>    
    </div>
    </div>
  
  </form>
  <?PHP  
  }
   else if($opcion=="GUARDAR")
  {
     $codigo = $_POST['codigo'];
	 $nombre  = $_POST['nombre'];
	 $veri = mysqli_query($conectar,"select * from unidades where UPPER(uni_codigo) = UPPER('".$codigo."') and (est_clave_int = 1 or uni_clave_int in(".$unidades."))");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $ins = mysqli_query($conectar,"insert into unidades(uni_codigo,uni_nombre,est_clave_int,uni_usu_actualiz,uni_fec_actualiz,usu_clave_int) values('".$codigo."','".$nombre."','".$estado."','".$usuario."','".$fecha."','".$idUsuario."')");
		if($ins>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
 
   else if($opcion=="GUARDAREDICION" )
  {
	 $ide =$_POST['id'];
     $codigo = $_POST['codigo'];
	 $nombre  = $_POST['nombre'];
	 $veri = mysqli_query($conectar,"SELECT * FROM unidades WHERE UPPER(uni_codigo  = '".$codigo."')) and uni_clave_int!='".$ide."' and (est_clave_int = 1  or uni_clave_int in(".$unidades."))");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update unidades set uni_codigo='".$codigo."',uni_nombre='".$nombre."',uni_usu_actualiz='".$usuario."',uni_fec_actualiz='".$fecha."' where uni_clave_int = '".$ide."'");
		if($upd>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
  else if($opcion=="CARGARLISTAUNIDADES")
  {
	  ?> <script src="js/jsunidades.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div>
      <table id="tbunidades" class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                  <th class="dt-head-center" style="width:20px"></th>
                  <th class="dt-head-center" style="width:20px"></th>
                  <th class="dt-head-center" style="width:40px">CÓDIGO</th>
                  <th class="dt-head-center">NOMBRE</th>
                  <th class="dt-head-center" style="width:40px">ESTADO</th>
                </tr>
                </thead>
                  <tfoot>
                <tr>
                <th></th>
                <th></th>
                <th class="dt-head-center">CÓDIGO</th>
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center">ESTADO</th>
                </tr>
                </tfoot>           
                </tbody>
                </table>
                </div>
      <?PHP

  }
  else if($opcion=="ELIMINAR")
  {
    $id = $_POST['id'];
	$update = mysqli_query($conectar,"update unidades set est_clave_int = 2 where uni_clave_int = '".$id."'");
	if($update>0){  echo 1;}else {echo 2;}
  }
  
?>