<?php
include ("../data/Conexion.php");
require_once('../Classes/PHPMailer-master/class.phpmailer.php');
require_once('../Classes/PHPMailer-master/class.smtp.php');
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
$perfil = strtoupper($dato['prf_descripcion']);
$percla = $dato['prf_clave_int'];
$coordi = $dato['usu_coordinador'];
$fontsize = $dato['usu_tam_fuente'];

date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$opcion = $_POST['opcion'];
function decrypt($string, $key)
{
	$result = "";
	$string = base64_decode($string);
	for($i=0; $i<strlen($string); $i++) 
	{
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)-ord($keychar));
		$result.=$char;
	}
	return $result;
}
function encrypt($string, $key) 
{
	$result = "";	
	for($i=0; $i<strlen($string); $i++) 
	{	
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)+ord($keychar));
		$result.=$char;
	}
	return base64_encode($result);
}
   //LIVERA LOS INGRESO A TAL PRESUPUESTO
  $update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");

if($opcion=="NUEVO")
{
	?>
    <form name="form1" id="form1" class="form-horizontal">
			     <div class="row">
			         <div class="col-md-6"><strong>Nombre:</strong>
                     <div class="ui corner labeled input">
			             <input type="text" name="txtnombre" id="txtnombre" class="form-control input-sm" autocomplete="off">
			         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
                     </div>
			     
			         <div class="col-md-6"><strong>Usuario:</strong>
                     <div class="ui corner labeled input">
			             <input type="text" name="txtusuario" id="txtusuario" class="form-control input-sm" autocomplete="off">
			         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
                     </div>
			     </div>
			     <div class="row">
			         <div class="col-md-6"><strong>Contraseña:</strong>
                     <div class="ui corner labeled input">
			             <input type="password" name="txtpass" id="txtpass" class="form-control input-sm" autocomplete="off">
			         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
                     </div>
			     
			         <div class="col-md-6"><strong>Repetir:</strong>
                     <div class="ui corner labeled input">
			             <input type="password" name="txtpass1" id="txtpass1" class="form-control input-sm" autocomplete="off">
                      <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
			         </div>
			     </div>
                  <div class="row">
			         <div class="col-md-6 hide"><strong>Cargo:</strong>
                     <div class="ui corner labeled input">
			            <select class="form-control input-sm" name="selcargo" id="selcargo">
			            <option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from cargos where est_clave_int=1 order by car_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['car_clave_int'];
								$cargo = $dato['car_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $cargo; ?></option>
						<?php
							}
						?>
						</select>
                       <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
			         </div>
			    
			         <div class="col-md-6"><strong>Perfil:</strong>
                     <div class="ui corner labeled input">
			            <select class="form-control input-sm" name="selperfil" id="selperfil">
			            <option value="">-Seleccione-</option>
						<?php
						if(strtoupper($perfil)=="ADMINISTRADOR")
						{
							$con = mysqli_query($conectar,"select distinct prf_clave_int,prf_descripcion from perfil where est_clave_int=1 order by prf_descripcion");
						}
						else 
						{
							$con = mysqli_query($conectar,"select distinct prf_clave_int,prf_descripcion from perfil where est_clave_int=1 and UPPER(prf_descripcion) not in('ADMINISTRADOR','COORDINADOR') order by prf_descripcion");
						}
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['prf_clave_int'];
								$per = $dato['prf_descripcion'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $per; ?></option>
						<?php
							}
						?>
						</select>
                       <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
			         </div>  
			         <div class="col-md-6"><strong>E-mail:</strong>
                     <div class="ui corner labeled input">
			             <input type="email" name="txtemail" id="txtemail" class="form-control input-sm" autocomplete="off">
			         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
                     </div>	
			     </div>
			     <div class="row">
			       	
                     <div class="col-md-6"><strong>Coordinador:</strong>
   						 <div class="ui corner labeled input">
				          <select name="selcoordinador" id="selcoordinador" class="form-control input-sm">
				          <option value="">--seleccione--</option>
				          <?php		
						  if(strtoupper($perfil)=="ADMINISTRADOR")
						  {  
						  	   $consulta = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
						  }
						  else if(strtoupper($perfil)=="COORDINADOR")
						  {
						      $consulta = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int  where usu_clave_int = '".$idUsuario."' and  u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
						  }
						  $num = mysqli_num_rows($consulta);
						  for($k=0;$k<$num;$k++)
						  {
						  	   $dat = mysqli_fetch_array($consulta);
						  
							  $idp = $dat['usu_clave_int'];	
							  $nom = $dat['usu_nombre'];
							  $ema = $dat['usu_email'];
						     ?>
				             <option value="<?php echo $idp;?>"><?Php echo $nom." - ".$ema;?></option>
				             <?php
					
						  }
						  ?>
				          </select>
            	<div class="ui corner label"> <i class="asterisk icon" title="<?php echo $perfil;?>"></i> </div></div>
   
          		</div>
          		<div class="col-md-6"><strong>Activo:</strong>
				<input name="ckactivo" type="checkbox" checked="checked" value="1" />
			</div>	    
          </div>
		
			  
			     

	</form>
        <?Php
}
else
if($opcion == 'EDITAR')
{
		$usuedi = $_POST['id'];
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usuedi."'"); 
		$dato = mysqli_fetch_array($con); 
		$nom = $dato['usu_nombre'];
		$usu = $dato['usu_usuario'];
		$con = decrypt($dato['usu_clave'],'p4v4svasquez');
		$ema = $dato['usu_email'];
		$act = $dato['est_clave_int'];
		$per = $dato['prf_clave_int'];
		$car = $dato['car_clave_int'];
		$cor = $dato['usu_coordinador'];
?>
		<form name="form1" id="form1" class="form-horizontal">
        <input type="hidden" name="idedicion" id="idedicion" value="<?php echo $usuedi?>"/>
		     <div class="row">
		         <div class="col-md-6"><strong>Nombre:</strong>
                 <div class="ui corner labeled input">
		             <input type="text" name="txtnombre" id="txtnombre" class="form-control input-sm" value="<?php echo $nom; ?>" autocomplete="off">
                     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
		         </div>
		     
		         <div class="col-md-6"><strong>Usuario:</strong>
                 <div class="ui corner labeled input">
		             <input type="text" name="txtusuario" id="txtusuario" class="form-control input-sm" value="<?php echo $usu; ?>" autocomplete="off">
                     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
		         </div>
		     </div>
		     <div class="row">
		         <div class="col-md-6"><strong>Contraseña:</strong>
                 <div class="ui corner labeled input">
		             <input type="password" name="txtpass" id="txtpass" class="form-control input-sm" value="<?php echo $con; ?>" autocomplete="off">
                     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
		         </div>
		  
		         <div class="col-md-6"><strong>Repetir:</strong>
                 <div class="ui corner labeled input">
		             <input type="password" name="txtpass1" id="txtpass1"  class="form-control input-sm" value="<?php echo $con; ?>" autocomplete="off">
                     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
		         </div>
		     </div>
                <div class="row">
			         <div class="col-md-6 hide"><strong>Cargo:</strong>
                     <div class="ui corner labeled input">
			            <select class="form-control input-sm" name="selcargo" id="selcargo">
			            <option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from cargos where est_clave_int=1 order by car_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['car_clave_int'];
								$cargo = $dato['car_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($car==$clave){echo 'selected';}?>><?php echo $cargo; ?></option>
						<?php
							}
						?>
						</select>
                        <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
			         </div>
			    
		         <div class="col-md-6"><strong>Perfil:</strong>
                 <div class="ui corner labeled input">
		            <select class="form-control input-sm" name="selperfil" id="selperfil">
                    <option>--Seleccione--</option>
					<?php
						if(strtoupper($perfil)=="ADMINISTRADOR")
						{
							$con = mysqli_query($conectar,"select distinct prf_clave_int,prf_descripcion from perfil where est_clave_int=1 order by prf_descripcion");
						}
						else 
						{
							$con = mysqli_query($conectar,"select distinct prf_clave_int,prf_descripcion from perfil where est_clave_int=1 and UPPER(prf_descripcion) not in('ADMINISTRADOR','COORDINADOR') order by prf_descripcion");
						}
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['prf_clave_int'];
							$peri = $dato['prf_descripcion'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($per == $clave){  echo "selected"; } ?>><?php echo $peri; ?></option>
					<?php
						}
					?>
					</select>
                    <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
		         </div>
		         <div class="col-md-6"><strong>E-mail:</strong>
	                 <div class="ui corner labeled input">
			             <input type="email" name="txtemail" id="txtemail" class="form-control input-sm" value="<?php echo $ema; ?>" autocomplete="off">
	                 <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
		         </div>
		     </div>
		     <div class="row">
		         
                 <div class="col-md-6"><strong>Coordinador:</strong>
     				<div class="ui corner labeled input">
          <select name="selcoordinador" id="selcoordinador" class="form-control input-sm">
          <option value="">--seleccione--</option>
          <?php
		    if(strtoupper($perfil)=="ADMINISTRADOR")
		  {  
		  		$con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
		  }
		  else if(strtoupper($perfil)=="COORDINADOR")
		  {
		      $con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int  where usu_clave_int = '".$idUsuario."' and  u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
		  }
		
		  while($dat = mysqli_fetch_array($con))
		  {
			  $idp = $dat['usu_clave_int'];
			  $nom = $dat['usu_nombre'];
			  $ema = $dat['usu_email'];
		     ?>
             <option <?PHP if($idp==$cor){echo 'selected';}?> value="<?php echo $idp;?>"><?Php echo $nom." - ".$ema;?></option>
             <?php	
		  }
		  ?>
          </select>
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
          </div>
			<div class="col-md-6"><strong>Activo:</strong>
			<input <?php if($act == 1){ echo 'checked="checked"'; } ?> name="ckactivo" type="checkbox" value="1" />
			</div>
          </div>
           <div class="row">
             	<div class="col-md-6">   
             	<h4>Presupuesto por asignar:</h4> 
                   <input type="button" class="btn btn-success btn-xs" value="Pasar &raquo; " onclick="CRUDUSUARIOS('PASAR','','')" />   
                   <input type="button" class="btn btn-success btn-xs" value="Todos &raquo;&raquo; " onclick="CRUDUSUARIOS('PASARTODOS','','')" />  
                       
             	  <select name="presupuestoagregar" id="presupuestoagregar" title="Presupuestos por asignar" class="form-control selectpicker" multiple="multiple" style="width:100%">
					<?php
                   
				   $con = mysqli_query($conectar,"select * from presupuesto where pre_clave_int NOT IN (select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$usuedi."') and est_clave_int in(0,5) order by pre_nombre");
	$num = mysqli_num_rows($con);
                    for($i = 0; $i < $num; $i++)
                    {
						$dato = mysqli_fetch_array($con);
						$clave = $dato['pre_clave_int'];
						$pres = $dato['pre_nombre'];
						
						?>
						
						<option value="<?php echo $clave; ?>"><?php echo $pres; ?></option>
						
						<?php
                    
                    }
                    
                    ?>
                  </select>
                </div>
             	<div class="col-md-6">
             		<h4>Presupuestos asignados:</h4> 
                <input type="button" class="btn btn-danger btn-xs" value="&laquo;&laquo; Todos"  onclick="CRUDUSUARIOS('QUITARTODOS','','')" />
                <input type="button" class="btn btn-danger btn-xs" value="&laquo; Quitar"  onclick="CRUDUSUARIOS('QUITAR','','')" />   
                   
                <select name="presupuestoagregados" id="presupuestoagregados" title="Presupuestos  asignados"class="form-control selectpicker" multiple="multiple" style="width:100%" data-selected-text-format="count > 1">				
				<?php 				
				$sql = mysqli_query($conectar,"select * from usuario_presupuesto uo inner join presupuesto o on (o.pre_clave_int = uo.pre_clave_int) where uo.usu_clave_int = '".$usuedi."' and o.est_clave_int in(0,5) order by o.pre_nombre");
				$num = mysqli_num_rows($sql);
				
				for($i = 0; $i < $num; $i++)
				{				
				$dato = mysqli_fetch_array($sql);
				$clave = $dato['pre_clave_int'];
				$pres = $dato['pre_nombre'];
				?>
				<option value="<?php echo $clave; ?>" selected><?php echo $pres; ?></option>
				<?php } 
				?>
                  </select>
                </div>
             </div>
		     
		</form>
	<?php
	echo "<style onload=INICIALIZARLISTAS('MODAL')></style>";
		
}
else
if($opcion=="GUARDAR")
{
	$fecha=date("Y/m/d H:i:s");
		$nom = $_POST['nombre'];
		$usu = $_POST['usuario'];
		$con1 = encrypt($_POST['pass'],'p4v4svasquez');
		$per = $_POST['perfil'];		
		$ema = $_POST['email'];
		$act = $_POST['activo'];
		$cargo = $_POST['cargo'];
		$coordinador = $_POST['coordinador'];
		
		$sql = mysqli_query($conectar,"select * from usuario where (UPPER(usu_usuario) = UPPER('".$usu."') OR UPPER(usu_email) = UPPER('".$ema."')) and est_clave_int = 1");
		$dato = mysqli_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		if($act == '' || $act==NULL){ $swact = 0; }else{ $swact = 1; }
	
		if(STRTOUPPER($conusu) == STRTOUPPER($usu))
		{
			echo 'error1';
			//echo "<div style='color:maroon;text-align:center' align='center'>El usuario ingresado ya existe</div>";
		}
		else
		if(STRTOUPPER($conema) == STRTOUPPER($ema))
		{
			echo 'error2';
		    //echo "<div style='color:maroon;text-align:center' align='center'>El e-mail ingresado ya existe</div>";
		}
		else
		{
				$con = mysqli_query($conectar,"insert into usuario (usu_usuario,usu_clave,usu_nombre,prf_clave_int,est_clave_int,usu_email,usu_usu_actualiz,usu_fec_actualiz,car_clave_int,usu_coordinador) values('".$usu."','".$con1."','".$nom."','".$per."','".$swact."','".$ema."','".$usuario."','".$fecha."','".$cargo."','".$coordinador."')");				
				
				if($con > 0)
				{
					
					//asignarpermisos
					$idu = mysqli_insert_id($conectar);
					$insp = mysqli_query($conectar,"insert into permiso(usu_clave_int,ven_clave_int,per_metodo) select '".$idu."',ven_clave_int,1 from permisoperfil where prf_clave_int = '".$per."'");
					
 					echo 'ok';
					
					$destinatario = $ema; 
					$asunto = "Confirmación de Cuenta de Usuario Sistema Presupuesto Linea Global"; 
					$nombre = $nom;
					$cuerpo = ' 
					<html> 					
					<body> 
					<tables style="border-collapse:collapse" border="1">
					<tr><td style="text-align:center"><img src="http://www.pavas.co/presupuesto/dist/img/LOGOGLOBAL.jpg" alt=""/></td></tr>
					<tr>
					<td>
					<h1>'.$nombre.'</h1> 
					<p> 
					<b><strong>Bienvenido a Sistema de Presupuesto Digital</strong></b>.</p>
					<br>
				
					</p>
					<p> A continuacion tiene tus datos para iniciar sesión como usuario: </p>
					<p><strong>Datos de Cuenta</strong></p>
					<p><strong>Usuario:</strong> '.$usu.'</p>
					<p><strong>Contraseña:</strong> '.decrypt($con1,'p4v4svasquez').'</p>
					<p>Para Iniciar sesión de clic	<a href="https://www.pavas.com.co/presupuesto/">aquí</a>
					<br></p>
					<p>Este mensaje es generado automáticamente por SISTEMA PRESUPUESTO DIGITAL, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio <a href="http://www.lineaglobalingenieria.com">www.lineaglobalingenieria.com</a></p>
					</td>
					</tr>
					</table>
					</body> 
					</html> 
					'; 
					
					$sincontenido = $nombre."\n\n";					
					$sincontenido.= "Bienvenido a Sistema de Presupuesto Digital\n\n";
					$sincontenido.="A continuacion tiene tus datos para iniciar sesión como usuario:\n\n";
					$sincontenido.="Datos de Cuenta\n";
					$sincontenido.="Usuario: ".$usu."\n";
					$sincontenido.="Contraseña: ".decrypt($con1,'p4v4svasquez')."\n";
					$sincontenido.='Para Iniciar sesión de clic	<a href="https://www.pavas.com.co/presupuesto/">aquí</a>\n\n';
					$sincontenido.='Este mensaje es generado automáticamente por SISTEMA PRESUPUESTO DIGITAL, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio <a href="http://www.lineaglobalingenieria.com">www.lineaglobalingenieria.com</a>';
					
					//para el envion en formato htm
					//$headers = "MIME-Version: 1.0\r\n"; 
					//$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
					
					//direccion del remitente 
					//$headers = "From: admin@pavastecnologia.com\r\n";
					
					//direccion de respuesta, si queremos que sea distinta que la del remitente 
					//$headers .= "Reply-To: arojas@pavastecnologia.com\r\n"; 
					
					$mail = new PHPMailer();
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "Sistema de Presupuesto Digital - LINEA GLOBAL";
					$mail->AddReplyTo("adminpavas@pavas.com.co","LINEA GLOBAL");
					$mail->Subject = utf8_decode($asunto);
					$mail->AddAddress($destinatario, "Usuario ".$nombre);
					$mail->MsgHTML($cuerpo);
					$mail->AltBody = $sincontenido;// cuando no admite html
					if(!$mail->Send()) 
					{}else{}
					/*if(mail($destinatario,$asunto,$cuerpo,$headers))
					{
					//return true;
					//echo "Mensaje Enviado";
					}
					else
					{
					//return false;
					//echo 'Error al enviar el Mensaje';		
					}*/  				
				}
				else
				{
					echo 'error3';
					//echo "<div style='color:maroon;text-align:center' align='center'>No se han podido guardar los datos</div>";
				}
		}	
}
else
if($opcion == 'GUARDAREDICION')
{
		
		$fecha=date("Y/m/d H:i:s");
		$nom = $_POST['nombre'];
		$usu = $_POST['usuario'];
		$con1 = $_POST['pass'];
		$con1 = encrypt($con1,'p4v4svasquez');
		$per = $_POST['perfil'];		
		$ema = $_POST['email'];
		$act = $_POST['activo'];
		$cargo = $_POST['cargo'];
		$coordinador = $_POST['coordinador'];
		$edi = $_POST['edi'];
		if($act == '' || $act==NULL){ $swact = 0; }else{ $swact = 1; }
		$sql = mysqli_query($conectar,"select * from usuario where (UPPER(usu_usuario) = UPPER('".$usu."') OR UPPER(usu_email) = UPPER('".$ema."')) AND usu_clave_int <> '".$edi."' and est_clave_int=1");
		$dato = mysqli_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		
	
		if(STRTOUPPER($conusu) == STRTOUPPER($usu))
		{
			echo 'error1';//echo "<div style='color:maroon;text-align:center' align='center'>El usuario ingresado ya existe</div>";
		}
		else
		if(STRTOUPPER($conema) == STRTOUPPER($ema))
			{
				echo 'error2';//echo "<div style='color:maroon;text-align:center' align='center'>El e-mail ingresado ya existe</div>";
			}
			else
			{
				$con = mysqli_query($conectar,"update usuario set usu_usuario = '".$usu."', usu_clave = '".$con1."', usu_nombre = '".$nom."', prf_clave_int = '".$per."', est_clave_int = '".$swact."', usu_email = '".$ema."', usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."',car_clave_int = '".$cargo."',usu_coordinador ='".$coordinador."' where usu_clave_int = '".$edi."'");
				if($con > 0)
				{
					echo 'ok';//echo "<div style='color:green;text-align:center' align='center'>Datos grabados correctamente</div>";
				}
				else
				{
					echo 'error3';//"<div style='color:maroon;text-align:center' align='center'>No se han podido guardar los datos</div>";
				}
		}	
		
		
}
else
if($opcion == 'CARGARLISTAUSUARIOS')
{	
	?>
		<script src="js/jsusuarios.js"></script>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<div>
            <table id="tbusuarios" class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
        		<thead>
        			<tr>
        				<th class="dt-head-center" style="width:20px"></th>
                        <th class="dt-head-center" style="width:20px"></th>
                        <th class="dt-head-center">NOMBRE</th>
        				<th class="dt-head-center">USUARIO</th>
        				<th class="dt-head-center">PERFIL</th>
        				<th class="dt-head-center">E-MAIL</th>        				
        				<th class="dt-head-center">CREADO POR</th>
        				<th class="dt-head-center">FECHA</th>
        				<th class="dt-head-center">ESTADO</th>
                        <th class="dt-head-center"></th>
        				
        			</tr>
        		</thead>
                <tfoot>
        			<tr>
                        <th></th>
                        <th></th>
        				 <th class="dt-head-center">NOMBRE</th>
        				<th class="dt-head-center">USUARIO</th>
        				<th class="dt-head-center">PERFIL</th>
        				<th class="dt-head-center">E-MAIL</th>        				
        				<th class="dt-head-center">CREADO POR</th>
        				<th class="dt-head-center">FECHA</th>
        				<th class="dt-head-center">ESTADO</th>
                        <th class="dt-head-center"></th>
        				
        			</tr>
        		</tfoot>
        		<tbody>
        			<?php
						$contador=0;
						if(strtoupper($perfil)=="ADMINISTRADOR")
						{
						$query = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_usuario,prf_descripcion,u.est_clave_int est,est_nombre,usu_email,usu_usu_actualiz,usu_fec_actualiz from usuario u left outer join perfil prf ON prf.prf_clave_int = u.prf_clave_int join estados e on e.est_clave_int = u.est_clave_int where u.est_clave_int = 1 order by u.usu_nombre");
						}
						else if(strtoupper($perfil)=="COORDINADOR")
						{
						  $query = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_usuario,prf_descripcion,u.est_clave_int est,est_nombre,usu_email,usu_usu_actualiz,usu_fec_actualiz from usuario u left outer join perfil prf ON prf.prf_clave_int = u.prf_clave_int join estados e on e.est_clave_int = u.est_clave_int where u.est_clave_int = 1 and (usu_clave_int ='".$idUsuario."' or usu_coordinador = '".$idUsuario."') order by u.usu_nombre");
						}
						else
						{
							$query = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_usuario,prf_descripcion,u.est_clave_int est,est_nombre,usu_email,usu_usu_actualiz,usu_fec_actualiz from usuario u left outer join perfil prf ON prf.prf_clave_int = u.prf_clave_int join estados e on e.est_clave_int = u.est_clave_int where u.est_clave_int = 1 and (usu_clave_int ='".$idUsuario."') order by u.usu_nombre");
						}
						//$res = $con->query($query);
						$num_registros = mysqli_num_rows($query);

					
						for($i = 0; $i < $num_registros; $i++)
						{
							$dato = mysqli_fetch_array($query);
							$clausu = $dato['usu_clave_int'];
							$nom = $dato['usu_nombre'];
							$usu = $dato['usu_usuario'];
							$pernom = $dato['prf_descripcion'];
							$est = $dato['est'];
							$ema = $dato['usu_email'];
							$usuact = $dato['usu_usu_actualiz'];
							$fecact = $dato['usu_fec_actualiz'];
							$estnom = $dato['est_nombre'];
							
				   if($estnom=="Activo"){$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
				   else {$est='<span class="label label-info pull-right">'.$estnom.'</span>';}
							
					?>
        			<tr id="row_usu<?php echo $clausu;?>">
                      <td title="<?php echo $perfil;?>" ><?php if((strtoupper($perfil)=="ADMINISTRADOR")|| (strtoupper($perfil)=="COORDINADOR" and $clausu!=$idUsuario)){ ?><a class="btn btn-block btn-default btn-xs" onclick="CRUDUSUARIOS('EDITAR','<?php echo $clausu; ?>','')" style="cursor:pointer;width:20px; height:20px" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-pencil"></i></a><?PHP } ?></td>
                        <td><?php if((strtoupper($perfil)=="ADMINISTRADOR" || strtoupper($perfil)=="COORDINADOR")and $clausu!=$idUsuario){ ?><a class="btn btn-block btn-danger btn-xs" onClick="CRUDUSUARIOS('ELIMINAR',<?PHP echo $clausu;?>,'')" style="width:20px; height:20px"><i class="glyphicon glyphicon-trash" ></i></a><?php } ?></td>
        				<td data-title="Nombre"><?php echo $nom; ?></td>
        				<td data-title="Usuario"><?php echo $usu; ?></td>
        				<td data-title="Perfil" ><?php echo $pernom; ?></td>
        				<td data-title="E-mail" ><?php echo $ema; ?></td>
        				
        				<td data-title="Creado por" ><?php echo $usuact; ?></td>
        				<td data-title="Fecha" ><?php echo $fecact; ?></td>
        				<td data-title="Estado" ><?Php echo $est;?></td>
                        <td>
                        <?php if((strtoupper($perfil)=="ADMINISTRADOR") || (strtoupper($perfil)=="COORDINADOR" and $clausu!=$idUsuario)){ ?>
                        <a class='btn btn-xs btn-primary' data-toggle='modal' data-target='#myModal'  onclick="CRUDUSUARIOS('ASIGNARPERMISOS','<?php echo $clausu;?>','')" title='Asignar Permisos' >Permisos<i class='fa fa-expeditedssl'></i></a>
                        <?PHP } ?>
                        </td>
        				
        			</tr>
        			<?php
						}
					?>
        		</tbody>
        	</table>
        </div>
		<?php
}
else if($opcion=="ELIMINAR")
{
   $id = $_POST['id'];
   $update = mysqli_query($conectar,"update usuario set est_clave_int = 2 where usu_clave_int = '".$id."'");
   if($update>0)
   {
      echo 1;
   }
   else
   {
      echo 2;
   }
}
else if($opcion=="PASAR" || $opcion=="PASARTODOS")
{
	$idu = $_POST['idu'];
	$pre = $_POST['presupuesto'];
	$ins = mysqli_query($conectar,"insert into usuario_presupuesto(usu_clave_int,pre_clave_int,usp_usu_actualiz,usp_fec_actualiz) values('".$idu."','".$pre."','".$usuario."','".$fecha."')");
	if($ins>0)
	{
		echo 1;
	}
	else
	{
		echo 2;
	}
}
else if($opcion=="QUITAR" || $opcion=="QUITARTODOS")
{
	$idu = $_POST['idu'];
	$pre = $_POST['presupuesto'];
	$dele = mysqli_query($conectar,"delete from usuario_presupuesto where pre_clave_int = '".$pre."' and usu_clave_int = '".$idu."'");
	if($dele>0)
	{
		echo 1;
	}
	else
	{
		echo 2;
	}
}
else if($opcion=="REFRESCARAGREGAR")
{
	$idu = $_POST['idu'];
	$con = mysqli_query($conectar,"select * from presupuesto where pre_clave_int NOT IN (select pre_clave_int from usuario_presupuesto where usu_clave_int = '".$idu."') and est_clave_int in(0,5) order by pre_nombre");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$clave = $dato['pre_clave_int'];
		$pres = $dato['pre_nombre'];		
		$datos[]  = array("id"=>$clave,"literal"=>$pres);	
	}
	echo json_encode($datos);
}
else if($opcion=="REFRESCARAGREGADOS")
{
	$idu = $_POST['idu'];
	$sql = mysqli_query($conectar,"select * from usuario_presupuesto uo inner join presupuesto o on (o.pre_clave_int = uo.pre_clave_int) where uo.usu_clave_int = '".$idu."' and o.est_clave_int in(0,5) order by o.pre_nombre ASC");
	$num = mysqli_num_rows($sql);
	
	for($i = 0; $i < $num; $i++)
	{				
	$dato = mysqli_fetch_array($sql);
	$clave = $dato['pre_clave_int'];
	$pres = $dato['pre_nombre'];
	$datos[]  = array("id"=>$clave,"literal"=>$pres);		
	} 
	echo json_encode($datos);				
}
else if($opcion=="GUARDARCAMBIOCONTRASENA")
{
		$ant = $_POST['ant'];
		$nue = $_POST['nue'];
		$conf = $_POST['conf'];
		$capt = $_POST['captcha'];
		$imgcapt = $_SESSION['captcha'];
		$fecha=date("Y/m/d H:i:s");	
		$clave = $_COOKIE["clave"];
		$veria =  decrypt($clave,"p4v4svasquez");	

		if($veria <> $ant)
		{
			$res = 2;
			$msn = 'La contraseña anterior no es válida. Verificar';
		}
		else		
		if($nue <> $conf)
		{
			$res = 2;
			$msn =  'Las contraseñas no coinciden. Verificar';
		}
		else
		if($capt!=$imgcapt)
		{
			$res = 2;
			$msn =  'Los caracteres de verificación no coinciden con los de la imagen. Verificar';
		}
		else
		{
			$nue  = encrypt($nue,"p4v4svasquez");
			$sql = mysqli_query($conectar,"update usuario set usu_clave = '".$nue."',usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$idUsuario."'");
			
			if($sql >0)
			{
				unset($_COOKIE["clave"]);
				setcookie("clave",$nue, time() + (86400 * 30), "/");
				$res = 1;
				
				$msn =  'Contraseña Cambiada Correctamente';
			}
			else
			{
				$res = 2;
				$msn =  'No se han podido guardar los datos';
			}
		}
		$datos[] = array("res"=>$res,"msn"=>$msn,"cla"=>$veria);
		
		echo json_encode($datos);
		
}
else if($opcion=="VERICAPTCHA")
{
$capt = $_POST['captcha'];
$imgcapt = $_SESSION['captcha'];
   if($capt!=$imgcapt)
   {
      $res = 2;
   }
   else
   {
	  $res = 1;
   }
   $datos[] = array("res"=>$res,"capt"=>$imgcapt,"captn"=>$capt);
   echo json_encode($datos);
}
else if($opcion=="ASIGNARPERMISOS")
{
	$usu = $_POST['id'];
	?>
	<input type="hidden" id="idedicion" value="<?php echo $usu;?>"/>
	<input type="hidden" id="idtipopermiso" value="2"/>
	<div class="col-md-12">
		<div class="nav-tabs-custom">
	    <ul class="nav nav-tabs pull-left" id="nav2">
	    	<li class="active"> <a onclick="CRUDUSUARIOS('CARGARPERMISOS','2','')" data-toggle="tab" aria-expanded="true"><strong>SISTEMAS</strong></a> </li>
	    	<li class=""> <a onclick="CRUDUSUARIOS('CARGARPERMISOS','0','')" data-toggle="tab" aria-expanded="true"><strong>PRESUPUESTO</strong></a> </li>
	    	<li class=""> <a onclick="CRUDUSUARIOS('CARGARPERMISOS','1','')" data-toggle="tab" aria-expanded="true"><strong>CONTROL</strong></a> </li>
	    </ul>
		</div>
	</div>
	<div class="col-md-12" id="divpermisos">
	</div>
	<?PHP
	echo "<script>CRUDUSUARIOS('CARGARPERMISOS','2','');</script>";
}
else
if($opcion== 'CARGARPERMISOS')
{
	$usu = $_POST['idu'];
	$tip = $_POST['tip'];
	$con = mysqli_query($conectar,"select usu_nombre from perfil where usu_clave_int = '".$usu."'");
	$dato = mysqli_fetch_array($con);
	$nom = $dato['usu_nombre'];		
?>

<script src="js/jspermisos.js"></script>
<script src="js/jspermisos2.js"></script>

<div class="row">
<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h4 class="panel-title">SIN SELECCIONAR</h4>
		</div>
		<div class="panel-body">
			<div class="row" align="right">
				<div class="col-md-2">
				<input type="button" id="asignar" style="width:70px" class="btn btn-primary btn-xs"  value="Pasar &raquo;"></div>
				<div class="col-md-2">
				<input type="button" style="width:70px" id="asignartodos" class="btn btn-primary btn-xs"  value="Todos &raquo;">
				</div>
			</div>
			<table id="tbpermisos1"  class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px;">
			<thead>
			<tr>
			<th style="width:20px"></th>
			<th class="dt-head-center">VENTANA</th>

			</tr> </thead>
			<tbody>
			<?php
			$con = mysqli_query($conectar,"select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where usu_clave_int = '".$usu."')  and ven_tipo = '".$tip."' order by ven_opcion");
			//echo "select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where usu_clave_int = '".$usu."')  and ven_tipo = '".$tip."' order by ven_opcion";
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['ven_clave_int'];
				$ventana = $dato['ven_opcion'];
			?>
			<tr id="<?php echo $clave; ?>">
			<td>
			<a role="button" class="btn btn-block btn-info btn-xs" style="width:20px; height:20px" data-toogle="tooltip" title="Asignar" onClick="CRUDUSUARIOS('AGREGARPERMISO1','',<?PHP echo $clave;?>)"><i class="fa fa-plus"></i></a>
			</td>
			<td><?php echo $ventana; ?></td>

			</tr>
			<?php
			}
			?>
			</tbody>
			<tfoot><tr>
			<th></th>
			<th></th>
			</tr>
			</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="col-md-6">
    <div class="panel panel-default">
	  	<div class="panel-heading">
	    	<h4 class="panel-title">SELECCIONADOS</h4>
	    </div>
	    <div class="panel-body">
		    <div class="row" align="left">
			    <div class="col-md-2">
			    <input type="button" style="width:70px" class="btn btn-danger btn-xs" value="&laquo; Quitar" id="quitar"></div>
			    <div class="col-md-2">
			<input type="button" style="width:70px"  class="btn btn-danger btn-xs" value="&laquo; Todos" id="quitartodos"></div>
		    </div>
	    
	        <table id="tbpermisos2" class="table table-bordered table-condensed compact" style="font-size:<?php echo $fontsize;?>px">
	        <thead>
	        <tr>
	        <th style="width:20px"></th>
	        <th class="dt-head-center">VENTANA</th>
	        <th class="dt-head-center">PERMISO</th></tr>
	        </thead>
	        <tbody>
	        <?Php
				$con = mysqli_query($conectar,"select p.per_clave_int cla,v.ven_opcion ven, p.per_metodo met from permiso p inner join ventana v on (v.ven_clave_int = p.ven_clave_int)  where p.usu_clave_int = '".$usu."' and v.ven_tipo = '".$tip."'");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$claven = $dato['cla'];
					$ven = $dato['ven'];
					$met = $dato['met'];
			?>
				<tr id="<?PHP echo $claven;?>"><th>
	            <a role="button" class="btn btn-block btn-danger btn-xs" style="cursor:pointer; width:20px; height:20px" onClick="CRUDUSUARIOS('ELIMINARPERMISO1','',<?PHP echo $claven?>)"><i class="fa fa-trash"></i></a>
	            </th><th><?php echo $ven;?></th>
	            <th>
					<select class="form-control input-sm"  id="metodo<?php echo $i; ?>" onchange="CRUDUSUARIOS('MODIFICARPERMISO',this.value,<?PHP echo $claven?>)">
					<option value="0" <?php if($met == 0){ echo "selected='selected'"; } ?>>Consulta</option>
					<option value="1" <?php if($met == 1){ echo "selected='selected'"; } ?>>Modificación</option>
					<?PHP
					if($ven=="ACTIVIDADES")
					{
					?>
					<option value="2" <?php if($met == 2){ echo "selected='selected'"; } ?>>Modificacion x aprobar</option>
					<?PHP
					}
					?>
					</select>
				</th>                
	           </tr>
			<?php
				}
			?>
	        </tbody>
	        <tfoot><tr>
	        	<th></th>
	        	<th></th>
	        	<th></th>
	        </tr>
	        </tfoot>
	        </table>
          </div>
        </div>
	</div>
</div>
<?php	
}
else
if($opcion=="AGREGARPERMISO")
{
   $usu = $_POST['idp'];
   $ventana = $_POST['ventana'];
   $ins = mysqli_query($conectar,"insert into permiso (usu_clave_int,ven_clave_int,per_metodo) values('".$usu."','".$ventana."',1)");
   if($ins>0)
   {
	   echo 'ok';
   }
   else
   {
      echo 'error';
   }
}
else
if($opcion=="ELIMINARPERMISO")
{
   $iddetalle = $_POST['id'];
   $delete = mysqli_query($conectar,"delete from permiso where per_clave_int = '".$iddetalle."'");
   if($delete>0)
   {
	  echo 'ok';
   }
   else
   {
       echo 'error';
   }
}
else
if($opcion=="MODIFICARPERMISO")
{
    $metodo = $_POST['metodo'];
	$ventana = $_POST['ventana'];
	$upd = mysqli_query($conectar,"update permiso set per_metodo ='".$metodo."' where per_clave_int = '".$ventana."'");
	if($upd>0)
	{
		echo 'ok';
	}
	else
	{
	    echo 'error';
	}
}
else
if($opcion == 'AGREGARTODOSP')
{
	$usu = $_POST['id'];
	$tip = $_POST['tip'];		
	$con = mysqli_query($conectar,"insert into permiso select null,'".$usu."',ven_clave_int,1 from ventana where ven_clave_int not in (select ven_clave_int from permiso where usu_clave_int = '".$usu."') and ven_tipo = '".$tip."'");
	if($con>0)
	{
	  echo 'ok';
	}
	else
	{
	  echo 'error';
	}

}
else
if($opcion=="ELIMINARTODOSP")
{
	$usu = $_POST['id'];
	$tip = $_POST['tip'];	
	$con = mysqli_query($conectar,"delete from permiso where usu_clave_int = '".$usu."' and ven_clave_int in(SELECT ven_clave_int FROM ventana WHERE ven_tipo ='".$tip."')");
	if($con>0)
	{
	  echo 'ok';
	}
	else
	{
	  echo 'error';
	}
}
else if($opcion=="CAMBIOCONTRASENA")
{
	?>
	<div class="row">
		<div class="col-md-4">
		 <span id="msn"></span>
             <div class="row">
                    <div class="col-md-12"><strong>Contraseña Anterior:</strong>
					<input name="anterior" id="anterior" type="password" class="form-control input-sm" data-clear-btn="true">
                    </div>
                   
                    <div class="col-md-12"><strong>Nueva Contraseña:</strong>
					<input name="nueva" id="nueva" type="password" class="form-control input-sm" data-clear-btn="true">
                	</div>
               
                    <div class="col-md-12"><strong>Confirmar Contraseña:</strong>
					<input name="confirmar" id="confirmar" type="password" class="form-control input-sm" data-clear-btn="true">
                    </div>
               
                <div class="col-md-12">
	                <div class="input-group">                
	                <img src="modulos/cambiarcontrasena/captcha.php" id="captcha" style="width:100%"/>
	                <span class="input-group-addon"><a class="btn btn-success" onClick="CRUDCUENTA('REFRESCARCAPTCHA','')"><i class="glyphicon glyphicon-refresh"></i></a></span>
	                </div>
               </div>
               <div class="col-md-12">
                  <strong>Ingresa la palabra:</strong>
                  <input type="text" name="vericaptcha" id="vericaptcha" class="form-control input-sm"  onChange="CRUDCUENTA('VERICAPTCHA','','')">
                  </div>              
                 
               </div>
           
             
                <div class="row">
                    <div class="col-md-12">
                    	<hr>
					<button type="button" id="btncambio"  class="btn btn-primary btn-block" onclick="CRUDCUENTA('GUARDARCAMBIOCONTRASENA','')">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
                
	<?php	
}
else if($opcion=="AJUSTECUENTA")
{
	$con = mysqli_query($conectar,"SELECT usu_usuario,usu_nombre,usu_email,usu_imagen,usu_tam_fuente FROM usuario WHERE usu_clave_int= '".$idUsuario."' limit 1");
	$dat = mysqli_fetch_array($con);
	$nom = $dat['usu_nombre'];
	$ema = $dat['usu_email'];
	$img = $dat['usu_imagen'];
	$tam  = $dat['usu_tam_fuente'];
	clearstatcache();
	if($img=="" || $img==NULL){
		$img = "dist/img/default-user.png";
	}
	
	?>
	<div class="row">
		<aside class="col-md-3">
          <div class="box box-success">
              <div class="box-body box-profile">
                <input type="file" id="imgcuenta" name="imgcuenta" data-allowed-file-extensions="png jpg gif jpge" class="dropify profile-user-img img-responsive img-circle" data-default-file="<?php echo $img;?>" data-height="128"  data-min-width="128"  data-show-remove="false" onChange="setpreview('rutaimagen','imgcuenta','formcuenta')">
                <span id="rutaimagen"></span>

                <h3 class="profile-username text-center"><?php echo $usuario;?></h3>               

                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <button id="btnguardarajuste" type="button" class="btn btn-primary btn-block" onclick="CRUDCUENTA('GUARDARAJUSTE','<?php echo $idUsuario;?>')">Guardar Cambios</button>
                  </li>               
                 
                </ul>

                
              </div>
              <!-- /.box-body -->
            </div>
          
        </aside>
        <div class="col-md-4">
        	<div class="row">
        		<div class="col-md-12">
        			<label>Nombre:</label>
        			<input type="text" id="txtnombre" name="txtnombre" class="form-control" value="<?php echo $nom;?>">
        		</div>
        		<div class="col-md-12">
        			<label>Correo electrónico:</label>
        			<input disabled type="text" id="txtcorreo" name="txtcorreo" class="form-control" value="<?php echo $ema;?>" >
        		</div>
        		<div class="col-md-12">
        			<label>Tamaño de Fuente:</label>
        			 <div class="input-group">
                      <input class="form-control" type="number" min="11" max="24" id="txttamanofuente" onchange="cambiartamano(this.value)" value="<?php echo $tam;?>">
                      <span style="font-size:<?php echo $tam;?>px" class="input-group-addon" id="tamfuente">A</span>
                  </div>
        		</div>
        	</div>
        </div>
	</div>
	<script>
      	$(document).ready(function(e) {
      		
      		$('#txtnombre,#txtcorreo').on('keyup change',function(){
      			var old = $(this).attr('data-old');
      			var act = $(this).val();
      			var type = $(this).attr('type');
      			
      			var btnguardar = $('#btnguardarajuste');
      			
      			if(type=="email" && act!="" && (act.indexOf('@', 0) == -1 || act.indexOf('.', 0) == -1) )
      			{
      				btnguardar.prop('disabled', true);
      			}
      			else if(act!=old)
      			{
      				
      				btnguardar.prop('disabled', false);
      			}	
      			else
      			{
      				console.log(old);
      				btnguardar.prop('disabled', true);
      			}
      		});
      	});
      </script>
	<?PHP
	echo "<script>INICIALIZARLISTAS('');</script>";
}
else if($opcion=="GUARDARAJUSTE")
{
	$id = $_POST['id'];
	$nom = $_POST['nom'];
	
	$ruta = $_POST['rut'];
	$trozos = explode(".", $ruta); 
	$rutaold = "../".$ruta;
	$extension = end($trozos);
	$rutan = "../modulos/usuarios/fotosUsuarios/".$id.".".$extension;

	$sql = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$id."' limit 1");
	$dato = mysqli_fetch_array($sql);
	$rutaa = $dato['usu_imagen'];

	/*if($fec!="" and CalculaEdad($fec)<18)
	{
		$res = "error";
		$msn = "Debe"
	}
	else
	{*/
		$upd = mysqli_query($conectar,"UPDATE usuario SET usu_nombre = '".$nom."',usu_fec_actualiz='".$fecha."',usu_usu_actualiz='".$usuario."' where usu_clave_int='".$id."'");
		if($upd>0)
		{
			
			if(rename($rutaold,$rutan) and $rutaa!=$ruta and $ruta!="")
			{ 
				$rutanew = "modulos/usuarios/fotosUsuarios/".$id.".".$extension;
				$update = mysqli_query($conectar,"update usuario set usu_imagen='".$rutanew."' where usu_clave_int='".$id."'");
			}
			$res = "ok";
			$msn = "Haz actualizado la Información de tu cuenta correctamente";

		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al actualizar tu cuenta";
		}
		clearstatcache();

		if($ruta=="")
		{
			$rutanew = "";
		}
		else
		{
			$rutanew = $rutanew."?".time();
		}
		$datos[] = array("res"=>$res,"msn"=>$msn,"img"=>$rutanew);
		echo json_encode($datos);
	//}


}
else if($opcion=="IRACONTROL")
{
	 setcookie("sistema","control", time() + (86400 * 30), "/");//1 dia
	 $datos[] = array("url"=>"ControlPresupuesto");
	 echo json_encode($datos);
}
else if($opcion=="IRAPRESUPUESTO")
{
	 setcookie("sistema","presupuesto", time() + (86400 * 30), "/");//1 dia
	 $datos[] = array("url"=>"Presupuesto");
	 echo json_encode($datos);
}
else if($opcion=="GUARDARFUENTE")
{
	$tam = $_POST['tam'];
	$upd = mysqli_query($conectar, "UPDATE usuario SET usu_tam_fuente = '".$tam."' WHERE usu_clave_int = '".$idUsuario."'");
	if($upd>0)
	{
		$res = "ok";
		$msn = "Tamaño de fuente modificado";
	}
	else
	{
		$res = "error";
		$msn = "Surgió un error al modificar el tamaño del texto";
	}
	$datos[] = array("res"=>$res, "msn"=>$msn);
	echo json_encode($datos);
}
?>
