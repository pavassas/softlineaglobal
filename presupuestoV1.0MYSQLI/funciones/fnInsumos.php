
<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
  	$opcion = $_POST['opcion'];

  $con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
  $dato = mysqli_fetch_array($con);
  $perfil = strtoupper($dato['prf_descripcion']);
  $percla = $dato['prf_clave_int'];
  $coordi = $dato['usu_coordinador'];
  $fontsize = $dato['usu_tam_fuente'];
  $cla = $dato['usu_clave_int'];
  $claveusuario = $dato['usu_clave_int'];
  $nombreusuario = $dato['usu_nombre'];
	//CONSULTA DE UNIDADES CREADAS POR EL USUARIO LOGUEADO
	//CONSULTA DE UNIDADES CREADAS POR ESE USUARIO
	$unidades = 0;
	$con = mysqli_query($conectar,"select uni_clave_int from unidades where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $uni = $dat['uni_clave_int'];
		   $idsu[] = $uni;
	   }
	   $unidades = implode(',',$idsu);
	}
	$insumos = 0;
	$con = mysqli_query($conectar,"select ins_clave_int from insumos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['ins_clave_int'];
		   $idsu[] = $ins;
	   }
	   $insumos = implode(',',$idsu);
	}
    //LIVERA LOS INGRESO A TAL PRESUPUESTO
  $update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");
  
	if(strtoupper($perfil)=="ADMINISTRADOR"){ $estado=1;}else{$estado=3;}
  	if($opcion=="NUEVO")
  	{
  	 ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
      
       <div class="form-group">
         <div class="col-md-6"><strong>Tipo de Recurso:</strong>
         <div class="ui corner labeled input">
         <select name="seltipoinsumo"id="seltipoinsumo" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpi_clave_int,tpi_nombre from tipoinsumos where est_clave_int = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpi_clave_int'];
			$nom = $dat['tpi_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-6"><strong>Nombre:</strong>
         <div class="ui corner labeled input">
         <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre"> <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        
         </div>
    </div>
   <div class="form-group">
         <div class="col-md-6"><strong>Unidad de medida:</strong>
         <div class="ui corner labeled input">
         <select name="selunidad" id="selunidad" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int in(".$unidades.")");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-6"><strong>Valor Unitario:</strong>
         <div class="ui corner labeled input">
         <input  name="txtvalor" id="txtvalor" class="form-control input-sm" type="number" step="0.1" placeholder="Ingrese valor"  onKeyPress="return NumCheck(event, this)">
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        
         </div>
    </div>
    <div class="form-group">
    
    	<div class="col-md-12"><strong>Descripción:</strong>
      
         <textarea id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" placeholder="Escribe aqui la descripcion del material"></textarea>
          
    	</div>
    </div>
  </form>
  <?PHP  
  }
else
  if($opcion=="EDITAR" )
  {
	  $id = $_POST['id'];
	   $coninfo = mysqli_query($conectar,"select tpi_clave_int tpi,ins_nombre nom,uni_clave_int idu,ins_valor val,ins_descripcion des,est_clave_int as est from insumos   where ins_clave_int = '".$id."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $tpi = $datinfo['tpi'];
	  $nom = $datinfo['nom'];
	  $uni = $datinfo['idu'];
	  $val = $datinfo['val'];
	  $des = $datinfo['des'];
	  $est = $datinfo['est'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
   <div class="form-group">
         <div class="col-md-6"><strong>Tipo de Recurso:</strong>
         <div class="ui corner labeled input">
         <select name="seltipoinsumo"id="seltipoinsumo" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpi_clave_int,tpi_nombre from tipoinsumos where est_clave_int = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpi_clave_int'];
			$nomt = $dat['tpi_nombre'];
			?>
            <option <?php if($tpi==$id){echo 'selected';}?> value="<?php echo $id;?>"><?php echo $nomt;?></option>
            <?Php	
		 }
		 ?>
         </select>
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-6"><strong>Nombre:</strong>
         <div class="ui corner labeled input">
         <input  name="txtnombre" id="txtnombre" class="form-control input-sm" value="<?php echo $nom;?>" type="text" autocomplete="off" placeholder="Ingrese nombre">
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        
         </div>
    </div>
   <div class="form-group">
         <div class="col-md-6"><strong>Unidad de medida:</strong>
         <div class="ui corner labeled input">
         <select name="selunidad" id="selunidad" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int in(".$unidades.")");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option <?php if($uni==$idu){echo 'selected';}?> value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
         </select>
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-6"><strong>Valor Unitario:</strong>
         <div class="ui corner labeled input">
         <input  name="txtvalor" id="txtvalor" class="form-control input-sm" type="number" step="0.1" value="<?php echo $val;?>"  onKeyPress="return NumCheck(event, this)" placeholder="Ingrese valor">
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        
         </div>
    </div>
    <div class="form-group">
    
    	<div class="col-md-12"><strong>Descripción:</strong>
        
         <textarea id="txtdescripcion"  name="txtdescripcion" class="form-control input-sm" placeholder="Escribe aqui la descripcion del material">
         <?php echo $des;?>
         </textarea>
    	</div>
    </div>
  </form>
  <?PHP  
  }
   else if($opcion=="GUARDAR")
  {
     $tipoinsumo = $_POST['tipoinsumo'];
	 $nombre  = $_POST['nombre'];
	 $unidad = $_POST['unidad'];
	 $valor = $_POST['valor'];
	 $descripcion = $_POST['descripcion'];
	 $veri = mysqli_query($conectar,"select * from insumos where UPPER(ins_nombre) = UPPER('".$nombre."') and tpi_clave_int = '".$tipoinsumo."' and uni_clave_int = '".$unidad."' and ins_valor = '".$valor."' and  (est_clave_int=1 or ins_clave_int in(".$insumos."))");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $ins = mysqli_query($conectar,"insert into insumos(ins_nombre,tpi_clave_int,uni_clave_int,ins_valor,ins_descripcion,est_clave_int,ins_usu_actualiz,ins_fec_actualiz,usu_clave_int) values('".$nombre."','".$tipoinsumo."','".$unidad."','".$valor."','".$descripcion."','".$estado."','".$usuario."','".$fecha."','".$idUsuario."')");
		if($ins>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
 
   else if($opcion=="GUARDAREDICION" )
  {
	 $ide =$_POST['id'];
    $tipoinsumo = $_POST['tipoinsumo'];
	 $nombre  = $_POST['nombre'];
	 $unidad = $_POST['unidad'];
	 $valor = $_POST['valor'];
	 $descripcion = $_POST['descripcion'];	 
	 $veri = mysqli_query($conectar,"select * from insumos where UPPER(uni_nombre) = UPPER('".$nombre."') and tpi_clave_int  = '".$tipoinsumo."' and ins_clave_int!='".$ide."' and (est_clave_int=1 or ins_clave_int in(".$insumos."))");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update insumos set ins_nombre = '".$nombre."',tpi_clave_int='".$tipoinsumo."',uni_clave_int='".$unidad."',ins_descripcion = '".$descripcion."',ins_valor = '".$valor."',est_clave_int='".$estado."',ins_usu_actualiz='".$usuario."',ins_fec_actualiz='".$fecha."' where ins_clave_int = '".$ide."'");
		if($upd>0)
		{
			if($valor>0)
			{
			   $updi = mysqli_query($conectar,"update actividadinsumos set ati_valor = '".$valor."',ati_usu_actualiz ='".$usuario."',ati_fec_actualiz = '".$fecha."' where ins_clave_int = '".$ide."'");
			}
			else
			{
			
			}
		   echo 1;
		}
		else
	    {
		  	echo 3;
		}	
	 }	 
  }
    else if($opcion=="CARGARLISTAINSUMOS")
  {
	  ?> <script src="js/jsinsumos.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div>
      <table id="tbinsumos" class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px" width="100%">
                <thead>
                <tr>
                <th class="dt-head-center" style="width:20px"></th>
                <th class="dt-head-center" style="width:20px"></th>
                <th class="dt-head-center" style="width:20px">CODIGO</th>
                <th class="dt-head-center">TIPO RECURSO</th>
                <th class="dt-head-center">TIPOLOGÍA</th>
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center" style="width:40px">UNIDAD</th>
                <th class="dt-head-center" style="width:100px">VR.UNITARIO</th>
                <th class="dt-head-center">DESCRIPCIÓN</th>
                <th class="dt-head-center">CIUDAD</th>
                <th class="dt-head-center">TIPO PROYECTO</th>
                <th class="dt-head-center" style="width:40px">ESTADO</th>
                </tr>
                </thead>
             
              
                 <tfoot>
                <tr>
                <th></th>
                <th></th>
                <th>CODIGO</th>
                <th>TIPO RECURSO</th>
                <th>TIPOLOGÍA</th>
                <th>NOMBRE</th>
                <th>UNIDAD</th>
                <th>VR.UNITARIO</th>
                <th>DESCRIPCIÓN</th>
                <th class="dt-head-center">CIUDAD</th>
                <th class="dt-head-center">TIPO PROYECTO</th>
                <th>ESTADO</th>
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP

  }
   else if($opcion=="ELIMINAR")
  {
    $id = $_POST['id'];
	$update = mysqli_query($conectar,"update insumos set est_clave_int = 2 where ins_clave_int = '".$id."'");
	if($update>0){  echo 1;}else {echo 2;}
  }
  else if($opcion=="UNIFICAR")
  {
	  
	 ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
    <div class="row">
        <div class="col-md-6">
        <strong>Recursos a Unificar:</strong>
        <select name="insumounificar" id="insumounificar" class="form-control input-sm selectpicker" multiple="multiple" data-live-search="true" data-live-search-placeholder="Buscar Recurso" data-actions-box="true" style="width:100%">
		<?php 
        $con = mysqli_query($conectar,"select distinct ins_clave_int, ins_nombre,ins_valor,u.uni_codigo as cod from insumos i join unidades u on u.uni_clave_int = i.uni_clave_int where i.est_clave_int = 1 or ins_clave_int in(".$insumos.") order by ins_nombre");
        while($dat = mysqli_fetch_array($con))
        {
			$idi = $dat['ins_clave_int'];
			$nomi = $dat['ins_nombre'];
			$valo = $dat['ins_valor'];
			 ?>
			<option value="<?Php echo $idi;?>"><?php echo $idi."-".$nomi."(".$valo.")";?></option>
			<?php
        }
        ?>
	
        </select>
        </div>
        <div class="col-md-6">
         <strong>Recurso Base:</strong>
        <select name="insumopor" id="insumopor" class="form-control input-sm selectpicker" data-live-search="true" data-live-search-placeholder="Buscar Tecnico" data-actions-box="true" style="width:100%">
      <?php 
        $con = mysqli_query($conectar,"select distinct ins_clave_int, ins_nombre,ins_valor from insumos i join unidades u on u.uni_clave_int = i.uni_clave_int where i.est_clave_int = 1 or ins_clave_int in(".$insumos.") order by ins_nombre");
        while($dat = mysqli_fetch_array($con))
        {
			$idi = $dat['ins_clave_int'];
			$nomi = $dat['ins_nombre'];
			$valo = $dat['ins_valor'];
			 ?>
			<option value="<?Php echo $idi;?>"><?php echo $idi."-".$nomi."(".$valo.")";?></option>
			<?php
        }
        ?>
        </select>
        </div>
    </div>
    </form>
      
      <?php
	  echo "<style onload=INICIALIZARLISTAS('MODAL')></style>";
  
  }
  else
  if($opcion=="GUARDARUNIFICAR")
  {
  $uniu = $_POST['uniu']; $uniu = implode(', ', (array)$uniu);
  $unip = $_POST['unip'];//insumo base
  
  $conu = mysqli_query($conectar,"select ins_clave_int, ins_nombre from insumos where ins_clave_int in(".$uniu.")");
  $cantu = mysqli_num_rows($conu);
  if($cantu>0)
  {
	  for($k=0;$k<$cantu;$k++)
	  {
		  $datu = mysqli_fetch_array($conu);
		  $idu = $datu['ins_clave_int'];
      if($idu>0)
      {
  		  $update1 = mysqli_query($conectar,"update actividadinsumos set ins_clave_int = '".$unip."' where ins_clave_int ='".$idu."'");
  		  $update2 = mysqli_query($conectar,"update insumospendientes set ins_clave_int = '".$unip."' where ins_clave_int ='".$idu."'");
  		  $update3 = mysqli_query($conectar,"update pre_gru_cap_act_insumo set ins_clave_int = '".$unip."' where ins_clave_int ='".$idu."'");
  		  $update4 = mysqli_query($conectar,"update pre_gru_cap_act_sub_insumo set ins_clave_int = '".$unip."' where ins_clave_int ='".$idu."'");
  		  $update5 = mysqli_query($conectar,"update inspendientespresupuesto set ins_clave_int = '".$unip."' where ins_clave_int ='".$idu."'");
      }
		  if($idu!=$unip and $idu>0)
		  {
		  $update4 = mysqli_query($conectar,"update insumos set est_clave_int = 2 where ins_clave_int = '".$idu."'");
		  }
	  }
	  $res = 1;
	  $msn = 'Recursos Unificados correctamente';
  }
  else
  {
	 $res = 2;
	$msn = 'No ha seleccionado ningun recurso a unificar';
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}

?>