﻿<?php
include ("../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);

// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario = $_COOKIE["usIdentificacion"];
//$clave= $_COOKIE["clave"];	
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$grupos = 0;
	$con = mysqli_query($conectar,"select gru_clave_int from grupos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['gru_clave_int'];
		   $idsu[] = $ins;
	   }
	   $grupos = implode(',',$idsu);
	}


$opcion = $_POST['opcion'];
if($opcion=="ESTADOSCONTRATOS")
{
	$id = $_POST['id'];	
  ?>
   <script src="js/jsestadoscontrato.js"></script>  
   <span id="msn1"></span>
    <div class="row">
        <div class="col-md-3">
            <div class="btn btn-block btn-info btn-xs" style="width:180px"><a style="color: white" title="Descargar Informe" href="modulos/presupuesto/estadocontratoexportarcontrol.php?edi=<?php echo $id;?>" target="_blank"><i class="fa fa-file-excel-o"></i>Descargar Informe Estado Contrato</a></div>
        </div>

    </div>
   <div class="table-responsive">
  <table id="tbEstadoscontrato" border="0" cellspacing="2" class=" table table-bordered table-condensed compact table-hover" style="font-size:11px">
  <thead class="">
    <tr>
      <th rowspan="4" style="background-color:rgba(215,215,215,1.00) " class="dt-head-center"></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>ORDEN COMPRA</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>N° CONT</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>GRUPO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>CONTRATISTA</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>NIT</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>OBJETO DEL CONTRATO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR INICIAL CONTRATO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>IVA</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR OTRO SI</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>IVA DEL OTRO SI</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong><span title="VALOR INICIAL +  IVA + VALOR OTRO SI + IVA DEL OTRO SI">VALOR FINAL</span></strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>ANTICIPO PAGADO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>ANTICIPO AMORTIZADO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>SALDO DEL ANTICIPO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR RETENCION EN GARANTIA</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>OTRAS DEDUCCIONES</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR NETO PAGADO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR FACTURADO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR IVA FACTURADO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR TOTAL FACTURADO</strong></th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>SALDO DEL CONTRATO</strong></th>
      <th colspan="2" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>CONTRATO</strong></th>
      <th colspan="20" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>POLIZAS</strong> </th>
      <th rowspan="4" style="width:100px;background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center" ><strong>FECHA DE ACTA LIQUIDACIÓN</strong></th>
    </tr>
    <tr>
      <th rowspan="3" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FECHA DE INICIO</strong></th>
      <th rowspan="3" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FECHA DE TERMINACION</strong></th>
      <th colspan="4" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>CUMPLIMIENTO</strong></th>
      <th colspan="4" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>SALARIOS Y PRESTACIONES</strong></th>
      <th colspan="4" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>BUEN MANEJO ANTICIPO</strong></th>
      <th colspan="4" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>RESPONSABILIDAD CIVIL</strong></th>
      <th colspan="4" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>ESTABILIDAD Y CALIDAD</strong></th>
    </tr>
    <tr>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>% ASEGURADO</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR ASEGURADO</strong></th>
      <th colspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FECHAS</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>% ASEGURADO</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR ASEGURADO</strong></th>
      <th colspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FECHAS</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>% ASEGURADO</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR ASEGURADO</strong></th>
      <th colspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FECHAS</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>% ASEGURADO</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR ASEGURADO</strong></th>
      <th colspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FECHAS</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>% ASEGURADO</strong></th>
      <th rowspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>VALOR ASEGURADO</strong></th>
      <th colspan="2" style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FECHAS</strong></th>
    </tr>
    <tr>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>INICIO</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FIN</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>INICIO</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FIN</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>INICIO</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FIN</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>INICIO</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FIN</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>INICIO</strong></th>
      <th style="background-color:rgba(215,215,215,1.00);vertical-align:middle" class="dt-head-center"><strong>FIN</strong></th>
    </tr>
    </thead>
    <tbody>
    <?php
	/*
	$con = mysqli_query($conectar,"select * from estados_contrato where pre_clave_int = '".$id."'");
    while($dat = mysqli_fetch_array($con))
	{
		$ide = $dat['esc_clave_int'];
		$ordc = $dat['esc_orden'];
		$gru = $dat['gru_clave_int'];
		$cont = $dat['esc_contrato'];
        $contra = $dat['esc_contratista'];
		$nit = $dat['esc_nit'];
		$obje = $dat['esc_obj_contrato'];
		$vali = $dat['esc_val_inicial'];
		$iv = $dat['esc_iva'];
		$valos = $dat['esc_vr_otrosi'];
		$ivos = $dat['esc_iva_otrosi'];
		$dedu = $dat['esc_deducciones'];
		$fei = $dat['esc_fec_inicio']; if($fei=="0000-00-00"){$fei="";}
		$fef = $dat['esc_fec_fin']; if($fei=="0000-00-00"){$fei="";}
		
		$aseguc = $dat['esc_cum_asegu'];
		$valorc = $dat['esc_cum_valor'];
		$inicioc = $dat['esc_cum_inicio']; if($inicioc=="0000-00-00"){$inicioc="";}
		$finc = $dat['esc_cum_fin']; if($finc=="0000-00-00"){$finc="";}
		
		$asegus = $dat['esc_sal_asegu'];
		$valors = $dat['esc_sal_valor'];
		$inicios = $dat['esc_sal_inicio']; if($inicios=="0000-00-00"){$inicios="";}
		$fins = $dat['esc_sal_fin']; if($fins=="0000-00-00"){$fins="";}
		
		$asegub = $dat['esc_bue_asegu'];
		$valorb = $dat['esc_bue_valor'];
		$iniciob = $dat['esc_bue_inicio']; if($iniciob=="0000-00-00"){$iniciob="";}
		$finb = $dat['esc_bue_fin']; if($finb=="0000-00-00"){$finb="";}
		
		$asegur = $dat['esc_res_asegu'];
		$valorr = $dat['esc_res_valor'];
		$inicior = $dat['esc_res_inicio']; if($inicior=="0000-00-00"){$inicior="";}
		$finr = $dat['esc_res_fin']; if($finr=="0000-00-00"){$finr="";}
		
		$asegue = $dat['esc_est_asegu'];
		$valore = $dat['esc_est_valor'];
		$inicioe = $dat['esc_est_inicio']; if($inicioe=="0000-00-00"){$inicioe="";}
		$fine = $dat['esc_est_fin']; if($fine=="0000-00-00"){$fine="";}
		
		$fechal = $dat['esc_fec_acta']; if($fechal=="0000-00-00"){$fechal="";}
		
		if($nit!="" and $gru>0)
		{
		   $condatos = mysqli_query($conectar,"select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo,sum(cpe_iva) iv,sum(cpe_ret_garantia) gar,sum(cpe_valor_neto) net from control_egreso where pre_clave_int = '".$id."'  and (cpe_documento = '".$nit."' and UPPER(cpe_beneficiario) = UPPER('".$contra."-".$cont."'))");//and gru_clave_int = '".$gru."'
		   $dato = mysqli_fetch_array($condatos);
		   $ant = $dato['ant'];
		   $amo = $dato['amo'];
		   $iva = $dato['iv'];
		   $ret = $dato['gar'];
		   $net = $dato['net'];
		   $bru = $net + $iva;
		   $sala = $ant - $amo;
		}
		else
		{
		   $ant = 0;
		   $amo = 0;
		   $iva = 0;
		   $ret = 0;
		   $net = 0;
		   $bru = 0;
		   $sala = 0;
		}
		
		$vf  = $vali + $iv + $valos + $ivos;
		$nett = $bru - $amo - $ret - $dedu;
		$salc = $vf - $bru;
		
	?>
    <tr style="font-size:11px; height:12px" id="<?php echo $ide;?>">     
      <td><a data-toggle="tooltip" title="Eliminar Contrato" class="btn btn-block btn-danger btn-xs" style="width:20px; height:20px" onClick="CONTROLESTADOS('ELIMINAR','<?PHP echo $ide;?>')"><i class="glyphicon glyphicon-trash"></i></a></td>
      <td><input onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  type="text" name="ordc<?php echo $ide;?>" id="ordc<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $ordc;?>"/></td>
      <td><input onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  type="text" name="cont<?php echo $ide;?>" id="cont<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $cont;?>"/></td>
      <td>
      
      <select  onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" id="grue<?php echo $ide;?>" style="background-color:transparent; border:thin; width:80px; cursor:pointer"    data-width="80px" class="form-control input-sm selectpicker">
      <option value=""></option>
    <?PHP //onmouseover="inicializarid(this.id)"
	$cong = mysqli_query($conectar,"select distinct g.gru_clave_int gru, g.gru_nombre nog from grupos g where est_clave_int = 1 or gru_clave_int in(".$grupos.") order by g.gru_nombre");
    $maxg = 0;
	while($datg = mysqli_fetch_array($cong))
    {
    $idg = $datg['gru'];
    $nom = $datg['nog'];
	if($idg>$maxg){$maxg=$idg;}
    ?>
    <option title="<?php echo $idg;?>" <?Php if($idg==$gru){ echo 'selected';}?>   value="<?php echo $idg;?>"><?php echo $nom;?></option>
    <?php					  
    }
    ?>
    </select>
  
    </td><?php $len = strlen($contra); $len*=7.1; if($len<=100){$len=100;}?>
       <td><input onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="contra<?php echo $ide;?>" id="contra<?php echo $ide;?>" style="background-color:transparent; border:thin; width:<?php echo $len;?>px; cursor:pointer; text-align:center" value="<?php echo $contra;?>" onKeyUp="ajustar(this.id)"></td>
       <?php $len = strlen($nit); $len*=7.1; if($len<=100){$len=100;}?>
      <td><input onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="nit<?php echo $ide;?>" id="nit<?php echo $ide;?>" style="background-color:transparent; border:thin; width:<?php echo $len;?>px; cursor:pointer; text-align:center" value="<?php echo $nit;?>" onKeyUp="ajustar(this.id)"></td>
      <td>
          <?php $len = strlen($obje); $len*=7.1; if($len<=100){$len=100;}?>
      <input onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="obje<?php echo $ide;?>" id="obje<?php echo $ide;?>" style="background-color:transparent; border:thin; width:<?php echo $len;?>px; cursor:pointer; text-align:left" value="<?php echo $obje;?>"  onKeyUp="ajustar(this.id)"></td>
      <td>     
      <input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="vali<?php echo $ide;?>" id="vali<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="$<?php echo number_format($vali,2,'.',',');?>" class="currency">       
      </td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="iv<?php echo $ide;?>" id="iv<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="$<?php echo number_format($iv,2,'.',',');?>" class="currency"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  type="text" name="valos<?php echo $ide;?>" id="valos<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="$<?php echo number_format($valos,2,'.',',');?>" class="currency"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="ivos<?php echo $ide;?>" id="ivos<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="$<?php echo number_format($ivos,2,'.',',');?>" class="currency"></td>
      <td><span id="vf<?php echo $ide;?>" class="currency"><?php echo number_format($vf,2,'.',',');?></span></td>
      <td><span id="ant<?php echo $ide;?>" class="currency"><?php echo $ant;?></span></td>
      <td><span id="amo<?php echo $ide;?>" class="currency"><?php echo $amo;?></span></td>
      <td><span id="sala<?php echo $ide;?>" class="currency"><?php echo $sala;?></span></td>
      <td><span id="ret<?php echo $ide;?>" class="currency"><?php echo $ret;?></span></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="dedu<?php echo $ide;?>" id="dedu<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"value="$<?php echo number_format($dedu,2,'.',',');?>" class="currency"></td>
      <td><span id="nett<?php echo $ide;?>" class="currency"><?php echo $nett;?></span></td>
      <td><span id="neto<?php echo $ide;?>" class="currency"><?php echo $net;?></span></td>
      <td><span id="iva<?php echo $ide;?>" class="currency"><?php echo $iva;?></span></td>
      <td><span id="tot<?php echo $ide;?>" class="currency"><?php echo $bru;?></span></td>
      <td><span id="salc<?php echo $ide;?>" class="currency"><?php echo $salc;?></span></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" class="fecha" type="date" name="fei<?php echo $ide;?>" id="fei<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $fei;?>" ></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" class="fecha" type="date" name="fef<?php echo $ide;?>" id="fef<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $fef;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)" name="aseguc<?php echo $ide;?>" type="text" id="aseguc<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" value="<?php echo $aseguc;?>" maxlength="6"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="valorc<?php echo $ide;?>" id="valorc<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $valorc;?>" class="currency"></td>
      <td><input onKeyPress="return NumCheck(event, this)"  onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" class="fecha" type="date" name="inicioc<?php echo $ide;?>" id="inicioc<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"   value="<?php echo $inicioc;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" class="fecha" type="date" name="finc<?php echo $ide;?>" id="finc<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $finc;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)" name="asegus<?php echo $ide;?>" type="text" id="asegus<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" value="<?php echo $asegus;?>" maxlength="6" ></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="valors<?php echo $ide;?>" id="valors<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $valors;?>" class="currency"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  class="fecha" type="date" name="inicios<?php echo $ide;?>" id="inicios<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $inicios;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" class="fecha" type="date" name="fins<?php echo $ide;?>" id="fins<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $fins;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)"  name="asegub<?php echo $ide;?>" type="text" id="asegub<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" value="<?php echo $asegub;?>" maxlength="6"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="valorb<?php echo $ide;?>" id="valorb<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $valorb;?>" class="currency"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" class="fecha" type="date" name="iniciob<?php echo $ide;?>" id="iniciob<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $iniciob;?>"></td>
      <td><input  onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  class="fecha" type="date" name="finb<?php echo $ide;?>" id="finb<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $finb;?>"/></td>
      <td><input onKeyPress="return NumCheck(event, this)" name="asegur<?php echo $ide;?>" type="text" id="asegur<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" value="<?php echo $asegur;?>" maxlength="6"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="valorr<?php echo $ide;?>" id="valorr<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $valorr;?>" class="currency"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" class="fecha" type="date" name="inicior<?php echo $ide;?>" id="inicior<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $inicior;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  class="fecha" type="date" name="finr<?php echo $ide;?>" id="finr<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $finr;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)" name="asegue<?php echo $ide;?>" type="text" id="asegue<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" value="<?php echo $asegue?>" maxlength="6"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onchange="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="text" name="valore<?php echo $ide;?>" id="valore<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center"  value="<?php echo $valore;?>" class="currency"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  class="fecha" type="date" name="inicioe<?php echo $ide;?>" id="inicioe<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $inicioe;?>"></td>
      <td><input onKeyPress="return NumCheck(event, this)" onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')"  class="fecha" type="date" name="fine<?php echo $ide;?>" id="fine<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $fine;?>" /></td>
      <td ><input onblur="CONTROLESTADOS('ACTUALIZAR','<?PHP echo $ide;?>')" type="date" class="fecha" name="fechal<?php echo $ide;?>" id="fechal<?php echo $ide;?>" style="background-color:transparent; border:thin; width:100px; cursor:pointer; text-align:center" value="<?php echo $fechal;?>" /></td>
      </tr>
      <?php
	}*/
	?>
  </tbody>
      <tr ><th><a role="button" title="Agregar Contratista"  class="btn btn-info btn-xs agregar"><i class="fa fa-plus"></i></a></th><th colspan='45'></th></tr>
  <tfoot>
  <tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
  </tfoot>
</table>

</div>
    <div id="events"></div>
  <?php
//  echo "<style onload=autoTabIndex('#tbEstadoscontrato','45')></style>";
  echo "<style onload=INICIALIZARLISTAS('')></style>";
}
else if($opcion=="AGREGARCONTRATO")
{
   $pre = $_POST['pre'];
   $ins = mysqli_query($conectar,"insert into estados_contrato(pre_clave_int) values('".$pre."')");
   if($ins>0)
   {
	 echo 1;
   }
   else
   {
	 echo 2;
   }
}
 else if($opcion=="ELIMINAR")
   {
	   $id = $_POST['id'];
	   $del = mysqli_query($conectar,"delete from estados_contrato where esc_clave_int = '".$id."'");
	   if($del>0)
	   {
		   echo 1;
	   }
	   else
	   {
		   echo 2;
		}
   }
   else if($opcion=="ACTUALIZAR")
   {
	   $id = $_POST['id'];
	   $ord = $_POST['ord'];
	   $gru = $_POST['gru'];
	   $cont = $_POST['cont'];
	   $contra = $_POST['contra'];
	   $nit = $_POST['nit'];
	   $vali = $_POST['vali'];
	   $iv = $_POST['iv'];
	   $obje = $_POST['obje'];
	   $valos = $_POST['valos'];
	   $ivos = $_POST['ivos'];
	   $dedu = $_POST['dedu'];
	   $fei = $_POST['fei'];
	   $fef = $_POST['fef'];
	   $aseguc = $_POST['aseguc'];
	   $valorc = $_POST['valorc'];
	   $inicioc = $_POST['inicioc'];
	   $finc = $_POST['finc'];
	   $asegus = $_POST['asegus'];
	   $valors = $_POST['valors'];
	   $inicios = $_POST['inicios'];
	   $fins = $_POST['fins'];
	   $asegub = $_POST['asegub'];
	   $valorb = $_POST['valorb'];
	   $iniciob = $_POST['iniciob'];
	   $finb = $_POST['finb'];
	   $asegur = $_POST['asegur'];
	   $valorr = $_POST['valorr'];
	   $inicior = $_POST['inicior'];
	   $finr = $_POST['finr'];
	   $asegue = $_POST['asegue'];
	   $valore = $_POST['valore'];
	   $inicioe = $_POST['inicioe'];
	   $fine = $_POST['fine'];
	   $fechal = $_POST['fechal'];
	   $pre = $_POST['pre'];
	   $num = 0;
	   $num1 = 0;
		$ant = 0;
		$amo = 0;
		$iva = 0;
		$ret = 0;
		$net = 0;
		$bru = 0;
		$sala = 0;
	   if($nit!="" and $gru!="" and $cont!="")
	   {
	      $ver = mysqli_query($conectar,"select * from estados_contrato where pre_clave_int = '".$pre."' and esc_nit = '".$nit."' and gru_clave_int = '".$gru."' and esc_contrato = '".$cont."'  and esc_clave_int!='".$id."'");
		  $num = mysqli_num_rows($ver);
	   }
	   if($nit!="" and $contra!="")
	   {
	        $ver1 = mysqli_query($conectar,"select * from estados_contrato where pre_clave_int = '".$pre."' and esc_nit = '".$nit."' and UPPER(esc_contratista) != UPPER('".$contra."')  and esc_clave_int!='".$id."'");
		  $num1 = mysqli_num_rows($ver1);
	   }
	   if((strtotime($fei)>strtotime($fef) and $fei!="" and $fef!="" and validateDate($fei, 'Y-m-d') and validateDate($fef, 'Y-m-d')) ||
           (strtotime($inicioc)>strtotime($finc) and $inicioc!="" and $finc!="" and validateDate($inicioc, 'Y-m-d') and validateDate($finc, 'Y-m-d'))||
           (strtotime($inicios)>strtotime($fins) and $inicios!="" and $fins!="" and validateDate($inicios, 'Y-m-d') and validateDate($fins, 'Y-m-d')) ||
           (strtotime($iniciob)>strtotime($finb) and $iniciob!="" and $finb!="" and validateDate($iniciob, 'Y-m-d') and validateDate($finb, 'Y-m-d')) ||
           (strtotime($inicior)>strtotime($finr) and $inicior!="" and $finr!="" and validateDate($inicior, 'Y-m-d') and validateDate($finr, 'Y-m-d')) ||
           (strtotime($inicioe)>strtotime($fine) and $inicioe!="" and $fine!="" and validateDate($inicioe, 'Y-m-d') and validateDate($fine, 'Y-m-d')))
	   {
		 $res = "error4";
	   }
	   else	   
	   if($num1>0)
	   {
	     $res = "error1";
	   }
	   else if($num>0)
	   {
	     $res = "error2";
		 
	   }
	   else
	   {
	       $update = mysqli_query($conectar,"update estados_contrato set esc_orden='".$ord."',esc_contrato = '".$cont."',gru_clave_int = '".$gru."',esc_contratista='".$contra."',esc_nit = '".$nit."',esc_obj_contrato ='".$obje."',esc_val_inicial = '".$vali."',esc_iva = '".$iv."',esc_vr_otrosi = '".$valos."',esc_iva_otrosi = '".$ivos."',esc_deducciones = '".$dedu."',esc_fec_inicio='".$fei."',esc_fec_fin = '".$fef."',esc_cum_asegu = '".$aseguc."',esc_cum_valor = '".$valorc."',esc_cum_inicio ='".$inicioc."',esc_cum_fin = '".$finc."',esc_sal_asegu = '".$asegus."',esc_sal_valor = '".$valors."',esc_sal_inicio ='".$inicios."',esc_sal_fin = '".$fins."',esc_bue_asegu = '".$asegub."',esc_bue_valor = '".$valorb."',esc_bue_inicio ='".$iniciob."',esc_bue_fin = '".$finb."',esc_res_asegu = '".$asegur."',esc_res_valor = '".$valorr."',esc_res_inicio ='".$inicior."',esc_res_fin = '".$finr."',esc_est_asegu = '".$asegue."',esc_est_valor = '".$valore."',esc_est_inicio ='".$inicioe."',esc_est_fin = '".$fine."',esc_fec_acta = '".$fechal."' where esc_clave_int = '".$id."'");

		   if($update>0)
		   {
				$con = mysqli_query($conectar,"select * from estados_contrato where esc_clave_int = '".$id."'");
				$dat = mysqli_fetch_array($con);
				$gru = $dat['gru_clave_int'];
				$cont = $dat['esc_contrato'];
				$contra = $dat['esc_contratista'];
				$nit = $dat['esc_nit'];
				$obje = $dat['esc_obj_contrato'];
				$vali = $dat['esc_val_inicial'];
				$iv = $dat['esc_iva'];
				$valos = $dat['esc_vr_otrosi'];
				$ivos = $dat['esc_iva_otrosi'];
				$dedu = $dat['esc_deducciones'];
				if($nit!="" /*and $gru>0*/)
				{
				$condatos = mysqli_query($conectar,"select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo,sum(cpe_iva) iv,sum(cpe_ret_garantia) gar,sum(cpe_valor_neto) net from control_egreso where pre_clave_int = '".$pre."'   and (cpe_documento = '".$nit."' and UPPER(cpe_beneficiario) = UPPER('".$contra."-".$cont."'))");
				$dato = mysqli_fetch_array($condatos); // gru_clave_int = '".$gru."' and cpe_documento = '".$nit."'
				$ant = $dato['ant'];
				$amo = $dato['amo'];
				$iva = $dato['iv'];
				$ret = $dato['gar'];
				$net = $dato['net'];
				$bru = $net + $iva;
				$sala = $ant - $amo;
				}
				else
				{
				$ant = 0;
				$amo = 0;
				$iva = 0;
				$ret = 0;
				$net = 0;
				$bru = 0;
				$sala = 0;
				}
				
				$vf  = $vali + $iv + $valos + $ivos;
				$nett = $bru - $amo - $ret - $dedu;
				$salc = $vf-$bru;
			    $res = "ok";
		   }
		   else
		   {
		      $res = "error3";
		   }
	   }
	   $datos[] = array("res"=>$res,"vf"=>number_format($vf,2,'.',','),"nett"=>number_format($nett,2,'.',','),"salc"=>number_format($salc,2,'.',','),"date" =>validateDate($fei,'Y-m-d'));
	   echo json_encode($datos);	    
   }
   else if($opcion=="PAGOSACTAS")
   {
	   $pre = $_POST['id'];
	   $datos = array();
	   $num = 0;
	   $cona = mysqli_query($conectar,"select distinct CONVERT(cpe_num_acta,UNSIGNED INTEGER) ACT from control_egreso where pre_clave_int = '".$pre."' and cpe_num_acta!='' order by ACT");
	   while($data = mysqli_fetch_array($cona))
	   {
		   $datos[] = $data['ACT'];
	      $num++;
	   }
   ?>
   <script src="js/jspagoactas.js"></script>
       <div class="row">
       <div class="col-md-3">
           <div class="btn btn-block btn-info btn-xs" style="width:180px"><a style="color: white" title="Descagar Informe" href="modulos/presupuesto/actasexportarcontrol.php?edi=<?php echo $pre;?>" target="_blank"><i class="fa fa-file-excel-o"></i>Descargar Informe Actas</a></div>
       </div></div>
   <div class="table-responsive">
  <table width="100%" id="tbPagosActas" border="0" cellspacing="2" class="table table-bordered table-condensed compact" style="font-size:11px">
  <thead >
  <tr>
    <th rowspan="2" class="dt-head-center" style="background-color:rgba(215,215,215,1.00); ">CONTRATISTA</th>
    <th rowspan="2" class="dt-head-center" style="background-color:rgba(215,215,215,1.00); ">VALOR CONTRATO</th>
    <th style="background-color:rgba(215,215,215,1.00); " class="dt-head-center" colspan="<?php if($num>0){echo $num;}else{echo "1";}?>">ACTAS</th>
    <th rowspan="2" class="dt-head-center" style="background-color:rgba(215,215,215,1.00); ">VALOR ACUMULADO</th>
    <th rowspan="2" class="dt-head-center" style="background-color:rgba(215,215,215,1.00); ">V/R RETENIDO</th>
    <th rowspan="2" class="dt-head-center" style="background-color:rgba(215,215,215,1.00); ">ANTICIPO</th>
    <th rowspan="2" class="dt-head-center" style="background-color:rgba(215,215,215,1.00); ">AMORTIZACIÓN</th>
    <th rowspan="2" class="dt-head-center" style="background-color:rgba(215,215,215,1.00); ">SALDO CONTRATO</th>
  </tr>
  <tr>
    <?php 
	  if($num>0)
	  {
		  for($k=0;$k<$num;$k++)
		  {
			  ?>
               <th style="background-color:rgba(215,215,215,1.00); " class="dt-head-center"><?php echo $datos[$k];?> </th>
              <?php
		  
		  }
	  }
	  else
	  {
		?>
         <th style="background-color:rgba(215,215,215,1.00); " class="dt-head-center"> </th>
		<?php		  
	  }
		?>

  </tr>
  </thead>
  <tbody>
	  <?php
      $conc = mysqli_query($conectar,"select DISTINCT e.cpe_beneficiario,e.cpe_documento from control_egreso e   where e.pre_clave_int = '".$pre."' order by e.cpe_beneficiario");
      while($datc = mysqli_fetch_array($conc))
      { 
	     $contra = $datc['cpe_beneficiario'];
		 $nit = $datc['cpe_documento'];
		 
		 $query1 = mysqli_query($conectar,"select sum(esc_val_inicial) val,sum(esc_iva) iv,sum(esc_vr_otrosi) os,sum(esc_iva_otrosi) as ivo,sum(esc_deducciones) dedu from estados_contrato where esc_nit = '".$nit."' and UPPER(CONCAT_WS('-',esc_contratista,CAST(esc_contrato AS CHAR))) = UPPER('".$contra."') and pre_clave_int = '".$pre."'");
		 $dat1 = mysqli_fetch_array($query1);
		 $vcontrato = $dat1['val']+$dat1['iv']+$dat1['os']+$dat1['ivo'];
		 
		 $query2 = mysqli_query($conectar,"select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo,sum(cpe_ret_garantia) as gar from control_egreso where pre_clave_int = '".$pre."' and cpe_documento = '".$nit."' and UPPER(cpe_beneficiario) = UPPER('".$contra."')");
		 $dat2 = mysqli_fetch_array($query2);
		 $anticipo = $dat2['ant'];
		 $amortizado = $dat2['amo'];
		 $retenido = $dat2['gar'] + $dat1['dedu'];//duda de si se suma tb las deduciones
		 
      ?>
      <tr>
      <td class="dt-left" ><?php echo $contra."- ".$nit;?></td>
      <td class="dt-right"><span class="currency">$<?php echo number_format($vcontrato,'2','.','');?></span></td>
      <?php 
	  $acum = 0;
	  if($num>0)
	  {
		  for($k=0;$k<$num;$k++)
		  {
			  $query3 = mysqli_query($conectar,"select sum(cpe_valor_neto) net,sum(cpe_iva) as iv from control_egreso where pre_clave_int = '".$pre."' and cpe_documento = '".$nit."' and cpe_num_acta = '".$datos[$k]."' and UPPER(cpe_beneficiario) = UPPER('".$contra."')");
			  $dat3 = mysqli_fetch_array($query3);
			  $net = $dat3['net'];
			  $iva = $dat3['iv'];
			  $bru = $net + $iva;
			  $acum = $acum + $bru;
			  ?>
              <td class="dt-center" ><span class="currency">$<?php echo number_format($bru,'2','.','');?></span></td>
              <?php
		  
		  }
	  }
	  else
	  {
		  $acum = 0;
		?>
        <td class="dt-center"></td>
		<?php		  
	  }
	  $saldocontrato = $vcontrato - $acum - $anticipo + $amortizado;
		?>
      
      <td class="dt-right"><span class="currency">$<?php echo number_format($acum,'2','.','');?></span></td>
      <td><span class="currency">$<?php echo number_format($retenido,2,'.',''); ?></span></td>
      <td><span class="currency">$<?php echo number_format($anticipo,2,'.',''); ?></span></td>
      <td><span class="currency">$<?php echo number_format($amortizado,2,'.',''); ?></span></td>
      <td><span class="currency">$<?php echo number_format($saldocontrato,2,'.',''); ?></span></td>
      </tr>
      <?php
      }
      ?>
  </tbody>
  <tfoot>
  <tr>
  <th></th>
  <th></th>
   <?php 
	  if($num>0)
	  {
		  for($k=0;$k<$num;$k++)
		  {
			  ?>
              <th></th>
              <?php
		  
		  }
	  }
	  else
	  {
		?>
        <th></th>
		<?php		  
	  }
		?>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  </tr>
  </tfoot>
  </table>
  </div>
   <?pHP
   }
?>