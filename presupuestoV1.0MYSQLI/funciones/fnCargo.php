
<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_COOKIE["usIdentificacion"];
  $clave= $_COOKIE["clave"];  
  $con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
  $dato = mysqli_fetch_array($con);
  $perfil = strtoupper($dato['prf_descripcion']);
  $percla = $dato['prf_clave_int'];
  $coordi = $dato['usu_coordinador'];
  $fontsize = $dato['usu_tam_fuente'];
  //$idUsuario= $_COOKIE["usIdentificacion"];
	//$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
    //LIVERA LOS INGRESO A TAL PRESUPUESTO
  $update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");
  
  $opcion = $_POST['opcion'];
  if($opcion=="NUEVO")
  {
	  ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group">
        <div class="col-md-6"><strong>Nombre:</strong>
        <div class="ui corner labeled input">
        <input type="text" placeholder="Ingrese el nombre" value="" id="txtnombre" name="txtnombre" class="form-control input-sm">
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        </div>
        <div class="col-md-6"><strong>Estado:</strong><br>
        <label for="opcion1"><input type="radio" id="opcion1" name="radestado" checked="checked" value="1"/>Activo
        </label>
        <label for="opcion2"><input type="radio" id="opcion2" name="radestado" value="0">Inactivo</label>
        </div>
    </div>
    </form>
      <?php	 
  }
  else if($opcion=="EDITAR")
  {
	  $id = $_POST['id'];
	    $coninfo = mysqli_query($conectar,"select c.car_clave_int car,c.car_nombre as nomc,c.est_clave_int as est from  cargos c  where c.car_clave_int = '".$id."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $idc = $datinfo['car'];
	  $nom  = $datinfo['nomc'];
	  $esta = $datinfo['est'];
	  ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
    <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    <div class="form-group">
        <div class="col-md-6"><strong>Nombre:</strong>
        <div class="ui corner labeled input">
        <input type="text" placeholder="Ingrese el Nombre" value="<?php echo $nom;?>" id="txtnombre" name="txtnombre" class="form-control input-sm">
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
        </div>
        <div class="col-md-6"><strong>Estado:</strong><br>
        <label for="opcion1"><input type="radio" id="opcion1" name="radestado" checked="checked" value="1" <?php if($esta==1 || $esta==""){echo 'checked="checked"';}?>>Activo
        </label>
        <label for="opcion2"><input type="radio" id="opcion2" name="radestado" value="0" <?php if($esta==0){echo 'checked="checked"';}?>>Inactivo</label>
        </div>
    </div>
    </form>
      <?php	 
  }
  else if($opcion=="GUARDAR")
  {
    $nombre = $_POST['nombre'];
    $estado  = $_POST['estado'];
    $veri = mysqli_query($conectar,"select * from cargos where UPPER(car_nombre) = UPPER('".$nombre."') and est_clave_int!=2");
    $numv = mysqli_num_rows($veri);
    if($numv>0)
    {
      echo 2;
    }
    else 
    {
      $ins = mysqli_query($conectar,"insert into cargos(car_nombre,est_clave_int,car_usu_actualiz,car_fec_actualiz) values('".$nombre."','".$estado."','".$usuario."','".$fecha."')");
      if($ins>0)
      {
        echo 1;
      }
      else
      {
        echo 3;
      }	
    }	 
  }
  else if($opcion=="GUARDAREDICION")
  {
	  $ide = $_POST['id'];
     $nombre = $_POST['nombre'];
	 $estado  = $_POST['estado'];
	 $veri = mysqli_query($conectar,"select * from cargos where UPPER(car_nombre) = UPPER('".$nombre."') and car_clave_int!='".$ide."' and est_clave_int!=2");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update cargos set car_nombre='".$nombre."',est_clave_int='".$estado."',car_usu_actualiz='".$usuario."',car_fec_actualiz='".$fecha."' where car_clave_int = '".$ide."'");
		if($upd>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
  else if($opcion=="CARGARLISTACARGOS")
  {
	  ?> <script src="js/jscargos.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <div>
    <table id="tbcargos" class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
      <thead>
        <tr>
          <th style="width:20px"></th> 
          <th style="width:20px"></th>               
          <th class="dt-head-center">NOMBRE</th>
          <th class="dt-head-center" style="width:40px">ESTADO</th>
        </tr>
      </thead>
                 <?PHP
			$con = mysqli_query($conectar,"select c.car_clave_int as di,c.car_nombre nom,c.est_clave_int as est,e.est_nombre as estn from  cargos c join estados e on e.est_clave_int = c.est_clave_int  where e.est_clave_int!=2 order by nom")
			?>
      <tbody>
      <?php
      while($dat = mysqli_fetch_array($con))
      {
        $idc = $dat['di'];

        $nom = $dat['nom'];
        $est = $dat['est'];
        $estnom = $dat['estn'];
        if($est=="0"){$est='<span class="label label-warning pull-right">'.$estnom.'</span>';}
        else {$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
        ?>
        <tr id="row_car<?php echo $idc;?>">
          <td><a class="btn btn-block btn-default btn-xs" onClick="CRUDCARGOS('EDITAR',<?PHP echo $idc;?>)" data-toggle="modal" data-target="#myModal" style="width:20px; height:20px"><i class="glyphicon glyphicon-pencil"></i></a></td>
          <td><a class="btn btn-block btn-danger btn-xs" onClick="CRUDCARGOS('ELIMINAR',<?PHP echo $idc;?>)" style="width:20px; height:20px"><i class="glyphicon glyphicon-trash" ></i></a></td>
          <td><?Php echo $nom;?></td>
          <td><?php echo $est;?></td>
        </tr>
      <?php
      }
      ?>
      </tbody>
        <tfoot>
          <tr>
            <th></th>
            <th></th> 
            <th>NOMBRE</th>
            <th>ESTADO</th>
          </tr>
        </tfoot>
      </table>
      </div>
      <?PHP
  }
  else if($opcion=="ELIMINAR")
  {
    $id = $_POST['id'];
    $update = mysqli_query($conectar,"update cargos set est_clave_int = 2 where car_clave_int = '".$id."'");
    if($update>0){  echo 1;}else {echo 2;}
  }

?>