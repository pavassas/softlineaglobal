<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario = $_COOKIE["usIdentificacion"];
	//$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = strtoupper($dato['prf_descripcion']);
	$percla = $dato['prf_clave_int'];
	$coordi = $dato['usu_coordinador'];
	$fontsize = $dato['usu_tam_fuente'];
	$cla = $dato['usu_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$nombreusuario = $dato['usu_nombre'];

	if(strtoupper($perfil)=="ADMINISTRADOR"){ $estado=1;}else{$estado=3;}
		$grupos = 0;
	$con = mysqli_query($conectar,"select gru_clave_int from grupos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['gru_clave_int'];
		   $idsu[] = $ins;
	   }
	   $grupos = implode(',',$idsu);
	}
	$capitulos = 0;
	$con = mysqli_query($conectar,"select cap_clave_int from capitulos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $cap = $dat['cap_clave_int'];
		   $idsu[] = $cap;
	   }
	   $capitulos = implode(',',$idsu);
	}
  //LIVERA LOS INGRESO A TAL PRESUPUESTO
  $update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");
  
    $opcion = $_POST['opcion'];
  if($opcion=="NUEVO")
  {
	  ?>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <form name="form1" id="form1" class="form-horizontal">
        <div class="form-group">
            <div class="col-md-6"><strong>Nombre:</strong>
            <div class="ui corner labeled input">
            <input type="text" placeholder="Ingrese el nombre" value="" id="txtnombre" name="txtnombre" class="form-control input-sm"/>
             <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
            </div>
             <div class="col-md-6"><strong>Categoria:</strong>
             <div class="ui corner labeled input">
            <select  id="selcategoria" name="selcategoria" class="form-control input-sm">
            <option value="">--seleccione--</option>
            <option value="1">1-Civil</option>
            <option value="2">2-Gastos Generales</option>
            <option value="3">3-Equipos Especiales</option>
            <option value="4">4-Instalaciones Especiales</option>
            <option value="5">5-Otros</option>
            </select>
             <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
            </div>
           
        </div>
        </form>
      <?php
	 
  }
  else if($opcion=="EDITAR")
  {
	  $id = $_POST['id'];
	  $coninfo = mysqli_query($conectar,"select p.gru_clave_int gru,p.gru_nombre as nom,p.est_clave_int as est,p.gru_categoria cat from  grupos p  where p.gru_clave_int = '".$id."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $idp = $datinfo['gru'];
	  $nom  = $datinfo['nom'];
	  $cat = $datinfo['cat'];
	  ?>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <form name="form1" id="form1" class="form-horizontal">
        <input type="hidden" id="idedicion" value="<?php echo $id;?>">
        <div class="form-group">
            <div class="col-md-6"><strong>Nombre:</strong>
            <div class="ui corner labeled input">
            <input type="text" placeholder="Ingrese el Nombre" value="<?php echo $nom;?>" id="txtnombre" name="txtnombre" class="form-control input-sm">
             <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
            </div>
             <div class="col-md-6"><strong>Categoría:</strong>
             <div class="ui corner labeled input">
            <select  id="selcategoria" name="selcategoria" class="form-control input-sm">
            <option value="">--seleccione--</option>
            <option <?Php if($cat==1){echo 'selected';}?> value="1">1-Civil</option>
            <option <?Php if($cat==2){echo 'selected';}?> value="2">2-Gastos Generales</option>
            <option <?Php if($cat==3){echo 'selected';}?> value="3">3-Equipos Especiales</option>
            <option <?Php if($cat==4){echo 'selected';}?> value="4">4-Instalaciones Especiales</option>
            <option <?Php if($cat==5){echo 'selected';}?> value="5">5-Otros</option>
            </select>
             <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
            </div>
            
        </div>
        </form>
      <?php
	 
  }
  else if($opcion=="GUARDAR")
  {
     $nombre = $_POST['nombre'];
	 $categoria = $_POST['categoria'];
	 $veri = mysqli_query($conectar,"select * from grupos where UPPER(gru_nombre) = UPPER('".$nombre."') and (est_clave_int= 1 or gru_clave_int in(".$grupos."))");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
		$cmax = mysqli_query($conectar,"select max(gru_orden) as ma from grupos");
		$dmax = mysqli_fetch_array($cmax);
		if($dmax['ma']=="" || $dmax['ma']==0){$max = 1;}else{$max = $dmax['ma']+1;} 
        $ins = mysqli_query($conectar,"insert into grupos(gru_nombre,est_clave_int,gru_usu_actualiz,gru_fec_actualiz,gru_categoria,gru_orden,usu_clave_int) values('".$nombre."','".$estado."','".$usuario."','".$fecha."','".$categoria."','".$max."','".$idUsuario."')");
		if($ins>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
   else if($opcion=="GUARDAREDICION")
  {
	  $ide = $_POST['id'];
     $nombre = $_POST['nombre'];
	 $categoria = $_POST['categoria'];
	 $veri = mysqli_query($conectar,"select * from grupos where UPPER(gru_nombre) = UPPER('".$nombre."') and gru_clave_int!='".$ide."' and (est_clave_int=1 or gru_clave_int in(".$grupos."))");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update grupos set gru_nombre='".$nombre."',gru_usu_actualiz='".$usuario."',gru_fec_actualiz='".$fecha."',gru_categoria = '".$categoria."' where gru_clave_int = '".$ide."'");
		if($upd>0)
		{
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
  else if($opcion=="CARGARLISTAGRUPOS")
  {
	  ?> <script src="js/jsgrupos.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
      <div id="result"></div>
        <div>
      <table id="tbgrupos" class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                  <th>ORDEN</th>
                  <th style="width:20px"></th> 
                  <th style="width:20px"></th>
                  <th style="width:20px" class="dt-head-center">CODIGO</th>  
                               
                  <th class="dt-head-center">NOMBRE</th>                  
                  <th class="dt-head-center" style="width:100px">CATEGORIA</th>
                  <th class="dt-head-center" style="width:100px">CAPITULOS</th>
                  <th class="dt-head-center">ESTADO</th>
                  <th class="dt-head-center" style="width:20px" >SUBIR</th>
                  <th class="dt-head-center" style="width:20px" >BAJAR</th> 
                </tr>
                </thead>
               <?PHP
			$con = mysqli_query($conectar,"select p.gru_clave_int as di,p.gru_nombre nom,p.est_clave_int as est,e.est_nombre as estn,p.gru_categoria cat,gru_orden,p.usu_clave_int  as usu from grupos p join estados e on e.est_clave_int  = p.est_clave_int where e.est_clave_int in(0,1) or gru_clave_int in(".$grupos.") order by gru_orden")
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['di'];	
				   $concap = mysqli_query($conectar,"select count(grc_clave_int) as tot from grupo_capitulos d join capitulos c on c.cap_clave_int = d.cap_clave_int where gru_clave_int = '".$idc."' and (c.est_clave_int =1 or c.cap_clave_int in(".$capitulos."))");
				   $datcap = mysqli_fetch_array($concap);
				   $cantc = $datcap['tot'];			 
				   $nom = $dat['nom'];
				   $est = $dat['est'];
				   $estnom = $dat['estn'];
				   $cat = $dat['cat'];
				   $ord = $dat['gru_orden'];
				   $usu = $dat['usu'];
					if($cat==1){$cat = "1-Civil";}else
					if($cat==2){$cat= "2-Gastos Generales";}else
					if($cat==3){$cat = "3-Equipos Especiales";}else
					if($cat==4){$cat = "4-Instalaciones Especiales";}else
					if($cat==5){$cat = "5-Otros";}
				   if($estnom=="Activo"){$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
				   else {$est='<span class="label label-info pull-right">'.$estnom.'</span>';}
				   ?>
                <tr id="row_gru<?php echo $idc;?>">
                <td class="dt-center"><?php echo $ord;?></td>
                <td class="dt-center">
                <?php if($perfil=="ADMINISTRADOR" || $usu==$idUsuario){		?>
                <a class="btn btn-block btn-default btn-xs" onClick="CRUDGRUPOS('EDITAR',<?PHP echo $idc;?>)" data-toggle="modal" data-target="#myModal" style="width:20px; height:20px"><i class="glyphicon glyphicon-pencil"></i></a>
               <?php } ?> 
               </td>
                <td>
                 <?php if($perfil=="ADMINISTRADOR" || $usu==$idUsuario)	{	?>
                <a class="btn btn-block btn-danger btn-xs" onClick="CRUDGRUPOS('ELIMINAR',<?PHP echo $idc;?>)" style="width:20px; height:20px"><i class="glyphicon glyphicon-trash" ></i></a><?php } ?></td>
                <td class="dt-center"><?Php echo $idc;?></td>
                
                <td class="dt-left"><?Php echo $nom;?></td>
                
                <td class="dt-center"><?php echo $cat;?></td>
                <td class="dt-center"> <?php if($perfil=="ADMINISTRADOR" || $usu==$idUsuario){	?>
                <a class="btn btn-block btn-success btn-xs" onClick="CRUDGRUPOS('ASOCIAR',<?PHP echo $idc;?>)" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-transfer"  ></i>&nbsp;Cantidad <?php echo $cantc;?></a><?php } ?></td>
                <td class="dt-center"><?php echo $est;?></td>
                <td class="dt-center"><button class="btn btn-xs up"><i class="glyphicon glyphicon-arrow-up"></i></button></td>
                <td class="dt-center"><button class="btn btn-xs down"><i class="glyphicon glyphicon-arrow-down"></i></button></td>
                </tr>
                <?php
			   }
			   ?>
                </tbody>
                 <tfoot>
                <tr>
                <th></th>
                <th></th>
                <th></th>
                <th class="dt-head-center"></th>
                
                <th class="dt-head-center">Nombre</th>
                
                <th class="dt-head-center">Categoria</th> 
                <th></th>
                <th class="dt-head-center">Estado</th>
                <th></th>
                <th></th>
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP

  } else if($opcion=="ELIMINAR")
  {
    $id = $_POST['id'];
	$con = mysqli_query($conectar,"select * from grupo_capitulos where gru_clave_int = '".$id."'");
	$num = mysqli_num_rows($con); 
	$update = mysqli_query($conectar,"update grupos set est_clave_int = 2 where gru_clave_int = '".$id."'");
	if($update>0){ if($num>0){$del = mysqli_query($conectar,"delete from grupo_capitulos where gru_clave_int= '".$id."'");}  echo 1;}else {echo 2;}
  }
  else if($opcion=="ASOCIAR")
  {
	  $id = $_POST['id'];
	  $con = mysqli_query($conectar,"select gru_nombre from grupos where gru_clave_int ='".$id."'");
	  $dat = mysqli_fetch_array($con);
	  $gru = $dat['gru_nombre'];
	  
	?>
    <input type="hidden" id="idasociar" name="idasociar" value="<?php echo $id;?>"/>
    <div class="row">
    <div class="col-md-12">
    <h6><strong>Grupo:</strong><small><?php echo $gru;?></small></h6>
    </div>
    </div>
    <div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
            <h5 class="panel-title">CAPITULOS X ASOCIAR<br><input type="button" id="asignar" class="btn btn-primary  btn-xs"  style="width:70px"  value="Pasar &raquo;">    <input type="button" id="asignartodos" class="btn btn-primary  btn-xs" style="width70px"  value="Todos &raquo;"></h5>
            </div>
            <div class="panel-body">
            <script src="js/jscapitulo1.js"></script>
                <table id="tbcapitulo1"  style="font-size:<?php echo $fontsize;?>px" class="table table-bordered table-condensed compact table-hover">
                <thead>
                <tr>          
                <th class="dt-head-center">NOMBRE</th>                
                </tr> 
                </thead>                
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
    	<div class="panel panel-default">
            <div class="panel-heading">
            <h5 class="panel-title">CAPITULOS ASOCIADOS<br><input type="button" id="quitar" class="btn btn-danger  btn-xs" style="width:70px" value="&laquo; Quitar" ><input type="button" id="quitartodos" class="btn btn-danger btn-xs " style="width:70px" value="&laquo; Todos" >	</h5>
            </div>
            <div class="panel-body">
            <script src="js/jscapitulo2.js"></script>
            <table id="tbcapitulo2" style="font-size:<?php echo $fontsize;?>px;" class="table table-bordered  table-condensed compact table-hover">
            <thead>
            <tr>
            <th>NOMBRE</th>
            </thead>        
            </table>
            </div>
        </div>
    </div>
    </div>
    
    <?pHP 
  }else
  if($opcion=="AGREGARCAPITULO")
	{
	   $idgrupo = $_POST['idgrupo'];
	   $capitulo = $_POST['capitulo'];
	   $con = mysqli_query($conectar,"select * from grupo_capitulos where gru_clave_int = '".$idgrupo."' and cap_clave_int ='".$capitulo."'");
	   $num = mysqli_num_rows($con);
	   if($num>0)
	   {
		   echo 'ok1';
	   }
	   else
	   {
		   $ins = mysqli_query($conectar,"insert into grupo_capitulos (gru_clave_int,cap_clave_int) values('".$idgrupo."','".$capitulo."')");
		   if($ins>0)
		   {
			   echo 'ok';
		   }
		   else
		   {
			  echo 'error';
		   }
	   }
	}
	else
	if($opcion=="ELIMINARCAPITULO")
	{
	   $iddetalle = $_POST['id'];
	   $delete = mysqli_query($conectar,"delete from grupo_capitulos where grc_clave_int = '".$iddetalle."'");
	   if($delete>0)
	   {
		  echo 'ok';
	   }
	   else
	   {
	       echo 'error';
	   }
	}
	else
	if($opcion == 'AGREGARTODOS')
	{
		$id = $_POST['id'];		
		$con = mysqli_query($conectar,"insert into grupo_capitulos select null,'".$id."',cap_clave_int from capitulos where cap_clave_int not in (select cap_clave_int from grupo_capitulos where gru_clave_int = '".$id."')");
		if($con>0)
		{
		  echo 'ok';
		}
		else
		{
		  echo 'error';
		}
	
	}
	else
	if($opcion=="ELIMINARTODOS")
	{
		$id = $_POST['id'];
		$con = mysqli_query($conectar,"delete from grupo_capitulos where gru_clave_int = '".$id."'");
		if($con>0)
		{
		  echo 'ok';
		}
		else
		{
		  echo 'error';
		}
	}
	else if($opcion=="CAMBIARORDEN")
	{
		$id1 = $_POST['id1'];
		$id2 = $_POST['id2'];
	    $con1 = mysqli_query($conectar,"select gru_orden from grupos where gru_clave_int = '".$id1."'");
		$con2 = mysqli_query($conectar,"select gru_orden from grupos where gru_clave_int = '".$id2."'");
		$dat1 = mysqli_fetch_array($con1); $ord1 = $dat1['gru_orden'];
	    $dat2 = mysqli_fetch_array($con2); $ord2 = $dat2['gru_orden'];
		$update1 = mysqli_query($conectar,"update grupos set gru_orden = '".$ord2."' where gru_clave_int ='".$id1."'");
		$update2 = mysqli_query($conectar,"update grupos set gru_orden = '".$ord1."' where gru_clave_int ='".$id2."'");
		if($update1>0 and $update2>0)
		{
		   echo 1;
		}
		else
		{
		   echo 2;
		}

		
	}
	else if($opcion=="CAMBIARORDEN2")
	{
		$ord1 = $_POST['ord1'];
		$ord2 = $_POST['ord2'];
		$id = $_POST['id'];
	    $con1 = mysqli_query($conectar,"select gru_clave_int from grupos where gru_orden = '".$ord1."' and gru_clave_int = '".$id."'");
		//$con2 = mysqli_query($conectar,"select gru_clave_int from grupos where gru_orden = '".$ord2."'");
		$dat1 = mysqli_fetch_array($con1); $id1 = $dat1['gru_clave_int'];
	   // $dat2 = mysqli_fetch_array($con2); $id2 = $dat2['gru_clave_int'];
	   if($id1==$id)
	   {
		
		$update1 = mysqli_query($conectar,"update grupos set gru_orden = '".$ord2."' where gru_orden ='".$ord1."' and gru_clave_int = '".$id."'");
	   }
	   else 
	   {
		$update1 = mysqli_query($conectar,"update grupos set gru_orden = '".$ord2."' where gru_orden ='".$ord1."' and gru_clave_int!='".$id."'");
	   }
		//$update2 = mysqli_query($conectar,"update grupos set gru_orden = '".$ord1."' where gru_clave_int ='".$id2."'");
		if($update1>0)
		{
		   echo 1;
		}
		else
		{
		   echo 2;
		}

		
	}
	?>