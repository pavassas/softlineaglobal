<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	date_default_timezone_set('America/Bogota');
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
  $con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_clave_int = '".$idUsuario."'");
  $dato = mysqli_fetch_array($con);
  $perfil = strtoupper($dato['prf_descripcion']);
  $percla = $dato['prf_clave_int'];
  $coordi = $dato['usu_coordinador'];
  $fontsize = $dato['usu_tam_fuente'];

	
	$con = mysqli_query($conectar,"select * from permiso pr where pr.usu_clave_int = '".$idUsuario."' and pr.ven_clave_int = 12");
	$dato = mysqli_fetch_array($con);
	$metact = $dato['per_metodo'];
	
	$unidades = 0;
	$con = mysqli_query($conectar,"select uni_clave_int from unidades where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $uni = $dat['uni_clave_int'];
		   $idsu[] = $uni;
	   }
	   $unidades = implode(',',$idsu);
	}
	$insumos = 0;
	$con = mysqli_query($conectar,"select ins_clave_int from insumos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['ins_clave_int'];
		   $idsu[] = $ins;
	   }
	   $insumos = implode(',',$idsu);
	}
	
	$tipoproyectos = 0;
	$con = mysqli_query($conectar,"select tpp_clave_int from tipoproyecto where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['tpp_clave_int'];
		   $idsu[] = $ins;
	   }
	   $tipoproyectos = implode(',',$idsu);
	}
	
		$actividades = 0;
	$cona = mysqli_query($conectar,"select act_clave_int from actividades where est_clave_int in(1,3) and act_usu_creacion = '".$idUsuario."'");
	$numa = mysqli_num_rows($cona);
	if($numa>0)
	{
	   $ids = array();
	   for($a=0;$a<$numa;$a++)
	   {
		   $data = mysqli_fetch_array($cona);
		   $ida = $data['act_clave_int'];
		   $ids[] = $ida;
	   }
	   $actividades = implode(",",$ids);
	}
  if(strtoupper($perfil)=="ADMINISTRADOR"){ $estado=1;}else{$estado=3;}
  $opcion = $_POST['opcion'];
  $fecha=date("Y/m/d H:i:s");
  
  //LIVERA LOS INGRESO A TAL PRESUPUESTO
  $update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");


  if($opcion=="NUEVO")
  {
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
    <div class="row">    
    <div class="col-md-12"><strong>Descripción:</strong>
    <div class="ui corner labeled input">
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" placeholder="Escribe aqui la descripcion">
     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
    </div>
    </div>
       <div class="row">
         <div class="col-md-4"><strong>Tipo de Proyecto:</strong>
         <div class="ui corner labeled input">
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control input-sm" title="<?php echo $idUsuario;?>">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where est_clave_int = 1 or tpp_clave_int in(".$tipoproyectos.")");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
        <div class="col-md-4"><strong>Unidad de medida:</strong>
        <div class="ui corner labeled input">
         <select name="selunidad" id="selunidad" class="form-control input-sm selectpicker"  data-live-search="true" data-live-search-placeholder="Buscar Unidad" data-actions-box="true">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int in(".$unidades.")");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $cod;?></option>
            <?php
		 }
		 ?>		 
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>    
       
        
    </div>
    
    <div class="row">
         <div class="col-md-4"><strong>Pais:</strong>
         <div class="ui corner labeled input">
          <select name="selpais" id="selpais" class="form-control input-sm" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Departamento:</strong>
         <div class="ui corner labeled input">
           <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    
         <div class="col-md-4"><strong>Ciudad:</strong>
         <div class="ui corner labeled input">
             <select name="selciudad" id="selciudad" class="form-control input-sm">
         <option value="">--seleccione--</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <?php echo "<style onload=CRUDACTIVIDADES('PENDIENTES','')></style>";?>
    <div class="row">
    <hr class="divider">
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-left" id="nav1" title="ASIGNACION DE APU Y SUBANALISIS">
        <li class="active">
        <a onclick="CRUDACTIVIDADES('PENDIENTES','');" data-toggle="tab" aria-expanded="true"><strong>APU</strong></a>
        </li>
        <li>
        <a onclick="CRUDACTIVIDADES('PENDIENTESSUB','');" data-toggle="tab" aria-expanded="true"><strong>SUB-ANALISIS</strong></a>
        </li>
        
        </ul>
        </div>
    	<div class="col-md-12">
           <div id="pendientes"></div>
        </div>
        <div class="col-md-6"></div>
         <?php
		$cona = mysqli_query($conectar,"SELECT sum(p.inp_rendimiento*p.inp_valor) as apu FROM insumospendientes p join insumos i on i.ins_clave_int = p.ins_clave_int where p.usu_clave_int ='".$idUsuario."'");
	$data = mysqli_fetch_array($cona);
	if($data['apu']=="" || $data['apu']==NULL){$total=0;}else { $total = $data['apu'];}
		?>
         <div class="col-md-6" style="text-align:right">
        <strong>Total Apu: <span class="currency" id="totalapu"><?php echo number_format($total,2,'.',',');?></span>  </strong>      
     	</div>
    </div>
  </form>
  <?PHP  
  }
else
  if($opcion=="EDITAR" )
  {
	  $ida = $_POST['id'];
	   $coninfo = mysqli_query($conectar,"select act_nombre as des,tpp_clave_int as tpp,uni_clave_int uni,act_analisis as ana,a.est_clave_int as est,c.ciu_clave_int ciu,d.dep_clave_int dep,p.pai_clave_int pai ,a.act_usu_creacion usu from actividades a LEFT join ciudad c on c.ciu_clave_int = a.ciu_clave_int LEFT join departamento d on d.dep_clave_int = c.dep_clave_int LEFT join pais p on p.pai_clave_int = d.pai_clave_int  where act_clave_int = '".$ida."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $tpp = $datinfo['tpp'];
	  $uni = $datinfo['uni'];
	  $ana = $datinfo['ana'];
	  $des = $datinfo['des'];
	  $est = $datinfo['est'];
	  $ciu = $datinfo['ciu'];
	  $pai = $datinfo['pai'];
	  $dep = $datinfo['dep'];
	  $usu = $datinfo['usu'];
	  if($usu == $idUsuario){$per="si";}else if($percla==1){$per="si";}else{$per = "no";}
	  
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $ida;?>">
  <input type="hidden" id="idpermiso" value="<?php echo $per;?>" title="<?php echo $percla."--".$usu."---".$idUsuario;?>">
<div class="row">    
    <div class="col-md-12"><strong>Descripción:</strong>
    <div class="ui corner labeled input">
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" value="<?php echo $des;?>" placeholder="Escribe aqui la descripcion">
     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
    </div>
    </div>
       <div class="row">
         <div class="col-md-4"><strong>Tipo de Proyecto:</strong>
         <div class="ui corner labeled input">
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where est_clave_int = 1 or tpp_clave_int in(".$tipoproyectos.")");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $idp = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option <?php if($tpp==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
        <div class="col-md-4"><strong>Unidad de medida:</strong>
        <div class="ui corner labeled input">
         <select name="selunidad" id="selunidad" class="form-control input-sm selectpicker"  data-live-search="true" data-live-search-placeholder="Buscar Unidad" data-actions-box="true">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int in(".$unidades.")");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option <?php if($uni==$idu){echo 'selected';}?> value="<?Php echo $idu;?>"><?php echo $cod;?></option>
            <?php
		 }
		 ?>		 
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>         
    </div>
  <div class="row">
         <div class="col-md-4"><strong>Pais:</strong>
         <div class="ui corner labeled input">
          <select name="selpais" id="selpais" class="form-control input-sm" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Departamento:</strong>
         <div class="ui corner labeled input">
             <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
			<?php
            $con = mysqli_query($conectar,"select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and est_clave_int = 1");
            while($dat = mysqli_fetch_array($con))
            {
            ?>
                <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
            <?php
            }
            ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
   
         <div class="col-md-4"><strong>Ciudad:</strong>
         <div class="ui corner labeled input">
             <select name="selciudad" id="selciudad" class="form-control input-sm">
         <option value="">--seleccione--</option>
       <?php
	     $con = mysqli_query($conectar,"select ciu_clave_int,ciu_nombre from ciudad where dep_clave_int ='".$dep."' and est_clave_int = 1");
	 while($dat = mysqli_fetch_array($con))
	 {
     ?>
     <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
     <?php
		}
	 ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
     </div>
     
      <?php  echo "<style onload=CRUDACTIVIDADES('AGREGADOS','".$ida."')></style>"?>
    <div class="row">
    <hr class="divider">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-left" id="nav1" title="ASIGNACION DE APU Y SUBANALISIS">
                    <li class="active">
                    <a onclick="CRUDACTIVIDADES('AGREGADOS','<?PHP echo $ida;?>');" data-toggle="tab" aria-expanded="true"><strong>APU</strong></a>
                    </li>
                    <li>
                    <a onclick="CRUDACTIVIDADES('AGREGADOSSUB','<?PHP echo $ida;?>');" data-toggle="tab" aria-expanded="true"><strong>SUB-ANALISIS</strong></a>
                    </li>

                    </ul>
                </div>

    	<div class="col-md-12">
           <div id="pendientes"></div>
        </div>
      <div class="col-md-6"></div>
        <?php
		$cons = mysqli_query($conectar,"SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int join insumos r on r.ins_clave_int  = a.ins_clave_int where i.act_clave_int = '".$ida."'");
		$dats = mysqli_fetch_array($cons);
		$total = $dats['tot'];
		$cons2 = mysqli_query($conectar,"SELECT sum((a.ati_rendimiento*a.ati_valor)*ats_rendimiento) as tot FROM  act_subanalisis AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_subanalisis join insumos r on r.ins_clave_int = a.ins_clave_int where i.act_clave_int = '".$ida."'");
		$dats2 = mysqli_fetch_array($cons2);
		$total2 = $dats2['tot'];
		$total = $total + $total2;
		?>
        <div class="col-md-6" style="text-align:right">
        <strong>Total Apu: <span class="currency" id="totalapu"><?php echo number_format($total,2,'.',',');?></span>  </strong>      
     	</div>
     </div>
    </div>
  </form>
  <?PHP  
  }
   else if($opcion=="GUARDAR")
  {
     $tipoproyecto = $_POST['tipoproyecto'];
	 $unidad = $_POST['unidad'];
	 $ciudad = $_POST['ciudad'];
	 $descripcion = mysqli_real_escape_string($conectar,$_POST['descripcion']);
	 $analisis  = $_POST['analisis'];
	 
	 $veri = mysqli_query($conectar,"select * from actividades where UPPER(act_nombre) = UPPER('".$descripcion."') and est_clave_int not in(2,4) and ciu_clave_int ='".$ciudad."' and tpp_clave_int ='".$tipoproyecto."'");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $ins = mysqli_query($conectar,"insert into actividades(act_nombre,tpp_clave_int,uni_clave_int,act_analisis,est_clave_int,ciu_clave_int,act_usu_actualiz,act_fec_actualiz,act_usu_creacion,act_fec_creacion) values('".$descripcion."','".$tipoproyecto."','".$unidad."','".$analisis."','".$estado."','".$ciudad."','".$usuario."','".$fecha."','".$idUsuario."','".$fecha."')");
		if($ins>0)
		{
		   $ida = mysqli_insert_id($conectar);
		   $con = mysqli_query($conectar,"select * from insumospendientes where usu_clave_int ='".$idUsuario."'");
		   $num = mysqli_num_rows($con);
		   if($num>0 and $ida>0)
		   {
$inse = mysqli_query($conectar,"insert into actividadinsumos select '','".$ida."',ins_clave_int,inp_rendimiento,inp_valor,'".$usuario."','".$fecha."','' from insumospendientes where usu_clave_int='".$idUsuario."'");
			   if($inse>0)
			   {
			      $vaciar = mysqli_query($conectar,"delete from insumospendientes where usu_clave_int = '".$idUsuario."'");
			   }
		   }
		   $con2 = mysqli_query($conectar,"select * from act_sub_pendientes where usu_clave_int ='".$idUsuario."'");
		   $num2 = mysqli_num_rows($con);
		   if($num2>0 and $ida>0)
		   {
$inse2= mysqli_query($conectar,"insert into act_subanalisis (ats_clave_int,act_clave_int,act_subanalisis,ats_rendimiento,ats_usu_actualiz,ats_fec_actualiz) select '','".$ida."',act_clave_int,asp_rendimiento,'".$usuario."','".$fecha."' from act_sub_pendientes where usu_clave_int ='".$idUsuario."'");
			   if($inse2>0)
			   {
			      $vaciar2 = mysqli_query($conectar,"DELETE FROM act_sub_pendientes where usu_clave_int ='".$idUsuario."'");
			   }
		   }
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  } 
  else if($opcion=="GUARDAREDICION" )
  {
	 $ide =$_POST['id'];
    $tipoproyecto = $_POST['tipoproyecto'];
	 $unidad = $_POST['unidad'];
	 $ciudad = $_POST['ciudad'];
	 $descripcion = mysqli_real_escape_string($conectar,$_POST['descripcion']);
	 $analisis  = $_POST['analisis'];
	 
	 $veri = mysqli_query($conectar,"select * from actividades where UPPER(act_nombre) = UPPER('".$descripcion."') and act_clave_int!='".$ide."' and est_clave_int not in(2,4) and ciu_clave_int ='".$ciudad."' and tpp_clave_int ='".$tipoproyecto."' and act_clave_int not in(".$actividades.")");
	 $numv = mysqli_num_rows($veri);
	 if($numv>0)
	 {
        echo 2;
	 }
	 else 
	 {
        $upd = mysqli_query($conectar,"update actividades set act_nombre='".$descripcion."',tpp_clave_int='".$tipoproyecto."',uni_clave_int='".$unidad."',ciu_clave_int='".$ciudad."',act_usu_actualiz='".$usuario."',act_fec_actualiz='".$fecha."' where act_clave_int = '".$ide."'");
		if($upd>0)
		{
		  
		   echo 1;
		}
		else
	    {
		  echo 3;
		}	
	 }	 
  }
    else if($opcion=="CARGARLISTAACTIVIDADES" || $opcion=="CARGARLISTAACTIVIDADESSUB")
  {
	  ?> <script src="js/jsactividades1.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
      <!--	<div id="resize_wrapper">-->
       
      <table id="tbactividades" style="font-size:<?php echo $fontsize;?>px" class="table table-bordered table-condensed compact table-hover" width="100%">
                <thead>
                <tr>
                <th class="dt-head-center" style="width:20px"></th>
                <th class="dt-head-center" style="width:20px"></th>
                <th class="dt-head-center" style="width:20px"></th>
                <th class="dt-head-center" style="width:20px">
                <?php if(strtoupper($perfil)=="ADMINISTRADOR")
				{?>
                <a role="button" class="btn btn-danger btn-xs" id="eliminar" style="width:20px; height:20px"><i class="fa fa-trash"></i></a><?PHP } ?>
</th>
                <th class="dt-head-center" style="width:20px">CÓDIGO</th>               
                <th class="dt-head-center" style="width:300px">DESCRIPCIÓN</th>               
                <th class="dt-head-center" style="width:30px">UND</th>  
                <th class="dt-head-center" style="width:40px">V/R APU</th>  
                <th class="dt-head-center" style="width:60px">CIUDAD</th> 
                <th class="dt-head-center" style="width:80px">TIPO PROYECTO</th>                            
                <th class="dt-head-center" style="width:40px">ESTADO</th>
                <th class="dt-head-center" style="width:40px">ANALISÍS</th>
                
                </tr>
                </thead>
               
                 <tfoot>
                <tr>
                 <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>                
                <th class="dt-head-center"></th>
                <th class="dt-head-center">&nbsp;</th>               
                </tr>
                </tfoot>
                </table>
             <!--   <div id="resize_handle">Arrastre para cambiar el tamaño</div>
			</div>#resize_wrapper-->
           
  <?PHP

  }
  else if($opcion=="AGREGARPENDIENTE")
  {
      $insumo = $_POST['insumo'];
	  $rendimiento = $_POST['rendimiento'];
	  $con = mysqli_query($conectar,"select ins_valor as val from insumos where ins_clave_int = '".$insumo."' limit 1");
	  $dat = mysqli_fetch_array($con);
	  $valor = $dat['val']; if($valor=="null" || $valor=="" || $valor==NULL){$valor=0;}
	  
	  
	  $ins = mysqli_query($conectar,"insert into insumospendientes(ins_clave_int,inp_rendimiento,inp_valor,usu_clave_int) values ('".$insumo."','".$rendimiento."','".$valor."','".$idUsuario."')");
	  if($ins>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
 
  else if($opcion=="DELETEPENDIENTE")
  {
      $ide = $_POST['id'];
	  $dele = mysqli_query($conectar,"delete from insumospendientes where inp_clave_int = '".$ide."'");
	  if($dele>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  } 
   else if($opcion=="AGREGAR")
  {
	  $act = $_POST['id'];
      $insumo = $_POST['insumo'];
	  $rendimiento = $_POST['rendimiento'];
	  $con = mysqli_query($conectar,"select ins_valor val from insumos where ins_clave_int = '".$insumo."' ");
	 $dat = mysqli_fetch_array($con);
	 $valor = $dat['val']; if($valor=="" || $valor==NULL){$valor=0;}
	  $ins = mysqli_query($conectar,"insert into actividadinsumos(act_clave_int,ins_clave_int,ati_rendimiento,ati_valor) values ('".$act."','".$insumo."','".$rendimiento."','".$valor."')");
	  if($ins>0)
	  {
		  $cona = mysqli_query($conectar,"select pre_clave_int,gru_clave_int,cap_clave_int from pre_gru_cap_act_insumo where act_clave_int = '".$act."' group by pre_clave_int,gru_clave_int,cap_clave_int");
		  $numa = mysqli_num_rows($cona);
		  if($numa>0)
		  {
			  for($k=0;$k<$numa;$k++)
			  {
				  $data = mysqli_fetch_array($cona);
				  $pre = $data['pre_clave_int'];
				  $gru = $data['gru_clave_int'];
				  $cap = $dat['cap_clave_int'];
				  $insi = mysqli_query($conectar,"insert into pre_gru_cap_act_insumo(pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int,ins_clave_int,pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_usu_actualiz,pgi_fec_actualiz) select '".$pre."','".$gru."','".$cap."','".$act."','".$insumo."','".$rendimiento."','".$valor."','".$rendimiento."','".$valor."','".$usuario."','".$fecha."')");
			  }
		  }
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
   else if($opcion=="DELETE")
  {
	  $ide= $_POST['id'];
	  $cona = mysqli_query($conectar,"select act_clave_int,ins_clave_int from actividadinsumos where ati_clave_int = '".$ide."' limit 1");
	  $data = mysqli_fetch_array($cona); $act = $data['act_clave_int']; $insu = $data['ins_clave_int'];
	  $ins = mysqli_query($conectar,"delete from actividadinsumos where ati_clave_int = '".$ide."'");
	  if($ins>0)
	  {
		  $delete = mysqli_query($conectar,"delete from pre_gru_cap_act_insumo where act_clave_int ='".$act."' and ins_clave_int = '".$insu."'");
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
  
  //agregasr subanalisis
    else if($opcion=="AGREGARPENDIENTESUB")
  {
      $actividad = $_POST['suba'];
	  $rend = $_POST['rend'];
	  
	  $ins = mysqli_query($conectar,"insert into act_sub_pendientes(act_clave_int,usu_clave_int,asp_rendimiento) values ('".$actividad."','".$idUsuario."','".$rend."')");
	  if($ins>0)
	  {
		  echo 1;
	  }
	  else	  
	  {
		  echo 2;
	  }
  }
 
  else if($opcion=="DELETEPENDIENTESUB")
  {
      $ide = $_POST['id'];
	  $dele = mysqli_query($conectar,"delete from act_sub_pendientes where asp_clave_int = '".$ide."'");
	  if($dele>0)
	  {
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  } 
   else if($opcion=="AGREGARSUB")
  {
	  $act = $_POST['id'];
      $sub = $_POST['suba'];
	  $rend = $_POST['rend'];	 
	
	  $ins = mysqli_query($conectar,"insert into act_subanalisis(act_clave_int,act_subanalisis,ats_usu_actualiz,ats_fec_actualiz,ats_rendimiento) values ('".$act."','".$sub."','".$usuario."','".$fecha."','".$rend."')");
	  if($ins>0)
	  {  
		  $res =  1;
	  }
	  else
	  {
		  $res =  2;
	  }
		$cons = mysqli_query($conectar,"SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int join insumos r on r.ins_clave_int  = a.ins_clave_int where i.act_clave_int = '".$act."'");
		$dats = mysqli_fetch_array($cons);
		$total = $dats['tot'];
		
		$cons2 = mysqli_query($conectar,"SELECT sum((a.ati_rendimiento*a.ati_valor)*ats_rendimiento) as tot FROM  act_subanalisis AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_subanalisis join insumos r on r.ins_clave_int = a.ins_clave_int where i.act_clave_int = '".$act."'");
		$dats2 = mysqli_fetch_array($cons2);
		$total2 = $dats2['tot'];
		$total = $total + $total2;	
	  
	 $datos[] = array("res"=>$res,"valor"=>"$".number_format($total,2,'.',','));
	 echo json_encode($datos);
  }
   else if($opcion=="DELETESUB")
  {
	  $ide= $_POST['id'];
	  $cona = mysqli_query($conectar,"select act_clave_int,act_subanalisis from act_subanalisis where ats_clave_int = '".$ide."' limit 1");
	  $data = mysqli_fetch_array($cona); $act = $data['act_clave_int']; $sub = $data['act_subanalisis'];
	  $ins = mysqli_query($conectar,"delete from act_subanalisis where ats_clave_int = '".$ide."'");
	  
		$cons = mysqli_query($conectar,"SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int join insumos r on r.ins_clave_int  = a.ins_clave_int where i.act_clave_int = '".$act."'");
		$dats = mysqli_fetch_array($cons);
		$total = $dats['tot'];
		
		$cons2 = mysqli_query($conectar,"SELECT sum((a.ati_rendimiento*a.ati_valor)*ats_rendimiento) as tot FROM  act_subanalisis AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_subanalisis join insumos r on r.ins_clave_int = a.ins_clave_int where i.act_clave_int = '".$act."'");
		$dats2 = mysqli_fetch_array($cons2);
		$total2 = $dats2['tot'];
		$total = $total + $total2;	
	  
	  if($ins>0)
	  {
		//  $delete = mysqli_query($conectar,"delete from pre_gru_cap_act_insumo where act_clave_int ='".$act."' and ins_clave_int = '".$ins."'");
		  $res =  1;
	  }
	  else
	  {
		  $res =  2;
	  }
	  $datos[] = array("res"=>$res,"valor"=>"$".number_format($total,2,'.',','));
	  echo json_encode($datos);
  }
  else
  if($opcion=="PENDIENTESSUB")
  { 
    ?>
     <script src="js/jsactagregadas.js"></script>
      <script src="js/jsactividadesporagregar.js"></script>
    <div class="panel panel-default">  
   	 	<div class="panel-body">
            <div class="col-md-6">
              <table id="tbactividadporagregar" class=" table table-bordered table-condensed compact" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th class="dt-head-center" style="width:20px"></th> 
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center">REND</th>   
                <th class="dt-head-center">TIPO</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">CIUDAD</th>               
                </tr>
                </thead>
                <tfoot>
                <tr>
                <th class="dt-head-center" style="width:20px"></th> 
                <th class="dt-head-center"></th> 
                <th class="dt-head-center"></th>  
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>             
                </tr>
                </tfoot>

         </table>
            </div>
            <div class="col-md-6">
              <table id="tbactividadagregados" class=" table table-bordered table-condensed compact" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th class="dt-head-center" style="width:20px"></th>
                
                <th class="dt-head-center">NOMBRE</th>  
                <th class="dt-head-center">REND</th> 
                <th class="dt-head-center">TIPO</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">CIUDAD</th>               
                </tr>
                </thead>
                <tfoot>
                <tr>
                <th class="dt-head-center"></th> 
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>  
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>             
                </tr>
                </tfoot>

         </table>
            </div>
    	</div>
    </div>
    <?php
  }
  else if($opcion=="AGREGADOSSUB")
  {
	  
	  $id = $_POST['id'];
	  $coninfo = mysqli_query($conectar,"select act_usu_creacion,est_clave_int from actividades a  where act_clave_int = '".$id."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	  $usu = $datinfo['act_usu_creacion'];
	  $esta = $datinfo['est_clave_int'];
	  if($usu == $idUsuario){$per="si";}else{$per = "no";}
	  ?>
      <script src="js/jssubeditaragregados.js"></script>
      <script src="js/jssubeditarporgregar.js"></script>
      
   <div class="panel panel-default">  
   <div class="panel-body">
            <div class="col-md-6">
              <table id="tbactividadporagregar" class=" table table-bordered table-condensed compact" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th class="dt-head-center" style="width:20px"></th> 
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center">REND</th>  
                <th class="dt-head-center">TIPO</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">CIUDAD</th>               
                </tr>
                </thead>
                <tfoot>
                <tr>
                <th class="dt-head-center" style="width:20px"></th> 
                <th class="dt-head-center"></th> 
                <th class="dt-head-center"></th>  
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>             
                </tr>
                </tfoot>

         </table>
            </div>
            <div class="col-md-6">
              <table id="tbactividadagregados" class=" table table-bordered table-condensed compact" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th class="dt-head-center" style="width:20px"></th> 
                <th class="dt-head-center">NOMBRE</th> 
                <th class="dt-head-center">REND</th>  
                <th class="dt-head-center">TIPO</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">CIUDAD</th>               
                </tr>
                </thead>
                <tfoot>
                <tr>
                <th class="dt-head-center" style="width:20px"></th> 
                <th class="dt-head-center"></th>  
                <th class="dt-head-center"></th> 
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>
                <th class="dt-head-center"></th>             
                </tr>
                </tfoot>

         </table>
            </div>
    	</div>
</div>
           
      <?PHP
  
  }
  else
  if($opcion=="PENDIENTES")
  {
	
	  ?>
      <script src="js/jsinsumospendientes.js"></script>
<!--      <script src="js/jsinsumosporagregar.js"></script>
-->      <div class="form-group" id="divagregarinsumo">
           <div class="col-md-10">
            	<div class="input-group"><span class="input-group-addon">Recursos:</span> 
                <?php 
				echo "<style onload=CRUDACTIVIDADES('CARGARLISTAPENDIENTES','')></style>";
				?>
            	<select name="selagregar" id="selagregar" onChange="CRUDACTIVIDADES('AGREGARPENDIENTE',this.value)" class="form-control input-sm selectpicker"  data-live-search="true" data-live-search-placeholder="Buscar Recurso" data-actions-box="true">
            	           
            	</select>
           		</div>
            </div>
             <div class="col-md-2">
           		<a onClick="CRUDACTIVIDADES('NUEVOINSUMOA','')" role="button" class="btn btn-block btn-primary">Nuevo Recurso<i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
                 
            
         </div>
         <div class="form-group" id="divnuevoinsumo">
         </div>
     <div class="panel panel-default">  
     <div class="panel-body">
  		<div class="col-md-6" style="display:none">
         <table id="tbinsumosporagregar" class=" table table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th class="dt-head-center"></th>                
                <th class="dt-head-center">TIPO</th>
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">REND</th>
                <th class="dt-head-center">V/R UNITARIO</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                <th></th>                
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>              
                </tr>
                </tfoot>

         </table>
  		</div>
       
       <div class="col-md-12" >
      <table id="tbinsumospendientes" class="table table-bordered table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th class="dt-head-center"></th>   
                <th class="dt-head-center">CODIGO</th>                
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">REND</th>
                <th class="dt-head-center">V/R UNITARIO</th>
                <th class="dt-head-center">TIPO</th>
               
                </tr>
                </thead>              
             <tfoot>
                <tr>
                <th colspan="5"></th>             
              <th  ></th> 
                
                <th></th>
                </tr>
                </tfoot>
                </table>
                </div>
                  </div>
</div>
           
      <?PHP
  }else
   if($opcion=="AGREGADOS")
  {
	  $id = $_POST['id'];
	  $coninfo = mysqli_query($conectar,"select act_usu_creacion,est_clave_int from actividades a  where act_clave_int = '".$id."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	  $usu = $datinfo['act_usu_creacion'];
	  $esta = $datinfo['est_clave_int'];
	
		
	  
	  if($usu == $idUsuario){$per="si";}else{$per = "no";}
	  ?>
      <script src="js/jsinsumosagregados.js"></script>
<!--      <script src="js/jsinsumoseditarporagregar.js"></script>
-->       <div class="form-group" id="divagregarinsumo2">
           <div class="col-md-10">
            	<div class="input-group"><span class="input-group-addon">Recursos:</span> 
                <?php 
				echo "<style onload=CRUDACTIVIDADES('CARGARLISTAAGREGADOS','".$id."')></style>";
				
				?>
            	<select name="selagregar3" id="selagregar3" onmouseover="inicializarid('selagregar3')" onChange="CRUDACTIVIDADES('AGREGAR',this.value)" class="form-control input-sm "  data-live-search="true" data-live-search-placeholder="Buscar Recurso" data-actions-box="true">
            	           
            	</select>
           		</div>
            </div>
             <div class="col-md-2">
           		<a onClick="CRUDACTIVIDADES('NUEVOINSUMO2A','')" role="button" class="btn btn-block btn-primary">Nuevo Recurso<i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
                 
            
         </div>
         <div class="form-group" id="divnuevoinsumo2">
         </div>
   <div class="panel panel-default">  
   <div class="panel-body">
      <div class="col-md-6" style="display:none">
             <table id="tbinsumosporagregar" class=" table table-condensed compact table-hover" style="font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th class="dt-head-center"></th>                
                <th class="dt-head-center">TIPO</th>
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">REND</th>
                <th class="dt-head-center">V/R UNITARIO</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                <th></th>                
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>              
                </tr>
                </tfoot>

         </table>
      </div>
             
       <div class="col-md-12">
      <table id="tbinsumospendientes" style=" font-size:<?php echo $fontsize;?>px" class="table table-condensed compact table-hover">
                <thead>
                <tr>
                <th class="dt-head-center"></th>                
                <th class="dt-head-center">TIPO</th>
                <th class="dt-head-center">NOMBRE</th>
                <th class="dt-head-center">UNIDAD</th>
                <th class="dt-head-center">REND</th>
                <th class="dt-head-center">V/R UNITARIO</th>
                
                </tr>
                </thead>
            
               <tfoot>
               <tr>
                <th colspan="4" style="text-align:right"></th> 
                 <th></th> 
                <th></th>
                </tr>
                </tfoot>
                </table>
                </div>
                <?php
	if($percla==1 and $esta==3)
	{
	?>
    <div class="row">
        <div class="col-md-6">
        <strong>Unidades a Unificar:</strong>
        <select name="unidadunificar" id="unidadunificar" multiple class="form-control input-sm selectpicker"  data-live-search="true" data-live-search-placeholder="Buscar Unidad" data-actions-box="true" style="width:100%">
         <?php 
		 $con = mysqli_query($conectar,"select u.uni_clave_int as id, uni_codigo as cod,uni_nombre nomu from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where a.act_clave_int = '".$id."' and u.est_clave_int = 3 group by id order by nomu");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['id'];
			 $nom = $dat['nomu'];
			 $cod = $dat['cod'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
        </select>
        </div>
        <div class="col-md-4">
         <strong>Unidad Base:</strong>
        <select name="unidadpor" id="unidadpor" class="form-control input-sm selectpicker"  data-live-search="true" data-live-search-placeholder="Buscar Unidad" data-actions-box="true" style="width:100%">
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 order by uni_codigo");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
        </select>
        </div>
        <div class="col-md-2"><br /><a class="btn btn-default" role="button" onclick="CRUDACTIVIDADES('GUARDARUNIFICAR','')"><i class="glyphicon glyphicon glyphicon-refresh"></i>Unificar</a></div>
    </div>
    <?php
	echo "<style onload=INICIALIZARLISTAS('')></style>";
	}
	
	?>
               </div>
</div>
           
      <?PHP
  }
  
  else if($opcion=="LISTAINSUMOS")
  {
	  
     $id=$_POST['id'];
     $pre=$_POST['pre'];
     $cap=$_POST['cap'];
	 $gru = $_POST['gru'];
	 
	 
	$con = mysqli_query($conectar,"select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$id."'   order by tip,tpl,nom,nomu");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$res = "si";
	  while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val']; if($val=="null" || $val==NULL){$val=0;}
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren']; if($ren=="null" || $ren==NULL){$ren=0;}
				   $tpl = $dat['tpl'];
				   $insu = $dat['id'];
				   
				   $con1 = mysqli_query($conectar,"select pgi_rendimiento,pgi_vr_unitario,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_gru_cap_act_insumo where ins_clave_int = '".$insu."' and pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$id."'");
				   $dato1 = mysqli_fetch_array($con1);
				   $renact = $dato1['pgi_rendimiento'];
				   $valact = $dato1['pgi_vr_unitario'];
				   $admact = $dato1['pgi_adm_act'];
					$impact = $dato1['pgi_imp_act'];
					$utiact = $dato1['pgi_uti_act'];			
					$ivaact = $dato1['pgi_iva_act'];
				   if($renact == '' || $renact ==0){ $renact = $ren; }
				  
				   $unidad = $cod;
				   $material=0; $equipo = 0; $mano = 0;
				   $mate = 0; $equi = 0; $man = 0;
				  
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				    if($valact == '' || $valact == 0 ){ $valact = $valor; }
					$totalc = $renact*$valact;
					$tadmact = ($valact*$admact)/100;
					$timpact = ($valact*$impact)/100;
					$tutiact = ($valact*$utiact)/100;
					$tivaact = ($tutiact*$ivaact)/100;
					$valact = $valact +($tadmact+$timpact+$tutiact+$tivaact);
					
				   $totalact = ($ren*$valor)-($totalc);
				   if($totalact == ''){ $totalact = 0; }
				   
				   
				   if($tpl==1){$material=$valor*$ren; $mate = $totalc;}else if($tpl==2){$equipo = $valor*$ren; $equi = $totalc;}else if($tpl==3){$mano = $valor*$ren; $man = $totalc;}
				   $total = $material + $equipo + $mano;
				   
				   if(round($ren,2)<=0){ $ren = rtrim($ren,'0');} else { $ren = number_format($ren,2,'.',','); } 
				   
				   $datos[] =  array("iddetalle"=>$idc,"insumo"=>$nom,"tipo"=>$tipo,"unidad"=>$unidad,"rendi"=>$ren,"valor"=>$valor,"valor1"=>number_format($valor,2,'.',','),"material"=>number_format($material,2,'.',','),"equipo"=>number_format($equipo,2,'.',','),"mano"=>number_format($mano,2,'.',','),"materiala"=>number_format($mate,2,'.',','),"equipoa"=>number_format($equi,2,'.',','),"manoa"=>number_format($man,2,'.',','),"total"=>$total,"renact"=>$renact,"valact"=>$valact,"totalact"=>$totalact,"res"=>$res,"insu"=>$insu);
					
			   }
	}
	else
	{
	  $res = "no";
	    $datos[] =  array("iddetalle"=>"","insumo"=>"","tipo"=>"","unidad"=>"","rendi"=>"","valor"=>"","valor1"=>"","material"=>"","equipo"=>"","mano"=>"","total"=>"","renact"=>"","valact"=>"","totalact"=>"","res"=>$res);
	}
			   
	echo json_encode($datos);
  
  }
  else if($opcion=="GUARDARDETALLEP")
  {
	  $id = $_POST['idd'];
	  $rend = $_POST['rend'];
	  $update = mysqli_query($conectar,"update insumospendientes set inp_rendimiento = '".$rend."' where inp_clave_int = '".$id."'");
	  //APU ACTIVIDAD
	$cona = mysqli_query($conectar,"SELECT sum(p.inp_rendimiento*p.inp_valor) as apu FROM insumospendientes p join insumos i on i.ins_clave_int = p.ins_clave_int where p.usu_clave_int ='".$idUsuario."'");
	$data = mysqli_fetch_array($cona);
	if($data['apu']=="" || $data['apu']==NULL){$valora=0;}else { $valora = $data['apu'];}
	  if($update>0)
	  {
		$res =  "ok"; 
	  }
	  else
	  {
		$res =  "error";
	  }
	  $datos[] = array("valor"=>"$".number_format($valora,2,'.',','),"res"=>$res);
	  echo json_encode($datos);
  }
  else if($opcion=="GUARDARDETALLE")
  {
     $id = $_POST['id'];
	 $act = $_POST['ida'];
	 $rendimiento = $_POST['rendimiento'];
	 $cona = mysqli_query($conectar,"select ins_clave_int,act_clave_int from actividadinsumos where ati_clave_int='".$id."' limit 1");
	 $data = mysqli_fetch_array($cona);
	 $ins = $data['ins_clave_int'];
	  $coninfo = mysqli_query($conectar,"select act_usu_creacion from actividades a  where act_clave_int = '".$act."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	  $usu = $datinfo['act_usu_creacion'];
	  if($usu == $idUsuario){$per="si";}else if($percla==1){$per = "si";}else if($metact==2){$per = "no";}else{$per = "no";}
	 
	 if($per=="no")
	 {
		 $res = 3;
		 $material=0; $equipo = 0; $mano = 0; $total = 0;
	 }
	 else if($per=="si" )
	 {
	 
		 $upd = mysqli_query($conectar,"update actividadinsumos set ati_rendimiento ='".$rendimiento."',ati_usu_actualiz='".$usuario."',ati_fec_actualiz='".$fecha."' where ati_clave_int ='".$id."'");
		 if($upd>0)
		 {
			
			$res =  1;
			//UPDATE PRE GRU CAP ACT INSUMO
			$updi = mysqli_query($conectar,"update pre_gru_cap_act_insumo set pgi_rend_ini = '".$rendimiento."' where act_clave_int ='".$act."' and ins_clave_int = '".$ins."'");
			
			//APU ACTIVIDAD
			 $cona = mysqli_query($conectar,"SELECT sum(a.ati_rendimiento*a.ati_valor) as apu FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int  JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int  JOIN departamento AS d on d.dep_clave_int = c.dep_clave_int  JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int join estados AS e on e.est_clave_int  = i.est_clave_int where i.act_clave_int ='".$act."'");
		 $data = mysqli_fetch_array($cona);
		 if($data['apu']=="" || $data['apu']==NULL){$valora=0;}else { $valora = $data['apu'];}
		 
			$cons2 = mysqli_query($conectar,"SELECT sum((a.ati_rendimiento*a.ati_valor)*ats_rendimiento) as tot FROM  act_subanalisis AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_subanalisis join insumos r on r.ins_clave_int = a.ins_clave_int where i.act_clave_int = '".$act."'");
			$dats2 = mysqli_fetch_array($cons2);
			$total2 = $dats2['tot'];
			$valora = $valora + $total2;	
		 //APU INSUMO
		  $coni = mysqli_query($conectar,"select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where d.ati_clave_int = '".$id."'   order by tip,tpl,nom,nomu");
		  $dati = mysqli_fetch_array($coni);
			$val = $dati['val'];
			$val1 = $dati['val1'];
			$ren = $dati['ren'];
			$tpl = $dati['tpl'];
			
			$material=0; $equipo = 0; $mano = 0;
			
			if($val1<=0){$valor = $val;}else{$valor=$val1;}
			if($tpl==1){$material=$valor*$ren;}else if($tpl==2){$equipo = $valor*$ren;}else if($tpl==3){$mano = $valor*$ren;}
			$total = $material + $equipo + $mano;
		 
			
		 }
		 else
		 {  $material=0; $equipo = 0; $mano = 0; $total = 0;
			$res = 2;
		 }
	 }
	 
	 $datos[] = array("valor"=>"$".number_format($valora,2,'.',','),"respuesta"=>$res,"material"=>"$".number_format($material,2,'.',','),"equipo"=>"$".number_format($equipo,2,'.',','),"mano"=>"$".number_format($mano,2,'.',','),"total"=>$total);
	 echo json_encode($datos);
  }
   else if($opcion=="GUARDARDETALLEV")
  {
     $id = $_POST['id']; 
	 $ida = $_POST['ida'];
	 
	 $valor = $_POST['valor'];
	 $upd = mysqli_query($conectar,"update actividadinsumos set ati_valor ='".$valor."',ati_usu_actualiz='".$usuario."',ati_fec_actualiz='".$fecha."' where ati_clave_int ='".$id."'");
	 if($upd>0)
	 {
        $res =  1;
		//APU ACTIVIDAD
		 $cona = mysqli_query($conectar,"SELECT sum(a.ati_rendimiento*a.ati_valor) as apu FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int  JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int  JOIN departamento AS d on d.dep_clave_int = c.dep_clave_int  JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int join estados AS e on e.est_clave_int  = i.est_clave_int where i.act_clave_int ='".$ida."'");
	 $data = mysqli_fetch_array($cona);
	 if($data['apu']=="" || $data['apu']==NULL){$valora=0;}else { $valora = $data['apu'];}
	 //APU INSUMO
	  $coni = mysqli_query($conectar,"select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where d.ati_clave_int = '".$id."'   order by tip,tpl,nom,nomu");
	  $dati = mysqli_fetch_array($coni);
		$val = $dati['val'];
		$val1 = $dati['val1'];
		$ren = $dati['ren'];
		$tpl = $dati['tpl'];
		
		$material=0; $equipo = 0; $mano = 0;
		
		if($val1<=0){$valor = $val;}else{$valor=$val1;}
		if($tpl==1){$material=$valor*$ren;}else if($tpl==2){$equipo = $valor*$ren;}else if($tpl==3){$mano = $valor*$ren;}
		$total = $material + $equipo + $mano;
	 
		
	 }
	 else
	 {  $material=0; $equipo = 0; $mano = 0; $total = 0;
	    $res = 2;
	 }
	 
	 $datos[] = array("valor"=>"$".number_format($valora,2,'.',','),"respuesta"=>$res,"material"=>"$".number_format($material,2,'.',','),"equipo"=>"$".number_format($equipo,2,'.',','),"mano"=>"$".number_format($mano,2,'.',','),"total"=>$total);
	 echo json_encode($datos); 
  }
    else if($opcion=="ELIMINAR")
  {
    $id = $_POST['id'];
    $veri = mysqli_query($conectar,"select count(*) as cp from pre_gru_cap_actividad pg join presupuesto p on (p.pre_clave_int = pg.pre_clave_int) where act_clave_int = '".$id."' and p.est_clave_int!=2");
	$datv = mysqli_fetch_array($veri);
	$nump = $datv['cp'];
	
	$veri = mysqli_query($conectar,"select count(*) as cp from pre_gru_cap_act_insumo pg join presupuesto p on (p.pre_clave_int = pg.pre_clave_int) where act_clave_int = '".$id."' and p.est_clave_int!=2");
	$datv = mysqli_fetch_array($veri);
	$nump1 = $datv['cp'];
	
	if($nump>0 || $nump1>0)
	{
		echo 3;
	}
	else
	{
		$update = mysqli_query($conectar,"delete from actividades where act_clave_int = '".$id."'");
		$delete = mysqli_query($conectar,"delete from actividadinsumos where act_clave_int = '".$id."'");
		//actividadinsumos
		//pre_gru_cap_actividad
		//pre_gru_cap_act_insumo
		if($update>0){  echo 1;}else {echo 2;}
	}
  }
  else if($opcion=="CARGARLISTAPENDIENTES")
  {
		$con = mysqli_query($conectar,"select distinct ins_clave_int, ins_nombre,uni_codigo,ins_valor from insumos i join unidades u on u.uni_clave_int = i.uni_clave_int where (i.est_clave_int = 1 or i.ins_clave_int in(".$insumos.")) and ins_clave_int not in(select ins_clave_int from insumospendientes where usu_clave_int = '".$idUsuario."') and ins_nombre!=''  order by ins_nombre");
		while($dat = mysqli_fetch_array($con))
		{
			$idi = $dat['ins_clave_int'];
			$nomi = $dat['ins_nombre']." - ".$dat['uni_codigo']." -  $".number_format($dat['ins_valor'],2,'.',',');
			$datos[] = array("id"=>$idi,"literal"=>$nomi);
		}
		echo json_encode($datos);
  }
   else if($opcion=="CARGARLISTAAGREGADOS"  || $opcion=="CARGARLISTAAGREGADOS2")
  {
	  $id = $_POST['id'];
	$conart = mysqli_query($conectar,"select distinct ins_clave_int from actividadinsumos where act_clave_int = '".$id."'");
	$numa = mysqli_num_rows($conart);
	if($numa>0)
	{
		$insi = array();
		for($k=0;$k<$numa;$k++)
		{
		$data = mysqli_fetch_array($conart);
		$codi = $data['ins_clave_int'];
		$insi[] = $codi;
		}
		$ins1 = implode(',',$insi);
	}
	else
	{
	$ins1=0;
	}
	  
		$con = mysqli_query($conectar,"select distinct  ins_clave_int, ins_nombre,ins_valor,uni_codigo from insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int where (i.est_clave_int = 1 or i.ins_clave_int in(".$insumos.")) and ins_clave_int not in(".$ins1.") order by ins_nombre");
		while($dat = mysqli_fetch_array($con))
		{
			$idi = $dat['ins_clave_int'];
			$nomi = $dat['ins_nombre'];
			$valor = $dat['ins_valor'];
			$codi  = $dat['uni_codigo'];
			 $lit = $nomi." - ".$codi." - ($".number_format($valor,2,'.',',').")";
			$datos[] = array("id"=>$idi,"literal"=>$lit);
		}
		echo json_encode($datos);
  }
  else if($opcion=="ACTUALIZARDATOS")
  {
	  $idactividad = $_POST['ida'];
	 $cona = mysqli_query($conectar,"sum(a.ati_rendimiento*a.ati_valor) as apu FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int  JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int  JOIN departamento AS d on d.dep_clave_int = c.dep_clave_int  JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int join estados AS e on e.est_clave_int  = i.est_clave_int where i.act_clave_int ='".$idactividad."'");
	 $data = mysqli_fetch_array($cona);
	 if($data['apu']=="" || $data['apu']==NULL){$valora=0;}else { $valora = $data['apu'];}
	 $datos[] = array("valor"=>number_format($valora,2,'.',','));
	 echo json_encode($datos);
	 
  }
  else if($opcion=="VALORINSUMO")
  {
     $id = $_POST['id'];
	 $con = mysqli_query($conectar,"select ins_valor val from insumos where ins_clave_int = '".$id."' ");
	 $dat = mysqli_fetch_array($con);
	 $val = $dat['val']; if($val=="" || $val==NULL){$val=0;}
	 $val  = number_format($val,1,'.',',');
	 $datos[] = array("valor"=>$val);
	 echo json_encode($datos);
  }
  else if($opcion=="ACTIVIDADESPORAPROBAR")
  {
	  ?> <script src="js/jsactividadesaprobar.js"></script>
     
        <div>
      <table id="tbactividadesaprobar" class="table table-condensed compact table-hover" width="100%" style="font-size:<?php echo $fontsize;?>px;">
                <thead>
                <tr>
                <th></th>
                <th>DESCARTAR</th>
                <th>APROBAR</th>
                <th>COORDINADOR</th>
                <th>PROYECTO</th>
                <th>CÓDIGO</th>               
                <th>NOMBRE</th>
                
                <th>UNIDAD</th>
                
                <th>V/R APU($)</th>
                <th>TIPO PROYECTO</th>
                <th>CIUDAD</th>                
                <th>ESTADO</th>
                <th>ANALISIS</th>
                <th>CREADA POR</th>
                <th>FECHA CREACIÓN</th>
                </tr>
                </thead>
               
                 <tfoot>
                <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>CÓDIGO</th>               
                <th>NOMBRE</th>
                <th>TIPO PROYECTO</th>
                <th>UNIDAD</th>
                <th>CIUDAD</th>
                <th>V/R APU($)</th>                
                <th>ESTADO</th>
                <th>ANALISIS</th>
                <th>CREADA POR</th>
                <th>FECHA CREACIÓN</th>
                </tr>
                </tfoot>
                </table>
                </div>
      <?PHP
	  echo "<style onload=INICIALIZARLISTAS('')></style>";

  }
  else if($opcion=="CARGARPROYECTOS")
  {
	  $cor = $_POST['cor'];
	  $con = mysqli_query($conectar,"select p.pre_clave_int,pre_nombre,tpp_nombre,ciu_nombre from presupuesto p join actividades a on a.pre_clave_int = p.pre_clave_int join tipoproyecto t on t.tpp_clave_int = p.tpp_clave_int join ciudad c on p.ciu_clave_int = c.ciu_clave_int where a.est_clave_int = 3 and p.pre_coordinador = '".$cor."' GROUP BY p.pre_clave_int ORDER BY p.pre_nombre");
	  $num  = mysqli_num_rows($con);
	  if($num>0)
	  { 
	 
		  while($dat = mysqli_fetch_array($con))
		  {
			  $idpr = $dat['pre_clave_int'];	
			  $nom = $dat['pre_nombre']." - ".$dat['tpp_nombre']." - ".$dat['ciu_nombre'];
		      $datos[] = array("id"=>$idpr,"literal"=>$nom,"res"=>"si");
		    
		  }
	  }
	  else
	  {
		  $datos[] = array("id"=>"","literal"=>"","res"=>"no");
	  }
		  echo json_encode($datos);
  }
  else if($opcion=="APROBARACTIVIDAD")
  {
     $id = $_POST['id'];
	 $sub = $_POST['suba'];
	 $cond = mysqli_query($conectar,"select tpp_clave_int,uni_clave_int,ciu_clave_int,act_usu_creacion from actividades where act_clave_int = '".$id."'");
	 $datd = mysqli_fetch_array($cond);
	 $tpp = $datd['tpp_clave_int'];
	 $uni = $datd['uni_clave_int'];
	 $ciu = $datd['ciu_clave_int'];
	 $con = mysqli_query($conectar,"update actividades set est_clave_int = 1,act_usu_actualiz = '".$usuario."',act_fec_actualiz = '".$fecha."' where act_clave_int ='".$id."'");
	 
	 if($con>0)
	 {
		 $upd = mysqli_query($conectar,"update tipoproyecto set est_clave_int = 1 where tpp_clave_int = '".$tpp."'");
		 $upd1 = mysqli_query($conectar,"update ciudad set est_clave_int = 1 where ciu_clave_int = '".$ciu."'");
		 $upd2 = mysqli_query($conectar,"update unidades set est_clave_int = 1 where uni_clave_int = '".$uni."'");
		 
		 //consultar insumos asociados a esta actividad y actualizar
		 $coni = mysqli_query($conectar,"select ins_clave_int from actividadinsumos where act_clave_int = '".$id."'");
		 $numi = mysqli_num_rows($coni);
		 if($numi>0)
		 {
	        for($ni=0;$ni<$numi;$ni++)
			{
			  $dati = mysqli_fetch_array($coni);
			  $idi = $dati['ins_clave_int'];
			  $udp3 = mysqli_query($conectar,"update insumos set est_clave_int = 1 where ins_clave_int ='".$idi."'");
			}
		 }
		 
		 $res = 1;
	 }
	 else
	 {
		 $res = 2;
	 }
	 $cona = mysqli_query($conectar,"select count(distinct a.act_clave_int) as cant from actividades a JOIN actividadinsumos AS i on a.act_clave_int = i.act_clave_int  where est_clave_int = '3'");
	 $data = mysqli_fetch_array($cona);
	 $pendi = $data['cant'];
	 $datos[] = array("respuesta"=>$res,"pendientes"=>$pendi);
	 echo json_encode($datos);
  }
    else if($opcion=="DESAPROBARACTIVIDAD")
  {
     $id = $_POST['id'];
	 $sub = $_POST['suba'];
	 $cond = mysqli_query($conectar,"select tpp_clave_int,uni_clave_int,ciu_clave_int from actividades where act_clave_int = '".$id."'");
	 $datd = mysqli_fetch_array($cond);
	 $tpp = $datd['tpp_clave_int'];
	 $uni = $datd['uni_clave_int'];
	 $ciu = $datd['ciu_clave_int'];
	 $con = mysqli_query($conectar,"update actividades set est_clave_int = 2,act_usu_actualiz = '".$usuario."',act_fec_actualiz = '".$fecha."' where act_clave_int ='".$id."'");
	 if($con>0)
	 {
		 //$upd = mysqli_query($conectar,"update tipoproyecto set est_clave_int = 4 where tpp_clave_int = '".$tpp."'");
		 //$upd1 = mysqli_query($conectar,"update ciudad set est_clave_int = 4 where ciu_clave_int = '".$ciu."'");
		 //$upd2 = mysqli_query($conectar,"update unidades set est_clave_int = 4 where uni_clave_int = '".$uni."'");
		 $res = 1;
	 }
	 else
	 {
		 $res = 2;
	 }
  $cona = mysqli_query($conectar,"select count(distinct a.act_clave_int) as cant from actividades a JOIN actividadinsumos AS i on a.act_clave_int = i.act_clave_int  where est_clave_int = '3'");
	 $data = mysqli_fetch_array($cona);
	 $pendi = $data['cant'];
	 $datos[] = array("respuesta"=>$res,"pendientes"=>$pendi);
	 echo json_encode($datos);
  }
  else if($opcion=="ACTUALIZARPENDIENTE")
  {
	    $sub = $_POST['suba'];
		$cona = mysqli_query($conectar,"select count(distinct a.act_clave_int) as cant from actividades a JOIN actividadinsumos AS i on a.act_clave_int = i.act_clave_int  where est_clave_int = '3' and act_analisis='".$sub."'");
		$data = mysqli_fetch_array($cona);
		$pendi = $data['cant'];
		$datos[] = array("pendientes"=>$pendi);
		echo json_encode($datos);
	 	
  }
  else if($opcion=="CREARACTIVIDAD")
  {
    
	  $ida = $_POST['actividad'];
	   $coninfo = mysqli_query($conectar,"select act_nombre as des,tpp_clave_int as tpp,uni_clave_int uni,act_analisis as ana,a.est_clave_int as est,c.ciu_clave_int ciu,d.dep_clave_int dep,p.pai_clave_int pai from actividades a join ciudad c on c.ciu_clave_int = a.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where act_clave_int = '".$ida."' limit 1");
	  $datinfo = mysqli_fetch_array($coninfo);
	
	  $tpp = $datinfo['tpp'];
	  $uni = $datinfo['uni'];
	  $ana = $datinfo['ana'];
	  $des = $datinfo['des'];
	  $est = $datinfo['est'];
	  $ciu = $datinfo['ciu'];
	  $pai = $datinfo['pai'];
	  $dep = $datinfo['dep'];
     ?>
     <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $ida;?>">
<div class="form-group">    
    <div class="col-md-4"><strong>Descripción:</strong>
    <div class="ui corner labeled input">
    <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" value="<?php echo $des;?>" placeholder="Escribe aqui la descripcion">
     <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
    </div>
    
         <div class="col-md-4"><strong>Tipo de Proyecto:</strong>
         <div class="ui corner labeled input">
         <select name="seltipoproyecto"id="seltipoproyecto" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpp_clave_int,tpp_nombre from tipoproyecto where est_clave_int = 1 or tpp_clave_int in(".$tipoproyectos.")");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $idp = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option <?php if($tpp==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
        <div class="col-md-4"><strong>Unidad de medida:</strong>
        <div class="ui corner labeled input">
         <select name="selunidad" id="selunidad" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option <?php if($uni==$idu){echo 'selected';}?> value="<?Php echo $idu;?>"><?php echo $cod;?></option>
            <?php
		 }
		 ?>		 
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
    </div>    
    
  <div class="form-group">
         <div class="col-md-4"><strong>Pais:</strong>
         <div class="ui corner labeled input">
          <select name="selpais" id="selpais" class="form-control input-sm" onChange="cargardepartamento('selpais','seldepartamento',1)">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Departamento:</strong>
         <div class="ui corner labeled input">
             <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
			<?php
            $con = mysqli_query($conectar,"select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and est_clave_int = 1");
            while($dat = mysqli_fetch_array($con))
            {
            ?>
                <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
            <?php
            }
            ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Ciudad:</strong>
         <div class="ui corner labeled input">
        <select name="selciudad" id="selciudad" class="form-control input-sm">
        <option value="">--seleccione--</option>
        <?php
        $con = mysqli_query($conectar,"select ciu_clave_int,ciu_nombre from ciudad where dep_clave_int ='".$dep."' and est_clave_int = 1");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
        <?php
        }
        ?>
        </select>
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
  
    
    <div class="form-group">
    	<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"> INSUMOS</h4>
  </div>
  <div class="panel-body">
             
       <div class="table-responsive">
       <script src="js/jsinsumosnuevos.js"></script>
      <table id="tbinsumosnuevos" class="table table-bordered table-condensed compact" style="width:100%; font-size:<?php echo $fontsize;?>px">
                <thead>
                <tr>
                <th>TIPO</th>
                <th>NOMBRE</th>
                <th>UNIDAD</th>
                <th>REND</th>
                <th>V/R ACTUAL</th>
                <th>VALOR</th>
                <th>DESCRIPCIÓN</th>
                </tr>
                </thead>
               <?PHP
		$con = mysqli_query($conectar,"select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where a.act_clave_int = '".$ida."'   order by tip,nom,nomu");
			?>
                <tbody>
               <?php
			   while($dat = mysqli_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   $ins = $dat['id'];
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				   
				
				   ?>
                <tr id="<?php echo $ins;?>">
             	  
                  <td data-title="Tipo"><?Php echo $tipo;?></td>
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title="Unidad"><?php echo $cod;?></td>
                  <td data-title="Rendimiento"><input style="width:90px;" class="form-control input-sm" onKeyPress="return NumCheck(event, this)" min="0" type="number" value="<?php echo $ren;?>" id="rend<?php echo $ins;?>"/></td>
                  <td><?php echo $valor;?></td>
                  <td data-title="Valor Unitario">$ <?php echo number_format($valor,2,'.',',');?></td>
                  <td data-title="Descripcion"><?php echo $des;?></td>
                 
                </tr>
                <?php
			   }
			   ?>
                </tbody>
               <tfoot>
                <tr>            
                
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                </tr>
                </tfoot>
                </table>
                </div>
               </div>
</div>
    </div>
        
  </form>
     <?PHP
  
  }
  else if($opcion=="GUARDARNUEVA")
  {
	$ida = $_POST['actividad'];
	$coninfo = mysqli_query($conectar,"select act_nombre as des,tpp_clave_int as tpp,c.ciu_clave_int ciu from actividades a join ciudad c on c.ciu_clave_int = a.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where act_clave_int = '".$ida."' limit 1");
	$datinfo = mysqli_fetch_array($coninfo);	
	$tpp = $datinfo['tpp'];
	$des = strtoupper($datinfo['des']);
	$ciu = $datinfo['ciu'];
	
	$tipoproyecto = $_POST['tipoproyecto'];
	$unidad = $_POST['unidad'];
	$ciudad = $_POST['ciudad'];
	$descripcion = strtoupper($_POST['descripcion']); $desc = $_POST['descripcion'];
	$estado  = $_POST['estado'];
	$analisis  = $_POST['analisis'];
	if($des==$descripcion and $tpp==$tipoproyecto and $ciudad==$ciu)
	{
	   $resp = 2;
	}
	else
	{
		$insa = mysqli_query($conectar,"insert into actividades(act_nombre,tpp_clave_int,uni_clave_int,act_analisis,est_clave_int,ciu_clave_int,act_usu_actualiz,act_fec_actualiz,act_usu_creacion,act_fec_creacion) values('".$desc."','".$tipoproyecto."','".$unidad."','".$analisis."','".$estado."','".$ciudad."','".$usuario."','".$fecha."','".$idUsuario."','".$fecha."')");	   
		
		if($insa>0)
		{
			$idac = mysqli_insert_id($conectar);
			$resp = 1;
		}
		else
		{
			$idac = 0;
		   $resp = 3;
		}
	}
	$datos[] = array("respuesta"=>$resp,"actividad"=>$idac);
	echo json_encode($datos);
  }
  else if($opcion=="NUEVOINSUMO")
  {
     $actividad = $_POST['actividad'];
	 $ins = $_POST['insumo'];
	 $ren = $_POST['rendimiento'];
	 $valor = $_POST['valor'];
	 if($ins>0)
	 {
	 $insi = mysqli_query($conectar,"insert into actividadinsumos(act_clave_int,ins_clave_int,ati_rendimiento,ati_valor,ati_usu_actualiz,ati_fec_actualiz) values ('".$actividad."','".$ins."','".$ren."','".$valor."','".$usuario."','".$fecha."')");
	     if($insi>0)
		 {
			 echo 1;
	     }
	 }
  }
   else if($opcion=="NUEVOINSUMOA" || $opcion=="NUEVOINSUMO2A")
  {
	  if($opcion=="NUEVOINSUMOA"){$gu = "GUARDARNUEVOINSUMO"; $ca = "CANCELARNUEVOINSUMO";}else{ $gu = "GUARDARNUEVOINSUMO2"; $ca="CANCELARNUEVOINSUMO2";}
	  ?>
      <div class="col-md-8">
      <label>Nombre:</label>
       <input type="text" id="nombreinsumo" name="nombreinsumo" value="" class="form-control input-sm"/>
      </div>
      <div class="col-md-4">
      <label>Tipo Recurso:</label>
    <select name="tipoinsumo"id="tipoinsumo" class="form-control input-sm">
         <option value="">--seleccione--</option>
         <?PHP
		 $con = mysqli_query($conectar,"select tpi_clave_int,tpi_nombre from tipoinsumos where est_clave_int = 1");
		 while($dat = mysqli_fetch_array($con))
		 {
		    $id = $dat['tpi_clave_int'];
			$nomt = $dat['tpi_nombre'];
			?>
            <option value="<?php echo $id;?>"><?php echo $nomt;?></option>
            <?Php	
		 }
		 ?>
         </select>
      </div>
      <div class="col-md-4">
      <label>Unidad</label>
      <select name="unidadinsumo" id="unidadinsumo" class="form-control input-sm">
      <option value="">--seleccione--</option>
         <?php 
		 $con = mysqli_query($conectar,"select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int in(".$unidades.")");
		 while($dat = mysqli_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option  value="<?Php echo $idu;?>"><?php echo $cod;?></option>
            <?php
		 }
		 ?>		 
        </select>
      </div>
      <div class="col-md-4">
      <label>Valor Unit:</label>
      <input  name="valorinsumo" id="valorinsumo" class="form-control input-sm" type="text" value=""  onKeyPress="return NumCheck(event, this)" placeholder="">
      </div>      
      <div class="col-md-2"><br>
         <a role="button"  onClick="CRUDACTIVIDADES('<?php echo $gu;?>','')" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Guardar Nuevo Insumo">Guardar<i class="glyphicon glyphicon-floppy-disk" ></i></a>
         </div>
         <div class="col-md-2"><br>
         <a role="button"  onClick="CRUDACTIVIDADES('<?php echo $ca;?>','')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Cancelar creación nuevo Insumo">Cancelar<i class="fa fa-ban"></i></a>
      </div>
      <?PHP
  }
  else if($opcion=="GUARDARNUEVOINSUMO")
  {
	$ti = $_POST['ti'];
	$nom = $_POST['nom'];
	$uni = $_POST['uni'];
	$valor = $_POST['valor'];
	$insumos = 0;
	$con = mysqli_query($conectar,"select ins_clave_int from insumos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['ins_clave_int'];
		   $idsu[] = $ins;
	   }
	   $insumos = implode(',',$idsu);
	}
	if(strtoupper($perfil)=="ADMINISTRADOR"){ $estado=1;}else{$estado=3;}
	//$rend = $_POST['rend'];
	$veri = mysqli_query($conectar,"select * from insumo where UPPER(ins_nombre) = UPPER('".$nom."') and uni_clave_int = '".$uni."' and ins_valor='".$valor."' and (est_clave_int=1 or ins_clave_int in(".$insumos."))");
	$numv = mysqli_num_rows($numv);
		if($numv>0)
		{
		 $res  = "error1";//ya existe el insuo	 
		 $datos[] = array("valor"=>"","nombre"=>"","tipo"=>"","unidad"=>"","descripcion"=>"","res"=>$res,"rend"=>"","ins"=>""); 
		}
		else 
		{
			$sql = mysqli_query($conectar,"INSERT INTO insumos(ins_nombre,tpi_clave_int,ins_valor,uni_clave_int,ins_fec_actualiz,ins_usu_actualiz,est_clave_int,usu_clave_int) VALUES('".$nom."','".$ti."','".$valor."','".$uni."','".$fecha."','".$usuario."','".$estado."','".$idUsuario."')");
			if($sql>0)
			{
				$res = "ok";
				$ins =mysqli_insert_id($conectar);
				$con = mysqli_query($conectar,"select ins_valor val,ins_nombre,tpi_nombre,uni_codigo,ins_descripcion from insumos i join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on(u.uni_clave_int = i.uni_clave_int) where ins_clave_int = '".$ins."' limit 1");
				$dat = mysqli_fetch_array($con);
				$val = $dat['val']; if($val=="" || $val==NULL){$val=0;}			
				$datos[] = array("valor"=>"$".number_format($val,2,'.',','),"nombre"=>$dat['ins_nombre'],"tipo"=>$dat['tpi_nombre'],"unidad"=>$dat['uni_codigo'],"descripcion"=>$dat['ins_descripcion'],"res"=>$res,"rend"=>number_format(0,2,'.',','),"ins"=>$ins); 
			}
			else 
			{
				$res = "error2";
				$datos[]= array("valor"=>"","nombre"=>"","tipo"=>"","unidad"=>"","descripcion"=>"","res"=>$res,"rend"=>"","ins"=>"");
			}
	  }	
	  echo json_encode($datos);
  }
?>