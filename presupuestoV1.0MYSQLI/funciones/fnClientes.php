<?php
include ("../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
date_default_timezone_set('America/Bogota');
$con = mysqli_query($conectar,"select u.usu_clave_int as Id,u.usu_nombre as nomu,p.prf_descripcion as nomp,p.prf_clave_int as idp from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.usu_clave_int = '".$idUsuario."'");
$dato = mysqli_fetch_array($con);
//$ven = $dato['ven_clave_int'];
$cla = $dato['Id'];
$claveusuario = $dato['Id'];
$nombreusuario = $dato['nomu'];
$percla = $dato['idp'];
$perfil = strtoupper($dato['nomp']);
/*if(strtoupper($perfil)=="ADMINISTRADOR"){*/ $estado=1;/*}else{$estado=3;}
*/
$clientes = 0;
	$con = mysqli_query($conectar,"select per_clave_int from persona where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysqli_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysqli_fetch_array($con);
		   $ins = $dat['per_clave_int'];
		   $idsu[] = $ins;
	   }
	   $clientes = implode(',',$idsu);
	}


$fecha=date("Y/m/d H:i:s");
  //LIVERA LOS INGRESO A TAL PRESUPUESTO
$update = mysqli_query($conectar, "UPDATE presupuesto_ingreso SET ped_ult_cierre = '".$fecha."',ped_estado = 0 WHERE usu_clave_int = '".$idUsuario."'");

$opcion = $_POST['opcion'];
if($opcion=="NUEVO")
{
	?>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group" style="display:none">
         <div class="col-md-12"><strong>Tipo Persona:</strong>
         <select name="seltipopersona" id="seltipopersona" class="form-control input-sm" onChange="validartipopersona()">
         <option value="1" selected>Cliente</option>
         <option value="2">Coordinador</option>
         </select>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-6"><strong>Tipo Documento:</strong>
         <div class="ui corner labeled input">

         <select name="seltipodocumento" id="seltipodocumento" class="form-control input-sm">
         <option value="1">NIT</option>
         <option value="2" selected>Cédula Ciudadania</option>
         <option value="3">Cédula Extranjera</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-6"><strong>Número Documento:</strong>
         <div class="ui corner labeled input">

         <input  type="text" name="txtdocumento" id="txtdocumento"  class="form-control input-sm" placeholder="Ingrese número de documento"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <div class="form-group" id="divrazon">
         <div class="col-md-12"><strong>Nombre Cliente:</strong>
         <div class="ui corner labeled input">

         <input type="text"  name="txtrazon" id="txtrazon" class="form-control input-sm" placeholder="Ingrese el nombre del cliente"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
   
   
    <div class="form-group">
         <div class="col-md-4"><strong>Pais:</strong>
         <div class="ui corner labeled input">

          <select name="selpais" id="selpais" class="form-control input-sm select2" onChange="cargardepartamento('selpais','seldepartamento',1)" style="width:100%">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
         
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Departamento:</strong>
         <div class="ui corner labeled input">

           <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Ciudad:</strong>
         <div class="ui corner labeled input">

             <select name="selciudad" id="selciudad" class="form-control input-sm select2">
         <option value="">--seleccione--</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <h5 style="display: none;">INFORMACIÓN CONTACTO</h5>
     <div class="form-group" style="display: none;">
         <div class="col-md-6"><strong>Nombre:</strong>
         <div class="ui corner labeled input">

          <input type="text"  name="txtnombre" id="txtnombre" class="form-control input-sm" placeholder="Ingrese el nombre contacto"/>
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-6"><strong>Apellido:</strong>
         <div class="ui corner labeled input">

          <input type="text"  name="txtapellido" id="txtapellido" class="form-control input-sm" placeholder="Ingrese el apellido contacto"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <div class="form-group" style="display: none;">
         
         <div class="col-md-6"><strong>Télefono:</strong>
         <div class="ui corner labeled input">

         <input type="text"  name="txttelefono" id="txttelefono" class="form-control input-sm" placeholder="Ingrese la telefono contacto"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
  
         <div class="col-md-6"><strong>Correo Electrónico:</strong>
         <div class="ui corner labeled input">

          <input type="text"  name="txtcorreo" id="txtcorreo" class="form-control input-sm" placeholder="Ingrese el correo electrónico"/>
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <div class="form-group" id="divdescripcion">
         <div class="col-md-12"><strong>Descripciòn:</strong>
          <textarea  name="txtdescripcion" id="txtdescripcion" class="form-control input-sm" placeholder="Ingrese la descripciòn del cliente"/>
         </div>
    </div>
  </form>
  <?PHP
}
else
if($opcion=="EDITAR")
{	
	  $id = $_POST['id'];
	  $condatos = mysqli_query($conectar,"select per_clave_int, per_tipo_per,per_tipo_doc,per_documento,per_razon,per_nombre,per_apellido,c.ciu_clave_int as ciu,d.dep_clave_int dep,pa.pai_clave_int pai,per_telefono,per_correo,per_descripcion,p.est_clave_int est from persona p join ciudad c on c.ciu_clave_int = p.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais pa on pa.pai_clave_int = d.pai_clave_int where per_clave_int = '".$id."' limit 1");
	  $dat = mysqli_fetch_array($condatos);
	  $tp = $dat['per_tipo_per'];
	  $td = $dat['per_tipo_doc'];
	  $doc = $dat['per_documento'];
	  $raz = $dat['per_razon'];
	  $nom = $dat['per_nombre'];
	  $ape = $dat['per_apellido'];
	  $pai = $dat['pai'];
	  $dep = $dat['dep'];
	  $ciu = $dat['ciu'];
	  $tel = $dat['per_telefono'];
	  $cor = $dat['per_correo'];
	  $des = trim($dat['per_descripcion']);
	?>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
   <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    <div class="form-group" style="display:none">
         <div class="col-md-12"><strong>Tipo Persona:</strong>
         <select name="seltipopersona" id="seltipopersona" class="form-control input-sm" onChange="validartipopersona()">
         <option  value="1" selected>Cliente</option>
         <option  value="2">Coordinador</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-6"><strong>Tipo Documento:</strong>
         <div class="ui corner labeled input">

         <select name="seltipodocumento" id="seltipodocumento" class="form-control input-sm">
         <option <?PHP if($td==1){echo 'selected';}?> value="1">NIT</option>
         <option <?PHP if($td==2){echo 'selected';}?> value="2" selected>Cédula Ciudadania</option>
         <option <?PHP if($td==3){echo 'selected';}?> value="3">Cédula Extranjera</option>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-6"><strong>Número Documento:</strong>
         <div class="ui corner labeled input">

         <input  type="text" name="txtdocumento" id="txtdocumento"  class="form-control input-sm" placeholder="Ingrese número de documento" value="<?php echo $doc;?>"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <div class="form-group" id="divrazon" style="display:<?php if($tp==1){}else{echo'none';}?>">
         <div class="col-md-12"><strong>Nombre Cliente:</strong>
         <div class="ui corner labeled input">

         <input type="text"  name="txtrazon" id="txtrazon" class="form-control input-sm" placeholder="Ingrese el nombre del cliente" value="<?php echo $raz;?>"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-4"><strong>Pais:</strong>
         <div class="ui corner labeled input">
          <select name="selpais" id="selpais" class="form-control input-sm select2" onChange="cargardepartamento('selpais','seldepartamento',1)" style="width:100%">
         <option value="">--seleccione--</option>
		<?php
        $con = mysqli_query($conectar,"select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>         
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
         <div class="col-md-4"><strong>Departamento:</strong>
         <div class="ui corner labeled input">
           <select name="seldepartamento" id="seldepartamento" class="form-control input-sm  select2" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
         <option value="">--seleccione--</option>
			<?php
            $con = mysqli_query($conectar,"select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and est_clave_int = 1");
            while($dat = mysqli_fetch_array($con))
            {
            ?>
                <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
            <?php
            }
            ?>
         </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
          <div class="col-md-4"><strong>Ciudad:</strong>
         <div class="ui corner labeled input">

        <select name="selciudad" id="selciudad" class="form-control input-sm select2">
        <option value="">--seleccione--</option>
        <?php
        $con = mysqli_query($conectar,"select ciu_clave_int,ciu_nombre from ciudad where dep_clave_int ='".$dep."' and est_clave_int = 1");
        while($dat = mysqli_fetch_array($con))
        {
        ?>
        <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
        <?php
        }
        ?>
        </select>
         <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
    <h5 style="display: none;">INFORMACIÓN CONTACTO
    <div class="form-group" style="display: none;">
         <div class="col-md-6"><strong>Nombre:</strong>
         <div class="ui corner labeled input">

          <input type="text"  name="txtnombre" id="txtnombre" class="form-control input-sm" placeholder="Ingrese el nombre contacto" value="<?php echo $nom;?>"/>
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div> 	
         </div> 
         <div class="col-md-6"><strong>Apellido:</strong>
         <div class="ui corner labeled input">
         
          <input type="text"  name="txtapellido" id="txtapellido" class="form-control input-sm" placeholder="Ingrese el apellido contacto" value="<?php echo $ape;?>"/>
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
   
    
    
    <div class="form-group" style="display: none;">
        
      <div class="col-md-6"><strong>Télefono:</strong>
         <div class="ui corner labeled input">
         <input type="text"  name="txttelefono" id="txttelefono" class="form-control input-sm" placeholder="Ingrese la telefono contacto" value="<?php echo $tel; ?>"/>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div> 
         <div class="col-md-6"><strong>Correo Electrónico:</strong>
         <div class="ui corner labeled input">
          <input type="text"  name="txtcorreo" id="txtcorreo" class="form-control input-sm" placeholder="Ingrese el correo electrónico" value="<?php echo $cor;?>"/>
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
         </div>
    </div>
   
    <div class="form-group" id="divdescripcion"  style="display:<?php if($tp==1){}else{echo'none';}?>">
         <div class="col-md-12"><strong>Descripciòn:</strong>
          <textarea  name="txtdescripcion" id="txtdescripcion" class="form-control input-sm" placeholder="Ingrese la descripciòn del cliente"><?php echo $des;?></textarea>
         </div>
    </div>
  </form>
  <?PHP
}
else if($opcion=="GUARDAR")
{
	$tipopersona = $_POST['tipopersona'];
	$tipodocumento = $_POST['tipodocumento'];
	$documento = $_POST['documento'];
	$razon = $_POST['razon'];
	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
	$ciudad = $_POST['ciudad'];
	$telefono = $_POST['telefono'];
	$correo = $_POST['correo'];
	$descripcion = $_POST['descripcion'];
	$veri = mysqli_query($conectar,"select * from persona where (per_documento = '".$documento."' or per_correo='".$correo."') and (est_clave_int = 1 or per_clave_int in(".$clientes."))");
	$numv = mysqli_num_rows($veri);
	if($numv>0)
	{
		echo 2;
	}
	else
	{
	   $ins = mysqli_query($conectar,"insert into persona(per_tipo_per,per_tipo_doc,per_documento,per_razon,per_nombre,per_apellido,ciu_clave_int,per_telefono,per_correo,per_descripcion,est_clave_int,per_usu_actualiz,per_fec_actualiz,usu_clave_int) values('".$tipopersona."','".$tipodocumento."','".$documento."','".$razon."','".$nombre."','".$apellido."','".$ciudad."','".$telefono."','".$correo."','".$descripcion."','".$estado."','".$usuario."','".$fecha."','".$idUsuario."')");
	   if($ins>0)
	   {
	    echo 1;
	   }
	   else
	   {
	     echo 3;
	   }
	}
}
else if($opcion=="GUARDAREDICION")
{
	$ide = $_POST['id'];
	$tipopersona = $_POST['tipopersona'];
	$tipodocumento = $_POST['tipodocumento'];
	$documento = $_POST['documento'];
	$razon = $_POST['razon'];
	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
	$ciudad = $_POST['ciudad'];
	$telefono = $_POST['telefono'];
	$correo = $_POST['correo'];
	$descripcion = $_POST['descripcion'];
	$veri = mysqli_query($conectar,"select * from persona where (per_documento = '".$documento."' or per_correo='".$correo."') and per_clave_int!='".$ide."' and (est_clave_int=1 or per_clave_int in(".$clientes."))");
	$numv = mysqli_num_rows($veri);
	if($numv>0)
	{
		echo 2;
	}
	else
	{
	   $ins = mysqli_query($conectar,"update persona set per_tipo_per='".$tipopersona."',per_tipo_doc='".$tipodocumento."',per_documento='".$documento."',per_razon='".$razon."',per_nombre='".$nombre."',per_apellido='".$apellido."',ciu_clave_int='".$ciudad."',per_telefono='".$telefono."',per_correo='".$correo."',per_descripcion='".$descripcion."',per_usu_actualiz='".$usuario."',per_fec_actualiz='".$fecha."' where per_clave_int = '".$ide."'");
	   if($ins>0)
	   {
	    echo 1;
	   }
	   else
	   {
	     echo 3;
	   }
	}
}
else if($opcion=="CARGARLISTACLIENTES")
{
?>
 <script src="js/jsclientes.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
      <div>
<table id="tbclientes" class="table table-bordered table-condensed compact table-hover" style="font-size:11px">
  <thead>
    <tr>
      <th style="width:20px"></th>
      <th style="width:20px"></th>
      <th class="dt-head-center" style="vertical-align:middle">T.PERSONA</th>
      <th class="dt-head-center" style="vertical-align:middle">T.DOCUMENTO</th>
      <th class="dt-head-center" style="vertical-align:middle">DOC</th>
      <th class="dt-head-center" style="vertical-align:middle">CLIENTE</th>
      <th class="dt-head-center" style="vertical-align:middle">UBICACIÓN</th>
    </tr>

    <!--<tr>
      <th rowspan="2" style="width:20px"></th>
      <th rowspan="2" style="width:20px"></th>
      <th rowspan="2" class="dt-head-center" style="vertical-align:middle">T.PERSONA</th>
      <th rowspan="2" class="dt-head-center" style="vertical-align:middle">T.DOCUMENTO</th>
      <th rowspan="2" class="dt-head-center" style="vertical-align:middle">DOC</th>
      <th rowspan="2" class="dt-head-center" style="vertical-align:middle">CLIENTE</th>
      <th rowspan="2" class="dt-head-center" style="vertical-align:middle">UBICACIÓN</th>
      <th colspan="4" class="dt-head-center">INFORMACION CONTACTO</th>
      <th rowspan="2" class="dt-head-center" style="vertical-align:middle">ESTADO</th>
    </tr>
    <tr>
      <th class="dt-head-center">NOMBRE</th>     
     
      <th class="dt-head-center">TELÉFONO</th>
      <th class="dt-head-center">CORREO</th>
      <th class="dt-head-center">DESCRIPCIÓN</th>
      </tr>-->
  <thead>
  <tbody>
  <?php
  $con = mysqli_query($conectar,"select per_clave_int, per_tipo_per,per_tipo_doc,per_documento,per_razon,per_nombre,per_apellido,c.ciu_nombre as ciu,d.dep_nombre dep,pa.pai_nombre pai,per_telefono,per_correo,per_descripcion,p.est_clave_int as est,e.est_nombre as estn,p.usu_clave_int as usu from persona p join ciudad c on c.ciu_clave_int = p.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais pa on pa.pai_clave_int = d.pai_clave_int join estados e on e.est_clave_int = p.est_clave_int where p.est_clave_int = 1 or per_clave_int in(".$clientes.") order by per_tipo_per,per_nombre,per_apellido");
  while($dat = mysqli_fetch_array($con))
  {
	  $idc = $dat['per_clave_int'];
	  $tp = $dat['per_tipo_per'];
	  if($tp=="1"){$tp="Cliente";}else if($tp=="2"){$tp="Coordinador";}
	  $td = $dat['per_tipo_doc'];
	  if($td==1){$td="NIT";}else if($td==2){$td = "Cédula de Ciudadania";}else if($td==3){$td="Cédula Extranjera";}
	  $doc = $dat['per_documento'];
	  $raz = $dat['per_razon'];
	  $nom = $dat['per_nombre']." ".$dat['per_apellido'];
	  $pai = $dat['pai']."/".$dat['dep']."/".$dat['ciu'];
	  $tel = $dat['per_telefono'];
	  $cor = $dat['per_correo'];
	  $des = $dat['per_descripcion'];
	  $est = $dat['est'];
	  $usu = $dat['usu'];
	  $estnom = $dat['estn'];
	if($estnom=="Activo"){$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
	else {$est='<span class="label label-info pull-right">'.$estnom.'</span>';}
  ?>
   <!-- <tr id="row_per<?php echo $idc;?>">
      <td>
      <?php if($perfil=="ADMINISTRADOR" || $usu==$idUsuario) { ?>
      <a class="btn btn-block btn-default btn-xs" onClick="CRUDCLIENTES('EDITAR',<?PHP echo $idc;?>)" data-toggle="modal" data-target="#myModal" style="width:20px; height:20px"><i class="glyphicon glyphicon-pencil"></i></a><?PHP } ?></td>
      <td>
      <?php if($perfil=="ADMINISTRADOR" || $usu==$idUsuario) { ?>
      <a class="btn btn-block btn-danger btn-xs" onClick="CRUDCLIENTES('ELIMINAR',<?PHP echo $idc;?>)" style="width:20px; height:20px"><i class="glyphicon glyphicon-trash" ></i></a><?php } ?></td>
      <td><?php echo $tp;?></td>
      <td><?php echo $td;?></td>
      <td><?php echo $doc;?></td> 
      <td><?php echo $raz;?></td>
      <td><?php echo $pai;?></td>
      <td><?php echo $nom;?></td>
      <td><?php echo $tel; ?></td>
      <td><?php echo $cor;?></td>
      <td><?php echo $des;?></td>
      <td><?php echo $est;?></td>
    </tr>-->
    <tr id="row_per<?php echo $idc;?>">
      <td>
      <?php if($perfil=="ADMINISTRADOR" || $usu==$idUsuario) { ?>
      <a class="btn btn-block btn-default btn-xs" onClick="CRUDCLIENTES('EDITAR',<?PHP echo $idc;?>)" data-toggle="modal" data-target="#myModal" style="width:20px; height:20px"><i class="glyphicon glyphicon-pencil"></i></a><?PHP } ?></td>
      <td>
      <?php if($perfil=="ADMINISTRADOR" || $usu==$idUsuario) { ?>
      <a class="btn btn-block btn-danger btn-xs" onClick="CRUDCLIENTES('ELIMINAR',<?PHP echo $idc;?>)" style="width:20px; height:20px"><i class="glyphicon glyphicon-trash" ></i></a><?php } ?></td>
      <td class="dt-center"><?php echo $tp;?></td>
      <td class="dt-center"><?php echo $td;?></td>
      <td class="dt-center"><?php echo $doc;?></td> 
      <td class="dt-center"><?php echo $raz;?></td>
      <td class="dt-center"><?php echo $pai;?></td>
    </tr>
    <?php
  }
  ?>
  </tbody>
   <tfoot>
    <!--<tr>
     <th></th>
     <th></th>
      <th>T.PERSONA</th>
      <th>T.DOCUMENTO</th>
      <th>DOC</th>
      <th>CLIENTE</th>
      <th>UBICACIÓN</th>
      <th>NOMBRE</th>
      <th>TELÉFONO</th>
      <th>CORREO</th>
      <th>DESCRIPCIÓN</th>
      <th>ESTADO</th>
    </tr>-->
    <tr>
     <th></th>
     <th></th>
      <th>T.PERSONA</th>
      <th>T.DOCUMENTO</th>
      <th>DOC</th>
      <th>CLIENTE</th>
      <th>UBICACIÓN</th>
    </tr>
  </tfoot>
</table>
</div>

<?php
}
else 
if($opcion=="CARGACIUDAD")
{
	 $dep = $_POST['departamento'];
	 $pai = $_POST['pais']; 
	 $con = mysqli_query($conectar,"select ciu_clave_int,ciu_nombre from ciudad c join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where d.dep_clave_int ='".$dep."' and p.pai_clave_int = '".$pai."' and c.est_clave_int = 1 order by ciu_nombre");
	 while($dat = mysqli_fetch_array($con))
	 {
		 $datos[] = array("id"=>$dat['ciu_clave_int'],"literal"=>$dat['ciu_nombre']);
     }
	 echo json_encode($datos);
  }
  else if($opcion=="ELIMINAR")
  {
    $id = $_POST['id'];
	$update = mysqli_query($conectar,"update persona set est_clave_int = 2 where per_clave_int = '".$id."'");
	if($update>0){  echo 1;}else {echo 2;}
  }
  else if($opcion=="UNIFICAR")
  {	  
	 ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
    <div class="row">
        <div class="col-md-6">
        <strong>Clientes a Unificar:</strong>
        <select name="clienteunificar" id="clienteunificar" class="form-control input-sm selectpicker" multiple="multiple" data-live-search="true" data-live-search-placeholder="Buscar Cliente" style="width:100%">
		<?php 
        $con = mysqli_query($conectar,"select distinct per_clave_int,per_documento,per_nombre,per_apellido,per_razon from persona where per_tipo_per = 1 and est_clave_int = 1 order by per_nombre");
        while($dat = mysqli_fetch_array($con))
        {
			 $idp = $dat['per_clave_int'];
			  $doc = $dat['per_documento'];
			  $nom = $dat['per_nombre'];
			  $ape = $dat['per_apellido'];
			  $clie = $dat['per_razon']
			 ?>
			<option value="<?Php echo $idp;?>"><?Php echo $clie." - ".$doc;?></option>
			<?php
        }
        ?>	
        </select>
        </div>
        <div class="col-md-6">
         <strong>Cliente Base:</strong>
        <select name="clientebase" id="clientebase" class="form-control input-sm selectpicker"  data-live-search="true" data-live-search-placeholder="Buscar Unidad"style="width:100%">
        <option value="">--Seleccione--</option>
      <?php 
        $con = mysqli_query($conectar,"select distinct per_clave_int,per_documento,per_nombre,per_apellido,per_razon from persona where per_tipo_per = 1 and est_clave_int = 1 order by per_nombre");
        while($dat = mysqli_fetch_array($con))
        {
			$idp = $dat['per_clave_int'];
			  $doc = $dat['per_documento'];
			  $nom = $dat['per_nombre'];
			  $ape = $dat['per_apellido'];
			  $clie = $dat['per_razon']
			 ?>
			<option value="<?Php echo $idp;?>"><?Php echo $clie." - ".$doc;?></option>
			<?php
        }
        ?>
        </select>
        </div>
    </div>
    </form>
      
      <?php
	  echo "<style onload=INICIALIZARLISTAS('MODAL')></style>";
  
  }
  else if($opcion=="GUARDARUNIFICAR")
  {
	  $clientes = $_POST['clientes']; $clientes = implode(', ', (array)$clientes);
  	  $clientebase = $_POST['clientebase'];//cliente base
	  $conu = mysqli_query($conectar,"select per_clave_int from persona where per_clave_int in(".$clientes.")");
	  $cantu = mysqli_num_rows($conu);
	  if($cantu>0)
	  {
		  for($k=0;$k<$cantu;$k++)
		  {
			  $datu = mysqli_fetch_array($conu);
			  $idp = $datu['per_clave_int'];
			  $update1 = mysqli_query($conectar,"update presupuesto set per_clave_int = '".$clientebase."' where per_clave_int ='".$idp."'");
			  if($ipd!=$clientebase)
			  {
		  		$update4 = mysqli_query($conectar,"update persona set est_clave_int = 2 where per_clave_int = '".$idp."'");
			  }
		  }
		  $res = 1;
	  	  $msn = 'Clientes Unificados correctamente';
	  }
	  else
	  {
	     $res = 2;
	     $msn = 'No ha seleccionado ningun cliente a unificar';
	  }
  	$datos[] = array("res"=>$res,"msn"=>$msn);
 	 echo json_encode($datos);
  }
  
  ?>