$(function() {
    "use strict";

    function s() {
        return /(Android|webOS|Phone|iPad|iPod|BlackBerry|Windows Phone)/i.test(navigator.userAgent)
    }

    function e() {
        $(".homepage-header").css({
            width: $(window).width(),
            height: $(window).height()
        })
    }

    function a(s) {
        $(".vegas-container").each(function() {
            $(this).vegas("pause")
        }), $("#" + s)[0]._vegas && $("#" + s).vegas("play"), $("body").addClass("demo-playing")
    }
    applyJayBar(".topbar"), $(window).resize(function() {
        e()
    }).resize(), $("html").addClass("animate"), $("#do-open-menu").on("click", function(s) {
        s.preventDefault(), $(".homepage-menu").toggleClass("open")
    }), $("#do-donate").on("click", function(s) {
        s.preventDefault(), $(".paypal-form").submit()
    }), $("#do-show-menu").on("click", function(s) {
        s.preventDefault(), $("body").addClass("menu-shown")
    }), $("#do-hide-menu").on("click", function(s) {
        s.preventDefault(), $("body").removeClass("menu-shown")
    }), "#support" === window.location.hash && $(".support").addClass("support-shown"), $(window).on("hashchange", function() {
        console.log(window.location.hash), "#support" === window.location.hash ? $(".support").addClass("support-shown") : $(".support").removeClass("support-shown")
    }), $(".jaybar-button-email").off("click").on("click", function(s) {
        return window.location.hash = "#support", s.preventDefault(), s.stopPropagation(), !1
    }), $(".email").on("click", function(s) {
        document.location = "mailto:" + $(this).attr("href").replace("#", "@"), s.preventDefault()
    }), $(".topbar-menu-button").on("click", function(s) {
        $(this).toggleClass("topbar-menu-button-active"), $(".topbar-menu").toggleClass("topbar-menu-open"), s.preventDefault()
    }), $(window).on("load", function() {
        setTimeout(function() {
            $("#carbonads").size() || s() || $(".sponsors").load("/partials/adblocked.html")
        }, 5e3), s() && $("html").addClass("mobile"), $("body").addClass("loaded")
    }), $("#version").size() && $.ajax({
        url: "https://jaysalvat.github.io/vegas/releases/latest/metadata.js",
        dataType: "jsonp",
        jsonpCallback: "__metadata",
        cache: !0,
        success: function(s) {
            $("#version span").text(s.version.replace("v", "")).attr("title", "Released " + s.date.split(" ")[0])
        }
      
      
    }), $(".homepage-header").vegas({
        overlay: !0,
        delay: 1e4,
        slidesToKeep: 1,
        transition: "fade2",
        transitionDuration: 8e3,
        animation: "random",
        animationDuration: 1e4,
        slides: [{
            src: "/img/unsplash1.jpg",
            color: "#DBC9B3"
        }, {
            src: "/img/unsplash3.jpg",
            color: "#93CAC5"
        }, {
            src: "/img/unsplash2.jpg",
            color: "#D3A87E"
        }, {
            src: "/img/unsplash5.jpg",
            color: "#E2DC56"
        }, {
            src: "/img/unsplash4.jpg",
            color: "#DBC9B3"
        }, {
            src: "/img/unsplash6.jpg",
            color: "#93CAC5"
        }, {
            src: "/img/unsplash8.jpg",
            color: "#A58A6F"
        }],
        walk: function(s, e) {
            s = null, $(".homepage-info").find("a").css("color", e.color), $(".jaybar-buttons").css("background", e.color), $(".vegas-timer-progress").css("backgroundColor", e.color)
        }
    });
    var i = [{
            name: "city",
            src: "/img/unsplash1.jpg",
            valign: "55%"
        }, {
            name: "lion",
            src: "/img/unsplash2.jpg",
            valign: "40%"
        }, {
            name: "car",
            src: "/img/unsplash3.jpg",
            valign: "45%"
        }, {
            name: "bear",
            src: "/img/unsplash4.jpg",
            valign: "13%",
            halign: "0%"
        }, {
            name: "fruits",
            src: "/img/unsplash5.jpg",
            valign: "45%"
        }, {
            name: "surf",
            src: "/img/unsplash6.jpg",
            valign: "45%"
        }, {
            name: "sea",
            src: "/img/unsplash8.jpg",
            valign: "40%"
        }],
        t = localStorage.getItem("theme-index") || 0;
    t >= i.length && (t = 0);
    var n = i[t];
    t++, localStorage.setItem("theme-index", t), $(".documentation").addClass("theme-" + n.name), $(".documentation-header").vegas({
        overlay: "/js/vegas/dist/overlays/05.png",
        slides: [{
            src: n.src,
            valign: n.valign || "center"
        }]
    }), $("#do-test1-1").on("click", function(s) {
        var e = "test1-1";
        a(e), $("#" + e).vegas({
            slides: [{
                src: "/img/unsplash1.jpg"
            }, {
                src: "/img/unsplash2.jpg"
            }, {
                src: "/img/unsplash3.jpg"
            }, {
                src: "/img/unsplash4.jpg"
            }]
        }), $("body").vegas({
            overlay: !0,
            slides: [{
                src: "/img/unsplash1.jpg"
            }, {
                src: "/img/unsplash2.jpg"
            }, {
                src: "/img/unsplash3.jpg"
            }, {
                src: "/img/unsplash4.jpg"
            }]
        }), s.preventDefault()
    }), $("#do-test2-1").on("click", function(s) {
        var e = "test2-1";
        a(e), $("#" + e).vegas({
            delay: 7e3,
            shuffle: !0,
            timer: !1,
            firstTransition: "fade",
            firstTransitionDuration: 5e3,
            transition: "slideDown2",
            transitionDuration: 2e3,
            slides: [{
                src: "/img/unsplash1.jpg"
            }, {
                src: "/img/unsplash2.jpg"
            }, {
                src: "/img/unsplash3.jpg"
            }, {
                src: "/img/unsplash4.jpg"
            }]
        }), s.preventDefault()
    }), $("#do-test2-2").on("click", function(s) {
        var e = "test2-2";
        a(e), $("#" + e).vegas({
            timer: !1,
            transition: "slideLeft2",
            slides: [{
                src: "/img/unsplash1.jpg"
            }, {
                src: "/img/unsplash2.jpg",
                transition: "slideRight2"
            }, {
                src: "/img/unsplash3.jpg"
            }, {
                src: "/img/unsplash4.jpg",
                transition: "slideRight2"
            }]
        }), s.preventDefault()
    }), $("#do-test2-3").on("click", function(s) {
        var e = "test2-3";
        a(e), $("#" + e + "-console div").remove(), $("#" + e).vegas({
            slides: [{
                src: "/img/unsplash1.jpg"
            }, {
                src: "/img/unsplash2.jpg"
            }, {
                src: "/img/unsplash3.jpg"
            }, {
                src: "/img/unsplash4.jpg"
            }],
            init: function() {
                $("#" + e + "-console").append("<div>Init</div>")
            },
            play: function() {
                $("#" + e + "-console").append("<div>Play</div>")
            },
            walk: function(s) {
                $("#" + e + "-console div").size() >= 3 && $("#" + e + "-console div").first().remove(), $("#" + e + "-console").append("<div>Slide index " + s + " image /img/slide" + (s + 1) + ".jpg</div>")
            }
        }), s.preventDefault()
    }), $("#test2-4").vegas({
        slides: [{
            src: "/img/unsplash3.jpg"
        }],
        overlay: !0
    }), $(".overlay-list li").on("click", function(s) {
        var e;
        s.preventDefault(), $(".overlay-list li").removeClass("active"), e = $(this).attr("class").replace("overlay-", ""), e = "/js/vegas/dist/overlays/" + e + ".png", $(this).addClass("active"), $("#test2-4 .vegas-overlay").css("background-image", "url(" + e + ")"), $("#code-overlay").text("'" + e + "'")
    }), $("#do-test2-5").on("click", function(s) {
        var e = "test2-5";
        a(e), $("#" + e).vegas({
            slides: [{
                src: "/img/unsplash4.jpg"
            }, {
                src: "/img/unsplash3.jpg",
                video: ["/videos/video.mp4", "/videos/video.webm", "/videos/video.ogv"]
            }]
        }), s.preventDefault()
    }), $("#test3-1").vegas({
        delay: 3e3,
        slides: [{
            src: "/img/unsplash1.jpg"
        }, {
            src: "/img/unsplash2.jpg"
        }, {
            src: "/img/unsplash3.jpg"
        }, {
            src: "/img/unsplash4.jpg",
            valign: "25%"
        }]
    }), $(".transition-list li").on("click", function(s) {
        var e;
        s.preventDefault(), $(".transition-list li").removeClass("active"), e = $(this).text(), $(this).addClass("active"), $("#test3-1").vegas("options", "transition", e), $("#code-transition").text(e)
    }), $("#test3-2").vegas({
        slides: [{
            src: "/img/unsplash1.jpg"
        }, {
            src: "/img/unsplash2.jpg"
        }, {
            src: "/img/unsplash3.jpg"
        }, {
            src: "/img/unsplash4.jpg",
            valign: "25%"
        }],
        animation: "kenburns"
    }), $(".animation-list li").on("click", function(s) {
        var e;
        s.preventDefault(), $(".animation-list li").removeClass("active"), e = $(this).text(), $(this).addClass("active"), $("#test3-2").vegas("options", "animation", e), $("#code-animation").text(e)
    }), $("#test4-1").vegas({
        autoplay: !1,
        slides: [{
            src: "/img/unsplash1.jpg",
            valign: "40%"
        }, {
            src: "/img/unsplash2.jpg",
            valign: "25%"
        }, {
            src: "/img/unsplash3.jpg"
        }, {
            src: "/img/unsplash4.jpg"
        }],
        walk: function() {
            $("#test4-1 .label").text($("#test4-1").vegas("current") + 1)
        },
        pause: function() {
            $(".method-list li").removeClass("working"), $(".method-list .method-pause").addClass("working")
        },
        play: function() {
            $(".vegas-container").not(this).each(function() {
                $(this).vegas("pause")
            }), $(".method-list li").removeClass("working"), $(".method-list .method-play").addClass("working")
        }
    }), $(".method-list li").on("click", function(s) {
        var e;
        s.preventDefault(), $(".method-list li").removeClass("active"), e = $(this).data("method"), $(this).addClass("active"), "first" === e ? $("#test4-1").vegas("jump", 0) : "last" === e ? $("#test4-1").vegas("jump", 3) : $("#test4-1").vegas(e), $(".method-example").removeClass("active"), $(".method-example-" + e).addClass("active")
    }), $("#do-test4-2").on("click", function(s) {
        var e = "test4-2",
            i = [{
                src: "/img/unsplash-extra1.jpg"
            }, {
                src: "/img/unsplash-extra2.jpg"
            }, {
                src: "/img/unsplash-extra3.jpg"
            }];
        $(".next, .previous, .add", "#" + e).css("visibility", "visible"), a(e), $("#" + e).vegas({
            slides: [{
                src: "/img/unsplash3.jpg"
            }, {
                src: "/img/unsplash4.jpg"
            }],
            transition: "slideLeft2"
        }), $("#" + e + " .previous").on("click", function(s) {
            $("#" + e).vegas("options", "transition", "slideRight2").vegas("previous"), s.preventDefault()
        }), $("#" + e + " .next").on("click", function(s) {
            $("#" + e).vegas("options", "transition", "slideLeft2").vegas("next"), s.preventDefault()
        }), $("#" + e + " .add").on("click", function(s) {
            var a = $("#" + e).vegas("options", "slides");
            a.push(i[0]), i.shift(), $("#" + e).vegas("options", "slides", a).vegas("options", "transition", "slideDown").vegas("jump", a.length - 1).vegas("options", "transition", "slideLeft2"), 0 === i.length && $(this).fadeOut(), s.preventDefault()
        }), s.preventDefault()
    })
});