<?php
	include('data/Conexion.php');
	error_reporting(0);
	date_default_timezone_set('America/Bogota');
	function decrypt($string, $key)
	{
		$result = "";
		$string = base64_decode($string);
		for($i=0; $i<strlen($string); $i++) 
		{
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}
	function encrypt($string, $key) 
	{
		$result = "";	
		for($i=0; $i<strlen($string); $i++) 
		{	
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		return base64_encode($result);
	}

	
	$codigo = $_GET['codigo'];
	if($codigo=="")
	{
		echo '<script>alert("No hay codigo de verificación"); window.location.href="index.php";</script>';
	}
	else
	{
	
		$con = mysqli_query($conectar,"select * from recuperar where rec_codigo = '".$codigo."' and rec_estado = 1");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			echo '<script>alert("Este codigo ya fue usado anteriormente"); window.location.href="index.php";</script>';
		}
	}
	
	if($_GET['restablecer'] == "si")
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		sleep(1);
		$cod = $_GET['cod'];
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		
		$con = mysqli_query($conectar,"select * from recuperar where rec_codigo = '".$cod."' and rec_estado = 1");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><h4><i class='icon fa fa-ban'></i>Alerta!</h4>Este codigo ya fue usado anteriormente</div>";
		}
		else
		{
			if($con1 == $con2)
			{
				$con = mysqli_query($conectar,"select * from recuperar where rec_codigo = '".$cod."'");
				$dato = mysqli_fetch_array($con);
				$usu = $dato['usu_clave_int'];
				
				$con = mysqli_query($conectar,"update recuperar set rec_estado = 1 where rec_codigo = '".$cod."'");
				$con = mysqli_query($conectar,"update usuario set usu_clave = '".encrypt($con1,'p4v4svasquez')."' where usu_clave_int = '".$usu."'");
				
				if($con >= 1)
				{
					echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button><h4><i class='icon fa fa-check'></i>Alerta!</h4>Su contrase&ntilde;a a sido restablecida correctamente!</div>";
				}
				else
				{
					echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><h4><i class='icon fa fa-ban'></i>Alerta!</h4>Error al restablecer la contraseña</div>";
				}
			}
			else
			{
				echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><h4><i class='icon fa fa-ban'></i>Alerta!</h4>Las contraseñas no coinciden</div>";
			}
		}
		
		exit();
	
	}
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PRESUPUESTO | RESTABLECER CONTRASEÑA</title>
        <!-- Jquery 1.8.3 -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <!-- validaciones -->
  <script type="text/javascript" src="llamadas3.js"></script>
  <!-- ventana emergente -->
  <link rel="stylesheet" href="dist/css/reveal.css" />
  <script type="text/javascript" src="dist/js/jquery.reveal.js"></script>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    </head>
    
<body class="hold-transition login-page">
<div class="login-box" style="background-color:#fff">
  <div class="login-logo">
<img src="dist/img/LOGOGLOBAL.jpg" height="70" width="100%"/>
   <h6>Restablecer Contraseña </h6>
   
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <form name="form" id="form-login" action="data/validarUsuarios.php" method="post">
      <div class="form-group has-feedback">
        <input type="password" name="contrasena1" required id="contrasena1" class="form-control" placeholder="Contraseña Nueva" />
         <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
       <input type="password" name="contrasena2" required id="contrasena2" class="form-control" placeholder="Repetir Contraseña" />
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-6">
          <input type="button" onclick="RESTABLECER('<?php echo $codigo; ?>')" value="Restablecer" class="btn btn-primary btn-block btn-flat" style="cursor:pointer">
        </div>
           <div class="col-xs-6"><a href="index.php" class="btn">Iniciar Sesion</a>
         </div>
        <!-- /.col -->
      </div>
      <div class="row">
          <div class="col-md-12" id="recu"></div>
      </div>

   
    </form>

    <!-- /.social-auth-links -->


  </div>
  <p class="login-box-msg">
 <small> PAVAS S.A.S.<br>
Copyright © Todos los derechos reservados</small>
  </p>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->



<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
    </body>
</html>