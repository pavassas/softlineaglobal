<?php
	session_start();
	$idUsuario = $_COOKIE['usIdentificacion'];
	date_default_timezone_set('America/Bogota');
	include('Conexion.php');
	setcookie("usIdentificacion", "", time() - 3600, "/");
	setcookie("usuario", "", time() - 3600, "/");
    setcookie("sistema", "", time() - 3600, "/");
	session_destroy();
	header("LOCATION:../index.php");
?>