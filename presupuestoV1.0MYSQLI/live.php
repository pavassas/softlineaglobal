<?php

print "<pre>\n";

$clave_semaforo = 34;  // Clave de ejemplo para el semaforo

$sem_id = sem_get ($clave_semaforo,1,0666,1);
print "Se obtuvo el ID de semaforo $sem_id\n";

print "Esperando al semaforo...\n";

if (! sem_acquire ($sem_id))
{
    echo 'Ocurrio un fallo esperando al semaforo.';
}

print "Se obtuvo el permiso para disponer de la via a las " .
      date ('H:i:s') . "\n";

// Aqui deberian hacerse cualquier cantidad de cosas interesantes

print "Reteniendo la via durante 10 segundos...\n";

//sleep (10);  // Solo como ejemplo, vamos a detenernos por 10 segundos
             // antes de decirle al semaforo que la via esta libre de
             // nuevo.

//if (! sem_release ($sem_id))
    //die ('Ocurrio un fallo liberando la via');

//print "Listo, la via fue liberada.\n";

//print "</pre>\n";

?>