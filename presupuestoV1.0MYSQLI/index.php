<?php
	error_reporting(0);
	include('data/Conexion.php');
	header('Content-Type: text/html; charset=UTF-8');
	date_default_timezone_set('America/Bogota');
	
	if($_GET['varContrasena'] == 1 || $_GET['varContrasena'] == 2)
	{
		echo "<script>alert('Usuario o Contraseña incorrecta');</script>";
	}
	elseif($_GET['varContrasena'] == 3)
	{
		echo "<script>alert('Su cuenta esta inactiva');</script>";
	}
	elseif($_GET['varContrasena'] == 4)
	{
		echo "<script>alert('No tiene acceso a este sistema. Verifcar');</script>";
	}
	if($_GET['recuperar'] == "si")
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		sleep(1);
		$dat = $_GET['dat'];
		$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$dat."' or usu_email = '".$dat."'");
		$dato = mysqli_fetch_array($con);
		$usucla = $dato['usu_clave_int'];
		$usu = $dato['usu_usuario'];
		$ema = $dato['usu_email'];
		$act = $dato['usu_sw_activo'];
		$clave = $dato['usu_clave'];
		
		$con = mysqli_query($conectar,"select * from recuperar where usu_clave_int = '".$usucla."' and rec_estado = 0");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			$dato = mysqli_fetch_array($con);
			$random = $dato['rec_codigo'];
		}
		else
		{
			$length = 50;
			$random = "";
			$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // change to whatever characters you want
			while ($length > 0) {
				$random .= $characters[mt_rand(0,strlen($characters)-1)];
				$length -= 1;
			}
			$con = mysqli_query($conectar,"insert into recuperar(rec_codigo,usu_clave_int,rec_estado) values('".$random."','".$usucla."','0')");
		}
		
		if($dat != '')
		{
			if(($usu == $dat) || ($ema == $dat))
			{
				// asunto del email
				$subject = "Recuperacion Control Presupuesto";
				
				// Cuerpo del mensaje
				$mensaje = "------------------------------------------- \n";
				$mensaje.= " Solicitud de recuperacion                  \n";
				$mensaje.= "------------------------------------------- \n\n";
				$mensaje.= "Para restablecer su contrasena, solo debes presionar clic en el siguiente enlace:\n";
				$mensaje.= "Restablecer Contrasena: https://pavas.com.co/presupuesto/restablecer.php?codigo=".$random."\n";
				$mensaje.= "Si no puedes hacer clic en el enlace, entonces debes copiarlo y pegarlo en su navegador";
				
				$mensaje.= "FECHA:    ".date("d/m/Y")."\n";
				$mensaje.= "Enviado desde https://www.pavas.com.co \n";
				
				// headers del email
				$headers = "From: adminpavas@pavas.com.co\r\n";
				
				// Enviamos el mensaje
				if (mail($ema, $subject, $mensaje, $headers)) {
					echo "<div class='ok'>Su contrase&ntilde;a a sido recuperada satisfactoriamente<br> Por favor revise su correo $ema</div>";
					echo "<script> form.contrasena.value = ''; </script>";
				} else {
					echo "<div class='validaciones'>Error de enviÃƒÂ³</div>";
				}
			}
			else
			{
				echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
			}
		}
		else
		{
			echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
		}
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="dist/img/favicon.ico" rel="shortcut icon">
  <title>PRESUPUESTOS | INICIAR SESION</title>
  <!-- Jquery 1.8.3 -->
  <script  type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <!-- validaciones -->
  <script  type="text/javascript" src="llamadas3.js"></script>
  <!-- ventana emergente -->
  <link rel="stylesheet" href="dist/css/reveal.css" />
  <script  type="text/javascript" src="dist/js/jquery.reveal.js"></script>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="dist/vegas/vegas.css?<?php echo time();?>"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" style="background-color:#ffffff">
  <div class="login-logo" >
    <img src="dist/img/LOGOGLOBAL.jpg" height="90" width="auto"/>
    <h3>Sistema de Presupuesto Digital</h3>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <form name="form" id="form-login" action="data/validarUsuarios.php" method="post">
      <div class="form-group has-feedback">
        <input name="usuario" id="usuario" type="text" class="form-control" placeholder="Usuario" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="contrasena" id="contrasena" type="password" class="form-control" placeholder="Contraseña" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" name="guardar" class="btn btn-primary btn-block btn-flat">INICIO PRESUPUESTO</button>
        </div>
          <div class="col-xs-6">
          <button type="submit" name="guardar1" class="btn btn-primary btn-block btn-flat">INICIO CONTROL</button>
        </div>
        <!-- /.col -->
      </div>
      <div class="row" style="display: none">
        <div class="col-xs-12">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Recordarme
            </label>
          </div>
       </div>
      </div>
    </form>

    <!-- /.social-auth-links -->

    <a data-reveal-id="recuperar" data-animation="fade" style="cursor:pointer">¿Olvidaste tu contraseña?</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<div id="recuperar" class="reveal-modal" style="left: 57%; top: 200px; height: 160px; width: 350px;">
	<table style="width: 100%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:center" colspan="2"><p>Restablecer Contraseña</p></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:center" colspan="2"><span lang="es-co"><strong>Correo electrónico o nombre 
			de usuario:</strong></span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:center" colspan="2">
			<input name="recuperarcontrasena" id="recuperarcontrasena" class="resplandor" type="text" style="width: 280px; height: 20px;" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align:center" colspan="2">
			<input style="font-weight:bold" name="recuperar" onclick="RECUPERAR()" type="button" value="Enviar" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2"><div id="recu" style="text-align:center"></div></td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<a class="close-reveal-modal">&#215;</a>
</div>

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="dist/vegas/vegas.js?<?php echo time();?>"></script>
<script>
  $(function () 
  {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });


    $('body').vegas({
    	verlay: !0,
        delay: 1e4,

        slidesToKeep: 1,
        transition: "fade2",
        transitionDuration: 8e3,
        animation: "random",
        animationDuration: 1e4,
        slides: [
        	{ src: 'dist/vegas/img/img1.jpg' },
        	{ src: 'dist/vegas/img/img2.jpeg' },
            { src: 'dist/vegas/img/rotabanner-proyectos-comerciales.png' },
            { src: 'dist/vegas/img/rotabanner-proyectos-industriales.png' },
            { src: 'dist/vegas/img/rotabanner-proyectos-infraestructura.png' }
        ],
        overlay: 'dist/vegas/overlays/08.png'
    });

    $('.vegas-container').removeAttr('style');
  });
</script>
</body>
</html>
