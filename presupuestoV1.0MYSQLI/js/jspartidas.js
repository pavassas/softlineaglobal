/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {
	
	$('.currency').formatCurrency({colorize: true});
	
	$("#tbPartidas tbody input").focus(function() {
  //$( this ).parent( "td" ).addClass('focus');
  		var nam = $(this).attr('class');
		var valo = $(this).attr('title');
		//console.log(valo);
		//sessionStorage.setItem('valor',valo);
		//console.log(sessionStorage.getItem('valor'));
			
		if(nam=="currency")
		{
			/*var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			res1 = res1.replace(",","").replace(",","");
			var val = $(this).val()*/
			
			   $(this).val(valo);
		
			
			if($(this).val()<0)
		    {
		      $(this).val('');
		    }
		}
});

$( "#tbPartidas tbody input" ).focusout(function() {
     var nam = $(this).attr('class');
	 
	if(nam=="currency")
	{
		 if($(this).val()<=0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
		$('.currency').formatCurrency({colorize: true});
	}
			
});

		var table2 = $('#tbPartidas').DataTable( {  
		"autoWidth": true,     
        "searching":true,
		"paging":true,
		"ordering": false,
		"info": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,15], ["Todos",15 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		fixedHeader: {
            header: true,
            footer: true
        },
		fnDrawCallback: function() {
			var k = 1;
			$("#tbPartidas tbody td").each(function () {				
				$(this).attr('tabindex', k);				
				k++;
			})
		}
		//keys:true		
		
    } );
     
	
	   var table2 = $('#tbPartidas').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

		
var currCell = $('#tbPartidas tbody td').first();
var editing = false;

// User clicks on a cell
$('#tbPartidas td').click(function() {
    currCell = $(this);
	//currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input[type=text]').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
});
// User navigates table using keyboard
$('#tbPartidas tbody').keydown(function (e) {
	//console.log("Cantidad Celdas:"+tds);
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
        e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
        e.preventDefault();
        c = currCell.prev();
    } 
	else if(!editing &&(e.which== 46))
	{
	  var ide =  currCell.parents('tr').attr('id');
	   //console.log("Eliminar:" + ide);

	}	
    
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
		//console.log("Estamos en la celda: "+c.attr('tabindex'));
	
		currCell = c;
			//currCell.focus();
			//currCell.children('input[type=text]').focus();
		
		var allInputs = currCell.children(":input");
		var allSelects = currCell.children("select");
		if(allInputs.length>0)
		{	
			currCell.children('input').focus();
			if(currCell.children('input').is(':disabled'))
			{
				currCell.focus();
			}
			else
			{
				currCell.children('input').focus();
			}
			//currCell.children(":input").val(sessionStorage.getItem('valor'))
		}
		else if(allSelects.length>0)
		{
			currCell.children('select').focus();
		}
		else
		{
			currCell.focus();
		}
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
    }
});

});


function iniciar(cu,o)
{
	
	$('#tbPartidas tbody').on('keydown',function (e) {
    var c = cu;
    if (e.which == 39) {
        // Right Arrow	
		o = 1;	
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
		o = 1;
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
		o = 1;
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
		o = 1;
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (e.which == 9 && !e.shiftKey) { 
        // Tab
		o = 1;
        e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
		o = 1;
        e.preventDefault();
        c = currCell.prev();
    }     
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
		//console.log("Estamos en la celda"+c.attr('tabindex'));
			var tab = c.attr('tabindex');
			currCell = c;			
			var allInputs = c.children("input:enabled");
			if(allInputs.length>0 && o==1)
			{				
				//currCell.children('input').focus();
				//console.log("Por aca");		
				detener(c);			   
			}
			else
			{
				currCell.focus();				
			}
    	}
	});
}
function detener(cu)
{
	cu.children('input').focus();
	$('#tbPartidas tbody').off('keydown');
	currCell = cu;
	cu.children('input').on('keydown',function(e){
	    if(e.keyCode==13)
		{
		  var nam = $(this).attr('name');
		  var del = "_";
		  var n = nam.split(del);
		  var ele = n[0];
		  var idp = n[1];
		  var ins = n[2];
		  //
		  cu = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		  if(cu.length>0)
		  {
			  currCell = cu;
			  
		  }
		 // currCell.focus();	 
		  //console.log(ins);
		  if(ele=="val")
		  {		  
		  	CRUDPARTIDAS('ACTUALIZARVALOR',idp,'','','',ins,'','');
		  }
		  else if(ele=="obs")
		  {
			CRUDPARTIDAS('ACTUALIZAROBSERVACION',idp,'','','',ins,'','');  
		  }		  
		  setTimeout(iniciar(cu,1),1000);
		}	
		else if(e.keyCode==27)
		{
		   currCell = cu;
		  // currCell.focus();
		  
		  setTimeout(iniciar(cu,2),1000);
		   
		}	
		
	});		
}