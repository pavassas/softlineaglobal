// JavaScript Document
function convertirdata2(d)
{
   var table = $('#tbactividad'+d).DataTable(
   {   
       "columnDefs": 
		 [ 
			{ "targets": [5 ], "className": "dt-center" },
			{ "targets": [6 ], "className": "dt-right" },
			{ "targets": [7 ], "className": "dt-right" }          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,-1 ], [10,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"order": [[2, 'asc']]
    } );
	
	 $('#tbactividad'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[2];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
			var table2 = "<div class='col-xs-1'></div><div id='divchil"+id+"' class='col-xs-11'><div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table2+="<thead><tr><th>Nombre Recurso</th><th>Unidad Medida</th><th>Rendimiento</th><th>Valor Unitario($)</th><th>Material($)</th><th>Equipo($)</th><th>Mano de obra($)</th></tr></thead><tbody>";
	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{
				var APU = 0;
		       
				for(var i=0; i<dato.length; i++)
				{
					table2+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
					"<td data-title='Valor Unitario' class='dt-right'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' > $"+dato[i].valor+"</div></td>"+
					"<td data-title='Material' class='dt-right'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo' class='dt-right'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra' class='dt-right'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
			   		APU+= Number(dato[i].total);
				}
				 
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
			table2+="</tbody></table></div></div>";
			row.child(table2).show();
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
        }
    });
}

$(document).ready(function(e) {
    

    var table = $('#tbcapagregados').DataTable( {       
         "columnDefs": 
		 [ 
		  { "targets": [7 ], "className": "dt-right" },
           { "targets": [8 ], "visible": false }          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Capitulos Agregados",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"order": [[2, 'asc']]
		
    } );
     $('#tbcapagregados tfoot th').each( function () {
	var title = $('#tbcapagregados tfoot th').eq( $(this).index() ).text();
	if(title==""){$(this).html('');}else{
	$(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span></div>' );}
	} );
 
    // DataTable
  /*  var table = $('#tbcapagregados').DataTable();
	 $('#tbcapagregados tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );*/
	
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	//
	
 
  // Add event listener for opening and closing details
    $('#tbcapagregados tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[8];
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
		var table1 = "<div class='col-xs-1'></div><div id='divchila"+id+"' class='col-xs-11'><div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbactividad"+id+"'>";
		table1+="<thead><tr><th><a class='btn btn-block btn-success' onclick='actualizaract("+id+")' role='button' data-toggle='tooltip' title='Guardar Cambios'><i class='fa fa-refresh'></i></a></th><th></th><th>Codigo</th><th>Actividad</th><th>Unidad</th><th>Cantidad</th><th>Valor Unitario($)</th><th>Valor Total</th></tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAACTIVIDADES",id:id},//CREAR FUNCION
			function(dato)
			{
				var del = '"DELETEASIGNAR"';
				var sumapuc = 0;//sumade apu de capitulo
				for(var i=0; i<dato.length; i++)
				{
				table1+="<tr><td class='details-control1'></td>"+
				"<td><a role='button' class='btn btn-block btn-danger btn-xs' onClick='CRUDPRESUPUESTO("+del+","+id+","+dato[i].iddetalle+")'><i class='glyphicon glyphicon-trash' data-toggle='tooltip' title='Quitar Actividad'></i></a></td>"+
				"<td data-title='Codigo'>"+dato[i].idactividad+"</td>"+
				"<td data-title='ACTIVIDAD'>"+dato[i].actividad+"</td>"+
				"<td data-title='UN'>"+dato[i].unidad+"</td>"+
				"<td data-title='Cantidad'><div  style='cursor:pointer' onDblClick='reemplazarinputc("+dato[i].iddetalle+","+dato[i].cantidad+","+id+")' id='divact"+dato[i].iddetalle+"'>"+dato[i].cantidad+"</div></td>"+
				"<td data-title='Valor Unitario($)'>$"+dato[i].apu+"</td>"+
				"<td data-title='Valor Total($)'><div id='divtotact"+dato[i].iddetalle+"'>$"+dato[i].total+"</div></td></tr>";
	
				}
				table1+="</tbody></table></div></div>";
				row.child(table1).show();
				convertirdata2(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
         }
    } );	
	// Add event listener for opening and closing details
 
});