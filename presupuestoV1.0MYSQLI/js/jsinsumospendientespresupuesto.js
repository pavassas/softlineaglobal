// JavaScript Document
$(document).ready(function(e) {
	
	var pre = $('#idedicion').val();
	var gru = $('#idgru').val();
	var cap = $('#idca').val();
    
	var val = "";
	$('.currency').formatCurrency({colorize: true});
	$( "#tbinsumospendientes input" ).focus(function() {
	$( this ).parent( "td" ).addClass('focus');
	var nam = $(this).attr('class');
	//console.log(nam);
	if(nam=="currency")
	{
		var str = $(this).val()
		var res = str.replace("$", "");
		var res1 = res.replace(",","").replace(",","");
		var val = $(this).val()
		$(this).val(res1)
		if($(this).val()<0)
		{
		  $(this).val('');
		}
	}
	else
	{
	   if($(this).val()<0)
		{
		  $(this).val('');
		}
	}

	});
	
	$( "#tbinsumospendientes input" ).focusout(function() {
	$( this ).parent( "td" ).removeClass('focus');
	var nam = $(this).attr('class'); 
		if(nam=="currency")
		{
			if($(this).val()<=0 || $(this).val()=="")
			{
			$(this).val(0.00);
			}
			$('.currency').formatCurrency({colorize: true});
		}
		else
		{
			if($(this).val()<=0 || $(this).val()=="")
			{
			$(this).val(0.00);
			}
		}	
	});

    var table = $('#tbinsumospendientes').DataTable( {       
         "columnDefs": 
		 [ 				
				{ "targets": [2], "className": "dt-left" },
				{ "targets": [3], "className": "dt-center" },
				{ "targets": [4], "className": "dt-center" },
				{ "targets": [5], "className": "dt-right" }
         ],
		 "searching": false,
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Agregados",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,		
        "ajax": { "url":"modulos/actividades/insumopendientespresupuestojson.php",
		"data":{pre:pre,gru:gru,cap:cap}
		},		
		"columns": [ 			
			{   
			    "orderable":      false,
				"data":           "Delete"				
			},						
			{ "data": "Clave" },
			{ "data": "Nombre" },
			{ "data": "Unidad" },
			{ "data": "Rendimiento"},
			{ "data": "Valor1" },
			{ "data": "Tipo" }
		],
		"order": [[2, 'asc'],[3, 'asc']],
		scrollY:        '40vh',
        scrollCollapse: true,
		fnDrawCallback: function () {
			var k = 1;
			$('#tbinsumospendientes tbody td').each(function () {
				$(this).attr('tabindex', k);
				k++;
			})
		}		
    } );
    
});