// JavaScript Document
$(document).ready(function(e) {
    	var pre = $('#idedicion').val();
		var selected = [];
   		 var table1 = $('#tbPresupuestoCompartido').DataTable( {       
		
		"dom": '<"top"i>frt<"bottom"lp><"clear">',
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[5,10], [5,10]],
		"language": {
			"lengthMenu": "Ver _MENU_ registros",
			"zeroRecords": "No se encontraron datos",
			"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
			"infoEmpty": "No se encontraron datos",
			"infoFiltered": "",
			"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,
        "ajax": {
            "url": "modulos/presupuesto/presupuestocompartidojson.php",
            "data": { pre: pre },
            "type": "POST"
		},		
		"columns": [
			{ "data": "Delete" },
			{ "data": "Nombre" },
			{ "data": "Mail" }
		]
    } );
     
	  /*$('#tbPresupuestoCompartido tfoot th').each( function () {
        var title = $('#tbcapitulo1 thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );*/
	$('#tbPresupuestoCompartido tbody').on('click', 'tr', function () {
		var id = this.id;
		var index = $.inArray(id, selected);		
		if ( index === -1 ) {
			selected.push( id );
		} else {
			selected.splice( index, 1 );
		}		
		$(this).toggleClass('selected');
	} );
    // DataTable
    var table1 = $('#tbPresupuestoCompartido').DataTable(); 
    // Apply the search
    table1.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});