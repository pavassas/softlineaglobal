// JavaScript Document
$(document).ready(function(e) {

    $(document).keyup(function(event) {
        if (event.which == 27) {

            $(".tooltipster-base").hide();
            // $("input").blur(); 
        }
    });
    $('.currency').formatCurrency({
        colorize: true
    });

    $("input").focus(function() {
        $(this).parent("td").addClass('focus');

        var nam = $(this).attr('class');

        if (nam == "currency") {
            var str = $(this).val()
            var res = str.replace("$", "");
            var res1 = res.replace(",", "").replace(",", "");
            var val = $(this).val()
            $(this).val(res1)
            if ($(this).val() < 0) {
                $(this).val('');
            }
        } else {
            //console.log('Prueba' + $(this).val())
            if ($(this).val() < 0) {
                //$(this).val('');
            }
        }
    });

    $("input").focusout(function() {
        $(this).parent("td").removeClass('focus');
        var nam = $(this).attr('class');

        if (nam == "currency") {
            if ($(this).val() < 0 || $(this).val() == "") {
                $(this).val(0.00);
            }
            $('.currency').formatCurrency({
                colorize: true
            });
        } else {
            //$(this).val('');
            if ($(this).val() < 0 || $(this).val() == "") {

            }
        }

        /*var forma = formato_numero($( this ).val(), 2,',', '.')
          $( this ).val(forma)*/

    });

    $("select").focusout(function() {
        $(this).parent("td").removeClass('focus');
    });
    var table = $('#tbgruagregados').DataTable({
        "columnDefs": [{
                "targets": [2],
                "className": "dt-right"
            },
            {
                "targets": [3],
                "className": "dt-left"
            },
            {
                "targets": [4],
                "className": "dt-center"
            },
            {
                "targets": [5],
                "className": "dt-center"
            },
            {
                "targets": [6],
                "className": "dt-right"
            },
            {
                "targets": [7],
                "className": "dt-right"
            },
            {
                "targets": [8],
                "className": "dt-center"
            },
            {
                "targets": [9],
                "className": "dt-center"
            },
            {
                "targets": [10],
                "className": "dt-center"
            },
            {
                "targets": [11],
                "className": "dt-right"
            },
            {
                "targets": [12],
                "className": "dt-right"
            },
            {
                "targets": [13],
                "className": "dt-right"
            },
            {
                "targets": [14],
                "visible": false
            }
        ],
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "pagingType": "simple_numbers",
        "lengthMenu": [
            [-1, 10, 20, 30],
            ["Todos", 10, 20, 30]
        ],
        "language": {
            "lengthMenu": "Ver _MENU_ registros",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
            "paginate": {
                "previous": "&#9668;",
                "next": "&#9658;"
            }
        },
        "paging": false,
        "searching": false,
        // fixedHeader: true

    });
    /*$('#tbgruagregados tfoot th').each( function () {
    var title = $('#tbgruagregados tfoot th').eq( $(this).index() ).text();
    if(title==""){$(this).html('');}else{
    $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" />' );}
    } );*/

    // Apply the search
    table.columns().every(function() {
        var that = this;

        $('input', this.footer()).on('keyup change', function() {
            that
                .search(this.value)
                .draw();
        });
    });

    var clonedHeaderRow;

    $("#tbgruagregados .persist-area").each(function() {
        clonedHeaderRow = $(".persist-header", this);
        clonedHeaderRow
            .before(clonedHeaderRow.clone())
            .css("width", clonedHeaderRow.width())
            .addClass("floatingHeader");

    });

    $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
    //
    // Add event listener for opening and closing details
    $('#tbgruagregados tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var dat = row.data();
        var id = dat[14];
        var pre = $('#presupuesto').val();
        //alert(pre +"-"+id)

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            var table1 = "<div id='divchilg" + id + "' style='margin-left:20px;'><table width='100%'  class='table table-bordered table-condensed table-striped compact' id='tbcapitulos" + id + "'>";
            table1 += "<thead><tr>" +
                "<th class='dt-head-center' style='width:20px'></th>" +
                "<th class='dt-head-center'>CAP</th>" +

                "<th class='dt-head-center'>DESCRIPCIÓN</th>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center info'>INICIAL</th>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center danger'>ACTUAL</th>" +
                "<th class='dt-head-center success'>CAMBIO.ALCANCE</th>" +
                "<th></th>" +
                "<th></th>" +
                "</tr></thead><tbody>";


            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: "LISTACAPITULOS",
                    pre: pre,
                    gru: id
                }, //CREAR FUNCION
                function(dato) {
                    if (dato[0].res == "no") {

                    } else {

                        for (var i = 0; i < dato.length; i++) {
                            var cap = dato[i].capitulo;
                            var cre = dato[i].cre;
                            if (cre == 1) {
                                table1 += "<tr style='background-color:rgba(215,215,215,0.5)' class='trdelete' id='cap" + dato[i].idd + "'>";
                            } else {
                                table1 += "<tr style='background-color:rgba(215,215,215,0.5)' id='cap" + dato[i].idd + "'>";
                            }
                            table1 += "<td class='details-control1c' style='width:20px'></td>" +
                                "<td data-title='CODIGO' style='width:85px'><span id='codc" + dato[i].idd + "'>" + dato[i].codc + "</span></td>" +
                                "<td data-title='NOMBRE'  style='width:280px'>" + dato[i].nombre + "</td>" +
                                "<td data-title='' style='width:60px'></td>" +
                                "<td data-title='' style='width:60px'></td>" +
                                "<td data-title='' style='width:80px'></td>" +
                                "<td data-title='Valor Total($)' style='width:80px'><div id='divtotcap" + dato[i].idd + "' class='currency' title='" + dato[i].totalt + "'>$" + dato[i].total + "</div></td>" +
                                "<td data-title='' style='width:60px'></td>" +
                                "<td data-title='' style='width:60px'></td>" +
                                "<td data-title='' style='width:60px'></td>" +
                                "<td data-title='' style='width:80px'></td>" +
                                "<td style='width:80px'><div id='divtotcapa" + dato[i].idd + "' class='currency' title= '" + dato[i].totalat + "'>$" + dato[i].totala + "</div></td>" +
                                "<td style='width:98px'><div id='divtotalcc" + dato[i].idd + "' class='currency' title= '" + dato[i].totalalct + "'>$" + dato[i].totalalc + "</div></td>" +
                                "<td>" + dato[i].idd + "</td>" +
                                "<td>" + dato[i].capitulo + "</td>" +
                                "</tr>";

                        }
                    }
                    table1 += "</tbody></table></div>";
                    table1 += "<div class='col-md-12'><input type='text' id='select" + pre + "g" + id + "' placeholder='CAPITULOS A AGREGAR'   onkeyup=filterGlobal('tbcapitulosagregadas" + id + "','select" + pre + "g" + id + "') name='pre_" + pre + "_gru_" + id + "' data-pre='" + pre + "' data-gru = '" + id + "' onfocus=cargarcapitulosagregar('" + pre + "','" + id + "')  class='form-control input-sm  capagregar'  style='width:100%'/>\
                <div  id='divselect" + pre + "g" + id + "' class='modal-text'></div>";
                    row.child(table1).show();
                    convertircap(id);
                }, "json");
            //row.child(format(row.data()) ).show();
            tr.addClass('shown');
        }
    });


    /*$('#selectcapitulo').tooltipster({
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        offsetX: 0,
        offsetY: 0,
        position: 'top',
        positionTracker: false,
        updateAnimation: 'fade',
        multiple: true,
        maxWidth: $(this).width(),
        trigger: 'click',
        distance: 1,
        offsetY: -12,
        arrow: false,
        functionBefore: function(origin, continueTooltip) {
            continueTooltip();
            var pre = $('#presupuesto').val();


            if (origin.data('ajax') !== 'cached') {
                $.ajax({
                    type: 'POST',
                    url: 'funciones/fnPresupuesto.php',
                    data: {
                        opcion: 'AGREGARCAPEVENTO',
                        pre: pre
                    },
                    success: function(data) {
                        // update our tooltip content with our returned data and cache it
                        origin.tooltipster('update', data).data('ajax', 'cached');
                    }
                });
            }
           
        },
        functionAfter: function(origin) {
            //content:""
            //alert('The tooltip has closed!');
        }
    });*/


});


function cargarcapitulosagregar(pre, gru) {
    $('.modal-overlay').show();
    $("#select" + pre + "g" + gru).addClass('modal-input');
    $("#divselect" + pre + "g" + gru).html('').show();

    $.ajax({
        type: 'POST',
        url: 'funciones/fnPresupuesto.php',
        data: {
            opcion: 'AGREGARCAPACTUAL',
            pre: pre,
            gru: gru
        },
        success: function(data) {
            // update our tooltip content with our returned data and cache it
            $("#divselect" + pre + "g" + gru).html(data)
        }
    });
}

function cargarcapituloeventoagregar() {
    var pre = $('#presupuesto').val();
    $('.modal-overlay').show();
    $("#selectcapitulo").addClass('modal-input');
    $("#divselectcapitulo").html('').show();
    $.ajax({
        type: 'POST',
        url: 'funciones/fnPresupuesto.php',
        data: {
            opcion: 'AGREGARCAPEVENTO',
            pre: pre
        },
        success: function(data) {
            // update our tooltip content with our returned data and cache it
            $("#divselectcapitulo").html(data)
        }
    });
}

function convertircap(d) {
    var pre = $('#presupuesto').val();
    $("#select" + pre + "g" + d + " input").focus(function() {
        $(this).parent("td").addClass('focus');
        var nam = $(this).attr('class');

        if (nam == "currency") {
            var str = $(this).val()
            var res = str.replace("$", "");
            var res1 = res.replace(",", "").replace(",", "");
            var val = $(this).val()
            $(this).val(res1)
            if ($(this).val() < 0) {
                $(this).val('');
            }
        } else {
            if ($(this).val() < 0) {
                $(this).val('');
            }
        }
    });

    $("#select" + pre + "g" + d + " input").focusout(function() {
        $(this).parent("td").removeClass('focus');
        var nam = $(this).attr('class');

        if (nam == "currency") {
            if ($(this).val() < 0 || $(this).val() == "") {
                $(this).val(0.00);
            }
        } else {
            //console.log("Busqueda capitulo");
            $(this).val('');
        }
    });

    $('.currency').formatCurrency({
        colorize: true
    });
    var table = $('#tbcapitulos' + d).DataTable({
        "columnDefs": [{
                "targets": [1],
                "className": "dt-left"
            },
            {
                "targets": [2],
                "className": "dt-left"
            },
            {
                "targets": [3],
                "className": "dt-right"
            },
            {
                "targets": [4],
                "className": "dt-right"
            },
            {
                "targets": [5],
                "className": "dt-right"
            },
            {
                "targets": [6],
                "className": "dt-right"
            },
            {
                "targets": [11],
                "className": "dt-right"
            },
            {
                "targets": [12],
                "className": "dt-right"
            },
            {
                "targets": [13, 14],
                "visible": false
            },
            {
                "targets": [1],
                "className": "dt-center"
            }
        ],
        "ordering": true,
        "info": false,
        "searching": false,
        "autoWidth": true,
        "pagingType": "simple_numbers",
        "lengthMenu": [
            [10, 20, 30, -1],
            [10, 20, 30, "Todos"]
        ],
        "language": {
            "lengthMenu": "Ver _MENU_ registros",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
            "paginate": {
                "previous": "&#9668;",
                "next": "&#9658;"
            }
        },
        "paging": false,
        "order": [
            [1, 'asc']
        ],
        fnDrawCallback: function() {
            $('#tbcapitulos' + d + ' thead').remove();
        }

    });
    $('#tbcapitulos' + d + ' tfoot th').each(function() {
        var title = $('#tbcapitulos' + d + ' tfoot th').eq($(this).index()).text();
        if (title == "") {
            $(this).html('');
        } else {
            $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
        }
    });

    // Apply the search
    table.columns().every(function() {
        var that = this;

        $('input', this.footer()).on('keyup change', function() {
            that
                .search(this.value)
                .draw();
        });
    });

    // Add event listener for opening and closing details
    $('#tbcapitulos' + d + ' tbody').on('click', 'td.details-control1c', function() {
        var tr = $(this).closest('tr');

        var row = table.row(tr);
        var dat = row.data();
        var data = table.row($(this).parents('tr')).data();
        var idc = data[14];
        var pre = $('#presupuesto').val();
        var fontsize = localStorage.getItem('fontSize');
        //console.log("Cap" + idc);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        } else {

            var table1 = "<div id='divchila" + pre + "g" + d + "c" + idc + "' style='padding-left:35px'>" +
                "<table width='100%'  class='table table-bordered table-condensed compact' style='font-size:" + fontsize + "px' id='tbactividadp" + pre + "g" + d + "c" + idc + "'>" +
                "<thead><tr>" +
                "<th style='width:20px'></th>" +
                "<th style='width:50px'></th>" +
                // "<th style='width:20px'></th>" +
                // "<th style='width:20px'></th>" +
                "<th class='dt-head-center' >ITEM</th>" +
                "<th class='dt-head-center' style='width:200px'>DESCRIPCION</th>" +
                "<th class='dt-head-center'>UN</th>" +
                "<th class='info dt-head-center'>CANT</th>" +
                "<th class='info dt-head-center'>VR.UNIT</th>" +
                "<th class='info dt-head-center'>PPTO<br>INICIAL</th>" +
                "<th class='danger dt-head-center'>CANT<br>PROYECTADA</th>" +
                "<th class='danger dt-head-center'>CANT<br>EJECUTADA</th>" +
                "<th class='danger dt-head-center'>CANT<br>FALTANTE</th>" +
                "<th class='danger dt-head-center'>VLR.UNIT<br>ACTUAL</th>" +
                "<th class='danger dt-head-center'>PPTO<br>ACTUAL</th>" +
                "<th class='success dt-head-center'>CAMBIO<br>ALCANCE</th>" +
                "<th>SUBA</th>" +
                "</tr></thead><tbody>";


            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: "LISTAACTIVIDADES",
                    pre: pre,
                    gru: d,
                    cap: idc
                }, //CREAR FUNCION
                function(dato) {
                    var res = dato[0].res;
                    if (res == "no") {

                    } else {
                        for (var i = 0; i < dato.length; i++) {
                            var numapu = dato[i].numapu;
                            var cre = dato[i].cre;
                            var items = dato[i].items;
                            var itemp = dato[i].itemp;
                            if (numapu > 0) {
                                if (items == "EXT") {
                                    var col = "style='color: rgba(255, 0, 0, 0.498039);font-weight:bold'";
                                } else {
                                    var col = "";
                                }
                            } else if (numapu <= 0 && itemp != "PRE") {
                                var col = "style='color: rgba(255, 0, 0, 0.498039)'";
                            } else if (itemp == "PRE") {
                                var col = "style='background-color: rgba(16,146,55, 0.498039)'";
                            }
                            if (cre == 1 && itemp != "PRE") {
                                table1 += "<tr id='" + dato[i].iddetalle + "' " + col + " class='trdeletea'>";
                            } else
                            if (cre == 1 && itemp == "PRE") {
                                table1 += "<tr id='" + dato[i].iddetalle + "' " + col + " class='trdeletee'>";
                            } else {
                                table1 += "<tr id='" + dato[i].iddetalle + "' " + col + ">";
                            }
                            table1 += "<td>" + dato[i].iddetalle + "</td>";
                            if (itemp != "PRE") {
                                table1 += "<td class='details-control1' style='width:30px'></td>";
                            } else {
                                table1 += "<td style='width:50px'></td>";
                            }
                            table1 +=
                                // "<td style='vertical-align:middle'><a class='btn btn-block btn-success btn-xs' onClick=CRUDPRESUPUESTOINICIAL('USARACTIVIDAD','" + dato[i].idactividad + "','" + id + "','" + d + "','" + id + "') role='button'  data-toggle='modal' data-target='#myModal' title='Usar actividad' style='width:20px; height:20px'><i class='glyphicon glyphicon-share'><i/></a></td>" +
                                // "<td style='vertical-align:middle'><a class='btn btn-block btn-default btn-xs' onClick=CRUDPRESUPUESTOINICIAL('ASOCIARAPU','" + dato[i].idactividad + "','" + dato[i].iddetalle + "','" + d + "','" + id + "') role='button'  data-toggle='modal' data-target='#myModal' title='Asociar Apu' style='width:20px; height:20px'><i class='glyphicon glyphicon-plus'><i/></a></td>" +
                                "<td data-title='ITEM' style='width:45px'>" + dato[i].items + "</td>" +
                                "<td data-title='DESCRIPCION'  style='width:280px'>" + dato[i].actividad + "</td>" +
                                "<td data-title='UN' style='width:62px'>" + dato[i].unidad + "</td>" +
                                "<td data-title='CANT.INICIAL' style='width:60px' title='" + dato[i].cantidadt + "'>" + dato[i].cantidad + "</td>" +
                                "<td data-title='VLR.UNIT.INICIAL' style='width:80px'><div id='divtotapu" + dato[i].iddetalle + "' class='currency' title='" + dato[i].aput + "'>" + dato[i].apu + "</div></td>" +
                                "<td data-title='PTO.INICIAL' style='width:80px' ><div id='divtoti" + dato[i].iddetalle + "' class='currency' title = '" + dato[i].totalt + "'>$" + dato[i].total + "</div></td>";
                            if (itemp == "PRE") {
                                table1 +=
                                    "<td data-title='CANT.PROYECTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempcp" + dato[i].iddetalle + "'  name=''  type='text' value='" + dato[i].cantidadp + "'  min='0'  onKeyPress='return NumCheck(event, this)'  onchange=guardarcantidadpen('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + d + "','" + idc + "') disabled><br><span id='spcp" + dato[i].iddetalle + "'></span></td>" +

                                    "<td data-title='CANT.EJECUTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempce" + dato[i].iddetalle + "' name='' type='text' value='" + dato[i].cantidade + "'  min='0'  onKeyPress='return NumCheck(event, this)'  onchange=guardarcantidadeen('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + d + "','" + idc + "') disabled><br><span id='spce" + dato[i].iddetalle + "'></span></td>";

                            } else {
                                table1 +=
                                    "<td data-title='CANT.PROYECTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempcp" + dato[i].iddetalle + "'  name=''  type='text' value='" + dato[i].cantidadp + "'  min='0'  onKeyPress='return NumCheck(event, this)'  onchange=guardarcantidadp('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + d + "','" + idc + "') ><br><span id='spcp" + dato[i].iddetalle + "'></span></td>" +

                                    "<td data-title='CANT.EJECUTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempce" + dato[i].iddetalle + "' name='' type='text' value='" + dato[i].cantidade + "'  min='0'  onKeyPress='return NumCheck(event, this)'  onchange=guardarcantidade('" + dato[i].iddetalle + "',this.value,'" + pre + "','" + d + "','" + idc + "')><br><span id='spce" + dato[i].iddetalle + "'></span></td>";
                            }

                            table1 +=
                                "<td data-title='CANT.FALTANTE' style='width:60px'><div  style='cursor:pointer' id='divactf" + dato[i].iddetalle + "'>" + dato[i].cantidadf + "</div></td>" +
                                "<td data-title='VLR.UNIT.ACTUAL' style='width:80px'><div id='rempva" + dato[i].iddetalle + "' class='currency' title = '" + dato[i].valoractt + "'>$" + dato[i].valoract + "</div></td>" +
                                "<td data-title='PPTO.ACTUAL' style='width:80px'><div id='divtotacta" + dato[i].iddetalle + "' class='currency' title = '" + dato[i].totalat + "'>$" + dato[i].totala + "</div></td>" +
                                "<td data-title='CAMBIOS EN EL ALCANCE' style='width:92px'><div id='divtotactc" + dato[i].iddetalle + "' class='currency' title = '" + dato[i].totalalt + "'>" + dato[i].totalal + "</div></td>" +
                                "<td data-title='SUBANALISIS' style='width:92px'>" + dato[i].suba + "</td>" +
                                "</tr>";

                        }
                    }
                    table1 += "</tbody></table></div>" +
                        "<div class='col-md-12'><input type='text' id='select" + pre + "g" + d + "c" + idc + "' placeholder='ACTIVIDADES A AGREGAR' name='pre_" + pre + "_gru_" + d + "_cap_" + idc + "'  class='form-control input-sm actagregar' data-pre='" + pre + "' data-gru='" + d + "' data-cap='" + idc + "'  style='width:100%' onfocus=cargaractividadesagregar('" + pre + "','" + d + "','" + idc + "') onkeyup=filterGlobal('tbactividadesagregadasp" + pre + "g" + d + "c" + idc + "','select" + pre + "g" + d + "c" + idc + "')  />\
                        <div id='divselect" + pre + "g" + d + "c" + idc + "' class='modal-text'></div>\
                        </div>" +
                        "<div class='col-md-12'><input type='text' id='enlazar" + pre + "g" + d + "c" + idc + "' onfocus=cargarenlazaragregar('" + pre + "','" + d + "','" + idc + "') placeholder='ENLAZAR PRESUPUESTO'  name='epre_" + pre + "_gru_" + d + "_cap_" + idc + "'  class='form-control input-sm preagregar' style='width:100%' onkeyup=filterGlobal('tbpresupuestoenlazarp" + pre + "g" + d + "c" + idc + "','enlazar" + pre + "g" + d + "c" + idc + "')   />\
                        <div id='divenlazar" + pre + "g" + d + "c" + idc + "' class='modal-text'></div>\
                        </div>";


                    row.child(table1).show();
                    convertirdata2(pre, d, idc);
                    //setTimeout(autoTabIndex("#tbactividadp"+pre+"g"+d+"c"+id,14),1000);
                }, "json");
            tr.addClass('shown1');
        }
    });

    /* $('.capagregar').tooltipster({
         content: 'Cargando...',
         contentAsHTML: true,
         theme: 'tooltipster-light',
         contentCloning: false,
         interactive: true,//interactuar dentro del tooltip
         trigger : 'click',
         maxWidth: $(this).width(),
         offsetX: 0,
         offsetY: 0,
         side: ['bottom', 'top'],
         positionTracker: false,
         updateAnimation: 'fade',
         multiple: true,
         distance: 1,
         offsetY: -12,
         arrow:false,
         reposition:true,

         // 'instance' is basically the tooltip. More details in the "Object-oriented Tooltipster" section.
         functionBefore: function(instance, helper) {
             var del = "_";
             //var n = $(this).attr('name').split(del);


             var $origin = $(helper.origin);
             var pre = $origin.attr('data-pre');//n[1];
             //console.log(pre);
             var gru = $origin.attr('data-gru');//n[3];

             // we set a variable so the data is only loaded once via Ajax, not every time the tooltip opens
             if ($origin.data('loaded') !== true) {
                 $.ajax({
                     type: 'POST',
                     url: 'funciones/fnPresupuesto.php',
                     data: {
                         opcion: 'AGREGARCAPACTUAL',
                         pre: pre,
                         gru: gru
                     },
                     success: function(data) {
                         // update our tooltip content with our returned data and cache it
                         instance.content(data);

                         // to remember that the data has been loaded
                         $origin.data('loaded', true);
                     }
                 });
             }
         }
     });*/

    /*
    $('.capagregar').tooltipster({
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        offsetX: 0,
        offsetY: 0,
        position: 'bottom',
        positionTracker: false,
        updateAnimation: 'fade',
        multiple: true,
        maxWidth: $(this).width(),
        trigger: 'click',
        distance: 1,
        offsetY: -12,
        arrow: false,
        functionBefore: function(origin, continueTooltip) {
            continueTooltip();

            var del = "_";
            var n = $(this).attr('name').split(del);
            var pre = n[1];
            var gru = n[3];

            if (origin.data('ajax') !== 'cached') {
                $.ajax({
                    type: 'POST',
                    url: 'funciones/fnPresupuesto.php',
                    data: {
                        opcion: 'AGREGARCAPACTUAL',
                        pre: pre,
                        gru: gru
                    },
                    success: function(data) {
                        // update our tooltip content with our returned data and cache it
                        origin.tooltipster('update', data).data('ajax', 'cached');
                    }
                });
            }
            // when the request has finished loading, we will change the tooltip's content


        },
        functionAfter: function(origin) {
            //content:""
            //alert('The tooltip has closed!');
        }
    });
    */

    $("#tbcapitulos" + d + " tbody").contextMenu({
        selector: 'tr.trdelete',
        callback: function(key, options) {

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dat = row.data();
            var idd = dat[13];
            var idc = dat[14];
            var pre = $('#presupuesto').val();
            if (key == "delete") {
                setTimeout(CRUDPRESUPUESTO('DELETEASIGNARCAPITULOACTUAL', pre, d, idc, '', idd), 1000);
            }
            /*else if(key=="add")
            {
                setTimeout(CRUDPRESUPUESTO('AGREGARACTIVIDAD',pre,d,idc,'',''),1000);
            }*/
            // window.console && console.log(m) || alert(m); 
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            //"copy": {name: "Duplicar", icon: "copy"},
            //"paste": {name: "Paste", icon: "paste"},
            //"add": {name: "Agregar Actividad", icon: "add"},
            "delete": {
                name: "Eliminar Capitulo",
                icon: "delete"
            }
        }
    });
    /*});*/


}

function cargaractividadesagregar(pre, gru, cap) {
    $('.modal-overlay').show();
    $("#select" + pre + "g" + gru + "c" + cap).addClass('modal-input');
    $("#divselect" + pre + "g" + gru + "c" + cap).html('').show();
    $.ajax({
        type: 'POST',
        url: 'funciones/fnPresupuesto.php',
        data: { opcion: 'AGREGARACTIVIDADACTUAL', pre: pre, gru: gru, cap: cap },

        success: function(data) {
            $("#divselect" + pre + "g" + gru + "c" + cap).html(data);
        }
    });
}

function cargarenlazaragregar(pre, gru, cap) {
    $('.modal-overlay').show();
    $("#enlazar" + pre + "g" + gru + "c" + cap).addClass('modal-input');
    $("#divenlazar" + pre + "g" + gru + "c" + cap).html('').show();
    $.ajax({
        type: 'POST',
        url: 'funciones/fnPresupuesto.php',
        data: { opcion: 'ENLAZARPRESUPUESTO', pre: pre, gru: gru, cap: cap },

        success: function(data) {
            $("#divenlazar" + pre + "g" + gru + "c" + cap).html(data);
        }
    });
}



function convertirdata2(pre, gru, cap) {

    $('.currency').formatCurrency({
        colorize: true
    });
    $("input").focus(function() {
        $(this).parent("td").addClass('focus');
        var nam = $(this).attr('class');
        var tit = $(this).attr('name');
        if (nam == "currency") {
            var str = $(this).val()
            var res = str.replace("$", "");
            var res1 = res.replace(",", "").replace(",", "");
            var val = $(this).val()
            $(this).val(res1)
            if ($(this).val() < 0 && tit != "busqueda") {
                $(this).val('');
            }
        } else {
            if ($(this).val() < 0) {
                $(this).val('');
            }
        }
    });

    $("input").focusout(function() {
        $(this).parent("td").removeClass('focus');
        var nam = $(this).attr('class');
        var tit = $(this).attr('name');
        if (nam == "currency") {
            if (($(this).val() < 0 || $(this).val() == "") && tit != "busqueda") {
                $(this).val(0.00);
            }
            $('.currency').formatCurrency({
                colorize: true
            });
        } else {
            if ($(this).val() < 0 || $(this).val() == "") {
                $(this).val('');
            }
        }
    });

    var table = $('#tbactividadp' + pre + 'g' + gru + 'c' + cap).DataTable({
        "columnDefs": [{
                "targets": [0],
                "visible": false
            },
            //{  "targets": [2], "visible": false },
            //{  "targets": [3], "visible": false },
            {
                "targets": [4],
                "className": "dt-center"
            },
            {
                "targets": [5],
                "className": "dt-center"
            },
            {
                "targets": [6],
                "className": "dt-right"
            },
            {
                "targets": [7],
                "className": "dt-right"
            },
            {
                "targets": [8],
                "className": "dt-center"
            },
            {
                "targets": [9],
                "className": "dt-center"
            },
            {
                "targets": [10],
                "className": "dt-center"
            },
            {
                "targets": [11],
                "className": "dt-right"
            },
            {
                "targets": [12],
                "className": "dt-right"
            },
            {
                "targets": [13],
                "className": "dt-right"
            },
            {
                "targets": [14],
                "visible": false
            }
        ],
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "language": {
            "lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
        },
        "paging": false,

        fnDrawCallback: function() {
            $("#tbactividadp" + pre + "g" + gru + "c" + cap + " thead").remove();
            var k = 1;
            $("#tbactividadp" + pre + "g" + gru + "c" + cap + " tbody td").each(function() {
                $(this).attr('tabindex', k);
                k++;
            })

        }
    });

    $('#tbactividadp' + pre + 'g' + gru + 'c' + cap + ' tbody').on('click', 'td.details-control1', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var dat = row.data();
        var suba = dat[14];
        //   console.log(row.Cells[14]);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        } else {
            var id = tr.attr('id');
            var pre = $('#presupuesto').val();
            // Open this row
            var table2 = "<div class='nav-tabs-custom'>" +
                "<ul class='nav nav-tabs pull-left' id='nav" + id + "' title'ASIGNACION DE APU Y SUBANALISIS'>" +
                "<li class='active'>" +
                "<a onclick=CRUDPRESUPUESTOINICIAL('VERAPU','" + id + "','','" + gru + "','" + cap + "') title='" + suba + "' data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>" +
                "</li>";

            if (suba == 1) {
                table2 += "<li>" +
                    "<a onclick=CRUDPRESUPUESTOINICIAL('VERSUBANALISIS','" + id + "','','" + gru + "','" + cap + "') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>" +
                    "</li>";
            }
            table2 += "</ul>" +
                "</div><div class='panel panel-default'><div class='panel-body'  ><div id='divchil" + id + "'><table  class='table table-bordered table-condensed compact table-hover' id='tbinsumos" + id + "p" + pre + "g" + gru + "c" + cap + "'>";
            table2 += "<thead><tr>" +
                "<th class='dt-head-center'>CODIGO</th>" +
                "<th class='dt-head-center' width='20%'>RECURSO</th>" +
                "<th class='dt-head-center' width='7%'>UND</th>" +
                "<th class='dt-head-center info 'width='6%'>REND</th>" +
                "<th class='info dt-head-center' width='6%'>V/UNITARIO</th>" +
                "<th class='info dt-head-center' width='6%'>MATERIAL</th>" +
                "<th class='info dt-head-center' width='6%'>EQUIPO</th>" +
                "<th class='info dt-head-center' width='6%'>M.DE.O</th>" +
                "<th class='danger dt-head-center' width='6%'>REND ACTUAL.</th>" +
                "<th class='danger dt-head-center' width='12%'>VR.UNIT ACTUAL</th>" +
                "<th class='danger dt-head-center' width='6%'>AIU</th>" +
                "<th class='danger dt-head-center' width='6%'>MATERIAL</th>" +
                "<th class='danger dt-head-center' width='6%'>EQUIPO</th>" +
                "<th class='danger dt-head-center' width='6%'>M.DE.O</th>" +
                "<th class='success dt-head-center' width='6%'>C.ALCANCE</th>" +
                "</tr></thead><tbody>";

            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: "LISTAINSUMOS",
                    id: id,
                    pre: pre,
                    gru: gru,
                    cap: cap
                },
                function(dato) {
                    var res = dato[0].res;
                    if (res == "no") {} else {
                        for (var i = 0; i < dato.length; i++) {
                            var cre = dato[i].cre;
                            if (cre == 1) {
                                table2 += "<tr class='delete1' id='" + dato[i].iddetalle + "'>";
                            } else {
                                table2 += "<tr>";
                            }
                            table2 +=
                                "<td data-title='Codigo'>" + dato[i].insu + "</td>" +
                                "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                                "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                                "<td data-title='Rendimiento' class='dt-center'>" + dato[i].rendi + "</td>" +
                                "<td data-title='Valor Unitario' class='dt-right currency' title='" + dato[i].valor + "'>$" + dato[i].valor1 + "</td>" +
                                "<td data-title='Material' class='dt-right currency'>$" + dato[i].material + "</td>" +
                                "<td data-title='Equipo' class='dt-right currency'>$" + dato[i].equipo + "</td>" +
                                "<td data-title='Mano de obra' class='dt-right'>$" + dato[i].mano + "</td>" +
                                "<td data-title='Rendimiento Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].REA + "</td>" +

                                "<td data-title='Valor Unitario Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].VAA + "<span id=''>" + "" + "</span>" + "</td>" +
                                "<td><a class='ivasactual tooltipstered' style='cursor:pointer' onmouseover='INICIALIZARTOOLTIP()' id='" + dato[i].iddetalle + "_" + pre + "_" + gru + "_" + cap + "_" + id + "_" + dato[i].insu + "'>AIU<span id='incp" + dato[i].iddetalle + "'>" + dato[i].incp + "</span></a></td>" +
                                "<td data-title='Material' class='dt-right'>" + dato[i].materiala + "</td>" +
                                "<td data-title='eqipo' class='dt-right'>" + dato[i].equipoa + "</td>" +
                                "<td data-title='mano' class='dt-right'>" + dato[i].manoa + "</td>" +
                                "<td  data-title='C.Alcance' class='dt-right'>" + dato[i].totalact + "</td></tr>";
                        }
                    }
                    table2 += "</tbody></table></div>" +
                        "<div class='col-md-12'><input type='text' id='selectp" + pre + "g" + gru + "c" + cap + "a" + id + "' name='pre_" + pre + "_gru_" + gru + "_cap_" + cap + "_act_" + id + "' class='form-control input-sm insrecurso' style='width:100%' placeholder='AGREGAR RECURSOS' onkeyup=filterGlobal('tbrecursosagregadosp" + pre + "g" + gru + "c" + cap + "a" + id + "','selectp" + pre + "g" + gru + "c" + cap + "a" + id + "') onfocus=cargarrecursosagregar('" + pre + "','" + gru + "','" + cap + "','" + id + "') />\
                        <div id='divselectp" + pre + "g" + gru + "c" + cap + "a" + id + "' class='modal-text'></div>\
                        </div>" +
                        "</div></div>";
                    row.child(table2).show();
                    convertirdata4(id, pre, gru, cap);
                    //setTimeout(autoTabIndex2("#tbinsumos"+id,14),1000);
                    setTimeout(INICIALIZARTOOLTIP(), 1000)
                }, "json");
            tr.addClass('shown1');
        }
    });

    /*$("#select" + pre + "g" + gru + "c" + cap).keyup(function(e) {
        //console.log("Por aca");
        if (e.keyCode == 13) {
            $("#tbactividadesagregadasp" + pre + "g" + gru + "c" + cap).DataTable().search($(this).val(), '', '').draw();
        }
    }).change(function() {
        $("#tbactividadesagregadasp" + pre + "g" + gru + "c" + cap).DataTable().search($(this).val(), '', '').draw();
    });*/

    var currCell = $('#tbactividadp' + pre + 'g' + gru + 'c' + cap + ' tbody td').first();
    var editing = false;
    var val = 0;
    // User clicks on a cell
    $('#tbactividadp' + pre + 'g' + gru + 'c' + cap + ' td').click(function() {
        currCell = $(this);
        currCell.toggleClass("editing");
        //currCell.focus();
        currCell.children('input').focus();
        val = currCell.children('input').val();
        //currCell.children('select').focus();
        //currCell.children('.select2').focus();
    });
    // User navigates table using keyboard
    $('#tbactividadp' + pre + 'g' + gru + 'c' + cap + ' tbody').keydown(function(e) {
        var c = "";
        if (e.which == 39) {
            // Right Arrow
            c = currCell.next();
        } else if (e.which == 37) {
            // Left Arrow
            c = currCell.prev();
        } else if (e.which == 38) {
            // Up Arrow
            c = currCell.closest('tr').prev().find('td:eq(' +
                currCell.index() + ')');
        } else if (e.which == 40) {
            // Down Arrow
            c = currCell.closest('tr').next().find('td:eq(' +
                currCell.index() + ')');
        } else if (!editing && (e.which == 9 && !e.shiftKey)) {
            // Tab
            e.preventDefault();
            c = currCell.next();
        } else if (!editing && (e.which == 9 && e.shiftKey)) {
            // Shift + Tab
            e.preventDefault();
            c = currCell.prev();
        }

        // If we didn't hit a boundary, update the current cell
        if (c.length > 0) {
            currCell = c;
            currCell.focus();
            val = currCell.children('input').val();
            //currCell.children('input').focus();
            //currCell.children('select').focus();
            //INICIALIZARLISTAS('PRESUPUESTO');
        }
        if (e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40 && !e.shiftKey && e.which != 9) {

            if (e.which == 27) {
                currCell.children('input').val(val);
                //currCell.focus();
            } else {

                currCell.children('input').focus();
            }
        }

    });

    $('#tbactividadp' + pre + 'g' + gru + 'c' + cap + ' tbody tr')
        .on('mouseover', function() {
            $(this).addClass('highlight');
        })
        .on('mouseleave', function() {
            $(this).removeClass('highlight');
        });
    $("#tbactividadp" + pre + "g" + gru + "c" + cap + " tbody").contextMenu({
        selector: 'tr.trdeletea',
        callback: function(key, options) {

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dat = row.data();
            var idd = dat[0];
            if (key == "delete") {
                setTimeout(CRUDPRESUPUESTO('DELETEASIGNARACTUAL', pre, gru, cap, '', idd), 1000);
            } else
            if (key == "restore") {
                setTimeout(CRUDPRESUPUESTO('ENVIARAMAESTRA', pre, gru, cap, '', idd), 1000);
            }
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            //"copy": {name: "Duplicar", icon: "copy"},
            "restore": {
                name: "Enviar a maestra",
                icon: "restore"
            },
            "delete": {
                name: "Eliminar Actividad",
                icon: "delete"
            }
        }
    });
    $("#tbactividadp" + pre + "g" + gru + "c" + cap + " tbody").contextMenu({
        selector: 'tr.trdeletee',
        callback: function(key, options) {

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dat = row.data();
            var idd = dat[0];
            if (key == "delete") {
                setTimeout(CRUDPRESUPUESTO('DELETEENLAZAR', pre, gru, cap, '', idd), 1000);
            }
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            //"copy": {name: "Duplicar", icon: "copy"},

            "delete": {
                name: "Eliminar Presupuesto Enlazado",
                icon: "delete"
            }
        }
    });

    /*
    $("#select" + pre + "g" + gru + "c" + cap).tooltipster({
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        animation: 'slide',
        updateAnimation: 'scale',
        position: 'bottom',
        positionTracker: false,
        multiple: true,
        maxWidth: 1100,
        minWidth: 1100,
        distance: 1,
        offsetY: -12,
        arrow: false,
        trigger: 'click',
        functionBefore: function(origin, continueTooltip) {
            continueTooltip();

            var del = "_";
            var n = $(this).attr('name').split(del);
            var pre = n[1];
            var gru = n[3];
            var cap = n[5];

            if (origin.data('ajax') !== 'cached') {
                $.ajax({
                    type: 'POST',
                    url: 'funciones/fnPresupuesto.php',
                    data: {
                        opcion: 'AGREGARACTIVIDADACTUAL',
                        pre: pre,
                        gru: gru,
                        cap: cap
                    },
                    success: function(data) {
                        // update our tooltip content with our returned data and cache it
                        origin.tooltipster('update', '<div id="divnueva">' + data + '</div>').data('ajax', 'cached');
                    }
                });
            }


        },
        functionAfter: function(origin) {
            //alert('The tooltip has closed!');
            //content:""
            //origin.tooltipster('update','');
        }
    });
    */
    /*  $("#select" + pre + "g" + gru + "c" + cap).tooltipster({
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        animation: 'slide',
        updateAnimation: 'scale',
        position: 'bottom',
        positionTracker: false,
        multiple: true,
        maxWidth: 1100,
        minWidth: 1100,
        distance: 1,
        offsetY: -12,
        arrow: false,
        trigger: 'click',
        functionBefore: function(instance, helper) {
            var $origin = $(helper.origin);

            //var del = "_";
            //var n = $(this).attr('name').split(del);
            var pre = $origin.attr('data-pre');// n[1];
            var gru = $origin.attr('data-gru');//n[3];
            var cap = $origin.attr('data-cap');//n[5];

            if ($origin.data('loaded') !== true) {
                $.ajax({
                    type: 'POST',
                    url: 'funciones/fnPresupuesto.php',
                    data: {
                        opcion: 'AGREGARACTIVIDADACTUAL',
                        pre: pre,
                        gru: gru,
                        cap: cap
                    },
                    success: function(data) {
                        // update our tooltip content with our returned data and cache it
                        //origin.tooltipster('update', '<div id="divnueva">' + data + '</div>').data('ajax', 'cached');
                        instance.content('<div id="divnueva">' + data + '</div>');

                        // to remember that the data has been loaded
                        $origin.data('loaded', true);
                    }
                });
            }
        },
        functionAfter: function(instance, helper) {
            //alert('The tooltip has closed!');
            //content:""
            //origin.tooltipster('update','');
        }
    });
*/
    /*$("#enlazar" + pre + "g" + gru + "c" + cap).tooltipster({
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        animation: 'slide',
        updateAnimation: 'scale',
        position: 'bottom',
        positionTracker: false,
        multiple: true,
        maxWidth: 1100,
        minWidth: 1100,
        distance: 1,
        offsetY: -12,
        arrow: false,
        trigger: 'click',
        functionBefore: function(origin, continueTooltip) {
            continueTooltip();

            var del = "_";
            var n = $(this).attr('name').split(del);
            var pre = n[1];
            var gru = n[3];
            var cap = n[5];

            if (origin.data('ajax') !== 'cached') {
                $.ajax({
                    type: 'POST',
                    url: 'funciones/fnPresupuesto.php',
                    data: {
                        opcion: 'ENLAZARPRESUPUESTO',
                        pre: pre,
                        gru: gru,
                        cap: cap
                    },
                    success: function(data) {
                        // update our tooltip content with our returned data and cache it
                        origin.tooltipster('update', '<div id="divnuevapre">' + data + '</div>').data('ajax', 'cached');
                    }
                });
            }


        },
        functionAfter: function(origin) {
            //alert('The tooltip has closed!');
            //content:""
            //origin.tooltipster('update','');
        }
    });*/
}

function cargarrecursosagregar(pre, gru, cap, act) {
    $('.modal-overlay').show();
    $("#selectp" + pre + "g" + gru + "c" + cap + "a" + act).addClass('modal-input');
    $("#divselectp" + pre + "g" + gru + "c" + cap + "a" + act).html('').show();
    $.ajax({
        type: 'POST',
        url: 'funciones/fnPresupuesto.php',
        data: { opcion: 'AGREGARRECURSO', pre: pre, gru: gru, cap: cap, act: act },
        success: function(data) {
            $("#divselectp" + pre + "g" + gru + "c" + cap + "a" + act).html(data);
        }
    });
}

function convertirdata4(id, pre, gru, cap) {
    // console.log("id" + id);
    $('.currency').formatCurrency({
        colorize: true
    });
    $("input").focus(function() {
        $(this).parent("td").addClass('focus');
        var nam = $(this).attr('class');

        if (nam == "currency") {
            var str = $(this).val()
            var res = str.replace("$", "");
            var res1 = res.replace(",", "").replace(",", "");
            var val = $(this).val()
            $(this).val(res1)
            if ($(this).val() < 0) {
                $(this).val('');
            }
        } else {
            if ($(this).val() < 0) {
                $(this).val('');
            }
        }

        /* var forma = formato_numero($( this ).val(), 2,'.', '')
          $( this ).val(forma);*/
        //jAlert(forma);

    });

    $("input").focusout(function() {
        $(this).parent("td").removeClass('focus');
        var nam = $(this).attr('class');

        if (nam == "currency") {
            if ($(this).val() < 0 || $(this).val() == "") {
                $(this).val(0.00);
            }
            $('.currency').formatCurrency({
                colorize: true
            });
        } else {
            if ($(this).val() < 0 || $(this).val() == "") {
                $(this).val('');
            }
        }
        /*var forma = formato_numero($( this ).val(), 2,',', '.')
          $( this ).val(forma)*/

    });
    var table = $('#tbinsumos' + id + 'p' + pre + 'g' + gru + 'c' + cap).DataTable({
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "language": {
            "lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
        },
        "order": [
            [1, 'asc']
        ],
        "paging": false,
        fnDrawCallback: function() {
            var k = 1;
            $('#tbinsumos' + id + 'p' + pre + 'g' + gru + 'c' + cap + ' tbody td').each(function() {
                $(this).attr('tabindex', k);
                k++;
            })
        }

    });

    /*  $("#selectp" + pre + "g" + gru + "c" + cap + "a" + id).keyup(function(e) {
        //console.log("Por aca");
        if (e.keyCode == 13) {
            $('#tbrecursosagregadosp' + pre + 'g' + gru + 'c' + cap + 'a' + id).DataTable().search($(this).val(), '', '').draw();
        }
    }).change(function() {
        $('#tbrecursosagregadosp' + pre + 'g' + gru + 'c' + cap + 'a' + id).DataTable().search($(this).val(), '', '').draw();
        
    });*/

    var currCell = $('#tbinsumos' + id + 'p' + pre + 'g' + gru + 'c' + cap + ' tbody td').first();
    var editing = false;
    var val = 0;

    // User clicks on a cell
    $('#tbinsumos' + id + 'p' + pre + 'g' + gru + 'c' + cap + ' td').click(function() {
        currCell = $(this);
        currCell.toggleClass("editing");
        //currCell.focus();
        currCell.children('input').focus();
        val = currCell.children('input').val();
        //currCell.children('select').focus();
        //currCell.children('.select2').focus();
    });
    // User navigates table using keyboard
    $('#tbinsumos' + id + 'p' + pre + 'g' + gru + 'c' + cap + ' tbody').keydown(function(e) {
        var c = "";
        if (e.which == 39) {
            // Right Arrow
            c = currCell.next();
        } else if (e.which == 37) {
            // Left Arrow
            c = currCell.prev();
        } else if (e.which == 38) {
            // Up Arrow
            c = currCell.closest('tr').prev().find('td:eq(' +
                currCell.index() + ')');
        } else if (e.which == 40) {
            // Down Arrow
            c = currCell.closest('tr').next().find('td:eq(' +
                currCell.index() + ')');
        } else if (!editing && (e.which == 9 && !e.shiftKey)) {
            // Tab
            e.preventDefault();
            c = currCell.next();
        } else if (!editing && (e.which == 9 && e.shiftKey)) {
            // Shift + Tab
            e.preventDefault();
            c = currCell.prev();
        }

        // If we didn't hit a boundary, update the current cell
        if (c.length > 0) {
            currCell = c;
            currCell.focus();
            val = currCell.children('input').val();
            //currCell.children('input').focus();
            //currCell.children('select').focus();
            //INICIALIZARLISTAS('PRESUPUESTO');
        }
        if (e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40 && !e.shiftKey && e.which != 9) {
            if (e.which == 27) {
                currCell.children('input').val(val);
                //currCelli.focus();
            } else {
                currCell.children('input').focus();

            }
            // currCelli.children('input').val(String.fromCharCode(e.which))
        }
    });
    $('.ivasactual').tooltipster({
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        animation: 'slide',
        updateAnimation: 'scale',
        position: 'bottom',
        positionTracker: false,
        multiple: true,
        distance: 1,
        offsetY: -12,
        arrow: false,
        trigger: 'click',


        functionBefore: function(origin, continueTooltip) {
            continueTooltip();
            var id = $(this).attr('id');
            var n = id.split("_");
            var idd = n[0];
            var pre = n[1];
            var gru = n[2];
            var cap = n[3];
            var act = n[4];
            var ins = n[5];

            // if (origin.data('ajax') !== 'cached') {
            $.ajax({
                type: 'POST',
                url: 'funciones/fnPresupuestoInicial.php',
                data: {
                    opcion: "DATOSAIU",
                    idd: idd,
                    pre: pre,
                    gru: gru,
                    cap: cap,
                    act: act,
                    ins: ins
                },
                success: function(data) {
                    // update our tooltip content with our returned data and cache it
                    /*  origin.tooltipster({
                          content: data,
                          multiple: true,
                          contentAsHTML: true,
                          trigger: 'click',
                          theme: 'tooltipster-shadow',
                          contentCloning: true,
                        interactive: true,
                        animation: 'slide',
                         updateAnimation: 'scale',

                          position: 'bottom',
                          positionTracker: false,
                          distance: 1,
                          offsetY: -12,
                          arrow: false,
                      });*/

                    origin.tooltipster('update', 'VALORES AIU : ' + data).data('ajax', 'cached');
                }
            });
            //}
            // when the request has finished loading, we will change the tooltip's content
            /*$.post('funciones/fnPresupuestoInicial.php',{opcion:"DATOSAIU",idd:idd,pre:pre,gru:gru,cap:cap,act:act,ins:ins},
             function(data)
             {
             origin.tooltipster('content', 'VALORES AIU : ' + data);
             });    */
        },
        functionAfter: function(origin) {
            //console.log("holacerrado");
            // window.alert('The tooltip has closed!');
            //origin.tooltipster();
        }
    });
    /*$('#selectp'+pre+'g'+gru+'c'+cap+'a'+id).tooltipster({
       // content: "",
       // contentAsHTML: true,
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        animation: 'slide',
        updateAnimation: 'scale',
        position: 'bottom',
        positionTracker: false,
        multiple: true,
        maxWidth: 1100,
        minWidth: 1100,
        distance: 1,
        offsetY: -12,
        arrow: false,
        trigger: 'click',
        functionBefore: function(origin, continueTooltip) {
            continueTooltip();

            var del = "_";

            var n = $(this).attr('name').split(del);
            var pre = n[1];
            var gru = n[3];
            var cap = n[5];
            var act = n[7];
            //console.log('Holac');
            // next, we want to check if our data has already been cached
            //if (origin.data('ajax') !== 'cached') {
                $.ajax({
                    type: 'POST',
                    url: 'funciones/fnPresupuesto.php',
                    data: {
                        opcion: 'AGREGARRECURSO',
                        pre: pre,
                        gru: gru,
                        cap: cap,
                        act: act
                    },
                    success: function(data) {
                        
                       origin.tooltipster('update', '<div id="divnuevorecurso">' + data + '</div>').data('ajax', 'cached');
                    }
                });
          //  }

        },
        functionAfter: function(origin) {
            //alert('The tooltip has closed!');
            //console.log("Hola");
            //console.log("Inicializo recursos");
            $('#divnuevorecurso').html('');
            //origin.tooltipster();
        }
    });*/

    /*$(function(){*/
    $('#tbinsumos' + id + 'p' + pre + 'g' + gru + 'c' + cap + ' tbody').contextMenu({
        selector: 'tr.delete1',
        callback: function(key, options) {

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dat = row.data();
            var idd = $(this).attr('id');

            if (key == "delete") {
                setTimeout(CRUDPRESUPUESTO('DELETERECURSOACT', pre, gru, cap, id, idd), 1000);
            }

            // window.console && console.log(m) || alert(m); 
        },
        items: {
            "delete": {
                name: "Eliminar Recurso",
                icon: "delete"
            }
        }
    });


}


function convertirsub(id, pre, gru, cap) {

    var table = $('#tbsubanalisis' + id + "p" + pre + "g" + gru + "c" + cap).DataTable({
        "columnDefs": [{
                "targets": [2],
                "className": "dt-left"
            },
            {
                "targets": [3],
                "className": "dt-center"
            },
            {
                "targets": [4],
                "className": "dt-right"
            },
            {
                "targets": [5],
                "className": "dt-right"
            },
            {
                "targets": [6],
                "className": "dt-right"
            },

        ],
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "pagingType": "simple_numbers",
        "lengthMenu": [
            [10, 20, 30, -1],
            [10, 20, 30, "Todos"]
        ],
        "language": {
            "lengthMenu": "Ver _MENU_ registros",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
            "paginate": {
                "previous": "&#9668;",
                "next": "&#9658;"
            }
        },
        "paging": false,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "modulos/presupuesto/subanalisis_actividades.php",
            "data": {
                id: id,
                pre: pre,
                gru: gru,
                cap: cap
            }
        },
        "columns": [{
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                "data": "Codigo"
            },
            {
                "data": "Nombre"
            },
            {
                "data": "Unidad",
                "className": "dt-center"
            },
            {
                "data": "Total",
                "className": "dt-right"
            },
            {
                "data": "TotalA",
                "className": "dt-right"
            },
            {
                "data": "Alcance",
                "className": "dt-right"
            },
        ],
        "order": [
            [2, 'asc']
        ],
        "searching": false
    });

    $('table #tbsubanalisis' + id + 'p' + pre + 'g' + gru + 'c' + cap + ' thead tr th').each(function(index, element) {
        index += 1;
        $('tr td:nth-child(' + index + ')').attr('data-title', $(this).attr('data-title'));
    });
    // DataTable
    var tables = $('#tbsubanalisis' + id + 'p' + pre + 'g' + gru + 'c' + cap).DataTable();
    // Apply the search
    tables.columns().every(function() {
        var that = this;

        $('input', this.footer()).on('keyup change', function() {
            that
                .search(this.value)
                .draw();
        });
    });

    var detailRows1 = [];
    $('#tbsubanalisis' + id + 'p' + pre + 'g' + gru + 'c' + cap + ' tbody').on('click', 'tr td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var dat = row.data();
        var ids = dat.Codigo;
        // var idx = $.inArray( tr.attr('id'), detailRows );        
        if (row.child.isShown()) {
            tr.removeClass('shown');
            row.child.hide();
            // Remove from the 'open' array
            // detailRows.splice( idx, 1 );
        } else {
            // Open this row
            var table1s = "<div class='panel panel-default'><div class='panel-body' id='divchil" + id + "' >" +
                "<div id='no-more-tables'><table  class='table table-bordered table-condensed compact' id='tbinsumos" + id + "'>";
            table1s += "<thead><tr>" +
                "<th class='dt-head-center'>CODIGO</th>" +
                "<th class='info dt-head-center' width='20%'>RECURSO</th>" +
                "<th class='info dt-head-center' width='7%'>UND</th>" +
                "<th class='info dt-head-center 'width='6%'>REND</th>" +
                "<th class='info dt-head-center' width='6%'>V/UNITARIO</th>" +
                "<th class='info dt-head-center' width='6%'>MATERIAL</th>" +
                "<th class='info dt-head-center' width='6%'>EQUIPO</th>" +
                "<th class='info dt-head-center' width='6%'>M.DE.O</th>" +
                "<th class='danger dt-head-center' width='6%'>REND ACTUAL.</th>" +
                "<th class='danger dt-head-center' width='12%'>VR.UNIT ACTUAL</th>" +
                "<th class='danger dt-head-center' width='6%'>AIU</th>" +
                "<th class='danger dt-head-center' width='6%'>MATERIAL</th>" +
                "<th class='danger dt-head-center' width='6%'>EQUIPO</th>" +
                "<th class='danger dt-head-center' width='6%'>M.DE.O</th>" +
                "<th class='success dt-head-center' width='6%'>C.ALCANCE</th>" +
                "</tr></thead><tbody>";


            $.post('funciones/fnPresupuestoInicial.php', {
                    opcion: "LISTAINSUMOSSUBANALISIS",
                    id: ids,
                    pre: pre,
                    gru: gru,
                    cap: cap,
                    act: id
                },
                function(dato) {
                    var res = dato[0].res;
                    if (res == "no") {} else {
                        for (var i = 0; i < dato.length; i++) {
                            table1s += "<tr>" +
                                "<td data-title='Codigo'>" + dato[i].insu + "</td>" +
                                "<td data-title='Insumo'>" + dato[i].insumo + "</td>" +
                                "<td data-title='Unidad'>" + dato[i].unidad + "</td>" +
                                "<td data-title='Rendimiento' class='dt-center'>" + dato[i].rendi + "</td>" +
                                "<td data-title='Valor Unitario' class='dt-right currency' title='" + dato[i].valor + "'>$" + dato[i].valor1 + "</td>" +
                                "<td data-title='Material' class='dt-right currency'>$" + dato[i].material + "</td>" +
                                "<td data-title='Equipo' class='dt-right currency'>$" + dato[i].equipo + "</td>" +
                                "<td data-title='Mano de obra' class='dt-right currency'>$" + dato[i].mano + "</td>" +
                                "<td data-title='Rendimiento Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].REA + "</td>" +
                                //onmouseover='INICIALIZARTOOLTIP()'
                                "<td data-title='Valor Unitario Ejecutado' class='dt-right' style='cursor:pointer'>" + dato[i].VAA + "</td>" +
                                "<td><a class='ivasactual' style='cursor:pointer'  title='This is bad content' id='" + dato[i].iddetalle + "_" + pre + "_" + gru + "_" + cap + "_" + ids + "_" + dato[i].insu + "'>AIU</a></td>" +
                                "<td data-title='Material' class='dt-right'><div id='materials" + dato[i].iddetalle + "'>$" + dato[i].materiala + "</div></td>" +
                                "<td data-title='Equipo' class='dt-right'><div id='equipos" + dato[i].iddetalle + "' class='currency'>$" + dato[i].equipoa + "</div></td>" +
                                "<td data-title='Mano' class='dt-right'><div id='manos" + dato[i].iddetalle + "' class='currency'>$" + dato[i].mano + "</div></td>" +
                                "<td  data-title='C.Alcance' class='dt-right'><div id='cambios" + dato[i].iddetalle + "' class='currency'>$" + dato[i].totalact + "</div></td></tr>";
                        }
                    }
                    table1s += "</tbody></table></div></div></div>";
                    row.child(table1s).show();
                    convertirdata4(id);
                }, "json");
            //row.child(format(row.data()) ).show();
            tr.addClass('shown');
            // row.child( format( row.data() ) ).show();

            // Add to the 'open' array
            //if ( idx === -1 ) {
            // detailRows.push( tr.attr('id') );
            //}
        }
    });
    // On each draw, loop over the `detailRows` array and show any child rows
    tables.on('draw', function() {
        $.each(detailRows1, function(i, id) {
            $('#' + id + ' td.details-control').trigger('click');
        });
    });
}

function filterGlobal(table, fil) {
    //console.log("Si entro"+table);
    $('#' + fil).keypress(function(e) {
        if (e.which == 13) {
            $('#' + table).DataTable().search($('#' + fil).val(), '', '').draw();
            setTimeout(function() { $('#' + fil).val(''); }, 1000)
        }
    });
}