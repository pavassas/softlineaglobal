// JavaScript Document


$(document).ready(function(e) {
	var pre = $('#presupuesto').val();
 $('.currency').formatCurrency({colorize: true});
 
 $( "#tbeventos input" ).focus(function() {
 $( this ).parent( "td" ).addClass('focus');

	var nam = $(this).attr('class');
		
	if(nam=="currency")
	{
		var str = $(this).val()
		var res = str.replace("$", "");
		var res1 = res.replace(",","").replace(",","");
		var val = $(this).val()
		$(this).val(res1)
		if($(this).val()<0)
		{
		  $(this).val('');
		}
	}
	else
	{
	   if($(this).val()<0)
		{
		  $(this).val('');
		}
	}
});

$( "#tbeventos input" ).focusout(function() {
$( this ).parent( "td" ).removeClass('focus');
 var nam = $(this).attr('class');
	 
	if(nam=="currency")
	{
		 if($(this).val()<0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
		$('.currency').formatCurrency({colorize: true});
	}
	else
	{
		 if($(this).val()<0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
	}
});
   

    var table = $('#tbeventos').DataTable( { 
		"dom": '<"top"i>rt<"bottom"lp><"clear">',
		"ordering": false,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[4], [4 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"sProcessing":'Buscando Datos...'
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/eventosjson.php",
                    "data": {pre:pre}
				},
		"columns": [
		     {	
			   "class":          "details-control",
			   "orderable":      false,				
			   "data":           null,
			   "defaultContent": ""
			},
			{ "orderable": false, "data": "Code"},
			{ "orderable": false, "data": null,"defaultContent": "" },		
			{ "orderable": false, "data": "Nombre" },	
			{ "orderable": false, "data": null,"defaultContent": "" },			
			{ "data": null,"defaultContent":"APROBO:" },
			{ "data": "Aprobo" },
			{ "orderable": false, "data": "TotalI","className":"dt-right" },
			{ "data": null,"defaultContent":"FECHA APROBACIÓN:" },
			{ "orderable": false, "data": "Fecha" },
			{ "orderable": false, "data": null,"defaultContent": "" },
			{ "orderable": false, "data": "TotalA","className":"dt-right" },
			{ "orderable": false, "data": "AlcanceA","className":"dt-right" },
		
			
		],
		"order": [[2, 'asc']],		
		fnDrawCallback: function () {			
			$('#tbeventos tbody tr').each(function () {
				$(this).css('background-color','rgba(215,215,215,0.5');
				$(this).addClass('deletee');			
			})
		},		
        paging: false
		
    } );
   
	//
	 // Add event listener for opening and closing details
    $('#tbeventos tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;
		var pre = $('#presupuesto').val();
		var fontsize = localStorage.getItem('fontSize');
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
		
		var table1 = "<div id='divchila"+pre+"c"+id+"' style='padding-left:35px'>"+
		"<table width='100%'  class='table table-bordered table-condensed compact' style='font-size:"+fontsize+"px' id='tbactividadp"+pre+"c"+id+"'>"+
		"<thead><tr>"+	
		"<th style='width:20px'></th>"+
		"<th style='width:20px'></th>"+
		"<th class='dt-head-center' >ITEM</th>"+
		"<th class='dt-head-center' style='width:200px'>DESCRIPCION</th>"+
		"<th class='dt-head-center'>UN</th>"+
		"<th class='info dt-head-center'>CANT</th>"+
		"<th class='info dt-head-center'>VR.UNIT</th>"+
		"<th class='info dt-head-center'>PPTO<br>INICIAL</th>"+		
		"<th class='danger dt-head-center'>CANT<br>PROYECTADA</th>"+
		"<th class='danger dt-head-center'>CANT<br>EJECUTADA</th>"+
		"<th class='danger dt-head-center'>CANT<br>FALTANTE</th>"+
		"<th class='danger dt-head-center'>VLR.UNIT<br>ACTUAL</th>"+
		"<th class='danger dt-head-center'>PPTO<br>ACTUAL</th>"+
		"<th class='success dt-head-center'>CAMBIO<br>ALCANCE</th>"+
		"<th>SUBA</th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuestoInicial.php',{opcion:"LISTAACTIVIDADESE",pre:pre,cap:id},//CREAR FUNCION
			function(dato)
			{
			     var res = dato[0].res;
				 if(res=="no"){}else
				 {					
					for(var i=0; i<dato.length; i++)
					{
						var numapu = dato[i].numapu;
						
						if(numapu>0){var col = "";}else if(numapu<=0){var col="style='background-color: rgba(255, 0, 0, 0.498039)'";}
					
						table1+="<tr id='"+dato[i].iddetalle+"' "+col+" class='trdeleteac' rel='"+dato[i].idactividad+"'>";				
					table1+="<td>"+dato[i].iddetalle+"</td>"+
					"<td class='details-control1'></td>"+				
					"<td data-title='ITEM' style='width:45px'>"+dato[i].items+"</td>"+
					"<td data-title='DESCRIPCION'>"+dato[i].actividad+"</td>"+
					"<td data-title='UN' style='width:62px'>"+dato[i].unidad+"</td>"+
					"<td data-title='CANT.INICIAL' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='canti"+dato[i].iddetalle+"'  name=''  type='text' value='"+dato[i].cantidad+"'  min='0'  onKeyPress='return NumCheck(event, this)'  onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA','"+id+"','"+dato[i].iddetalle+"','','')></td>"+
					"<td data-title='VLR.UNIT.INICIAL' style='width:80px'><div id='divtotapue"+dato[i].iddetalle+"' class='currency'>"+dato[i].apu+"</div></td>"+
					"<td data-title='PTO.INICIAL' style='width:80px' ><div id='divtotie"+dato[i].iddetalle+"' class='currency'>$"+dato[i].total+"</div></td>"+					
					"<td data-title='CANT.PROYECTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='cantp"+dato[i].iddetalle+"'  name=''  type='text' value='"+dato[i].cantidadp+"'  min='0'  onKeyPress='return NumCheck(event, this)'  onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA','"+id+"','"+dato[i].iddetalle+"','','')></td>"+
										
					"<td data-title='CANT.EJECUTADA' style='width:60px'><input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='cante"+dato[i].iddetalle+"' name='' type='text' value='"+dato[i].cantidade+"'  min='0'  onKeyPress='return NumCheck(event, this)'  onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARACTEXTRA','"+id+"','"+dato[i].iddetalle+"','','')></td>"+					
					"<td data-title='CANT.FALTANTE' style='width:60px'><div  style='cursor:pointer' id='divactfe"+dato[i].iddetalle+"'>"+dato[i].cantidadf+"</div></td>"+					
					"<td data-title='VLR.UNIT.ACTUAL' style='width:80px'><input disabled style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='vaae"+dato[i].iddetalle+"' name='' type='text' value='$"+dato[i].valoract+"'  min='0' onKeyPress='return NumCheck(event, this)' class='currency'/></td>"+
					"<td data-title='PPTO.ACTUAL' style='width:80px'><div id='divtotactae"+dato[i].iddetalle+"' class='currency'>$"+dato[i].totala+"</div></td>"+
					"<td data-title='CAMBIOS EN EL ALCANCE' style='width:92px'><div id='divtotactce"+dato[i].iddetalle+"' class='currency'>"+dato[i].totalal+"</div></td>"+
					"<td data-title='SUBANALISIS' style='width:92px'>"+dato[i].suba+"</td>"+
					"</tr>";
					
					}
				 }
				table1+="</tbody></table></div>"+
				"<div class='col-md-12'><input type='text' id='select"+pre+"c"+id+"' placeholder='ACTIVIDADES A AGREGAR'    name='pre_"+pre+"_cap_"+id+"'  class='form-control input-sm actagregare'  style='width:100%'/></div>";

				;
				row.child(table1).show();
				convertiracte(pre,id);
				//setTimeout(autoTabIndex("#tbactividadp"+pre+"g"+d+"c"+id,14),1000);
			},"json");
            tr.addClass('shown1');
         }
    } );	
	
	 $("#tbeventos tbody").contextMenu({
        selector: 'tr.deletee', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idd = dat.Codigo;			
			var pre = $('#presupuesto').val();			
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTOINICIAL('DELETEASIGNAREVENTO',idd,'','',''),1000);
			}				
        },
        items: {           
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    	});	
	});


function convertiracte(pre,cap)
{
	
	$("#select"+pre+"c"+cap).focus(function() {
	//	$(this).val('');
	});
	
	$("#select"+pre+"c"+cap).focusout(function() {
		//$(this).val('');
	});
	
$('.currency').formatCurrency({colorize: true});
$( "#select"+pre+"c"+cap + "input" ).focus(function() {
$( this ).parent( "td" ).addClass('focus');
	var nam = $(this).attr('class');
	var tit = $(this).attr('name');
	if(nam=="currency")
	{
		var str = $(this).val()
		var res = str.replace("$", "");
		var res1 = res.replace(",","").replace(",","");
		var val = $(this).val()
		$(this).val(res1)
		if($(this).val()<0 && tit!="busqueda")
		{
		  $(this).val('');
		}
	}
	else
	{
	   if($(this).val()<0)
		{
		  $(this).val('');
		}
	}

/* var forma = formato_numero($( this ).val(), 2,'.', '')
  $( this ).val(forma);*/
  //jAlert(forma);

});

$("#select"+pre+"c"+cap + "input" ).focusout(function() {
$( this ).parent( "td" ).removeClass('focus');
 var nam = $(this).attr('class');
 var tit = $(this).attr('name');	 
	if(nam=="currency")
	{
		 if(($(this).val()<0 || $(this).val()=="") && tit!="busqueda")
		 {
			$(this).val(0.00);
		 }
		$('.currency').formatCurrency({colorize: true});
	}
	else
	{
		 if($(this).val()<0 || $(this).val()=="")
		 {
			$(this).val();
		 }
	}	
/*var forma = formato_numero($( this ).val(), 2,',', '.')
  $( this ).val(forma)*/

});

   var table = $('#tbactividadp'+pre+'c'+cap).DataTable(
   {   
       "columnDefs": 
		 [ 		   
			{ "targets": [0], "visible": false	 },
			{ "targets": [4], "className": "dt-center" },
			{ "targets": [5], "className": "dt-center" },
			{ "targets": [6], "className": "dt-right" },
			{ "targets": [7], "className": "dt-right" },
			{ "targets": [8], "className": "dt-center" },
			{ "targets": [9], "className": "dt-center" },
			{ "targets": [10], "className": "dt-center" },
			{ "targets": [11], "className": "dt-right" },
			{ "targets": [12], "className": "dt-right" },
			{ "targets": [13], "className": "dt-right" },
			{ "targets": [14], "visible": false}
         ],
		"searching":false,
		"ordering": false,
		"info": false,
		"autoWidth": true,
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		},		
        "paging":         false,
		fnDrawCallback: function() {
			$("#tbactividadp"+pre+"c"+cap+" thead").remove();
			var k = 1;
			$("#tbactividadp"+pre+"c"+cap+" tbody td").each(function () {
				$(this).attr('tabindex', k);
				k++;
			})

		}
    } );
	
	 $('#tbactividadp'+pre+'c'+cap+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		
		var suba = dat[13];
		
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
			var id = tr.attr('id');
			var act = tr.attr('rel');
			var pre = $('#presupuesto').val();
            // Open this row
			var table2 = "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDPRESUPUESTOINICIAL('VERAPUE','"+id+"','"+act+"','','"+cap+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>";
			
			if(suba==1)
			{
			table2+="<li>"+
			"<a onclick=CRUDPRESUPUESTOINICIAL('VERSUBANALISISE','"+id+"','"+act+"','','"+cap+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>";
			}
			table2+="</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' ><div><table  class='table table-bordered table-condensed compact' id='tbinsumos"+id+"p"+pre+"c"+cap+"'>";
			table2+="<thead><tr>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"<th class='dt-head-center' width='20%'>RECURSO</th>"+
			"<th class='dt-head-center' width='7%'>UND</th>"+
			"<th class='dt-head-center info 'width='6%'>REND</th>"+
			"<th class='info dt-head-center' width='6%'>V/UNITARIO</th>"+
			"<th class='info dt-head-center' width='6%'>MATERIAL</th>"+
			"<th class='info dt-head-center' width='6%'>EQUIPO</th>"+
			"<th class='info dt-head-center' width='6%'>M.DE.O</th>"+
			"<th class='danger dt-head-center' width='6%'>REND ACTUAL.</th>"+
			"<th class='danger dt-head-center' width='12%'>VR.UNIT ACTUAL</th>"+
			"<th class='danger dt-head-center' width='6%'>AIU</th>"+
			"<th class='danger dt-head-center' width='6%'>MATERIAL</th>"+
			"<th class='danger dt-head-center' width='6%'>EQUIPO</th>"+
			"<th class='danger dt-head-center' width='6%'>M.DE.O</th>"+
			"<th class='success dt-head-center' width='6%'>C.ALCANCE</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuestoInicial.php',{opcion:"LISTAINSUMOSE",id:id,pre:pre,cap:cap,act:act},
			function(dato)
			{		
			  var res = dato[0].res;
			  if(res=="no"){}else
			  {				
				for(var i=0; i<dato.length; i++)
				{		
					table2+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right currency'>$"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right currency'>$"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right currency'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td>"+					
					"<td data-title='Rendimiento Ejecutado' class='dt-right' style='cursor:pointer'>"+dato[i].REA+"</td>"+
					
					"<td data-title='Valor Unitario Ejecutado' class='dt-right' style='cursor:pointer'>"+dato[i].VAA+"</td>"+
					"<td title='Hola'><a class='ivasactuale' style='cursor:pointer' onmouseover='INICIALIZARTOOLTIP()'  id='"+dato[i].iddetalle+"_"+pre+"_"+cap+"_"+id+"_"+act+"_"+dato[i].insu+"'>AIU<span id='incpe"+dato[i].deti+"'>"+dato[i].incpe+"</span></a></td>"+
					"<td data-title='Material' class='dt-right'>"+dato[i].materiala+"</td>"+
					"<td data-title='eqipo' class='dt-right'>"+dato[i].equipoa+"</td>"+
					"<td data-title='mano' class='dt-right'>"+dato[i].manoa+"</td>"+					
					"<td  data-title='C.Alcance' class='dt-right'>"+dato[i].totalact+"</td></tr>";				 
				}
			  }
			table2+="</tbody></table></div></div></div>";
			row.child(table2).show();
			convertirinsumos(id,pre,cap,act);
			//setTimeout(autoTabIndex2("#tbinsumos"+id+"p"+pre+"c"+cap,14),1000);
			setTimeout(INICIALIZARTOOLTIP(),1000)
			},"json");
            tr.addClass('shown1');
        }
    });
	
	$("#select"+pre+"c"+cap).keyup(function(e){
		//console.log("Por aca");
	    if(e.keyCode==13)
		{
			$("#tbactividadesagregadasp"+pre+"c"+cap).DataTable().search($(this).val(),'','').draw();
		}
	}).change(function(){
	       $("#tbactividadesagregadasp"+pre+"c"+cap).DataTable().search($(this).val(),'','').draw();
	});
	
	var currCell = $('#tbactividadp'+pre+'c'+cap+' tbody td').first();
	var editing = false;
	var val = 0;	
	// User clicks on a cell
	$('#tbactividadp'+pre+'c'+cap+' td').click(function() {
	currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	val = currCell.children('input').val();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbactividadp'+pre+'c'+cap+' tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell.closest('tr').prev().find('td:eq(' + 
		  currCell.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell.closest('tr').next().find('td:eq(' + 
		  currCell.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell = c;
		currCell.focus();
		val = currCell.children('input').val();
		//currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {
		   
		   if(e.which==27)
		   {
			 currCell.children('input').val(val); 
			 //currCell.focus();
		   }
		   else
		   {			
				
				currCell.children('input').focus();
		   }
	   }
	
	});
	
	$('#tbactividadp'+pre+'c'+cap+' tbody tr')
        .on( 'mouseover', function () {			
			//$(this).css('background-color','');
			$(this).addClass('highlight');
        } )
        .on( 'mouseleave', function () {
			//$(this).css('background-color','rgba(215,215,215,0.5);');
			$(this).removeClass('highlight');           
        } );
	
     $("#select"+pre+"c"+cap).tooltipster({
		        content:"",
				contentAsHTML: true,
				theme:'tooltipster-shadow',
				contentCloning: false,
				interactive:true,
				animation:'slide',	
				updateAnimation:'scale',								
				position: 'bottom',
				positionTracker: false,
				multiple: true,							
				maxWidth:1100,
				minWidth:1100,
				distance:1,
				offsetY:-12,
				arrow:false,
				trigger: 'click',						
				functionBefore: function(origin, continueTooltip) {					
				continueTooltip();	
				
				var del = "_";
				var n = $(this).attr('name').split(del);
				var pre = n[1];
				var cap = n[3];
				
				if (origin.data('ajax') !== 'cached') {
				 $.ajax({
					type: 'POST',
					url: 'funciones/fnPresupuesto.php',
					data: {opcion:'AGREGARACTIVIDADEVENTO',pre:pre,cap:cap},//agregaractividadactual
					success: function(data) {
					   // update our tooltip content with our returned data and cache it
					   origin.tooltipster('update','<div id="divnueva">' +  data + '</div>').data('ajax', 'cached');
					}
				 });
				}
					 
				
				   },				   
				functionAfter: function(origin) {
					//alert('The tooltip has closed!');
					//content:""
					//origin.tooltipster('update','');
				}
			});
			
		$("#tbactividadp"+pre+"c"+cap+" tbody").contextMenu({
        selector: 'tr.trdeleteac', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idd = dat[0];
			//var ida = dat[10];
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTOINICIAL('DELETEACTEVENTO',cap,idd,'',''),1000);
			}			
        },
        items: {            
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
}

function convertirinsumos(id,pre,cap,act)
{
	console.log("Conversion tabla recursos en eventos");
	$('.currency').formatCurrency({colorize: true});
$( "input" ).focus(function() {
$( this ).parent( "td" ).addClass('focus');
	var nam = $(this).attr('class');
		
	if(nam=="currency")
	{
		var str = $(this).val()
		var res = str.replace("$", "");
		var res1 = res.replace(",","").replace(",","");
		var val = $(this).val()
		$(this).val(res1)
		if($(this).val()<0)
		{
		  $(this).val('');
		}
	}
	else
	{
	   if($(this).val()<0)
		{
		  $(this).val('');
		}
	}

/* var forma = formato_numero($( this ).val(), 2,'.', '')
  $( this ).val(forma);*/
  //jAlert(forma);

});

$( "input" ).focusout(function() {
$( this ).parent( "td" ).removeClass('focus');
 var nam = $(this).attr('class');
	 
	if(nam=="currency")
	{
		 if($(this).val()<0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
		$('.currency').formatCurrency({colorize: true});
	}
	else
	{
		 if($(this).val()<0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
	}	
/*var forma = formato_numero($( this ).val(), 2,',', '.')
  $( this ).val(forma)*/

});
    var table = $('#tbinsumos'+id+'p'+pre+'c'+cap).DataTable(
   {   
		"searching":false,
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		},
		"order": [[1, 'asc']],
        "paging":         false,
		fnDrawCallback: function () {
			var k = 1;
			$('#tbinsumos'+id+'p'+pre+'c'+cap+' tbody td').each(function () {
				$(this).attr('tabindex', k);
				k++;
			})
		}
		
    } );

	var currCell = $('#tbinsumos'+id+'p'+pre+'c'+cap+' tbody td').first();
	var editing = false;
	var val = 0;
	
	// User clicks on a cell
	$('#tbinsumos'+id+'p'+pre+'c'+cap+' td').click(function() {
	currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	val = currCell.children('input').val();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbinsumos'+id+'p'+pre+'c'+cap+' tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell.closest('tr').prev().find('td:eq(' + 
		  currCell.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell.closest('tr').next().find('td:eq(' + 
		  currCell.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell = c;
		currCell.focus();
		val = currCell.children('input').val();
		//currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {   
		   if(e.which==27)
		   {
			 currCell.children('input').val(val); 
			 //currCelli.focus();
		   }
		   else
		   { 	    
		       
		         currCell.children('input').focus();
		         
		   }
		  // currCelli.children('input').val(String.fromCharCode(e.which))
	   }


	});
    console.log("Conversion tabla recursos en eventos 2");
    $('.ivasactuale').tooltipster({
        content: "",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        contentCloning: false,
        interactive: true,
        animation: 'slide',
        updateAnimation: 'scale',
        position: 'bottom',
        positionTracker: false,
        multiple: true,
        distance: 1,
        offsetY: -12,
        arrow: false,
        trigger: 'click',


        functionBefore: function(origin, continueTooltip) {
            continueTooltip();
            var id = $(this).attr('id');
            var n = id.split("_");
            var idd = n[0];
            var pre = n[1];
            console.log("Prueba de tooltip eventos de cambio");

            // var gru = n[2];
            var cap = n[2];
            var idc = n[3];
            var act = n[4];
            var ins = n[5];

            // if (origin.data('ajax') !== 'cached') {
            $.ajax({
                type: 'POST',
                url: 'funciones/fnPresupuestoInicial.php',
                data: {
                    opcion: "DATOSAIUE",
                    idd: idd,
                    pre: pre,
                    idc:idc,
                    cap: cap,
                    act: act,
                    ins: ins
                },
                success: function(data) {
                    // update our tooltip content with our returned data and cache it
                    /*  origin.tooltipster({
                          content: data,
                          multiple: true,
                          contentAsHTML: true,
                          trigger: 'click',
                          theme: 'tooltipster-shadow',
                          contentCloning: true,
                        interactive: true,
                        animation: 'slide',
                         updateAnimation: 'scale',

                          position: 'bottom',
                          positionTracker: false,
                          distance: 1,
                          offsetY: -12,
                          arrow: false,
                      });*/

                    origin.tooltipster('update','VALORES AIU EVENTOS : ' + data).data('ajax', 'cached');
                }
            });
            //}
            // when the request has finished loading, we will change the tooltip's content
            /*$.post('funciones/fnPresupuestoInicial.php',{opcion:"DATOSAIU",idd:idd,pre:pre,gru:gru,cap:cap,act:act,ins:ins},
             function(data)
             {
             origin.tooltipster('content', 'VALORES AIU : ' + data);
             });	*/
        },
        functionAfter: function(origin) {
            //console.log("holacerrado");
            // window.alert('The tooltip has closed!');
            //origin.tooltipster();
        }
    });
}


function convertirsube(id,pre,cap,act)
{

    var table = $('#tbsubanalisis'+id+"p"+pre+"c"+cap).DataTable({
		"columnDefs": 
		 [ 
			{ "targets": [2], "className": "dt-left" },
			{ "targets": [3], "className": "dt-center" },
			{ "targets": [4], "className": "dt-right" },
			{ "targets": [5], "className": "dt-right" },
			{ "targets": [6], "className": "dt-right" },			
			       
         ],
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,	
		"ajax": {
                    "url": "modulos/presupuesto/subanalisis_actividadese.php",
                    "data": {id:id,pre:pre,cap:cap,act:act}
				},
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},				
			{ "data": "Codigo" },
			{ "data": "Nombre" },			
			{ "data": "Unidad","className": "dt-center" },			
			{ "data": "Total", "className": "dt-right"},
			{ "data": "TotalA", "className": "dt-right"},
			{ "data": "Alcance", "className": "dt-right"},
			
		],
		"order": [[2, 'asc']]	,
		"searching":false	
    } );
     
	
	$('table #tbsubanalisis'+id+'p'+pre+'c'+cap+' thead tr th').each(function(index,element){
	index += 1;
	$('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
    // DataTable
    var tables= $('#tbsubanalisis'+id+'p'+pre+'c'+cap).DataTable();
 
    // Apply the search
    tables.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	 var detailRows1 = [];
 
    $('#tbsubanalisis'+id+'p'+pre+'c'+cap+' tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var ids = dat.Codigo;		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1s= "<div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-bordered table-condensed compact' id='tbinsumos"+id+"'>";
			table1s+="<thead><tr>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"<th class='info dt-head-center' width='20%'>RECURSO</th>"+
			"<th class='info dt-head-center' width='7%'>UND</th>"+
			"<th class='info dt-head-center 'width='6%'>REND</th>"+
			"<th class='info dt-head-center' width='6%'>V/UNITARIO</th>"+
			"<th class='info dt-head-center' width='6%'>MATERIAL</th>"+
			"<th class='info dt-head-center' width='6%'>EQUIPO</th>"+
			"<th class='info dt-head-center' width='6%'>M.DE.O</th>"+
			"<th class='danger dt-head-center' width='6%'>REND ACTUAL.</th>"+
			"<th class='danger dt-head-center' width='12%'>VR.UNIT ACTUAL</th>"+
			"<th class='danger dt-head-center' width='6%'>AIU</th>"+
			"<th class='danger dt-head-center' width='6%'>MATERIAL</th>"+
			"<th class='danger dt-head-center' width='6%'>EQUIPO</th>"+
			"<th class='danger dt-head-center' width='6%'>M.DE.O</th>"+
			"<th class='success dt-head-center' width='6%'>C.ALCANCE</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuestoInicial.php',{opcion:"LISTAINSUMOSSUBANALISISE",id:ids,pre:pre,gru:gru,cap:cap,act:id,acti:act},
			function(dato)
			{		
			  var res = dato[0].res;
			  if(res=="no"){}else
			  {				
				for(var i=0; i<dato.length; i++)
				{		
					table1s+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right currency'>$"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right currency'>$"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right currency'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right currency'>$"+dato[i].mano+"</td>"+					
					"<td data-title='Rendimiento Ejecutado' class='dt-right' style='cursor:pointer'>"+dato[i].REA+"</td>"+
					
					"<td data-title='Valor Unitario Ejecutado' class='dt-right' style='cursor:pointer'>"+dato[i].VAA+"</td>"+
					"<td title='Hola'><a class='ivasactual' style='cursor:pointer' onmouseover='INICIALIZARTOOLTIP()' title='This is bad content' id='"+dato[i].iddetalle+"_"+pre+"_"+cap+"_"+ids+"_"+dato[i].insu+"'>AIU</a></td>"+
					"<td data-title='Material' class='dt-right'><div id='materials"+dato[i].iddetalle+"'>$"+dato[i].materiala+"</div></td>"+
					"<td data-title='eqipo' class='dt-right'><div id='equipos"+dato[i].iddetalle+"' class='currency'>$"+dato[i].equipoa+"</div></td>"+
					"<td data-title='mano' class='dt-right'><div id='manos"+dato[i].iddetalle+"' class='currency'>$"+dato[i].mano+"</div></td>"+					
					"<td  data-title='C.Alcance' class='dt-right'><div id='cambios"+dato[i].iddetalle+"' class='currency'>$"+dato[i].totalact+"</div></td></tr>";				 
				}
			  }
			table1s+="</tbody></table></div></div></div>";
				row.child(table1s).show();
				//convertirdata4(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
		tables.on( 'draw', function () {
			$.each( detailRows1, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
 

}

function filterGlobale (table,fil) {
	//console.log("Si entro"+table);
    $('#'+table).DataTable().search(
        $('#'+fil).val(),'',''
    ).draw();
}
