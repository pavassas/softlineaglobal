function convertirdata4(d)
{
   var table = $('#tbgrupos'+d).DataTable(
   {   
        
         "columnDefs": 
		 [ 
		   { "targets": [ 4 ], "className": "dt-center" },
		   { "targets": [ 5 ], "className": "dt-right" },
		   { "targets": [ 6 ], "visible": false},
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Grupos ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"order": [[2, 'asc']]
    } );
	
	 $('#tbgrupos'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var pre = d;
		var id = dat[6];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
			var table1 = "<div id='divchil"+id+"'  class='col-xs-12'><table  class='table table-condensed table-striped' id='tbcapitulo"+id+"'>";
			table1+="<thead><tr>"+
			"<th class='dt-head-center'></th>"+
			"<th class='dt-head-center'>CAPITULO</th>"+
			"<th class='dt-head-center'>UN</th>"+
			"<th class='dt-head-center'>CANT</th>"+
			"<th class='dt-head-center'>VR.UNIT</th>"+
			"<th class='dt-head-center'>VR.TOTAL</th>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTACAPITULOS",gru:id,pre:pre},//CREAR FUNCION
			function(dato)
			{				
			    var res = dato[0].res;
				if(res=="no"){}else
				{				
					for(var i=0; i<dato.length; i++)
					{
						table1+="<tr style='background-color:rgba(196,215,155,1.00)'><td class='details-control1'></td>"+
						"<td data-title='CAP'>"+dato[i].nombre+"</td>"+
						"<td data-title='UN'></td>"+
						"<td data-title='CANT'></td>"+
						"<td data-title='VR.UNIT'></td>"+
						"<td data-title='VR.TOTAL'>$"+dato[i].total+"</td>"+
						
						"<td data-title='CODIGO'>"+dato[i].capitulo+"</td></tr>";
					}
				}
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata(id,pre);
				},"json");
				//row.child(format(row.data()) ).show();
				tr.addClass('shown1');
			}
    });
}

function convertirdata3(pre,d,cap)
{
   var table = $("#tbactividadp"+pre+"g"+d+"c"+cap).DataTable(
   {   
        
         "columnDefs": 
		 [ 
		   { "targets": [ 4 ], "className": "dt-center" },
		   { "targets": [ 5 ], "className": "dt-right" },
		   { "targets": [ 6 ], "className": "dt-right"},
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"order": [[2, 'asc']],
		"paging":false
    } );
	
	 $("#tbactividadp"+pre+"g"+d+"c"+cap+" tbody").on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[1];
		var fontsize = localStorage.getItem('fontSize');
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
			var table2 = "<div id='divchil"+id+"' class='col-xs-12'><table  class='table table-condensed table-bordered' id='tbinsumos"+id+"' style='font-size:"+fontsize+"px'>";
			table2+="<thead><tr>"+
			"<th class='dt-head-center'>RECURSO</th>"+
			"<th class='dt-head-center'>UN.MEDIDA</th>"+
			"<th class='dt-head-center'>REND</th>"+
			"<th class='dt-head-center'>VR.UNIT</th>"+
			"<th class='dt-head-center'>MATERIAL</th>"+
			"<th class='dt-head-center'>EQUIPO</th>"+
			"<th class='dt-head-center'>M.DE.O</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id,pre:'',gru:'',cap:''},
			function(dato)
			{
		
				for(var i=0; i<dato.length; i++)
				{
					table2+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='UN.MEDIDA'>"+dato[i].unidad+"</td>"+
					"<td data-title='REND' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='VR.UNIT' class='dt-right'>$"+dato[i].valor+"</td>"+
					"<td data-title='MATERIAL' class='dt-right'>$ "+dato[i].material+"</td>"+
					"<td data-title='EQUIPO' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='M.DDE.O' class='dt-right'>$"+dato[i].mano+"</td></tr>";
				}
			table2+="</tbody></table></div>";
			row.child(table2).show();
			},"json");
            tr.addClass('shown1');
        }
    });
}


function convertirdata(d,pre)
{
   var table = $('#tbcapitulo'+d).DataTable(
   {   
         "columnDefs": 
		 [ 
		   { "targets": [ 3 ], "className": "dt-center" },
		   { "targets": [ 4 ], "className": "dt-right" },
		   { "targets": [ 5 ], "className": "dt-right"},
           { "targets": [ 6 ], "visible": false },
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Capitulos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false
		
    } );
	
	  $('#tbcapitulo'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var cap = dat[6];
		var fontsize = localStorage.getItem('fontSize');
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
		var table1 = "<div id='divchila"+pre+"g"+d+"c"+cap+"'  class='col-xs-12'><table  class='table table-bordered table-striped' id='tbactividadp"+pre+"g"+d+"c"+cap+"' style='font-size:"+fontsize+"px'>";
		table1+="<thead><tr>"+
		"<th class='dt-head-center'></th>"+
		"<th class='dt-head-center'>CODIGO</th>"+
		"<th class='dt-head-center'>ACTIVIDAD</th>"+
		"<th class='dt-head-center'>UN</th>"+
		"<th class='dt-head-center'>CANT</th>"+
		"<th class='dt-head-center'>VR.UNIT</th>"+
		"<th class='dt-head-center'>VR.TOTAL</th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAACTIVIDADES",cap:cap,pre:pre,gru:d},//CREAR FUNCION
			function(dato)
			{
				var res = dato[0].res;
				if(res=="no"){}
				else
				{
					for(var i=0; i<dato.length; i++)
					{
					table1+="<tr>"+
					"<td class='details-control1'></td>"+				
					"<td data-title='CODIGO'>"+dato[i].idactividad+"</td>"+
					"<td data-title='ACTIVIDAD'>"+dato[i].actividad+"<span class='help-block' style='font-size:10px'>("+dato[i].ciudad+"-"+dato[i].tpp+")</span></td>"+
					"<td data-title='UN'>"+dato[i].unidad+"</td>"+
					"<td data-title='CANT'>"+dato[i].cantidad+"</td>"+
					"<td data-title='VR.TOTAL'>$"+dato[i].apu+"</td>"+
					"<td data-title='VR.TOTAL'>$"+dato[i].total+"</td></tr>";
		
					}
				}
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata3(pre,d,cap);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
         }
    } );
}
// JavaScript Document
$(document).ready(function(e) {
    

    var table = $('#tbpresupuesto').DataTable( { 
	      
          
		"columnDefs": 
		 [ 
		   { "targets": [ 7 ], "className": "dt-center" },
		   { "targets": [ 8 ], "className": "dt-center" },
		   { "targets": [ 9 ], "className": "dt-center" },
		   { "targets": [ 10 ], "className": "dt-center" },
		   { "targets": [ 11 ], "className": "dt-right" },
           { "targets": [ 12 ], "visible": false },
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10,20,30 ], ["Todos",10,20,30 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		}
		
    } );
     
	  $('#tbpresupuesto tfoot th').each( function () {
        var title = $('#tbpresupuesto tfoot th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
 
    // DataTable
    //var table = $('#tbpresupuesto').DataTable();

    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
    $('#tbpresupuesto tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[12];
		//alert(id);
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
			var table1 = "<div id='divchilg"+id+"'  class='col-xs-12'><table  class='table table-condensed table-striped' id='tbgrupos"+id+"'>";
			table1+="<thead><tr>"+
			"<th class='dt-head-center'></th>"+
			"<th class='dt-head-center'>GRUPO</th>"+
			"<th class='dt-head-center'>UN</th>"+
			"<th class='dt-head-center'>CANT</th>"+
			"<th class='dt-head-center'>VR.UNIT($)</th>"+
			"<th class='dt-head-center'>VR.TOTAL</th>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAGRUPOS",pre:id},//CREAR FUNCION
			function(data)
			{				
			  
			   var res = data[0].res;
			 //  alert(res);
			   if(res=="no")
			   {
			   }
			   else
			   {
					for(var i=0; i<data.length; i++)
					{
						table1+="<tr class='success'><td class='details-control1'></td>"+
						"<td data-title='GRU'>"+data[i].nombre+"</td>"+
						"<td data-title='UN'></td>"+
						"<td data-title='CANT'></td>"+
						"<td data-title='VLR UNIT($)'></td>"+
						"<td data-title='VLR TOTAL($)'>$"+data[i].total+"</td>"+
						
						"<td data-title='CODIGO'>"+data[i].grupo+"</td></tr>";
					}
			   }
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata4(id);
				},"json");
				//row.child(format(row.data())).show();
				tr.addClass('shown');
			}
	} );
	
	// Add event listener for opening and closing details
 
});
