// JavaScript Document
$(document).ready(function(e) {
    
	var pre = $('#idedicion').val();
	var gru = $('#idgru').val();
	var cap = $('#idca').val();

  var table2 = $('#tbactividadporagregar').DataTable( {       
         "columnDefs": 
		 [ 				
			{ "targets": [3], "className": "dt-left" },
			{ "targets": [4], "className": "dt-left" },
			{ "targets": [5], "className": "dt-center" },
			{ "targets": [7], "visible": false }			       
         ],
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Por Asociar",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		},
		"paging":true,
		"processing": true,
        "serverSide": true,		
        "ajax": { "url" :"modulos/presupuesto/actividadporagregarjson.php",
		"data":{pre:pre,gru:gru,cap:cap}
		},		
		"columns": [ 
		    {	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},			
			{   
			  "orderable": false,
			  "data":      "Agregar"				
			},
			{ "data": "Nombre" },
			{ "data": "Rendimiento" },
			{ "data": "Tipo" },
			{ "data": "Unidad" },
			{ "data": "Ciudad"}	,	
			{ "data": "Codigo"}			
		],
		"order": [[2, 'asc']]	
		
    } );
     
 
    // DataTable
    var table2 = $('#tbactividadporagregar').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	
	var detailRows = [];
 
    $('#tbactividadporagregar tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table2.row( tr );
		var dat = row.data();
		var id = dat.Codigo;
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1 = "<table  class='table table-bordered table-condensed compact' id='tbinsumoss"+id+"' style='font-size:9px'>";
			table1+="<thead><tr><th>COD</th><th>RECURSO</th><th>UND</th><th>REND</th><th>V/UNITARIO</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{		
				for(var i=0; i<dato.length; i++)
				{
					table1+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1+="</tbody></table>";
				row.child(table1).show();
				setTimeout(convertirdata(id),1000);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
	table2.on( 'draw', function () {
		$.each( detailRows, function ( i, id ) {
		$('#'+id+' td.details-control').trigger( 'click' );
		} );
	} );
	
	// Add event listener for opening and closing details
	var currCell1 = $('#tbactividadporagregar tbody td').first();
	var editing = false;
	
	// User clicks on a cell
	$('#tbactividadporagregar td').click(function() {
	currCell1 = $(this);
	currCell1.toggleClass("editing");
	//currCell.focus();
	currCell1.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbactividadporagregar tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell1.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell1.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell1.closest('tr').prev().find('td:eq(' + 
		  currCell1.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell1.closest('tr').next().find('td:eq(' + 
		  currCell1.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell1.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell1.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell1 = c;
		currCell1.focus();
		currCell1.children('input').focus();
		}
	});
});
function convertirdata(d)
{
   var table = $('#tbinsumoss'+d).DataTable(
   {   
        "columnDefs": 
		 [ 
			{ "targets": [2], "className": "dt-center"},
			{ "targets": [3], "className": "dt-center" },
			{ "targets": [4], "className": "dt-right" },
			{ "targets": [5], "className": "dt-right" },
			{ "targets": [6], "className": "dt-right" },  
			{ "targets": [7], "className": "dt-right" },          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,-1 ], [10,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Insumos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false
		 
    } );
}