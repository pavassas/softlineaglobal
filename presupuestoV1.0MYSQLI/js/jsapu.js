/* Formatting function for row details - modify as you need */
var currCell = $('#tbApu tbody td').first();
var editing = false;
// JavaScript Document
$(document).ready(function(e) {
	
	$('.currency').formatCurrency({colorize: true});
	$( "#tbApu input" ).focus(function() {
		$( this ).parent( "td" ).addClass('focus');
		var nam = $(this).attr('class');
		if(nam=="currency")
		{
			var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			var val = $(this).val()
			$(this).val(res1)
			if($(this).val()<0 && $(this).val()!="")
			{
			  $(this).val('');
			}
		}
		else
		{
		   if($(this).val()<0)
			{
			  $(this).val('');
			}
		}
	});

	$( "#tbApu input" ).focusout(function() {
		$( this ).parent( "td" ).removeClass('focus');
 		var nam = $(this).attr('class');
		if(nam=="currency")
		{
			 if(($(this).val()<0 || $(this).val()==""))
			 {
				$(this).val(0.00);
			 }
			$('.currency').formatCurrency({colorize: true});
		}
		else
		{
			 if($(this).val()<0 && $(this).val()!="")
			 {
				$(this).val(0.00);
			 }
		}	
	});
		var table2 = $('#tbApu').DataTable( {  
		"autoWidth": true,     
        "searching":false,
		"paging":false,
		"ordering": false,
		"info": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,15], ["Todos",15 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		//fixedHeader: true,
		fnDrawCallback: function() {
			var k = 1;
			$("#tbApu tbody td").each(function () {				
				$(this).attr('tabindex', k);				
				k++;
			})
		}		
    } );
     
	var table2 = $('#tbApu').DataTable();
	/*
	// User clicks on a cell
	$('#tbApu td').on('click',function() 
	{
		$('#tbApu tbody').on('keydown');
		currCell = $(this);		
		currCell.focus();
		//console.log("Estamos en la celda"+currCell.attr('tabindex'));		         

	});
	$('#tbApu td').on('dblclick',function() 
	{
		$('#tbApu tbody').on('keydown');
		currCell = $(this);
		//currCell.toggleClass("editing");
		//currCell.children('input').focus();		
		var allInputs = currCell.children(":input");
		if(allInputs.length>0)
		{				
			detener(currCell);		
		}
		else
		{
			currCell.focus();				
		}	
	});
	// User navigates table using keyboard
	   

	$('#tbApu tbody').on('keydown',function (e) {
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (e.which == 9 && !e.shiftKey) { 
        // Tab
       // e.preventDefault();
        c = currCell.next();
    } else if (e.which == 9 && e.shiftKey) { 
        // Shift + Tab
       // e.preventDefault();
        c = currCell.prev();
    } 	    
    // If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
	
		currCell = c;			
		currCell.focus();
		//console.log("Estamos en la celda 1 - "+currCell.attr('tabindex'));
	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {   
		   if(e.keyCode==13)
	       {
			 // console.log("Estamos en la celda ENTER1 - "+currCell.attr('tabindex'));	
			  var allInputs = currCell.children(":input");
			  if(allInputs.length>0)
			  {
				  detener(currCell);
			  }
			  else
			  {
				  currCell.focus();
			  }
			  	
		   }
		   else
		   {   
		     
			  	         
		   }
	   }
	}); 
	*/
	
	  var clonedHeaderRow;
    
       $(".persist-area").each(function() {
           clonedHeaderRow = $(".persist-header", this);
           clonedHeaderRow
             .before(clonedHeaderRow.clone())
             .css("width", clonedHeaderRow.width())
             .addClass("floatingHeader");
             
       });
       
       $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
		
	  $('#tbApu tbody').contextMenu({
        selector: 'tr.deleterecurso', 
        callback: function(key, options) {
			
			var del = "_";
			var n = $(this).attr('id').split(del);
			var pre = n[1];
			var gru = n[2];
			var cap = n[3];
			var act = n[4];
			var idd = n[5];
			
			
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTO('DELETERECURSOACTAPU',pre,gru,cap,act,idd),1000);
			}
			
           // window.console && console.log(m) || alert(m); 
        },
        items: {
            "delete": {name: "Eliminar Recurso", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
	
	$(".insrecurso").tooltipster({
		content:"",
		contentAsHTML: true,
		theme:'tooltipster-shadow',
		contentCloning: false,
		interactive:true,
		animation:'slide',	
		updateAnimation:'scale',								
		position: 'bottom',
		positionTracker: false,
		onlyOne:true,						
		maxWidth:1100,
		minWidth:1100,
		distance:1,
		offsetY:-12,
		arrow:false,
		trigger: 'click',						
		functionBefore: function(origin, continueTooltip) {					
		continueTooltip();
				
		var del = "_";
		var n = $(this).attr('name').split(del);
		var pre = n[1];
		var gru = n[3];
		var cap = n[5];
		var act = n[7];
		//console.log('Holac');
				 // next, we want to check if our data has already been cached
				if (origin.data('ajax') !== 'cached') {
				 $.ajax({
					type: 'POST',
					url: 'funciones/fnPresupuesto.php',
					data: {opcion:'AGREGARRECURSO2',pre:pre,gru:gru,cap:cap,act:act},
					success: function(data) {
						//var tabler = $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+id).DataTable();
					   // tabler.destroy();
					   // update our tooltip content with our returned data and cache it
					   origin.tooltipster('update','<div id="divnuevorecurso">' +  data + '</div>').data('ajax', 'cached');
					}
				 });
				}				
				
				   },				   
				functionAfter: function(origin) {
					//alert('The tooltip has closed!');
					//console.log("Hola");
					
					//origin.tooltipster();
				}
			});
});

function iniciar(cu)
{
	$('#tbApu td').on('click',function() 
	{
		$('#tbApu tbody').on('keydown');
		currCell = $(this);		
		currCell.focus();
		//console.log("Estamos en la celda"+currCell.attr('tabindex'));		         

	});
	
	$('#tbApu tbody').on('keydown',function (e) {
    var c = cu;
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
       // e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
       // e.preventDefault();
        c = currCell.prev();
    }     
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
		//console.log("Estamos en la celda"+c.attr('tabindex'));
			currCell = c;
			currCell.focus();
			//console.log("Estamos en la celda 2 - "+currCell.attr('tabindex'));
    	}
	if(e.which!=37 && e.which!=38 && e.which!=39 && e.which!=40 && !e.shiftKey && e.which!=9)
	   {   
		   if(e.keyCode==13)
	       {
			  detener(currCell);
			  //console.log("Estamos en la celda ENTER2 - "+currCell.attr('tabindex'));		
		   }
		   else
		   {   
		      //currCell.focus();
			 // console.log("Estamos en la celda 3- "+currCell.attr('tabindex'));		         
		   }
		  // currCelli.children('input').val(String.fromCharCode(e.which))
	   }
	});
}
function detener(cur)
{	
	cur.children('input').focus();
	$('#tbApu tbody').off('keydown');
	cur.children('input').on('keydown',function(e){
		var vala = $(this).attr('title');
		
		if(e.keyCode==27)
		{ 
			cur.children('input').val(vala);
			cur.children('input').off('keydown');
			currCell = cur;
			//currCell.focus();
			iniciar(currCell);
		}
		else
	    if(e.keyCode==13)
		{
		  var nam = $(this).attr('name');
		  var del = "_";
		  var n = nam.split(del);
		  var ele = n[0];
		  var idc = n[1]
		  var pre = n[2];
		  var gru = n[3];
		  var cap = n[4];
		  var act = n[5];
		  var ins = n[6];
		  //
		  cur.children('input').off('keydown');
		  cur = cur.closest('tr').next().find('td:eq(' + cur.index() + ')');
		  if(cur.length>0)
		  {
			  currCell = cur;
			  
		  }
		  //currCell.focus();
		  /*if(ele=="remren")
		  {
			  // console.log($('#remren'+idc).val());
			   guardarcantidadrenact(idc,pre,gru,cap,$('#remren'+idc).val(),act,ins);
		  }		  
		  else if(ele=="remval")
		  {
			 guardarvaloract(idc,pre,gru,cap,$('#remval'+idc).val(),act,ins)
		  }
		  else if(ele=="remrens")
		  {
			 guardarcantidadrenactsu(idc,pre,gru,cap,$('#remren'+pre+'g'+gru+'c'+cap+'a'+act+'s'+idc).val(),act,'')
		  }	*/	  
		  setTimeout(iniciar(cur),1000);
		}
		
		else if (e.keyCode == 38) { 
        // Up Arrow
          //cur = currCell.closest('tr').prev().find('td:eq(' +  currCell.index() + ')');
		  //currCell = cur;
		   setTimeout(iniciar(cur),1000);
    	} else if (e.keyCode == 40) { 

        // Down Arrow
          //cur= currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		  //currCell = cur;
		  setTimeout(iniciar(cur),1000);
   		} 
	});		
}