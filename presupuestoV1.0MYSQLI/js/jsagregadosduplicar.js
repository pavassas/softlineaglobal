
$(document).ready(function(e) {
    

    var table = $('#tbgruagregadosduplicar').DataTable( {       
         "columnDefs": 
		 [ 
		  { "targets": [7 ], "className": "dt-right" },
           { "targets": [8 ], "visible": false }          
         ],
		"ordering": false,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10,20,30 ], ["Todos",10,20,30 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Grupo Agregados",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false,
		fixedHeader: true	
    } );
    /* $('#tbgruagregadosduplicar tfoot th').each( function () {
	var title = $('#tbgruagregadosduplicar tfoot th').eq( $(this).index() ).text();
	if(title==""){$(this).html('');}else{
	$(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span></div>' );}
	} );*/
 
  
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	//
	
 
  // Add event listener for opening and closing details
    $('#tbgruagregadosduplicar tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[8];
		var pre = $('#idedicion').val();
		//alert(pre +"-"+id)
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
		var table1 = "<div id='divchilg"+id+"'><table  class='table table-condensed compact' id='tbcapitulosg"+id+"'>";
		table1+="<thead><tr>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'>CAP</th>"+
		"<th class='dt-head-center'>DESCRIPCIÓN</th>"+
		"<th class='dt-head-center'>UN</th>"+
		"<th class='dt-head-center'>CANT</th>"+
		"<th class='dt-head-center'>VR.UNIT</th>"+
		"<th class='dt-head-center'>VR.TOTAL</th>"+
		"<th class='dt-head-center'></th>"+
		"<th class='dt-head-center'></th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTACAPITULOS",pre:pre,gru:id},//CREAR FUNCION
			function(dato)
			{
				if(dato[0].res=="no")
				{
				
				}
				else
				{
				
					for(var i=0; i<dato.length; i++)
					{
						var cap = dato[i].capitulo;
					table1+="<tr style='background-color:rgba(215,215,215,0.5)' id='cap"+dato[i].idd+"'>"+
					"<td class='details-control1'></td>"+
					"<td style='vertical-align:middle'><a role='button' class='btn btn-block btn-default btn-xs' onClick=CRUDPRESUPUESTO('AGREGARACTIVIDAD',"+pre+",'"+id+"','"+cap+"','','')  style='width:20px; height:20px; '><i class='glyphicon glyphicon-plus' data-toggle='tooltip' title='Agregar Actividad'></i></a></td>"+
					"<td style='vertical-align:middle'><a role='button' class='btn btn-block btn-danger btn-xs' onClick=CRUDPRESUPUESTO('DELETEASIGNARCAPITULO',"+pre+",'"+id+"','"+cap+"','',"+dato[i].idd+")  style='width:20px; height:20px; '><i class='glyphicon glyphicon-trash' data-toggle='tooltip' title='Quitar Capitulo'></i></a></td>"+
					"<td data-title='CODIGO'>"+dato[i].codc+"</td>"+
					"<td data-title='NOMBRE'>"+dato[i].nombre+"</td>"+
					"<td data-title='UN'></td>"+
					"<td data-title='CANT'></td>"+
					"<td data-title=''></td>"+
					"<td data-title='Valor Total($)'><div id='divtotcap"+dato[i].idd+"'>$"+dato[i].total+"</div></td>"+
					"<td>"+dato[i].idd+"</td>"+
					"<td>"+dato[i].capitulo+"</td>"+
					"</tr>";
		
					}
				}
				table1+="</tbody></table></div>";
			
				row.child(table1).show();
				convertirdata2(id);
				
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
         }
    } );	

});


function convertirdata2(d)
{
   var pre = $('#idedicion').val();
   var table = $('#tbcapitulosg'+d).DataTable(
   {   
       "columnDefs": 
		 [ 
		    { "targets": [0], "orderable" : false },
			{ "targets": [1], "orderable" : false, "visible" : false},
			{ "targets": [2], "orderable" : false, "visible" : false},  
			{ "targets": [3], "visible" : true,"className" : "dt-center" },
			{ "targets": [6], "className" : "dt-center" },
			{ "targets": [7], "className" : "dt-right" },
			{ "targets": [8], "className" : "dt-right" }, 
			{ "targets": [9], "visible" : false} ,
			{ "targets": [10], "visible" : false} ,
			
			        
         ],
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Capitulos ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false,
		"order": [[3, 'asc']],
		fnDrawCallback: function() {
			$('#tbcapitulosg'+d+' thead').remove();
		}
    } );	
	 $('#tbcapitulosg'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var idc = dat[10];
		var fontsize = localStorage.getItem('fontSize');
		//console.log(idc);
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
		var table1 = "<span id='result"+pre+"g"+d+"c"+idc+"'></span>"+
		"<div id='divchila"+pre+"g"+d+"c"+idc+"' class='col-md-12'>"+		
		"<table  class='table table-bordered table-condensed compact' id='tbactividadp"+pre+"g"+d+"c"+idc+"' style='font-size:"+fontsize+"px'>";
		table1+="<thead><tr >"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px'></th>"+
		"<th class='dt-head-center' style='width:20px' >ITEM</th>"+
		"<th class='dt-head-center'>ACTIVIDAD</th>"+
		"<th class='dt-head-center' style='width:20px'>UN</th>"+
		"<th class='dt-head-center' style='width:20px'>CANT</th>"+
		"<th class='dt-head-center' style='width:100px'>VR.UNIT</th>"+
		"<th class='dt-head-center' style='width:100px'>VR.TOTAL</th>"+
		"<th class='dt-head-center'>CODIGO</th>"+
		"<th class='dt-head-center'>SUBA</th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAACTIVIDADES",cap:idc,pre:pre,gru:d},//CREAR FUNCION
			function(dato)
			{
				var res = dato[0].res;
				if(res=="no")
				{
				}
				else
				{					
					for(var i=0; i<dato.length; i++)
					{
					   var k = i+1;
					   if(k<10){ k = "0"+k}
					   var f = idc +"."+k;
						
					table1+="<tr id='act"+dato[i].iddetalle+"'>"+
					"<td>"+dato[i].iddetalle+"</td>"+
					"<td class='details-control2'></td>"+
					"<td style='vertical-align:middle'>"+dato[i].DA+"</td>"+
					"<td  style='vertical-align:middle'>"+dato[i].DU+"</td>"+					
					"<td  data-title='ITEM'>"+dato[i].items+"</td>"+
					"<td data-title='ACTIVIDAD'>"+dato[i].actividad+/*"<span class='help-block' style='font-size:10px'>("+dato[i].ciudad+"-"+dato[i].tpp+")</span>*/"</td>"+
					"<td data-title='UN'>"+dato[i].unidad+"</td>"+
					"<td  data-title='Cantidad'>"+dato[i].cantidad+"</td>"+
					"<td data-title='Valor Unitario($)' class='currency' title='"+dato[i].apu+"'>"+dato[i].apu+"</td>"+
					"<td data-title='Valor Total($)' title='"+dato[i].total+"'><div id='divtotact"+dato[i].iddetalle+"' class='currency'>"+dato[i].total+"</div></td>"+			
					"<td data-title='CODIGO'>"+dato[i].idactividad+"</td>"+	
					"<td data-title='CODIGO'>"+dato[i].suba+"</td>"+	
					
					"</tr>";
		             
					}
				}
				table1+="</tbody></table>"+
				"</div>";
				
				
				row.child(table1).show();
				convertiract(pre,d,idc);
				setTimeout(autoTabIndex("#tbactividadp"+pre+"g"+d+"c"+idc,10),1000);
				setTimeout(INICIALIZARLISTAS(''),1000);				
				
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
         }
    });
	
}
// JavaScript Document
function convertiract(pre,g,c)
{
   var table = $("#tbactividadp"+pre+"g"+g+"c"+c).DataTable(
   {   
       "columnDefs": 
		 [ 
		 	{ "orderable" : true, "className" : 'dt-center', "targets": [4], },           
			{ "targets": [7 ], "className" : "dt-center" },
			{ "targets": [8 ], "className" : "dt-right" },
			{ "targets": [9 ], "className" : "dt-right" } ,
			//{ "targets": [4], "visible": true,"className": "dt-center"} ,
			{ "targets": [10], "visible": false,"className": "dt-center" },
			{ "targets":[0],"visible":false},
			{ "targets":[1],"orderable":false},
			{ "targets":[2],"orderable":false,"visible":false},
			{ "targets":[3],"orderable":false,"visible":false}   
         ],
		 "searching":false,
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"paging":false,
		/*rowReorder: {
			selector: 'td:nth-child(2)'
			},*/
		fnDrawCallback: function() {
			$("#tbactividadp"+pre+"g"+g+"c"+c+" thead").remove();
		}
    } );
	var table = $("#tbactividadp"+pre+"g"+g+"c"+c).DataTable();
	//funci
	
	 $('#tbactividadp'+pre+'g'+g+'c'+c+' tbody').on('click', 'td.details-control2', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var ida = dat[10];
		var suba = dat[11];
		var fontsize = localStorage.getItem('fontSize');
        
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown2');
        }
        else 
		{
            // Open this row
			var table2 = "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+ida+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDPRESUPUESTO('VERAPU','"+pre+"','"+g+"','"+c+"','"+ida+"','') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>";
			if(suba==1)
		   {
			table2+= "<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+ida+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>";
		   }
			table2+="</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+ida+"' ><div><table style='font-size:"+fontsize+"px'  class='table table-condensed table-striped compact' id='tbinsumos"+ida+"'>";
			table2+="<thead><tr>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"<th class='dt-head-center'>DESCRIPCION</th>"+
			"<th class='dt-head-center'>UND</th>"+
			"<th class='dt-head-center'>REND</th>"+
			"<th class='dt-head-center'>V/UNITARIO</th>"+
			"<th class='dt-head-center'>MATERIAL</th>"+
			"<th class='dt-head-center'>EQUIPO</th>"+
			"<th class='dt-head-center'>M.DE.O</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAINSUMOS",id:ida,pre:pre,gru:g,cap:c},
			function(dato)
			{
		       
				for(var i=0; i<dato.length; i++)
				{
					table2+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'> $"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right'> $"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td></tr>";
				}
				 
			table2+="</tbody></table></div></div></div>";
			row.child(table2).show();
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown2');
        }
    });
	var currCell = $('#tbactividadp'+pre+'g'+g+'c'+c+' tbody td').first();
	var editing = false;
	
	// User clicks on a cell
	$('#tbactividadp'+pre+'g'+g+'c'+c+' td').click(function() {
	currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbactividadp'+pre+'g'+g+'c'+c+' tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell.closest('tr').prev().find('td:eq(' + 
		  currCell.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell.closest('tr').next().find('td:eq(' + 
		  currCell.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell = c;
		currCell.focus();
		currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
	}
	});
}



function convertirsub(id)
{
    var table = $('#tbsubanalisis'+id).DataTable({
		"columnDefs": 
		 [ 
			{ "targets": [2], "className": "dt-left" },
			{ "targets": [3], "className": "dt-center" },
			{ "targets": [4], "className": "dt-center" },
			{ "targets": [5], "className": "dt-right" },
			
			{ "targets": [8], "visible": false },  
			{ "targets": [9], "visible": false },          
         ],
		"searching": false,
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,	
		"ajax": {
                    "url": "modulos/actividades/subanalisis_actividades.php",
                    "data": {id:id}
				},
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},				
			{ "data": "Codigo" },
			{ "data": "Nombre" },			
			{ "data": "Unidad" },
			{ "data": "Rendimiento" },
			{ "data": "Total",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data": "Tipo_Proyecto" },
			{ "data": "Ciudad" },
			{ "data": "Estado","className": "dt-center", "orderable": false,
			  "render": function ( data, type, full, meta ) {
					 if(data=="Inactivo")
					 {
					return '<span class="label label-warning ">'+data+'</span>';
					 }
					 else if(data=="Por Aprobar")
					 {
					 return '<span class="label label-info">'+data+'</span>';						 
					 }
					 else
					 {
					return '<span class="label label-success">'+data+'</span>'; 
					 }
   				 }
			},
			{ "data": "Analisis", "className": "dt-center", "orderable": false,
			   "render": function ( data, type, full, meta ) {
					 if(data=="No")
					 {
					return '<span class="label label-warning">No</span>';
					 }
					 else
					 {
					return '<span class="label label-success">Si</span>'; 
					 }
   				 } 
		    }
		],
		"order": [[2, 'asc']]		
    } );
     
	
	$('table #tbsubanalisis'+id+' thead tr th').each(function(index,element){
	index += 1;
	$('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
    // DataTable
    var tables= $('#tbsubanalisis'+id).DataTable();
 
    // Apply the search
   /* tables.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
*/
	 var detailRows1 = [];
 
    $('#tbsubanalisis'+id+' tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1s= "<div class='nav-tabs-custom' style='display:none'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDACTIVIDADES('VERAPU','"+id+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table1s+="<thead><tr><th>CODIGO</th><th>DESCRIPCION</th><th>UND</th><th>REND</th><th>V/UNITARIO</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table1s+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1s+="</tbody></table></div></div></div>";
				row.child(table1s).show();
				convertirdata(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
		tables.on( 'draw', function () {
			$.each( detailRows1, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
 

}