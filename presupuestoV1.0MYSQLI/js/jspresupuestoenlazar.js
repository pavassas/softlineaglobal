
// JavaScript Document
$(document).ready(function(e) {
    
	var selected = [];	
	var pre = $('#presupuesto').val();
    var gru = $('#gruenlazar').val();
	var cap = $('#capenlazar').val();
    var table = $('#tbpresupuestoenlazarp'+pre+'g'+gru+'c'+cap).DataTable( { 
	      
         "dom": '<"top"pif>rt<"bottom"l><"clear">',
		"columnDefs": 
		 [ 
		   { "targets": [ 0 ], "visible": false },
		   //{ "targets": [ 1 ], "visible": false },
		   { "targets": [ 2,14 ], "visible": false },
		   { "targets": [ 7 ], "className": "dt-center" },
		   { "targets": [ 8 ], "className": "dt-center" },
		   { "targets": [ 9 ], "className": "dt-center" },
		   { "targets": [ 10 ], "className": "dt-center" },
		   { "targets": [ 11 ], "className": "dt-right" }       
         ],	
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[18,30 ], [18,30 ]],
		"language": {
		"lengthMenu": "",
		"zeroRecords": "",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"sProcessing":     "Procesando...",
		"sLoadingRecords": "Cargando...",
		"oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    		}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/presupuestoenlazarjson.php",
                    "data": {pre:pre,gru:gru,cap:cap}
				},
			"columns": [
			{
			"class":          "details-control",
			"orderable":      false,
			"data":           null,
			"defaultContent": ""
			},
			{
				
				"orderable":      false,
				"data":           "Presupuesto",
				"render": function ( data, type, full, meta ) { 
      					return "<a class='btn btn-block btn-default btn-xs' style='cursor:pointer; width:20px;height:20px;' onclick=CRUDPRESUPUESTO('ENLAZAR','"+data+"','','','','') title='Enlazar Presupuesto'><i class='glyphicon glyphicon-link'></i></a>";
   				 }
			},	
			{
				
				"orderable":      false,
				"data":           "Presupuesto",
				"render": function ( data, type, full, meta ) { 
      					return "<a class='btn btn-block btn-danger btn-xs' style='cursor:pointer;width:20px;height:20px' onclick=CRUDPRESUPUESTO('ELIMINAR','"+data+"','','','','') title='Eliminar Presupuesto'><i class='glyphicon glyphicon-trash'></i></a>";
   				 }
			},	
			{ "data" : "Nombre", "className": "dt-left"},
			{ "data" : "Creacion", "className": "dt-center"},
			{ "data" : "Persona", "className": "dt-left"},
			{ "data" : "Tipop", "className": "dt-center"},
			{ "data" : "Estadop", "className": "dt-left"},
			{ "data" : "Adm", "className": "dt-center" },
			{ "data" : "Imp", "className": "dt-center" },
			{ "data" : "Uti" , "className": "dt-center"},
			{ "data" : "Iva", "className": "dt-center" },			
			{ "data" : "Total", "className": "dt-right" },	
			{ "data" : "TotalA", "className": "dt-right" },	
			{
				
				"orderable":      false,
				"data":           "Presupuesto",
				"render": function ( data, type, full, meta ) { 
      					return "<a class='btn btn-block btn-default btn-xs' style='cursor:pointer; width:20px;height:20px' href='modulos/presupuesto/presupuestoexportar.php?edi="+data+"' target='_blank' title='Exportar Presupuesto'><i class='glyphicon glyphicon-save-file'></i></a>";
   				 }
			}
				
		],
		"order": [[3, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) 
			{
                $(row).addClass('selected');
				
            }	
        }
		
    } );
    
 
  // Add event listener for opening and closing details
   var detailRows = [];
	
	table.on( 'draw', function () {
			$.each( detailRows, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
	
	/* $("#enlazar" + pre + "g" + gru + "c" + cap).keyup(function(e) {
        //console.log("Por aca");
        if (e.keyCode == 13) {
            $("#tbpresupuestoenlazar" ).DataTable().search($(this).val(), '', '').draw();
        }
    }).change(function() {
        $("#tbpresupuestoenlazar").DataTable().search($(this).val(), '', '').draw();
    });
	*/
	// Apply the search
   
      //$('.dataTables_wrapper').css('height','500px');
});
function convertirdata4(d)
{
   var table = $('#tbgrupos'+d).DataTable(
   {   
        
         "columnDefs": 
		 [ 
		   { "targets": [ 4 ], "className": "dt-center" },
		   { "targets": [ 5 ], "className": "dt-right" },
		   { "targets": [ 6 ], "visible": false},
       
         ],
		
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Grupos ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}		
		},
		"order": [[2, 'asc']]
    } );
	
	 $('#tbgrupos'+d+' tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var pre = d;
		var id = dat[6];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
			var table1 = "<div id='divchil"+id+"'  class='col-xs-12'><table  class='table table-condensed table-striped' id='tbcapitulo"+id+"'>";
			table1+="<thead><tr>"+
			"<th class='dt-head-center'></th>"+
			"<th class='dt-head-center'>CAP</th>"+
			"<th class='dt-head-center'>DESCRIPCION</th>"+
			"<th class='dt-head-center'>UN</th>"+
			"<th class='dt-head-center'>CANT</th>"+
			"<th class='dt-head-center'>VR.UNIT</th>"+
			"<th class='dt-head-center'>VR.TOTAL</th><th></th>"+
			
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTACAPITULOS",gru:id,pre:pre},//CREAR FUNCION
			function(dato)
			{				
			    var res = dato[0].res;
				if(res=="no"){}else
				{				
					for(var i=0; i<dato.length; i++)
					{
						table1+="<tr style='background-color:rgba(196,215,155,1.00)'><td class='details-control'></td>"+
						"<td data-title='CODIGO'>"+dato[i].codc+"</td>"+
						"<td data-title='DESCRIPCION'>"+dato[i].nombre+"</td>"+
						"<td data-title='UN'></td>"+
						"<td data-title='CANT'></td>"+
						"<td data-title='VR.UNIT'></td>"+
						"<td data-title='VR.TOTAL'>$"+dato[i].total+"</td>"+
						"<td data-title='CAP'>"+dato[i].capitulo+"</td>"+
						"</tr>";
						
						
					}
				}
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata(id,pre);
				},"json");
				//row.child(format(row.data()) ).show();
				tr.addClass('shown');
			}
    });
}

function convertirdata(d,pre)
{
   var table = $('#tbcapitulo'+d).DataTable(
   {   
         "columnDefs": 
		 [ 
		   { "targets": [ 3 ], "className": "dt-center" },
		   { "targets": [ 4 ], "className": "dt-right" },
		   { "targets": [ 5 ], "className": "dt-right"},
           { "targets": [ 6 ], "visible": false },
       
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Capitulos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false
		
    } );
	
	  $('#tbcapitulo'+d+' tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var cap = dat[7];
		var fontsize = localStorage.getItem('fontSize');
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
		var table1 = "<div id='divchila"+pre+"g"+d+"c"+cap+"'  class='col-xs-12'><table  class='table table-condensed compact' id='tbactividadpr"+pre+"g"+d+"c"+cap+"' style='font-size:"+fontsize+"px'>";
		table1+="<thead><tr>"+
		"<th class='dt-head-center'></th>"+
		"<th class='dt-head-center'>ITEM</th>"+
		"<th class='dt-head-center'>ACTIVIDAD</th>"+
		"<th class='dt-head-center'>UND</th>"+
		"<th class='dt-head-center'>CANT</th>"+
		"<th class='dt-head-center'>VR.UNIT</th>"+
		"<th class='dt-head-center'>VR.TOTAL</th>"+
		"<th class='dt-head-center'>CODIGO</th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAACTIVIDADES",cap:cap,pre:pre,gru:d},//CREAR FUNCION
			function(dato)
			{
				var res = dato[0].res;
				if(res=="no"){}
				else
				{
					for(var i=0; i<dato.length; i++)
					{
					   var k = i+1;
					   if(k<10){ k = "0"+k}
					   var f = cap +"."+k;

					table1+="<tr>"+
					"<td class='details-control'></td>"+				
					"<td data-title='ITEM'>"+f+"</td>"+
					"<td data-title='ACTIVIDAD'>"+dato[i].actividad+"<span class='help-block' style='font-size:10px'>("+dato[i].ciudad+"-"+dato[i].tpp+")</span></td>"+
					"<td data-title='UND'>"+dato[i].unidad+"</td>"+
					"<td data-title='CANT'>"+dato[i].cantidad+"</td>"+
					"<td data-title='VR.TOTAL'>$"+dato[i].apu+"</td>"+
					"<td data-title='VR.TOTAL'>$"+dato[i].total+"</td>"+
					"<td data-title='CODIGO'>"+dato[i].idactividad+"</td>"+
					"</tr>";
		
					}
				}
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata3(pre,d,cap);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
         }
    } );
}

function convertirdata3(pre,d,cap)
{
   var table = $("#tbactividadpr"+pre+"g"+d+"c"+cap).DataTable(
   {   
        
         "columnDefs": 
		 [ 
		   { "targets": [ 1 ], "visible": true,"className": "dt-center" },
		   { "targets": [ 5 ], "className": "dt-center" },
		   { "targets": [ 6 ], "className": "dt-right" },
		   { "targets": [ 7 ], "className": "dt-right"},
		   { "targets": [ 8 ], "visible":false},
       
         ],
		"searching":false,
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"paging":false,		
		fnDrawCallback: function() {
			$("#tbactividadpr"+pre+"g"+d+"c"+cap+" thead").remove();
		}
    } );
	
	 $("#tbactividadpr"+pre+"g"+d+"c"+cap+" tbody").on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat[8];
		var fontsize = localStorage.getItem('fontSize');
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
			var table2 = "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDPRESUPUESTO('VERAPU','"+pre+"','"+d+"','"+cap+"','"+id+"','') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' ><div><table style='font-size:"+fontsize+"px'  class='table table-condensed table-striped compact' id='tbinsumos"+id+"'>";
			table2+="<thead><tr>"+
			"<th class='dt-head-center'>RECURSO</th>"+
			"<th class='dt-head-center'>UND</th>"+
			"<th class='dt-head-center'>REND</th>"+
			"<th class='dt-head-center'>V/UNITARIO</th>"+
			"<th class='dt-head-center'>MATERIAL</th>"+
			"<th class='dt-head-center'>EQUIPO</th>"+
			"<th class='dt-head-center'>M.DE.O</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAINSUMOS",id:id,pre:pre,gru:d,cap:cap},
			function(dato)
			{
		       
				for(var i=0; i<dato.length; i++)
				{
					table2+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'> $"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right'> $"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td></tr>";
				}
				 
			table2+="</tbody></table>></div></div></div>";
			row.child(table2).show();
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
}



function convertirsub(id)
{
    var table = $('#tbsubanalisis'+id).DataTable({
		
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,	
		"ajax": {
                    "url": "modulos/actividades/subanalisis_actividades.php",
                    "data": {id:id}
				},
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},				
			{ "data": "Codigo" },
			{ "data": "Nombre" },
			{ "data": "Tipo_Proyecto" },
			{ "data": "Unidad","className": "dt-center", },
			{ "data": "Ciudad" },
			{ "data": "Total", "className": "dt-right",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data": "Estado","className": "dt-center", "orderable": false,
			  "render": function ( data, type, full, meta ) {
					 if(data=="Inactivo")
					 {
					return '<span class="label label-warning ">'+data+'</span>';
					 }
					 else if(data=="Por Aprobar")
					 {
					 return '<span class="label label-info">'+data+'</span>';						 
					 }
					 else
					 {
					return '<span class="label label-success">'+data+'</span>'; 
					 }
   				 }
			},
			{ "data": "Analisis", "className": "dt-center", "orderable": false,
			   "render": function ( data, type, full, meta ) {
					 if(data=="No")
					 {
					return '<span class="label label-warning">No</span>';
					 }
					 else
					 {
					return '<span class="label label-success">Si</span>'; 
					 }
   				 } 
		    }
		],
		"order": [[2, 'asc']]		
    } );
     
	
	$('table #tbsubanalisis'+id+' thead tr th').each(function(index,element){
	index += 1;
	$('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
    // DataTable
    var tables= $('#tbsubanalisis'+id).DataTable();
 
    // Apply the search
    tables.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	 var detailRows1 = [];
 
    $('#tbsubanalisis'+id+' tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1s= "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDACTIVIDADES('VERAPU','"+id+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table1s+="<thead><tr><th>Nombre Recurso</th><th>Unidad Medida</th><th>Rendimiento</th><th>Valor Unitario($)</th><th>Material($)</th><th>Equipo($)</th><th>Mano de obra($)</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table1s+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1s+="</tbody></table></div></div></div>";
				row.child(table1s).show();
				convertirdata(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
		tables.on( 'draw', function () {
			$.each( detailRows1, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
 

}