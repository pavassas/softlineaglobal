// JavaScript Document
$(document).ready(function(e) {
    

    var table = $('#tbunidades').DataTable( {       
     
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		
		},
		"processing": true,
        "serverSide": true,
        "ajax": "modulos/unidades/unidadesjson.php",
		"columns": [ 			
			{				
				"orderable":      false,
				"data":           "Editar"				
			},	
			{	
				"orderable":      false,
				"data":           "Eliminar",				
			},			
			{ "data": "Cod", "className":"dt-center" },		
		
			{ "data": "Nombre" },			
			{ "data": "Estado",
			   "render": function ( data, type, full, meta ) {
					 if(data=="Activo")
					 {
						return '<span class="label label-success pull-center">'+data+'</span>';
					 }
					 else
					 {
						return '<span class="label label-info pull-center">'+data+'</span>'; 
					 }
   				 }
			}			
		],
		"order": [[2, 'asc']]		
    } );
     
	  $('#tbunidades tfoot th').each( function () {
        var title = $('#tbunidades thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
 
    // DataTable
    var table = $('#tbunidades').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
    $('#tbunidades tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	// Add event listener for opening and closing details
 
});