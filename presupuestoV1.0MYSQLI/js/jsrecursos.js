/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {
	
	$('.currency').formatCurrency({colorize: true});
	
	$("input").focus(function() {
  //$( this ).parent( "td" ).addClass('focus');
  		var nam = $(this).attr('class');
			
		if(nam=="currency")
		{
			var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			res1 = res1.replace(",","").replace(",","");
			var val = $(this).val()
			$(this).val(res1)
			if($(this).val()<0)
		    {
		      $(this).val('');
		    }
		}
		//this.selectionStart = this.selectionEnd = this.value.length;
});

$( "input" ).focusout(function() {
     var nam = $(this).attr('class');
	 
	if(nam=="currency")
	{
		 if($(this).val()<=0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
		$('.currency').formatCurrency({colorize: true});
	}
			
});


		var idp = $('#presupuesto').val()
		var table2 = $('#tbrecursos').DataTable( {
		"dom": '<"top"i>rt<"bottom"lp><"clear">',       
         "searching":true,
		"paging":true,
		"ordering": false,
		"info": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,100], ["Todos",100 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		fixedHeader: {
            header: true,
            footer: true
        },
		fnDrawCallback: function() {
			var k = 1;
			$("#tbrecursos tbody td").each(function () {				
				$(this).attr('tabindex', k);				
				k++;
			})
		}
    } );
     
	 
	 
 	/*$('#tbfacturas tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );*/

	   var table2 = $('#tbrecursos').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
/*
    // DataTable
    
 
    // Apply the search
   /* table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );*/	
	
	
var currCell = $('#tbrecursos tbody td').first();
var editing = false;

// User clicks on a cell
$('#tbrecursos td').click(function() {
    currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
});
// User navigates table using keyboard
$('#tbrecursos tbody').keydown(function (e) {
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
        e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
        e.preventDefault();
        c = currCell.prev();
    } 
    
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
		//console.log("Estamos en la celda"+c.attr('tabindex'));
		var tab = c.attr('tabindex');
			currCell = c;
			currCell.focus();
			currCell.children('input').focus();		
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
    	}
});

   
 
});

function filterColumn (id, i ) {
    $('#tbrecursos').DataTable().column( i ).search( $('#'+id).val() ).draw();
	//console.log( $('#'+id).val());
}


