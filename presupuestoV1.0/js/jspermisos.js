// JavaScript Document
$(document).ready(function(e) {
    

    var table1 = $('#tbpermisos1').DataTable( {       
		
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,5,10], ['Todos',5,10]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		}
		
    } );
     
	  $('#tbpermisos1 tfoot th').each( function () {
        var title = $('#tbpermisos1 thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
 	$('#tbpermisos1 tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );

    // DataTable
    var table1 = $('#tbpermisos1').DataTable();
 
    // Apply the search
    table1.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
 
 
    $('#asignar').click( function() 
	{
		var idu = $('#idedicion').val();
		//console.log(idu)
        var table = $('#tbpermisos1').DataTable();
		
		var nums = table.rows('.selected').data().length;
		if(nums<=0)
		{
			error('No a seleccionado ninguna ventana a asignar');
			//alert("")
		}
		else
		{
			$("#tbpermisos1 tbody tr").each(function (index) 
			{
				if($(this).hasClass('selected'))
				{					
					var id = this.id
					
					CRUDUSUARIOS('AGREGARPERMISO','',id);
					
				}
			});
			
			setTimeout(CRUDUSUARIOS('CARGARPERMISOS',idu,''),1000);
		
		}
	});
	$('#asignartodos').click( function() 
	{
		CRUDUSUARIOS('AGREGARTODOSP','','');
	})
});