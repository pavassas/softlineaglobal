// JavaScript Document
$(document).ready(function(e) {
	var val = "";
  $( "input" ).focus(function() {
$( this ).parent( "td" ).addClass('focus');
  var form = formato_numero($( this ).val(), 2,'.', '')
 // $( this ).val(form)

});
$( "select" ).focus(function() {
$( this ).parent( "td" ).addClass('focus');
});
$( "input" ).focusout(function() {
$( this ).parent( "td" ).removeClass('focus');
 var form = formato_numero($( this ).val(), 2,',', '.')
 // $( this ).val(form)

});

$( "select" ).focusout(function() {
  $( this ).parent( "td" ).removeClass('focus');			
});  

    var table = $('table.insumos').DataTable( {       
         "columnDefs": 
		 [ 
				
				{ "targets": [2 ], "className": "dt-center" },
				{ "targets": [3], "className": "dt-center" },
				{ "targets": [4 ], "className": "dt-right" },
				{ "targets": [5 ], "visible": false },
				          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Actuales",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		}
		
    } );
     
 
    // DataTable
    var table = $('table.insumos').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  
});