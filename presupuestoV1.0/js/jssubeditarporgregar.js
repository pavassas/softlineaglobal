// JavaScript Document
$(document).ready(function(e) {
	var val = "";
	$( "input" ).focus(function() {
	$( this ).parent( "td" ).addClass('focus');
	var form = formato_numero($( this ).val(), 2,'.', '')	
	});
	$( "select" ).focus(function() {
	$( this ).parent( "td" ).addClass('focus');
	});
	$( "input" ).focusout(function() {
	$( this ).parent( "td" ).removeClass('focus');
	var form = formato_numero($( this ).val(), 2,',', '.')
	});

$( "select" ).focusout(function() {
  $( this ).parent( "td" ).removeClass('focus');			
});  
    var id = $('#idedicion').val();
    var table = $('#tbactividadporagregar').DataTable( {       
         "columnDefs": 
		 [ 
				
				{ "targets": [2 ], "className": "dt-center" },
				{ "targets": [3], "className": "dt-left" },
				{ "targets": [4 ], "className": "dt-center" }				          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Por Asociar",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":true,
		"processing": true,
        "serverSide": true,		
		"ajax": {
                    "url": "modulos/actividades/subeditarporagregarjson.php",
                    "data": {id:id}
				},	
		"columns": [ 			
			{   "orderable":      false,
				"data":           "Agregar"				
			},			
			{ "data": "Nombre" },
			{ "data": "Rendimiento" },
			{ "data": "Tipo" },
			{ "data": "Unidad" },
			{ "data": "Ciudad"}		
		],
		"order": [[1, 'asc'],[3, 'asc']]	
		
    } );
     
 
    // DataTable
    var table = $('#tbactividadporagregar').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
    $('#tbactividadporagregar tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	// Add event listener for opening and closing details
		var currCell = $('#tbactividadporagregar tbody td').first();
	var editing = false;
	
	// User clicks on a cell
	$('#tbactividadporagregar td').click(function() {
	currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbactividadporagregar tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell.closest('tr').prev().find('td:eq(' + 
		  currCell.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell.closest('tr').next().find('td:eq(' + 
		  currCell.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell = c;
		currCell.focus();
		currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
	}
	});
 
});