// JavaScript Document
$(document).ready(function(e) {
 $('.currency').formatCurrency({colorize: true});
	
	$( "input,textarea" ).focus(function() {
  //$( this ).parent( "td" ).addClass('focus');
  		var nam = $(this).attr('class');
			
		if(nam=="currency")
		{
			var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			res1 = res1.replace(",","").replace(",","");
			var val = $(this).val()
			$(this).val(res1)
			if($(this).val()<=0)
		    {
		      $(this).val('');
		    }
		}
		//this.selectionStart = this.selectionEnd = this.value.length;
});

$( "input, textarea" ).focusout(function() {
     var nam = $(this).attr('class');
	 
	if(nam=="currency")
	{
		 if($(this).val()<=0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
		$('.currency').formatCurrency({colorize: true});
	}
			
});
  
  
var currCell = $('#tableinforme tbody td').first();
var editing = false;

// User clicks on a cell
$('#tableinforme tbody td').click(function() {
    currCell = $(this);
	//currCell.toggleClass("editing");
	currCell.focus();
	currCell.children('input').focus();
	currCell.children('textarea').focus();
});

// User navigates table using keyboard
$('#tableinforme tbody').keydown(function (e) {
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    }  else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
        e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
        e.preventDefault();
        c = currCell.prev();
    } 
    
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
        currCell = c;
        currCell.focus();
		currCell.children('input').focus();
		currCell.children('textarea').focus();
    }
});

 
// User can cancel edit by pressing escape
$('#edit').keydown(function (e) {
    if (editing && e.which == 27) { 
        editing = false;
        $('#edit').hide();
        currCell.toggleClass("editing");
        currCell.focus();
		currCell.children('input').focus();
		currCell.children('textarea').focus();
    }
}); 

  $('.GUARDARGENERAL').change(function(){
	 var id = $(this).attr('name');
  }) 
	
});

function VERIFICAR(o,id)
{
	 var pre = $('#presupuesto').val();
	 var des = $('#descripcion'+id).val();
	 var plan = $('#plan'+id).val(); var plan1 = plan.replace("$", "");
	 var plan2 = plan1.replace(",","").replace(",","").replace(",","").replace(",",""); plan = plan2
	 var real = $('#real'+id).val(); var real1 = real.replace("$", "");
	 var real2 = real1.replace(",","").replace(",","").replace(",","").replace(",",""); real = real2
	 var comp = $('#comprometido'+id).val(); var comp1 = comp.replace("$", "");
	 var comp2 = comp1.replace(",","").replace(",","").replace(",","").replace(",",""); comp = comp2
	 
	 $.post('funciones/fnPresupuestoEjecutivo.php',{opcion:o,id:id,pre:pre,des:des,plan:plan,real:real,comp:comp},function(data)
	 {
		 var res = $.trim(data[0].res);
		 if(res=="ok" && o=="GUARDARGENERAL")
		 {
			 //FUNCION
			 $('#descripcion'+id).attr('title','ACTUALIZARGENERAL');
			 $('#plan'+id).attr('title','ACTUALIZARGENERAL');
			 $('#real'+id).attr('title','ACTUALIZARGENERAL');
			 $('#comprometido'+id).attr('title','ACTUALIZARGENERAL');
			 //NAME
			 $('#descripcion'+id).attr('name',data[0].nid);
			 $('#plan'+id).attr('name',data[0].nid);
			 $('#real'+id).attr('name',data[0].nid);
			 $('#comprometido'+id).attr('name',data[0].nid);
			 $('#validado'+id).attr('title',data[0].nid);
			 $('#actualizado'+id).attr('title',data[0].nid);
			 
			 $('#asignado'+id).html(data[0].asignado);
			 $('#disponible'+id).html(data[0].disponible);	
			 //ID
			 $('#descripcion'+id).attr('id','descripcion'+data[0].nid);
			 $('#plan'+id).attr('id','plan'+data[0].nid);
			 $('#real'+id).attr('id','real'+data[0].nid);
			 $('#comprometido'+id).attr('id','comprometido'+data[0].nid);
			 $('#asignado'+id).attr('id','asignado'+data[0].nid);
			 $('#disponible'+id).attr('id','disponible'+data[0].nid);
			 $('#validado'+id).attr('id','validado'+data[0].nid).attr('data-toggle','modal').attr('data-target','#myModal');
			 $('#actualizado'+id).attr('id','actualizado'+data[0].nid).attr('data-toggle','modal').attr('data-target','#myModal');
			 $('#desviacion'+id).attr('id','desviacion'+data[0].nid);
			 $('#eje_'+id).attr('id','eje_'+data[0].nid).addClass('context1');
			 
			 //TOTALES
			 $('#tplan').html(data[0].top);
			 $('#treal').html(data[0].tor);
			 $('#tcomprometido').html(data[0].toc);
			 $('#tasignado').html(data[0].toa);
			 $('#tdisponible').html(data[0].tod);
			 
			 //NUEVA FILA
			 $('#tableinforme tbody tr:last').after('<tr id="eje_0">'+
			 '<td><input type="text" title="GUARDARGENERAL" name="0" id="descripcion0" style="width:100%; border:thin; text-align:left; background-color:transparent; text-transform:uppercase"  onChange="VERIFICAR(this.title,this.name)" placeholder="INGRESAR DESCRIPCIÓN" value="" ></td>'+
			 '<td> <input type="text" name="0" title="GUARDARGENERAL" id="plan0" style="width:100%; border:thin; text-align:right; background-color:transparent" onChange="VERIFICAR(this.title,this.name)" onKeyPress="return NumCheck(event, this)"  placeholder="" value="0" class="currency"/></td>'+
			 '<td><input type="text" name="0" title="GUARDARGENERAL" id="real0" style="width:100%; border:thin; text-align:right; background-color:transparent" onChange="VERIFICAR(this.title,this.name)"  onKeyPress="return NumCheck(event, this)"  placeholder="" value="0" class="currency"></td>'+
			 '<td colspan="2"><input name="0" type="text" title="GUARDARGENERAL" id="comprometido0" style="width:100%; border:thin; text-align:right; background-color:transparent" onChange="VERIFICAR(this.title,this.name)"  onKeyPress="return NumCheck(event, this)" placeholder="" value="0" class="currency"></td>'+
			 '<td><div id="asignado0" class="currency" style="text-align:right">0</div></td>'+
			 '<td><div id="disponible0" class="currency" style="text-align:right">0</div></td>'+
			 "<td><div id='validado0' title='0' onclick=VALORES('SELECCIONVALIDADO',this.name) class='currency' style='text-align:right; cursor:pointer'>0</div></td>"+
			 "<td><div id='actualizado0' title='0' onclick=VALORES('SELECCIONACTUAL',this.name) class='currency' style='text-align:right; cursor:pointer'>0</div></td>"+
			 '<td><div id="desviacion0" class="currency" style="text-align:right">0</div></td></tr>');
			 	
			 setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
			 setTimeout(autoTabIndex('#tableinforme',8),2000);	
			 setTimeout(convertirm(),3000);	
	     }
		 else if(o=="ACTUALIZARGENERAL" && res=="ok")
	     {
		     $('#asignado'+id).html(data[0].asignado);
			 $('#disponible'+id).html(data[0].disponible);	
			 //TOTALES
			 $('#tplan').html(data[0].top);
			 $('#treal').html(data[0].tor);
			 $('#tcomprometido').html(data[0].toc);
			 $('#tasignado').html(data[0].toa);
			 $('#tdisponible').html(data[0].tod);
			 setTimeout("$('.currency').formatCurrency({colorize: true})",1000);		 
		 }	 
		 else
		 {
			 error("Surgio un error. Verificar");
	     }
     },"json"); 
}

function convertirm()
{
	$.contextMenu({
        selector: '.context1', 
        callback: function(key, options) {
			
			var pre = $('#presupuesto').val();
		    var id = $(this).attr('id');
			var del = "_";
			var n = id.split(del);
			var ele = n[0];
			var idi = n[1];
			/*var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idd = dat[0];
			var ida = dat[10];
			*/
			if(key=="delete")
			{
				$.post('funciones/fnPresupuestoEjecutivo.php',{opcion:"DELETEGENERAL",id:idi,pre:pre},
				function(data)
				{
					var res = $.trim(data[0].res);
					if(res=="ok")
					{
						$('#eje_'+idi).remove();
						setTimeout(CALCULOSTOTALES(''),1000);						 
						 setTimeout(autoTabIndex('#tableinforme',8),2000);	
					}
					else
					{
					   error("Surgio un error al eliminar registro. Verificar");
					}
				},"json");
				//setTimeout(CRUDPRESUPUESTO('DELETEINFO',pre,g,c,'',idd),1000);
			}
						
           // window.console && console.log(m) || alert(m); 
        },
        items: {          
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
}

function VALORES(o,id)
{
  	   var pre = $('#presupuesto').val();
	   $('#msn').html('');
	   $('#btnguardar').css('display','none');
	   if(o=="SELECCIONVALIDADO") { $('#myModalLabel').html("SELECCION ITEM TOTAL VALIDADO"); }else
	   {  $('#myModalLabel').html("SELECCION ITEM TOTAL ACTUALIZADO");  }
	   $('#contenido2').html('');
	   $.post('funciones/fnPresupuestoEjecutivo.php',{opcion:o,igp:id,pre:pre},
	   function(data)
	   {
		   $('#contenido2').html(data);
	   })	
}

function SELECCIONARACTIVIDAD(o,igp,gru,cap,act,idd)
{
		var pre = $('#presupuesto').val();	
		var ck = $('input:checkbox[name=ckseleccion'+idd+']:checked').val(); if(ck==1){ck=1;}else{ck=0;}
		if(ck==0)
		{
		 // $('input:checkbox[name=ckselcapitulo'+pre+'g'+gru+'c'+cap+']').removeAttr('checked');
		}
		
		//console.log(ck);
		$.post('funciones/fnPresupuestoEjecutivo.php',{opcion:o,igp:igp,pre:pre,gru:gru,cap:cap,act:act,idd:idd,ck:ck},
		function(data)
		{
			var res = $.trim(data[0].res);
            var cantsel = data[0].totalsele;
            var numve = data[0].numve;
            var totdif = data[0].totdif;
            if(numve==totdif){
                $('input:checkbox[name=ckselcapitulo'+pre+'g'+gru+'c'+cap+']').prop('checked',true);
			}
			else{
                $('input:checkbox[name=ckselcapitulo'+pre+'g'+gru+'c'+cap+']').removeAttr('checked');
			}
			if(res=="ok1")
			{
			  // $('#validado'+igp).html(data[0].total);
			  // $('#actualizado'+igp).html(data[0].totala);
			  // $('#desviacion'+igp).html(data[0].desviacion);
			  // ok("Item seleccionado asignado correctamente");
                $('#sel_' + cap).html(cantsel);
			   setTimeout(CALCULOSTOTALES(igp),1000);
			   //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
			   //ACTUALIZAR TOTALES			  
			}
			else if(res =="ok2")
			{
			   //$('#validado'+igp).html(data[0].total);
			   //$('#actualizado'+igp).html(data[0].totala);
			   //$('#desviacion'+igp).html(data[0].desviacion);
			 //  ok("Item desasignado correctamente");
                $('#sel_' + cap).html(cantsel);
			    setTimeout(CALCULOSTOTALES(igp),1000);
			   //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
			   //ACTUALIZAR TOTALES
			}
			if(res=="ok3")
			{
			   //$('#validado'+igp).html(data[0].total);
			   //$('#actualizado'+igp).html(data[0].totala);
			   //$('#desviacion'+igp).html(data[0].desviacion);
			  // ok("Item seleccionado asignado correctamente");
                $('#sel_' + cap).html(cantsel);
			    setTimeout(CALCULOSTOTALES(igp),1000);
			   //setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
			   //ACTUALIZAR TOTALES			  
			}
			else if(res =="ok4")
			{
			  // $('#validado'+igp).html(data[0].total);
			  // $('#actualizado'+igp).html(data[0].totala);
			  // $('#desviacion'+igp).html(data[0].desviacion);			   
			  // ok("Item desasignado correctamente");
                $('#sel_' + cap).html(cantsel);
			    setTimeout(CALCULOSTOTALES(igp),1000);
			  // setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
			   //ACTUALIZAR TOTALES
			}
			else if(res =="error1" )
			{
			   error("Surgio un error al asignar item seleccionado. Verificar");
			}
			else if(res=="error2")
			{
			   error("Surgio un error al desasignar item. Verificar")
		    }
			else if(res =="error3" )
			{
			   error("Surgio un error al asignar item seleccionado. Verificar");
			}
			else if(res=="error4")
			{
			   error("Surgio un error al desasignar item. Verificar")
		    }
		},"json");
}

function CALCULOSTOTALES(igp)
{
	var pre = $('#presupuesto').val();
	$.post('funciones/fnPresupuestoEjecutivo.php',{opcion:"CALCULOTOTALES",pre:pre,igp:igp},
	function(data)
	{
		$('#tplan').html(data[0].top);
		$('#treal').html(data[0].tor);
		$('#tcomprometido').html(data[0].toc);
		$('#tasignado').html(data[0].toa);
		$('#tdisponible').html(data[0].tod);
		$('#tvalidado').html(data[0].tova);
		$('#tactualizado').html(data[0].toac);
		$('#tdesviacion').html(data[0].tode);
		if(igp>0)
		{
			$('#validado'+igp).html(data[0].validado);
			$('#actualizado'+igp).html(data[0].actualizado);
			$('#desviacion'+igp).html(data[0].desviacion);
		}
		setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
		
	},"json");
}

function SELECCIONARCAPITULO(o,igp,pre,gru,cap)
{
	    var ck = $('input:checkbox[name=ckselcapitulo'+pre+'g'+gru+'c'+cap+']:checked').val(); if(ck==1){ck=1;}else{ck=0;}
		$.post('funciones/fnPresupuestoEjecutivo.php',{opcion:o,pre:pre,gru:gru,cap:cap,igp:igp,ck:ck},
		function(data)
		{
			var res = $.trim(data[0].res);
			var cantsel = data[0].totalsele;
            var numve = data[0].numve;
            var totdif = data[0].totdif;
            if(numve==totdif){
                $('input:checkbox[name=ckselcapitulo'+pre+'g'+gru+'c'+cap+']').prop('checked',true);
            }
            else{
                $('input:checkbox[name=ckselcapitulo'+pre+'g'+gru+'c'+cap+']').removeAttr('checked');
            }
			if(res=="ok1")
			{
			   // $('#validado'+igp).html(data[0].total);
			  // $('#actualizado'+igp).html(data[0].totala);
			  // $('#desviacion'+igp).html(data[0].desviacion);
				//ok("Items del capitulos seleccionado asignados correctamente");
				 setTimeout(CALCULOSTOTALES(igp),1000);
                 $('#sel_' + cap).html(cantsel);
				if(o=="SELECCIONCAPINICIAL")
				{

					$("#tbactividadinf"+pre+"g"+gru+"c"+cap+" tbody tr").each(function (index) 
					{						
						$('td', this).eq(0).children("input[type=checkbox]").prop('checked',true);
						
					});
				}
			}
			else if(res=="ok2")
			{
                $('#sel_' + cap).html(cantsel);
				//$('#validado'+igp).html(data[0].total);
			   //$('#actualizado'+igp).html(data[0].totala);
			   //$('#desviacion'+igp).html(data[0].desviacion);
			   // ok("Item del capitulo desasignados correctamente");
				 setTimeout(CALCULOSTOTALES(igp),1000);
				$("#tbactividadinf"+pre+"g"+gru+"c"+cap+" tbody tr").each(function (index) 
					{						
						$('td', this).eq(0).children("input[type=checkbox]").removeAttr('checked');						
					});
			}
			else
			if(res=="ok3")
			{
                $('#sel_' + cap).html(cantsel);
			   //$('#validado'+igp).html(data[0].total);
			   //$('#actualizado'+igp).html(data[0].totala);
			  // $('#desviacion'+igp).html(data[0].desviacion);
				//ok("Items del capitulo seleccionado asignados correctamente");
				setTimeout(CALCULOSTOTALES(igp),1000);
				if(o=="SELECCIONCAPACTUAL")
				{
					$("#tbactividadinfa"+pre+"g"+gru+"c"+cap+" tbody tr").each(function (index) 
					{						
						$('td', this).eq(0).children("input[type=checkbox]").prop('checked',true);
						
					});
				}
			}
			else if(res=="ok4")
			{
                $('#sel_' + cap).html(cantsel);
			   //$('#validado'+igp).html(data[0].total);
			   //$('#actualizado'+igp).html(data[0].totala);
			   //$('#desviacion'+igp).html(data[0].desviacion);
			    //ok("Item del capitulo desasignados correctamente");
				 setTimeout(CALCULOSTOTALES(igp),1000);
				$("#tbactividadinfa"+pre+"g"+gru+"c"+cap+" tbody tr").each(function (index) 
					{						
						$('td', this).eq(0).children("input[type=checkbox]").removeAttr('checked');						
					});
			}
			else if(res =="error1" )
			{
			   error("Surgio un error al asignar items del capitulo seleccionado. Verificar");
			}
			else if(res=="error2")
			{
			   error("Surgio un error al desasignar items. Verificar")
		    }
			else if(res =="error3" )
			{
			   error("Surgio un error al asignar items del capitulo seleccionado. Verificar");
			}
			else if(res=="error4")
			{
			   error("Surgio un error al desasignar items. Verificar")
		    }
		    else if(res=="error5"){
            	error("ERROR BD. No hay codigo de item general del proyecto seleccionado")
			}
		},"json");
}


function SELECCIONARAIU(o,pre,igp)
{
	 var adm = $('input:checkbox[name=ckadm]:checked').val(); if(adm==1){adm=1;}else{adm=0;}
	 var imp = $('input:checkbox[name=ckimp]:checked').val(); if(imp==1){imp=1;}else{imp=0;}
	 var uti = $('input:checkbox[name=ckuti]:checked').val(); if(uti==1){uti=1;}else{uti=0;}
	 var iva = $('input:checkbox[name=ckiva]:checked').val(); if(iva==1){iva=1;}else{iva=0;}
	 $.post('funciones/fnPresupuestoEjecutivo.php',{opcion:o,pre:pre,igp:igp,adm:adm,imp:imp,iva:iva,uti:uti},
	 function(data)
	 {
		 var res = $.trim(data[0].res);
		 if(res=="ok1")
		 {
			// ok("Cambios guardados");
			 setTimeout(CALCULOSTOTALES(igp),1000);
		 }
		 else if(res=="ok2")
		 {
			 //ok("Cambios guardados");
			 setTimeout(CALCULOSTOTALES(igp),1000);
		 }
		 if(res=="error1")
		 {
			 error("Surgio un error al guardar cambios")
		 }
		 else if(res=="error2")
		 {
			 error("Surgio un error al guardar cambios")
		 }	 
		
     },"json");
	
}