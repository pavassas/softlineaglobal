// JavaScript Document
$(document).ready(function(e) {
    
  var pre = $('#idedicion').val();
  var gru = $('#idgru').val();
  var cap = $('#idca').val();
  var table2 = $('#tbinsumosporagregar').DataTable( {       
         "columnDefs": 
		 [ 				
			{ "targets": [3], "className": "dt-center" },
			{ "targets": [4], "className": "dt-center" },
			{ "targets": [5], "className": "dt-right" },
			{"targets":[1],"visible":false}
			       
         ],
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[5,10], [5,10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Por Asociar",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":true,
		"processing": true,
        "serverSide": true,		
        "ajax":{
			"url":"modulos/actividades/insumosporagregarpresupuestojson.php",
			"data":{pre:pre,gru:gru,cap:cap}
		},		
		"columns": [ 
			
			{   "orderable":      false,
				"data":           "Agregar"
				
			},						
			{ "data": "Tipo" },
			{ "data": "Nombre" },
			{ "data": "Unidad" },
			{ "data": "Rendimiento"},
			{ "data": "Valor" }			
			
		],
		"order": [[1, 'asc']]	
		
    } );
     
 
    // DataTable
    var table2 = $('#tbinsumosporagregar').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	
	// Add event listener for opening and closing details
	var currCell1 = $('#tbinsumosporagregar tbody td').first();
	var editing = false;
	
	// User clicks on a cell
	$('#tbinsumosporagregar td').click(function() {
	currCell1 = $(this);
	currCell1.toggleClass("editing");
	//currCell.focus();
	currCell1.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
	});
	// User navigates table using keyboard
	$('#tbinsumosporagregar tbody').keydown(function (e) {
	var c = "";
	if (e.which == 39) {
		// Right Arrow
		c = currCell1.next();
	} else if (e.which == 37) { 
		// Left Arrow
		c = currCell1.prev();
	} else if (e.which == 38) { 
		// Up Arrow
		c = currCell1.closest('tr').prev().find('td:eq(' + 
		  currCell1.index() + ')');
	} else if (e.which == 40) { 
		// Down Arrow
		c = currCell1.closest('tr').next().find('td:eq(' + 
		  currCell1.index() + ')');
	} 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
		// Tab
		e.preventDefault();
		c = currCell1.next();
	} else if (!editing && (e.which == 9 && e.shiftKey)) { 
		// Shift + Tab
		e.preventDefault();
		c = currCell1.prev();
	} 
	
	// If we didn't hit a boundary, update the current cell
	if (c.length > 0) {
		currCell1 = c;
		currCell1.focus();
		currCell1.children('input').focus();
		}
	});
});