/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {
  //$('.close1').click(function() {
//	$('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible');
//	}); 
		var selected = [];
		var gru  = $('#idgru').val();
	    var pre =  $('#idedicion').val();
		//var tb = $('#tablaseleccionada').val();
		//var tab = $('#'+tb).DataTable();
		//tab.remove();
		
		$('#tablaseleccionada').val('tbcapitulosagregadas'+gru);
		
		
		var table2 = $('#tbcapitulosagregadas'+gru).DataTable( {       
          
		"dom": '<"top"i>rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[4], [4 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"sProcessing":'Buscando Datos...'
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/cap_agregadasjson.php",
                    "data": {gru:gru,pre:pre}
				},
		"columns": [
			{
				
				"orderable":      false,
				"data":           "Codigo",
				"render": function ( data, type, full, meta ) {
      					return "<a class='btn btn-block btn-default btn-xs' onClick=CRUDPRESUPUESTO('ASIGNARCAPITULO','"+pre+"','"+gru+"','"+data+"','','') style='width:20px; height:20px' title='Asignar Actividad'><i class='glyphicon glyphicon-plus'></i></a>";
   				 }
			},			
			{ "data": "Codigo" },
			{ "data": "Nombre" }
			
		],
		"order": [[2, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        },
		scrollY:        '30vh',
        scrollCollapse: true,
        paging:         false
		
    } );
     
	 $('#tbcapitulosagregadas'+gru+' tbody').on('click', 'tr', function () {
        var id = this.id;
		
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	//filtros de busqueda
	
	 /* $('#tbcapitulosagregadas tfoot th').each( function () {
        var title = $('#tbcapitulosagregadas tfoot th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" />' );}
    } );*/
	var table2 = $('#tbcapitulosagregadas'+gru).DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	$('#asignacionescap').click( function() 
	{
		var table = $('#tbcapitulosagregadas'+gru).DataTable();		
		var nums = table.rows('.selected').data().length;
		if(nums<=0)
		{
			error("No ha seleccionado ningun capitulo");		
		return false;
		}
		else{
		
			for(var i = 0; i<selected.length;i++)
			{	
				if(selected[i]=="" || selected[i]==null)
				{
					
				}
				else
				{
					var id = selected[i];
					var n=id.split("row_");			      
					CRUDPRESUPUESTO('ASIGNARCAPITULO',pre,gru,n[1],'','');	
				}
			}
			setTimeout(recalcularcapitulos(pre),1000);
			setTimeout(actualizarcap(pre,gru),2000);		
			
		}
    } ); 
	
	$('body').keyup(function(e) {
       if(e.which===27){$('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible'); } // 13 is the keycode for the Escape key
	});
	
	
});
function filterColumnc (id, i ) {
	var gru  = $('#idgru').val();
    $('#tbcapitulosagregadas'+gru).DataTable().column( i ).search(
        $('#'+id).val()
    ).draw();
}

