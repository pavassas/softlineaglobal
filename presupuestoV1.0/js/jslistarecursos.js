/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {
	
	$('.currency').formatCurrency({colorize: true});
	
	$("input").focus(function() {
  //$( this ).parent( "td" ).addClass('focus');
  		var nam = $(this).attr('class');
			
		if(nam=="currency")
		{
			var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			res1 = res1.replace(",","").replace(",","");
			var val = $(this).val()
			$(this).val(res1)
			if($(this).val()<0)
		    {
		      $(this).val('');
		    }
		}
		//this.selectionStart = this.selectionEnd = this.value.length;
});

$( "input" ).focusout(function() {
     var nam = $(this).attr('class');
	 
	if(nam=="currency")
	{
		 if($(this).val()<=0 || $(this).val()=="")
		 {
			$(this).val(0.00);
		 }
		$('.currency').formatCurrency({colorize: true});
	}
			
});


		var pre = $('#presupuesto').val()
		var table2 = $('#tbrecursos2').DataTable( {
            "columnDefs":
                [
                    { "targets": [0,10,11,12,13,14,15], "visible": false }
                ],
		"dom": '<"top"pl>rt<"bottom"><"clear">',		
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[15,20,30,-1 ], [15,20,30,"Todos" ]],

		//pageResize: true, 
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"decimal": ".",
        "thousands": ","
		},
		"processing": true,
        "serverSide": true,		
        "ajax":{
			"url":"modulos/presupuesto/recursosjson.php",
			"data":{pre:pre}
		},		
		"columns": [
            { "data" : "Creacion" },
			{ "data" : "Codigo" },
			{ "data" : "Nombre" },	
			{ "data" : "Unidad", "className": "dt-center" },	
			{ "data" : "CantP",  "className": "dt-center" },	
			{ "data" : "Valor",  "className": "dt-right"  },	
			{ "data" : "TotalP", "className": "dt-right"  },
			{ "data" : "CantC",  "className": "dt-center" },
			{ "data" : "Valor",  "className": "dt-right"  },
			{ "data" : "TotalC", "className": "dt-right"  },
			{ "data" : "CantA",  "className": "dt-center" },
			{ "data" : "ValorA", "className": "dt-right"  },
			{ "data" : "AdmA", "className": "dt-right"  },
			{ "data" : "ImpA", "className": "dt-right"  },
			{ "data" : "UtiA", "className": "dt-right"  },
			{ "data" : "IvaA", "className": "dt-right"  },
			{ "data" : "TotalA", "className": "dt-right"  },
			{ "data" : "Disponible", "className": "dt-right"  }
		],
		"order": [[0, 'asc'],[2, 'asc']],
		//fixedHeader: true,
		fnDrawCallback: function() {
			var k = 1;
			
			$("#tbrecursos2 tbody td").each(function () {
							
				$(this).attr('tabindex', k);				
				k++;
			})
		},
		 "fnFooterCallback": function ( row, data, start, end, display ) {
             /*var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
					//console.log(intVal(b));
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
					//console.log(b);
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                '$'+pageTotal +' ( $'+ total +' total)'
            );*/
        }
    } );
     
	 
	 
 	/*$('#tbfacturas tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );*/

	   var table2 = $('#tbrecursos2').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	
	
/*
    // DataTable
    
 
    // Apply the search
   /* table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );*/	
	   /*var clonedHeaderRow;
    
       $(".persist-area").each(function() {
           clonedHeaderRow = $(".persist-header", this);
           clonedHeaderRow
             .before(clonedHeaderRow.clone())
             .css("width", clonedHeaderRow.width())
             .addClass("floatingHeader");
             
       });
       
       $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
		
	*/
var currCell = $('#tbrecursos2 tbody td').first();
var editing = false;

// User clicks on a cell
$('#tbrecursos2 td').click(function() {
    currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
});
// User navigates table using keyboard
$('#tbrecursos2 tbody').keydown(function (e) {
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
        e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
        e.preventDefault();
        c = currCell.prev();
    } 
    
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
		//console.log("Estamos en la celda"+c.attr('tabindex'));
		var tab = c.attr('tabindex');
			currCell = c;
			currCell.focus();
			currCell.children('input').focus();		
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
    	}
	});
 
});

function filterColumn (id, i ) {
	
	//console.log("Si entro"+table);
	$('#'+id).keypress(function(e){
	    if(e.which==13)
		{
			$('#tbrecursos2').DataTable().column( i ).search( $('#'+id).val() ).draw();
			
			//$('#'+fil).val('')
		}
	});
	
    

    //$('#tbrecursos2').DataTable().column( i ).search( $('#'+id).val() ).draw();
	//console.log( $('#'+id).val());
}


