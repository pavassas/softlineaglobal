/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
}

function convertirdata(d)
{
   var table = $('#tbinsumos'+d).DataTable(
   {   
        "columnDefs": 
		 [ 
			{ "targets": [2], "className": "dt-center"},
			{ "targets": [3], "className": "dt-center" },
			{ "targets": [4], "className": "dt-right"},
			{ "targets": [5], "className": "dt-right" },
			{ "targets": [6], "className": "dt-right" },  
			{ "targets": [7], "className": "dt-right" },          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,-1 ], [10,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Insumos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false
		 
    } );
}

function filterColumn (id, i ) {
    $('#tbactividades').DataTable().column( i ).search($.trim($('#'+id).val()) ).draw();
	//console.log( $('#'+id).val());
}

// JavaScript Document
$(document).ready(function(e) {
    
    var selected = [];
	var suba = $('#opcanalisis2').val();
    var table = $('#tbactividades').DataTable({
		"dom": '<"top"pl>rt<"bottom"><"clear">',
		 "columnDefs": 
		 [ 	
		    { "targets": [4,1], "visible": false },		
			{ "targets": [11], "visible": false },
			{ "targets": [12], "visible": false }          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[15,20,30,-1 ], [15,20,30,"Todos" ]],

		//pageResize: true, 
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		},
		"processing": true,
        "serverSide": true,		
        "ajax":{
			//"url":"data.json",
			"url":"modulos/actividades/proceso_actividades.php",
			"data":{suba:suba},
			"type": "POST" //Pero en el PHP tendrias que validar que estas recibiendo con $_POST y no $_GET
		},		
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},
			{ "data" : "Exportar", "orderable": false},	
			{ "data" : "Editar", "orderable": false},	
			{ "data" : "Eliminar", "orderable": false},			
			{ "data" : "Codigo" },
			{ "data" : "Nombre" },			
			{ "data" : "Unidad","className": "dt-center", },
			{ "data" : "Total", "className": "dt-right" },
			
			{ "data" : "Ciudad" },
{ "data" : "Tipo_Proyecto" },			
			{ "data" : "Estado","className": "dt-center", "orderable": false,
			  "render": function ( data, type, full, meta ) {
					 if(data=="Inactivo")
					 {
					return '<span class="label label-warning ">'+data+'</span>';
					 }
					 else if(data=="Por Aprobar")
					 {
					 return '<span class="label label-info">'+data+'</span>';						 
					 }
					 else
					 {
					return '<span class="label label-success">'+data+'</span>'; 
					 }
   				 }
			},
			{ "data": "Analisis", "className": "dt-center", "orderable": false,
			   "render": function ( data, type, full, meta ) {
					 if(data=="No")
					 {
					return '<span class="label label-warning">No</span>';
					 }
					 else
					 {
					return '<span class="label label-success">Si</span>'; 
					 }
   				 } 
		    },
			{ "data": "Suba" }
		],
		"order": [[4, 'asc']],
		fixedHeader: true,
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) 
			{
                $(row).addClass('selected');
				
            }
        }	
    } );
     
	 /* $('#tbactividades tfoot th').each( function () {
        var title = $('#tbactividades thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control input-sm" placeholder="'+title+'" /></div>' );}
    } );*/
 $('#tbactividades tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } ); 
	
    // DataTable
    var table = $('#tbactividades').DataTable();
    $('.busqueda').keyup(function(e){
		var col = $(this).attr('data-column');
		if(e.keyCode==13)
		{
			$('#tbactividades').DataTable().column(col).search($.trim($(this).val()) ).draw();
		}
		
		}).change(function(){
		var col = $(this).attr('data-column');
		    $('#tbactividades').DataTable().column(col).search($.trim($(this).val()) ).draw();
		});
	
	 var detailRows = [];
 
    $('#tbactividades tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;
		var suba = dat.Suba		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1 = "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDACTIVIDADES('VERAPU','"+id+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>";
			
			if(suba>0 && suba!=null)
			{
			table1+="<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>";
			}
			table1+="</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-bordered table-condensed compact' id='tbinsumos"+id+"'>";
			table1+="<thead><tr><th>CODIGO</th><th>DESCRIPCIÓN</th><th>UND</th><th>REND</th><th>V/UNITARIO</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table1+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1+="</tbody></table></div></div></div>";
				row.child(table1).show();
				setTimeout(convertirdata(id),1000);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
	table.on( 'draw', function () {
		$.each( detailRows, function ( i, id ) {
		$('#'+id+' td.details-control').trigger( 'click' );
		} );
	} );
	$('#eliminar').click( function() 
	{
		var table = $('#tbactividades').DataTable();
		var col = table.rows('.selected').data().length;
		if(col<=0 ) 
		{
			error('No ha seleccionado ninguna actividad');
		    //$('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4></div>'); return false;
		}
		else
		{
			jConfirm('Realmente desee eliminar las actividades seleccionadas', 'Dialogo de Confirmación LG',function(r) {		    //var conf = confirm("Realmente desee anular los ticket seleccionados");
				if(r==true)
				{
					var er = 0;
					for(var i = 0; i<selected.length;i++)
					{					
						if(selected[i]=="" || selected[i]==null || selected[i]==0)
						{
						}
						else
						{
							var id = selected[i];
							var n=id.split("row_");	
							$.post('funciones/fnActividades.php',{opcion:"ELIMINAR",id:n[1]},
							function(data)
							{
							   if(data==1)
							   {
								   var table = $('#tbactividades').DataTable();
						table.row('#row_'+n[1]).remove().draw(false);	
 
							   }
							   else
							   {
								   er++;
							   }
							});								      	
							
						}
					}
					selected.length = 0;
					
					if(er<=0)
					{
						ok('Actividades Eliminadas Correctamente')
					//$('#msn1').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-check"></i>Alerta!</h4></div>');
					}
					else
					{
						error('Surgio un error al eliminar actividades');
						//$('#msn1').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4> </div>');
					}
					
				}
				else
				{
					return false;
				}
			});
		}
 	} );

	var wrapper = $('#resize_wrapper');
	$('#resize_handle').on( 'mousedown', function (e) {
		var mouseStartY = e.pageY;
		var resizeStartHeight = wrapper.height();
	
		$(document)
			.on( 'mousemove.demo', function (e) {
				var height = resizeStartHeight + (e.pageY - mouseStartY);
				if ( height < 200 ) {
					height = 200;
				}
	
				wrapper.height( height );
			} )
			.on( 'mouseup.demo', function (e) {
				$(document).off( 'mousemove.demo mouseup.demo' );
			} );
	
		return false;
	} );
	
	$('#ExportarApu').on('click',function(e)
	{
		var table = $('#tbactividades').DataTable();
		var col = table.rows('.selected').data().length;
		var selecte1 = $.map(selected,function(elemento,i) { return elemento.replace('row_',''); } );
		if(col<=0)
		{
			error("No ha seleccionado ninguna actividad a exportar. Verificar");
		}
		else
		{
			if(selecte1!="" && selecte1!=null)
			{
				var selecte = selecte1.join('-');
				window.open('modulos/actividades/actividadesexportar.php?edi='+selecte);
				 
			}
		}
		e.stopPropagation();
	});
});

function convertirsub(id)
{

    var table = $('#tbsubanalisis'+id).DataTable({
		
		"columnDefs": 
		 [ 
			{ "targets": [2], "className": "dt-left" },
			{ "targets": [3], "className": "dt-center" },
			{ "targets": [4], "className": "dt-center" },
			{ "targets": [5], "className": "dt-right" },
			{ "targets": [6], "className": "dt-right" },
			
			{ "targets": [9], "visible": false },  
			{ "targets": [10], "visible": false },          
         ],
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false,
		"processing": true,
        "serverSide": true,	
		"ajax": {
                    "url": "modulos/actividades/subanalisis_actividades.php",
                    "data": {id:id}
				},
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},				
			{ "data": "Codigo2" },
			{ "data": "Nombre" },			
			{ "data": "Unidad" },
			{ "data": "Rendimiento" },
			{ "data": "Total",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data": "Totales",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data": "Tipo_Proyecto" },
			{ "data": "Ciudad" },
			
			{ "data": "Estado","className": "dt-center", "orderable": false,
			  "render": function ( data, type, full, meta ) {
					 if(data=="Inactivo")
					 {
					return '<span class="label label-warning ">'+data+'</span>';
					 }
					 else if(data=="Por Aprobar")
					 {
					 return '<span class="label label-info">'+data+'</span>';						 
					 }
					 else
					 {
					return '<span class="label label-success">'+data+'</span>'; 
					 }
   				 }
			},
			{ "data": "Analisis", "className": "dt-center", "orderable": false,
			   "render": function ( data, type, full, meta ) {
					 if(data=="No")
					 {
					return '<span class="label label-warning">No</span>';
					 }
					 else
					 {
					return '<span class="label label-success">Si</span>'; 
					 }
   				 } 
		    }
		],
		"order": [[2, 'asc']]		
    } );
     
	
 
	$('table #tbsubanalisis'+id+' thead tr th').each(function(index,element){
	index += 1;
	$('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
    // DataTable
    var tables= $('#tbsubanalisis'+id).DataTable();
 
    // Apply the search
    tables.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	 var detailRows1 = [];
 
    $('#tbsubanalisis'+id+' tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1s= "<div class='nav-tabs-custom' style='display:none'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDACTIVIDADES('VERAPU','"+id+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li style='display:none'>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table1s+="<thead><tr><th>CODIGO</th><th>DESCRIPCIÓN</th><th>UND</th><th>REND</th><th>V/UNITARIO</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnActividades.php',{opcion:"LISTAINSUMOS",id:id},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table1s+="<tr>"+
					"<td data-title='Insumo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1s+="</tbody></table></div></div></div>";
				row.child(table1s).show();
				convertirdata(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
		tables.on( 'draw', function () {
			$.each( detailRows1, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
 

}


	