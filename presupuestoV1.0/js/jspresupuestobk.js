// JavaScript Document
$(document).ready(function(e) {

    var selected = [];
    var tipo = $('#bustipoproyecto').val();
    var cli = $('#buscliente').val();
    var nom = $('#busnombre').val();
    var fec = $('#busfecha').val();
    var pre = $('#presupuesto').val();

    var table = $('#tbpresupuesto').DataTable({

        "dom": '<"top"pi>rt<"bottom"l><"clear">',
        "columnDefs": [{
                "targets": [0],
                "visible": true
            },
            {
                "targets": [1],
                "visible": true
            },
            {
                "targets": [2, 13],
                "visible": true
            },
            {
                "targets": [7],
                "className": "dt-center"
            },
            {
                "targets": [8],
                "className": "dt-center"
            },
            {
                "targets": [9],
                "className": "dt-center"
            },
            {
                "targets": [10],
                "className": "dt-center"
            },
            {
                "targets": [11],
                "className": "dt-right"
            }
        ],
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "pagingType": "simple_numbers",
        "lengthMenu": [
            [18, 30],
            [18, 30]
        ],
        "language": {
            "lengthMenu": "",
            "zeroRecords": "",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
            "paginate": {
                "previous": "&#9668;",
                "next": "&#9658;"
            },
            "sProcessing": "Procesando...",
            "sLoadingRecords": "Cargando...",
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "modulos/presupuesto/presupuestojsonbk.php",
            "data": {
                tipoproyecto: tipo,
                nombre: nom,
                fec: fec,
                cliente: cli,
                pre: pre
            }
        },
        "columns": [
			{
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {

                "orderable": false,
                "data": "Inf",
                "render": function(data, type, full, meta) {
                    return "<a class='btn btn-block btn-success btn-xs' style='cursor:pointer; width:20px;height:20px;' onclick=CRUDPRESUPUESTO('RESTAURARINFORME','" + data + "','','','','') title='Restaurar Presupuesto'><i class='glyphicon glyphicon-retweet'></i></a>";
                }
            },
            {

                "orderable": false,
                "data": "Inf",
                "render": function(data, type, full, meta) {
                    return "<a class='btn btn-block btn-danger btn-xs' style='cursor:pointer; width:20px;height:20px;' onclick=CRUDPRESUPUESTO('ELIMINARINFORME','','','','','" + data + "') title='Eliminar Informe'><i class='glyphicon glyphicon-trash'></i></a>";
                }
            },
            {
               // "data": "Inf",
                "className": "dt-center",
				"data": null,
                "defaultContent": ""
				
            },
            {
                "data": "FechaInforme",
                "className": "dt-left"
            },
            {
                "data": "Nombre",
                "className": "dt-left"
            },
            {
                "data": "Creacion",
                "className": "dt-center"
            },
            {
                "data": "Persona",
                "className": "dt-left"
            },
            {
                "data": "Tipop",
                "className": "dt-center"
            },
            {
                "data": "Estadop",
                "className": "dt-left"
            },
            {
                "data": "Adm",
                "className": "dt-center"
            },
            {
                "data": "Imp",
                "className": "dt-center"
            },
            {
                "data": "Uti",
                "className": "dt-center"
            },
            {
                "data": "Iva",
                "className": "dt-center"
            },
            {
                "data": "Total",
                "className": "dt-right"
            },
            {

                "orderable": false,
                "data": "Inf",
                "render": function(data, type, full, meta) {
                    return "<a class='btn btn-block btn-default btn-xs' style='cursor:pointer; width:20px;height:20px' href='modulos/presupuesto/presupuestoexportarblo.php?edi=" + data + "' target='_blank' title='Exportar Informe'><i class='glyphicon glyphicon-save-file'></i></a>";
                }
            }

        ],
        "order": [
            [3, 'asc']
        ],
        fixedHeader: true,
        "rowCallback": function(row, data, iDisplayIndex, iDisplayIndexFull) {
            if ($.inArray(data.DT_RowId, selected) !== -1) {
                $(row).addClass('selected');

            }
		$('td:eq(3)', row).html( iDisplayIndex + 1 );
        }

    });
	 var table2 = $('#tbpresupuesto').DataTable(); 
    table.on( 'order.dt search.dt', function () {
        table.column(3, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();


    // Add event listener for opening and closing details
    var detailRows = [];
    $('#tbpresupuesto tbody').on('click', 'td.details-control', function() {

        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var dat = row.data();
        var inf = dat.Inf;
        var pre = dat.Presupuesto;
        //alert(id);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            var table1 = "<div id='divchilg" + inf + "'  class='col-xs-12'><table  class='table table-condensed table-striped' id='tbgrupos" + inf + "'>";
            table1 += "<thead><tr>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'>GRUPO</th>" +
                "<th class='dt-head-center'>UN</th>" +
                "<th class='dt-head-center'>CANT</th>" +
                "<th class='dt-head-center'>VR.UNIT($)</th>" +
                "<th class='dt-head-center'>VR.TOTAL</th>" +
                "<th class='dt-head-center'>CODIGO</th>" +
                "</tr></thead><tbody>";


            $.post('funciones/fnPresupuesto.php', {
                    opcion: "LISTAGRUPOBK",
                    pre: pre,
                    inf: inf
                }, //CREAR FUNCION
                function(data) {

                    var res = data[0].res;
                    //  alert(res);
                    if (res == "no") {} else {
                        for (var i = 0; i < data.length; i++) {
                            table1 += "<tr class='success'><td class='details-control'></td>" +
                                "<td data-title='GRU'>" + data[i].nombre + "</td>" +
                                "<td data-title='UN'></td>" +
                                "<td data-title='CANT'></td>" +
                                "<td data-title='VLR UNIT($)'></td>" +
                                "<td data-title='VLR TOTAL($)'>$" + data[i].total + "</td>" +

                                "<td data-title='CODIGO'>" + data[i].grupo + "</td></tr>";
                        }
                    }
                    table1 += "</tbody></table></div>";
                    row.child(table1).show();
                    convertirdata4(pre, inf);
                }, "json");
            //row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
    // Add event listener for opening and closing details

    $('#tbpresupuesto tbody').on('click', 'tr', function() {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var row = table.row($(this));
            var dat = row.data();
            //var nom = dat.Nombre;
            //$('#presupuestoseleccionado').html("/"+nom).css('color','#6E835D');
            var id = dat.Presupuesto;
            //$('#presupuesto>option[value="'+n[1]+'"]').attr('selected','selected');
            //CRUDPRESUPUESTO('EDITAR',id,'','','','');
            //setTimeout(INICIALIZARLISTAS('PRESUPUESTO'),1);
            // $('#myModal').modal('hide');

        }
    });
    table.on('draw', function() {
        $.each(detailRows, function(i, id) {
            $('#' + id + ' td.details-control').trigger('click');
        });
    });
    $('#tbpresupuesto_wrapper.dataTables_wrapper').css('height', '500px');

    $("#tbpresupuesto tbody").contextMenu({
        selector: 'tr',
        callback: function(key, options) {

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dat = row.data();
            var idp = dat.Presupuesto;

            if (key == "delete") {
                setTimeout(CRUDPRESUPUESTO('ELIMINARINFORME', idp, '', '', '', ''), 1000);
            } else if (key == "restore") {
                var url = "modulos/presupuesto/informeexportar.php?edi=" + idp;
                window.open(url);
            }

            // window.console && || alert(m); 
        },
        items: {
            // "edit": {name: "Edit", icon: "edit"},            	
            "delete": {
                name: "Eliminar",
                icon: "delete"
            },
            "restore": {
                name: "Exportar",
                icon: "restore"
            },
            "sep1": "---------",
            "quit": {
                name: "Cerrar",
                icon: function($element, key, item) {
                    return 'context-menu-icon context-menu-icon-quit';
                }
            }
        }
    });
});

function convertirdata4(pre, inf) {
    var inf = inf;
    var pre = pre;
    //console.log(inf);
    var table = $('#tbgrupos' + inf).DataTable({

        "columnDefs": [{
                "targets": [4],
                "className": "dt-center"
            },
            {
                "targets": [5],
                "className": "dt-right"
            },
            {
                "targets": [6],
                "visible": false
            },
        ],

        "ordering": true,
        "info": true,
        "autoWidth": false,
        "pagingType": "simple_numbers",
        "lengthMenu": [
            [-1, 10],
            ["Todos", 10]
        ],
        "language": {
            "lengthMenu": "Ver _MENU_ registros - Lista de Grupos ",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
            "paginate": {
                "previous": "&#9668;",
                "next": "&#9658;"
            }
        },
        "order": [
            [2, 'asc']
        ]
    });

    $('#tbgrupos' + inf + ' tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var dat = row.data();

        var idg = dat[6];

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            var table1 = "<div id='divchil" + idg + "'  class='col-xs-12'><table  class='table table-condensed table-striped' id='tbcapitulo" + idg + "'>";
            table1 += "<thead><tr>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'>CAP</th>" +
                "<th class='dt-head-center'>DESCRIPCION</th>" +
                "<th class='dt-head-center'>UN</th>" +
                "<th class='dt-head-center'>CANT</th>" +
                "<th class='dt-head-center'>VR.UNIT</th>" +
                "<th class='dt-head-center'>VR.TOTAL</th>" +
                "<th></th>" +
                "</tr></thead><tbody>";

            $.post('funciones/fnPresupuesto.php', {
                    opcion: "LISTACAPITULOSBK",
                    gru: idg,
                    pre: pre,
                    inf: inf
                }, //CREAR FUNCION
                function(dato) {
                    var res = dato[0].res;
                    if (res == "no") {} else {
                        for (var i = 0; i < dato.length; i++) {
                            table1 += "<tr style='background-color:rgba(196,215,155,1.00)'>" +
                                "<td class='details-control'></td>" +
                                "<td data-title='CODIGO'>" + dato[i].codc + "</td>" +
                                "<td data-title='DESCRIPCION'>" + dato[i].nombre + "</td>" +
                                "<td data-title='UN'></td>" +
                                "<td data-title='CANT'></td>" +
                                "<td data-title='VR.UNIT'></td>" +
                                "<td data-title='VR.TOTAL'>$" + dato[i].total + "</td>" +
                                "<td data-title='CAP'>" + dato[i].capitulo + "</td>" +
                                "</tr>";
                        }
                    }
                    table1 += "</tbody></table></div>";
                    row.child(table1).show();
                    convertirdata(idg, pre, inf);
                }, "json");
            //row.child(format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
}

function convertirdata(idg, pre, inf) {
    var idg = idg;
    var pre = pre;
    var inf = inf;
    var table = $('#tbcapitulo' + idg).DataTable({
        "columnDefs": [{
                "targets": [3],
                "className": "dt-center"
            },
            {
                "targets": [4],
                "className": "dt-right"
            },
            {
                "targets": [5],
                "className": "dt-right"
            },
            {
                "targets": [7],
                "visible": false
            },

        ],
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "pagingType": "simple_numbers",
        "lengthMenu": [
            [-1, 10],
            ["Todos", 10]
        ],
        "language": {
            "lengthMenu": "Ver _MENU_ registros - Lista de Capitulos",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
            "paginate": {
                "previous": "&#9668;",
                "next": "&#9658;"
            }
        },
        "paging": false

    });

    $('#tbcapitulo' + idg + ' tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var dat = row.data();
        var cap = dat[7];

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            var table1 = "<div id='divchila" + pre + "g" + idg + "c" + cap + "'  class='col-xs-12'><table  class='table table-condensed compact' id='tbactividadpr" + pre + "g" + idg + "c" + cap + "' style='font-size:11px'>";
            table1 += "<thead><tr>" +
                "<th class='dt-head-center'></th>" +
                "<th class='dt-head-center'>ITEM</th>" +
                "<th class='dt-head-center'>ACTIVIDAD</th>" +
                "<th class='dt-head-center'>UND</th>" +
                "<th class='dt-head-center'>CANT</th>" +
                "<th class='dt-head-center'>VR.UNIT</th>" +
                "<th class='dt-head-center'>VR.TOTAL</th>" +
                "<th class='dt-head-center'>CODIGO</th>" +
				"<th class='dt-head-center'>SUBA</th>"+
				"<th class='dt-head-center'>IDDETALLE</th>"+
                "</tr></thead><tbody>";


            $.post('funciones/fnPresupuesto.php', {
                    opcion: "LISTAACTIVIDADESBK",
                    cap: cap,
                    pre: pre,
                    gru: idg,
                    inf: inf
                }, //CREAR FUNCION
                function(dato) {
                    var res = dato[0].res;
                    if (res == "no") {} else {
                        for (var i = 0; i < dato.length; i++) {
                            table1 += "<tr id='" + dato[i].iddetalle + "'>" +
                                "<td class='details-control'></td>" +
                                "<td data-title='ITEM'>" + dato[i].items + "</td>" +
                                "<td data-title='ACTIVIDAD'>" + dato[i].actividad + "</td>" +
                                "<td data-title='UND'>" + dato[i].unidad + "</td>" +
                                "<td data-title='CANT'>" + dato[i].cantidad + "</td>" +
                                "<td data-title='VR.TOTAL'>$" + dato[i].apu + "</td>" +
                                "<td data-title='VR.TOTAL'>$" + dato[i].total + "</td>" +
                                "<td data-title='CODIGO'>" + dato[i].idactividad + "</td>" +
                                "<td data-title='SUBA'>" + dato[i].suba + "</td>" +
								"<td>"+dato[i].iddetalle+"</td>"+
                                "</tr>";

                        }
                    }
                    table1 += "</tbody></table></div>";
                    row.child(table1).show();
                    convertirdata3(pre, idg, cap, inf);
                }, "json");
            //row.child(format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
}

function convertirdata3(pre, idg, cap, inf) {
    var table = $("#tbactividadpr" + pre + "g" + idg + "c" + cap).DataTable({

        "columnDefs": [{
                "targets": [1],
                "visible": true,
                "className": "dt-center"
            },
            {
                "targets": [5],
                "className": "dt-center"
            },
            {
                "targets": [6],
                "className": "dt-right"
            },
            
            {
                "targets": [7,8,9],
                "visible": false
            },

        ],
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": true,
        "pagingType": "simple_numbers",
        "lengthMenu": [
            [-1, 10],
            ["Todos", 10]
        ],
        "language": {
            "lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
            "zeroRecords": "No se encontraron datos",
            "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron datos",
            "infoFiltered": "",
            "paginate": {
                "previous": "&#9668;",
                "next": "&#9658;"
            }
        },
        "paging": false,
        fnDrawCallback: function() {
            $("#tbactividadpr" + pre + "g" + idg + "c" + cap + " thead").remove();
        }
    });

    $("#tbactividadpr" + pre + "g" + idg + "c" + cap + " tbody").on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var dat = row.data();
        var ida = dat[7];
		var suba = dat[8];
		var idd = dat[9];

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
			var id = tr.attr('id');
            // Open this row
            var table2 = "<div class='nav-tabs-custom'>" +
                "<ul class='nav nav-tabs pull-left' id='nav" + ida + "' title'ASIGNACION DE APU Y SUBANALISIS'>" +
                "<li class='active'>" +
                "<a onclick=CRUDPRESUPUESTO('VERAPUBK','" + pre + "','" + idg + "','" + cap + "','" + ida + "','"+inf+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>" +
                "</li>";
				if(suba>0)
				{
					  table2 +="<li>"+
					"<a onclick=CRUDPRESUPUESTO('VERSUBANALISIS','"+pre+"','"+g+"','"+c+"','"+ida+"','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
					"</li>";
				}
				
                "</li>";
				table2 +="</ul>" +
                "</div><div class='panel panel-default'><div class='panel-body' id='divchil" + ida + "' ><div><table style='font-size:11px'  class='table table-condensed table-striped compact' id='tbinsumos" + ida + "'>";
            	table2 += "<thead><tr>" +
               "<th class='dt-head-center'>CODIGO</th>"+
				"<th class='dt-head-center'>DESCRIPCION</th>"+
				"<th class='dt-head-center'>UND</th>"+
				"<th class='dt-head-center'>REND</th>"+
				"<th class='dt-head-center'>V/UNITARIO</th>"+
				"<th class='dt-head-center'>MATERIAL</th>"+
				"<th class='dt-head-center'>EQUIPO</th>"+
				"<th class='dt-head-center'>M.DE.O</th>"+
                "</tr></thead><tbody>";

            $.post('funciones/fnPresupuesto.php', {
                    opcion: "LISTAINSUMOSBK",
                    id: ida,
                    pre: pre,
                    gru: idg,
                    cap: cap,
					inf:inf
                },
                function(dato) {

                    for (var i = 0; i < dato.length; i++) {
                        table2 += "<tr><td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'> $"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right'> $"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td></tr>";
                    }
                    table2 += "</tbody></table></div></div></div>";
                    row.child(table2).show();
                }, "json");
            tr.addClass('shown');
        }
    });
}
function convertirlistainsumos(pre,gru,cap,act,inf)
{
   var table = $("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act).DataTable(
   {   
		"searching":false,
		"ordering": false,
		"info": false,
		"autoWidth": true,
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		},
		"order": [[1, 'asc']],
        "paging":         false,
		fnDrawCallback: function() {
			var k = 1;
			$("#tbinsumosp"+pre+"g"+gru+"c"+cap+"a"+act+" tbody td").each(function () {
				$(this).attr('tabindex', k);
				k++;
			})
		}
    } );
}



function convertirsub(pre,gru,cap,act) {
	//console.log("Hola4");
    var table = $('#tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable({
		"columnDefs": 
		 [ 
			{ "targets": [2], "className": "dt-left" },
			{ "targets": [3], "className": "dt-center" },
			//{ "targets": [4], "className": "dt-center" },
			{ "targets": [5], "className": "dt-right" },
			//{ "targets": [6], "className": "dt-right" },			
			{ "targets": [4,6,7,8,9,10], "visible": false }  
			      
         ],
		"searching": false,
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,	
		"ajax": {
                    "url": "modulos/presupuesto/subanalisis_actividades.php",
                    "data": {id:act,pre:pre,gru:gru,cap:cap}
				},
		"columns": [ 
			{	"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},				
			{ "data": "Codigo" },
			{ "data": "Nombre" },			
			{ "data": "Unidad" },
			{ "data": "Rendimiento" },
			{ "data": "Total",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data": "Totales",
			  "render": function (data, type, full, meta) {
				return data;
				}
			},
			{ "data":  null,"defaultContent": ""},
			{ "data":  null,"defaultContent": ""},
			{ "data":  null,"defaultContent": ""},
			{ "data":  null,"defaultContent": ""}
		],
		"order": [[2, 'asc']]		
    } );
     
	
	$('table #tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act+' thead tr th').each(function(index,element){
	index += 1;
	$('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
    // DataTable
    var tables= $('#tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable();
 
    // Apply the search
   /* tables.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
*/
	 var detailRows1 = [];
 
    $('#tbsubanalisisp'+pre+'g'+gru+'c'+cap+'a'+act+' tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Codigo;		
       // var idx = $.inArray( tr.attr('id'), detailRows );		
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide(); 
            // Remove from the 'open' array
           // detailRows.splice( idx, 1 );
        }
        else 
		{
			
            // Open this row
			var table1s= "<div class='nav-tabs-custom' style='display:none'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+id+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDACTIVIDADES('VERAPU','"+id+"') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+id+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+id+"' >"+
			"<div id='no-more-tables'><table  class='table table-condensed table-striped' id='tbinsumos"+id+"'>";
			table1s+="<thead><tr><th>CODIGO</th><th>DESCRIPCION</th><th>UND</th><th>REND</th><th>V/UNITARIO</th><th>MATERIAL</th><th>EQUIPO</th><th>M.DE.O</th></tr></thead><tbody>";	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAINSUMOSSUB",id:id,pre:pre,gru:gru,cap:cap,act:act},
			function(dato)
			{
				var APU = 0;
		
				for(var i=0; i<dato.length; i++)
				{
					table1s+="<tr>"+
					"<td data-title='Codigo'>"+dato[i].insu+"</td>"+
					"<td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
				/*"<td data-title='Rendimiento'><div style='cursor:pointer' onDblClick='reemplazarinput("+dato[i].iddetalle+","+dato[i].rendi+","+id+")' id='divdetalle"+dato[i].iddetalle+"'>"+dato[i].rendi+"</div></td>"+
				"<td data-title='Valor Unitario'><div style='cursor:pointer' onDblClick='reemplazarinputv("+dato[i].iddetalle+","+dato[i].valor+","+id+")' id='divdetallev"+dato[i].iddetalle+"' >$"+dato[i].valor1+"</div></td>"+*/
					"<td data-title='Rendimiento'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario'>$"+dato[i].valor1+"</td>"+
					"<td data-title='Material'> <div id='divdetallem"+dato[i].iddetalle+"'>$"+dato[i].material+"</div></td>"+
					"<td data-title='Equipo'><div id='divdetallee"+dato[i].iddetalle+"'>$"+dato[i].equipo+"</div></td>"+
					"<td data-title='Mano de obra'> <div id='divdetallema"+dato[i].iddetalle+"'>$"+dato[i].mano+"</div></td></tr>";
					   APU+= Number(dato[i].total);
				}
			//$('#divapu'+id).html(formato_numero(APU,2,'.',','));
				table1s+="</tbody></table></div></div></div>";
				row.child(table1s).show();
				convertirdata(id);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown');
           // row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            //if ( idx === -1 ) {
               // detailRows.push( tr.attr('id') );
            //}
        }
    } );
	  // On each draw, loop over the `detailRows` array and show any child rows
		tables.on( 'draw', function () {
			$.each( detailRows1, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
 

}
