// JavaScript Document
$(document).ready(function(e) {
	
	$('.currency').formatCurrency({colorize: true});
    var events = $('#events');
	 $("input" ).focus(function() {
		var nam = $(this).attr('class');
		
		if(nam=="currency")
		{
			var str = $(this).val()
			var res = str.replace("$", "");
			var res1 = res.replace(",","").replace(",","");
			res1 = res1.replace(",","").replace(",","");
			var val = $(this).val()
			$(this).val(res1)
			if($(this).val()<0)
		    {
		      $(this).val('');
		    }
		}
		//this.selectionStart = this.selectionEnd = this.value.length;
		
		
	});
	$( "select" ).focus(function() {
	});
	$( "input" ).focusout(function() {
		 var nam = $(this).attr('class');
		 if(name!='fecha')
		 {
		 //$(this).get(0).type = 'text';
		 }
		/* */
		 if(nam=="currency")
		 {
			 if($(this).val()<0 || $(this).val()=="")
			 {
				$(this).val(0.00);
			 }
		 $('.currency').formatCurrency({colorize: true});
		 }
	});
	
	$( "select" ).focusout(function() {
	});
	
		var idp = $('#presupuesto').val()
		var table2 = $('#tbEstadoscontrato').DataTable( {
			"dom": '<"top"plf>rt<"bottom"><"clear">',
		 "columnDefs": 
		 [ 
		 	{ "targets": [1], "visible":false },			       
         ],
		 //"scrollX": true,
		// scrollY:     500,
        // scrollCollapse: true,
         "searching":true,
		"paging":true,
		"ordering": false,
		"info": true,
            "autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[20,30,40,-1], [20,30,40,"Todos"]],
		"language": {
			"lengthMenu": "Ver _MENU_ registros",
			"zeroRecords": "No se encontraron datos",
			"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
			"infoEmpty": "No se encontraron datos",
			"infoFiltered": "",
			"paginate": {"previous": "&#9668;","next":"&#9658;"},
			"sProcessing":'Buscando Datos...'
		},
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "modulos/presupuesto/estadocontratojson.php",
			"data": {pre: idp},
			"type":"POST"
		},
			//keys: true,
		"columns": [
		    { "orderable" : false,"data" : "Delete"},
		    { "data" : "Orden","className" : "dt-center" },
            { "data" : "Contrato","className" : "dt-left" },
            { "data" : "Grupo","className" : "dt-center" },
            { "data" : "Contratista","className" : "dt-left" },
            { "data" : "Nit","className" : "dt-left" },
            { "data" : "Objeto","className" : "dt-left" },
            { "data" : "ValorInicial","className" : "dt-right" },
            { "data" : "Iva","className" : "dt-right" },
            { "data" : "VrOtro","className" : "dt-right" },
            { "data" : "IvaOtro","className" : "dt-right" },
            { "data" : "VrFinal","className" : "dt-right" },
            { "data" : "Anticipo","className" : "dt-right" },
            { "data" : "AnticipoAmo","className" : "dt-right" },
            { "data" : "SaldoAnticipo","className" : "dt-right" },
            { "data" : "RetGarantia","className" : "dt-right" },
            { "data" : "Deducciones","className" : "dt-right" },
            { "data" : "Neto","className" : "dt-right" },
            { "data" : "Facturado","className" : "dt-right" },
            { "data" : "IvaFacturador","className" : "dt-right" },
            { "data" : "TotalFacturado","className" : "dt-right" },
            { "data" : "SaldoContrato","className" : "dt-right" },
            { "data" : "fei","className" : "dt-center" },
            { "data" : "fef","className" : "dt-center" },
            { "data" : "aseguc","className" : "dt-center" },
            { "data" : "valorc","className" : "dt-right" },
            { "data" : "inicioc","className" : "dt-center" },
            { "data" : "finc","className" : "dt-center" },
            { "data" : "asegus","className" : "dt-center" },
            { "data" : "valors","className" : "dt-right" },
            { "data" : "inicios","className" : "dt-center" },
            { "data" : "fins","className" : "dt-center" },
            { "data" : "asegub","className" : "dt-center" },
            { "data" : "valorb","className" : "dt-right" },
            { "data" : "iniciob","className" : "dt-center" },
            { "data" : "finb","className" : "dt-center" },
            { "data" : "asegur","className" : "dt-center" },
            { "data" : "valorr","className" : "dt-right" },
            { "data" : "inicior","className" : "dt-center" },
            { "data" : "finr","className" : "dt-center" },
            { "data" : "asegue","className" : "dt-center" },
            { "data" : "valore","className" : "dt-right" },
            { "data" : "inicioe","className" : "dt-center" },
            { "data" : "fine","className" : "dt-center" },
            { "data" : "fechal","className" : "dt-center" }
		],
		"order": [[1, 'asc']],
		fnDrawCallback: function() {
        var k = 1;
        INICIALIZARLISTAS('');
        $("#tbEstadoscontrato tbody td").each(function () {
           // $(this).attr('tabindex', 1);
            k++;
        })
    }
		
    } );


	var table2 = $('#tbEstadoscontrato').DataTable(); 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
   /* table2
        .on( 'key', function ( e, datatable, key, cell, originalEvent ) {
            events.prepend( '<div>Key press: '+key+' for cell <i>'+cell.data()+'</i></div>' );
        } )
        .on( 'key-focus', function ( e, datatable, cell ) {
            events.prepend( '<div>Cell focus: <i>'+cell.data()+'</i></div>' );
        } )
        .on( 'key-blur', function ( e, datatable, cell ) {
            events.prepend( '<div>Cell blur: <i>'+cell.data()+'</i></div>' );
        } )*/

	$('.agregar').on('click',function()
	{
		var pre = $('#presupuesto').val();
		if(pre=="" || pre==null)
		{
			error('Seleccionar el presupuesto agregar contrato. Verificar');
		}
		else
		{
			$.post('funciones/fnEstadoscontrato.php',{opcion:'AGREGARCONTRATO',pre:pre},
			function(data)
			{
			  data = $.trim(data);
			  if(data==1)
			  {
                  var tablee = $('#tbEstadoscontrato').DataTable();
                  tablee.draw('full-hold');
			    // setTimeout(CRUDPRESUPUESTOINICIAL('ESTADOSCONTRATOS','','','',''),1000);
			  }
			});
		}
	});

	var currCell = $('#tbEstadoscontrato tbody td').first();
var editing = false;

// User clicks on a cell
$('#tbEstadoscontrato td').click(function() {
    currCell = $(this);
	currCell.toggleClass("editing");
	//currCell.focus();
	currCell.children('input').focus();
	//currCell.children('select').focus();
	//currCell.children('.select2').focus();
});
// User navigates table using keyboard
$('#tbEstadoscontrato tbody').keydown(function (e) {
	var tds = $("#tbEstadoscontrato tbody td").length;
	//console.log("Cantidad Celdas:"+tds);
    var c = "";
    if (e.which == 39) {
        // Right Arrow
        c = currCell.next();
    } else if (e.which == 37) { 
        // Left Arrow
        c = currCell.prev();
    } else if (e.which == 38) { 
        // Up Arrow
        c = currCell.closest('tr').prev().find('td:eq(' + 
          currCell.index() + ')');
    } else if (e.which == 40) { 
        // Down Arrow
        c = currCell.closest('tr').next().find('td:eq(' + 
          currCell.index() + ')');
    } 
	else if (!editing && (e.which == 9 && !e.shiftKey)) { 
        // Tab
        e.preventDefault();
        c = currCell.next();
    } else if (!editing && (e.which == 9 && e.shiftKey)) { 
        // Shift + Tab
        e.preventDefault();
        c = currCell.prev();
    } 
    
    // If we didn't hit a boundary, update the current cell
    if (c.length > 0) {
        currCell = c;
        currCell.focus();
		currCell.children('input').focus();
		//currCell.children('select').focus();
		//INICIALIZARLISTAS('PRESUPUESTO');
    }
});

    var clonedHeaderRow;
    
       $("#tbEstadoscontrato .persist-area").each(function() {
           clonedHeaderRow = $(".persist-header", this);
           clonedHeaderRow
             .before(clonedHeaderRow.clone())
             .css("width", clonedHeaderRow.width())
             .addClass("floatingHeader")
             
       });
       
       $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
});


