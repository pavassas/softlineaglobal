// JavaScript Document
$(document).ready(function(e) {
    

    var table = $('#tbinsumos').DataTable( {       
        
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"processing": true,
        "serverSide": true,
        "ajax": "modulos/insumos/proceso_insumos.php",
		"columns": [ 			
			{ "data": "Editar", "className":"dt-center" },
			{ "data": "Eliminar", "className":"dt-center" },			
			{ "data": "Codigo", "className":"dt-center" },
			{ "data": "Tipo" },
			{ "data": "Tipologia",
			  "render": function ( data, type, full, meta ) {
					 if(data==1)
					 {
					return 'Material';
					 }
					 else if(data==2)
					 {
					return 'Equipo'; 
					 }
					 else if(data==3)
					 {
					return 'Mano de Obra'; 
					 }
   				 }
			},
			{ "data": "Nombre" },
			{ "data": "Cod","className": "dt-center"  },
			{ "data": "Valor","className": "dt-right" },
			{ "data": "Descripcion" },
            { "data": "Ciudad" },
            { "data": "TipoProyecto" },
			{ "data": "Estado",
			   "render": function ( data, type, full, meta ) {
					 if(data=="Activo")
					 {
						return '<span class="label label-success pull-center">'+data+'</span>';
					 }
					 else
					 {
						return '<span class="label label-info pull-center">'+data+'</span>'; 
					 }
   				 }
			}			
		],
		"order": [[2, 'asc']]

		
    } );
     
	  $('#tbinsumos tfoot th').each( function () {
        var title = $('#tbinsumos thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
			if(title=="ESTADO")
			{
				$(this).html('');
			}
			else
			if(title=="TIPOLOGÍA")
			{
			 $(this).html('<input class="form-control input-sm" list="listas" placeholder="'+title+'" /><datalist id="listas">  <option value="1" label="Material"><option value="2" label="Equipo"><option value="3" label="Mano de Obra"></datalist>');
	
			}	
			else
			{
			
        $(this).html( '<div class="input-group"><input type="text" class="form-control input-sm 	" placeholder="'+title+'" /></div>' );
			}
		}
    } );
 
    // DataTable
    var table = $('#tbinsumos').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
    $('#tbinsumos tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	// Add event listener for opening and closing details
 
});