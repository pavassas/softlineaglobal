/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

    if ( $.fn.dataTable.isDataTable( '#example' ) ) {
        var table2 = $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable();
        table2.destroy();
	}

		var selected = [];
		var cap = $('#idca').val();
		var gru  = $('#idgru').val();
		var pre =  $('#presupuesto').val();
		var act  = $('#idact').val();
		var opcr = $('#opcionr').val();
		var table2 = $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable( {     
		 "columnDefs": 
		 [ 		
           { "targets": [1], "visible": false }          
         ],
		"dom": '<"top"i>rt<"bottom"p><"clear">',
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10], [10]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"sProcessing":'Buscando Datos'
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/rec_agregarjson.php",
                    "data": {cap:cap,gru:gru,pre:pre,act:act,opc:1,opcr:opcr}
				},
		"columns": [
			{ "data":  "Asignar"},	
			{ "data": "Codigo" },							
			{ "data": "Tipo" },
			{ "data": "Tipologia" },
			{ "data": "Nombre" },			
			{ "data": "Cod","className":"dt-center" },
			{ "data": "Valor", "className": "dt-right"},
			{ "data": "Rendimiento","className":"dt-center"  }					
		],
		"order": [[2, 'asc'],[3, 'asc'],[4, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        },
		//scrollY:        '35vh',
        //scrollCollapse: true,
        //paging:         false
		
    } );
     
	/* $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act+' tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected); 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        } 
        $(this).toggleClass('selected');
    } );*/
	//filtros de busqueda
	
	 $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act+' tfoot th').each( function () {
        var title = $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act+' tfoot th').eq( $(this).index() ).text();
		
			if(title=="" || title=="VR.UNITARIO" || title=="REND"){$(this).html('');}else
			if(title=="TIPOLOGIA")
			{
			$(this).html('<input class="form-control input-sm" list="listas" placeholder="'+title+'" /><datalist id="listas">  <option value="1" label="Material"><option value="2" label="Equipo"><option value="3" label="Mano de Obra"></datalist>');
			
			}	else if($.trim(title)!="Nuevo"){
			$(this).html( '<input type="text"  class="form-control input-sm" style="width:100%"  placeholder="'+title+'" />' );}
    } );
	   var table2 = $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable();
 
    // Apply the search
   	table2.columns().every( function () {
        var that = this;
          $( 'input', this.footer() ).keyup( function (e) {
				if (e.keyCode == 13) {
							that
								.search( this.value )
								.draw();
				}
        } ).change( function () {				
							that
								.search( this.value )
								.draw();				
        } );
    } );	
	$("#selectp"+pre+"g"+gru+"c"+cap+"a"+act).keyup(function(e){
		//console.log("Por aca");
	    if(e.keyCode==13)
		{
			$('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable().search($(this).val(),'','').draw();
		}
	}).change(function(){
	       $('#tbrecursosagregadosp'+pre+'g'+gru+'c'+cap+'a'+act).DataTable().search($(this).val(),'','').draw();
	});
});

function filterColumn (id, i ) {
    $('#tbactividadesagregadas').DataTable().column( i ).search( $('#'+id).val() ).draw();
	//console.log( $('#'+id).val());
}
	


