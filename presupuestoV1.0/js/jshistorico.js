
// JavaScript Document
$(document).ready(function(e) {
    
	var selected = [];
	var tipo = $('#bustipoproyecto').val();
	var cli = $('#buscliente').val();
	var nom = $('#busnombre').val();
	var fec = $('#busfecha').val();

    var table = $('#tbpresupuesto').DataTable( { 
	      
         "dom": '<"top"pi>rt<"bottom"l><"clear">',
		"columnDefs": 
		 [ 
		   { "targets": [ 1 ], "visible": false},
		    { "targets": [ 5], "className": "dt-center" },
		   { "targets": [ 8 ], "className": "dt-center" },
		   { "targets": [ 9 ], "className": "dt-center" },
		   { "targets": [ 10 ], "className": "dt-center" },
		   { "targets": [ 11 ], "className": "dt-right" }
		        
         ],	
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[18,30 ], [18,30 ]],
		"language": {
		"lengthMenu": "",
		"zeroRecords": "No se encontraron datos",
		"info": "",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"},
		"sProcessing":     "Procesando...",
		"sLoadingRecords": "Cargando...",
		"oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    		}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/historicojson.php",
                    "data": {tipoproyecto:tipo,nombre:nom,fec:fec,cliente:cli,hi:0}
				},
			"columns": [
			{
			"class":          "details-control",
			"orderable":      false,
			"data":           null,
			"defaultContent": ""
			},		
			{				
				"orderable":      false,
				"data":           "Presupuesto"				
			},		
			{ "data" : "Nombre", "className": "dt-left"},
			{ "data" : "Creacion", "className": "dt-center"},
			{ "data" : "Persona", "className": "dt-left"},
			{ "data" : "Tipop", "className": "dt-left"},
			{ "data" : "Adm", "className": "dt-center" },
			{ "data" : "Imp", "className": "dt-center" },
			{ "data" : "Uti" , "className": "dt-center"},
			{ "data" : "Iva", "className": "dt-center" },			
			{ "data" : "Total", "className": "dt-right currency" },		
			{
				
				"orderable":      false,
				"data":           "Presupuesto",
				"render": function ( data, type, full, meta ) { 
      					return "<a class='btn btn-block btn-default btn-xs' style='cursor:pointer; width:20px; height:20px' href='modulos/presupuesto/presupuestoexportar.php?edi="+data+"' target='_blank' title='Exportar Presupuesto'><i class='glyphicon glyphicon-save-file'></i></a>";
   				 }
			}
				
		],
		"order": [[4, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) 
			{
                $(row).addClass('selected');				
            }	
        }		
    } );
    
 
    // DataTable
    //var table = $('#tbpresupuesto').DataTable();

	
  // Add event listener for opening and closing details
   var detailRows = [];
    $('#tbpresupuesto tbody').on('click', 'td.details-control', function () {
      
		var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var id = dat.Presupuesto;
		//alert(id);
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
			var table1 = "<div id='divchilg"+id+"'><table  class='table table-condensed' id='tbgrupos"+id+"'>";
			table1+="<thead><tr>"+
			"<th class='dt-head-center'></th>"+
			"<th class='dt-head-center'>CAP</th>"+
			"<th class='dt-head-center'>ITEM</th>"+
			"<th class='dt-head-center'>GRUPO</th>"+
			"<th class='dt-head-center'>UN</th>"+
			"<th class='dt-head-center'>CANT</th>"+
			"<th class='dt-head-center'>VR.UNIT</th>"+
			"<th class='dt-head-center'>VR.TOTAL</th>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAGRUPOS",pre:id},//CREAR FUNCION
			function(data)
			{				
			  
			   var res = data[0].res;
			 //  alert(res);
			   if(res=="no")
			   {
			   }
			   else
			   {
					for(var i=0; i<data.length; i++)
					{
						table1+="<tr style=\"background-color:rgba(215,215,215,1.00);font-weight:bold\">"+
						"<td class='details-control1'></td>"+
						"<td></td>"+
						"<td></td>"+
						"<td data-title='GRU'>"+data[i].nombre+"</td>"+
						"<td data-title='UN'></td>"+
						"<td data-title='CANT'></td>"+
						"<td data-title='VR UNIT'></td>"+
						"<td data-title='VR TOTAL'>$"+data[i].total+"</td>"+						
						"<td data-title='CODIGO'>"+data[i].grupo+"</td>"+
						"</tr>";
					}
			   }
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata4(id);
				},"json");
				//row.child(format(row.data())).show();
				tr.addClass('shown');
			}
	} );
	
	/* $('#tbpresupuesto tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
			      
			var id = $(this).attr('id');
			var n=id.split("row_");	
			//jAlert(n[1],null);
			
        }
    } );*/
	// Add event listener for opening and closing details
	table.on( 'draw', function () {
			$.each( detailRows, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
		
		/*$(function(){*/
    $("#tbpresupuesto tbody").contextMenu({
        selector: 'tr', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idp = dat.Presupuesto;
					
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTO('ELIMINARPERMANENTE',idp,'','','',''),1000);
			}	
			else if(key=="copy")
			{
				setTimeout(CRUDPRESUPUESTO('DUPLICARPRESUPUESTO',idp,'','','',''),1000);
			}
			else if(key=="restore")
			{
				setTimeout(CRUDPRESUPUESTO('RESTAURARPRESUPUESTO',idp,'','','',''),1000);
			}
			//console.log(idp) 
           // window.console && || alert(m); 
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Duplicar", icon: "copy"},
            "restore": {name: "Restaurar", icon: "restore"},			
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
/*});*/
   setTimeout("$('.currency').formatCurrency({colorize: true})",1000);
});
function convertirdata4(d)
{
   var table = $('#tbgrupos'+d).DataTable(
   {   
        
         "columnDefs": 
		 [ 
		   { "targets": [ 6 ], "className": "dt-right" },
		   { "targets": [ 7 ], "className": "dt-right" },
		   { "targets": [ 8 ], "visible": false},
       
         ],
		"searching":false,
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}		
		},
		"order": [[2, 'asc']]
    } );
	
	 $('#tbgrupos'+d+' tbody').on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var pre = d;
		var idg = dat[8];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
			var table1 = "<div id='divchil"+idg+"'><table  class='table table-condensed' id='tbcapitulo"+idg+"'>";
			table1+="<thead><tr>"+
			"<th class='dt-head-center'></th>"+
			"<th class='dt-head-center'>CAPITULO</th>"+
			"<th class='dt-head-center'>UN</th>"+
			"<th class='dt-head-center'>CANT</th>"+
			"<th class='dt-head-center'>VR.UNIT</th>"+
			"<th class='dt-head-center'>VR.TOTAL</th>"+
			"<th class='dt-head-center'>CODIGO</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTACAPITULOS",gru:idg,pre:pre},//CREAR FUNCION
			function(dato)
			{				
			    var res = dato[0].res;
				if(res=="no"){}else
				{				
					for(var i=0; i<dato.length; i++)
					{
						table1+="<tr style='background-color:rgba(215,215,215,0.5)'>"+
						"<td class='details-control2'></td>"+
						"<td data-title='CODIGO'>"+dato[i].codc+"</td>"+
						"<td data-title='CAP'>"+dato[i].nombre+"</td>"+
						"<td data-title='UN'></td>"+
						"<td data-title='CANT'></td>"+
						"<td data-title='VR.UNIT'></td>"+
						"<td data-title='VR.TOTAL'>$"+dato[i].total+"</td>"+
						
						"<td data-title='CODIGO'>"+dato[i].capitulo+"</td></tr>";
					}
				}
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata(idg,pre);
				},"json");
				//row.child(format(row.data()) ).show();
				tr.addClass('shown1');
			}
    });
}

function convertirdata(d,pre)
{
   var table = $('#tbcapitulo'+d).DataTable(
   {   
         "columnDefs": 
		 [ 
		   { "targets": [ 4 ], "className": "dt-center" },
		   { "targets": [ 5 ], "className": "dt-right" },
		   { "targets": [ 6 ], "className": "dt-right"},
           { "targets": [ 7 ], "visible": false },
       
         ],
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Capitulos",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"searching":false,
		"order": [[3, 'asc']],
		fnDrawCallback: function() {
			$('#tbcapitulo'+d+' thead').remove();
		}
		
    } );
	
	  $('#tbcapitulo'+d+' tbody').on('click', 'td.details-control2', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var cap = dat[7];
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown2');
        }
        else 
		{
            // Open this row
		var table1 = "<div id='divchila"+pre+"g"+d+"c"+cap+"'><table  class='table table-bordered' id='tbactividadpr"+pre+"g"+d+"c"+cap+"' style='font-size:11px'>";
		table1+="<thead><tr>"+
		"<th class='dt-head-center'></th>"+
		
		"<th class='dt-head-center'>CODIGO</th>"+
		"<th class='dt-head-center' style='width:20px' >ITEM</th>"+
		"<th class='dt-head-center'>ACTIVIDAD</th>"+
		"<th class='dt-head-center'>UN</th>"+
		"<th class='dt-head-center'>CANT</th>"+
		"<th class='dt-head-center'>VR.UNIT</th>"+
		"<th class='dt-head-center'>VR.TOTAL</th>"+
		"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAACTIVIDADES",cap:cap,pre:pre,gru:d},//CREAR FUNCION
			function(dato)
			{
				var res = dato[0].res;
				if(res=="no"){}
				else
				{
					for(var i=0; i<dato.length; i++)
					{
					table1+="<tr>"+
					"<td class='details-control1'></td>"+	
								
					"<td data-title='CODIGO'>"+dato[i].idactividad+"</td>"+
					"<td data-title='ITEM'>"+dato[i].items+"</td>"+
					"<td data-title='ACTIVIDAD'>"+dato[i].actividad+"</td>"+
					"<td data-title='UN'>"+dato[i].unidad+"</td>"+
					"<td data-title='CANT'>"+dato[i].cantidad+"</td>"+
					"<td data-title='VR.TOTAL'>$"+dato[i].apu+"</td>"+
					"<td data-title='VR.TOTAL'>$"+dato[i].total+"</td></tr>";
		
					}
				}
				table1+="</tbody></table></div>";
				row.child(table1).show();
				convertirdata3(pre,d,cap);
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown2');
         }
    } );
}

function convertirdata3(pre,d,cap)
{
   var table = $("#tbactividadpr"+pre+"g"+d+"c"+cap).DataTable(
   {   
        
         "columnDefs": 
		 [ 
		   { "targets": [ 1 ], "visible": false, "className": "dt-center" },
		   { "targets": [ 4 ], "className": "dt-center" },
		   { "targets": [ 5 ], "className": "dt-center" },
		   { "targets": [ 6 ], "className": "dt-right" },
		   { "targets": [ 7 ], "className": "dt-right"},
		   
       
         ],
		"searching":false,
		"ordering": true,
		"info": false,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10 ], ["Todos",10 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros - Lista de Actividades Asignadas ",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"paging":false,		
		fnDrawCallback: function() {
			$("#tbactividadpr"+pre+"g"+d+"c"+cap+" thead").remove();
		}
    } );
	
	 $("#tbactividadpr"+pre+"g"+d+"c"+cap+" tbody").on('click', 'td.details-control1', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
		var dat = row.data();
		var ida = dat[1];
 
        if ( row.child.isShown() ) 
		{
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown1');
        }
        else 
		{
            // Open this row
			var table2 = "<div class='nav-tabs-custom'>"+
			"<ul class='nav nav-tabs pull-left' id='nav"+ida+"' title'ASIGNACION DE APU Y SUBANALISIS'>"+
			"<li class='active'>"+
			"<a onclick=CRUDPRESUPUESTO('VERAPU','"+pre+"','"+d+"','"+cap+"','"+ida+"','') data-toggle='tab' aria-expanded='true'><strong>APU</strong></a>"+
			"</li>"+
			"<li>"+
			"<a onclick=CRUDACTIVIDADES('VERSUBANALISIS','"+ida+"') data-toggle='tab' aria-expanded='true'><strong>SUB-ANALISIS</strong></a>"+
			"</li>"+
			"</ul>"+
			"</div><div class='panel panel-default'><div class='panel-body' id='divchil"+ida+"' ><div><table style='font-size:11px'  class='table table-condensed table-striped compact' id='tbinsumos"+ida+"'>";
			table2+="<thead><tr>"+
			"<th class='dt-head-center'>RECURSO</th>"+
			"<th class='dt-head-center'>UND</th>"+
			"<th class='dt-head-center'>REND</th>"+
			"<th class='dt-head-center'>V/UNITARIO</th>"+
			"<th class='dt-head-center'>MATERIAL</th>"+
			"<th class='dt-head-center'>EQUIPO</th>"+
			"<th class='dt-head-center'>M.DE.O</th>"+
			"</tr></thead><tbody>";
	      
	              
			$.post('funciones/fnPresupuesto.php',{opcion:"LISTAINSUMOS",id:ida,pre:pre,gru:d,cap:cap},
			function(dato)
			{
		       
				for(var i=0; i<dato.length; i++)
				{
					table2+="<tr><td data-title='Insumo'>"+dato[i].insumo+"</td>"+
					"<td data-title='Unidad'>"+dato[i].unidad+"</td>"+
					"<td data-title='Rendimiento' class='dt-center'>"+dato[i].rendi+"</td>"+
					"<td data-title='Valor Unitario' class='dt-right'> $"+dato[i].valor+"</td>"+
					"<td data-title='Material' class='dt-right'> $"+dato[i].material+"</td>"+
					"<td data-title='Equipo' class='dt-right'>$"+dato[i].equipo+"</td>"+
					"<td data-title='Mano de obra' class='dt-right'>$"+dato[i].mano+"</td></tr>";
				}
				 
			table2+="</tbody></table></div></div></div>";
			row.child(table2).show();
			},"json");
             //row.child(format(row.data()) ).show();
            tr.addClass('shown1');
        }
    });
}

