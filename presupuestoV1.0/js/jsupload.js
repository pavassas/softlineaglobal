		function subirArchivos() 
		{
		$('#progreso').css('display','none');	
		$('#publishform1').html('');
		
		var formData = new FormData($(".formulario1")[0]);
		var message = "";    
		//hacemos la petición ajax  
		$.ajax({
		url: 'modulos/actividades/montar_archivo.php',  
		type: 'POST',
		// Form data
		// datos del formulario
		data: formData,
		//necesario para subir archivos via ajax
		cache: false,
		contentType: false,
		processData: false,
		//mientras enviamos el archivo
		beforeSend: function(){
			
			//$('#mensaje').html('<div class="alert alert-info">Guardando archivo, por favor espere...</div>');
			//message = "Guardando Gestión, por favor espere...";
			//showMessage(message)         
		},
		//una vez finalizado correctamente
		success: function(data){
			if(data=="error1")
			{
				message = "Formato De Archivo de antes no Permitido .Verificar";
			    mostrarRespuesta(message, false);
			}
			else			
			if(data!="")
			{							
				$('#progreso span').html("Analizando Archivo Por favor Espere...");		
				$('#progreso').focus();
				$('#mensaje').html('');
				ejecutarArchivos(data);						
			 	//mostrarArchivos();
			}			
			//mensaje de error adicionales
		},
//si ha ocurrido un error
		error: function(){
			message = "Ha ocurrido un error";
			mostrarRespuesta(message, false);
		}
	});
  
			/*
			$("#archivo").upload('modulos/actividades/subir_archivo.php',
			{
			   
			},
			function(respuesta) 
			{
				//Subida finalizada.
				//$("#barra_de_progreso").val(0);
				if (respuesta == 1) {
					mostrarRespuesta('El archivo ha sido subido correctamente.', true);
					$("#archivo").val('');
				} else if(respuesta==0) {
					mostrarRespuesta('El archivo NO se ha podido subir.', false);
					
				}
				mostrarArchivos();
			}, function(progreso, valor) {
				//Barra de progreso.
			//	angular.element($('#ProgressCtrl')).scope().cargar(valor);
			$('#cargando').html("<img alt='cargando' src='dist/img/cargando.gif' height='20' width='40' />");
			$('#textcarga').html(valor+'/100 %');
			});
		*/}
		function eliminarArchivos(archivo) {
                $.ajax({
                    url: 'modulos/actividades/eliminar_archivo.php',
                    type: 'POST',
                    timeout: 10000,
                    data: {archivo: archivo},
                    error: function() {
                        mostrarRespuesta('Error al intentar eliminar el archivo.', false);
                    },
                    success: function(respuesta) {
                        if (respuesta == 1) {
                            mostrarRespuesta('El archivo ha sido eliminado.', true);
                        } else {
                            mostrarRespuesta('Error al intentar eliminar el archivo.', false);                            
                        }
                        //mostrarArchivos();
                    }
                });
            }
			function ejecutarArchivos(archivo) 
			{
				$('#progreso').css('display','block');
				$('#publishform1').html('');	
				//var url = 'modulos/actividades/ejecutar_archivo.php?archivo='+archivo
				$.post('modulos/actividades/ejecutar_archivo.php',{archivo:archivo},function(data)
					{
						//console.log("Nombre de Archivo: "+archivo);
						$('#publishform1').html(data);	
					}
				);
				/*
				$('#publishform1').html('').append($("<iframe>", {
				src: url,
				css: { "width": "100%", "height": "400px" }
				}));*/
		
			}
			function CAMBIARPROGESO(e)
			{
				if(e=="error")
				{				
					$('#progreso span').html("Surgieron Errores En el archivo a Importar. Verificar");
				}
				else if(e=="ok")
				{
					$('#progreso span').html("Guardando Archivo.Por Favor Espere...");
					$('#archivoactividad').replaceWith( $('#archivoactividad').val('').clone( true ) );	
					$('#progreso').hide(10000);
					$('#mensaje').html('');		
					var message = "Archivo Montado exitosamente";
					setTimeout(mostrarRespuesta(message, true),1000);				
				}
			}
			function OCULTARPROGRESO()
			{
				
			}
            function mostrarArchivos() {
                $.ajax({
                    url: 'modulos/actividades/mostrar_archivos.php',
                    dataType: 'JSON',
                    success: function(respuesta) {
                        if (respuesta) {
                            var html = '';
                            for (var i = 0; i < respuesta.length; i++) {
                                if (respuesta[i] != undefined) {
                                    html += '<div class="row"> <span class="col-xs-4"> ' + respuesta[i] + ' </span> <div class="col-xs-1"> <a class="eliminar_archivo btn btn-danger" role="buttom" href="javascript:void(0);"><i class="glyphicon glyphicon-trash"></i> </a> </div> </div> <hr />';
                                }
                            }
                            $("#archivos_subidos").html(html);
                        }
                    }
                });
            }
            function mostrarRespuesta(mensaje, ok1){
                //$("#respuesta").removeClass('alert-success').removeClass('alert-danger').html(mensaje);
                if(ok1){
					ok(mensaje);
					$('#cargando').html("");
                    //$("#respuesta").addClass('alert-success');
                }else{
                   // $("#respuesta").addClass('alert-danger');
				   error(mensaje);
					$('#cargando').html("");
					$('#textcarga').html('0/100 %');
				
                }
				//setTimeout("$('#respuesta').html('').removeClass('alert-success').removeClass('alert-danger')",10000);
            }
            $(document).ready(function() {
               // mostrarArchivos();
                $("#boton_subir").on('click', function() {
					//alert('ok');
                    subirArchivos();
                });
                $("#archivos_subidos").on('click', '.eliminar_archivo', function() {
					
                    var archivo = $(this).parents('.row').eq(0).find('span').text();
                    archivo = $.trim(archivo);
                    eliminarArchivos(archivo);
                });
				$("#archivos_subidos").on('click', '.ejecutar_archivo', function() {
                    var archivo = $(this).parents('.row').eq(0).find('span').text();
                    archivo = $.trim(archivo);
					alert(archivo);
                    ejecutarArchivos(archivo);
                });
            });