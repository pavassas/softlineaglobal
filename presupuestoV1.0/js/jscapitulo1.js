// JavaScript Document
$(document).ready(function(e) {
    	var ida = $('#idasociar').val();
		var selected = [];
   		 var table1 = $('#tbcapitulo1').DataTable( {       
		
		"dom": '<"top"i>frt<"bottom"lp><"clear">',
		"ordering": true,
		"info": false,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[5,10], [5,10]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},
		"paging":false,
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/grupos/capitulos1json.php",
                    "data": {idg: ida}
				},		
		"columns": [
		{ "data": "Nombre" },	
		]
    } );
     
	  $('#tbcapitulo1 tfoot th').each( function () {
        var title = $('#tbcapitulo1 thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );
	$('#tbcapitulo1 tbody').on('click', 'tr', function () {
		var id = this.id;
		var index = $.inArray(id, selected);		
		if ( index === -1 ) {
			selected.push( id );
		} else {
			selected.splice( index, 1 );
		}		
		$(this).toggleClass('selected');
	} );
    // DataTable
    var table1 = $('#tbcapitulo1').DataTable();
 
    // Apply the search
    table1.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
 
 
    $('#asignar').click( function() 
	{
		var table = $('#tbcapitulo1').DataTable();		
		var nums = table.rows('.selected').data().length;
		if(nums<=0)
		{		
			error('"No a seleccionado ningun capitulo a asignar');
			//alert(")
		}
		else
		{
			for(var i = 0; i<selected.length;i++)
			{	
				if(selected[i]=="" || selected[i]==null)
				{
					
				}
				else
				{				
					var id = selected[i];
					var n=id.split("rowc_");
					//alert(id);
					CRUDGRUPOS('AGREGARCAPITULO',n[1]);
					
				}
			}
			selected.length = 0;
			//setTimeout(CRUDGRUPOS('ASOCIAR',ida),2000);
			setTimeout(cargarlistadostipo('CARGARLISTAGRUPOS'),2000);
		
		}
	});
	$('#asignartodos').click( function() 
	{
		var ida = $('#idasociar').val();
		CRUDGRUPOS('AGREGARTODOS','');
	})
});