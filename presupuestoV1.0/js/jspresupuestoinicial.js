
// JavaScript Document
$(document).ready(function(e) {
    var selected = [];
    var table = $('#tbpresupuesto').DataTable( { 
	      
         "dom": '<"top"pi>rt<"bottom"l><"clear">',
		"columnDefs": 
		 [ 
		   { "targets": [ 6 ], "className": "dt-center" },
		   { "targets": [ 7 ], "className": "dt-center" },
		   { "targets": [ 8 ], "className": "dt-center" },
		   { "targets": [ 9 ], "className": "dt-right" },
		   { "targets": [ 10 ], "visible":false },
		   { "targets": [ 11 ], "visible":false }       
         ],	
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[-1,10,20,30 ], ["Todos",10,20,30 ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/presupuesto/presupuestoseleccionjson.php",
                    "data": {tipoproyecto:'',nombre:'',fec:'',cliente:''}
				},
			"columns": [			
			{ "data" : "Nombre", "className": "dt-left"},
			{ "data" : "Creacion", "className": "dt-center"},
			{ "data" : "Persona", "className": "dt-left"},
			{ "data" : "Tipop", "className": "dt-left"},
			{ "data" : "Estadop", "className": "dt-left"},
			{ "data" : "Adm", "className": "dt-center" },
			{ "data" : "Imp", "className": "dt-center" },
			{ "data" : "Uti" , "className": "dt-center"},
			{ "data" : "Iva", "className": "dt-center" },			
			{ "data" : "Total", "className": "dt-center" },	
			{				
				"orderable":      false,
				"data":           "Presupuesto",
				"render": function ( data, type, full, meta ) { 
      					return "<a title='Eliminar Presupuesto' class='btn btn-block btn-danger btn-xs' style='cursor:pointer;width:20px;height:20px' onclick=CRUDPRESUPUESTO('ELIMINAR','"+data+"','','','','') ><i class='glyphicon glyphicon-trash'></i></a>";
   				 }
			},		
			{				
				"orderable":      false,
				"data":           "Presupuesto",
				"render": function ( data, type, full, meta ) { 
      					return "<a class='btn btn-block btn-default btn-xs' style='cursor:pointer' href='modulos/presupuesto/presupuestoexportarcontrol.php?edi="+data+"' target='_blank' title='Exportar Presupuesto'><i class='glyphicon glyphicon-save-file'></i></a>";
   				 }
			}
				
		],
		"paging":false,
		//"searching":false,
		"order": [[0, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) 
			{
                $(row).addClass('selected');
				
            }	
        }
		
    } );
     
	/*  $('#tbpresupuesto tfoot th').each( function () {
        var title = $('#tbpresupuesto thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<div class="input-group"><input type="text" class="form-control" placeholder="'+title+'" /></div>' );}
    } );*/
     
	 
    // DataTable
    var table = $('#tbpresupuesto').DataTable();

    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
	//seleccion
   $('#tbpresupuesto tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
			var row = table.row($(this));
		    var dat = row.data();
		    var nom = dat.Nombre;
			$('#presupuestoseleccionado').html("/"+nom).css('color','#6E835D');
			var id = dat.Presupuesto;			
			$('#presupuesto').val(id);	
			//$('#presupuesto>option[value="'+n[1]+'"]').attr('selected','selected');
			CRUDPRESUPUESTOINICIAL('INFORMES',id,'','','');
			//setTimeout(INICIALIZARLISTAS('PRESUPUESTO'),1);
			// $('#myModal').modal('hide');
			
        }
    } );
	
	  $("#tbpresupuesto tbody").contextMenu({
        selector: 'tr', 
        callback: function(key, options) {
			
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var dat = row.data();
			var idp = dat.Presupuesto;
					
			if(key=="delete")
			{
				setTimeout(CRUDPRESUPUESTO('ELIMINAR',idp,'','','',''),1000);
			}	
			else if(key=="copy")
			{
				setTimeout(CRUDPRESUPUESTO('DUPLICARPRESUPUESTO',idp,'','','',''),1000);
			}
			else if(key=="restore")
			{
				var url = "modulos/presupuesto/presupuestoexportarcontrol.php?edi="+idp;
				window.open(url);
			}
			
			//console.log(idp) 
           // window.console && || alert(m); 
        },
        items: {
            //"edit": {name: "Edit", icon: "edit"},
            //"cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Duplicar", icon: "copy"},
            "restore": {name: "Exportar", icon: "restore"},			
            "delete": {name: "Eliminar", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Cerrar", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
 
});
function filtrarColumnPresupuesto (id, i ) {
    $('#tbpresupuesto').DataTable().column( i ).search( $('#'+id).val() ).draw();
	//console.log( $('#'+id).val());
}