var app = angular.module('myApp', [
	'ui.router',
	'ui.bootstrap',
	'oc.lazyLoad'
	//,
	//'angularFileUpload'
	
])

app.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', 'JS_REQUIRES', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, jsRequires){
	
	// LAZY MODULES
    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });
	
	$urlRouterProvider.otherwise("/Dashboard");
	
	$stateProvider
		.state('Dashboard', {
			url: '/Dashboard',
			templateUrl: 'modulos/dashboard/dashboard.php'
		})
		.state('Division', {
			url: '/Localización',
			templateUrl: 'modulos/division/division.php'
		})
		.state('Cargos', {
			url: '/Cargos',
			templateUrl: 'modulos/cargos/cargos.php'
		})
		.state('Clientes', {
			url: '/Clientes',
			templateUrl: 'modulos/clientes/clientes.php'
		})
		
		.state('Unidades', {
			url: '/UnidadesdeMedida',
			templateUrl: 'modulos/unidades/unidades.php'
		})
		.state('Tipoproyecto', {
			url: '/Tipodeproyecto',
			templateUrl: 'modulos/tipoproyecto/tipoproyecto.php'
			//resolve: loadSequence('multiselect')
		})
		.state('Estadoproyecto', {
			url: '/EstadosProyecto',
			templateUrl: 'modulos/estadosproyecto/estadosproyecto.php'
		})
		.state('Tipocontrato', {
			url: '/TipodeContrato',
			templateUrl: 'modulos/tipocontrato/tipocontrato.php'
		})
		.state('Tipoinsumo', {
			url: '/TiposdeRecurso',
			templateUrl: 'modulos/tipoinsumos/tipoinsumos.php'
		})
		.state('Insumos', {
			url: '/Recursos',
			templateUrl: 'modulos/insumos/insumos.php'
		})
		.state('Capitulos', {
			url: '/Capitulos',
			templateUrl: 'modulos/capitulos/capitulos.php'
		})
		.state('Grupos', {
			url: '/Grupos',
			templateUrl: 'modulos/grupos/grupos.php'
		})
		.state('Actividades', {
			url: '/Actividades',
			templateUrl: 'modulos/actividades/actividades.php'
		})
		.state('Usuarios', {
			url: '/Usuarios',
			templateUrl: 'modulos/usuarios/usuarios.php'
		})
		.state('Perfiles', {
			url: '/Perfiles',
			templateUrl: 'modulos/perfiles/perfiles.php'
		})
		.state('Importacion', {
			url: '/Importación',
			templateUrl: 'modulos/actividades/importacion.php'
		})
		.state('Presupuesto', {
			url: '/Presupuesto',
			templateUrl: 'modulos/presupuesto/presupuesto.php'
		})
		.state('Historico', {
			url: '/HistoricoPresupuesto',
			templateUrl: 'modulos/presupuesto/historico.php'
		})
		.state('HistoricoControl', {
			url: '/HistoricoControlPresupuesto',
			templateUrl: 'modulos/presupuesto/historicocontrol.php'
		})
		.state('Presupuestoinicial', {
			url: '/ControlPresupuesto',
			templateUrl: 'modulos/presupuesto/presupuestoinicial.php'
		})
		.state('Presupuestoegreso', {
			url: '/ControlPresupuestalxEgreso',
			templateUrl: 'modulos/presupuesto/presupuestoegreso.php'
		})
		.state('Informeejecutivo', {
			url: '/InformeEjecutivo',
			templateUrl: 'modulos/presupuesto/presupuestoejecutivo.php'
		})
		.state('Importacionpresupuesto', {
			url: '/ImportaciónPresupuesto',
			templateUrl: 'modulos/presupuesto/importacion.php'
		})
		.state('Importacionpresupuesto2', {
			url: '/ImportaciónPresupuesto2',
			templateUrl: 'modulos/presupuesto/importacionpresupuesto.php'
		})
		.state('cambiocontrasena', {
			url: '/CambiodeContraseña',
			templateUrl: 'modulos/cambiarcontrasena/cambiocontrasena.php'
		})
		;
		
		// Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
        function loadSequence() {
            var _args = arguments;
            return {
                deps: ['$ocLazyLoad', '$q',
                    function ($ocLL, $q) {
                        var promise = $q.when(1);
                        for (var i = 0, len = _args.length; i < len; i++) {
                            promise = promiseThen(_args[i]);
                        }
                        return promise;

                        function promiseThen(_arg) {
                            if (typeof _arg == 'function')
                                return promise.then(_arg);
                            else
                                return promise.then(function () {
                                    var nowLoad = requiredData(_arg);
                                    if (!nowLoad)
                                        return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                    return $ocLL.load(nowLoad);
                                });
                        }

                        function requiredData(name) {
                            if (jsRequires.modules)
                                for (var m in jsRequires.modules)
                                    if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
                                        return jsRequires.modules[m];
                            return jsRequires.scripts && jsRequires.scripts[name];
                        }
                    }]
            };
        }
}]);

app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        //*** Javascript Plugins
        /*'multiselect': [
        	'dist/js/prettify.js',
        	'dist/css/bootstrap-multiselect.css',
        	'dist/js/bootstrap-multiselect.js',
        	'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'],*/
        /*'multiselect': ['dist/css/checklist/jquery.multiselect.css',
        'dist/css/checklist/jquery.multiselect.filter.css',
        'dist/css/checklist/styleselect.css',
        'dist/css/checklist/prettify.css',
        'dist/css/checklist/jquery-ui.css',
        'http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js',
        'dist/js/checklist/jquery.multiselect.js',
        'dist/js/checklist/jquery.multiselect.filter.js',
        'dist/js/checklist/prettify.js']*/
    }
});

app.controller('TabsDemoCtrl', function ($scope, $window) {
  $scope.tabs = [
    { title:'Dynamic Title 1', content:'Dynamic content 1' },
    { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
  ];
	$scope.alertMe = function(g) {
	setTimeout(function() {
		cargarlistadostipo(g);
	});
	};
});

app.controller('TabsCtrlPresupuesto', function ($scope, $window) {
  $scope.tabs = [
    { title:'Dynamic Title 1', content:'Dynamic content 1' },
    { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
  ];
	$scope.seleccionTab = function(g) {
	setTimeout(function() {
		CRUDPRESUPUESTOINICIAL(g,'','');
	});
	};
});
/*
 app.controller('AppController', ['$scope', 'FileUploader', function($scope, FileUploader) {
        var uploader = $scope.uploader = new FileUploader({
            url: 'modulos/actividades/upload.php'
        });

        // FILTERS

        uploader.filters.push({
            name: 'customFilter',
            fn: function(item , options) {
                return this.queue.length < 10;
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
			$
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
			
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
			
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
			
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
			
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
			
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem,response, status, headers);
			$('#archivo').replaceWith( $('#archivo').val('').clone( true ) );
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
			
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
			$('file').replaceWith( $('file').val('').clone( true ) );
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
			$('file').replaceWith( $('file').val('').clone( true ) );
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
			$('file').replaceWith( $('file').val('').clone( true ) );
        };

        console.info('uploader', uploader);
    }]);
*/	
	
	
	
	app.controller('ProgressCtrl', function ($scope) {
  
  	$scope.max = 100;

 	 $scope.cargar = function(v) {
    var value = v;// Math.floor(Math.random() * 100 + 1);
    var type;

    if (value < 25) {
      type = 'success';
    } else if (value < 50) {
      type = 'info';
    } else if (value < 75) {
      type = 'warning';
    } else {
      type = 'danger';
    }

    $scope.showWarning = type === 'danger' || type === 'warning';

    $scope.dynamic = value;
    $scope.type = type;
  };
  

});
	
	



