<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idpresupuesto = $_GET['edi'];
include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('safe_mode', 0);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}
//DATOS DEL PRESUPUESTO
$con  = mysql_query("select UPPER(per_nombre) nom,UPPER(per_apellido)  ape,UPPER(per_razon) cli, per_documento,pre_nombre,pre_fecha,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_usu_creacion,pre_cliente,tpp_clave_int,date_format(pre_fecha,'%Y%m%d') as fec,pre_apli_iva,pre_coordinador as cor,pre_av_plan,pre_av_real,pre_documento  from presupuesto pr left outer join persona p on p.per_clave_int = pr.per_clave_int where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$fech = $dat['fec'];
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$nomo = $dat['pre_nombre'];
$fechai  =$dat['pre_fecha'];
$apli = $dat['pre_apli_iva'];
$tpp = $dat['tpp_clave_int'];
$precliente = $dat['pre_cliente'];
$avplan = $dat['pre_av_plan'];
$avreal = $dat['pre_av_real'];
$atraso = $avplan - $avreal;
$codproyecto = $dat['pre_codigo'];
$docproyecto = $dat['pre_documento'];
$nomcliente = $dat['cli'];
$cor = $dat['cor'];
$cont = mysql_query("select tpp_nombre from tipoproyecto where tpp_clave_int = '".$tpp."'");
$datt =  mysql_fetch_array($cont);
$tppr =  strtoupper($datt['tpp_nombre']);

if($precliente!=""){$cliente=$precliente;}else { $cliente = $nomcliente." - NIT: ".$dat['per_documento'];}
$creado = $dat['pre_usu_creacion'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$creado."' limit 1");
$dat = mysql_fetch_array($con);
$creadopor = $dat['usu'];
$car  = $dat['car_clave_int'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$cor."' limit 1");
$dat = mysql_fetch_array($con);
$aprobadopor = $dat['usu'];
$carc  = $dat['car_clave_int'];

$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$car."'");
$datc = mysql_fetch_array($conc);
$cargo = $datc['car'];
$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$carc."'");
$datc = mysql_fetch_array($conc);
$cargoc = $datc['car'];

$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysql_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

$totadm = ($totalc * $adm)/100;
$totimp = ($totalc * $imp)/100;
$totuti = ($totalc * $uti)/100;
if($apli==0){ $totiva = ($totuti * $iva)/100; }else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100; }
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$adma = $dat['pri_administracion'];
$ivaa = $dat['pri_iva'];
$impa = $dat['pri_imprevisto'];
$utia = $dat['pri_utilidades'];

$totadma = ($totalca * $adma)/100;
$totimpa = ($totalca * $impa)/100;
$totutia = ($totalca * $utia)/100;
if($apli==0) { $totivaa = ($totutia * $ivaa)/100; } else { $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;

$alccos = $totalc - $totalca;
$alcpre = $totpre - $totprea;
$alcadm = $totadm - $totadma;
$alcimp = $totimp - $totimpa;
$alcuti = $totuti - $totutia;
$alciva = $totiva - $totivaa;


/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);

// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => array('argb' => 'FF993300'),
        ),
    ),
);

// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
    ->setLastModifiedBy("Pavas.co")
    ->setTitle("Informe Presupuesto")
    ->setSubject("Informe Presupuesto")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Informes");

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A1' , "")
    ->setCellValue('E1' , "ESTADO DE CONTRATO")
    ->setCellValue('E2' , "VERSIÓN: 05")
    ->setCellValue('J2' , "FECHA VERSION: 30/03/2016")
    ->setCellValue('O2' , "APROBADO POR: NATALÍ PINZÓN")
    ->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL")
    ->setCellValue('A4' , "CONTRATISTA: ")
    ->setCellValue('A5' , "CLIENTE: ".$cliente)
    ->setCellValue('A6' , "NOMBRE DE LA OBRA: ".$nomo)

    ->setCellValue('A11' , "N° CONTRATO")
    ->setCellValue('B11' , "GRUPO")
    ->setCellValue('C11' , "CONTRATISTA")
    ->setCellValue('D11' , "NIT")
    ->setCellValue('E11' , "OBJETO DEL CONTRATO")
    ->setCellValue('F11' , "VALOR INICIAL CONTRATO")
    ->setCellValue('G11' , "IVA")
    ->setCellValue('H11' , "VALOR OTRO SI")
    ->setCellValue('I11' , "IVA DEL OTRO SI")
    ->setCellValue('J11' , "VALOR FINAL")
    ->setCellValue('K11' , "ANTICIPO PAGADO")
    ->setCellValue('L11' , "ANTICIPO AMORTIZADO")
    ->setCellValue('M11' , "SALDO DEL ANTICIPO")
    ->setCellValue('N11' , "VALOR RETENCIÓN EN GARANTIA")
    ->setCellValue('O11' , "OTRAS DEDUCCIONES")
    ->setCellValue('P11' , "VALOR NETO PAGADO")
    ->setCellValue('Q11' , "VALOR FACTURADO")
    ->setCellValue('R11' , "VALOR IVA FACTURADO")
    ->setCellValue('S11' , "VALOR TOTAL FACTURADO")
    ->setCellValue('T11' , "SALDO DEL CONTRATO")

    ->setCellValue('U11' , "CONTRATO")
    ->setCellValue('U12' , "FECHA INICIO")
    ->setCellValue('V12' , "FECHA DE TERMINACION")
    ->mergeCells('U11:V11')
    ->mergeCells('U12:U14')
    ->mergeCells('V12:V14')

    ->setCellValue('W11' , "POLIZAS")

    ->setCellValue('W12' , "CUMPLIMIENTO")
    ->setCellValue('W13' , "% ASEGURADO")
    ->setCellValue('X13' , "VALOR ASEGURADO")
    ->setCellValue('Y13' , "FECHA")
    ->setCellValue('Y14' , "INICIO")
    ->setCellValue('Z14' , "FIN")
    ->mergeCells('W11:AP11')
    ->mergeCells('W12:Z12')
    ->mergeCells('W13:W14')
    ->mergeCells('X13:X14')
    ->mergeCells('Y13:Z13')

   // ->setCellValue('X11' , "")
   // ->setCellValue('Y11' , "")
   // ->setCellValue('Z11' , "")
    ->setCellValue('AA12' , "SALARIOS PRESTACIONALES")
    ->setCellValue('AA13' , "% ASEGURADO")
    ->setCellValue('AB13' , "VALOR ASEGURADO")
    ->setCellValue('AC13' , "FECHA")
    ->setCellValue('AC14' , "INICIO")
    ->setCellValue('AD14' , "FIN")
    ->mergeCells('AA12:AD12')
    ->mergeCells('AC13:AD13')
    ->mergeCells('AA13:AA14')
    ->mergeCells('AB13:AB14')

    ->setCellValue('AE12' , "BUENO MANEJO ANTICIPO")
    ->setCellValue('AE13' , "% ASEGURADO")
    ->setCellValue('AF13' , "VALOR ASEGURADO")
    ->setCellValue('AG13' , "FECHA")
    ->setCellValue('AG14' , "INICIO")
    ->setCellValue('AH14' , "FIN")
    ->mergeCells('AE12:AH12')
    ->mergeCells('AG13:AH13')
    ->mergeCells('AE13:AE14')
    ->mergeCells('AF13:AF14')

    ->setCellValue('AI12' , "RESPONSABILIDAD CIVIL")
    ->setCellValue('AI13' , "% ASEGURADO")
    ->setCellValue('AJ13' , "VALOR ASEGURADO")
    ->setCellValue('AK13' , "FECHA")
    ->setCellValue('AK14' , "INICIO")
    ->setCellValue('AL14' , "FIN")
    ->mergeCells('AI12:AL12')
    ->mergeCells('AK13:AL13')
    ->mergeCells('AI13:AI14')
    ->mergeCells('AJ13:AJ14')

    ->setCellValue('AM12' , "ESTABILIDAD Y CALIDAD")
    ->setCellValue('AM13' , "% ASEGURADO")
    ->setCellValue('AN13' , "VALOR ASEGURADO")
    ->setCellValue('AO13' , "FECHA")
    ->setCellValue('AO14' , "INICIO")
    ->setCellValue('AP14' , "FIN")
    ->mergeCells('AM12:AP12')
    ->mergeCells('AO13:AP13')
    ->mergeCells('AM13:AM14')
    ->mergeCells('AN13:AN14')


    ->setCellValue('AQ11' , "FECHA DE ACTA LIQUIDACION")
    ->mergeCells('AQ11:AQ14')


    ->mergeCells('A1:D2')
    ->mergeCells('E1:R1')
    ->mergeCells('E2:I2')
    ->mergeCells('J2:N2')
    ->mergeCells('O2:R2')

    ->mergeCells('A3:R3')
    ->mergeCells('A4:R4')
    ->mergeCells('A5:R5')
    ->mergeCells('A6:R6')
    ->mergeCells('A7:R7')


    ->mergeCells('A11:A14')
    ->mergeCells('B11:B14')
    ->mergeCells('C11:C14')
    ->mergeCells('D11:D14')
    ->mergeCells('E11:E14')
    ->mergeCells('F11:F14')
    ->mergeCells('G11:G14')
    ->mergeCells('H11:H14')
    ->mergeCells('I11:I14')
    ->mergeCells('J11:J14')
    ->mergeCells('K11:K14')
    ->mergeCells('L11:L14')
    ->mergeCells('M11:M14')
    ->mergeCells('N11:N14')
    ->mergeCells('O11:O14')
    ->mergeCells('P11:P14')
    ->mergeCells('Q11:Q14')
    ->mergeCells('R11:R14')
    ->mergeCells('S11:S14')
    ->mergeCells('T11:T14')
    ->mergeCells('AQ11:AQ14')
;

cellColor('A1:R2', 'FFFFFF');
cellColor('A11:AQ14', '92D050');

$objPHPExcel->getActiveSheet()->getRowDimension(8)->setVisible(false);
$objPHPExcel->getActiveSheet()->getRowDimension(9)->setVisible(false);
$objPHPExcel->getActiveSheet()->getRowDimension(10)->setVisible(false);
$objPHPExcel->getActiveSheet()->getStyle('A11:AQ14')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("A1:R2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A11:AQ14')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle("A11:AQ14")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E1:R2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:R6')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('E1:R1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A11:AQ14')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('E2:R2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A1:R6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('A11:AQ14')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A11:AQ14')->getAlignment()->setWrapText(true);
//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('10')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getStyle('A8:R9')->getFont()->setSize(10);


$conpre = mysql_query("select * from estados_contrato where pre_clave_int = '".$idpresupuesto."'");

$numpre = mysql_num_rows($conpre);
if($numpre<=0){$numpre=1;}
$hasta = $numpre + 15;

$acum = $hasta;
$filc = 15;
$totalvalor  = 0;
for ($i = 15; $i < $hasta; $i++)
{
    $dat = mysql_fetch_array($conpre);
    $ide = $dat['esc_clave_int'];
    $ordc = $dat['esc_orden'];
    $gru = $dat['gru_clave_int'];
    $cong = mysql_query("select gru_nombre from grupos where gru_clave_int = '".$gru."'");
    $datg = mysql_fetch_array($cong);
    $nomg = $datg['gru_nombre'];

    $cont = $dat['esc_contrato'];
    $contra = $dat['esc_contratista'];
    $nit = $dat['esc_nit'];
    $obje = $dat['esc_obj_contrato'];
    $vali = $dat['esc_val_inicial'];
    $iv = $dat['esc_iva'];
    $valos = $dat['esc_vr_otrosi'];
    $ivos = $dat['esc_iva_otrosi'];
    $dedu = $dat['esc_deducciones'];
    $fei = $dat['esc_fec_inicio']; if($fei=="0000-00-00"){$fei="";}
    $fef = $dat['esc_fec_fin']; if($fei=="0000-00-00"){$fei="";}

    $aseguc = $dat['esc_cum_asegu'];
    $valorc = $dat['esc_cum_valor'];
    $inicioc = $dat['esc_cum_inicio']; if($inicioc=="0000-00-00"){$inicioc="";}
    $finc = $dat['esc_cum_fin']; if($finc=="0000-00-00"){$finc="";}

    $asegus = $dat['esc_sal_asegu'];
    $valors = $dat['esc_sal_valor'];
    $inicios = $dat['esc_sal_inicio']; if($inicios=="0000-00-00"){$inicios="";}
    $fins = $dat['esc_sal_fin']; if($fins=="0000-00-00"){$fins="";}

    $asegub = $dat['esc_bue_asegu'];
    $valorb = $dat['esc_bue_valor'];
    $iniciob = $dat['esc_bue_inicio']; if($iniciob=="0000-00-00"){$iniciob="";}
    $finb = $dat['esc_bue_fin']; if($finb=="0000-00-00"){$finb="";}

    $asegur = $dat['esc_res_asegu'];
    $valorr = $dat['esc_res_valor'];
    $inicior = $dat['esc_res_inicio']; if($inicior=="0000-00-00"){$inicior="";}
    $finr = $dat['esc_res_fin']; if($finr=="0000-00-00"){$finr="";}

    $asegue = $dat['esc_est_asegu'];
    $valore = $dat['esc_est_valor'];
    $inicioe = $dat['esc_est_inicio']; if($inicioe=="0000-00-00"){$inicioe="";}
    $fine = $dat['esc_est_fin']; if($fine=="0000-00-00"){$fine="";}

    $fechal = $dat['esc_fec_acta']; if($fechal=="0000-00-00"){$fechal="";}

    if($nit!="" and $gru>0)
    {
        $condatos = mysql_query("select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo,sum(cpe_iva) iv,sum(cpe_ret_garantia) gar,sum(cpe_valor_neto) net from control_egreso where pre_clave_int = '".$idpresupuesto."'  and (cpe_documento = '".$nit."' and UPPER(cpe_beneficiario) = UPPER('".$contra."-".$cont."'))");//and gru_clave_int = '".$gru."'
        $dato = mysql_fetch_array($condatos);
        $ant = $dato['ant'];
        $amo = $dato['amo'];
        $iva = $dato['iv'];
        $ret = $dato['gar'];
        $net = $dato['net'];
        $bru = $net + $iva;
        $sala = $ant - $amo;
    }
    else
    {
        $ant = 0;
        $amo = 0;
        $iva = 0;
        $ret = 0;
        $net = 0;
        $bru = 0;
        $sala = 0;
    }

    $vf  = $vali + $iv + $valos + $ivos;
    $nett = $bru - $amo - $ret - $dedu;
    $salc = $vf - $bru;

    $objPHPExcel->getActiveSheet()
        ->setCellValue('A' . $filc, $cont)
        ->setCellValue('B' . $filc, $gru)
        ->setCellValue('C' . $filc, $contra)
        ->setCellValue('D' . $filc, $nit)
        ->setCellValue('E' . $filc, $obje)
        ->setCellValue('F' . $filc, $vali)
        ->setCellValue('G' . $filc, $iv)
        ->setCellValue('H' . $filc, $valos)
        ->setCellValue('I' . $filc, $ivos)
        ->setCellValue('J' . $filc, '=SUM(F'.$filc.':I'.$filc.')')//$vf
        ->setCellValue('K' . $filc, $ant)
        ->setCellValue('L' . $filc, $amo)
        ->setCellValue('M' . $filc, $sala)
        ->setCellValue('N' . $filc, $ret)
        ->setCellValue('O' . $filc, $dedu)
        ->setCellValue('P' . $filc, '=S'.$filc.'-L'.$filc.'-N'.$filc.'-O'.$filc)//nett
        ->setCellValue('Q' . $filc, $net)
        ->setCellValue('R' . $filc, $iva)
        ->setCellValue('S' . $filc, $bru)
        ->setCellValue('T' . $filc, '=J'.$filc.'-S'.$filc)//salc
        ->setCellValue('U' . $filc, $fei)
        ->setCellValue('V' . $filc, $fef)
        ->setCellValue('W' . $filc, $aseguc)
        ->setCellValue('X' . $filc, $valorc)
        ->setCellValue('Y' . $filc, $inicioc)
        ->setCellValue('Z' . $filc, $finc)
        ->setCellValue('AA' . $filc, $asegus)
        ->setCellValue('AB' . $filc, $valors)
        ->setCellValue('AC' . $filc, $inicios)
        ->setCellValue('AD' . $filc, $fins)
        ->setCellValue('AE' . $filc, $asegub)
        ->setCellValue('AF' . $filc, $valorb)
        ->setCellValue('AG' . $filc, $iniciob)
        ->setCellValue('AH' . $filc, $finb)
        ->setCellValue('AI' . $filc, $asegur)
        ->setCellValue('AJ' . $filc, $valorr)
        ->setCellValue('AK' . $filc, $inicior)
        ->setCellValue('AL' . $filc, $finr)
        ->setCellValue('AM' . $filc, $asegue)
        ->setCellValue('AN' . $filc, $valore)
        ->setCellValue('AO' . $filc, $inicioe)
        ->setCellValue('AP' . $filc, $fine)
        ->setCellValue('AQ' . $filc, $fechal)
    ;
    cellColor('A' . $filc.':AQ'.$filc, 'FFFFFF');
    $objPHPExcel->getActiveSheet()->getComment('B'.$filc)->setAuthor('Linea Global');
    $objPHPExcel->getActiveSheet()->getComment('B'.$filc)->getText()->createTextRun($nomg)->getFont()->setSize(7.5);

    $objPHPExcel->getActiveSheet()->getStyle('F'.$filc.":T".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('X'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('AB'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('AF'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('AJ'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('AN'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':AQ'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':AQ'.$filc)->getFont()->setSize(10);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':AQ'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':AQ'.$filc)->getAlignment()->setWrapText(true);

    //FIN CONSULTAS

    $filc = $filc+ 1;

}
$filc2 = $filc +1;
if($numpre<=0){ $filcs = $filc; }else { $filcs = $filc-1;}

$objPHPExcel->getActiveSheet()
    /*->setCellValue('A8' , "PARTIDA PRESUPUESTAL")
    ->setCellValue('A9' , "SALDO PRESUPUESTAL")
    ->setCellValue('E8' , $totpre)
    ->setCellValue('E9' , '=+E8-H'.$filc2)
    ->setCellValue('A'.$filc , 'TOTALES')
    ->setCellValue('H'.$filc , '=SUM(H13:H'.$filcs.')')
    ->setCellValue('I'.$filc , '=SUM(I13:I'.$filcs.')')
    ->setCellValue('J'.$filc , '=SUM(J13:J'.$filcs.')')
    ->setCellValue('K'.$filc , '=SUM(K13:K'.$filcs.')')
    ->setCellValue('L'.$filc , '=SUM(L13:L'.$filcs.')')
    ->setCellValue('M'.$filc , '=SUM(M13:M'.$filcs.')')
    ->setCellValue('O'.$filc , '=SUM(O13:O'.$filcs.')')
    ->setCellValue('P'.$filc , '=SUM(P13:P'.$filcs.')')
    ->setCellValue('A'.$filc2 ,'TOTAL INVERTIDO')
    ->setCellValue('H'.$filc2 , '=+J'.$filc.'+L'.$filc.'-M'.$filc)
    ->setCellValue('I'.$filc2 , '')
    ->setCellValue('J'.$filc2 , '')
    ->setCellValue('K'.$filc2 , '')
    ->setCellValue('L'.$filc2 , '')
    ->setCellValue('M'.$filc2 , '')
    ->setCellValue('O'.$filc2 , '')
    ->setCellValue('P'.$filc2 , '')
    ->mergeCells('A8:D8')
    ->mergeCells('A9:D9')
    ->mergeCells('E8:G8')
    ->mergeCells('E9:G9')
    ->mergeCells('H8:R9')
    ->mergeCells('A10:R10')*/
   // ->mergeCells('A'.$filc.':AQ'.$filc)
    //->mergeCells('A'.$filc2.':AQ'.$filc2)
    ///->mergeCells('Q'.$filc.':R'.$filc)
;
$objPHPExcel->getActiveSheet()->getStyle('A8:G9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("A8:G9")->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('E8:G9')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('E8:G9')->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':AQ'.$filc)->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('H'.$filc.':P'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('H'.$filc.":M".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('O'.$filc.":P".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');


//$objPHPExcel->getActiveSheet()->getStyle('A'.$filc2.':AQ'.$filc2)->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('H'.$filc2.':P'.$filc2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

//$objPHPExcel->getActiveSheet()->getStyle('H'.$filc2.':P'.$filc2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);


//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25.57);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(18);
$objPHPExcel->getActiveSheet()->getStyle('A15:A'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B15:B'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('C15:C'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('D15:D'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('E15:E'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('F15:F'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('G15:G'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('L15:L'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('M15:M'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('N15:N'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('O15:O'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('P15:P'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));


$objPHPExcel->getActiveSheet()->getStyle('U15:V'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('Y15:Z'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('AC15:AD'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('AG15:AH'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('AK15:AL'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('AO15:AQ'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
//$objPHPExcel->getActiveSheet()->getStyle('H13:M'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
//$objPHPExcel->getActiveSheet()->getStyle('O13:P'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
//$objPHPExcel->getActiveSheet()->getStyle('A15:AQ'.$filc2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//$objPHPExcel->getActiveSheet()->getStyle('A'.$filc2.':AQ'.$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

//añadir comentarios a celldas
$objPHPExcel->getActiveSheet()->getComment('B11')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('B11')->getText()->createTextRun('Grupo al cual corresponde el gasto')->getFont()->setBold(true)->setSize(7.5);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(100);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('ESTADO CONTRATO');
//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
$callStartTime = microtime(true);
$archivo =  date('Ymd').' ESTADO CONTRATO  '.$tppr.' '.$nomo.'.xlsx';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//$objWriter->save(str_replace(__FILE__,'descargas/'.$archivo,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
// Set password for readonly activesheet
/*
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("P4v4s2017.*");*/
// Set password for readonly data


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');