<?php
include("../../data/Conexion.php");
$table = 'insumos';
// Table's primary key
$primaryKey = 'i.ins_clave_int';//'act_clave_int'
$pre = $_GET['pre'];
$par = $_GET['par'];
$conda = mysql_query("select par_nombre,par_estado,par_tipo from partidas where par_clave_int ='".$par."'");
$datp  = mysql_fetch_array($conda);
$nompa = $datp['par_nombre'];
$estp = $datp['par_estado'];
$tipop = $datp['par_tipo'];
$columns = array(
	array(
		'db' => 'i.ins_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ins_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowpa_'.$d;
		}// r0
	),
	array( 'db' => 'i.ins_clave_int', 'dt' => 'Codigo', 'field' => 'ins_clave_int' ), //r1
    array( 'db' => "'".$par."'", 'dt' => 'Partida','as'=>'Partida', 'field' => 'Partida' ),  //r2
    array( 'db' => "'".$estp."'", 'dt' => 'EstadoPartida','as'=>'EstadoPartida', 'field' => 'EstadoPartida' ),  //r2
	array( 'db' => 'i.ins_nombre', 'dt' => 'Nombre', 'field' => 'ins_nombre' ),  //r2
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),  //r3
	array( 'db' => 'd.pre_clave_int', 'dt' => 'CantP','field' =>'pre_clave_int','formatter'=>function($d,$row){
			$con = mysql_query("SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant							 
			FROM insumos i
			JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysql_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}
			
			$con2 = mysql_query("SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS  Cant1								 
			FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysql_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}
			$totc = $can + $can1;
			return  "<span title='".$totc."'>".number_format($totc,2,'.','')."</span>";
	}), //r5
	array( 'db' => 'd.pre_clave_int', 'dt' => 'TotalP','field' =>'pre_clave_int','formatter'=>function($d,$row){
		
		     $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
	 		$data = mysql_fetch_array($cona);
	         $apli = $data['apli'];
			 
			 $conad = mysql_query("SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
			 $datad = mysql_fetch_array($conad);
			 $valor = $datad['vr'];
			 $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
			 
			 $con = mysql_query("SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant								 
			FROM insumos i JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysql_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}
			
			$con2 = mysql_query("SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1 FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
			$dat = mysql_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}
			
			$cantidad = $can + $can1;
			if($apli==0)
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = ($totuti*$iva)/100;
			}
			else
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = (($totadm + $totimp + $totuti)*$iva)/100;
			}
			$valor = $valor + $totadm + $totimp + $totuti + $totiva;
			$totale = $valor * $cantidad;
			
			return "<span id='total".$row[0]."' class='currency' title='".$totale."'>$".number_format($totale,2,'.',',')."</span>";
			
	}), //r6
	array( 'db' => 'd.pre_clave_int','dt'=>'Valor','field'=>'pre_clave_int','formatter'=>function($d,$row){
			$cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
	 		$data = mysql_fetch_array($cona);
	        $apli = $data['apli'];
			 
			 $conad = mysql_query("SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."' LIMIT 1");
			 $datad = mysql_fetch_array($conad);
			 $valor = $datad['vr'];
			 $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];
			 if($apli==0)
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = ($totuti*$iva)/100;
			}
			else
			{
			    $totadm = ($valor*$adm)/100;
				$totimp = ($valor*$imp)/100;
				$totuti = ($valor*$uti)/100;
				$totiva = (($totadm + $totimp + $totuti)*$iva)/100;
			}
			$valor = $valor + $totadm + $totimp + $totuti + $totiva;
		
	   return "<span class='currency'>$".number_format($valor,2,'.',',') ."</span>";
	}), //r7
	array('db'=> 'd.pre_clave_int', 'dt'=> 'CantC', 'field' =>'pre_clave_int','formatter'=> function($d,$row){
        $concanc = mysql_query("select sum(pai_cant_comprometida) as canc from partida_item where par_clave_int = '".$row[2]."' and ins_clave_int = '".$row[0]."'");
        $datca = mysql_fetch_array($concanc);
        if($datca['canc']=="" || $datca['canc']==NULL){$cantc = 0;}else{$cantc = $datca['canc'];}
			//$totalc = $row[7] * $cantco;
			return "<span title='".$cantc."' id='cant_".$row[2]."_".$row[0]."'>".number_format($cantc,2,'.',',')."</span>";
	} ),//r8
    array( 'db' => 'd.pre_clave_int', 'dt' => 'Totalc','field' =>'pre_clave_int','formatter'=>function($d,$row){
        $conitem = mysql_query("select d.pre_clave_int pre,d.gru_clave_int gru,d.cap_clave_int cap,g.gru_orden ord from pre_gru_cap_actividad d join partida_item p on p.pre_clave_int = d.pre_clave_int and p.gru_clave_int = d.gru_clave_int and p.cap_clave_int = d.cap_clave_int  and p.act_clave_int = d.act_clave_int join grupos g on g.gru_clave_int = d.gru_clave_int where p.par_clave_int = '".$row[2]."' and p.ins_clave_int = '".$row[0]."' group by pre,gru,cap order by ord,gru,cap");
        $numit = mysql_num_rows($conitem);
        $cono = mysql_query("select sum(pao_valor) totc from partida_observacion where par_clave_int = '".$row[2]."' and ins_clave_int = '".$row[0]."'");
        $dato = mysql_fetch_array($cono);
        $comp = $dato['totc']; if($comp=="" || $comp==NULL){$comp=0;}

        if($row[3]==1){ $dis =  'disabled';}else{$dis="";}
        if($numit<=0) { $dis =  'disabled'; }

        return "<input ".$dis." title='".$comp."' type='text' name='val_".$row[2]."_".$row[0]."' id='val".$row[0]."' style='width:100%; border:thin; text-align:right' onchange=CRUDPARTIDAS('ACTUALIZARVALOR','".$row[2]."','','','','".$row[0]."','','') class='currency' placeholder='$#####' value='$".number_format($comp,2,'.',',')."'>";
    }),

    array( 'db' => 'd.pgi_creacion', 'dt' => 'Creacion', 'field' => 'pgi_creacion' ), //r1
    array( 'db' => 'd.pre_clave_int', 'dt' => 'Observacion','field' =>'pre_clave_int','formatter'=>function($d,$row){
        $cono = mysql_query("select pao_observacion from partida_observacion where par_clave_int = '".$row[2]."' and ins_clave_int = '".$row[0]."'");
        $dato = mysql_fetch_array($cono);
        $numo = mysql_num_rows($cono);
        if($numo>0)
        {
            $obs = $dato['pao_observacion'];
        }
        else
        {
            $obs = "";
        }
        if($row[3]==1){ $dis =  'disabled';}else{$dis="";}
        return "<input ".$dis." type='tex' name='obs_".$row[2]."_".$row[0]."' id='obs".$row[0]."' style='width:100%; border:thin' onchange=CRUDPARTIDAS('ACTUALIZAROBSERVACION','".$row[2]."','','','','".$row[0]."','','') placeholder='Diligencie Observacion(Opcional)' value='".$obs."'>";
        }),
    array('db' => 'd.pre_clave_int', 'dt' => 'Items','field' =>'pre_clave_int','formatter'=>function($d,$row){
        $conitem = mysql_query("select d.pre_clave_int pre,d.gru_clave_int gru,d.cap_clave_int cap,g.gru_orden ord from pre_gru_cap_actividad d join partida_item p on p.pre_clave_int = d.pre_clave_int and p.gru_clave_int = d.gru_clave_int and p.cap_clave_int = d.cap_clave_int  and p.act_clave_int = d.act_clave_int join grupos g on g.gru_clave_int = d.gru_clave_int where p.par_clave_int = '".$row[2]."' and p.ins_clave_int = '".$row[0]."' group by pre,gru,cap order by ord,gru,cap");
        $numit = mysql_num_rows($conitem);
        if($numit>0)
        {
            $item  ="				
				<div class='btn-group btn-group-xs'>
				<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
				Ver Items
				<span class='caret'></span>
				</button>
				<ul class='dropdown-menu'>";
            for($s=0;$s<$numit;$s++)
            {
                $dat2 = mysql_fetch_array($conitem);
                $pre = $dat2['pre'];
                $gru = $dat2['gru'];
                $cap = $dat2['cap'];
                $conc = mysql_query("select pgc_codigo from pre_gru_capitulo d  where d.pre_clave_int = '".$pre."' and d.gru_clave_int ='".$gru."' and d.cap_clave_int = '".$cap."'");
                $datc = mysql_fetch_array($conc);
                $codc = $datc['pgc_codigo'];
                $conitem2 = mysql_query("select d.pgca_clave_int idd,d.act_clave_int act from pre_gru_cap_actividad d where d.pre_clave_int = '".$pre."' and d.gru_clave_int ='".$gru."' and d.cap_clave_int = '".$cap."' order by idd");
                $numit2 = mysql_num_rows($conitem2);
                if($numit2>0)
                {
                    for($l=1;$l<=$numit2;$l++)
                    {
                        $datl2 = mysql_fetch_array($conitem2);
                        $acti2 = $datl2['act'];
                        $convitem = mysql_query("select * from pre_gru_cap_actividad d join partida_item p on p.pre_clave_int = d.pre_clave_int and p.gru_clave_int = d.gru_clave_int and p.cap_clave_int = d.cap_clave_int  and p.act_clave_int = d.act_clave_int where p.par_clave_int = '".$row[2]."' and d.act_clave_int = '".$acti2."' and p.ins_clave_int = '".$row[0]."'");
                        $numitv = mysql_num_rows($convitem);
                        if($l<10){$it="".$codc.".0".$l;}else{$it="".$codc.".".$l;}
                        if($numitv>0)
                        {
                            $item .="<li><a >".$it."</a></li>";
                        }
                    }
                }
                else
                {
                    $item = "";
                }
            }
            $item .="</ul></div>";
        }
        else
        {
            $item = "";
        }
        return $item;
    }),
    array('db' => 'd.pre_clave_int', 'dt' => 'Ck','field' =>'pre_clave_int',"formatter"=>function($d,$row){
        $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$d."' limit 1");
        $data = mysql_fetch_array($cona);
        $apli = $data['apli'];

        $conad = mysql_query("SELECT AVG(pgi_vr_ini) vr,AVG(pgi_adm_ini) adm,AVG(pgi_imp_ini) imp,AVG(pgi_uti_ini) uti,AVG(pgi_iva_ini) iva FROM pre_gru_cap_act_insumo WHERE pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
        $datad = mysql_fetch_array($conad);
        $valor = $datad['vr'];
        $adm = $datad['adm']; $imp = $datad['imp']; $uti = $datad['uti']; $iva = $datad['iva'];

        $con = mysql_query("SELECT  sum(d.pgi_rend_ini * d.pgi_cant_ini) AS Cant								 
			FROM insumos i JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int			
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
        $dat = mysql_fetch_array($con); $can = $dat['Cant']; if($can=="" || $can==NULL){$can = 0;}

        $con2 = mysql_query("SELECT sum(d.pgi_rend_ini * pgi_cant_ini*pgi_rend_sub_ini) AS Cant1 FROM insumos i
			JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
			WHERE d.pre_clave_int = '".$d."' and d.ins_clave_int = '".$row[0]."' and d.pgi_cant_ini>0 and d.pgi_rend_ini>0");
        $dat = mysql_fetch_array($con2);  $can1 = $dat['Cant1']; if($can1=="" || $can1==NULL){$can1 = 0;}

        $cantidad = $can + $can1;
        if($apli==0)
        {
            $totadm = ($valor*$adm)/100;
            $totimp = ($valor*$imp)/100;
            $totuti = ($valor*$uti)/100;
            $totiva = ($totuti*$iva)/100;
        }
        else
        {
            $totadm = ($valor*$adm)/100;
            $totimp = ($valor*$imp)/100;
            $totuti = ($valor*$uti)/100;
            $totiva = (($totadm + $totimp + $totuti)*$iva)/100;
        }
        $valor = $valor + $totadm + $totimp + $totuti + $totiva;
        $totale = $valor * $cantidad;

        $cono = mysql_query("select sum(pai_val_comprometido) totc from partida_item where pre_clave_int = '".$d."' and ins_clave_int = '".$row[0]."'");
        $dato = mysql_fetch_array($cono);
        $comp = $dato['totc']; if($comp=="" || $comp==NULL){$comp=0;}

        $compro = (int)$totale - (int)$comp;
        $conitem = mysql_query("select d.pre_clave_int pre,d.gru_clave_int gru,d.cap_clave_int cap,g.gru_orden ord from pre_gru_cap_actividad d join partida_item p on p.pre_clave_int = d.pre_clave_int and p.gru_clave_int = d.gru_clave_int and p.cap_clave_int = d.cap_clave_int  and p.act_clave_int = d.act_clave_int join grupos g on g.gru_clave_int = d.gru_clave_int where p.par_clave_int = '".$row[2]."' and p.ins_clave_int = '".$row[0]."' group by pre,gru,cap order by ord,gru,cap");
        $numit = mysql_num_rows($conitem);
        if($compro>0 || $numit>0)
        {
           if($numit>0){$fa = "fa-check";}else { $fa = "";}
            return "<a title='".$comp." - ".$totale."=".$compro."' role='button' class='btn btn-default btn-xs' style='width:20px; height:20px' onclick=CRUDPARTIDAS('SELECCIONARITEM','".$row[2]."','','','','".$row[0]."','','') data-toggle='modal' data-target='#myModal'><i class='fa ".$fa."' id='check".$row[0]."'></i></a>";
            }
    })
    );


$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( '../../data/ssp.class.php' );

 $groupBy = 'i.ins_clave_int';
 $joinQuery = " FROM insumos i join unidades  u on u.uni_clave_int  = i.uni_clave_int join pre_gru_cap_act_insumo d on d.ins_clave_int = i.ins_clave_int left outer join pre_gru_cap_act_sub_insumo ds on ds.pre_clave_int = d.pre_clave_int  ";
$extraWhere =  " d.pre_clave_int = '".$pre."'";
if($estp==1)
{
    $extraWhere.=" and i.ins_clave_int in(select ins_clave_int from partida_item where pre_clave_int = '".$pre."' and par_clave_int = '".$par."')";
}
if($tipop==1)
{
    $extraWhere.=" and i.ins_clave_int in(select ins_clave_int from partida_item where pre_clave_int = '".$pre."' and par_clave_int = '".$par."' )";
}
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

