<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idpresupuesto = $_GET['edi'];
include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('safe_mode', 0);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}
//DATOS DEL PRESUPUESTO
$con  = mysql_query("select UPPER(per_nombre) nom,UPPER(per_apellido)  ape,UPPER(per_razon) cli, per_documento,pre_nombre,pre_fecha,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_usu_creacion,pre_cliente,tpp_clave_int,date_format(pre_fecha,'%Y%m%d') as fec,pre_apli_iva,pre_coordinador as cor,pre_av_plan,pre_av_real,pre_documento  from presupuesto pr left outer join persona p on p.per_clave_int = pr.per_clave_int where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$fech = $dat['fec'];
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$nomo = $dat['pre_nombre'];
$fechai  =$dat['pre_fecha'];
$apli = $dat['pre_apli_iva'];
$tpp = $dat['tpp_clave_int'];
$precliente = $dat['pre_cliente'];
$avplan = $dat['pre_av_plan'];
$avreal = $dat['pre_av_real'];
$atraso = $avplan - $avreal;
$codproyecto = $dat['pre_codigo'];
$docproyecto = $dat['pre_documento'];
$nomcliente = $dat['cli'];
$cor = $dat['cor'];
$cont = mysql_query("select tpp_nombre from tipoproyecto where tpp_clave_int = '".$tpp."'");
$datt =  mysql_fetch_array($cont);
$tppr =  strtoupper($datt['tpp_nombre']);

if($precliente!=""){$cliente=$precliente;}else { $cliente = $nomcliente." - NIT: ".$dat['per_documento'];}
$creado = $dat['pre_usu_creacion'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$creado."' limit 1");
$dat = mysql_fetch_array($con);
$creadopor = $dat['usu'];
$car  = $dat['car_clave_int'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$cor."' limit 1");
$dat = mysql_fetch_array($con);
$aprobadopor = $dat['usu'];
$carc  = $dat['car_clave_int'];

$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$car."'");
$datc = mysql_fetch_array($conc);
$cargo = $datc['car'];
$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$carc."'");
$datc = mysql_fetch_array($conc);
$cargoc = $datc['car'];

$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysql_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

$totadm = ($totalc * $adm)/100;
$totimp = ($totalc * $imp)/100;
$totuti = ($totalc * $uti)/100;
if($apli==0){ $totiva = ($totuti * $iva)/100; }else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100; }
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$adma = $dat['pri_administracion'];
$ivaa = $dat['pri_iva'];
$impa = $dat['pri_imprevisto'];
$utia = $dat['pri_utilidades'];

$totadma = ($totalca * $adma)/100;
$totimpa = ($totalca * $impa)/100;
$totutia = ($totalca * $utia)/100;
if($apli==0) { $totivaa = ($totutia * $ivaa)/100; } else { $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;

$alccos = $totalc - $totalca;
$alcpre = $totpre - $totprea;
$alcadm = $totadm - $totadma;
$alcimp = $totimp - $totimpa;
$alcuti = $totuti - $totutia;
$alciva = $totiva - $totivaa;


/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);

// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => array('argb' => 'FF993300'),
        ),
    ),
);

// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
    ->setLastModifiedBy("Pavas.co")
    ->setTitle("Informe Presupuesto")
    ->setSubject("Informe Presupuesto")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Informes");


$datos = array();
$num = 0;
$cona = mysql_query("select distinct cpe_num_acta from control_egreso where pre_clave_int = '".$idpresupuesto."' and cpe_num_acta!='' order by cpe_num_acta");
while($data = mysql_fetch_array($cona))
{
    $datos[] = $data['cpe_num_acta'];
    $num++;
}
$col1 = 67;//inicio actas
if($num>0){$col2 = 67 + $num - 1; } else{$col2 = 67;} ;//fin acta
$col3 = $col2 + 1;//valor acumulado
$col4 = $col3 + 1;//vr-retenido
$col5 = $col4 + 1;//anticipo
$col6 = $col5 + 1;//amotizacion
$col7 = $col6 + 1; //saldo contrato
$col1 = chr($col1);
$col2 = chr($col2);
$col3 = chr($col3);
$col4 = chr($col4);
$col5 = chr($col5);
$col6 = chr($col6);
$col7 = chr($col7);

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A1' , "")
    ->setCellValue('C1' , "CONTROL PAGO ACTAS")
    ->setCellValue('C2' , "VERSIÓN: 05")
    ->setCellValue('E2' , "FECHA VERSION: 30/03/2016")
    ->setCellValue('G2' , "APROBADO POR: NATALÍ PINZÓN")
    ->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL")
    ->setCellValue('A4' , "CONTRATISTA: ")
    ->setCellValue('A5' , "CLIENTE: ".$cliente)
    ->setCellValue('A6' , "NOMBRE DE LA OBRA: ".$nomo)

    ->setCellValue('A11' , "N° CONTRATO")
    ->setCellValue('B11' , "VALOR CONTRATO")
    ->setCellValue($col1.'11' , "ACTAS")
    ->setCellValue($col3.'11' , "VALOR ACUMULADO")
    ->setCellValue($col4.'11' , "VR/RETENIDO")
    ->setCellValue($col5.'11' , "ANTICIPO")
    ->setCellValue($col6.'11' , "AMORTIZACIÓN")
    ->setCellValue($col7.'11' , "SALDO CONTRATO")
   ->mergeCells($col1.'11:'.$col2.'11')

    ->mergeCells('A1:B2')
    ->mergeCells('C1:H1')
    ->mergeCells('C2:D2')
    ->mergeCells('E2:F2')
    ->mergeCells('G2:H2')

    ->mergeCells('A3:H3')
    ->mergeCells('A4:H4')
    ->mergeCells('A5:H5')
    ->mergeCells('A6:H6')
    ->mergeCells('A7:H7')

    ->mergeCells('A11:A12')
    ->mergeCells('B11:B12')
    ->mergeCells($col3.'11:'.$col3.'12')
    ->mergeCells($col4.'11:'.$col4.'12')
    ->mergeCells($col5.'11:'.$col5.'12')
    ->mergeCells($col6.'11:'.$col6.'12')
    ->mergeCells($col7.'11:'.$col7.'12');
   if($num>0)
    {
        $col = 67;
        for($k=0;$k<$num;$k++)
        {
            $c = chr($col);
            $objPHPExcel->getActiveSheet()->setCellValue($c.'12',$datos[$k]);
            $col++;
        }

    }
    else
    {
        $col = 67;
        $c = chr($col);
        $objPHPExcel->getActiveSheet()->setCellValue($c.'12',0);
    }

cellColor('A1:R2', 'FFFFFF');
cellColor('A11:'.$col7.'12', '92D050');

$objPHPExcel->getActiveSheet()->getRowDimension(8)->setVisible(false);
$objPHPExcel->getActiveSheet()->getRowDimension(9)->setVisible(false);
$objPHPExcel->getActiveSheet()->getRowDimension(10)->setVisible(false);
$objPHPExcel->getActiveSheet()->getStyle('A11:'.$col7.'12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("A1:H2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A11:'.$col7.'12')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A11:'.$col7.'12')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D1:H2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:H6')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('C1:H2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A11:'.$col7.'12')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('C1:H2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A1:H6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('A11:'.$col7.'12')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A11:'.$col7.'12')->getAlignment()->setWrapText(true);
//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('10')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getStyle('A8:H9')->getFont()->setSize(10);


$conpre = mysql_query("select DISTINCT e.cpe_beneficiario,e.cpe_documento from control_egreso e   where e.pre_clave_int = '".$idpresupuesto."' order by e.cpe_beneficiario");

$numpre = mysql_num_rows($conpre);
if($numpre<=0){$numpre=1;}
$hasta = $numpre + 13;

$acum = $hasta;
$filc = 13;
$totalvalor  = 0;
for ($i = 13; $i < $hasta; $i++)
{
    $datc = mysql_fetch_array($conpre);
    $contra = $datc['cpe_beneficiario'];
    $nit = $datc['cpe_documento'];

    $query1 = mysql_query("select sum(esc_val_inicial) val,sum(esc_iva) iv,sum(esc_vr_otrosi) os,sum(esc_iva_otrosi) as ivo,sum(esc_deducciones) dedu from estados_contrato where esc_nit = '".$nit."' and UPPER(CONCAT_WS('-',esc_contratista,CAST(esc_contrato AS CHAR))) = UPPER('".$contra."') and pre_clave_int = '".$idpresupuesto."'");
    $dat1 = mysql_fetch_array($query1);
    $vcontrato = $dat1['val']+$dat1['iv']+$dat1['os']+$dat1['ivo'];

    $query2 = mysql_query("select sum(cpe_anticipo) as ant,sum(cpe_amortizacion) as amo,sum(cpe_ret_garantia) as gar from control_egreso where pre_clave_int = '".$idpresupuesto."' and cpe_documento = '".$nit."' and UPPER(cpe_beneficiario) = UPPER('".$contra."')");
    $dat2 = mysql_fetch_array($query2);
    $anticipo = $dat2['ant'];
    $amortizado = $dat2['amo'];
    $retenido = $dat2['gar'] + $dat1['dedu'];//duda de si se suma tb las deduciones

        $objPHPExcel->getActiveSheet()
        ->setCellValue('A' . $filc, $contra."- ".$nit)
        ->setCellValue('B' . $filc, $vcontrato);//$vf
    $acumu = 0;
    if($num>0) {

        $col8 = 67;
        for ($k = 0; $k < $num; $k++) {
            $query3 = mysql_query("select sum(cpe_valor_neto) net,sum(cpe_iva) as iv from control_egreso where pre_clave_int = '".$idpresupuesto."' and cpe_documento = '".$nit."' and cpe_num_acta = '".$datos[$k]."' and UPPER(cpe_beneficiario) = UPPER('".$contra."')");
            $dat3 = mysql_fetch_array($query3);
            $net = $dat3['net'];
            $iva = $dat3['iv'];
            $bru = $net + $iva;

            $objPHPExcel->getActiveSheet()
                ->setCellValue(chr($col8).$filc, $bru);//$vf
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($col8))->setWidth(20);
            $col8++;
        }
    }
    else{
        $objPHPExcel->getActiveSheet()
            ->setCellValue($col1 . $filc, "");
        $objPHPExcel->getActiveSheet()->getColumnDimension(chr($col1))->setWidth(20);
    }

        $objPHPExcel->getActiveSheet()
        ->setCellValue($col3 . $filc, '=SUM('.$col1.$filc.':'.$col2.$filc.')')
        ->setCellValue($col4 . $filc, $retenido)
        ->setCellValue($col5. $filc, $anticipo)
        ->setCellValue($col6 . $filc, $amortizado)
        ->setCellValue($col7 . $filc, '=B'.$filc.'-'.$col3.$filc.'-'.$col5.$filc.'+'.$col6.$filc);//$vf

    cellColor('A' . $filc.':'.$col7.$filc, 'FFFFFF');

    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.":".$col7.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');


    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':'.$col7.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':'.$col7.$filc)->getFont()->setSize(10);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':'.$col7.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


    //FIN CONSULTAS

    $filc = $filc+ 1;

}
$filc2 = $filc +1;
if($numpre<=0){ $filcs = $filc; }else { $filcs = $filc-1;}

$objPHPExcel->getActiveSheet()->getStyle('A8:G9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle("A8:G9")->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':AQ'.$filc)->getFont()->setBold(true);



//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($col3)->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($col4)->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($col5)->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($col6)->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($col7)->setWidth(20);

$objPHPExcel->getActiveSheet()->getStyle('B13:B'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B13:'.$col7.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(100);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('CTRL PAGO DE ACTAS');
//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
$callStartTime = microtime(true);
$archivo = $fech.' CTRL PAGO ACTAS '.$tppr.' '.$nomo.'.xlsx';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//$objWriter->save(str_replace(__FILE__,'descargas/'.$archivo,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
// Set password for readonly activesheet
/*
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("P4v4s2017.*");*/
// Set password for readonly data


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');