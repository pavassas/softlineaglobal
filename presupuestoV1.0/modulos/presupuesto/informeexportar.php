<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idpresupuesto = $_GET['edi'];
include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('safe_mode', 0);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");


function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}
//DATOS DEL PRESUPUESTO
$con  = mysql_query("select UPPER(per_nombre) nom,UPPER(per_apellido)  ape,UPPER(per_razon) cli, per_documento,pre_nombre,pre_fecha,pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_usu_creacion,pre_cliente,tpp_clave_int,date_format(pre_fecha,'%Y%m%d') as fec,pre_apli_iva,pre_coordinador as cor,pre_av_plan,pre_av_real,pre_documento  from presupuesto pr left outer join persona p on p.per_clave_int = pr.per_clave_int where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$fech = $dat['fec'];
$adm = $dat['pre_administracion'];
$iva = $dat['pre_iva'];
$imp = $dat['pre_imprevisto'];
$uti = $dat['pre_utilidades'];
$nomo = $dat['pre_nombre'];
$fechai  =$dat['pre_fecha'];
$apli = $dat['pre_apli_iva'];
$tpp = $dat['tpp_clave_int'];
$precliente = $dat['pre_cliente'];
$avplan = $dat['pre_av_plan'];
$avreal = $dat['pre_av_real'];
$atraso = $avplan - $avreal;
$codproyecto = $dat['pre_codigo'];
$docproyecto = $dat['pre_documento'];
$nomcliente = $dat['cli'];
$cor = $dat['cor'];
$cont = mysql_query("select tpp_nombre from tipoproyecto where tpp_clave_int = '".$tpp."'");
$datt =  mysql_fetch_array($cont);
$tppr =  strtoupper($datt['tpp_nombre']);

if($precliente!=""){$cliente=$precliente;}else { $cliente = $nomcliente." - NIT: ".$dat['per_documento'];}
$creado = $dat['pre_usu_creacion'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$creado."' limit 1");
$dat = mysql_fetch_array($con);
$creadopor = $dat['usu'];
$car  = $dat['car_clave_int'];
$con = mysql_query("select UPPER(usu_nombre) usu ,car_clave_int from usuario where usu_clave_int = '".$cor."' limit 1");
$dat = mysql_fetch_array($con);
$aprobadopor = $dat['usu'];
$carc  = $dat['car_clave_int'];

$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$car."'");
$datc = mysql_fetch_array($conc);
$cargo = $datc['car'];
$conc = mysql_query("select UPPER(car_nombre) car from cargos where car_clave_int = '".$carc."'");
$datc = mysql_fetch_array($conc);
$cargoc = $datc['car'];

$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysql_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

$totadm = ($totalc * $adm)/100;
$totimp = ($totalc * $imp)/100;
$totuti = ($totalc * $uti)/100;
if($apli==0){ $totiva = ($totuti * $iva)/100; }else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100; }
$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$adma = $dat['pri_administracion'];
$ivaa = $dat['pri_iva'];
$impa = $dat['pri_imprevisto'];
$utia = $dat['pri_utilidades'];

$totadma = ($totalca * $adma)/100;
$totimpa = ($totalca * $impa)/100;
$totutia = ($totalca * $utia)/100;
if($apli==0) { $totivaa = ($totutia * $ivaa)/100; } else { $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;

	$alccos = $totalc - $totalca;
	$alcpre = $totpre - $totprea;
	$alcadm = $totadm - $totadma;
	$alcimp = $totimp - $totimpa;
	$alcuti = $totuti - $totutia;
	$alciva = $totiva - $totivaa;

	$cons = mysql_query("select sum(cpe_valor_neto) net,sum(cpe_iva) iv, sum(cpe_amortizacion)  as am,sum(cpe_ret_garantia) gar,sum(cpe_valor_aprobado) as ap,sum(cpe_anticipo) as anti from control_egreso where pre_clave_int = '".$idpresupuesto."'");
	$dat = mysql_fetch_array($cons);
	if($dat['net']=="" || $dat['net']==NULL){$totalneto = 0;}else{$totalneto = $dat['net'];}
	if($dat['iv']=="" || $dat['iv']==NULL){$totaliva = 0;}else{$totaliva = $dat['iv'];}
	$totalbru = $totalneto + $totaliva;
	if($dat['am']=="" || $dat['am']==NULL){$totalamo = 0;}else{$totalamo = $dat['am'];}
	if($dat['anti']=="" || $dat['anti']==NULL){$totalanti = 0;}else{$totalanti = $dat['anti'];}
	if($dat['gar']=="" || $dat['gar']==NULL){$totalgarantia = 0;}else{$totalgarantia = $dat['gar'];}
	if($dat['ap']=="" || $dat['ap']==NULL){$totalaprobado = 0;}else{$totalaprobado = $dat['ap'];}

	$saldo = $totpre - ($totalbru + $totalanti - $totalamo);

	$totalsaldo = $totalaprobado - $totalbru ;

     if($totalsaldo<0){$totalsaldo = "(". number_format($totalsaldo*(-1),2,'.',',').")"; $col1 = "red";}
     else{$totalsaldo = number_format($totalsaldo,2,'.',','); $col1="black";}

/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);

// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => array('argb' => 'FF993300'),
        ),
    ),
);

$styleRED = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'CC0000')
    ));


// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
    ->setLastModifiedBy("Pavas.co")
    ->setTitle("Informe Presupuesto")
    ->setSubject("Informe Presupuesto")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Informes");



// Create a first sheet
//echo date('H:i:s') , " Agregar datos" , EOL;
// Add data
//INFORME EJECUTIVO
//SEXTA hoja
//INFORMACION GENERAL DEL PROYECTO
//DATOS


//COSTODIRECTO
$cond = mysql_query("select igp_real,igp_comprometida,igp_codigo,igp_documento from inf_gen_proyecto where pre_clave_int = '".$idpresupuesto."' and igp_tipo=1 limit 1");
$datd = mysql_fetch_array($cond);
if($datd['igp_comprometida']=="" || $datd['igp_real']==NULL){$cdreal = 0;}else{$cdreal = $datd['igp_real'];};
if($datd['igp_comprometida']=="" || $datd['igp_comprometida']==NULL){$cdcomprometido = 0;}else{$cdcomprometido = $datd['igp_comprometida'];};


//EQUIPOS ESPECIALES
$cond = mysql_query("select igp_real,igp_comprometida,igp_codigo,igp_documento from inf_gen_proyecto where pre_clave_int = '".$idpresupuesto."' and igp_tipo=2 limit 1");
$datd = mysql_fetch_array($cond);
if($datd['igp_comprometida']=="" || $datd['igp_real']==NULL){$eereal = 0;}else{$eereal = $datd['igp_real'];};
if($datd['igp_comprometida']=="" || $datd['igp_comprometida']==NULL){$eecomprometido = 0;}else{$eecomprometido = $datd['igp_comprometida'];};


//EQUIPOS ESPECIALES
$cond = mysql_query("select igp_real,igp_comprometida,igp_codigo,igp_documento from inf_gen_proyecto where pre_clave_int = '".$idpresupuesto."' and igp_tipo=3 limit 1");
$datd = mysql_fetch_array($cond);
if($datd['igp_comprometida']=="" || $datd['igp_real']==NULL){$iereal = 0;}else{$iereal = $datd['igp_real'];};
if($datd['igp_comprometida']=="" || $datd['igp_comprometida']==NULL){$iecomprometido = 0;}else{$iecomprometido = $datd['igp_comprometida'];};

//EVENTOS DE CAMBIO
$cond = mysql_query("select igp_real,igp_comprometida,igp_codigo,igp_documento from inf_gen_proyecto where pre_clave_int = '".$idpresupuesto."' and igp_tipo=4 limit 1");
$datd = mysql_fetch_array($cond);
if($datd['igp_comprometida']=="" || $datd['igp_real']==NULL){$ecreal = 0;}else{$ecreal = $datd['igp_real'];};
if($datd['igp_comprometida']=="" || $datd['igp_comprometida']==NULL){$eccomprometido = 0;}else{$eccomprometido = $datd['igp_comprometida'];}

//if($docproyecto=="" || $docproyecto==NULL) { $docproyecto = 1;} $docproyecto = number_format($docproyecto,1,'.','.');
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()
    ->setCellValue('B2' , "INFORME DE AVANCE MENSUAL")
    ->setCellValue('B4' , "1. INFORME EJECUTIVO")
    ->setCellValue('B5' , "1.1")
    ->setCellValue('C5' , "DATOS DEL PROYECTO")
    ->setCellValue('C6' , "NOMBRE DEL PROYECTO")
    ->setCellValue('E6' , $nomo)
    //->setCellValue('C7' , "CODIGO DEL PROYECTO")
    //->setCellValue('E7' , $codproyecto)
    ->setCellValue('C7' , "DOCUMENTO Nro")
    ->setCellValue('E7' , $docproyecto)
    ->setCellValue('C8' , "FECHA INFORME")
    ->setCellValue('E8' , date('Y-m-d'))
    ->setCellValue('B10' , "1.2")
    ->setCellValue('C10' , "ESTADO DE AVANCE PROGRAMACIÓN")
    // ->setCellValue('B12' , "1.2.1")
    ->setCellValue('C12' , "PORCENTAJE DE AVANCE %")
    ->setCellValue('E12' , "AVANCE PLANEADO")
    ->setCellValue('E13' , $avplan."%")
    ->setCellValue('F12' , "AVANCE REAL")
    ->setCellValue('F13' , $avreal."%")
    ->setCellValue('G12' , "% ATRASO")
    ->setCellValue('G13' , $atraso."%")
    ->setCellValue('E15' , "Nota: Se detalla el estado de avance en el respectivo informe de programación")

    ->setCellValue('B17' , "1.3")
    ->setCellValue('C17' , "INFORMACIÓN PRESUPUESTAL")
    ->setCellValue('E19' , "FACTURACIÒN")
    ->setCellValue('C19' , "PLAN")
    ->setCellValue('D19' , "ACTUAL")
    ->setCellValue('E20' , "CAUSADO")
    ->setCellValue('F20' , "ANTICIPO")
    ->setCellValue('G19' , "DESVIACION PLAN VS. ACTUAL")
    ->setCellValue('H19' , "DISPONIBLE")
    ->setCellValue('I19' , "OBSERVACIÓN")

    ->mergeCells('B2:J2')
    ->mergeCells('B3:J3')
    ->mergeCells('B4:D4')
    ->mergeCells('E4:J4')
    ->mergeCells('C5:G5')
    ->mergeCells('B6:B8')
    ->mergeCells('C6:D6')
    ->mergeCells('E6:G6')
    ->mergeCells('C7:D7')
    ->mergeCells('E7:G7')
    ->mergeCells('C8:D8')
    ->mergeCells('E8:G8')
    //->mergeCells('C9:D9')
    //->mergeCells('E9:G9')
    ->mergeCells('H5:H8')
    ->mergeCells('B9:H9')
    ->mergeCells('C10:G10')
    ->mergeCells('B11:J11')
    ->mergeCells('C12:D12')
    ->mergeCells('B13:D13')
    ->mergeCells('B14:J14')
    ->mergeCells('B15:D15')
    ->mergeCells('E15:H15')
    ->mergeCells('B16:J16')
    ->mergeCells('C17:J17')
    ->mergeCells('B18:J18')
    ->mergeCells('B19:B20')
    ->mergeCells('C19:C20')
    ->mergeCells('E19:F19')

    ->mergeCells('D19:D20')
    ->mergeCells('G19:G20')
    ->mergeCells('H19:H20')
    ->mergeCells('B21:J21')
    ->mergeCells('I19:J20');
							   //COLORES DE CELDAS
								cellColor('B2:J2', 'C4BD97');
								cellColor('B4:D4', 'C4BD97');
								cellColor('B5:G5', 'C4BD97');
								cellColor('B10:G10', 'C4BD97');
								cellColor('B12:D12', 'C4BD97');
								cellColor('B17:J17', 'C4BD97');
								cellColor('C19:J20', 'C4BD97');
$objPHPExcel->getActiveSheet()->getCell('B12')->setValueExplicit("1.2.1",PHPExcel_Cell_DataType::TYPE_STRING);
								//BORDES DE CELDAS
								$objPHPExcel->getActiveSheet()->getStyle('B2:J2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle('B4:D4')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle('B5:G5')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								$objPHPExcel->getActiveSheet()->getStyle('C6:G8')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								$objPHPExcel->getActiveSheet()->getStyle('B10:G10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle('B12:G12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle('E13:G13')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle('E15:G15')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								$objPHPExcel->getActiveSheet()->getStyle('B17:J17')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle('B19:J20')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								//SIGNO PESO ABSOLUTO IZQUIERDA
								//$objPHPExcel->getActiveSheet()->getStyle('E13:F13')->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

								//TEXTO EN NEGRILLA
								$objPHPExcel->getActiveSheet()->getStyle("B2:J2")->getFont()->setBold(true)->setName('Calibri');
								$objPHPExcel->getActiveSheet()->getStyle("B4:D4")->getFont()->setBold(true)->setName('Calibri');
								$objPHPExcel->getActiveSheet()->getStyle("B5:G5")->getFont()->setBold(true)->setName('Calibri');
								$objPHPExcel->getActiveSheet()->getStyle("B10:G10")->getFont()->setBold(true)->setName('Calibri');
								$objPHPExcel->getActiveSheet()->getStyle("C12:G12")->getFont()->setBold(true)->setName('Calibri');
								$objPHPExcel->getActiveSheet()->getStyle("B17:J17")->getFont()->setBold(true)->setName('Calibri');
								$objPHPExcel->getActiveSheet()->getStyle("C19:J20")->getFont()->setBold(true)->setName('Calibri');

								//ALINEACION DE TEXTO HORIZONTAL
$objPHPExcel->getActiveSheet()->getStyle('E7:E8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
								$objPHPExcel->getActiveSheet()->getStyle('B2:H2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle('B5')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle('B10:G10')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle('B12:G12')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle('E13:G13')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle('B17:J17')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

								$objPHPExcel->getActiveSheet()->getStyle('C19:J20')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

								//ALINEACION DE TEXTO VERTICAL
								$objPHPExcel->getActiveSheet()->getStyle('B2:J2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								$objPHPExcel->getActiveSheet()->getStyle('C19:J20')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

								//TAMÑO DE FUENTE
								$objPHPExcel->getActiveSheet()->getStyle('A11:R12')->getFont()->setSize(10);
								//AJUSTE DE TEXTO
								$objPHPExcel->getActiveSheet()->getStyle('C19:J20')->getAlignment()->setWrapText(true);
								//ALTO DE FILAS
								$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(5.25);
								$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(5.25);
								$objPHPExcel->getActiveSheet()->getRowDimension('11')->setRowHeight(5.25);
								$objPHPExcel->getActiveSheet()->getRowDimension('14')->setRowHeight(5.25);
								$objPHPExcel->getActiveSheet()->getRowDimension('16')->setRowHeight(15);
								$objPHPExcel->getActiveSheet()->getRowDimension('18')->setRowHeight(5.25);
								$objPHPExcel->getActiveSheet()->getRowDimension('19')->setRowHeight(15);
								$objPHPExcel->getActiveSheet()->getRowDimension('20')->setRowHeight(15);
								$objPHPExcel->getActiveSheet()->getRowDimension('21')->setRowHeight(5.25);
								//ANCHO DE COLUMNAS
								$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5.15);
								$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(54.60);
								$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20.15);
								$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20.15);
								$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(23.15);
								$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(22.30);
								$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
								$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(19.15);
								$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(19.15);
								$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(19.15);

//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");

//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

//$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");

$conpre = mysql_query("select distinct g.gru_nombre nom,g.gru_clave_int cla from grupos g where g.est_clave_int = 1 and g.gru_categoria = 1 order by gru_orden");

$numpre = mysql_num_rows($conpre);
$hasta = $numpre + 22;

$acum = $hasta;
$filc = 22;
$totalvalor  = 0;
	for ($i = 22; $i < $hasta; $i++)
    {
        $datg = mysql_fetch_array($conpre);
        $gru = $datg['nom'];
        $idg = $datg['cla'];
        $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
        $datsum = mysql_fetch_array($consu);
        if($datsum['totc']=="" || $datsum['totc']==NULL){$plan=0;}else{$plan=$datsum['totc'];}
        if($datsum['totca']=="" || $datsum['totca']==NULL){$actu=0;}else{$actu=$datsum['totca'];}


        $concau = mysql_query("select sum(c.cpe_valor_neto+c.cpe_iva) as cau from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
        $datc = mysql_fetch_array($concau);
        if($datc['cau']=="" || $datc['cau']==NULL){$cau = 0;}else{$cau = $datc['cau'];}

        $conamo = mysql_query("select sum(c.cpe_anticipo - c.cpe_amortizacion) as amo from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
        $datam = mysql_fetch_array($conamo);
        if($datam['amo']=="" || $datam['amo']==NULL){$amo = 0;}else{$amo = $datam['amo'];}
        $sumam = $sumam + $amo;
        $sump = $sump + $plan;
        $suma = $suma + $actu;
        $sumc = $sumc + $cau;


        $desv = $plan - $actu;
        $sumd = $sumd + $desv;
        $disp = $actu - $cau;
        $sumdi = $sumdi + $disp;
        $cono  = mysql_query("select gpi_observacion from gru_pre_informe where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."' limit 1");
        $dato  = mysql_fetch_array($cono);
        $obse = $dato['gpi_observacion'];


        $objPHPExcel->getActiveSheet()->setCellValue('B' . $filc,str_replace("GRUPO ".$idg." - ","",$gru))
            ->setCellValue('C' . $filc, $plan)
            ->setCellValue('D' . $filc, $actu)
            ->setCellValue('E' . $filc, $cau)
            ->setCellValue('F' . $filc, $amo)
            ->setCellValue('G' . $filc, '=+C'.$filc.'-D'.$filc)
            ->setCellValue('H' . $filc, '=+D'.$filc.'-E'.$filc)
            ->setCellValue('I' . $filc, $obse)
            ->mergeCells('I'.$filc.':J'.$filc)
        ;
        cellColor('B'.$filc.':J'.$filc, 'FFFFFF');


        $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':J'.$filc)->getFont()->setSize(10)->setName('Calibri');
        //FORMATO DE  NUMERO
        //$objPHPExcel->getActiveSheet()->getStyle('C'.$filc.':H'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$filc.':H'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('C'.$filc.':H'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
        $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':J'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //FIN CONSULTAS

        $filc = $filc+ 1;

    }

$filc1 = $filc +1;
$filc2 = $filc + 1;
$filcs = $filc-1;
$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(5.25);
$objPHPExcel->getActiveSheet()
    ->setCellValue('B' . $filc2, 'SUB TOTAL CIVIL')
    ->setCellValue('C' . $filc2, '=SUM(C22:C'.$filcs.')')
    ->setCellValue('D' . $filc2, '=SUM(D22:D'.$filcs.')')
    ->setCellValue('E' . $filc2, '=SUM(E22:E'.$filcs.')')
    ->setCellValue('F' . $filc2, '=SUM(F22:F'.$filcs.')')
    ->setCellValue('G' . $filc2, '=SUM(G22:G'.$filcs.')')
    ->setCellValue('H' . $filc2, '=SUM(H22:H'.$filcs.')')
    ->mergeCells('I'.$filc2.':J'.$filc2)
;
		  cellColor('B' . $filc2.':J'.$filc2, 'FFFFFF');

		$objPHPExcel->getActiveSheet()->getStyle('C'.$filc2.':H'.$filc2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':J'.$filc2)->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':J'.$filc2)->getFont()->setBold(true)->setName('Calibri');
		//FORMATO DE  NUMERO
		//$objPHPExcel->getActiveSheet()->getStyle('C'.$filc2.':H'.$filc2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':H'.$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
    $objPHPExcel->getActiveSheet()->getStyle('B'.$filc2.':H'.$filc2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $fil = $filc2+1;
    $objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(5.25);
    $conpre = mysql_query("select distinct g.gru_nombre nom,g.gru_clave_int cla from grupos g where g.est_clave_int = 1 and g.gru_categoria = 2 order by gru_orden");

    $numpre = mysql_num_rows($conpre);
    $hasta = $numpre + $filc2+2;
    $fil2 = $filc +3;
    $acum = $hasta;
    $fila2 = $fil2;
    $totalvalor  = 0;
	for ($i = $fil2; $i < $hasta; $i++)
    {
        $datg = mysql_fetch_array($conpre);
        $gru = $datg['nom'];
        $idg = $datg['cla'];
        $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
        $datsum = mysql_fetch_array($consu);
        if($datsum['totc']=="" || $datsum['totc']==NULL){$plan=0;}else{$plan=$datsum['totc'];}
        if($datsum['totca']=="" || $datsum['totca']==NULL){$actu=0;}else{$actu=$datsum['totca'];}


        $concau = mysql_query("select sum(c.cpe_valor_neto+c.cpe_iva) as cau from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
        $datc = mysql_fetch_array($concau);
        if($datc['cau']=="" || $datc['cau']==NULL){$cau = 0;}else{$cau = $datc['cau'];}

        $conamo = mysql_query("select sum(c.cpe_amortizacion) as amo from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
        $datam = mysql_fetch_array($conamo);
        if($datam['amo']=="" || $datam['amo']==NULL){$amo = 0;}else{$amo = $datam['amo'];}

        $cono  = mysql_query("select gpi_observacion from gru_pre_informe where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."' limit 1");
        $dato  = mysql_fetch_array($cono);
        $obse = $dato['gpi_observacion'];


        $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila2, str_replace("GRUPO ".$idg." - ","",$gru))
            ->setCellValue('C' . $fila2, $plan)
            ->setCellValue('D' . $fila2, $actu)
            ->setCellValue('E' . $fila2, $cau)
            ->setCellValue('F' . $fila2, $amo)
            ->setCellValue('G' . $fila2, '=+C'.$fila2.'-D'.$fila2)
            ->setCellValue('H' . $fila2, '=+D'.$fila2.'-E'.$fila2)
            ->setCellValue('I' . $fila2, $obse)
            ->mergeCells('I'.$fila2.':J'.$fila2)
        ;
        cellColor('B' . $fila2.':I'.$fila2, 'FFFFFF');


        $objPHPExcel->getActiveSheet()->getStyle('B'.$fila2.':J'.$fila2)->getFont()->setSize(10)->setName('Calibri');
        //FORMATO DE  NUMERO
        //$objPHPExcel->getActiveSheet()->getStyle('C'.$fila2.':H'.$fila2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$fila2.':H'.$fila2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('C'.$fila2.':H'.$fila2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
        $objPHPExcel->getActiveSheet()->getStyle('B'.$fila2.':J'.$fila2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //FIN CONSULTAS
        $fila2 = $fila2+ 1;

    }
        $filac1 = $fila2 +1;
        $filac2 = $fila2 + 1;
        $filacs = $fila2-1;
        $objPHPExcel->getActiveSheet()->getRowDimension($fila2)->setRowHeight(5.25);
        $objPHPExcel->getActiveSheet()
            ->setCellValue('B' . $filac2, 'SUB TOTAL GASTOS GENERALES')
            ->setCellValue('C' . $filac2, '=SUM(C'.$fil2.':C'.$filacs.')')
            ->setCellValue('D' . $filac2, '=SUM(D'.$fil2.':D'.$filacs.')')
            ->setCellValue('E' . $filac2, '=SUM(E'.$fil2.':E'.$filacs.')')
            ->setCellValue('F' . $filac2, '=SUM(F'.$fil2.':F'.$filacs.')')
            ->setCellValue('G' . $filac2, '=SUM(G'.$fil2.':G'.$filacs.')')
            ->setCellValue('H' . $filac2, '=SUM(H'.$fil2.':H'.$filacs.')')
            ->mergeCells('I'.$filac2.':J'.$filac2)
        ;
		  cellColor('B' . $filac2.':J'.$filac2, 'FFFFFF');


		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac2.':J'.$filac2)->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac2.':J'.$filac2)->getFont()->setBold(true)->setName('Calibri');
		//FORMATO DE  NUMERO
		//$objPHPExcel->getActiveSheet()->getStyle('C'.$filac2.':H'.$filac2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filac2.':H'.$filac2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac2.':H'.$filac2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
        $objPHPExcel->getActiveSheet()->getStyle('B'.$filac2.':H'.$filac2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        $fild2 = $fila2+2;
        $objPHPExcel->getActiveSheet()->getRowDimension($fild2)->setRowHeight(5.25);
        $conpre = mysql_query("select distinct g.gru_nombre nom,g.gru_clave_int cla from grupos g where g.est_clave_int = 1 and g.gru_categoria = 3 order by gru_orden");

        $numpre = mysql_num_rows($conpre);
        $hasta = $numpre + $fila2+3;
        $fil3 = $fila2 +3;
        $fila3 = $fil3;
        $totalvalor  = 0;
		for ($i = $fil3; $i < $hasta; $i++)
        {
            $datg = mysql_fetch_array($conpre);
            $gru = $datg['nom'];
            $idg = $datg['cla'];
            $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datsum = mysql_fetch_array($consu);
            if($datsum['totc']=="" || $datsum['totc']==NULL){$plan=0;}else{$plan=$datsum['totc'];}
            if($datsum['totca']=="" || $datsum['totca']==NULL){$actu=0;}else{$actu=$datsum['totca'];}

            $concau = mysql_query("select sum(c.cpe_valor_neto+c.cpe_iva) as cau from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datc = mysql_fetch_array($concau);
            if($datc['cau']=="" || $datc['cau']==NULL){$cau = 0;}else{$cau = $datc['cau'];}

            $conamo = mysql_query("select sum(c.cpe_amortizacion) as amo from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datam = mysql_fetch_array($conamo);
            if($datam['amo']=="" || $datam['amo']==NULL){$amo = 0;}else{$amo = $datam['amo'];}

            $cono  = mysql_query("select gpi_observacion from gru_pre_informe where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."' limit 1");
            $dato  = mysql_fetch_array($cono);
            $obse = $dato['gpi_observacion'];


            $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila3, str_replace("GRUPO ".$idg." - ","",$gru))
                ->setCellValue('C' . $fila3, $plan)
                ->setCellValue('D' . $fila3, $actu)
                ->setCellValue('E' . $fila3, $cau)
                ->setCellValue('F' . $fila3, $amo)
                ->setCellValue('G' . $fila3, '=+C'.$fila3.'-D'.$fila3)
                ->setCellValue('H' . $fila3, '=+D'.$fila3.'-E'.$fila3)
                ->setCellValue('I' . $fila3, $obse)
                ->mergeCells('I'.$fila3.':J'.$fila3)
            ;
            cellColor('B' . $fila3.':I'.$fila3, 'FFFFFF');


            $objPHPExcel->getActiveSheet()->getStyle('B'.$fila3.':J'.$fila3)->getFont()->setSize(10)->setName('Calibri');
            //FORMATO DE  NUMERO
            //$objPHPExcel->getActiveSheet()->getStyle('C'.$fila3.':H'.$fila3)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$fila3.':H'.$fila3)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('C'.$fila3.':H'.$fila3)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
            $objPHPExcel->getActiveSheet()->getStyle('B'.$fila3.':J'.$fila3)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            //FIN CONSULTAS

            $fila3 = $fila3+ 1;

        }
$filac3 = $fila3 + 1;
$filacs2 = $fila3-1;
$objPHPExcel->getActiveSheet()->getRowDimension($fila3)->setRowHeight(5.25);
$objPHPExcel->getActiveSheet()
    ->setCellValue('B' . $filac3, 'SUB TOTAL EQUIPOS ESPECIALES')
    ->setCellValue('C' . $filac3, '=SUM(C'.$fil3.':C'.$filacs2.')')
    ->setCellValue('D' . $filac3, '=SUM(D'.$fil3.':D'.$filacs2.')')
    ->setCellValue('E' . $filac3, '=SUM(E'.$fil3.':E'.$filacs2.')')
    ->setCellValue('F' . $filac3, '=SUM(F'.$fil3.':F'.$filacs2.')')
    ->setCellValue('G' . $filac3, '=SUM(G'.$fil3.':G'.$filacs2.')')
    ->setCellValue('H' . $filac3, '=SUM(H'.$fil3.':H'.$filacs2.')')
    ->mergeCells('I'.$filac3.':J'.$filac3)
;
		  cellColor('B' . $filac3.':J'.$filac3, 'FFFFFF');


		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac3.':J'.$filac3)->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac3.':J'.$filac3)->getFont()->setBold(true)->setName('Calibri');
		//FORMATO DE  NUMERO
		//$objPHPExcel->getActiveSheet()->getStyle('C'.$filac3.':H'.$filac3)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filac3.':H'.$filac3)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac3.':H'.$filac3)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
			$objPHPExcel->getActiveSheet()->getStyle('B'.$filac3.':H'.$filac3)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$fila4 = $filac3 +1;
$filac4 = $fila4 + 1;
$filac5 = $fila4 + 2;
$filacs4 = $fila4-1;
$objPHPExcel->getActiveSheet()->getRowDimension($fila4)->setRowHeight(5.25);
$objPHPExcel->getActiveSheet()
    ->setCellValue('B' . $filac4, 'IMPREVISTO 3%')
    ->setCellValue('C' . $filac4, 0)
    ->setCellValue('D' . $filac4, 0)
    ->setCellValue('E' . $filac4, 0)
    ->setCellValue('F' . $filac4, 0)
    ->setCellValue('G' . $filac4, 0)
    ->setCellValue('H' . $filac4, 0)
    ->setCellValue('B' . $filac5, 'TOTAL OBRA CIVIL')
    ->setCellValue('C' . $filac5, '=+C'.$filc2.'+C'.$filac2.'+C'.$filac3.'+C'.$filac4)
    ->setCellValue('D' . $filac5, '=+D'.$filc2.'+D'.$filac2.'+D'.$filac3.'+D'.$filac4)
    ->setCellValue('E' . $filac5, '=+E'.$filc2.'+E'.$filac2.'+E'.$filac3.'+E'.$filac4)
    ->setCellValue('F' . $filac5, '=+F'.$filc2.'+F'.$filac2.'+F'.$filac3.'+F'.$filac4)
    ->setCellValue('G' . $filac5, '=+G'.$filc2.'+G'.$filac2.'+G'.$filac3.'+G'.$filac4)
    ->setCellValue('H' . $filac5, '=+H'.$filc2.'+H'.$filac2.'+H'.$filac3.'+H'.$filac4)
;
		  cellColor('B' . $filac4.':H'.$filac4, 'FFFFFF');
   		$objPHPExcel->getActiveSheet()->getRowDimension($filac4)->setVisible(false);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac4.':I'.$filac5)->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac4.':I'.$filac5)->getFont()->setBold(true)->setName('Calibri');
		//FORMATO DE  NUMERO
		//$objPHPExcel->getActiveSheet()->getStyle('C'.$filac4.':H'.$filac5)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filac4.':H'.$filac5)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac4.':H'.$filac5)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filac4.':H'.$filac5)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$fil = $filac5+1;
	$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(5.25);
	$conpre = mysql_query("select distinct g.gru_nombre nom,g.gru_clave_int cla from grupos g where g.est_clave_int = 1 and g.gru_categoria = 4 order by gru_orden");

$numpre = mysql_num_rows($conpre);
$hasta = $numpre + $filac5+2;

$acum = $hasta;
$fila6 = $filac5+2;
$fila6b = $filac5+2;

$totalvalor  = 0;
		for ($i = $fila6b; $i < $hasta; $i++)
        {
            $datg = mysql_fetch_array($conpre);
            $gru = $datg['nom'];
            $idg = $datg['cla'];
            $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datsum = mysql_fetch_array($consu);
            if($datsum['totc']=="" || $datsum['totc']==NULL){$plan=0;}else{$plan=$datsum['totc'];}
            if($datsum['totca']=="" || $datsum['totca']==NULL){$actu=0;}else{$actu=$datsum['totca'];}



            $concau = mysql_query("select sum(c.cpe_valor_neto+c.cpe_iva) as cau from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datc = mysql_fetch_array($concau);
            if($datc['cau']=="" || $datc['cau']==NULL){$cau = 0;}else{$cau = $datc['cau'];}

            $conamo = mysql_query("select sum(c.cpe_amortizacion) as amo from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datam = mysql_fetch_array($conamo);
            if($datam['amo']=="" || $datam['amo']==NULL){$amo = 0;}else{$amo = $datam['amo'];}


            $cono  = mysql_query("select gpi_observacion from gru_pre_informe where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."' limit 1");
            $dato  = mysql_fetch_array($cono);
            $obse = $dato['gpi_observacion'];


            $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila6, str_replace("GRUPO ".$idg." - ","",$gru))
                ->setCellValue('C' . $fila6, $plan)
                ->setCellValue('D' . $fila6, $actu)
                ->setCellValue('E' . $fila6, $cau)
                ->setCellValue('F' . $fila6, $amo)
                ->setCellValue('G' . $fila6, '=+C'.$fila6.'-D'.$fila6)
                ->setCellValue('H' . $fila6, '=+D'.$fila6.'-E'.$fila6)
                ->setCellValue('I' . $fila6, $obse)
                ->mergeCells('I'.$fila6.':J'.$fila6)
            ;
            cellColor('B' . $fila6.':J'.$fila6, 'FFFFFF');


            $objPHPExcel->getActiveSheet()->getStyle('B'.$fila6.':J'.$fila6)->getFont()->setSize(10)->setName('Calibri');
            //FORMATO DE  NUMERO
            //$objPHPExcel->getActiveSheet()->getStyle('C'.$fila6.':H'.$fila6)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$fila6.':H'.$fila6)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('C'.$fila6.':H'.$fila6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
            $objPHPExcel->getActiveSheet()->getStyle('B'.$fila6.':J'.$fila6)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //FIN CONSULTAS

            $fila6 = $fila6+ 1;

        }
$filc6 = $fila6 + 1;
$filcs6 = $fila6-1;
$objPHPExcel->getActiveSheet()->getRowDimension($fila6)->setRowHeight(5.25);
$objPHPExcel->getActiveSheet()
    ->setCellValue('B' . $filc6, 'SUB TOTAL INSTALACIONES ESPECIALES')
    ->setCellValue('C' . $filc6, '=SUM(C'.$fila6b.':C'.$filcs6.')')
    ->setCellValue('D' . $filc6, '=SUM(D'.$fila6b.':D'.$filcs6.')')
    ->setCellValue('E' . $filc6, '=SUM(E'.$fila6b.':E'.$filcs6.')')
    ->setCellValue('F' . $filc6, '=SUM(F'.$fila6b.':F'.$filcs6.')')
    ->setCellValue('G' . $filc6, '=SUM(G'.$fila6b.':G'.$filcs6.')')
    ->setCellValue('H' . $filc6, '=SUM(H'.$fila6b.':H'.$filcs6.')')
    ->mergeCells('I'.$filc6.':J'.$filc6)
;
		  cellColor('B' . $filc6.':J'.$filc6, 'FFFFFF');


		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc6.':J'.$filc6)->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc6.':J'.$filc6)->getFont()->setBold(true)->setName('Calibri');
		//FORMATO DE  NUMERO
		//$objPHPExcel->getActiveSheet()->getStyle('C'.$filc6.':H'.$filc6)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filc6.':H'.$filc6)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc6.':H'.$filc6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
			$objPHPExcel->getActiveSheet()->getStyle('B'.$filc6.':H'.$filc6)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			//INICIO

			$fil = $filc6+1;
	$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(5.25);


//EVENTOS DE CAMBI0
if($apli==0)
{
    //SUBANALISIS
    $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
        ",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
        " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");

    $datsu = mysql_fetch_array($consu);
    $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
    $totals = $totals + ($totads+$totims+$totuts+$totivs);

    //consulta del total de la suma de las actividades asignadas a este capitulo
    $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
        ",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
        " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
    $datsu = mysql_fetch_array($consu);
    $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
    $plan = $totale + ($totad+$totim+$totut+$totiv) + ($totals);

    $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
        ",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
        " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
    $datsu = mysql_fetch_array($consu);
    $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
    $totals = $totals + ($totads+$totims+$totuts+$totivs);

    $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
        ",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
        " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
    $datsu = mysql_fetch_array($consu);
    $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totivae = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
    $actu = $totalea + ($totada+$totima+$totuta+$totivae) + ($totals);
}
else
{
    //SUBANALISIS
    $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
        ",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
        "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
        "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
        " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");

    $datsu = mysql_fetch_array($consu);
    $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
    $totals = $totals + ($totads+$totims+$totuts+$totivs);

    //consulta del total de la suma de las actividades asignadas a este capitulo
    $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
        ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
        ",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
        "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
        "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
        " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
    $datsu = mysql_fetch_array($consu);
    $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
    $plan = $totale + ($totad+$totim+$totut+$totiv) + ($totals);

    $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
        ",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
        "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
        "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
        " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
    $datsu = mysql_fetch_array($consu);
    $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
    $totals = $totals + ($totads+$totims+$totuts+$totivs);

    $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".

        ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
        ",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
        "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
        "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
        " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
    $datsu = mysql_fetch_array($consu);
    $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totivae = $datsu['totiv'];
    if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
    $actu = $totalea + ($totada+$totima+$totuta+$totivae) + ($totals);
}
$cau = 0;
$amo = 0;
	//$conpre = mysql_query("select distinct g.gru_nombre nom,g.gru_clave_int cla from grupos g where g.est_clave_int = 1 and g.gru_categoria = 5 order by gru_orden");

$numpre = mysql_num_rows($conpre);
$hasta = $numpre + $filc6+2;

$acum = $hasta;
$fila7 = $filc6+2;
$fila7b = $filc6+2;

$totalvalor  = 0;
	/*	for ($i = $fila7b; $i < $hasta; $i++)
        {
            $datg = mysql_fetch_array($conpre);
            $gru = $datg['nom'];
            $idg = $datg['cla'];
            $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datsum = mysql_fetch_array($consu);
            if($datsum['totc']=="" || $datsum['totc']==NULL){$plan=0;}else{$plan=$datsum['totc'];}
            if($datsum['totca']=="" || $datsum['totca']==NULL){$actu=0;}else{$actu=$datsum['totca'];}


            $concau = mysql_query("select sum(c.cpe_valor_neto+c.cpe_iva) as cau from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datc = mysql_fetch_array($concau);
            if($datc['cau']=="" || $datc['cau']==NULL){$cau = 0;}else{$cau = $datc['cau'];}

            $conamo = mysql_query("select sum(c.cpe_amortizacion) as amo from control_egreso c where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idg."'");
            $datam = mysql_fetch_array($conamo);
            if($datam['amo']=="" || $datam['amo']==NULL){$amo = 0;}else{$amo = $datam['amo'];}
*/

            $cono  = mysql_query("select gpi_observacion from gru_pre_informe where pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '31' limit 1");
            $dato  = mysql_fetch_array($cono);
            $obse = $dato['gpi_observacion'];


            $objPHPExcel->getActiveSheet()->setCellValue('B' . $fila7, "EVENTOS DE CAMBIO")
                ->setCellValue('C' . $fila7, $plan)
                ->setCellValue('D' . $fila7, $actu)
                ->setCellValue('E' . $fila7, $cau)
                ->setCellValue('F' . $fila7, $amo)
                ->setCellValue('G' . $fila7, '=+C'.$fila7.'-D'.$fila7)
                ->setCellValue('H' . $fila7, '=+D'.$fila7.'-E'.$fila7)
                ->setCellValue('I' . $fila7, $obse)
                ->mergeCells('I'.$fila7.':J'.$fila7)
            ;
            cellColor('B' . $fila7.':J'.$fila7, 'FFFFFF');


            $objPHPExcel->getActiveSheet()->getStyle('B'.$fila7.':J'.$fila7)->getFont()->setSize(10)->setName('Calibri');
            //FORMATO DE  NUMERO
            //$objPHPExcel->getActiveSheet()->getStyle('C'.$fila7.':H'.$fila7)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$fila7.':H'.$fila7)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('C'.$fila7.':H'.$fila7)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
            $objPHPExcel->getActiveSheet()->getStyle('B'.$fila7.':J'.$fila7)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            //FIN CONSULTAS

        //    $fila7 = $fila7+ 1;

      //  }
$filc7 = $fila7 + 1;
$filcs7 = $fila7-1;
$objPHPExcel->getActiveSheet()->getRowDimension($fila7)->setRowHeight(5.25);
$objPHPExcel->getActiveSheet()
    ->setCellValue('B' . $filc7, 'SUB TOTAL EVENTOS DE CAMBIO')
    ->setCellValue('C' . $filc7, '=SUM(C'.$fila7b.':C'.$filcs7.')')
    ->setCellValue('D' . $filc7, '=SUM(D'.$fila7b.':D'.$filcs7.')')
    ->setCellValue('E' . $filc7, '=SUM(E'.$fila7b.':E'.$filcs7.')')
    ->setCellValue('F' . $filc7, '=SUM(F'.$fila7b.':F'.$filcs7.')')
    ->setCellValue('G' . $filc7, '=SUM(G'.$fila7b.':G'.$filcs7.')')
    ->setCellValue('H' . $filc7, '=SUM(H'.$fila7b.':H'.$filcs7.')')
    ->mergeCells('I'.$filc7.':J'.$filc7)
;
		  cellColor('B' . $filc7.':J'.$filc7, 'FFFFFF');


		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc7.':J'.$filc7)->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc7.':J'.$filc7)->getFont()->setBold(true)->setName('Calibri');
		//FORMATO DE  NUMERO
		//$objPHPExcel->getActiveSheet()->getStyle('C'.$filc7.':H'.$filc7)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filc7.':H'.$filc7)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$filc7.':H'.$filc7)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
			$objPHPExcel->getActiveSheet()->getStyle('B'.$filc7.':H'.$filc7)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			//FIN


$fil = $filc7+1;
$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(5.25);

$fil5 = $filc7+2;
$fil6 = $filc7+3;
$fil7 = $filc7+4;
$fil8 = $filc7+5;
$fil9 = $filc7+6;
$fil10 = $filc7+7;
$fil11 = $filc7+8;
$fil12 = $filc7+9;
$fil13 = $filc7+10;

$fil14 = $filc7+11;
$fil15 = $filc7+12;
$fil16 = $filc7+13;
$fil17 = $filc7+14;
$fil18 = $filc7+15;
$fil19 = $filc7+16;
$fil20 = $filc7+17;
$fil21 = $filc7+18;
$fil22 = $filc7+19;
$fil23 = $filc7+20;



$objPHPExcel->getActiveSheet()

    ->setCellValue('B' . $fil6, 'ADMINISTRACION')
    ->setCellValue('C' . $fil6, $totadm)
    ->setCellValue('D' . $fil6, $totadma)
    ->setCellValue('E' . $fil6, 0)
    ->setCellValue('F' . $fil6, 0)
    ->setCellValue('G' . $fil6, '=+C'.$fil6.'-D'.$fil6)
    ->setCellValue('H' . $fil6, '=+D'.$fil6.'-E'.$fil6)

    ->setCellValue('B' . $fil7, 'IMPREVISTOS')
    ->setCellValue('C' . $fil7, $totimp)
    ->setCellValue('D' . $fil7, $totimpa)
    ->setCellValue('E' . $fil7, 0)
    ->setCellValue('F' . $fil7, 0)
    ->setCellValue('G' . $fil7, '=+C'.$fil7.'-D'.$fil7)
    ->setCellValue('H' . $fil7, '=+D'.$fil7.'-E'.$fil7)

    ->setCellValue('B' . $fil8, 'UTILIDAD')
    ->setCellValue('C' . $fil8, $totuti)
    ->setCellValue('D' . $fil8, $totutia)
    ->setCellValue('E' . $fil8, 0)
    ->setCellValue('F' . $fil8, 0)
    ->setCellValue('G' . $fil8, '=+C'.$fil8.'-D'.$fil8)
    ->setCellValue('H' . $fil8, '=+D'.$fil8.'-E'.$fil8)

    ->setCellValue('B' . $fil9, 'IVA')
    ->setCellValue('C' . $fil9, $totiva)
    ->setCellValue('D' . $fil9, $totivaa)
    ->setCellValue('E' . $fil9, 0)
    ->setCellValue('F' . $fil9, 0)
    ->setCellValue('G' . $fil9, '=+C'.$fil9.'-D'.$fil9)
    ->setCellValue('H' . $fil9, '=+D'.$fil9.'-E'.$fil9)

    ->setCellValue('B' . $fil11, 'SUB TOTAL INDIRECTOS OBRA')
    ->setCellValue('C' . $fil11, '=SUM(C'.$fil6.':C'.$fil9.')')
    ->setCellValue('D' . $fil11, '=SUM(D'.$fil6.':D'.$fil9.')')
    ->setCellValue('E' . $fil11, '=SUM(E'.$fil6.':E'.$fil9.')')
    ->setCellValue('F' . $fil11, '=SUM(F'.$fil6.':F'.$fil9.')')
    ->setCellValue('G' . $fil11, '=SUM(G'.$fil6.':G'.$fil9.')')
    ->setCellValue('H' . $fil11, '=SUM(H'.$fil6.':H'.$fil9.')')
    ->setCellValue('B' . $fil13, 'TOTAL CONTRUCCION PROYECTO')
    ->setCellValue('C' . $fil13, '=+C'.$filac5.'+C'.$filc6.'+C'.$filc7.'+C'.$fil11)
    ->setCellValue('D' . $fil13, '=+D'.$filac5.'+D'.$filc6.'+D'.$filc7.'+D'.$fil11)
    ->setCellValue('E' . $fil13, '=+E'.$filac5.'+E'.$filc6.'+E'.$filc7.'+E'.$fil11)
    ->setCellValue('F' . $fil13, '=+F'.$filac5.'+F'.$filc6.'+F'.$filc7.'+F'.$fil11)
    ->setCellValue('G' . $fil13, '=+G'.$filac5.'+G'.$filc6.'+G'.$filc7.'+G'.$fil11)
    ->setCellValue('H' . $fil13, '=+H'.$filac5.'+H'.$filc6.'+H'.$filc7.'+H'.$fil11)

    ->setCellValue('B' . $fil15, '1.4')
    ->setCellValue('C' . $fil15, 'INFORMACIÓN GENERAL DEL PROYECTO')
    ->setCellValue('B' . $fil17, 'DESCRIPCIÓN')
    ->setCellValue('C' . $fil17, 'PLAN')
    ->setCellValue('D' . $fil17, 'REAL')
    ->setCellValue('E' . $fil17, 'COMPROMETIDO')
    ->setCellValue('F' . $fil17, 'ASIGNADO')
    ->setCellValue('G' . $fil17, 'DISPONIBILIDAD')
    ->setCellValue('H' . $fil17, 'PRESUPUESTO VALIDADO')
    ->setCellValue('I' . $fil17, 'PRESUPUESTO ACTUALIZADO')
    ->setCellValue('J' . $fil17, 'DESVIACIÓN')
    ->mergeCells('C'.$fil15.':J'.$fil15);

$cong = mysql_query("select igp_clave_int,pre_clave_int,igp_descripcion,igp_plan,igp_real,igp_comprometida from inf_gen_proyecto where pre_clave_int = '".$idpresupuesto."'");
$numg = mysql_num_rows($cong); if($numg<=0){$numg=1;}
$igp = 0;
$top = 0;
$tor = 0;
$toc = 0;
$toa = 0;
$tod = 0;
$tov = 0;
$tova = 0;
$toac = 0;
$tode = 0;
$filinicial = $fil18;
for($ej=0;$ej<$numg;$ej++) {
    $datg = mysql_fetch_array($cong);
    $igp = $datg['igp_clave_int'];
    if ($igp == "" || $igd == "null") {
        $igp = 0;
    }
    $descr = $datg['igp_descripcion'];
    $plan = $datg['igp_plan'];
    $real = $datg['igp_real'];
    $comprometido = $datg['igp_comprometida'];
    $asignado = $real + $comprometido;
    $disponible = $plan - $real;

    $top = $top + $plan;
    $tor = $tor + $real;
    $toc = $toc + $comprometido;
    $toa = $toa + $asignado;
    $tod = $tod + $disponible;
     /*
    if ($apli == 0) {
        $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut" .
            ",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv" .
            " from  pre_gru_cap_act_sub_insumo pa join inf_gen_validado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totads = $datsu['totad'];
        $totims = $datsu['totim'];
        $totuts = $datsu['totut'];
        $totivs = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $totals = 0;
        } else {
            $totals = $datsu['tot'];
        }
        $totals = $totals + ($totads + $totims + $totuts + $totivs);
        //consulta del total de la suma de las actividades asignadas a este capitulo
        $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut" .
            ",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv" .
            " from  pre_gru_cap_act_insumo pa join inf_gen_validado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totad = $datsu['totad'];
        $totim = $datsu['totim'];
        $totut = $datsu['totut'];
        $totiv = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $total = 0;
        } else {
            $total = $datsu['tot'];
        }
        $total = $total + ($totad + $totim + $totut + $totiv) + ($totals);

        $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut" .
            ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv" .
            " from  pre_gru_cap_act_sub_insumo pa join inf_gen_actualizado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totads = $datsu['totad'];
        $totims = $datsu['totim'];
        $totuts = $datsu['totut'];
        $totivs = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $totals = 0;
        } else {
            $totals = $datsu['tot'];
        }
        $totals = $totals + ($totads + $totims + $totuts + $totivs);
        //consulta del total de la suma de las actividades asignadas a este capitulo
        $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut" .
            ",(sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv" .
            " from  pre_gru_cap_act_insumo pa join inf_gen_actualizado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totad = $datsu['totad'];
        $totim = $datsu['totim'];
        $totut = $datsu['totut'];
        $totiv = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $totala = 0;
        } else {
            $totala = $datsu['tot'];
        }
        $totala = $totala + ($totad + $totim + $totut + $totiv) + ($totals);
    } else {
        $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut" .
            ",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+" .
            "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+" .
            "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv" .
            " from  pre_gru_cap_act_sub_insumo pa join inf_gen_validado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totads = $datsu['totad'];
        $totims = $datsu['totim'];
        $totuts = $datsu['totut'];
        $totivs = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $totals = 0;
        } else {
            $totals = $datsu['tot'];
        }
        $totals = $totals + ($totads + $totims + $totuts + $totivs);
        //consulta del total de la suma de las actividades asignadas a este capitulo
        $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim" .
            ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut" .
            ",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+" .
            "((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+" .
            "((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv" .
            " from  pre_gru_cap_act_insumo pa join inf_gen_validado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totad = $datsu['totad'];
        $totim = $datsu['totim'];
        $totut = $datsu['totut'];
        $totiv = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $total = 0;
        } else {
            $total = $datsu['tot'];
        }

        $total = $total + ($totad + $totim + $totut + $totiv) + ($totals);

        $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act) as tot" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut" .
            ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+" .
            "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+" .
            "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv" .
            " from  pre_gru_cap_act_sub_insumo pa join inf_gen_actualizado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totads = $datsu['totad'];
        $totims = $datsu['totim'];
        $totuts = $datsu['totut'];
        $totivs = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $totals = 0;
        } else {
            $totals = $datsu['tot'];
        }
        $totals = $totals + ($totads + $totims + $totuts + $totivs);
        //consulta del total de la suma de las actividades asignadas a este capitulo
        $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act) as tot" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim" .
            ",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut" .
            ",(sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+" .
            "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+" .
            "((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv" .
            " from  pre_gru_cap_act_insumo pa join inf_gen_actualizado i on pa.pre_clave_int = i.pre_clave_int and pa.gru_clave_int = i.gru_clave_int and pa.cap_clave_int = i.cap_clave_int and pa.act_clave_int  = i.act_clave_int where pa.pre_clave_int  = '" . $idpresupuesto . "' and i.igp_clave_int  = '" . $igp . "'");
        $datsu = mysql_fetch_array($consu);
        $totad = $datsu['totad'];
        $totim = $datsu['totim'];
        $totut = $datsu['totut'];
        $totiv = $datsu['totiv'];
        if ($datsu['tot'] == "" || $datsu['tot'] == NULL) {
            $totala = 0;
        } else {
            $totala = $datsu['tot'];
        }
        $totala = $totala + ($totad + $totim + $totut + $totiv) + ($totals);
    }
    $validado = $total;
    $actualizado = $totala;
    */
    $consu = mysql_query("SELECT sum(pgca_valor_act) validado FROM pre_gru_cap_actividad pa JOIN inf_gen_validado i ON pa.pre_clave_int = i.pre_clave_int AND pa.gru_clave_int = i.gru_clave_int AND pa.cap_clave_int = i.cap_clave_int AND pa.act_clave_int = i.act_clave_int WHERE pa.pre_clave_int  = '".$idpresupuesto."' and i.igp_clave_int  = '".$igp."'");
    $datsu = mysql_fetch_array($consu);
    $validado = $datsu['validado']; if($validado=="" || $validado==NULL){$validado = 0;}

    $consu = mysql_query("SELECT sum(pgca_valor_acta) actualizado FROM pre_gru_cap_actividad pa JOIN inf_gen_actualizado i ON pa.pre_clave_int = i.pre_clave_int AND pa.gru_clave_int = i.gru_clave_int AND pa.cap_clave_int = i.cap_clave_int AND pa.act_clave_int = i.act_clave_int WHERE pa.pre_clave_int  = '".$idpresupuesto."' and i.igp_clave_int  = '".$igp."'");
    $datsu = mysql_fetch_array($consu);
    $actualizado = $datsu['actualizado']; if($actualizado=="" || $actualizado==NULL){$actualizado = 0;}


    $toac = $toac + $actualizado;
    $tova = $tova + $validado;

    $con2 = mysql_query("select igp_adm_ini from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "' and igp_adm_ini = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $tova = $tova + $totadm;
        $validado = $validado + $totadm;
    }
    $con2 = mysql_query("select igp_imp_ini from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "' and igp_imp_ini = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $tova = $tova + $totimp;
        $validado = $validado + $totimp;
    }
    $con2 = mysql_query("select igp_uti_ini from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "' and igp_uti_ini = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $tova = $tova + $totuti;
        $validado = $validado + $totuti;
    }
    $con2 = mysql_query("select igp_iva_ini from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "'  and igp_iva_ini = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $tova = $tova + $totiva;
        $validado = $validado + $totiva;
    }

    $con2 = mysql_query("select igp_adm_act from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "' and igp_adm_act = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $toac = $toac + $totadma;
        $actualizado = $actualizado + $totadma;
    }
    $con2 = mysql_query("select igp_imp_act from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "'  and igp_imp_act = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $toac = $toac + $totimpa;
        $actualizado = $actualizado + $totimpa;
    }
    $con2 = mysql_query("select igp_uti_act from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "'  and igp_uti_act = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $toac = $toac + $totutia;
        $actualizado = $actualizado + $totutia;
    }
    $con2 = mysql_query("select igp_iva_act from inf_gen_proyecto where pre_clave_int = '" . $idpresupuesto . "' and igp_clave_int ='" . $igp . "' and igp_iva_act = '1' limit 1");
    $num2 = mysql_num_rows($con2);
    if ($num2 > 0) {
        $toac = $toac + $totivaa;
        $actualizado = $actualizado + $totivaa;
    }
    $desviacion = $validado - $actualizado;
    $objPHPExcel->getActiveSheet()
        ->setCellValue('B' . $fil18,$descr)
        ->setCellValue('C' . $fil18, $plan)
        ->setCellValue('D' . $fil18, $real)
        ->setCellValue('E' . $fil18, $comprometido)
        ->setCellValue('F' . $fil18, '=+D'.$fil18.'+E'.$fil18)
        ->setCellValue('G' . $fil18, '=+C'.$fil18.'-F'.$fil18)
        ->setCellValue('H' . $fil18, $validado)
        ->setCellValue('I' . $fil18, $actualizado)
        ->setCellValue('J' . $fil18, '=+H'.$fil18.'-I'.$fil18);
    $fil18++;
}
$fil23 = $fil18;
$filfinal = $fil18 - 1;
$objPHPExcel->getActiveSheet()
    ->setCellValue('B' . $fil23, 'TOTALES')
    ->setCellValue('C' . $fil23, '=SUM(C'.$filinicial.':C'.$filfinal.')')
    ->setCellValue('D' . $fil23, '=SUM(D'.$filinicial.':D'.$filfinal.')')
    ->setCellValue('E' . $fil23, '=SUM(E'.$filinicial.':E'.$filfinal.')')
    ->setCellValue('F' . $fil23, '=SUM(F'.$filinicial.':F'.$filfinal.')')
    ->setCellValue('G' . $fil23, '=SUM(G'.$filinicial.':G'.$filfinal.')')
    ->setCellValue('H' . $fil23, '=SUM(H'.$filinicial.':H'.$filfinal.')')
    ->setCellValue('I' . $fil23, '=SUM(I'.$filinicial.':I'.$filfinal.')')
    ->setCellValue('J' . $fil23, '=SUM(J'.$filinicial.':J'.$filfinal.')');

/*

		->setCellValue('B' . $fil18, 'COSTOS DIRECTOS')
		->setCellValue('C' . $fil18, '=+C'.$filc2.'+C'.$filac2)
		->setCellValue('D' . $fil18, $cdreal)
		->setCellValue('E' . $fil18, $cdcomprometido)
		->setCellValue('F' . $fil18, '=+D'.$fil18.'+E'.$fil18)
		->setCellValue('G' . $fil18, '=+C'.$fil18.'-D'.$fil18)
		->setCellValue('H' . $fil18, '=C'.$fil18)
		->setCellValue('I' . $fil18, '=+D'.$filc2.'+D'.$filac2)
		->setCellValue('J' . $fil18, '=+H'.$fil18.'-I'.$fil18)

		->setCellValue('B' . $fil19, 'EQUIPOS ESPECIALES')
		->setCellValue('C' . $fil19, '=C'.$filac3)
		->setCellValue('D' . $fil19, $eereal)
		->setCellValue('E' . $fil19, $eecomprometido)
		->setCellValue('F' . $fil19, '=+D'.$fil19.'+E'.$fil19)
		->setCellValue('G' . $fil19, '=+C'.$fil19.'-D'.$fil19)
		->setCellValue('H' . $fil19, '=C'.$fil19)
		->setCellValue('I' . $fil19, '=D'.$filac3)
		->setCellValue('J' . $fil19, '=+H'.$fil19.'-I'.$fil19)

		->setCellValue('B' . $fil20, 'INSTALACIONES ESPECIALES')
		->setCellValue('C' . $fil20, '=C'.$filc6)
		->setCellValue('D' . $fil20, $iereal)
		->setCellValue('E' . $fil20, $iecomprometido)
		->setCellValue('F' . $fil20, '=+D'.$fil20.'+E'.$fil20)
		->setCellValue('G' . $fil20, '=+C'.$fil20.'-D'.$fil20)
		->setCellValue('H' . $fil20, '=C'.$fil20)
		->setCellValue('I' . $fil20, '=D'.$filc6)
		->setCellValue('J' . $fil20, '=+H'.$fil20.'-I'.$fil20)

		->setCellValue('B' . $fil21, 'EVENTOS DE CAMBIOS')
		->setCellValue('C' . $fil21, '=C'.$filc7)
		->setCellValue('D' . $fil21, $ecreal)
		->setCellValue('E' . $fil21, $eccomprometido)
		->setCellValue('F' . $fil21, '=+D'.$fil21.'+E'.$fil21)
		->setCellValue('G' . $fil21, '=+C'.$fil21.'-D'.$fil21)
		->setCellValue('H' . $fil21, '=C'.$fil21)
		->setCellValue('I' . $fil21, '=D'.$filc7)
		->setCellValue('J' . $fil21, '=+H'.$fil21.'-I'.$fil21)

		->setCellValue('B' . $fil23, 'TOTALES')
		->setCellValue('C' . $fil23, '=SUM(C'.$fil18.':C'.$fil21.')')
		->setCellValue('D' . $fil23, '=SUM(D'.$fil18.':D'.$fil21.')')
		->setCellValue('E' . $fil23, '=SUM(E'.$fil18.':E'.$fil21.')')
		->setCellValue('F' . $fil23, '=SUM(F'.$fil18.':F'.$fil21.')')
		->setCellValue('G' . $fil23, '=SUM(G'.$fil18.':G'.$fil21.')')
		->setCellValue('H' . $fil23, '=SUM(H'.$fil18.':H'.$fil21.')')
		->setCellValue('I' . $fil23, '=SUM(I'.$fil18.':I'.$fil21.')')
		->setCellValue('J' . $fil23, '=SUM(J'.$fil18.':J'.$fil21.')')
		 ->mergeCells('C'.$fil15.':J'.$fil15)
		 ;	*/
		  cellColor('B' . $fil5.':H'.$fil12, 'FFFFFF');
		  cellColor('B' . $fil13.':G'.$fil13, 'C4BD97');
		  cellColor('B' . $fil15.':J'.$fil15, 'C4BD97');
		  cellColor('B' . $fil17.':J'.$fil17, 'C4BD97');
		   cellColor('B' . $fil23.':J'.$fil23, 'C4BD97');


		$objPHPExcel->getActiveSheet()->getRowDimension($fil5)->setRowHeight(5.25);
		$objPHPExcel->getActiveSheet()->getRowDimension($fil10)->setRowHeight(5.25);
		$objPHPExcel->getActiveSheet()->getRowDimension($fil12)->setRowHeight(5.25);
		$objPHPExcel->getActiveSheet()->getRowDimension($fil14)->setRowHeight(15);
		$objPHPExcel->getActiveSheet()->getRowDimension($fil16)->setRowHeight(5.25);
		$objPHPExcel->getActiveSheet()->getRowDimension($fil17)->setRowHeight(30);
		//$objPHPExcel->getActiveSheet()->getRowDimension($fil22)->setRowHeight(5.25);


		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil5.':H'.$fil13)->getFont()->setSize(10)->setName('Calibri');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil11.':H'.$fil11)->getFont()->setBold(true)->setName('Calibri');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil13.':H'.$fil13)->getFont()->setBold(true)->setName('Calibri');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil15.':J'.$fil15)->getFont()->setBold(true)->setName('Calibri');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil17.':J'.$fil17)->getFont()->setBold(true)->setName('Calibri');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil23.':J'.$fil23)->getFont()->setBold(true)->setName('Calibri');

		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil17.':J'.$fil17)->getAlignment()->setWrapText(true);

		//FORMATO DE  NUMERO
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil5.':G'.$fil13)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil15.':J'.$fil15)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil17.':J'.$fil17)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil17.':J'.$fil17)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$filinicial.':J'.$fil23)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$fil23)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

			$objPHPExcel->getActiveSheet()->getStyle('B'.$fil6.':H'.$fil9)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$fil11.':H'.$fil11)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$fil13.':H'.$fil13)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$fil15.':J'.$fil15)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

				$objPHPExcel->getActiveSheet()->getStyle('B'.$fil17.':J'.$filfinal)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

				$objPHPExcel->getActiveSheet()->getStyle('B'.$fil23.':J'.$fil23)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			//$objPHPExcel->getActiveSheet()->getStyle('C'.$fil6.':H'.$fil13)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$fil6.':H'.$fil13)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

			//$objPHPExcel->getActiveSheet()->getStyle('C'.$fil17.':J'.$fil23)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$fil17.':j'.$fil23)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');



$objPHPExcel->getActiveSheet()->getComment('E12')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('E12')->getText()->createTextRun('Resultado del ultimo corte de programación')->getFont()->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('F12')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('F12')->getText()->createTextRun('Resultado del ultimo corte de programación')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('C19')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('C19')->getText()->createTextRun('De la hoja "Alcance Presupuestado" vincular el subotal de las columnas "Presupuesto Inicial" de cada capítulo')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('D19')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('D19')->getText()->createTextRun('De la hoja "alcance presupuestado" vincular el subtotal de la columna "presupuesto actualizado" de  cada capítulo.')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('E19')->setAuthor('Linea Global');
$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('E19')->getText()->createTextRun('Las celdas ya están formuladas de modo que reflejen la sumatoria de las facturas relacionadas a cada capítulo en el Control Presupuestal por Egreso.');
$objCommentRichText->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('E19')->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()->getComment('E19')->getText()->createTextRun("FORMULA:")->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('E19')->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()->getComment('E19')->getText()->createTextRun("=SUMAR.SI('CONTROL PRESUPUESTAL POR EGRESO'!$A$13:$A$45,XX,'CONTROL PRESUPUESTAL POR EGRESO'!$I$13:$I$45)+SUMAR.SI('CONTROL PRESUPUESTAL POR EGRESO'!$A$13:$A$45,XX,'CONTROL PRESUPUESTAL POR EGRESO'!$K$13:$K$45)\r\nCambiar el número resaltado en negrita (XX) por el número correspondiente a cada cap")->getFont()->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('G19')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('G19')->getText()->createTextRun('Diferencia entre Plan y Actual. Debe coincidir con la columna cambio en el alcance de la hoja "alcance presupuestado"')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('H19')->setAuthor('Linea Global');
$objPHPExcel->getActiveSheet()->getComment('H19')->getText()->createTextRun('Diferencia entre Actual y Causado')->getFont()->setBold(true)->setSize(7.5);
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(80);

//$objWorksheet->getTabColor()->setRGB('C4BD97');
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('INFORME EJECUTIVO');
//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
//segunda hoja
//segunda hoja
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setCellValue('A1' , "")
    ->setCellValue('D1' , "ALCANCE PRESUPUESTADO")
    ->setCellValue('D2' , "VERSIÓN: 05")
    ->setCellValue('H2' , "FECHA VERSION: 30/03/2016")
    ->setCellValue('K2' , "APROBADO POR: NATALÍ PINZÓN")
    ->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL ")
    ->setCellValue('A4' , "CLIENTE: ".$cliente)
    ->setCellValue('A5' , "NOMBRE DE LA OBRA: ".$nomo)
    ->setCellValue('A6' , "FECHA INICIAL: ".$fechai)
    ->setCellValue('A8' , "GRUPO/CAP")
    ->setCellValue('B8' , "ITEM")
    ->setCellValue('C8' , "DESCRIPCION")
    ->setCellValue('D8' , "UN")
    ->setCellValue('E8' , "INICIALES")
    ->setCellValue('h8' , "ACTUALES")
    ->setCellValue('E9' , "CANTIDAD INICIAL")
    ->setCellValue('F9' , "VALOR UNIT INICIAL")
    ->setCellValue('G9' , "PRESUPUESTO INICIAL")
    ->setCellValue('H9' , "CANTIDAD PROYECTADA")
    ->setCellValue('I9' , "CANTIDAD EJECUTADA")
    ->setCellValue('J9' , "CANTIDAD FANTANTE")
    ->setCellValue('K9' , "VALOR UNIT ACTUAL")
    ->setCellValue('L9' , "PRESUPUESTO ACTUALIZADO")
    ->setCellValue('M8' , "CAMBIOS EN EL ALCANCE")
    ->mergeCells('A8:A9')
    ->mergeCells('B8:B9')
    ->mergeCells('C8:C9')
    ->mergeCells('D8:D9')
    ->mergeCells('M8:M9')
    ->mergeCells('E8:G8')
    ->mergeCells('H8:L8')
    ->mergeCells('A1:C2')
    ->mergeCells('D1:M1')
    ->mergeCells('D2:G2')
    ->mergeCells('H2:J2')
    ->mergeCells('K2:M2')
    ->mergeCells('A3:M3')
    ->mergeCells('A4:M4')
    ->mergeCells('A5:M5')
    ->mergeCells('A6:M6')
    ->mergeCells('A7:M7');
							  cellColor('A1:M1', 'FFFFFF');
							  cellColor('E8:G9', 'B8CCE4');//INICIALES
							  cellColor('H8:L9', 'BE7C7C');//ACTUALIES
							  cellColor('M8:M9', '92D050');//CAMBIOS ALCANCE
							$objPHPExcel->getActiveSheet()->getStyle("A8:M9")->getFont()->setBold(true);
							$objPHPExcel->getActiveSheet()->getStyle('D1:M2')->getFont()->setBold(true);
							$objPHPExcel->getActiveSheet()->getStyle("A3:M10")->getFont()->setSize(10)->setBold(true)->setName('Calibri');
							$objPHPExcel->getActiveSheet()->getStyle('A8:M9')->getAlignment()->setWrapText(true);
							$objPHPExcel->getActiveSheet()->getStyle('A8:M9')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							$objPHPExcel->getActiveSheet()->getStyle('A8:M9')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
							$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
							$objPHPExcel->getActiveSheet()->getStyle('D2:M2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
							$objPHPExcel->getActiveSheet()->getStyle('D1:M1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					        $objPHPExcel->getActiveSheet()->getStyle('A8:M9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(62);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(11);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
//echo date('H:i:s') , " Set column align" , EOL;
/*$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");*/
//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");


//CICLO DE GRUPO

$congru = mysql_query("select distinct g.gru_nombre as nom,g.gru_clave_int as cla from grupos g join pre_gru_cap_actividad p on p.gru_clave_int = g.gru_clave_int where p.pre_clave_int = '".$idpresupuesto."' order by g.gru_orden ASC");
$numg = mysql_num_rows($congru);
$filc = 10;
$hasta = $filc+$numg;
$totaldirecto  = 0;
for($g=10; $g<$hasta;$g++)
{
    $datg = mysql_fetch_array($congru);
    $grupo = $datg['nom'];
    $idgrupo = $datg['cla'];
    $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idgrupo."'");
    $datsum = mysql_fetch_array($consu);
    if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}
    if($datsum['totca']=="" || $datsum['totca']==NULL){$totala=0;}else{$totala=$datsum['totca'];}
    $totalalcc =  $total - $totala;

    //AÑADIR FILA
    $objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $grupo)
        ->setCellValue('B' . $filc, "")
        ->setCellValue('C' . $filc, "")
        ->setCellValue('D' . $filc, "")
        ->setCellValue('E' . $filc, "")
        ->setCellValue('F' . $filc, "")
        ->setCellValue('G' . $filc, $total)
        ->setCellValue('L' . $filc, $totala)
        ->setCellValue('M' . $filc, $totalalcc)
        ->mergeCells('A' . $filc.':F' . $filc)
        ->mergeCells('H' . $filc.':k' . $filc)
    ;
    cellColor('A' . $filc.':M'.$filc, '92D050');


    $objPHPExcel->getActiveSheet()->getStyle('A' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getFont()->setSize(10)->setName('Calibri');
    //BORDES GRUPOS
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $objPHPExcel->getActiveSheet()->getStyle('G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('L'.$filc.":M".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');




    $filc = $filc + 1;//INCREMENTA 1 PARA EMPEZAR LOS CAPITULOS
    $conc = mysql_query("select d.pgc_clave_int idd, c.cap_clave_int id,c.cap_nombre as nom,c.cap_codigo as codc,d.pgc_codigo as co,p.pre_apli_iva apli from  presupuesto p join pre_gru_capitulo d on d.pre_clave_int = p.pre_clave_int join capitulos c on d.cap_clave_int = c.cap_clave_int  where p.pre_clave_int = '".$idpresupuesto."' and d.gru_clave_int = '".$idgrupo."' and d.cap_clave_int in(select cap_clave_int from pre_gru_cap_actividad where pre_clave_int = '".$idpresupuesto."' and gru_clave_int = '".$idgrupo."'  ) order by idd");
    $numc = mysql_num_rows($conc);
    $hasta1 = $filc + $numc;
    for($c=$filc;$c<$hasta1;$c++)
    {
        $datc = mysql_fetch_array($conc);
        $idd = $datc['idd'];
        $idcapitulo = $datc['id'];
        $nom = $datc['nom'];
        $apli = $datc['apli'];
        $codc = number_format($datc['co'],2,'.','');
        $codca = $datc['co'];
        $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."' and gru_clave_int = '".$idgrupo."' and cap_clave_int = '".$idcapitulo."'");
        $datsum = mysql_fetch_array($consu);
        if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}
        if($datsum['totca']=="" || $datsum['totca']==NULL){$totala=0;}else{$totala=$datsum['totca'];}
        $totalalcc =  $total - $totala;
        //AÑADIR FILA
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $codc)
            ->setCellValue('B' . $filc, $nom)
            ->setCellValue('C' . $filc, "")
            ->setCellValue('D' . $filc, "")
            ->setCellValue('E' . $filc, "")
            ->setCellValue('F' . $filc, "")
            ->setCellValue('G' . $filc, $total)
            ->setCellValue('L' . $filc, $totala)
            ->setCellValue('M' . $filc, $totalalcc)
            ->mergeCells('B' . $filc.':F' . $filc)
            ->mergeCells('H' . $filc.':k' . $filc)
        ;
        cellColor('A' . $filc.':M'.$filc, 'C4D79B');
        $objPHPExcel->getActiveSheet()->getStyle('A' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $filc.':M' . $filc)->getFont()->setSize(10)->setName('Calibri');
        $objPHPExcel->getActiveSheet()->getStyle('A'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $objPHPExcel->getActiveSheet()->getStyle('G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('L'.$filc.':M'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setOutlineLevel(1);
        $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setVisible(false);


        $filc = $filc + 1;//SE INCREMENTA 1 PARA EMPEZAR LAS ACTIVIDAD
        $cona = mysql_query("select d.pgca_clave_int idd, d.cap_clave_int idc,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,d.pgca_cantidad cant,d.pgca_cant_proyectada cantp,d.pgca_cant_ejecutada cante,d.pgca_valor_act vala,ciu_nombre,tpp_nombre,d.pgca_item as Item,d.pgca_creacion as cre   from  pre_gru_cap_actividad  d join actividades a on a.act_clave_int = d.act_clave_int join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where d.pre_clave_int = '".$idpresupuesto."' and d.gru_clave_int = '".$idgrupo."' and d.cap_clave_int = '".$idcapitulo."' order by idd asc");
        $numa = mysql_num_rows($cona);

        $hasta2 = $filc + $numa;
        $i = 1;
        for($a=$filc;$a<$hasta2;$a++)
        {
            $data = mysql_fetch_array($cona);
            //AÑADIR FILA
            $idd = $data['idd'];
            $idca = $data['id'];
            $noma = utf8_encode(convert_htmlentities($data['nom']));
            $uni = $data['uni'];
            $ida = $data['ida'];
            $cant= $data['cant'];
            $cantp = $data['cantp']; //if($cantp<=0){$cantp= $cant;}
            $cante = $data['cante'];
            $cre = $data['cre'];
            //$item = $dat['Item'];
            if($i<10){$item="".$codca.".0".$i;}else{$item="".$codca.".".$i;}
            if($cre==1){$item="EXT";}



            if($apli==0)
            {
                //SUBANALISIS
                $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
                    ",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
                    " from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");

                $datsu = mysql_fetch_array($consu);
                $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
                $totals = $totals + ($totads+$totims+$totuts+$totivs);

                //consulta del total de la suma de las actividades asignadas a este capitulo
                $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
                    ",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
                    " from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
                $datsu = mysql_fetch_array($consu);
                $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
                $apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);

                $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)) tot".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_adm_act)/100) totad".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_imp_act)/100) totim".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100) totut".
                    ",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                    " from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
                $datsu = mysql_fetch_array($consu);
                $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
                $totals = $totals + ($totads+$totims+$totuts+$totivs);

                $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
                    ",sum(((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                    " from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
                $datsu = mysql_fetch_array($consu);
                $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
                $apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
            }
            else
            {

                //SUBANALISIS
                $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
                    ",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100)+".
                    "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100)+".
                    "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
                    " from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");

                $datsu = mysql_fetch_array($consu);
                $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
                $totals = $totals + ($totads+$totims+$totuts+$totivs);

                //consulta del total de la suma de las actividades asignadas a este capitulo
                $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
                    ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
                    ",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
                    "((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
                    "((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
                    " from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
                $datsu = mysql_fetch_array($consu);
                $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
                $apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);

                $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)) tot".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_adm_act)/100) totad".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_imp_act)/100) totim".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100) totut".
                    ",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_adm_act)/100)+".
                    "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_imp_act)/100)+".
                    "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                    " from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
                $datsu = mysql_fetch_array($consu);
                $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
                $totals = $totals + ($totads+$totims+$totuts+$totivs);

                $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
                    ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
                    ",sum((((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100)+".
                    "((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100)+".
                    "((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                    " from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.gru_clave_int  = '".$idgrupo."' and pa.cap_clave_int= '".$idcapitulo."' and a.act_clave_int = '".$ida."'");
                $datsu = mysql_fetch_array($consu);
                $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
                if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
                $apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);

            }


            $ciu = $data['ciu_nombre'];
            $tpp = $data['tpp_nombre'];
            $preact = $apua*$cantp;//PRESUPUESTO ACTUAL
            $cantf = $cantp - $cante;//CANTIDAD FALTANTE
            $total = $apu * $cant;//PRESUPUESTO INICIAL
            //$apu = number_format($apu,2,'.',',');//APU INICIAL
            $alcance  = $total-$preact;	//CAMBIO EN EL ALCANCE
            //$total = number_format($total,2,'.',',');
            //$preact = number_format($preact,2,'.',',');
            //$apua = number_format($apua,2,'.',',');//APU ACTUAL

            //$preact = number_format($preact,2,'.',',');
            //$apua = number_format($apua,2,'.',',');//APU ACTUAL
            $totaldirecto = $totaldirecto + $total;
            //$total = number_format($total,2,'.',',');

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, "")
                ->setCellValue('B' . $filc, $item)
                ->setCellValue('C' . $filc, $noma)
                ->setCellValue('D' . $filc, $uni)
                ->setCellValue('E' . $filc, $cant)
                ->setCellValue('F' . $filc, $apu)
                ->setCellValue('G' . $filc, "=E".$filc."*F".$filc)
                ->setCellValue('H' . $filc, $cantp)
                ->setCellValue('I' . $filc, $cante)
                ->setCellValue('J' . $filc, "=H".$filc."-I".$filc)
                ->setCellValue('k' . $filc, $apua)
                ->setCellValue('L' . $filc, "=H".$filc."*K".$filc)
                ->setCellValue('M' . $filc, "=G".$filc."-L".$filc)
            ;
            cellColor('A' . $filc.':M'.$filc, 'FFFFFF');
            $objPHPExcel->getActiveSheet()->getStyle('A' . $filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
            $objPHPExcel->getActiveSheet()->getStyle('D'.$filc.':E'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
            $objPHPExcel->getActiveSheet()->getStyle('H'.$filc.':J'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
            $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $filc.':M' . $filc)->getFont()->setSize(10)->setName('Calibri');

            $objPHPExcel->getActiveSheet()->getStyle('E'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet()->getStyle('H'.$filc.':J'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

            $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':M'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setOutlineLevel(2);
            $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setVisible(false);

            if($item=="EXT"){
                $objPHPExcel->getActiveSheet()->getStyle('B'.$filc.':M'.$filc)->applyFromArray($styleRED);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$filc.':G'.$filc)->getNumberFormat()->setFormatCode('[Red][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
                $objPHPExcel->getActiveSheet()->getStyle('k'.$filc.':M'.$filc)->getNumberFormat()->setFormatCode('[Red][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
            }
            else
            {
                $objPHPExcel->getActiveSheet()->getStyle('F'.$filc.':G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
                $objPHPExcel->getActiveSheet()->getStyle('k'.$filc.':M'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
            }

            if($a==($hasta2-1)){}else{$filc = $filc + 1;}//INCREMENTO DE FILA EN ACTIVIDAD
            $i++;
        }
        $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setCollapsed(true);
        $objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);

        if($c==($hasta1-1)){}else{$filc = $filc + 1;}//INCREMENTO DE FILA EN CAPITULO

    }
    $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setCollapsed(true);
    $objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);
    if($g==($hasta-1)){}else{$filc = $filc + 1;};//INCREMENTO DE FILA EN GRUPO



}

$filc1 = $filc +1;
$filc2 = $filc +2;
$filc3 = $filc +3;
$filc4 = $filc +4;
$filc5= $filc +5;
$filc6= $filc +6;
$filc7= $filc +7;
$filc8= $filc +8;
$filc9= $filc +9;
$filc10= $filc +10;


$consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
$datsum = mysql_fetch_array($consu);
if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

		$totadm = ($totalc * $adm)/100;
		$totimp = ($totalc * $imp)/100;
		$totuti = ($totalc * $uti)/100;
		if($apli==0){ $totivai = ($totuti * $iva)/100;}else{ $totivai = (($totadm+$totimp+$totuti) * $iva)/100; }
		$totpre = $totalc;// + $totadm + $totimp + $totuti + $totivai;



$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$idpresupuesto."' limit 1");
$dat = mysql_fetch_array($con);
$adma = $dat['pri_administracion'];
$ivaa = $dat['pri_iva'];
$impa = $dat['pri_imprevisto'];
$utia = $dat['pri_utilidades'];

		$totadma = ($totalca * $adma)/100;
		$totimpa = ($totalca * $impa)/100;
		$totutia = ($totalca * $utia)/100;
		if($apli==0){ $totivaa = ($totutia * $ivaa)/100; }else{ $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
		$totprea = $totalca;// + $totadma + $totimpa + $totutia + $totivaa ;


		$alccos = $totpre - $totalca;

		$alcpre = $totpre - $totprea;
		$alcadm = $totadm - $totadma;
		$alcimp = $totimp - $totimpa;
		$alcuti = $totuti - $totutia;
		$alciva = $totivai - $totivaa;

$objPHPExcel->getActiveSheet()->setCellValue('A'.$filc3, "ADMINISTRACION")
    ->setCellValue('A'.$filc4, "IMPREVISTO")
    ->setCellValue('A'.$filc5, "UTILIDADES")
    ->setCellValue('A'.$filc6, "IVA")
    ->setCellValue('A'.$filc2, "SUBTOTAL COSTO DIRECTO")
    ->setCellValue('F'.$filc3, $adm."%")
    ->setCellValue('F'.$filc4, $imp."%")
    ->setCellValue('F'.$filc5, $uti."%")
    ->setCellValue('F'.$filc6, $iva."%")
    ->setCellValue('A'.$filc7, "TOTAL COSTO DIRECTO")
    ->setCellValue('G'.$filc2, $totpre)
    ->setCellValue('G'.$filc3, $totadm)
    ->setCellValue('G'.$filc4, $totimp)
    ->setCellValue('G'.$filc5, $totuti)
    ->setCellValue('G'.$filc6, $totivai)
    ->setCellValue('G'.$filc7, '=SUM(G'.$filc2.':G'.$filc6.')')
    ->setCellValue('A'.$filc9, "DILIGENCIADO POR: ".$creadopor." - ".$cargo)
    ->setCellValue('A'.$filc10, "APROBADO POR: ".$aprobadopor." - ".$cargoc)
    ->mergeCells('A' .$filc8.':M' .$filc8)
    ->mergeCells('A' .$filc9.':M' .$filc9)
    ->mergeCells('A' .$filc10.':M' .$filc10)
    // ->mergeCells('A' .$filc1.':G' .$filc1)
    //  ->mergeCells('A' .$filc2.':F' .$filc2)
    //->mergeCells('A' .$filc3.':E' .$filc3)
    // ->mergeCells('A' .$filc4.':E' .$filc4)
    // ->mergeCells('A' .$filc5.':E' .$filc5)
    // ->mergeCells('A' .$filc6.':E' .$filc6)
    //  ->mergeCells('A'.$filc7.':F'.$filc7)

    ->setCellValue('H'.$filc3, "ADMINISTRACIÓN")
    ->setCellValue('H'.$filc4, "IMPREVISTO")
    ->setCellValue('H'.$filc5, "UTILIDADES")
    ->setCellValue('H'.$filc6, "IVA")
    ->setCellValue('H'.$filc2, "SUBTOTAL COSTO DIRECTO")
    ->setCellValue('K'.$filc3, $adma."%")
    ->setCellValue('K'.$filc4, $impa."%")
    ->setCellValue('K'.$filc5, $utia."%")
    ->setCellValue('K'.$filc6, $ivaa."%")
    ->setCellValue('H'.$filc7, "TOTAL COSTO DIRECTO")
    ->setCellValue('L'.$filc2, $totalca)
    ->setCellValue('L'.$filc3, $totadma)
    ->setCellValue('L'.$filc4, $totimpa)
    ->setCellValue('L'.$filc5, $totutia)
    ->setCellValue('L'.$filc6, $totivaa)
    ->setCellValue('L'.$filc7, '=SUM(L'.$filc2.':L'.$filc6.')')


    ->setCellValue('M'.$filc2, "=G".$filc2."-L".$filc2)
    ->setCellValue('M'.$filc3, "=G".$filc3."-L".$filc3)
    ->setCellValue('M'.$filc4, "=G".$filc4."-L".$filc4)
    ->setCellValue('M'.$filc5, "=G".$filc5."-L".$filc5)
    ->setCellValue('M'.$filc6, "=G".$filc6."-L".$filc6)
    ->setCellValue('M'.$filc7, "=G".$filc7."-L".$filc7)



    ->mergeCells('A' .$filc1.':M' .$filc1)
    ->mergeCells('A' .$filc2.':F' .$filc2)
    ->mergeCells('A' .$filc3.':E' .$filc3)
    ->mergeCells('A' .$filc4.':E' .$filc4)
    ->mergeCells('A' .$filc5.':E' .$filc5)
    ->mergeCells('A' .$filc6.':E' .$filc6)
    ->mergeCells('A' .$filc7.':F' .$filc7)

    ->mergeCells('H' .$filc2.':K' .$filc2)
    ->mergeCells('H' .$filc3.':J' .$filc3)
    ->mergeCells('H' .$filc4.':J' .$filc4)
    ->mergeCells('H' .$filc5.':J' .$filc5)
    ->mergeCells('H' .$filc6.':J' .$filc6)
    ->mergeCells('H' .$filc7.':K' .$filc7)


;
							  cellColor('A' .$filc2.':G'.$filc7, 'FFFFFF');



//echo date('H:i:s') , " Set column widths" , EOL;

$objPHPExcel->getActiveSheet()->getStyle('G'.$filc2.':G'.$filc7)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('L'.$filc2.':M'.$filc7)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('F' .$filc3.':F' .$filc6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc3.':E' .$filc6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':F' .$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc7.':F' .$filc7)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('F' .$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('F' .$filc7)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));



$objPHPExcel->getActiveSheet()->getStyle('J' .$filc3.':J' .$filc6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc3.':H' .$filc6)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc2.':J' .$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc7.':J' .$filc7)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc1)->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension($filc8)->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension($filc2)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc3)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc4)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc5)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc6)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc7)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc9)->setRowHeight(12.75);
$objPHPExcel->getActiveSheet()->getRowDimension($filc10)->setRowHeight(12.75);

//echo date('H:i:s') , " Set column number format" , EOL;
/*$objPHPExcel->getActiveSheet()->getStyle('G9:G'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE );
$objPHPExcel->getActiveSheet()->getStyle('F9:F'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('K9:K'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('L9:L'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('M9:M'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);*/

$objPHPExcel->getActiveSheet()->getStyle('A1:M6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('G' .$filc2.':G' .$filc7)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); cellColor('G' .$filc2.':G' .$filc7, 'B8CCE4');//TOTALES AI U INICIAL
$objPHPExcel->getActiveSheet()->getStyle('M' .$filc2.':M' .$filc7)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); cellColor('L' .$filc2.':L' .$filc7, 'BE7C7C');//TOTALES AI U ACTUAL


$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':F' .$filc2)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':F' .$filc2)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc3.':F' .$filc3)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc3.':F' .$filc3)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc4.':F' .$filc4)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc4.':F' .$filc4)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc5.':F' .$filc5)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc5.':F' .$filc5)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc6.':F' .$filc6)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc6.':F' .$filc6)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc7.':F' .$filc7)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc7.':F' .$filc7)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('H' .$filc2.':L' .$filc2)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc2.':L' .$filc2)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc3.':L' .$filc3)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc3.':L' .$filc3)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc4.':L' .$filc4)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc4.':L' .$filc4)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc5.':L' .$filc5)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc5.':L' .$filc5)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc6.':L' .$filc6)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc6.':L' .$filc6)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc7.':L' .$filc7)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc7.':L' .$filc7)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('A' .$filc9.':M' .$filc10)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':M' .$filc10)->getFont()->setSize(10)->setName('Calibri');	;
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc2.':M' .$filc10)->getFont()->setBold(true);


//eventos de cambio
$filat = $filc10 + 2;
$filat2 = $filat-1;
$filc11 = $filc10 + 3;
$filc12 = $filc11 + 1;
$objPHPExcel->getActiveSheet()->getRowDimension($filat2)->setRowHeight(6);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$filat , "EVENTOS DE CAMBIO")
    ->setCellValue('A'.$filc11 , "CAP.")
    ->setCellValue('B'.$filc11 , "ITEM.")
    ->setCellValue('C'.$filc11 , "DESCRIPCION")
    ->setCellValue('D'.$filc11 , "UNID")
    ->setCellValue('E'.$filc11 , "INICIALES")
    ->setCellValue('H'.$filc11 , "ACTUALES")
    ->setCellValue('M'.$filc11 , "CAMBIOS EN EL ALCANCE")
    ->mergeCells('A'.$filat.':M'.$filat)
    ->mergeCells('E'.$filc11.':G'.$filc11)
    ->mergeCells('H'.$filc11.':L'.$filc11)
    ->mergeCells('A'.$filat2.':M'.$filat2)
;
cellColor('A'.$filc11.':D'.$filc11, 'FFFFFF');
cellColor('E'.$filc11.':G'.$filc11, 'B8CCE4');//INICIALES
cellColor('H'.$filc11.':L'.$filc11, 'BE7C7C');//ACTUALIES
cellColor('M'.$filc11.':M'.$filc11, '92D050');//CAMBIOS ALCANCE
$objPHPExcel->getActiveSheet()->getStyle("A".$filat.":M".$filat)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A'.$filat.':M'.$filat)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A'.$filat.':M'.$filat)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A'.$filat.':M'.$filat)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);



$objPHPExcel->getActiveSheet()->getStyle("A".$filc11.":M".$filc11)->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle("A3:M10")->getFont()->setSize(10)->setBold(true)->setName('Calibri');
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc11.':M'.$filc11)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc11.':M'.$filc11)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc11.':M'.$filc11)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc11.':M'.$filc11)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


$cone = mysql_query("select pev_clave_int,pev_descripcion,pev_aprobo,pev_fec_aprobo,pev_codigo  from eventoscambio where pre_clave_int  = '".$idpresupuesto."'");
$nume = mysql_num_rows($cone);
$hastam = $filc12 + $nume;
$filc13 = $filc12;
$filas = "=";
$filas2 = "=";
for($k=$filc12;$k<$hastam;$k++)
{

    $date = mysql_fetch_array($cone);
    $idpev = $date['pev_clave_int'];
    $pev = utf8_encode(convert_htmlentities($date['pev_descripcion']));

    $apro = $date['pev_aprobo'];
    $feca = $date['pev_fec_aprobo'];
    $codp = $date['pev_codigo'];
    $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.$filc13 ,  number_format($codp,2,'.',''))
        ->setCellValue('B'.$filc13  , "")
        ->setCellValue('C'.$filc13  , $pev)
        ->setCellValue('D'.$filc13  , "")
        ->setCellValue('E'.$filc13  , "APROBÓ:")
        ->setCellValue('F'.$filc13  , $apro)
        ->setCellValue('G'.$filc13  , "")
        ->setCellValue('H'.$filc13  , "FECHA APROBACIÓN:")
        ->setCellValue('J'.$filc13  , $feca)
        ->mergeCells('H'.$filc13.':I'.$filc13);
    cellColor('A' . $filc13.':M'.$filc13, '92D050');
    $objPHPExcel->getActiveSheet()->getStyle('A' .$filc13.':M' .$filc13)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A' .$filc13.':M' .$filc13)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('E'.$filc13)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
    $conc = mysql_query("select d.pce_clave_int idd, d.pev_clave_int idc,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,d.pce_cantidad cant,d.pce_cant_proyectada cantp,d.pce_cant_ejecutada cante,ciu_nombre,tpp_nombre from  pre_cap_actividad  d join actividades a on a.act_clave_int = d.act_clave_int join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where d.pre_clave_int = '".$idpresupuesto."'  and d.pev_clave_int = '".$idpev."' order by idd asc");
    $numc = mysql_num_rows($conc);
    $filc14 = $filc13 + 1;
    $filc13 = $filc13 + $numc;
    $hastac = $filc14 + $numc;
    for($l=1;$l<=$numc;$l++)
    {
        $datc = mysql_fetch_array($conc);
        if($l==1){$filadesde = $filc14;}
        $idd = $datc['idd'];
        $idca = $datc['id'];
        $nom = $datc['nom'];
        $uni = $datc['uni'];
        $ida = $datc['ida'];
        $cant= $datc['cant'];
        $cantp = $datc['cantp'];
        $cante = $datc['cante'];
        if($l<10){$item="".$codp.".0".$l;}else{$item="".$codp.".".$l;}


        //verificar si tiene apu asociado
        $veriapu = mysql_query("select * from actividadinsumos where act_clave_int ='".$ida."'");
        $numapu = mysql_num_rows($veriapu);

        if($apli==0)
        {
            //SUBANALISIS
            $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
                ",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
                " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.pev_clave_int= '".$idpev."' and a.act_clave_int = '".$ida."'");

            $datsu = mysql_fetch_array($consu);
            $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
            $totals = $totals + ($totads+$totims+$totuts+$totivs);

            //consulta del total de la suma de las actividades asignadas a este capitulo
            $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
                ",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
                " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.pev_clave_int= '".$idpev."' and a.act_clave_int = '".$ida."'");
            $datsu = mysql_fetch_array($consu);
            $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
            $apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);

            $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)) tot".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_adm_act)/100) totad".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_imp_act)/100) totim".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100) totut".
                ",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.pev_clave_int= '".$idpev."' and a.act_clave_int = '".$ida."'");
            $datsu = mysql_fetch_array($consu);
            $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
            $totals = $totals + ($totads+$totims+$totuts+$totivs);

            $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
                ",sum(((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
                " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.pev_clave_int= '".$idpev."' and a.act_clave_int = '".$ida."'");
            $datsu = mysql_fetch_array($consu);
            $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
            $apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
        }
        else
        {
            //SUBANALISIS
            $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)) tot".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100) totad".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100) totim".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100) totut".
                ",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_adm_ini)/100)+".
                "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_imp_ini)/100)+".
                "((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
                " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.pev_clave_int= '".$idpev."' and a.act_clave_int = '".$ida."'");

            $datsu = mysql_fetch_array($consu);
            $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
            $totals = $totals + ($totads+$totims+$totuts+$totivs);

            //consulta del total de la suma de las actividades asignadas a este capitulo
            $consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)) tot".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100) totad".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100) totim".
                ",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100) totut".
                ",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_adm_ini)/100)+".
                "((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_imp_ini)/100)+".
                "((((pa.pgi_rend_ini*pa.pgi_vr_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
                " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
            $datsu = mysql_fetch_array($consu);
            $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
            $apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);

            $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)) tot".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_adm_act)/100) totad".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_imp_act)/100) totim".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100) totut".
                ",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_adm_act)/100)+".
                "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_imp_act)/100)+".
                "((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                " from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'  and pa.pev_clave_int= '".$idpev."' and a.act_clave_int = '".$ida."'");
            $datsu = mysql_fetch_array($consu);
            $totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
            $totals = $totals + ($totads+$totims+$totuts+$totivs);

            $consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act)) tot".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100) totad".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
                ",sum((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
                ",sum((((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_adm_act)/100)+".
                "((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100)+".
                "((((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
                " from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."' and pa.pev_clave_int= '".$idpev."' and a.act_clave_int = '".$ida."'");
            $datsu = mysql_fetch_array($consu);
            $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
            if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
            $apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
        }

        $ciu = $datc['ciu_nombre'];
        $tpp = $datc['tpp_nombre'];
        $preact = $apua*$cantp;//PRESUPUESTO ACTUAL
        $cantf = $cantp - $cante;//CANTIDAD FALTANTE
        $total = $apu * $cant;//PRESUPUESTO INICIAL
        //$apu = number_format($apu,2,'.',',');//APU INICIAL
        $alcance  = $total-$preact;	//CAMBIO EN EL ALCANCE
        // $total = number_format($total,2,'.',',');
        // $preact = number_format($preact,2,'.',',');
        //$apua = number_format($apua,2,'.',',');//APU ACTUAL
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$filc14 , "")
            ->setCellValue('B'.$filc14  , $item)
            ->setCellValue('C'.$filc14  , $nom)
            ->setCellValue('D'.$filc14  , $uni)
            ->setCellValue('E'.$filc14  , $cant)
            ->setCellValue('F'.$filc14  , $apu)
            ->setCellValue('G'.$filc14  ,"=E".$filc14."*F".$filc14/* $total*/)
            ->setCellValue('H'.$filc14  , $cantp)
            ->setCellValue('I'.$filc14  , $cante)
            ->setCellValue('J'.$filc14  , $cantf)
            ->setCellValue('k'.$filc14  , $apua)
            ->setCellValue('L'.$filc14  , $preact)
            ->setCellValue('M'.$filc14  , $alcance);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $filc14)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->getStyle('B'.$filc14.':M'.$filc14)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        //$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':M'.$filc)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getRowDimension($filc14)->setRowHeight(12.75);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $filc14.':M' . $filc14)->getFont()->setSize(10)->setName('Calibri');
        $objPHPExcel->getActiveSheet()->getStyle('E'.$filc14)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $objPHPExcel->getActiveSheet()->getStyle('H'.$filc14.':J'.$filc14)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$filc14.':G'.$filc14)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('K'.$filc14.':M'.$filc14)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('A'.$filc14.':M'.$filc14)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


        $objPHPExcel->getActiveSheet()->getRowDimension($filc14)->setOutlineLevel(1);
        $objPHPExcel->getActiveSheet()->getRowDimension($filc14)->setVisible(false);
        if($l==$numc)
        {
            $filad = $filadesde-1;
            $filas.= "+G".$filad;
            $filas2.= "+L".$filad;
            $objPHPExcel->getActiveSheet()
                ->setCellValue('G'.$filad , "=SUM(G".$filadesde.":G".$filc14.")")
                ->setCellValue('L'.$filad , "=SUM(L".$filadesde.":L".$filc14.")")
                ->setCellValue('M'.$filad , "=G".$filad."-L".$filad)
                ->mergeCells('A'.$filadesde.':A'.$filc14)
            ;
            $objPHPExcel->getActiveSheet()->getStyle('G'.$filad)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('L'.$filad.':M'.$filad)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

        }
        $filc14++;


    }
    $objPHPExcel->getActiveSheet()->getRowDimension($filc13)->setCollapsed(true);
    $objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);
    //total del capitulo
    $filc13++;
}
$filc15 = $filc13;
$filc16 = $filc13 + 1;
$filc17= $filc13 + 2;
$filc18= $filc13 + 3;
$objPHPExcel->getActiveSheet()->getRowDimension($filc15)->setRowHeight(6);
$objPHPExcel->getActiveSheet()->getRowDimension($filc17)->setRowHeight(6);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$filc16 , "TOTAL EVENTOS DE CAMBIO")
    ->setCellValue('H'.$filc16 , "TOTAL EVENTOS DE CAMBIO")
    ->setCellValue('G'.$filc16 , $filas)
    ->setCellValue('L'.$filc16 , $filas2)
    ->setCellValue('M'.$filc16 , "=G".$filc16."-L".$filc16)

    ->setCellValue('A'.$filc18 , "TOTAL PRESUPUESTO DE OBRA + EVENTOS DE CAMBIO")
    ->setCellValue('H'.$filc18 , "TOTAL PRESUPUESTO DE OBRA + EVENTOS DE CAMBIO")
    ->setCellValue('G'.$filc18 , "=G".$filc16."+G".$filc7)
    ->setCellValue('L'.$filc18 , "=L".$filc16."+L".$filc7)
    ->setCellValue('M'.$filc18 , "=G".$filc18."-L".$filc18)

    ->mergeCells('A'.$filc16.':F'.$filc16)
    ->mergeCells('H'.$filc16.':K'.$filc16)
    ->mergeCells('A'.$filc18.':F'.$filc18)
    ->mergeCells('H'.$filc18.':K'.$filc18)
    ->mergeCells('A'.$filc15.':M'.$filc15)
    ->mergeCells('A'.$filc17.':M'.$filc17)
;
cellColor('G'.$filc16, 'B8CCE4');//INICIALES
cellColor('L'.$filc16, 'BE7C7C');//ACTUALIES
cellColor('M'.$filc16, '92D050');//CAMBIOS ALCANCE
cellColor('G'.$filc18, 'B8CCE4');//INICIALES
cellColor('L'.$filc18, 'BE7C7C');//ACTUALIES
cellColor('M'.$filc18, '92D050');//CAMBIOS ALCANCE
$objPHPExcel->getActiveSheet()->getStyle('G'.$filc16)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('L'.$filc16.':M'.$filc16)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc16.':M'.$filc16)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc16.':F' .$filc16)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc16.':K' .$filc16)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

$objPHPExcel->getActiveSheet()->getStyle('G'.$filc18)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('L'.$filc18.':M'.$filc18)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc18.':M'.$filc18)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A' .$filc18.':F' .$filc18)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('H' .$filc18.':K' .$filc18)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
// Añadir una imagen al informe
//echo date('H:i:s') , " Añadir una imagen al informe" , EOL;
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(30);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('ALCANCE PRESUPUESTADO');





//SEXTA hoja
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A1' , "")
    ->setCellValue('E1' , "CONTROL PRESUPUESTAL POR EGRESOS")
    ->setCellValue('E2' , "VERSIÓN: 05")
    ->setCellValue('J2' , "FECHA VERSION: 30/03/2016")
    ->setCellValue('O2' , "APROBADO POR: NATALÍ PINZÓN")
    ->setCellValue('A3' , "INTERVENTOR: LINEA GLOBAL")
    ->setCellValue('A4' , "CONTRATISTA: ")
    ->setCellValue('A5' , "CLIENTE: ".$cliente)
    ->setCellValue('A6' , "NOMBRE DE LA OBRA: ".$nomo)

    ->setCellValue('A11' , "GRUPO")
    ->setCellValue('B11' , "FECHA")
    ->setCellValue('C11' , "NIT O CEDULA")
    ->setCellValue('D11' , "BENEFICIARIO")
    ->setCellValue('E11' , "N° DE FRA")
    ->setCellValue('F11' , "N° DE ACTA")
    ->setCellValue('G11' , "FECHA FACTURA")
    ->setCellValue('H11' , "VALOR NETO")
    ->setCellValue('I11' , "IVA")
    ->setCellValue('J11' , "VALOR BRUTO")
    ->setCellValue('K11' , "RETEGARANTIA")
    ->setCellValue('L11' , "ANTICIPO")
    ->setCellValue('M11' , "AMORTIZACION ANTICIPO")
    ->setCellValue('N11' , "N° ORDEN DE COMP.")
    ->setCellValue('O11' , "VALOR APROBADO ORDEN DE COMPRA DE OBRA")
    ->setCellValue('P11' , "SALDO O.C")
    ->setCellValue('Q11' , "ESTADO DE ORDEN DE COMPRA")
    ->setCellValue('R11' , "OBSERVACIONES")
    ->mergeCells('A1:D2')
    ->mergeCells('E1:R1')
    ->mergeCells('E2:I2')
    ->mergeCells('J2:N2')
    ->mergeCells('O2:R2')

    ->mergeCells('A3:R3')
    ->mergeCells('A4:R4')
    ->mergeCells('A5:R5')
    ->mergeCells('A6:R6')
    ->mergeCells('A7:R7')

    ->mergeCells('A11:A12')
    ->mergeCells('B11:B12')
    ->mergeCells('C11:C12')
    ->mergeCells('D11:D12')
    ->mergeCells('E11:E12')
    ->mergeCells('F11:F12')
    ->mergeCells('G11:G12')
    ->mergeCells('H11:H12')
    ->mergeCells('I11:I12')
    ->mergeCells('J11:J12')
    ->mergeCells('K11:K12')
    ->mergeCells('L11:L12')
    ->mergeCells('M11:M12')
    ->mergeCells('N11:N12')
    ->mergeCells('O11:O12')
    ->mergeCells('P11:P12')
    ->mergeCells('Q11:Q12')
    ->mergeCells('R11:R12')
;

								cellColor('A1:R2', 'FFFFFF');
								cellColor('A11:R11', '92D050');
								$objPHPExcel->getActiveSheet()->getStyle('A11:R12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle("A1:R2")->getFont()->setBold(true);
								$objPHPExcel->getActiveSheet()->getStyle('A11:R11')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle("A11:R11")->getFont()->setBold(true);
								$objPHPExcel->getActiveSheet()->getStyle('E1:R2')->getFont()->setBold(true);
								$objPHPExcel->getActiveSheet()->getStyle('A3:R6')->getFont()->setBold(true);

								$objPHPExcel->getActiveSheet()->getStyle('E1:R1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								$objPHPExcel->getActiveSheet()->getStyle('A11:R11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle('E2:R2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
								$objPHPExcel->getActiveSheet()->getStyle('A1:R6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								$objPHPExcel->getActiveSheet()->getStyle('A11:R12')->getFont()->setSize(10);
								$objPHPExcel->getActiveSheet()->getStyle('A11:R11')->getAlignment()->setWrapText(true);
								//echo date('H:i:s') , " Set column height" , EOL;
								$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(32.25);
								$objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(6);
								$objPHPExcel->getActiveSheet()->getRowDimension('10')->setRowHeight(6);
								$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(12.75);
								$objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(12.75);
								$objPHPExcel->getActiveSheet()->getStyle('A8:R9')->getFont()->setSize(10);
//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");

	$con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva from presupuesto where pre_clave_int = '".$idpresupuesto."' limit 1");
	$dat = mysql_fetch_array($con);
	$adm = $dat['pre_administracion'];
	$iva = $dat['pre_iva'];
	$imp = $dat['pre_imprevisto'];
	$uti = $dat['pre_utilidades'];
	$apli = $dat['pre_apli_iva'];
    $consu = mysql_query("SELECT sum(pgca_valor_act) as totc,sum(pgca_valor_acta) as totca FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");
    $datsum = mysql_fetch_array($consu);
    if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
    if($datsum['totca']=="" || $datsum['totca']==NULL){$totalca=0;}else{$totalca=$datsum['totca'];}

	 /* if($apli==0)
	  {
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
			",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);

			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
			",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}
			$totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);
	  }
	  else
	  {
		$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini) as tot".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
		",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
		" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totals = $totals + ($totads+$totims+$totuts+$totivs);

		$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini) as tot".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
		",(sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
		"((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
		" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$idpresupuesto."'");
		$datsu = mysql_fetch_array($consu);
		$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totalc = 0;}else {$totalc  = $datsu['tot'];}
		$totalc = $totalc + ($totad+$totim+$totut+$totiv) + ($totals);
	  }
		*/
		$totadm = ($totalc * $adm)/100;
		$totimp = ($totalc * $imp)/100;
		$totuti = ($totalc * $uti)/100;
		if($apli==0) { $totiva = ($totuti * $iva)/100; } else { $totiva = (($totadm + $totimp + $totuti) * $iva)/100;  }
		$totpre = $totalc + $totadm + $totimp + $totuti + $totiva;


$conpre = mysql_query("select cpe_clave_int,pre_clave_int,gru_clave_int,cpe_num_factura,cpe_num_acta,cpe_fec_factura,cpe_valor_neto,cpe_iva,cpe_ret_garantia,cpe_amortizacion,cpe_num_orden,cpe_valor_aprobado,cpe_observaciones,date_format(cpe_fec_factura,'%d-%b-%y') ff,cpe_documento,cpe_beneficiario,cpe_fecha,cpe_anticipo  from control_egreso where pre_clave_int = '".$idpresupuesto."'");

$numpre = mysql_num_rows($conpre); if($numpre<=0){$numpre=1;}
$hasta = $numpre + 13;

$acum = $hasta;
$filc = 13;
$totalvalor  = 0;
for ($i = 13; $i < $hasta; $i++)
{
    $dat = mysql_fetch_array($conpre);
    $cpe = $dat['cpe_clave_int'];
    $pre = $dat['pre_clave_int'];
    $gru = $dat['gru_clave_int'];
    $fac = $dat['cpe_num_factura'];
    $act = $dat['cpe_num_acta'];
    $fec = $dat['cpe_fecha'];
    $doc = $dat['cpe_documento'];
    $ben = $dat['cpe_beneficiario'];
    $fecf = $dat['cpe_fec_factura'];
    $net = $dat['cpe_valor_neto'];
    $iva = $dat['cpe_iva'];
    $ret = $dat['cpe_ret_garantia'];
    $anti = $dat['cpe_anticipo'];
    $amo = $dat['cpe_amortizacion'];
    $ord = $dat['cpe_num_orden'];
    $apro = $dat['cpe_valor_aprobado'];
    $obs = $dat['cpe_observaciones'];

    $ff = $dat['ff'];
    $bru = $net + $iva;
    $sal = $apro-$bru;
    if($sal>0){$est="Abierta";}else if($sal==0){$est = "Cerrada";}else{$est="Sobregirada";}
    if($sal<0){$sal = "(".$sal*(-1).")"; $col = "red";}else{$col="black";}

    $objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $gru)
        ->setCellValue('B' . $filc, $fec)
        ->setCellValue('C' . $filc, $doc)
        ->setCellValue('D' . $filc, $ben)
        ->setCellValue('E' . $filc, $fac)
        ->setCellValue('F' . $filc, $act)
        ->setCellValue('G' . $filc, $ff)
        ->setCellValue('H' . $filc, $net)
        ->setCellValue('I' . $filc, $iva)
        ->setCellValue('J' . $filc, '=+H'.$filc.'+I'.$filc)
        ->setCellValue('K' . $filc, $ret)
        ->setCellValue('L' . $filc, $anti)
        ->setCellValue('M' . $filc, $amo)
        ->setCellValue('N' . $filc, $ord)
        ->setCellValue('O' . $filc, $apro)
        ->setCellValue('P' . $filc, '=+O'.$filc.'-J'.$filc)
        ->setCellValue('Q' . $filc, '=IF(P' .$filc.'>0,"Abierta",IF(P'.$filc.'<0,"Sobregirada","Cerrada"))')
        ->setCellValue('R' . $filc, $obs);
    cellColor('A' . $filc.':R'.$filc, 'FFFFFF');

    $objPHPExcel->getActiveSheet()->getStyle('H'.$filc.":M".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getStyle('O'.$filc.":P".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':R'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':R'.$filc)->getFont()->setSize(10);


    //FIN CONSULTAS

    $filc = $filc+ 1;

}
$filc2 = $filc +1;
if($numpre<=0){ $filcs = $filc; }else { $filcs = $filc-1;}

	$objPHPExcel->getActiveSheet()
        ->setCellValue('A8' , "PARTIDA PRESUPUESTAL")
        ->setCellValue('A9' , "SALDO PRESUPUESTAL")
        ->setCellValue('E8' , $totpre)
        ->setCellValue('E9' , '=+E8-H'.$filc2)
        ->setCellValue('A'.$filc , 'TOTALES')
        ->setCellValue('H'.$filc , '=SUM(H13:H'.$filcs.')')
        ->setCellValue('I'.$filc , '=SUM(I13:I'.$filcs.')')
        ->setCellValue('J'.$filc , '=SUM(J13:J'.$filcs.')')
        ->setCellValue('K'.$filc , '=SUM(K13:K'.$filcs.')')
        ->setCellValue('L'.$filc , '=SUM(L13:L'.$filcs.')')
        ->setCellValue('M'.$filc , '=SUM(M13:M'.$filcs.')')
        ->setCellValue('O'.$filc , '=SUM(O13:O'.$filcs.')')
        ->setCellValue('P'.$filc , '=SUM(P13:P'.$filcs.')')
        ->setCellValue('A'.$filc2 ,'TOTAL INVERTIDO')
        ->setCellValue('H'.$filc2 , '=+J'.$filc.'+L'.$filc.'-M'.$filc)
        ->setCellValue('I'.$filc2 , '')
        ->setCellValue('J'.$filc2 , '')
        ->setCellValue('K'.$filc2 , '')
        ->setCellValue('L'.$filc2 , '')
        ->setCellValue('M'.$filc2 , '')
        ->setCellValue('O'.$filc2 , '')
        ->setCellValue('P'.$filc2 , '')
        ->mergeCells('A8:D8')
        ->mergeCells('A9:D9')
        ->mergeCells('E8:G8')
        ->mergeCells('E9:G9')
        ->mergeCells('H8:R9')
        ->mergeCells('A10:R10')
        ->mergeCells('A'.$filc.':G'.$filc)
        ->mergeCells('A'.$filc2.':G'.$filc2)
        ///->mergeCells('Q'.$filc.':R'.$filc)
    ;
	$objPHPExcel->getActiveSheet()->getStyle('A8:G9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle("A8:G9")->getFont()->setBold(true);
	//$objPHPExcel->getActiveSheet()->getStyle('E8:G9')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
	$objPHPExcel->getActiveSheet()->getStyle('E8:G9')->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
	$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':R'.$filc)->getFont()->setBold(true);
	//$objPHPExcel->getActiveSheet()->getStyle('H'.$filc.':P'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$filc.":M".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('O'.$filc.":P".$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');


	$objPHPExcel->getActiveSheet()->getStyle('A'.$filc2.':R'.$filc2)->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('H'.$filc2.':P'.$filc2)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

	//$objPHPExcel->getActiveSheet()->getStyle('H'.$filc2.':P'.$filc2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);


//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10.57);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10.57);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10.14);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20.14);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11.43);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11.43);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15.29);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(14.86);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(19.43);
$objPHPExcel->getActiveSheet()->getStyle('A13:A'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B13:B'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('C13:C'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('D13:D'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$objPHPExcel->getActiveSheet()->getStyle('E13:E'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('F13:F'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('G13:G'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('L13:L'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('M13:M'.$filcs)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('N13:N'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('O13:O'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('P13:P'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':G'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
//$objPHPExcel->getActiveSheet()->getStyle('H13:M'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
//$objPHPExcel->getActiveSheet()->getStyle('O13:P'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
$objPHPExcel->getActiveSheet()->getStyle('A13:R'.$filc2)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc2.':G'.$filc2)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

//añadir comentarios a celldas


$objPHPExcel->getActiveSheet()->getComment('Q11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('P11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
//$objPHPExcel->getActiveSheet()->getComment('P11')->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()->getComment('Q11')->getText()->createTextRun('Si el saldo es mayor a 0 entonces la OC esta abierta y si es igual a Cero entonces esta cerrada')->getFont()->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('P11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('O11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
//$objPHPExcel->getActiveSheet()->getComment('O11')->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()->getComment('P11')->getText()->createTextRun('Diferencia entre el valor aprobado en la OC y el valor bruto de la factura')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('O11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('N11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('O11')->getText()->createTextRun('Monto Aprobado para la OC. Si la OC se repite en varias facturas, relacionar el valor aprobado una sola vez')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('B11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('B11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('B11')->getText()->createTextRun('Fecha de recepción del gasto/Factura en Interventoria')->getFont()->setBold(true)->setSize(7.5);

$objPHPExcel->getActiveSheet()->getComment('A11')->setAuthor('Linea Global');
//$objCommentRichText = $objPHPExcel->getActiveSheet()->getComment('A11')->getText()->createTextRun('Linea Global:');
//$objCommentRichText->getFont()->setBold(true)->setSize(9);
$objPHPExcel->getActiveSheet()->getComment('A11')->getText()->createTextRun('Grupo al cual corresponde el gasto')->getFont()->setBold(true)->setSize(7.5);
/*$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

$objPHPExcel->getActiveSheet()->getProtection()->setPassword("P4v4s2017.*");*/

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('logo');
$objDrawing->setDescription('logo');
$objDrawing->setPath('../../dist/img/LOGOGLOBAL4.jpg');
$objDrawing->setHeight(60);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(100);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('CONTROL PRESUPUESTAL POR EGRESO');
//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
$callStartTime = microtime(true);
$archivo = date('Ymd').' PPTO DE OBRA '.$tppr.' '.$nomo.'.xlsx';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//$objWriter->save(str_replace(__FILE__,'descargas/'.$archivo,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
// Set password for readonly activesheet
/*
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("P4v4s2017.*");*/
// Set password for readonly data


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');