<?php
include("../../data/Conexion.php");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//JOIN actividadinsumos a on i.act_clave_int = a.act_clave_int
// DB table to use
$table = 'actividades';

// Table's primary key
$primaryKey = 'i.act_clave_int';//'act_clave_int'
$id = $_GET['id'];

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'i.act_clave_int',
		'dt' => 'DT_RowId', 'field' => 'act_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
	array( 'db' => 'i.act_clave_int', 'dt' => 'Editar', 'field' => 'act_clave_int' ),
	array( 'db' => 'i.act_clave_int', 'dt' => 'Codigo', 'field' => 'act_clave_int'),
	
	array( 'db' => 'i.act_nombre', 'dt' => 'Nombre', 'field' => 'act_nombre' ),
	array( 'db' => 't.tpp_nombre', 'dt' => 'Tipo_Proyecto', 'field' => 'tpp_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => "c.ciu_nombre", 'dt' => 'Ciudad', 'field' => 'ciu_nombre'  ),	
	array('db'  => 'e.est_nombre','dt' => 'Estado', 'field' => 'est_nombre' ),
	array('db'  => 'i.act_analisis','dt' => 'Analisis', 'field' => 'act_analisis' ),
	array('db'  => 'i.act_clave_int','dt' => 'Total', 'field' => 'act_clave_int', 'formatter' => function( $d, $row ) {
		
		$cons = mysql_query("SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int where i.act_clave_int = '".$d."'");
		$dats = mysql_fetch_array($cons);
		$total = $dats['tot'];
		
		/*$cons2 = mysql_query("SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  act_subanalisis AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_subanalisis where i.act_clave_int = '".$d."'");
		$dats2 = mysql_fetch_array($cons2);
		$total2 = $dats2['tot'];
		$total = $total + $total2;*/
		
		
            return '<div id="divapu'.$row[0].'">$'.number_format($total,2,'.',',').'</div>';
        }),
		array( 'db' => 's.ats_rendimiento', 'dt' => 'Rendimiento', 'field' => 'ats_rendimiento' ),
		array( 'db' => 's.ats_rendimiento', 'dt' => 'Totales', 'field' => 'ats_rendimiento', 'formatter' => function( $d, $row ) {
			
				$cons = mysql_query("SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int where i.act_clave_int = '".$row[1]."'");
				$dats = mysql_fetch_array($cons);
				$total = $dats['tot'];
				
				$totales = $total * $d;
				return '$'.number_format($totales,2,'.',',');
				
		
		}),
        array( 'db' => 'i.act_clave_int', 'dt' => 'Codigo2', 'field' => 'act_clave_int',"formatter"=>function($d,$row){
			return "S-".$d;
	} )
	
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = ' s.ats_clave_int ';
 $joinQuery = "FROM  actividades AS i join act_subanalisis s on s.act_subanalisis = i.act_clave_int JOIN actividadinsumos AS a on a.act_clave_int = s.act_subanalisis JOIN tipoproyecto AS t on t.tpp_clave_int = i.tpp_clave_int  JOIN unidades AS  u on u.uni_clave_int  = i.uni_clave_int LEFT JOIN ciudad AS c on c.ciu_clave_int = i.ciu_clave_int  JOIN departamento AS d on d.dep_clave_int = c.dep_clave_int  JOIN pais AS  p on p.pai_clave_int = d.pai_clave_int join estados AS e on e.est_clave_int  = i.est_clave_int";
$extraWhere = " e.est_clave_int not in('2','4') and s.act_clave_int = '".$id."'";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

