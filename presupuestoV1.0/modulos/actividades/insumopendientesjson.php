<?php
include("../../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario = $_COOKIE["usIdentificacion"];
$table = 'insumos';

// Table's primary key
$primaryKey = 'd.inp_clave_int';//'act_clave_int'

$columns = array(
	array(
		'db' => 'd.inp_clave_int',
		'dt' => 'DT_RowId', 'field' => 'inp_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_insp'.$d;
		}
	),
	array( 'db' => 'd.inp_clave_int', 'dt' => 'Delete', 'field' => 'inp_clave_int','formatter'=>function($d,$row){
	   return "<a role='button' class='btn btn-danger btn-xs' style='width:20px; height:20px' onClick=CRUDACTIVIDADES('DELETEPENDIENTE','".$d."')><i class='fa fa-trash'></i></a>";	
	}),
	array( 'db' => 'i.ins_nombre', 'dt' => 'Nombre', 'field' => 'ins_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Unidad', 'field' => 'uni_codigo' ),
	array( 'db' => 't.tpi_nombre ','dt'=>'Tipo', 'field' => 'tpi_nombre'),
	array( 'db'  => 'i.ins_valor','dt' => 'Valor', 'field' => 'ins_valor','formatter'=>function($d,$row){
		return "<span class='currency'>$ ".number_format($d,2,'.',',')."</span>";
		} ),
	array( 'db'  => 'd.inp_valor','dt' => 'Valor1', 'field' => 'inp_valor','formatter'=>function($d,$row){
		return "<span class='currency'>$ ".number_format($d,2,'.',',')."</span>";
		} ),
	array( 'db' => 'd.inp_rendimiento', 'dt' => 'Rendimiento', 'field' => 'inp_rendimiento','formatter'=>function($d,$row){
		return "<input  style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rempp".$row[0]."' type='text' value='".number_format($d,'2','.','')."'   onKeyPress='return NumCheck(event, this)' min='0'  onchange=guardarrendimientop('".$row[0]."',this.value,'')><br><span id='sp2r".$row[0]."'></span>";
		 return $d;
		} ),
		array( 'db' => 'd.ins_clave_int', 'dt' => 'Codigo', 'field' => 'ins_clave_int','formatter'=>function($d,$row){
	   return $d;	
	}),
);

$sql_details = array(
	'user' => 'usrpavas', //'root',
	'pass' => '9A12)WHFy$2p4v4s',//,'coquetteic'
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);

/*$sql_details = array(
	'user' => 'root',
	'pass' => 'coquetteic',
	'db'   => 'bdpresupuesto',
	'host' => '127.0.0.1'
);*/
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

 $groupBy = 'i.ins_clave_int';
 $joinQuery = " FROM  insumos i join insumospendientes d on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where d.usu_clave_int ='".$idUsuario."'";
$extraWhere = "";   
 
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

