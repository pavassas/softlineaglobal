<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
$idactividad = $_GET['edi'];
$idactividad = str_replace("-",",",$idactividad);//por facturar seleccionados
include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

/** Include PHPExcel */
require_once '../../Classes/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
   // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}
PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );			

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		),
	),
);
// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THICK,
			'color' => array('argb' => 'FF993300'),
		),
	),
);

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Pavas.co")
							 ->setLastModifiedBy("Pavas.co")
							 ->setTitle("ANALISIS PRECIO UNITARIOS")
							 ->setSubject("Informe Apu")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Informes");
// Create a first sheet
// Add data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()                         
                              ->setCellValue('A1' , "CODIGO")
							  ->setCellValue('B1' , "DESCRIPCIÓN")
							  ->setCellValue('C1' , "UNIDAD")
							  ->setCellValue('D1' , "RENDIM")
							  ->setCellValue('E1' , "PRECIO")
							  ->setCellValue('F1' , "EQUIPO")
							  ->setCellValue('G1' , "MATERIAL")
							  ->setCellValue('H1' , "M.DE.O")
							  ->setCellValue('I1' , "CIUDAD")
							  ->setCellValue('J1' , "TIPO PROYECTO");  
							  cellColor('A1:J1', '92D050');	
							  //bordes de los insumos
							   $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							   $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
							   $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));	
							   $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(12.5);
							   $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(6);
							  
			$conpre = mysql_query("select a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,ciu_nombre,tpp_nombre from actividades a  join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where a.act_clave_int in(".$idactividad.")");
			$numpre = mysql_num_rows($conpre);
			$hasta = $numpre + 2;
			$acum = $hasta;
			$filc = 3;
			$totalvalor  = 0;
			$totalequipo = 0;
			$totalmaterial = 0;
			$totalmano = 0;
			$ic=1;
			for ($i = 3; $i <= $hasta; $i++) 
			{
				$dat = mysql_fetch_array($conpre);
				$noma = $dat['nom'];
				$uni = $dat['uni'];
				$ida = $dat['ida'];
				$tpp = $dat['tpp_nombre'];
				$ciu = $dat['ciu_nombre'];			
	
				$conact = mysql_query("select d.ati_clave_int idd, i.ins_clave_int idi,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$ida."'   order by tip,tpl,nom,nomu");
				$numa = mysql_num_rows($conact);
				if($numa>0)
				{
				 $objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $ida)
				  ->setCellValue('B'. $filc, $noma)
				  //->setCellValue('C' . $filc, $uni)
				  ->setCellValue('D'. $filc, "")
				  ->setCellValue('E'. $filc, "")
				  ->setCellValue('F'. $filc, "")
				  ->setCellValue('G'. $filc, "")
				  ->setCellValue('H'. $filc, "")
				  ->setCellValue('I'. $filc, $ciu)
				  ->setCellValue('J'. $filc, $tpp) ;						 								
				  cellColor('A'.$filc.':J'.$filc,'92D050');
				 
				 $objPHPExcel->getActiveSheet()->getCell('C'.$filc)->setValueExplicit($uni,PHPExcel_Cell_DataType::TYPE_STRING);
				 $sum1 = $filc+1; $sum2 = $filc + $numa;
				 $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFont()->setBold(true);
				 $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
				 $objPHPExcel->getActiveSheet()->getStyle('C'.$filc.':D'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
				 $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':A'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
				 //BORDES DE LAS ACTIVIDADES
				 $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':C'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					  
				 $tvalor = 0;
				 $tmaterial = 0;
				 $tequipo = 0;
				 $tmano = 0;
				 $totalm = 0; $totale = 0; $totalma = 0; $apu = 0;
				 $fil = 0;
				 for($k=1;$k<=$numa;$k++)
				 {
					$dati = mysql_fetch_array($conact);			
					$idc = $dati['idd'];
					$nom = $dati['nom'];
					$unii = $dati['nomu'];
					$val = $dati['val1']; 
					$cod = $dati['cod'];
					$ren = $dati['ren'];
					$tpl = $dati['tpl'];
					$insu = $dati['idi'];
					$totalcini = $ren*$val;					
					$unidad = $cod;
					$material=0; $equipo = 0; $mano = 0;
					$mate = 0; $equi = 0; $man = 0;
					$apu = $apu + $totalcini;
					if($tpl==1){$material=$totalcini; $totalm = $totalm + $material;}else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo; }else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano;}
					$total = $material + $equipo + $mano;
					if($cod=="%"){$rent = $ren*100;}else{$rent = $ren;}
				   
					$fil = $filc  + $k;
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $fil, $insu)
					->setCellValue('B' . $fil, $nom)
					->setCellValue('C' . $fil, $cod)
					->setCellValue('D' . $fil, $rent)
					->setCellValue('E' . $fil, $val)
					->setCellValue('F' . $fil, $equipo)
					->setCellValue('G' . $fil, $material)
					->setCellValue('H' . $fil, $mano)
					->setCellValue('I' . $fil, $ciu)
					->setCellValue('J' . $fil, $tpp);
															 								
					cellColor('A'.$fil.':H'.$fil, 'FFFFFF'); 
					cellColor('I'.$fil.':J'.$fil, '92D050');
					$objPHPExcel->getActiveSheet()->getCell('C' . $fil)->setValueExplicit($cod,PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
					$objPHPExcel->getActiveSheet()->getStyle('C'.$fil.':D'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':A'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H' . $fil)->getFont()->setSize(10);	
					$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(12.75);				
					//bordes de los insumos
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
					$objPHPExcel->getActiveSheet()->getStyle('E'.$fil.':H'.$fil)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
			}
			//SUBANALISIS
	    	$consulta = mysql_query("SELECT i.act_clave_int idc,i.act_nombre nom,	u.uni_codigo cod,s.ats_rendimiento ri FROM	actividades AS i JOIN act_subanalisis s ON s.act_subanalisis = i.act_clave_int JOIN tipoproyecto AS t ON t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS u ON u.uni_clave_int = i.uni_clave_int WHERE s.act_clave_int in (".$ida.") GROUP BY s.act_subanalisis ORDER BY i.act_nombre ASC");
			$nums = mysql_num_rows($consulta);
			if($nums>0)
			{
				while($dats = mysql_fetch_array($consulta))
				{
					$idc = $dats['idc'];
					$insu = "S-".$idc;
					$nom = $dats['nom'];
					$tipo = "";
					$unidad = $dats['cod'];
					$ren = $dats['ri'];
					$tpl = 1;					  
					$cons = mysql_query("SELECT sum(a.ati_rendimiento*a.ati_valor) as tot FROM  actividades AS i JOIN actividadinsumos AS a on a.act_clave_int = i.act_clave_int join insumos r on r.ins_clave_int  = a.ins_clave_int where i.act_clave_int = '".$idc."'");
					$dats = mysql_fetch_array($cons);
					$valor = $dats['tot'];					   
					$totalcini = $ren*$valor;						
							
					$material=0; $equipo = 0; $mano = 0;
					$mate = 0; $equi = 0; $man = 0;
					$apu = $apu + $totalcini;
					if($tpl==1){$material=$totalcini; $totalm = $totalm + $material;}else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo; }else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano;}
					$total = $material + $equipo + $mano;	
					if($unidad=="%"){$rent = $ren*100;}else{$rent = $ren;}	
					if(round($rent,2)<=0){ $rent = rtrim($rent,'0');} else { $rent = $rent; } 	   
					$fil = $fil+1;
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $fil, $insu)
					->setCellValue('B' . $fil, $nom)
					->setCellValue('C' . $fil, $unidad)
					->setCellValue('D' . $fil, $rent)
					->setCellValue('E' . $fil, $valor)
					->setCellValue('F' . $fil, $equipo)
					->setCellValue('G' . $fil, $material)
					->setCellValue('H' . $fil, $mano)
					->setCellValue('I' . $fil, $ciu)
					->setCellValue('J' . $fil, $tpp);	
					
					if(round($rent,2)<=0){} else {
				$objPHPExcel->getActiveSheet()->getStyle('D'.$fil)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			} 
					$objPHPExcel->getActiveSheet()->getCell('C' . $fil)->setValueExplicit($unidad,PHPExcel_Cell_DataType::TYPE_STRING);					 								
					cellColor('A'.$fil.':H'.$fil,'FFFFFF');
					cellColor('I'.$fil.':J'.$fil,'92D050');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
					$objPHPExcel->getActiveSheet()->getStyle('C'.$fil.':D'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':A'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H' . $fil)->getFont()->setSize(10);	
					$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(12.75);			
					//bordes de los insumos
					$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':H'.$fil)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$fil.':H'.$fil)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
				   }
			   }		
		
			$filt = $filc + $numa + $nums + 1; 
			$tvalor = $tmaterial + $tequipo + $tmano;
			$objPHPExcel->getActiveSheet()
			->setCellValue('D' . $filc, "")
			->setCellValue('E' . $filc, "=SUM(F".$filc.":H".$filc.")")
			->setCellValue('F' . $filc, $totale)
			->setCellValue('G' . $filc, $totalm)
			->setCellValue('H' . $filc, $totalma);			
			//FORMATO MONEDA EN TOTALES
			$objPHPExcel->getActiveSheet()->getStyle('E'.$filc.':H'.$filt)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':J'.$filc)->getFont()->setSize(10)->setBold(true);;	
			$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);		
		//FIN CONSULTAS
		//BORDES TOTALES
			$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':J'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
	
	 		$filc = $filc + $numa + $nums + 2; 
		}	
		$ic++;							
}
$filc2 = $filc +1;		
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(63);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

//$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle("APU'S");

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()
                             
                              ->setCellValue('A1' , "CODIGO")
							  ->setCellValue('B1' , "DESCRIPCIÓN")
							  ->setCellValue('C1' , "UNIDAD")
							  ->setCellValue('D1' , "RENDIM")
							  ->setCellValue('E1' , "PRECIO")
							  ->setCellValue('F1' , "EQUIPO")
							  ->setCellValue('G1' , "MATERIAL")
							  ->setCellValue('H1' , "M.DE.O")
							  ->setCellValue('I1' , "CIUDAD")
							  ->setCellValue('J1' , "TIPO PROYECTO"); 							    
						
								cellColor('A1:J1', '92D050');
								$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));		
								$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
															
								//echo date('H:i:s') , " Set column height" , EOL;
								$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(12.75);
								
							  
$conpre = mysql_query("select a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,ciu_nombre,tpp_nombre from  act_subanalisis  d join actividades a on a.act_clave_int = d.act_subanalisis join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where s.act_clave_int in(".$idactividad.")");

$numpre = mysql_num_rows($conpre);
$hasta = $numpre + 2;
$acum = $hasta;
$filc = 3;
$totalvalor  = 0;
$totalequipo = 0;
$totalmaterial = 0;
$totalmano = 0;
$ic=1;
$codca = 0;
for ($i = 3; $i <= $hasta; $i++) 
{
	$dat = mysql_fetch_array($conpre);
	$noma = $dat['nom'];
	$uni = $dat['uni'];
	$ida = $dat['ida'];
	$ciu = $dat['ciu_nombre'];
	$tpp = $dat['tpp_nombre'];
	$item = "S-".$dat['ida'];
	
	$conact = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,IFNULL(d.ati_rendimiento,0) as ren,IFNULL(i.ins_valor,0) as val,IFNULL(d.ati_valor,0) as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$ida."'   order by idd");
	$numa = mysql_num_rows($conact);
	if($numa>0)
	{
		$objPHPExcel->getActiveSheet()->setCellValue('A' . $filc, $item)
		  ->setCellValue('B' . $filc, $noma)
		  //->setCellValue('C' . $filc, $uni)
		  ->setCellValue('D' . $filc, "")
		  ->setCellValue('E' . $filc, "")
		  ->setCellValue('F' . $filc, "")
		  ->setCellValue('G' . $filc, "")
		  ->setCellValue('H' . $filc, "") ;						 								
		  cellColor('A' . $filc.':C'.$filc, '92D050');
		  $objPHPExcel->getActiveSheet()->getCell('C' . $filc)->setValueExplicit($uni,PHPExcel_Cell_DataType::TYPE_STRING);
		  $sum1 = $filc+1; $sum2 = $filc + $numa;
		  $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':H'.$filc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
		  $objPHPExcel->getActiveSheet()->getStyle('C'.$filc.':D'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':A'.$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

         //BORDES DE LAS ACTIVIDADES
		 $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':C'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		 
	     $tvalor = 0;
		 $tmaterial = 0;
		 $tequipo = 0;
		 $tmano = 0;
		 $totalm = 0; $totale = 0; $totalma = 0; $apu = 0;
		 $fil = 0;
		 for($k=1;$k<=$numa;$k++)
		 {
			$dati = mysql_fetch_array($conact);			
			$idc = $dati['idd'];
			$nom = $dati['nom'];
			$tipo = $dati['tip'];
			$unii = $dati['nomu'];
			$val = $dati['val1']; 
			$cod = $dati['cod'];
			$ren = $dati['ren'];
			$tpl = $dati['tpl'];
			$insu = $dati['id'];
			$totalcini = $ren*$val;
			  
		    $unidad = $cod;
		    $material=0; $equipo = 0; $mano = 0;
		    $mate = 0; $equi = 0; $man = 0;
		    $apu = $apu + $totalcini;
		    if($tpl==1){$material=$totalcini; $totalm = $totalm + $material;}else if($tpl==2){$equipo = $totalcini; $totale = $totale + $equipo; }else if($tpl==3){$mano = $totalcini; $totalma = $totalma + $mano;}
		   $total = $material + $equipo + $mano;
		   if($cod=="%"){$rent = $ren*100;}else{$rent = $ren;}
		   if(round($rent,2)<=0){ $rent = rtrim($rent,'0');} else { $rent = $rent; } 
				   
			$fil = $filc + $k;
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $fil, $insu)
			->setCellValue('B' . $fil, $nom)
			->setCellValue('C' . $fil, $cod)
			->setCellValue('D' . $fil, $rent)
			->setCellValue('E' . $fil, $valor)
			->setCellValue('F' . $fil, $equipo)
			->setCellValue('G' . $fil, $material)
			->setCellValue('H' . $fil, $mano)
			->setCellValue('I' . $fil, $ciu)
			->setCellValue('J' . $fil, $tpp)
			;	
			if(round($rent,2)<=0){} else {
				$objPHPExcel->getActiveSheet()->getStyle('D'.$fil)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			} 	
			$objPHPExcel->getActiveSheet()->getCell('C' . $fil)->setValueExplicit($cod,PHPExcel_Cell_DataType::TYPE_STRING);					 								
			cellColor('A' . $fil.':H'.$fil, 'FFFFFF');
			cellColor('I' . $fil.':J'.$fil, '92D050');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':J'.$fil)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
			$objPHPExcel->getActiveSheet()->getStyle('C'.$fil.':D'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':A'.$fil)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':J' . $fil)->getFont()->setSize(10);	
			$objPHPExcel->getActiveSheet()->getRowDimension($fil)->setRowHeight(12.75);	
			
			//bordes de los insumos
			$objPHPExcel->getActiveSheet()->getStyle('A'.$fil.':J'.$fil)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$objPHPExcel->getActiveSheet()->getStyle('E'.$fil.':H'.$fil)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');


		}
		
		$filt = $filc + $numa + 1; 
		$tvalor = $tmaterial + $tequipo + $tmano;
		$objPHPExcel->getActiveSheet()
			->setCellValue('E' . $filc, "=SUM(F".$filc.":H".$filc.")")
			->setCellValue('F' . $filc, $totale)
			->setCellValue('G' . $filc, $totalm)
			->setCellValue('H' . $filc, $totalma)
			->setCellValue('I' . $filc, $ciu)
			->setCellValue('J' . $filc, $tpp);
			
			//FORMATO MONEDA EN TOTALES
			$objPHPExcel->getActiveSheet()->getStyle('E'.$filc.':H'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');

		$objPHPExcel->getActiveSheet()->getStyle('A' .$filc.':D' .$filc)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':J' . $filc)->getFont()->setSize(10)->setBold(true);;	
		$objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);		
		//FIN CONSULTAS
		//BORDES TOTALES
		$objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':J'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	 $filc = $filc + $numa + 2; 
	}		
	$ic++;							
}
$filc2 = $filc +1;
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(63);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getStyle('D3:D'.$filc)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle("SUBANALISIS");

$callStartTime = microtime(true);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
$archivo = 'APU.xlsx';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');