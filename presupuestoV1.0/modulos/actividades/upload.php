<?php
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];	
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
include ("../../data/Conexion.php");
require_once '../../Classes/PHPExcel/IOFactory.php';
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
if ( !empty( $_FILES ) ) 
{

    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
	$nameEXC = $_FILES['file']['name'];
	$dir = "xls/".$nameEXC;
    $uploadPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'xls' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];

	if(move_uploaded_file( $tempPath, $uploadPath ))
	{
	
		$objPHPEXCEL=PHPExcel_IOFactory::load($dir);
		$objHoja=$objPHPEXCEL->getActiveSheet()->toArray(null,true,true,true,true,true,true,true,true,true);
		$act =  "";
		foreach($objHoja as $iIndidce => $objCelda)
		{
			$COLA = $objCelda['A']; $COLB = $objCelda['B']; $COLC = $objCelda['C']; 
			$COLD = $objCelda['D']; $COLE = $objCelda['E']; $COLI = $objCelda['I'];
			$COLJ = $objCelda['J']; 
			
			$COLF = $objCelda['F']; $COLG = $objCelda['G']; $COLH = $objCelda['H'];
			 
			if($COLA=="ID" || $COLA=="" || $COLA==NULL || $COLB=="DESCRIPCIÓN" || $COLC=="UNIDAD" || $COLD=="RENDIM" || $COLE=="PRECIO" || $COLI=="CIUDAD" || $COLJ=="TIPO PROYECTO" || $COLC=="%" || $COLC=="" || $COLB=="")
			{
				
			}
			else
			{
				$COLA = number_format($COLA,0,'','');
				if($COLD=="" || $COLD==NULL || $COLD<=0){$act = $COLB; $ins="";}else {$ins = $COLA;}
				//VERIFICAR UNIDADES 
				$veru = mysql_query("select uni_clave_int from unidades where UPPER(uni_codigo)=UPPER('".$COLC."') limit 1");
				$numu = mysql_num_rows($veru);
				if($numu>0){$datu = mysql_fetch_array($veru); $idu = $datu['uni_clave_int'];}
				else 
				{
				$ins = mysql_query("insert into unidades (uni_codigo,uni_nombre,uni_sw_activo,uni_usu_actualiz,uni_fec_actualiz) values('".$COLC."','".$COLC."',1,'".$usuario."','".$fecha."')"); 
				$idu = mysql_insert_id();
				}
			
				//VERIFICAR CIUDAD //DETARMENTO COD 36 PARA LAS CIUDADES QUE NO EXISTEN
				if($COLD=="" || $COLD==NULL || $COLD<=0)
				{
					$verc = mysql_query("select ciu_clave_int from ciudad where UPPER(ciu_nombre)=UPPER('".$COLI."') limit 1");
					$numc = mysql_num_rows($verc);
					if($numc>0){$datc = mysql_fetch_array($verc); $idc = $datc['ciu_clave_int'];}
					else 
					{		
					$insc = mysql_query("insert into ciudad (ciu_nombre,dep_clave_int,ciu_sw_activo,ciu_usu_actualiz,ciu_fec_actualiz) values('".$COLI."','36',1,'".$usuario."','".$fecha."')"); 
					$idc = mysql_insert_id();
					}
					//ACTUALIZA O INSERTA TIPO PROYECTO IF NO EXISTEN
					$vert = mysql_query("select tpp_clave_int from tipoproyecto where UPPER(tpp_nombre)=UPPER('".$COLJ."') limit 1");
					$numt = mysql_num_rows($vert);
					if($numt>0){$datt = mysql_fetch_array($vert); $tpp = $datt['tpp_clave_int'];}
					else 
					{		
					$inst = mysql_query("insert into tipoproyecto (tpp_nombre,tpp_sw_activo,tpp_usu_actualiz,tpp_fec_actualiz) values('".$COLJ."',1,'".$usuario."','".$fecha."')"); 
					$tpp = mysql_insert_id();
					}
					//ACTUALIZA O INSERTA ACTIVIDADES IF NO EXISTEN
					$vera = mysql_query("select * from actividades where act_clave_int='".$act."'");
					$numa = mysql_num_rows($vera);
					if($numa>0)
					{
					    	
					   $update = mysql_query("update actividades act_nombre='".$COLB."',tpp_clave_int='".$tpp."',uni_clave_int='".$idu."',ciu_clave_int='".$idc."',act_usu_actualiz ='".$usuario."',act_fec_actualiz ='".$fecha."' where act_clave_int = '".$act."'");
					}
					else
					{
					   $inserta = mysql_query("insert into actividades (act_clave_int,act_nombre,tpp_clave_int,uni_clave_int,act_analisis,act_sw_activo,ciu_clave_int,act_usu_actualiz,act_fec_actualiz) values('".$act."','".$COLB."','".$tpp."','".$idu."','0','1','".$idc."','".$usuario."','".$fecha."')");
					}
				}
				else 
				{
					$idc = ""; $tpp="";
					if($COLF>0){$tpi=2;}else if($COLG>0){$tpi=1;}else if($COLH>0){$tpi=3;}else {$tpi=4;}
				//insercion de insumos y actividad asociadas
					$veri = mysql_query("select ins_clave_int from insumos where ins_clave_int = '".$COLA."' limit 1");
					$numi  = mysql_num_rows($veri); 
					if($numi>0)
					{
						$dati = mysql_fetch_array($veri);
						$idi = $dati['ins_clave_int'];
					}
					else
					{
						$ins = mysql_query("insert into insumos(ins_clave_int,ins_nombre,tpi_clave_int,uni_clave_int,ins_valor,ins_sw_activo,ins_usu_actualiz,ins_fec_actualiz) value('".$COLA."','".$COLB."','".$tpi."','".$idu."','".$COLE."','1','".$usuario."','".$fecha."')");
					$idi = mysql_insert_id();
					
					}
			  //verificar detalle
					$verif = mysql_query("select ati_clave_int from actividadinsumos where act_clave_int ='".$act."' and ins_clave_int ='".$idi."'");
				  $numvf = mysql_num_rows($verif);
				  if($numvf>0)
				  {
					  $datf = mysql_fetch_array($verif);
					  $idd = $datf['ati_clave_int'];
					  $updateai = mysql_query("update actividadinsumos set ati_rendimiento= '".$COLD."',act_valor='".$COLE."' where ati_clave_int = '".$idd."'");
					  
				  }
			  else{
				  $insertai = mysql_query("insert into actividadinsumos(act_clave_int,ins_clave_int,ati_rendimiento,ati_valor,ati_usu_actualiz,ati_fec_actualiz) values('".$act."','".$idi."','".$COLD."','".$COLF."','".$usuario."','".$fecha."')");
				  $idd = mysql_insert_id();			 
				 }
			  }		
			
			/*$veri = mysql_query("select * from pruebas where COLA='".$COLA."'");
			$numv = mysql_num_rows($veri);
				if($numv<=0)
				{
				  
				$ins = mysql_query("INSERT INTO pruebas(COLA,COLB,COLC,COLD,COLE,COLI,COLJ) values('".$COLA."','".$COLB."','".$idu."','".$COLD."','".$COLE."','".$COLI."','".$COLJ."')");
				}*/
		   }
		}
		$answer = array( 'answer' => 'Transferencia de archivos completada-'.$dir."-".$COLA);
		$json = json_encode( $answer );		
		echo $json;
	}
}
else 
{
    echo 'Sin archivos';
}

?>