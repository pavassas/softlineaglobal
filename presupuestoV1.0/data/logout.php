<?php
	session_start();
	setcookie("usIdentificacion", "", time() - 3600, "/");
	setcookie("usuario", "", time() - 3600, "/");
    setcookie("sistema", "", time() - 3600, "/");
	session_destroy();
	header("LOCATION:../index.php");
?>