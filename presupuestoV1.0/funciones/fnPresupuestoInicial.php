<?php
	include ("../data/Conexion.php");
	error_reporting(0);
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario = $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$unidades = 0;
	$con = mysql_query("select uni_clave_int from unidades where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysql_fetch_array($con);
		   $uni = $dat['uni_clave_int'];
		   $idsu[] = $uni;
	   }
	   $unidades = implode(',',$idsu);
	}
	$insumos = 0;
	$con = mysql_query("select ins_clave_int from insumos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysql_fetch_array($con);
		   $ins = $dat['ins_clave_int'];
		   $idsu[] = $ins;
	   }
	   $insumos = implode(',',$idsu);
	}
	
	$clientes = 0;
	$con = mysql_query("select per_clave_int from persona where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysql_fetch_array($con);
		   $ins = $dat['per_clave_int'];
		   $idsu[] = $ins;
	   }
	   $clientes = implode(',',$idsu);
	}
$tipoproyectos = 0;
	$con = mysql_query("select tpp_clave_int from tipoproyecto where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysql_fetch_array($con);
		   $ins = $dat['tpp_clave_int'];
		   $idsu[] = $ins;
	   }
	   $tipoproyectos = implode(',',$idsu);
	}
	
	$tipocontratos = 0;
	$con = mysql_query("select tpc_clave_int from tipocontrato where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysql_fetch_array($con);
		   $ins = $dat['tpc_clave_int'];
		   $idsu[] = $ins;
	   }
	   $tipocontratos = implode(',',$idsu);
	}
	$grupos = 0;
	$con = mysql_query("select gru_clave_int from grupos where est_clave_int = 3 and usu_clave_int = '".$idUsuario."'");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$idsu = array();
	   for($u=0;$u<$num;$u++)
	   {
		   $dat = mysql_fetch_array($con);
		   $ins = $dat['gru_clave_int'];
		   $idsu[] = $ins;
	   }
	   $grupos = implode(',',$idsu);
	}
	$opcion = $_POST['opcion'];
  
 if($opcion=="CARGARLISTAPRESUPUESTO")
  {
	  ?>
<script src="js/jspresupuestoinicial.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<div>
  <table id="tbpresupuesto" class="table table-bordered compact table-hover" width="100%" style="font-size:11px">
    <thead>
      <tr>
        <th class="dt-head-center">NOMBRE</th>
        <th class="dt-head-center">FECHA</th>
        <th class="dt-head-center">CLIENTE</th>
        <th class="dt-head-center">T.PROYECTO</th>
        <th class="dt-head-center">ESTADO</th>
        <th class="dt-head-center">% ADM</th>
        <th class="dt-head-center">% IMP</th>
        <th class="dt-head-center">% UTIL</th>
        <th class="dt-head-center">% IVA</th>
        <th class="dt-head-center">TOTAL</th>
        <th style="width:20px"></th>
        <th style="width:20px"></th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th></th>
        <th></th>
      </tr>
    </tfoot>
  </table>
</div>
<?PHP

  }
   else
   if($opcion=="INFORMES")
   {
	   $pre = $_POST['pre'];


	   //DEPURACION DE INFORMACION NO RELACIONADA EN EL PRESUPUESTO QUE QUEDO REGISTRADA

       $coninfo = mysql_query("select pr.pre_nombre nom,pr.pre_descripcion des,t.tpp_clave_int as tpp,e.esp_clave_int est,pe.per_clave_int cli,c.ciu_clave_int ciu,d.dep_clave_int dep,p.pai_clave_int pai,pr.pre_administracion as adm,pr.pre_imprevisto imp,pr.pre_utilidades uti,pr.pre_iva as iv,pr.pre_fecha as fe,pr.pre_sw_distribuido as di,pr.pre_coordinador as cor,pr.tpc_clave_int tpc from presupuesto pr left outer join persona pe on pe.per_clave_int = pr.per_clave_int join tipoproyecto t on t.tpp_clave_int = pr.tpp_clave_int join estadosproyecto e on e.esp_clave_int = pr.esp_clave_int join ciudad c on c.ciu_clave_int = pr.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where pr.pre_clave_int = '".$pre."' limit 1");
	   $datinfo = mysql_fetch_array($coninfo);
	
	   $tpp = $datinfo['tpp'];
	   $nom = $datinfo['nom'];
	   $des = $datinfo['des'];
	   $ciu = $datinfo['ciu'];
	   $pai = $datinfo['pai'];
	   $dep = $datinfo['dep'];
	   $est = $datinfo['est'];
	   $cli = $datinfo['cli'];
	   $adm = $datinfo['adm'];
	   $imp = $datinfo['imp'];
	   $uti = $datinfo['uti'];
	   $iva = $datinfo['iv'];
	   $fechap = $datinfo['fe'];
	   $distri = $datinfo['di'];
	   $cor = $datinfo['cor'];
	   $tpc = $datinfo['tpc'];
	  
	 ?>
<div class="box collapsed-box box-solid">
  <div class="box-header with-border">
    <h3 class="box-title text-primary">Información</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i> </button>
    </div>
    <!-- /.box-tools --> 
  </div>
  <!-- /.box-header -->
  <div class="box-body" id="micolass">
    <div class="form-group">
      <div class="col-md-4"><strong>Nombre:</strong>
        <div class="ui corner labeled input">
          <input type="text" id="txtnombre" name="txtnombre" disabled value="<?php echo $nom;?>" class="form-control input-sm" placeholder="Escribe aqui el nombre">
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
      <div class="col-md-4"><strong>Descripción:</span></strong>
        <input type="text" id="txtdescripcion" disabled name="txtdescripcion" class="form-control input-sm" value="<?php echo $des;?>" placeholder="Escribe aqui la descripcion">
      </div>
      <div class="col-md-4"><strong>Coordinador:</strong>
        <div class="ui corner labeled input">
          <select name="selcoordinador" id="selcoordinador" class="form-control input-sm" disabled>
            <option value="">--seleccione--</option>
            <?php
		  $con = mysql_query("select usu_clave_int,usu_nombre,usu_email from usuario u join perfil p on p.prf_clave_int = u.prf_clave_int where u.est_clave_int = 1 and UPPER(prf_descripcion) in('COORDINADOR','ADMINISTRADOR')");
		  while($dat = mysql_fetch_array($con))
		  {
			  $idp = $dat['usu_clave_int'];
			  $nom = $dat['usu_nombre'];
			  $ema = $dat['usu_email'];
		     ?>
            <option <?PHP if($idp==$cor){echo 'selected';}?> value="<?php echo $idp;?>"><?Php echo $nom." - ".$ema;?></option>
            <?php	
		  }
		  ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-4"><strong>Tipo de Proyecto:</strong>
        <div class="ui corner labeled input">
          <select name="seltipoproyecto"id="seltipoproyecto" class="form-control input-sm" disabled>
            <option value="">--seleccione--</option>
            <?PHP
		 $con = mysql_query("select tpp_clave_int,tpp_nombre from tipoproyecto where est_clave_int = 1 or tpp_clave_int in(".$tipoproyectos.") or tpp_clave_int = '".$tpp."'");
		 while($dat = mysql_fetch_array($con))
		 {
		    $idp = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
            <option <?php if($tpp==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
      <div class="col-md-4"><strong>Estado Presupuesto:</strong>
        <div class="ui corner labeled input">
          <select name="selestado" id="selestado" class="form-control input-sm" disabled>
            <option value="">--seleccione--</option>
            <?php 
		 $con = mysql_query("select esp_clave_int,esp_nombre from estadosproyecto where est_clave_int = 1");
		 while($dat = mysql_fetch_array($con))
		 {
			 $ides = $dat['esp_clave_int'];
			 $nom = $dat['esp_nombre'];
			
		    ?>
            <option <?php if($ides==$est){echo 'selected';}?>  value="<?Php echo $ides;?>"><?php echo $nom;?></option>
            <?php
		 }
		 ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
      <div class="col-md-4"><strong>Cliente:</strong>
        <div class="ui corner labeled input">
          <select name="selcliente" id="selcliente" class="form-control input-sm" disabled>
            <option value="">--seleccione--</option>
            <?php
		   if($perfil=="ADMINISTRADOR")
		  {
		  $con = mysql_query("select per_clave_int,per_documento,per_nombre,per_apellido,per_razon from persona where per_tipo_per = 1 and est_clave_int = 1");
		  }
		  else
		  {
			   $con = mysql_query("select per_clave_int,per_documento,per_nombre,per_apellido,per_razon from persona where per_tipo_per = 1 and est_clave_int = 1 or per_clave_int in(".$clientes.")");
		  }
		  while($dat = mysql_fetch_array($con))
		  {
			  $idc = $dat['per_clave_int'];
			  $doc = $dat['per_documento'];
			  $nom = $dat['per_nombre'];
			  $ape = $dat['per_apellido'];
			  $cliente = $dat['per_razon']
		     ?>
            <option <?PHP if($idc==$cli){echo 'selected';}?> value="<?php echo $idc;?>"><?Php echo $cliente." - ".$doc;?></option>
            <?php
	
		  }
		  ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-4"><strong>Pais:</strong>
        <div class="ui corner labeled input">
          <select name="selpais" id="selpais" class="form-control input-sm" onChange="cargardepartamento('selpais','seldepartamento',1)" disabled>
            <option value="">--seleccione--</option>
            <?php
        $con = mysql_query("select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysql_fetch_array($con))
        {
        ?>
            <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
            <?php
        }
        ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
      <div class="col-md-4"><strong>Departamento:</strong>
        <div class="ui corner labeled input">
          <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')" disabled>
            <option value="">--seleccione--</option>
            <?php
            $con = mysql_query("select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and est_clave_int = 1");
            while($dat = mysql_fetch_array($con))
            {
            ?>
            <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
            <?php
            }
            ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
      <div class="col-md-4"><strong>Ciudad:</strong>
        <div class="ui corner labeled input">
          <select name="selciudad" id="selciudad" class="form-control input-sm" disabled>
            <option value="">--seleccione--</option>
            <?php
        $con = mysql_query("select ciu_clave_int,ciu_nombre from ciudad where dep_clave_int ='".$dep."' and est_clave_int = 1");
        while($dat = mysql_fetch_array($con))
        {
        ?>
            <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
            <?php
        }
        ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-4"><strong>Tipo de Contrato:</strong>
        <div class="ui corner labeled input">
          <select name="seltipocontrato"id="seltipocontrato" class="form-control input-sm">
            <option value="">--seleccione--</option>
            <?PHP
		 $con = mysql_query("select tpc_clave_int,tpc_nombre from tipocontrato where est_clave_int = 1 or tpc_clave_int in(".$tipocontratos.")");
		 while($dat = mysql_fetch_array($con))
		 {
		    $idp = $dat['tpc_clave_int'];
			$nom = $dat['tpc_nombre'];
			?>
            <option <?php if($tpc==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
            <?Php	
		 }
		 ?>
          </select>
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
      <div class="col-md-4"><strong>Fecha Inicial:</strong>
        <div class="ui corner labeled input">
          <input type="date" name="fecha" id="fecha" class="form-control input-sm" disabled value="<?php echo $fechap;?>">
          <div class="ui corner label"> <i class="asterisk icon"></i> </div>
        </div>
      </div>
      <div class="col-md-1"><br>
        <a role="button" name="" class="btn btn-primary" onClick="CRUDPRESUPUESTO('GUARDAREDICION2','','')">Guardar</a> </div>
    </div>
    <!-- /.box-body --> 
  </div>
  <!-- /.box-body --> 
</div>
<div class="col-md-12">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-left" id="nav1">
      <li class="active"> <a onclick="CRUDPRESUPUESTOINICIAL('INFORMERECURSOS','','','','')" data-toggle="tab" aria-expanded="true"><strong>RECURSOS</strong></a> </li>
      <li class="" style="display: none"> <a onclick="CRUDPRESUPUESTOINICIAL('INFORMEAPU','','','','')" data-toggle="tab" aria-expanded="false"><strong>APU'S </strong></a> </li>
      <li class="" style="display: none"> <a onclick="CRUDPRESUPUESTOINICIAL('INFORMESUB','','','','')" data-toggle="tab" aria-expanded="false"><strong>SUBANALISIS </strong></a> </li>
      <li class=""> <a onclick="CRUDPRESUPUESTOINICIAL('INFORMEPAR','','','','')" data-toggle="tab" aria-expanded="false" title="PARTIDA PRESUPUESTAL"><strong>PARTIDA PPTAL</strong></a> </li>
      <li class=""> <a onclick="CRUDPRESUPUESTOINICIAL('INFORMEEJECUTIVO','','','','')" data-toggle="tab" aria-expanded="false" title="INFORME EJECUTIVO"><strong>INF.EJECUTIVO</strong></a> </li>
      <li class=""> <a onclick="CRUDPRESUPUESTOINICIAL('ALCANCEPRESUPUESTADO','','','','')" data-toggle="tab" aria-expanded="false" title="ALCANCE PRESUPUESTADO"><strong>ALCANCE PRESUPUESTADO</strong></a> </li>
      <li class=""> <a onclick="CRUDPRESUPUESTOINICIAL('CONTROLEGRESO','','','','')" data-toggle="tab" aria-expanded="false" title="CONTROL PRESUPUESTAL POR EGRESO"><strong>CTRL PPTAL EGRESO</strong></a> </li>
      <li class=""> <a onclick="CRUDPRESUPUESTOINICIAL('PAGOSACTAS','','','','')" data-toggle="tab" aria-expanded="false" title="CONTROL PAGOS DE ACTAS"><strong>CTRL PAGOS ACTAS</strong></a> </li>
      <li class=""> <a onclick="CRUDPRESUPUESTOINICIAL('ESTADOSCONTRATOS','','','','')" data-toggle="tab" aria-expanded="false" title="ESTADO DE CONTRATOS"><strong>EST.CONTRATO</strong></a> </li>
      <li class=""><abbr> <a onclick="CRUDPRESUPUESTOINICIAL('CARGARLISTAPRESUPUESTO','','','','')" data-toggle="tab" aria-expanded="false" class="btn btn-info btn-sm" title="LISTADO DE PRESUPUESTOS"><i class="glyphicon glyphicon-th-list"></i></a></abbr> </li>
      <li class="bg-info"><abbr> <a onclick="CRUDPRESUPUESTO('GENERARINFORME','','','','','')" data-toggle="tab" aria-expanded="false" class="btn btn-default btn-sm" title="GENERAR INFORME"><i class="fa  fa-file-text-o"></i></a></abbr> </li>
      <li class="bg-warning"> <abbr> <a onclick="CRUDPRESUPUESTO('CARGARLISTARESTAURAR','','','','','')"  class="btn btn-default btn-sm" title="Restaurar Presupuesto"><i class="glyphicon glyphicon-retweet"></i></a> </abbr> </li>
    </ul>
  </div>
</div>
<?php
	echo "<style onload=CRUDPRESUPUESTOINICIAL('INFORMERECURSOS','','','','')></style>";
	 ?>
<input type="hidden" id="ALCSELECCIONADO" value="0">
<input type="hidden" id="RECSELECCIONADO" value="0">
<input type="hidden" id="PARSELECCIONADO" value="0">
<input type="hidden" id="PARVERSELECCIONADO" value="0">
<input type="hidden" id="ESTCONTSELECCIONADO" value="0">
       <input type="hidden" id="EGRESELECCIONADO" value="0">
<div class="col-md-12" id="tablainformes" style="display: none"></div>
<div class="col-md-12" id="tablainformes2" style="display: none"></div>
<div class="col-md-12" id="tablainformesalcance" style="display:none"></div>
<div class="col-md-12" id="tablainformescontratos" style="display:none"></div>
<div class="col-md-12" id="tablainformesegreso" style="display:none"></div>
<div class="col-md-12" id="tablainformesrecursos"></div>
<?PHP
   }
   else
   if($opcion=="ALCANCEPRESUPUESTADO")
  {
		$pre = $_POST['id'];
		$con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
		$dat = mysql_fetch_array($con);   
		$adm = $dat['pre_administracion'];
		$iva = $dat['pre_iva'];
		$imp = $dat['pre_imprevisto'];
		$uti = $dat['pre_utilidades'];
		$apli = $dat['apli'];
		
        $cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$pre."'");
        $date = mysql_fetch_array($cone);
        if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else {  $tote = $date['tote'];}
        if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else {  $toti = $date['toti'];}

        $consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."'");
        $datsum = mysql_fetch_array($consu);
        if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
		$totalc = $totalc + $toti;
		
			
		$totadm = ($totalc * $adm)/100;
		$totimp = ($totalc * $imp)/100;
		$totuti = ($totalc * $uti)/100;
		if($apli==0){ $totivai = ($totuti * $iva)/100;}else{ $totivai = (($totadm+$totimp+$totuti) * $iva)/100; }
		$totpre = $totalc + $totadm + $totimp + $totuti + $totivai;
		
		
		$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$pre."' limit 1");
		$dat = mysql_fetch_array($con);   
		$adma = $dat['pri_administracion'];
		$ivaa = $dat['pri_iva'];
		$impa = $dat['pri_imprevisto'];
		$utia = $dat['pri_utilidades'];
		$consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."'");		
		$datsum = mysql_fetch_array($consu);
		if($datsum['totc']=="" || $datsum['totc']==NULL){$totalca=0;}else{$totalca=$datsum['totc'];}
		$totalca = $totalca + $tote;//se le suma lo enlazado
		
		$totadma = ($totalca * $adma)/100;
		$totimpa = ($totalca * $impa)/100;
		$totutia = ($totalca * $utia)/100;
		if($apli==0){ $totivaa = ($totutia * $ivaa)/100; }else{ $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
		$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa ;
		
		$alccos = $totalc - $totalca;
		$alcpre = $totpre - $totprea;
		$alcadm = $totadm - $totadma;
		$alcimp = $totimp - $totimpa;
		$alcuti = $totuti - $totutia;
		$alciva = $totivai - $totivaa;	 
	  ?>
<div class="form-group">
  <div class="col-md-12"> 
    <script src="js/jsagregadosinicial.js"></script> 
    <script src="js/jseventosinicial.js"></script>
    <div>
      <table id="tbgruagregados" class="persist-area table table-bordered table-condensed compact table-hover" width="100%" style="font-size:11px">
        <thead class="persist-header">
          <tr>
            <th rowspan="2" class="dt-head-center" style='width:20px; vertical-align:middle'></th>
            <th rowspan="2" class="dt-head-center" style='width:20px; vertical-align:middle'>CAP</th>
            <th rowspan="2" class="dt-head-center" style='width:20px; vertical-align:middle'>ITEM</th>
            <th rowspan="2" class="dt-head-center" style="vertical-align: middle; width: 285px; min-width: 285px;">DESCRIPCIÓN</th>
            <th rowspan="2" class="dt-head-center" style="width: 40px; vertical-align: middle; min-width: 40px;" >UN</th>
            <th colspan="3" class="dt-head-center info" style="width: 210px; min-width: 210px;">INICIALES</th>
            <th colspan="5" class='dt-head-center danger' style="width: 339px; min-width: 339px;">ACTUALES</th>
            <th rowspan="2" class="dt-head-center success" style="vertical-align:middle; width:80px; min-width:80px">CAMBIO<br>
              ALCANCE</th>
            <th class="dt-head-center"></th>
          </tr>
          <tr>
            <th class="dt-head-center info" style='width:40px'>CANT</th>
            <th class="dt-head-center info" style='width:60px'>VR<br>
              UNIT</th>
            <th class="dt-head-center info" style='width:60px'>PPTO.INI</th>
            <th class='dt-head-center danger' style='width:40px'>CANT<br>
              PROYE</th>
            <th class='dt-head-center danger' style='width:40px'>CANT<br>
              EJECU</th>
            <th class='dt-head-center danger' style='width:40px'>CANT<br>
              FALT</th>
            <th class='dt-head-center danger' style='width:60px'>VR.UNIT</th>
            <th class="dt-head-center danger" style='width:60px'>PPTO</th>
            <th class="dt-head-center"></th>
          </tr>
        </thead>
        <tbody>
          <?php
			   
               $con = mysql_query("select d.prg_clave_int idd, g.gru_clave_int id,g.gru_nombre as nom from  presupuesto p join pre_grupo d on p.pre_clave_int = d.pre_clave_int join grupos g on d.gru_clave_int = g.gru_clave_int  where p.pre_clave_int = '".$pre."'   order by gru_orden,nom");
			   while($dat = mysql_fetch_array($con))
			   {
				   $idd = $dat['idd'];
				   $nom = $dat['nom'];
				   $gru = $dat['id'];
				   //total sub analisis
				    $consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."'");
				    $datsum = mysql_fetch_array($consu);
				    if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}
				
				    $consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."'");
				    $datsum = mysql_fetch_array($consu);
				    if($datsum['totc']=="" || $datsum['totc']==NULL){$totala=0;}else{$totala=$datsum['totc'];}
			
					$cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."'");
					$date = mysql_fetch_array($cone); 					
					if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else {  $tote = $date['tote'];}	
					if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else {  $toti = $date['toti'];}
					$totala = $totala + $tote;
					$total = $total + $toti;
					$totalalcc =  $total - $totala;
					
				    /*if($total>0)
					{*/
				   ?>
          <tr id="gru<?php echo $idd;?>" style="background-color:rgba(215,215,215,1.00); font-weight:bold">
            <td class="details-control"></td>
            <td></td>
            <td></td>
            <td data-title="Nombre" style="width:300px"><?Php echo $nom;?></td>
            <td></td>
            <td></td>
            <td></td>
            <td data-title="Valor Total"><div id="divtotgrui<?php echo $gru;?>" class="currency" title="<?php echo $total; ?>">$<?php echo number_format($total,2,'.',',');?></div></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td data-title=""><div id="divtotgrua<?php echo $gru;?>" class="currency" title="<?php echo $totala;?>">$<?php echo number_format($totala,2,'.',',');?></div></td>
            <td data-title="" style="width:100px"><div id="divtotgrual<?php echo $gru;?>" class="currency" title="<?php echo $totalalcc;?>">$ <?php echo number_format($totalalcc,2,'.',',');?></div></td>
            <td><?php echo $gru;?></td>
          </tr>
          <?php
				//	}
					
			   }
			   ?>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="15" class="dt-head-right"></th>
          </tr>
          <tr>
            <th colspan="7" class="dt-head-right" style="background-color:rgba(163,163,163,1.00); color:#FFF; font-weight:bold">SUBTOTAL OBRA CIVIL</th>
            <th class="dt-head-right info"><span id="totcos" class="currency">$<?php echo number_format($totalc,2,'.',',');?></span></th>
            <th colspan="4" class="dt-head-right" style="background-color:rgba(163,163,163,1.00); color:#FFF; font-weight:bold">SUBTOTAL OBRA CIVIL</th>
            <th class="dt-head-right danger"><span id="totcosp" class="currency">$<?php echo number_format($totalca,2,'.',',');?></span></th>
            <th class="dt-head-right" style="background-color:rgba(215,215,215,1.00); font-weight:bold"><span id="alccos" class="currency">$<?php echo number_format($alccos,2,'.',',');?></span></th>
            <th class="dt-head-center"></th>
          </tr>
          <tr>
            <th colspan="6" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>ADMINISTRACION</th>
            <th class="dt-head-center"><label id="administracion" > <?php echo $adm;?> %</label></th>
            <th class="dt-head-right info"><span id="totadm" class="currency">$<?php echo number_format($totadm,2,'.',',');?></span></th>
            <th colspan="3" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>ADMINISTRACIÓN</th>
            <th class="dt-head-center"><div class="input-group">
                <input type="text" step="1" max="100" min="0" id="administracionp" class="form-control input-sm" value="<?php echo $adma?>"  onKeyPress="return NumCheck(event, this)" onchange="actualizarcostoi()" style="text-align:right" >
                <span class="input-group-addon">%</span></div></th>
            <th class="dt-head-right danger"><span id="totadmp" class="currency">$<?php echo number_format($totadma,2,'.',',');?></span></th>
            <th class="dt-head-right" style="background-color:rgba(215,215,215,1.00); font-weight:bold"><span id="alcadm" class="currency">$<?php echo number_format($alcadm,2,'.',',');?></span></th>
            <th class="dt-head-center"></th>
          </tr>
          <tr>
            <th colspan="6" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>IMPREVISTO</th>
            <th class="dt-head-center"><label id="administracion" > <?php echo $imp;?> %</label></th>
            <th class="dt-head-right info"><span id="totimp" class="currency">$<?php echo number_format($totimp,2,'.',',');?></span></th>
            <th colspan="3" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>IMPREVISTO</th>
            <th class="dt-head-center"><div class="input-group">
                <input type="text" step="1" max="100" min="0" id="imprevistop" class="form-control input-sm" value="<?php echo $impa?>" onKeyPress="return NumCheck(event, this)" onchange="actualizarcostoi()" style="text-align:right">
                <span class="input-group-addon">%</span></div></th>
            <th class="dt-head-right danger"><span id="totimpp" class="currency">$<?php echo number_format($totimpa,2,'.',',');?></span></th>
            <th class="dt-head-right" style="background-color:rgba(215,215,215,1.00); font-weight:bold"><span id="alcimp" class="currency">$<?php echo number_format($alcimp,2,'.',',');?></span></th>
            <th class="dt-head-center"></th>
          </tr>
          <tr>
            <th colspan="6" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>UTILIDADES </th>
            <th class="dt-head-center"><label id="administracion" > <?php echo $uti;?> %</label></th>
            <th class="dt-head-right info"><span id="totuti" class="currency">$<?php echo number_format($totuti,2,'.',',');?></span></th>
            <th colspan="3" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>UTILIDADES</th>
            <th class="dt-head-center"><div class="input-group">
                <input type="text" step="1" max="100" min="0" id="utilidadesp" class="form-control input-sm" value="<?php echo $utia;?>" onKeyPress="return NumCheck(event, this)"  onchange="actualizarcostoi()" style="text-align:right">
                <span class="input-group-addon">%</span></div></th>
            <th class="dt-head-right danger"><span id="totutip" class="currency">$ <?php echo number_format($totutia,2,'.',',');?></span></th>
            <th class="dt-head-right" style="background-color:rgba(215,215,215,1.00); font-weight:bold"><span id="alcuti" class="currency">$ <?php echo number_format($alcuti,2,'.',',');?></span></th>
            <th class="dt-head-center"></th>
          </tr>
          <tr>
            <th colspan="6" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>IVA</th>
            <th class="dt-head-center"><label id="administracion" > <?php echo $iva;?> %</label></th>
            <th class="dt-head-right info"><span id="totiva" class="currency">$<?php echo number_format($totivai,2,'.',',');?></span></th>
            <th colspan="3" class="dt-head-right" style='background-color:rgba(215,215,215,1.00)'>IVA</th>
            <th class="dt-head-center"><div class="input-group">
                <input type="text" step="1" max="100" min="0" id="ivap" class="form-control input-sm" value="<?php echo $ivaa;?>" onKeyPress="return NumCheck(event, this)" onchange="actualizarcostoi()" style="text-align:right">
                <span class="input-group-addon">%</span></div></th>
            <th class="dt-head-right danger"><span id="totivap" class="currency">$<?php echo number_format($totivaa,2,'.',',');?></span></th>
            <th class="dt-head-right" style="background-color:rgba(215,215,215,1.00); font-weight:bold"><span id="alciva" class="currency">$ <?php echo number_format($alciva,2,'.',',');?></span></th>
            <th class="dt-head-center"></th>
          </tr>
          <tr>
            <th colspan="7" class="dt-head-right" style="background-color:rgba(163,163,163,1.00); color:#FFF; font-weight:bold">TOTAL PRESUPUESTO DE OBRA</th>
            <th class="dt-head-right info"><span id="totpre" class="currency">$ <?php echo number_format($totpre,2,'.',',');?></span></th>
            <th colspan="4" class="dt-head-right" style="background-color:rgba(163,163,163,1.00); color:#FFF; font-weight:bold">TOTAL PRESUPUESTO DE OBRA</th>
            <th class="dt-head-right danger"><span id="totprep" class="currency">$ <?php echo number_format($totprea,2,'.',',');?></span></th>
            <th class="dt-head-right" style="background-color:rgba(215,215,215,1.00); font-weight:bold"><span id="alcpre" class="currency">$ <?php echo number_format($alcpre,2,'.',',');?></span></th>
            <th class="dt-head-center"></th>
          </tr>
        </tfoot>
      </table>
      <?php
		  if($apli==0)
			{  
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
			$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
			$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);
			}
			else
			{						   
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
			$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".

			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
			$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);					 
			}	
			
			/*$cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$pre."'");
			$date = mysql_fetch_array($cone); 
			if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else {  $tote = $date['tote'];}	
			if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else {  $toti = $date['toti'];}	*/
		
			$alcanceevento= $totale - $totalea;
			$totalobra = $totpre + $totale;
			$totalobraa = $totprea + $totalea;//$tote = totalenlazado
			$alcanceobra = $totalobra - $totalobraa;
		  ?>
      <table id="tbeventos"  class="table table-bordered table-condensed compact" width="100%" style="font-size:11px">
        <thead>
          <tr>
            <th colspan="13" class="dt-head-center" style='vertical-align:middle'>EVENTOS DE CAMBIO</th>
          </tr>
          <tr>
            <th class="dt-head-center" style="width:20px"></th>
            <th class="dt-head-center" style="width:20px">CAP</th>
            <th class="dt-head-center" style="width:20px">ITEM</th>
            <th class="dt-head-center" style="vertical-align: middle; width: 285px; min-width: 285px;">DESCRIPCIÓN</th>
            <th class="dt-head-center" style="width: 40px; vertical-align: middle; min-width: 40px;">UN</th>
            <th colspan="3" class="dt-head-center info" style="width: 210px; min-width: 210px;">INICIALES</th>
            <th colspan="4" class='dt-head-center danger' style="width: 339px; min-width: 339px;">ACTUALES</th>
            <th class="dt-head-center success" style="vertical-align:middle; width:80px; min-width:80px">CAMBIO<br>
              ALCANCE</th>
          </tr>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th style='width:40px'></th>
            <th style='width:60px'></th>
            <th style='width:60px'></th>
            <th style='width:40px'></th>
            <th style='width:40px'></th>
            <th style='width:60px'></th>
            <th style='width:60px'></th>
            <th></th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th colspan="12" class="dt-head-center" style='vertical-align:middle'><input type='text' id='selectcapitulo' placeholder='CAPITULOS EXTRA A AGREGAR'   onkeyup="filterGlobale('tbcapituloseventos','selectcapitulo')" name='selectcapitulo'  class='form-control input-sm'  style='width:100%'/></th>
            <th><a class="btn btn-primary btn-xs" onClick="CRUDPRESUPUESTO('GUARDARCAPITULOEVENTO','<?PHP echo $pre;?>','','','','')"><i class="fa fa-plus"></i>Agregar</a></th>
          </tr>
          <tr>
            <th colspan="7" class="dt-head-right" style='vertical-align:middle'>TOTAL EVENTOS DE CAMBIO</th>
            <th class="dt-head-right info" style='vertical-align:middle'><div class="currency" id="totaleventoi"><?php echo $totale;?></div></th>
            <th colspan="3" class="dt-head-right " style='vertical-align:middle'>TOTAL EVENTOS DE CAMBIO</th>
            <th class="dt-head-right danger" style='vertical-align:middle'><div class="currency" id="totaleventoa"><?php echo $totalea;?></div></th>
            <th  class="dt-head-right " ><div class="currency" id="totaleventoal"><?php echo $alcanceevento;?></div></th>
          </tr>
          <tr>
            <th colspan="13" class="dt-head-right" style='vertical-align:middle'></th>
          </tr>
          <tr>
            <th colspan="7" class="dt-head-right" style='vertical-align:middle; '>TOTAL PRESUPUESTO OBRA + EVENTOS DE CAMBIO</th>
            <th class="dt-head-center info" style='vertical-align:middle'><div class="currency" id="totalobraei"><?php echo $totalobra;?></div></th>
            <th colspan="3" class="dt-head-right " style='vertical-align:middle'>TOTAL PRESUPUESTO OBRA + EVENTOS DE CAMBIO</th>
            <th class="dt-head-right danger" style='vertical-align:middle'><div class="currency" id="totalobraea"><?php echo $totalobraa;?></div></th>
            <th class="dt-head-right"><div class="currency" id="totalobraeal"><?php echo $alcanceobra;?></div></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<?PHP
	 
  }
  
  else if($opcion=="LISTACAPITULOS")
  {
	$pre = $_POST['pre'];
	$gru = $_POST['gru'];
	$con = mysql_query("select d.pgc_clave_int idd, c.cap_clave_int id,c.cap_nombre as nom,c.cap_codigo as codc,d.pgc_codigo as co,p.pre_apli_iva apli,d.pgc_creacion as cre from  presupuesto p join pre_gru_capitulo d on d.pre_clave_int = p.pre_clave_int join capitulos c on d.cap_clave_int = c.cap_clave_int  where p.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' order by idd");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		while($dat = mysql_fetch_array($con))
		{
            $idd = $dat['idd'];
            $cap = $dat['id'];
            $nom = $dat['nom'];
            $apli = $dat['apli'];
            $cre = $dat['cre'];
            $codc = number_format($dat['co'],2,'.','');

            $consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."'");
            $datsum = mysql_fetch_array($consu);
            if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}

            $consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."'");
            $datsum = mysql_fetch_array($consu);
            if($datsum['totc']=="" || $datsum['totc']==NULL){$totala=0;}else{$totala=$datsum['totc'];}
		
		$cone = mysql_query("SELECT sum(enl_can_ini*enl_val_ini) as total,sum(enl_can_act*enl_val_act) as totala FROM pre_enlazado WHERE pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."'");
		$date = mysql_fetch_array($cone);
		$totalen = (double)$date['total']; $totalaen = (double)$date['totala'];
		$total = $total + $totalen; $totala = $totala + $totalaen;

		$totalalcc =  $total - $totala;
				
		$datos[] =  array("grupo"=>$gru,"capitulo"=>$cap,"nombre"=>$nom,"total"=>number_format($total,2,'.',','),"totala"=>number_format($totala,2,'.',','),"totalalc"=>number_format($totalalcc,2,'.',','),"idd"=>$idd,"res"=>"si","codc"=>$codc,"cre"=>$cre,"totalt"=>$total,"totalat"=>$totala,"totalalct"=>$totalalcc,"totalen"=>$totalen,"totalaen"=>$totalaen);
		}
	}
	else
	{
	$datos[] =  array("grupo"=>0,"capitulo"=>0,"nombre"=>"","total"=>0,"idd"=>0,"res"=>"no","codc"=>"","cre"=>$cre);
	}	   
	echo json_encode($datos);  
  }
   else if($opcion=="LISTAACTIVIDADES")
  {
     $pre = $_POST['pre'];
	 $gru = $_POST['gru'];
	 $cap = $_POST['cap'];
	 $conc = mysql_query("select pgc_codigo,pre_apli_iva apli from pre_gru_capitulo d join presupuesto p on p.pre_clave_int = d.pre_clave_int where p.pre_clave_int = '".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."'");
	 $datc = mysql_fetch_array($conc);
	 $codc = $datc['pgc_codigo'];
	 $apli = $datc['apli'];
	 
	 $conp = mysql_query("select est_clave_int from presupuesto where pre_clave_int = '".$pre."'");
	 $datp = mysql_fetch_array($conp); $estado = $datp['est_clave_int'];
	 //$con = mysql_query("")
	$con = mysql_query("select d.pgca_clave_int idd, d.cap_clave_int idc,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,d.pgca_cantidad cant,d.pgca_cant_proyectada cantp,d.pgca_cant_ejecutada cante,d.pgca_valor_act vala,d.pgca_valor_act valaA,ciu_nombre,tpp_nombre,d.pgca_item as Item,d.pgca_creacion as cre  from  pre_gru_cap_actividad  d join actividades a on a.act_clave_int = d.act_clave_int join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' order by idd asc");
	$num = mysql_num_rows($con);
	
	$cone = mysql_query("SELECT e.enl_clave_int idd,pr.pre_nombre,e.pre_clave_int,e.pre_enlazado,e.gru_clave_int,e.cap_clave_int,enl_can_ini,enl_can_act,enl_ejecutada,enl_val_ini,enl_val_act FROM pre_enlazado e JOIN presupuesto pr ON pr.pre_clave_int = e.pre_enlazado WHERE e.pre_clave_int = '".$pre."' and e.gru_clave_int = '".$gru."' and e.cap_clave_int = '".$cap."'");
	$nume = mysql_num_rows($cone);
	if($num>0 || $nume>0)
	{
		$res = "si";
		$k = 1;
		$ia = 0;
		for($i=1;$i<=$num;$i++)
		{
			$dat = mysql_fetch_array($con);
			$idd = $dat['idd'];
			$idca = $dat['id'];
			$nom = strtoupper($dat['nom']);
			$uni = $dat['uni'];
			$ida = $dat['ida'];				
			$cant= $dat['cant'];
			$cantp = $dat['cantp']; //if($cantp<=0){$cantp= $cant;}
			$cante = $dat['cante']; 
			$cre = $dat['cre'];
			$vala = $dat['vala'];
			$valact = $dat['valaa'];
					//$item = $dat['Item'];
					if($i<10){$item="".$codc.".0".$i;}else{$item="".$codc.".".$i;}
					
					if($cre==1){$item="EXT";}else {$ia = $i;}
					 
					  /* if($k<10){ $it = "0".$k;}else { $it = $k;}
					   $it = $cap.".".$it;
					   if($estado==5)
					   {
						   $it = $item;
					   }*/
					   //verificar si tiene apu asociado
					   $veriapu = mysql_query("select * from actividadinsumos where act_clave_int ='".$ida."'");
					   $numapu = mysql_num_rows($veriapu);
					   
					 if($apli==0)
					 {  
						 //SUBANALISIS
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
						",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
						" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
	
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						   
						//consulta del total de la suma de las actividades asignadas a este capitulo
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
						",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
						" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
						$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
						",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
						" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
						",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
						" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
						$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
					 }
					 else
					 {						   
						 //SUBANALISIS
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
						",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
						" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
	
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						   
						//consulta del total de la suma de las actividades asignadas a este capitulo
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
						",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
						" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
						$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
						",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
						" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
						",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
						" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
						$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);					 
					 }					
					
					$ciu = $dat['ciu_nombre'];
					$tpp = $dat['tpp_nombre'];
					$preact = $apua*$cantp;//PRESUPUESTO ACTUAL				
					$cantf = $cantp - $cante;//CANTIDAD FALTANTE			      
					$total = $apu * $cant;//PRESUPUESTO INICIAL
					//$apu = number_format($apu,2,'.',',');//APU INICIAL
					$alcance  = $total-$preact;	//CAMBIO EN EL ALCANCE			
					//$total = number_format($total,2,'.',',');
					//$preact = number_format($preact,2,'.',',');
					//$apua = number_format($apua,2,'.',',');//APU ACTUAL
					
					if($totals>0){ $suba = 1; }else { $suba = 0;}
				
					
				 
				   $datos[] =  array("iddetalle"=>$idd,"actividad"=>$nom,"total"=>$total,"unidad"=>$uni,"idactividad"=>$ida,"apu"=>number_format($apu,2,'.',','),"cantidad"=>number_format($cant,2,'.',','),"cantidadt"=>$cant,"cantidadp"=>$cantp,"cantidade"=>number_format($cante,2,'.',','),"cantidadf"=>$cantf,"valoract"=>number_format($apua,2,'.',','),"totala"=>number_format($preact,2,'.',','),"totalal"=>number_format($alcance,2,'.',','),"ciudad"=>$ciu,"tpp"=>$tpp,"res"=>$res,"items"=>$item,"numapu"=>$numapu,"suba"=>$suba,"codc"=>$codc,"cre"=>$cre,"aput"=>$apu,"valoractt"=>$apua,"totalat"=>$preact,"totalalt"=>$alcance,"totalt"=>$total,"itemp"=>"");
			      $k++;
				  
			   }
			   //presupuestos enlazados
			   $ia = $ia + 1;
			   for($ne=0;$ne<$nume;$ne++)
			   {
				   $date = mysql_fetch_array($cone);
					$idd = $date['idd'];
					$idca = 0;
					$nom = $date['pre_nombre'];
					$uni = "";
					$ida = 0;	
					$ciu = "";
					$tpp = "";			
					$cant= $date['enl_can_ini'];
					$cantp = $date['enl_can_act']; //if($cantp<=0){$cantp= $cant;}
					$cante = $date['enl_ejecutada'];
					$vali = $date['enl_val_ini'];
					$vala = $date['enl_val_act']; 
					$cre = 1;
					$suba = 0;
					//$codc = 0;
					//$item = $dat['Item'];
					if($i<10){$item="".$codc.".0".$ia;}else{$item="".$codc.".".$ia;}
					//$item="".$codc.".".$ia;
					//$item="PRE";
					
					$preact = $vala*$cantp;//PRESUPUESTO ACTUAL				
					$cantf = $cantp - $cante;//CANTIDAD FALTANTE			      
					$total = $vali * $cant;//PRESUPUESTO INICIAL
					$vali = number_format($vali,2,'.',',');//APU INICIAL
					$alcance  = $total-$preact;	//CAMBIO EN EL ALCANCE			
					$total = number_format($total,2,'.',',');
					$preact = number_format($preact,2,'.',',');
					$vala = number_format($vala,2,'.',',');//APU ACTUAL
					
					 
					  /* if($k<10){ $it = "0".$k;}else { $it = $k;}
					   $it = $cap.".".$it;
					   if($estado==5)
					   {
						   $it = $item;
					   }*/
					   //verificar si tiene apu asociado
					$numapu = 0;
					$datos[] =  array("iddetalle"=>$idd,"actividad"=>$nom,"total"=>$total,"unidad"=>$uni,"idactividad"=>$ida,"apu"=>$vali,"cantidad"=>number_format($cant,2,'.',','),"cantidadp"=>$cantp,"cantidade"=>number_format($cante,2,'.',','),"cantidadf"=>$cantf,"valoract"=>$vala,"totala"=>$preact,"totalal"=>$alcance,"ciudad"=>$ciu,"tpp"=>$tpp,"res"=>$res,"items"=>$item,"numapu"=>$numapu,"suba"=>$suba,"codc"=>$codc,"cre"=>$cre,"itemp"=>"PRE");  
					$ia++;
			   }
		 }
		  else
		  {
			$res = "no";
			$datos[] =  array("iddetalle"=>"","actividad"=>"","total"=>"","unidad"=>"","idactividad"=>"","apu"=>"","cantidad"=>"","cantidadp"=>"","cantidade"=>"","cantidadf"=>"","valoract"=>"","totala"=>"","totalal"=>"","ciudad"=>"","tpp"=>"","res"=>$res,"items"=>"","suba"=>0,"codc"=>$codc,"cre"=>$cre,"itemp"=>"");	  
		  }
			   
			   
	echo json_encode($datos);
  
  }
   else if($opcion=="LISTAACTIVIDADESE")//ACTIVIDADES EVENTOS DE CAMBIOS
  {
     $pre = $_POST['pre'];
	 $cap = $_POST['cap'];
	 $conc = mysql_query("select pev_codigo,pre_apli_iva apli from eventoscambio d join presupuesto p on p.pre_clave_int = d.pre_clave_int where p.pre_clave_int = '".$pre."'  and pev_clave_int = '".$cap."'");
	 $datc = mysql_fetch_array($conc);
	 $codc = $datc['pev_codigo'];
	 $apli = $datc['apli'];
	 
	 $conp = mysql_query("select est_clave_int from presupuesto where pre_clave_int = '".$pre."'");
	 $datp = mysql_fetch_array($conp); $estado = $datp['est_clave_int'];
	 //$con = mysql_query("")
	$con = mysql_query("select d.pce_clave_int idd, d.pev_clave_int idc,a.act_clave_int as ida,a.act_nombre as nom,u.uni_codigo as uni,d.pce_cantidad cant,d.pce_cant_proyectada cantp,d.pce_cant_ejecutada cante,ciu_nombre,tpp_nombre from  pre_cap_actividad  d join actividades a on a.act_clave_int = d.act_clave_int join unidades u on u.uni_clave_int = a.uni_clave_int join tipoproyecto t on t.tpp_clave_int = a.tpp_clave_int join ciudad c on c.ciu_clave_int = a.ciu_clave_int where d.pre_clave_int = '".$pre."'  and d.pev_clave_int = '".$cap."' order by idd asc");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$res = "si";
		$k = 1;
		for($i=1;$i<=$num;$i++)
		{
			$dat = mysql_fetch_array($con);
			$idd = $dat['idd'];
			$idca = $dat['id'];
			$nom = strtoupper($dat['nom']);
			$uni = $dat['uni'];
			$ida = $dat['ida'];				
			$cant= $dat['cant'];
			$cantp = $dat['cantp']; 
			$cante = $dat['cante']; 
					if($i<10){$item="".$codc.".0".$i;}else{$item="".$codc.".".$i;}
					 
					 
					   //verificar si tiene apu asociado
					   $veriapu = mysql_query("select * from actividadinsumos where act_clave_int ='".$ida."'");
					   $numapu = mysql_num_rows($veriapu);
					   
					 if($apli==0)
					 {  
						 //SUBANALISIS
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
						",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
						" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
	
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						   
						//consulta del total de la suma de las actividades asignadas a este capitulo
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
						",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
						" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
						$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
						",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
						" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
						",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
						" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
						$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
					 }
					 else
					 {						   
						 //SUBANALISIS
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
						",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
						" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
	
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						   
						//consulta del total de la suma de las actividades asignadas a este capitulo
						$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
						",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
						",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
						"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
						" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
						$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
						",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
						" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
						$totals = $totals + ($totads+$totims+$totuts+$totivs);  
						
						$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
						",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
						",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
						"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
						" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$ida."'");
						$datsu = mysql_fetch_array($consu);
						 $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
						if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
						$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);					 
					 }					
					
					$ciu = $dat['ciu_nombre'];
					$tpp = $dat['tpp_nombre'];
					$preact = $apua*$cantp;//PRESUPUESTO ACTUAL				
					$cantf = $cantp - $cante;//CANTIDAD FALTANTE			      
					$total = $apu * $cant;//PRESUPUESTO INICIAL
					$apu = number_format($apu,2,'.',',');//APU INICIAL
					$alcance  = $total-$preact;	//CAMBIO EN EL ALCANCE			
					$total = number_format($total,2,'.',',');
					$preact = number_format($preact,2,'.',',');
					$apua = number_format($apua,2,'.',',');//APU ACTUAL
					
					if($totals>0){ $suba = 1; }else { $suba = 0;}
				
					
				 
				   $datos[] =  array("iddetalle"=>$idd,"actividad"=>$nom,"total"=>$total,"unidad"=>$uni,"idactividad"=>$ida,"apu"=>$apu,"cantidad"=>number_format($cant,2,'.',','),"cantidadp"=>$cantp,"cantidade"=>number_format($cante,2,'.',','),"cantidadf"=>$cantf,"valoract"=>$apua,"totala"=>$preact,"totalal"=>$alcance,"ciudad"=>$ciu,"tpp"=>$tpp,"res"=>$res,"items"=>$item,"numapu"=>$numapu,"suba"=>$suba,"codc"=>$codc);
			      $k++;
				  
			   }
		 }
		  else
		  {
			$res = "no";
			$datos[] =  array("iddetalle"=>"","actividad"=>"","total"=>"","unidad"=>"","idactividad"=>"","apu"=>"","cantidad"=>"","cantidadp"=>"","cantidade"=>"","cantidadf"=>"","valoract"=>"","totala"=>"","totalal"=>"","ciudad"=>"","tpp"=>"","res"=>$res,"items"=>"","suba"=>0,"codc"=>$codc);	  
		  }
			   
			   
	echo json_encode($datos);
  
  }
  else if($opcion=="LISTAINSUMOSE")
  {
	  
     $id=$_POST['id'];
	 $act = $_POST['act'];
     $pre=$_POST['pre'];
     $cap=$_POST['cap'];
	 $gru = $_POST['gru']; 
	 $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
	 $data = mysql_fetch_array($cona);
	 $apli = $data['apli'];
	 
	$con = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$act."' order by idd ASC");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$res = "si";
	  while($dat = mysql_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = strtoupper($dat['nom']);
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val']; if($val=="null" || $val==NULL){$val=0;}
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   $tpl = $dat['tpl'];
				   $insu = $dat['id'];
				   
				   $con1 = mysql_query("select pgi_clave_int, pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_cap_act_insumo where pre_clave_int = '".$pre."'  and pev_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$insu."'");
				   $dato1 = mysql_fetch_array($con1);
				   $deti = $dato1['pgi_clave_int'];
				   $ren = $dato1['pgi_rend_ini']; if($ren=="null" || $ren==NULL){$ren=0;}
				   $valor = $dato1['pgi_vr_ini']; if($valor=="null" || $valor==NULL){$valor=0;}
				   $admini = $dato1['pgi_adm_ini'];
				   $impini = $dato1['pgi_imp_ini'];
				   $utiini = $dato1['pgi_uti_ini'];			
				   $ivaini = $dato1['pgi_iva_ini'];
				   

					$tadmini = ($valor*$admini)/100;
					$timpini = ($valor*$impini)/100;
					$tutiini = ($valor*$utiini)/100;
					if($apli==0) {$tivaini = ($tutiini*$ivaini)/100;}
					else {$tivaini = (($tadmini+$timpini+$tutiini)*$ivaini)/100;}
                   $valor = $valor +($tadmini+$timpini+$tutiini+$tivaini);
                   $totalcini = $ren*$valor;

				   $renact = $dato1['pgi_rend_act'];
				   $valact = $dato1['pgi_vr_act'];
				   $admact = $dato1['pgi_adm_act'];
				   $impact = $dato1['pgi_imp_act'];
				   $utiact = $dato1['pgi_uti_act'];			
				   $ivaact = $dato1['pgi_iva_act'];				  
				  
				   $unidad = $cod;
				   $material=0; $equipo = 0; $mano = 0;
				   $mate = 0; $equi = 0; $man = 0;				  
				   

					$tadmact = ($valact*$admact)/100;
					$timpact = ($valact*$impact)/100;
					$tutiact = ($valact*$utiact)/100;
					if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
					{ $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}
                   $valactu = $valact;
                   $valact = $valact +($tadmact+$timpact+$tutiact+$tivaact);
                   $totalc = $renact*$valact;
				   $totalact = ($totalcini)-($totalc);
				   if($totalact == ''){ $totalact = 0; }				   
				   
				   if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
				   $total = $material + $equipo + $mano;				   
				   //BOTONES
                   $incpe = (($valact - $valactu)/$valactu)*100;

				   $btnrendact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='rendac".$cap."a".$act."i".$insu."' name='' type='text' value='".$renact."'  min='0'  onKeyPress='return NumCheck(event, this)' onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARRECURSO','".$act."',".$insu.",'".$deti."','".$cap."')>";
				   $btnvalact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='valac".$cap."a".$act."i".$insu."' name=''  type='text' value='$".$valactu."' min=''  onKeyPress='return NumCheck(event, this)' onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARRECURSO','".$act."',".$insu.",'".$deti."','".$cap."') class='currency'>";
				   				   
				   $datos[] =  array("iddetalle"=>$idc,"insumo"=>$nom,"tipo"=>$tipo,"unidad"=>$unidad,"rendi"=>$ren,"valor"=>$valor,"valor1"=>number_format($valor,2,'.',','),"material"=>number_format($material,2,'.',','),"equipo"=>number_format($equipo,2,'.',','),"mano"=>number_format($mano,2,'.',','),"materiala"=>"<div id='materiale".$deti."' class='currency'>$".number_format($mate,2,'.',',')."</div>","equipoa"=>"<div id='equipoe".$deti."' class='currency'>$".number_format($equi,2,'.',',')."</div>","manoa"=>"<div id='manoe".$deti."' class='currency'>$".number_format($man,2,'.',',')."</div>","total"=>$total,"renact"=>$renact,"valact"=>$valactu,"totalact"=>"<div id='cambioe".$deti."' class='currency'>$".$totalact."</div>","res"=>$res,"insu"=>$insu,"REA"=>$btnrendact,"VAA"=>$btnvalact,"incpe"=>number_format($incpe,2,'.',',')."%","deti"=>$deti);
				   				
			   }
			   
			   $consulta = mysql_query("SELECT i.act_clave_int idc,i.act_nombre nom,	u.uni_codigo cod,s.pgi_rend_sub_ini ri,s.pgi_rend_sub_act ra FROM	actividades AS i JOIN pre_cap_act_sub_insumo s ON s.act_subanalisis = i.act_clave_int JOIN tipoproyecto AS t ON t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS u ON u.uni_clave_int = i.uni_clave_int WHERE s.pre_clave_int = '".$pre."' AND s.pev_clave_int = '".$cap."' AND s.act_clave_int = '".$act."' GROUP BY s.act_subanalisis ORDER BY i.act_nombre ASC");
			   $nums = mysql_num_rows($consulta);
			   if($nums>0)
			   {
				   while($dats = mysql_fetch_array($consulta))
				   {
					   $idc = $dats['idc'];
					   $insu = "S-".$idc;
					   $nom = $dats['nom'];
					   $tipo = "";
					   $unidad = $dats['cod'];
					   $ren = $dats['ri'];
					   $renact = $dats['ra'];
					   $tpl = 1;
					   $material=0; $equipo = 0; $mano = 0;
				       $mate = 0; $equi = 0; $man = 0;
					   if($apli==0)
					   {							
							$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
							" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");
							
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totals + ($totads+$totims+$totuts+$totivs);
							
							$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
							",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
							" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");		
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalc = $totals + ($totads+$totims+$totuts+$totivs);	
					   }
					   else
					   {						  
							$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
							",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
							"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
							"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
							" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");
							
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totals + ($totads+$totims+$totuts+$totivs);
							
							$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
							",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
							"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
							"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
							" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");		
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalc = $totals + ($totads+$totims+$totuts+$totivs);	
					   }
					   
						$valor = $totalcini;
						$valact = $totalc;
						$totalc = $renact*$valact;
						$totalcini = $ren*$valor;
						
						$totalact = ($totalcini)-($totalc);
						if($totalact == ''){ $totalact = 0; }				   
						
						if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
						$total = $material + $equipo + $mano;
						$btnrendact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remren".$pre."c".$cap."a".$act."s".$idc."' name='' type='text' value='".$renact."'  min='0'  onKeyPress='return NumCheck(event, this)' onchange=CRUDPRESUPUESTOINICIAL('ACTUALIZARSUB','".$act."','".$idc."','".$id."','".$cap."')>";
						$btnvalact = "<span class='currency'>".$valact."</span>";
						$datos[] =  array("iddetalle"=>$idc,"insumo"=>$nom,"tipo"=>$tipo,"unidad"=>$unidad,"rendi"=>$ren,"valor"=>$valor,"valor1"=>number_format($valor,2,'.',','),"material"=>number_format($material,2,'.',','),"equipo"=>number_format($equipo,2,'.',','),"mano"=>number_format($mano,2,'.',','),"materiala"=>"<div id='materiale".$pre."c".$cap."a".$act."s".$idc."' class='currency'>$".number_format($mate,2,'.',',')."</div>","equipoa"=>"<div id='equipoe".$pre."c".$cap."a".$act."s".$idc."' class='currency'>$".number_format($equi,2,'.',',')."</div>","manoa"=>"<div id='manoe".$pre."c".$cap."a".$act."s".$idc."' class='currency'>$".number_format($man,2,'.',',')."</div>","total"=>$total,"renact"=>$renact,"valact"=>$valact,"totalact"=>"<div id='cambioe".$pre."c".$cap."a".$act."s".$idc."' class='currency'>$".$totalact."</div>","res"=>$res,"insu"=>$insu,"REA"=>$btnrendact,"VAA"=>$btnvalact);						   
		    }
		}			   
	}
	else
	{
	  $res = "no";
	    $datos[] =  array("iddetalle"=>"","insumo"=>"","tipo"=>"","unidad"=>"","rendi"=>"","valor"=>"","valor1"=>"","material"=>"","equipo"=>"","mano"=>"","total"=>"","renact"=>"","valact"=>"","totalact"=>"","res"=>$res,"insu"=>"","REA"=>"","VAA"=>"");
	}
			   
	echo json_encode($datos);
  
   
  
  }
  else if($opcion=="LISTAINSUMOS")
  {
     $id=$_POST['id'];
	 $conact = mysql_query("select act_clave_int from pre_gru_cap_actividad where pgca_clave_int = '".$id."'");
	 $datact = mysql_fetch_array($conact);
	 $act = $datact['act_clave_int'];
     $pre=$_POST['pre'];
     $cap=$_POST['cap'];
	 $gru = $_POST['gru']; 
	 $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
	 $data = mysql_fetch_array($cona);
	 $apli = $data['apli'];
	 
	$con = mysql_query("select d.pgi_clave_int idd, i.ins_clave_int idi,i.ins_nombre AS nom,t.tpi_nombre tip,u.uni_codigo AS cod,
	u.uni_nombre nomu,i.ins_valor AS val,d.pgi_rend_ini,d.pgi_vr_ini,d.pgi_adm_ini,d.pgi_imp_ini,d.pgi_uti_ini,d.pgi_iva_ini,d.pgi_rend_act,d.pgi_vr_act,d.pgi_adm_act,d.pgi_imp_act,d.pgi_uti_act,d.pgi_iva_act,t.tpi_tipologia AS tpl,d.pgi_creacion as cre from  insumos i JOIN pre_gru_cap_act_insumo d ON (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where d.pre_clave_int = '".$pre."' AND d.gru_clave_int = '".$gru."' AND d.cap_clave_int = '".$cap."' AND d.act_clave_int = '".$act."'  order by idd ASC");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$res = "si";
	  while($dat = mysql_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = strtoupper($dat['nom']);
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val']; if($val=="null" || $val==NULL){$val=0;}
				   //$val1 = $dat['val1'];
				   //$des = $dat['des'];
				   $cod = $dat['cod'];
				   //$est = $dat['est'];
				   //$ren = $dat['ren'];
				   $tpl = $dat['tpl'];
				   $insu = $dat['idi'];
				   $cre  = $dat['cre'];
				  
				   
				   //$con1 = mysql_query("select pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$insu."'");
				   //$dato1 = mysql_fetch_array($con1);
				   $ren = $dat['pgi_rend_ini']; if($ren=="null" || $ren==NULL){$ren=0;}
				   $valor = $dat['pgi_vr_ini']; if($valor=="null" || $valor==NULL){$valor=0;}
				   $admini = $dat['pgi_adm_ini'];
				   $impini = $dat['pgi_imp_ini'];
				   $utiini = $dat['pgi_uti_ini'];			
				   $ivaini = $dat['pgi_iva_ini'];
				   

					$tadmini = ($valor*$admini)/100;
					$timpini = ($valor*$impini)/100;
					$tutiini = ($valor*$utiini)/100;
					if($apli==0) {$tivaini = ($tutiini*$ivaini)/100;}
					else {$tivaini = (($tadmini+$timpini+$tutiini)*$ivaini)/100;}

                   $valori = $valor +($tadmini+$timpini+$tutiini+$tivaini);
                    $totalcini = $ren*$valori;

                    $renact = $dat['pgi_rend_act'];
				   $valact = $dat['pgi_vr_act'];
				   $admact = $dat['pgi_adm_act'];
				   $impact = $dat['pgi_imp_act'];
				   $utiact = $dat['pgi_uti_act'];			
				   $ivaact = $dat['pgi_iva_act'];				  
				  
				   $unidad = $cod;
				   $material=0; $equipo = 0; $mano = 0;
				   $mate = 0; $equi = 0; $man = 0;				  
				   

					$tadmact = ($valact*$admact)/100;
					$timpact = ($valact*$impact)/100;
					$tutiact = ($valact*$utiact)/100;
					if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
					{ $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}
					
                   $valora = $valact +($tadmact+$timpact+$tutiact+$tivaact);
                   $totalc = $renact*$valora;

				   $totalact = ($totalcini)-($totalc);
				   if($totalact == ''){ $totalact = 0; }
				   //$totalact = money_format('%(#10.2n',$totalact);
                   //
                   $incp = (($valora - $valact)/$valact)*100;
				   
				   if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
				   $total = $material + $equipo + $mano;				   
				   //BOTONES
				   
				   $btnrendact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remren".$idc."' name='' type='text' value='".$renact."'  min='0'  onKeyPress='return NumCheck(event, this)' onchange=guardarcantidadrenact(".$idc.",".$pre.",".$gru.",".$cap.",this.value,".$act.",".$insu.")><br><span id='spanren".$idc."'></span>";
				   $btnvalact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remval".$idc."' name=''  type='text' value='$".$valact."' min=''  onKeyPress='return NumCheck(event, this)' onchange='guardarvaloract(".$idc.",".$pre.",".$gru.",".$cap.",this.value,".$act.",".$insu.")' class='currency'><br><span id='spanval".$idc."'></span>";
				   				   
				   $datos[] =  array("iddetalle"=>$idc,"insumo"=>$nom,"tipo"=>$tipo,"unidad"=>$unidad,"rendi"=>$ren,"valor"=>$valori,"valor1"=>number_format($valori,2,'.',','),"material"=>number_format($material,2,'.',','),"equipo"=>number_format($equipo,2,'.',','),"mano"=>number_format($mano,2,'.',','),"materiala"=>"<div id='material".$idc."' class='currency' title='".$mate."'>$".number_format($mate,2,'.',',')."</div>","equipoa"=>"<div id='equipo".$idc."' class='currency' title='".$equi."'>$".number_format($equi,2,'.',',')."</div>","manoa"=>"<div id='mano".$idc."' class='currency' title='".$man."'>$".number_format($man,2,'.',',')."</div>","total"=>$total,"renact"=>$renact,"valact"=>$valact,"totalact"=>"<div id='cambio".$idc."' class='currency' title='".$totalact."'>$".number_format($totalact,2,'.',',')."</div>","res"=>$res,"insu"=>$insu,"REA"=>$btnrendact,"VAA"=>$btnvalact,"cre"=>$cre,"incp"=>number_format($incp,2,'.',',')."%");
				   				
			   }
			   
			   $consulta = mysql_query("SELECT i.act_clave_int idc,i.act_nombre nom,	u.uni_codigo cod,s.pgi_rend_sub_ini ri,s.pgi_rend_sub_act ra FROM	actividades AS i JOIN pre_gru_cap_act_sub_insumo s ON s.act_subanalisis = i.act_clave_int JOIN tipoproyecto AS t ON t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS u ON u.uni_clave_int = i.uni_clave_int WHERE s.pre_clave_int = '".$pre."' AND s.gru_clave_int = '".$gru."' AND s.cap_clave_int = '".$cap."' AND s.act_clave_int = '".$act."' GROUP BY s.act_subanalisis ORDER BY i.act_nombre ASC");
			   $nums = mysql_num_rows($consulta);
			   if($nums>0)
			   {
				   while($dats = mysql_fetch_array($consulta))
				   {
					   $idc = $dats['idc'];
					   $insu = "S-".$idc;
					   $nom = $dats['nom'];
					   $tipo = "";
					   $unidad = $dats['cod'];
					   $ren = $dats['ri'];
					   $renact = $dats['ra'];
					   $tpl = 1;
					   $material=0; $equipo = 0; $mano = 0;
				       $mate = 0; $equi = 0; $man = 0;
					   if($apli==0)
					   {							
							$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
							",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");
							
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totals + ($totads+$totims+$totuts+$totivs);
							
							$consu = mysql_query("select sum(pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
							",sum(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
							",sum(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
							",sum(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
							",sum((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$idc."' "); //and pa.act_subanalisis = '".$idc."'
							
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totalcini + $totals + ($totads+$totims+$totuts+$totivs);
							
							$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
							",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");		
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalc = $totals + ($totads+$totims+$totuts+$totivs);
							
							$consu = mysql_query("select sum(pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act) tot".			
							",sum(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
							",sum(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
							",sum(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
							",sum((((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$idc."'");	// and pa.act_subanalisis = '".$idc."'	
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalc = $totalc + $totals + ($totads+$totims+$totuts+$totivs);		
					   }
					   else
					   {						  
							$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
							",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
							",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
							"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
							"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");							
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totals + ($totads+$totims+$totuts+$totivs);
							
							$consu = mysql_query("select sum(pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
							",sum(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
							",sum(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
							",sum(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
							",sum(((((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
							"(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
							"(((pa.pgi_rend_sub_ini*pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$idc."'");// and pa.act_subanalisis = '".$idc."'							
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalcini = $totalcini +  $totals + ($totads+$totims+$totuts+$totivs);
							
							$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
							",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
							",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
							"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
							"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");		
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalc = $totals + ($totads+$totims+$totuts+$totivs);	
							
							$consu = mysql_query("select sum(pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act) tot".			
							",sum(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
							",sum(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
							",sum(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
							",sum(((((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
							"(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
							"(((pa.pgi_rend_sub_act*pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
							" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$idc."' ");//	and pa.act_subanalisis = '".$idc."'	
							$datsu = mysql_fetch_array($consu);
							$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
							if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
							$totalc = $totals + ($totads+$totims+$totuts+$totivs);	
					   }
					   
						$valor = $totalcini;
						$valact = $totalc;
						$totalc = $renact*$valact;
						$totalcini = $ren*$valor;
						
						$totalact = ($totalcini)-($totalc);
						if($totalact == ''){ $totalact = 0; }				   
						
						if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
						$total = $material + $equipo + $mano;
						$btnrendact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remren".$pre."g".$gru."c".$cap."a".$act."s".$idc."' name='' type='text' value='".$renact."'  min='0'  onKeyPress='return NumCheck(event, this)' onchange=guardarcantidadrenactsu(".$idc.",".$pre.",".$gru.",".$cap.",this.value,".$act.",'')>";
						$btnvalact = "<span class='currency'>".$valact."</span>";
						$datos[] =  array("iddetalle"=>$idc,"insumo"=>$nom,"tipo"=>$tipo,"unidad"=>$unidad,"rendi"=>$ren,"valor"=>$valor,"valor1"=>number_format($valor,2,'.',','),"material"=>number_format($material,2,'.',','),"equipo"=>number_format($equipo,2,'.',','),"mano"=>number_format($mano,2,'.',','),"materiala"=>"<div id='material".$pre."g".$gru."c".$cap."a".$act."s".$idc."' class='currency' title='".$mate."'>$".number_format($mate,2,'.',',')."</div>","equipoa"=>"<div id='equipo".$pre."g".$gru."c".$cap."a".$act."s".$idc."' class='currency' title='".$equi."'>$".number_format($equi,2,'.',',')."</div>","manoa"=>"<div id='mano".$pre."g".$gru."c".$cap."a".$act."s".$idc."' class='currency' title='".$man."'>$".number_format($man,2,'.',',')."</div>","total"=>$total,"renact"=>$renact,"valact"=>$valact,"totalact"=>"<div id='cambio".$pre."g".$gru."c".$cap."a".$act."s".$idc."' class='currency' title='".$totalact."'>$".$totalact."</div>","res"=>$res,"insu"=>$insu,"REA"=>$btnrendact,"VAA"=>$btnvalact,"cre"=>0);
				   				
						
					   
		    }
		}			   
	}
	else
	{
	  $res = "no";
	    $datos[] =  array("iddetalle"=>"","insumo"=>"","tipo"=>"","unidad"=>"","rendi"=>"","valor"=>"","valor1"=>"","material"=>"","equipo"=>"","mano"=>"","total"=>"","renact"=>"","valact"=>"","totalact"=>"","res"=>$res,"insu"=>"","REA"=>"","VAA"=>"","cre"=>"");
	}
			   
	echo json_encode($datos);
  
   
  }
  else if($opcion=="LISTAINSUMOSSUBANALISIS")
  { 
     $id=$_POST['id'];
     $pre=$_POST['pre'];
	 $act = $_POST['act'];
	 $con = mysql_query("select act_clave_int from pre_gru_cap_actividad where pgca_clave_int = '".$act."' limit 1");
	$dat  =mysql_fetch_array($con);
	$act = $dat['act_clave_int'];
     $cap=$_POST['cap'];
	 $gru = $_POST['gru']; 
	 $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
	 $data = mysql_fetch_array($cona);
	 $apli = $data['apli'];
	 
	$con = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$id."' order by tip,tpl,nom,nomu");
	$num = mysql_num_rows($con);
	if($num>0)
	{
		$res = "si";
	  while($dat = mysql_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val']; if($val=="null" || $val==NULL){$val=0;}
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   $tpl = $dat['tpl'];
				   $insu = $dat['id'];
				   
				   $con1 = mysql_query("select  pgi_clave_int det,pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_gru_cap_act_sub_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and act_subanalisis = '".$id."' and ins_clave_int = '".$insu."'");
				   $dato1 = mysql_fetch_array($con1);
				   $det = $dato1['det'];
				   $ren = $dato1['pgi_rend_ini']; 
				   $valor = $dato1['pgi_vr_ini']; 
				   $admini = $dato1['pgi_adm_ini'];
				   $impini = $dato1['pgi_imp_ini'];
				   $utiini = $dato1['pgi_uti_ini'];			
				   $ivaini = $dato1['pgi_iva_ini'];
				   
					$totalcini = $ren*$valor;
					$tadmini = ($totalcini*$admini)/100;
					$timpini = ($totalcini*$impini)/100;
					$tutiini = ($totalcini*$utiini)/100;
					if($apli==0) {$tivaini = ($tutiini*$ivaini)/100;}
					else {$tivaini = (($tadmini+$timpini+$tutiini)*$ivaini)/100;}
					$totalcini = $totalcini +($tadmini+$timpini+$tutiini+$tivaini);
				   			 
				   $renact = $dato1['pgi_rend_act'];
				   $valact = $dato1['pgi_vr_act'];
				   $admact = $dato1['pgi_adm_act'];
				   $impact = $dato1['pgi_imp_act'];
				   $utiact = $dato1['pgi_uti_act'];			
				   $ivaact = $dato1['pgi_iva_act'];				  
				  
				   $unidad = $cod;
				   $material=0; $equipo = 0; $mano = 0;
				   $mate = 0; $equi = 0; $man = 0;				  
				   
					$totalc = $renact*$valact;
					$tadmact = ($totalc*$admact)/100;
					$timpact = ($totalc*$impact)/100;
					$tutiact = ($totalc*$utiact)/100;
					if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
					{ $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}
					
					$totalc = $totalc +($tadmact+$timpact+$tutiact+$tivaact);
					
				   $totalact = ($totalcini)-($totalc);
				   if($totalact == ''){ $totalact = 0; }				   
				   
				   if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
				   $total = $material + $equipo + $mano;
				   
				   //BOTONES
				   
				   $btnrendact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remrens".$det."' name='' type='text' value='".$renact."'  min='0'  onKeyPress='return NumCheck(event, this)' onchange=guardarcantidadrenacts(".$det.",".$pre.",".$gru.",".$cap.",this.value,".$act.",".$insu.")><br><span id='spanrens".$det."'></span>";
				   $btnvalact = "<input style='width:100%; border:thin; background-color:transparent; text-align: right; cursor:pointer' id='remvals".$det."' name=''  type='text' value='$".$valact."' min=''  onKeyPress='return NumCheck(event, this)' onchange=guardarvaloracts('".$det."','".$pre."','".$gru."','".$cap."',this.value,'".$act."','".$insu."') class='currency'><br><span id='spanvals".$det."'></span>";
				   				   
				   $datos[] =  array("iddetalle"=>$det,"insumo"=>$nom,"tipo"=>$tipo,"unidad"=>$unidad,"rendi"=>$ren,"valor"=>$valor,"valor1"=>number_format($valor,2,'.',','),"material"=>number_format($material,2,'.',','),"equipo"=>number_format($equipo,2,'.',','),"mano"=>number_format($mano,2,'.',','),"materiala"=>number_format($mate,2,'.',','),"equipoa"=>number_format($equi,2,'.',','),"manoa"=>number_format($man,2,'.',','),"total"=>$total,"renact"=>$renact,"valact"=>$valact,"totalact"=>$totalact,"res"=>$res,"insu"=>$insu,"REA"=>$btnrendact,"VAA"=>$btnvalact);					
			   }
			   
	}
	else
	{
	  $res = "no";
	    $datos[] =  array("iddetalle"=>"","insumo"=>"","tipo"=>"","unidad"=>"","rendi"=>"","valor"=>"","valor1"=>"","material"=>"","equipo"=>"","mano"=>"","total"=>"","renact"=>"","valact"=>"","totalact"=>"","res"=>$res,"insu"=>"","REA"=>"","VAA"=>"");
	}
			   
	echo json_encode($datos);
  
   
  
  }
  else if($opcion=="GUARDARDETALLEP")
  {
	$id = $_POST['id'];
	$cantidad = $_POST['cantidad'];
	$conv = mysql_query("select pgca_cant_proyectada,pgca_valor_act,pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int from pre_gru_cap_actividad where pgca_clave_int = '".$id."'");
	$datv = mysql_fetch_array($conv);
	
	$pre = $datv['pre_clave_int']; $gru = $datv['gru_clave_int']; $cap = $datv['cap_clave_int']; $act = $datv['act_clave_int'];
	 
	 $upd = mysql_query("update pre_gru_cap_actividad set pgca_cant_proyectada ='".$cantidad."',pgca_usu_actualiz='".$usuario."',pgca_fec_actualiz='".$fecha."' where pgca_clave_int ='".$id."'");
	 if($upd>0)
	 {
		  $updi = mysql_query("update pre_gru_cap_act_insumo set pgi_cant_act = '".$cantidad."' where pre_clave_int='".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."' and act_clave_int ='".$act."'");
		  $updi2 = mysql_query("update pre_gru_cap_act_sub_insumo set pgi_cant_act = '".$cantidad."' where pre_clave_int='".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."' and act_clave_int ='".$act."'");
		  
		  
        $res =  1;
	 }
	 else
	 {
	    $res =  2;
	 }
	 $datos[] = array("res"=>$res,"act"=>$act);
	 echo json_encode($datos);
  }
    else if($opcion=="GUARDARDETALLEE")
  {
     $id = $_POST['id'];
	 $cantidad = $_POST['cantidad'];
	 $conv = mysql_query("select pgca_cant_proyectada,pgca_valor_act,pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int from pre_gru_cap_actividad where pgca_clave_int = '".$id."'");
	$datv = mysql_fetch_array($conv);
	$pre = $datv['pre_clave_int']; $gru = $datv['gru_clave_int']; $cap = $datv['cap_clave_int']; $act = $datv['act_clave_int'];
	
	 $upd = mysql_query("update pre_gru_cap_actividad set pgca_cant_ejecutada ='".$cantidad."',pgca_usu_actualiz='".$usuario."',pgca_fec_actualiz='".$fecha."' where pgca_clave_int ='".$id."'");
	 if($upd>0)
	 {
        $res = 1;
	 }
	 else
	 {
	    $res =  2;
	 }
	 $datos[] = array("res"=>$res,"act"=>$act);
	 echo json_encode($datos);
  }
    else if($opcion=="GUARDARDETALLEV")
  {
     $id = $_POST['id'];
	 $valor = $_POST['valor'];
	 $upd = mysql_query("update pre_gru_cap_actividad set pgca_valor_act ='".$valor."',pgca_usu_actualiz='".$usuario."',pgca_fec_actualiz='".$fecha."' where pgca_clave_int ='".$id."'");
	 if($upd>0)
	 {
        echo 1;
	 }
	 else
	 {
	    echo 2;
	 }
  }
  else if($opcion=="CALCULOTOTALES")
  {
     $act = $_POST['act'];//ID DEL DETALLE
	 $cap = $_POST['cap'];
	 $pre = $_POST['pre'];
	 $idda = $_POST['idda'];
	 $gru = $_POST['gru'];
	 $acti = $_POST['acti'];
	 $idsu = $_POST['idsu'];
	 $con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
	$dat = mysql_fetch_array($con);   
	$adm = $dat['pre_administracion'];
	$iva = $dat['pre_iva'];
	$imp = $dat['pre_imprevisto'];
	$uti = $dat['pre_utilidades'];
	$apli = $dat['apli'];
	
	$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$pre."' limit 1");
	$dat = mysql_fetch_array($con);   
	$adma = $dat['pri_administracion'];
	$ivaa = $dat['pri_iva'];
	$impa = $dat['pri_imprevisto'];
	$utia = $dat['pri_utilidades'];
		
	if($acti>0)
	 {
	   
		if($apli==0)
		{ 
			$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			 
			$conapu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($conapu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			
			//APU ACTUALES		
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
			//APU ACTUAL MATERIALEES
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 1");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int)   where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'  and t.tpi_tipologia = 1");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apuam = 0;}else {$apuam  = $datsu['tot'];}
			$apuam = $apuam + ($totada+$totima+$totuta+$totiva) + ($totals);
			
			//APU ACTUAL EQUIPO
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 2");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int)   where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'  and t.tpi_tipologia = 2");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apuae = 0;}else {$apuae  = $datsu['tot'];}
			$apuae = $apuae + ($totada+$totima+$totuta+$totiva) + ($totals);
			
			//APU ACTUAL MANO DE OBRA
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 3");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int)   where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'  and t.tpi_tipologia = 3");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apuamo = 0;}else {$apuamo  = $datsu['tot'];}
			$apuamo = $apuamo + ($totada+$totima+$totuta+$totiva) + ($totals);
			
			//total actividad
			
			$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini) as tot".			
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
				" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
	
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);		
			 
			$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini) as tot".			
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
				" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
			$datsum = mysql_fetch_array($consu);
			$totad = $datsum['totad']; $totim = $datsum['totim']; $totut = $datsum['totut']; $totiv = $datsum['totiv'];
			if($datsum['tot']=="" || $datsum['tot']==NULL){$totalact=0;}else{$totalact=$datsum['tot'];}
			$totalact = $totalact + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act) as tot".			
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
				",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
				" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);	
					
			$consu = mysql_query("select sum(pa.pgi_rend_act * pa.pgi_vr_act * pa.pgi_cant_act) tot,
				sum(((pa.pgi_rend_act * pa.pgi_vr_act * pa.pgi_cant_act) * pa.pgi_adm_act) / 100) totad,
				sum(((pa.pgi_rend_act * pa.pgi_vr_act * pa.pgi_cant_act) * pa.pgi_imp_act) / 100) totim,
				sum(((pa.pgi_rend_act * pa.pgi_vr_act * pa.pgi_cant_act) * pa.pgi_uti_act) / 100) totut,
				sum((((pa.pgi_rend_act * pa.pgi_vr_act * pa.pgi_cant_act) * pa.pgi_uti_act) / 100) * pa.pgi_iva_act) / 100 as totiv
				from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
			$datsum = mysql_fetch_array($consu);
			$totada = $datsum['totad']; $totima = $datsum['totim']; $totuta = $datsum['totut']; $totiva = $datsum['totiv'];
			if($datsum['tot']=="" || $datsum['tot']==NULL){$totalacta=$totalact;}else{$totalacta=$datsum['tot'];}
			$totalacta = $totalacta + ($totada+$totima+$totuta+$totiva) + ($totals); //PRESUPUESTO ACTUAL A CTIVIDAD
			
		}
		else
		{
				 
			$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100)+".
			"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100)+".
			"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			 
			$conapu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
			"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
			"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($conapu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);		
			
			
			//APU ACTUALES
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
			
			
			//APU ACTUALES MATERIALES
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 1");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 1");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apuam = 0;}else {$apuam  = $datsu['tot'];}
			$apuam = $apuam + ($totada+$totima+$totuta+$totiva) + ($totals);
			
			//APU ACTUALES EQUIPOS
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 2");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 2");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apuae = 0;}else {$apuae  = $datsu['tot'];}
			$apuae = $apuae + ($totada+$totima+$totuta+$totiva) + ($totals);
			
			//APU ACTUALES EQUIPOS
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 3");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int join insumos i on(pa.ins_clave_int = i.ins_clave_int) join tipoinsumos t  on(t.tpi_clave_int = i.tpi_clave_int) where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and t.tpi_tipologia = 3");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apuamo = 0;}else {$apuamo  = $datsu['tot'];}
			$apuamo = $apuamo + ($totada+$totima+$totuta+$totiva) + ($totals);
			
			//TOTAL ACTIVIDAD
			
			$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini) as tot".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
				",(sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
				"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
				"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
				" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
	
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);		
			 
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini) as tot".			
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100) totad".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100) totim".
				",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100) totut".
				",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_adm_ini)/100)+".
				"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_imp_ini)/100)+".
				"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pa.pgi_cant_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
				" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
			$datsum = mysql_fetch_array($consu);
			$totad = $datsum['totad']; $totim = $datsum['totim']; $totut = $datsum['totut']; $totiv = $datsum['totiv'];
			if($datsum['tot']=="" || $datsum['tot']==NULL){$totalact=0;}else{$totalact=$datsum['tot'];}
			$totalact = $totalact + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act) as tot".			
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
				",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
				"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
				"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
				" from pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);	
					
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pa.pgi_cant_act) as tot".			
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pa.pgi_cant_act)*pa.pgi_adm_act)/100) totad".
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pa.pgi_cant_act)*pa.pgi_imp_act)/100) totim".
				",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pa.pgi_cant_act)*pa.pgi_uti_act)/100) totut".
				",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pa.pgi_cant_act)*pa.pgi_adm_act)/100)+".
				"(((pa.pgi_rend_act*pa.pgi_vr_act*pa.pgi_cant_act)*pa.pgi_imp_act)/100)+".
				"(((pa.pgi_rend_act*pa.pgi_vr_act*pa.pgi_cant_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
				" from pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int  where  pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int = '".$gru."' and pa.cap_clave_int = '".$cap."' and pa.act_clave_int = '".$acti."'");
			$datsum = mysql_fetch_array($consu);
			$totada = $datsum['totad']; $totima = $datsum['totim']; $totuta = $datsum['totut']; $totiva = $datsum['totiv'];
			if($datsum['tot']=="" || $datsum['tot']==NULL){$totalacta=$totalact;}else{$totalacta=$datsum['tot'];}
			$totalacta = $totalacta + ($totada+$totima+$totuta+$totiva) + ($totals); //PRESUPUESTO ACTUAL A CTIVIDAD
			
			
			}
				

		$consui = mysql_query("select (pa.pgca_cantidad-pa.pgca_cant_ejecutada) as fal from pre_gru_cap_actividad pa join actividades a on a.act_clave_int  = pa.act_clave_int  where pa.pgca_clave_int  = '".$act."'");
		$datsumi = mysql_fetch_array($consui);
		if($datsumi['fal']=="" || $datsumi['fal']==NULL){$faltantei=0;}else{$faltantei=$datsumi['fal'];}
		
		$consu = mysql_query("select (pa.pgca_cant_proyectada-pa.pgca_cant_ejecutada) as fal from pre_gru_cap_actividad pa join actividades a on a.act_clave_int  = pa.act_clave_int  where pa.pgca_clave_int  = '".$act."'");
		
		$datsum = mysql_fetch_array($consu);
		if($datsum['fal']=="" || $datsum['fal']==NULL){$faltante=$faltantei;}else{$faltante=$datsum['fal'];}
		
		$totalalca = $totalact - $totalacta;//CAMBIO ALCANCE DE ACTIVIDAD
		//if($totalalca<0){$totalalca="<span class='text-danger'>($".number_format($totalalca*(-1),2,'.',',').")</span>";}else{$totalalca="$".number_format($totalalca,2,'.',',');}
		
		//CONSULTA DE ACTUALIZAR DE TOTAL ACTIVIDAD ACTUAL EN PRE_GRU_CAP_ACTIVIDAD
		$UPDATEACTIVIDAD = mysql_query("UPDATE pre_gru_cap_actividad SET pgca_valor_acta = '".$totalacta."' WHERE pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$acti."'");
	 }
	 else
	 {
	   $apua = 0;
	   $apu = 0;
	   $totalact = 0;
	   $totalacta = 0;
	   $totalalca = 0;
	   $faltante  = 0;
	 }


	 if($idda>0)
	 {
		 /*if($apli==0)
		 {
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);	

			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act))*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totad+$totim+$totut+$totiv) + ($totals);
		 }
		 else
		 {
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);	

			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totad+$totim+$totut+$totiv) + ($totals);
		 }*/
		 
		 $cona = mysql_query("select pgca_valor_act,pgca_cantidad,pgca_cant_proyectada from pre_gru_cap_actividad where pgca_clave_int = '".$idda."' limit 1");
		 $data = mysql_fetch_array($cona);
		 $canti = $data['pgca_cantidad'];
		 $cantip  = $data['pgca_cant_proyectada'];
		 if($cantip<=0){$cantip=$canti;}
		 $tota = $apu*$cantip;
		 $totpreact = number_format($tota,2,'.','');
		 $totalidda = number_format($apu,2,'.','');
	 }	 
	 else
	 {
		$totpreact = 0;
	    $totalidda = 0;	//total unitario actividad actual
	 }
		
	 
	 if($gru>0)
     {
		 
			$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."'");		
			$datsum = mysql_fetch_array($consu);
			if($datsum['totc']=="" || $datsum['totc']==NULL){$totalg=0;}else{$totalg=$datsum['totc'];}
			
			$consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."'");		
			$datsum = mysql_fetch_array($consu);
			if($datsum['totc']=="" || $datsum['totc']==NULL){$totalag=0;}else{$totalag=$datsum['totc'];}
		   
			$cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."'");
			$date = mysql_fetch_array($cone); 
			if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else {  $tote = $date['tote'];}	
			if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else {  $toti = $date['toti'];}	
			$totalag = $totalag + $tote;
			$totalg = $totalg + $toti;	
			
			$totalalcg =  $totalg - $totalag; //CAMBIO ALCANCE GRUO
	 }
	 else 
	 {
	   $totalag = 0;
	   $totalalcg = 0;	
	}
	 
  
	 if($cap>0)
	 {
		 $conidca = mysql_query("select pgc_clave_int from pre_gru_capitulo where pre_clave_int = '".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."' limit 1");
		$datidca =  mysql_fetch_array($conidca);
		$idca = $datidca['pgc_clave_int'];
		
		$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."'");		
		$datsum = mysql_fetch_array($consu);
		if($datsum['totc']=="" || $datsum['totc']==NULL){$totalcap=0;}else{$totalcap=$datsum['totc'];}
		
		$consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."'");		
		$datsum = mysql_fetch_array($consu);
		if($datsum['totc']=="" || $datsum['totc']==NULL){$totalcapa=0;}else{$totalcapa=$datsum['totc'];}
		//total de presupuesto enlazado
		 $cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."'");
		 $date = mysql_fetch_array($cone); 
		 if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else {  $tote = $date['tote'];}	
		 if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else {  $tote = $date['toti'];}	

		 $totalcapa = $totalcapa + $tote;
		 $totalcap = $totalcap + $toti;
		
		$totalalcc = $totalcap - $totalcapa;//CAMBIO ALCANCE CAPITULO
		//if($totalalcc<0){$totalalcc="<span class='text-danger'>($".number_format($totalalcc*(-1),2,'.',',').")</span>";}else{$totalalcc="$".number_format($totalalcc,2,'.',',');}
		
	 }
	 else
	 {
		 $idca = 0;
		 $totalcap = 0;
		 $totalcapa = 0;
		 $totalalcc = 0;
     }
	 
	 if($idsu>0)
	 {
		 if($apli==0)
		{
			$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and pa.act_subanalisis = '".$idsu."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsui = $totals + ($totads+$totims+$totuts+$totivs);	
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and pa.act_subanalisis = '".$idsu."'");		
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsua = $totals + ($totads+$totims+$totuts+$totivs);
		}
		else
		{
			$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
			",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
			"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
			"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and pa.act_subanalisis = '".$idsu."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsui = $totals + ($totads+$totims+$totuts+$totivs);
			
			$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
			",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
			"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$acti."' and pa.act_subanalisis = '".$idsu."'");		
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totalsua = $totals + ($totads+$totims+$totuts+$totivs);	
		}
		$alcancesub = $totalsui - $totalsua;
		
	 }
	 else
	 {
		 $totalsui = 0;
		 $totalsua = 0;
		 $alcancesub = 0;
		 
	 }
	 
	 $cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$pre."'");
	$date = mysql_fetch_array($cone); 
	if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else {  $tote = $date['tote'];}	
	if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else {  $toti = $date['toti'];}
	
	$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."'");		
	$datsum = mysql_fetch_array($consu);
	if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}
	
	 $total = $total + $toti;
	
	$totadmi = ($total * $adm)/100;
	$totimpi = ($total * $imp)/100;
	$totutii = ($total * $uti)/100;
	if($apli==0){$totivai = ($totutii * $iva)/100;}else{$totivai = (($totadmi + $totimpi +  $totutii) * $iva)/100;}
	$totalpre = $total + $totadmi + $totimpi + $totutii + $totivai;
	
	$consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."'");		
	$datsum = mysql_fetch_array($consu);
	if($datsum['totc']=="" || $datsum['totc']==NULL){$totala=0;}else{$totala=$datsum['totc'];}
	$totala = $totala +  $tote;	
	$totadma = ($totala * $adma)/100;
	$totimpa = ($totala * $impa)/100;
	$totutia = ($totala * $utia)/100;
	if($apli==0){ $totivaa = ($totutia * $ivaa)/100; }else{ $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; } 
	$totalprea = $totala + $totadma + $totimpa + $totutia + $totivaa ;
	
	$alccos = $total - $totala;
	$alcpre = $totalpre - $totalprea;
	$alcadm = $totadmi - $totadma;
	$alcimp = $totimpi - $totimpa;
	$alcuti = $totutii - $totutia;
	$alciva = $totivai - $totivaa;

      $conpro = mysql_query("select sum(pai_val_comprometido) totc from partida_item where pre_clave_int = '".$pre."'");
      $dato = mysql_fetch_array($conpro);
      $totalco = $dato['totc'];

      if($totala>0)
      {
          $totaldispo = $total  - $totala;
      }
      else
      {
          $totaldispo = $total - $totalco;
      }
	
	
	$datos[] = array("totprea"=>"$".number_format($totalprea,2,'.',','),"totalcap"=>"$".number_format($totalcap,2,'.',','),"totalact"=>"$".number_format($totalact,2,'.',','),"totalcapa"=>"$".number_format($totalcapa,2,'.',','),"totalacta"=>"$".number_format($totalacta,2,'.',','),"totalalca"=>$totalalca,"totalalcc"=>$totalalcc,"totadma"=>"$".number_format($totadma,2,'.',','),"totimpa"=>"$".number_format($totimpa,2,'.',','),"totutia"=>"$".number_format($totutia,2,'.',','),"totivaa"=>"$".number_format($totivaa,2,'.',','),"totalnetoa"=>"$".number_format($totala,2,'.',','),"alccos"=>"$".number_format($alccos,2,'.',','),"alcpre"=>"$".number_format($alcpre,2,'.',','),"alcadm"=>"$".number_format($alcadm,2,'.',','),"alcimp"=>"$".number_format($alcimp,2,'.',','),"alcuti"=>"$".number_format($alcuti,2,'.',','),"alciva"=>"$".number_format($alciva,2,'.',','),"faltante"=>$faltante,"totalidda"=>$totalidda,"totpreact"=>$totpreact,"totgrui"=>"$".number_format($totalg,2,'.',','),"totgruact"=>"$".number_format($totalag,2,'.',','),"totgrualc"=>"$".number_format($totalalcg,2,'.',','),"idca"=>$idca,"totpre"=>"$".number_format($totalpre,2,'.',','),"totalapu"=>"$".number_format($apu,2,'.',','),"totalapua"=>"$".number_format($apua,2,'.',','),"totalapuam"=>"$".number_format($apuam,2,'.',','),"totalapuae"=>"$".number_format($apuae,2,'.',','),"totalapuamo"=>"$".number_format($apuamo,2,'.',','),"totalsui"=>"$".number_format($totalsui,2,'.',','),"totalsua"=>"$".number_format($totalsua,2,'.',','),"alcancesub"=>"$".number_format($alcancesub,2,'.',','),"totadm"=>"$".number_format($totadmi,2,'.',','),"totimp"=>"$".number_format($totimpi,2,'.',','),"totuti"=>"$".number_format($totutii,2,'.',','),"totiva"=>"$".number_format($totivai,2,'.',','),"totalneto"=>"$".number_format($total,2,'.',','),"toti"=>$toti,"tote"=>$tote,"ivaa"=>$ivaa,"totaldispo"=>$totaldispo);
	
	echo json_encode($datos);
	
  }
  else if($opcion=="ACTUALIZARCOSTOS")
  {
     $idpresupuesto = $_POST['idpresupuesto'];
	 $adma = $_POST['adm'];
	 $utia = $_POST['uti'];
	 $impa = $_POST['imp'];
	 $ivaa = $_POST['iva'];
	 $conv = mysql_query("select * from presupuestoinicial where pre_clave_int = '".$idpresupuesto."'");
	 $numv = mysql_num_rows($conv);
	 if($numv>0)
	 {
	 $upd = mysql_query("update presupuestoinicial set pri_administracion = '".$adma."',pri_imprevisto='".$impa."',pri_utilidades='".$utia."',pri_iva='".$ivaa."' where pre_clave_int = '".$idpresupuesto."'");
	 }
	 else
	 {
	  $upd = mysql_query("insert into presupuestoinicial(pre_clave_int,pri_administracion,pri_imprevisto,pri_utilidades,pri_iva,pri_usu_actualiz,pri_fec_actualiz) values('".$idpresupuesto."','".$adma."','".$impa."','".$utia."','".$ivaa."','".$usuario."','".$fecha."')");
	 }
	 if($upd>0)
	 {
		 //DATOS REALES PRESUPUESTO
			$con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva apli from presupuesto where pre_clave_int = '".$idpresupuesto."' limit 1");
			$dat = mysql_fetch_array($con);   
			$adm = $dat['pre_administracion'];
			$iva = $dat['pre_iva'];
			$imp = $dat['pre_imprevisto'];
			$uti = $dat['pre_utilidades'];
			$apli = $dat['apli'];
			
			$cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$idpresupuesto."'");
			$date = mysql_fetch_array($cone); 					
			if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else {  $tote = $date['tote'];}	
			if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else {  $toti = $date['toti'];}
			$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");		
			$datsum = mysql_fetch_array($consu);
			if($datsum['totc']=="" || $datsum['totc']==NULL){$total=0;}else{$total=$datsum['totc'];}
			$total = $total + $toti;
			
			
			$totadm = ($total * $adm)/100;
			$totimp = ($total * $imp)/100;
			$totuti = ($total * $uti)/100;
			if($apli==0) { $totivai = ($totuti * $iva)/100; }else { $totivai = (($totadm + $totimp + $totuti) * $iva)/100; }
		    $totalpre = $total + $totadm + $totimp + $totuti + $totivai;
		    
			$consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$idpresupuesto."'");		
			$datsum = mysql_fetch_array($consu);
			if($datsum['totc']=="" || $datsum['totc']==NULL){$totala=0;}else{$totala=$datsum['totc'];}
			$totala = $totala + $tote;
			//total presupuesto enla zado
		
			
			$totadma = ($totala * $adma)/100;
			$totimpa = ($totala * $impa)/100;
			$totutia = ($totala * $utia)/100;
			if($apli==0) { $totivaa = ($totutia * $ivaa)/100; } else { $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
			$totprea = $totala + $totadma + $totimpa + $totutia + $totivaa;
			
			$alccos = $total - $totala;
			$alcpre = $totalpre - $totprea;
			$alcadm = $totadm - $totadma;
			$alcimp = $totimp - $totimpa;
			$alcuti = $totuti - $totutia;
			$alciva = $totivai - $totivaa;
		
	    $datos[] = array("totpre"=>"$".number_format($totprea,2,'.',','),"totadm"=>"$".number_format($totadma,2,'.',','),"totimp"=>"$".number_format($totimpa,2,'.',','),"totuti"=>"$".number_format($totutia,2,'.',','),"totiva"=>"$".number_format($totivaa,2,'.',','),"totalneto"=>"$".number_format($totala,2,'.',','),"alccos"=>"$".number_format($alccos,2,'.',','),"alcpre"=>"$".number_format($alcpre,2,'.',','),"alcadm"=>"$".number_format($alcadm,2,'.',','),"alcimp"=>"$".number_format($alcimp,2,'.',','),"alcuti"=>"$".number_format($alcuti,2,'.',','),"alciva"=>"$".number_format($alciva,2,'.',','));
		echo json_encode($datos);
	 }
  }
  else if($opcion=="USARACTIVIDAD")
  {
	  $actividad = $_POST['actividad'];
	  $iddetalle = $_POST['iddetalle'];
	  $presupuesto = $_POST['presupuesto'];
	  $grupo = $_POST['grupo'];
	  $capitulo = $_POST['capitulo'];
	   $coninfo = mysql_query("select act_nombre as des,tpp_clave_int as tpp,uni_clave_int uni,act_analisis as ana,a.est_clave_int as est,c.ciu_clave_int ciu,d.dep_clave_int dep,p.pai_clave_int pai from actividades a join ciudad c on c.ciu_clave_int = a.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where act_clave_int = '".$actividad."' limit 1");
	  $datinfo = mysql_fetch_array($coninfo);
	
	  $tpp = $datinfo['tpp'];
	  $uni = $datinfo['uni'];
	  $ana = $datinfo['ana'];
	  $des = $datinfo['des'];
	  $est = $datinfo['est'];
	  $ciu = $datinfo['ciu'];
	  $pai = $datinfo['pai'];
	  $dep = $datinfo['dep'];
     ?>
<form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $actividad;?>">
  <div class="form-group">
    <div class="col-md-4"><strong>Descripción:<span class="symbol required"></span></strong>
      <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" value="<?php echo $des;?>" placeholder="Escribe aqui la descripcion">
    </div>
    <div class="col-md-4"><strong>Tipo de Proyecto:<span class="symbol required"></span></strong>
      <select name="seltipoproyecto"id="seltipoproyecto" class="form-control input-sm">
        <option value="">--seleccione--</option>
        <?PHP
		 $con = mysql_query("select tpp_clave_int,tpp_nombre from tipoproyecto where est_clave_int = 1 or tpp_clave_int = '".$tpp."'");
		 while($dat = mysql_fetch_array($con))
		 {
		    $idp = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
        <option <?php if($tpp==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
        <?Php	
		 }
		 ?>
      </select>
    </div>
    <div class="col-md-4"><strong>Unidad de medida:<span class="symbol required"></span></strong>
      <select name="selunidad" id="selunidad" class="form-control input-sm">
        <option value="">--seleccione--</option>
        <?php 
		 $con = mysql_query("select uni_clave_int,uni_codigo,uni_nombre from unidades where est_clave_int = 1 or uni_clave_int='".$uni."'");
		 while($dat = mysql_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
        <option <?php if($uni==$idu){echo 'selected';}?> value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
        <?php
		 }
		 ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-4"><strong>Pais:<span class="symbol required"></span></strong>
      <select name="selpais" id="selpais" class="form-control input-sm" onChange="cargardepartamento('selpais','seldepartamento',1)">
        <option value="">--seleccione--</option>
        <?php
        $con = mysql_query("select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysql_fetch_array($con))
        {
        ?>
        <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
      </select>
    </div>
    <div class="col-md-4"><strong>Departamento:<span class="symbol required"></span></strong>
      <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')">
        <option value="">--seleccione--</option>
        <?php
            $con = mysql_query("select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and est_clave_int = 1");
            while($dat = mysql_fetch_array($con))
            {
            ?>
        <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
        <?php
            }
            ?>
      </select>
    </div>
    <div class="col-md-4"><strong>Ciudad:<span class="symbol required"></span></strong>
      <select name="selciudad" id="selciudad" class="form-control input-sm">
        <option value="">--seleccione--</option>
        <?php
	     $con = mysql_query("select ciu_clave_int,ciu_nombre from ciudad where (dep_clave_int ='".$dep."' and est_clave_int = 1) or ciu_clave_int ='".$ciu."'");
	 while($dat = mysql_fetch_array($con))
	 {
     ?>
        <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
        <?php
		}
	 ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-4"><strong>SubAnalisis:</strong>
      <label for="opcion3">
        <input type="radio" name="radanalisis" id="opcion3" class="flat-red"  value="Si" <?php if($ana=="Si" || $ana==""){echo 'checked';}?>>
        Si</label>
      <label for="opcion4">
        <input type="radio" name="radanalisis" id="opcion4" class="flat-red" value="No" <?php if($ana=="No"){echo 'checked';}?>>
        No</label>
    </div>
    <div class="col-md-4"><strong>Estado:</strong><br>
      <label for="opcion1">
        <input type="radio" name="radestado" id="opcion1" disabled class="flat-red" value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
        Activo</label>
      <label for="opcion2">
        <input type="radio" name="radestado" id="opcion2" disabled  class="flat-red" value="0" <?php if($est==0){echo 'checked';}?>>
        Inactivo </label>
      <label>
        <input type="radio" name="radestado" id="opcion5" class="flat-red" disabled value="3" <?php if($est==3){echo 'checked';}?>>
        Por Aprobar</label>
      <label>
        <input type="radio" name="radestado" id="opcion8" class="flat-red" disabled value="5" <?php if($est==5){echo 'checked';}?>>
        Importada</label>
      <?php
		
		
		
        ?>
    </div>
  </div>
  <div class="form-group"> 
    <script src="js/jsinsumousar.js"></script>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">RECURSOS</h4>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <table class="insumos table table-bordered table-condensed compact table-hover" style="background-color: rgba(0,153,255,0.5); font-size:11px">
              <thead>
                <tr>
                  <th class="dt-head-center">TIPO</th>
                  <th class="dt-head-center">NOMBRE</th>
                  <th class="dt-head-center">UN</th>
                  <th class="dt-head-center">REND</th>
                  <th class="dt-head-center">VR.UNIT</th>
                  <th class="dt-head-center">DESCRIPCIÓN</th>
                </tr>
              </thead>
              <?PHP
		$con = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where a.act_clave_int = '".$actividad."'   order by tip,nom,nomu");
			?>
              <tbody>
                <?php
			   while($dat = mysql_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   $ins = $dat['id'];
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				   
				   /*$conactual = mysql_query("select pgi_rendimiento,pgi_vr_unitario from pre_gru_cap_act_insumo where pre_clave_int = '".$presupuesto."' and gru_clave_int = '".$grupo."' and cap_clave_int = '".$capitulo."' and act_clave_int ='".$actividad."' and ins_clave_int ='".$ins."'");
				   $data = mysql_fetch_array($conactual);
				   $rena = $data['pgi_rendimiento'];
				   $vala = $data['pgi_vr_unitario'];
				   if($rena>0){$ren=$rena;}
				   if($vala>0){$valor=$vala;}*/
				    
				   ?>
                <tr id="row_ins<?php echo $idc;?>">
                  <td data-title="Tipo"><?Php echo $tipo;?></td>
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title="Unidad"><?php echo $cod;?></td>
                  <td data-title="Rendimiento"><?php echo $ren;?></td>
                  <td data-title="Valor Unitario">$ <?php echo number_format($valor,2,'.',',');?></td>
                  <td data-title="Descripcion"><?php echo $des;?></td>
                </tr>
                <?php
			   }
			   ?>
              </tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
            <a role="button" name="" class="btn btn-primary" id="btnguardar" rel="" onClick="CRUDPRESUPUESTOINICIAL('GUARDARACTIVIDADUSARINICIAL','<?PHP echo $actividad; ?>','<?php echo $iddetalle;?>','<?php echo $grupo;?>','<?php echo $capitulo;?>')"><i class="fa fa-save"></i>Usar Iniciales</a> </div>
          <div class="col-md-6">
            <table class="insumos table table-bordered table-condensed compact table-hover" style="background-color: rgba(255,0,0,0.5); font-size:11px">
              <thead>
                <tr>
                  <th class="dt-head-center">TIPO</th>
                  <th class="dt-head-center">NOMBRE</th>
                  <th class="dt-head-center">UN</th>
                  <th class="dt-head-center">REND</th>
                  <th class="dt-head-center">VR.UNIT</th>
                  <th class="dt-head-center">DESCRIPCIÓN</th>
                </tr>
              </thead>
              <?PHP
		$con = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where a.act_clave_int = '".$actividad."'   order by tip,nom,nomu");
			?>
              <tbody>
                <?php
			   while($dat = mysql_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   $ins = $dat['id'];
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				   
				   $conactual = mysql_query("select pgi_rend_act,pgi_vr_act from pre_gru_cap_act_insumo where pre_clave_int = '".$presupuesto."' and gru_clave_int = '".$grupo."' and cap_clave_int = '".$capitulo."' and act_clave_int ='".$actividad."' and ins_clave_int ='".$ins."'");
				   $data = mysql_fetch_array($conactual);
				   $rena = $data['pgi_rend_act'];
				   $vala = $data['pgi_vr_act'];
				   if($rena>0){$ren=$rena;}
				   if($vala>0){$valor=$vala;}
				    
				   ?>
                <tr id="row_ins<?php echo $idc;?>">
                  <td data-title="Tipo"><?Php echo $tipo;?></td>
                  <td data-title="Nombre"><?Php echo $nom;?></td>
                  <td data-title="Unidad"><?php echo $cod;?></td>
                  <td data-title="Rendimiento"><?php echo $ren;?></td>
                  <td data-title="Valor Unitario">$ <?php echo number_format($valor,2,'.',',');?></td>
                  <td data-title="Descripcion"><?php echo $des;?></td>
                </tr>
                <?php
			   }
			   ?>
              </tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
            <a role="button" name="" class="btn  btn-danger" id="btnguardar" rel="" onClick="CRUDPRESUPUESTOINICIAL('GUARDARACTIVIDADUSAR','<?PHP echo $actividad; ?>','<?php echo $iddetalle;?>','<?php echo $grupo;?>','<?php echo $capitulo;?>')"><i class="fa fa-save"></i>Usar Actuales</a> </div>
        </div>
      </div>
    </div>
  </div>
</form>
<span id="msn1"></span>
<?PHP
  }
  else if($opcion=="GUARDARACTIVIDADUSAR" || $opcion=="GUARDARACTIVIDADUSARINICIAL")
  {
      $actividad = $_POST['actividad'];
	  $iddetalle = $_POST['iddetalle'];
	  $presupuesto = $_POST['presupuesto'];
      $grupo = $_POST['grupo'];
	  $capitulo = $_POST['capitulo'];
	   $coninfo = mysql_query("select act_nombre as des,tpp_clave_int as tpp,c.ciu_clave_int ciu from actividades a join ciudad c on c.ciu_clave_int = a.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where act_clave_int = '".$actividad."' limit 1");
	  $datinfo = mysql_fetch_array($coninfo);	
	  $tpp = $datinfo['tpp'];
	  $des = strtoupper($datinfo['des']);
	  $ciu = $datinfo['ciu'];
	  
	$tipoproyecto = $_POST['tipoproyecto'];
	$unidad = $_POST['unidad'];
	$ciudad = $_POST['ciudad'];
	$descripcion = strtoupper($_POST['descripcion']); $desc = $_POST['descripcion'];
	$estado  = $_POST['estado'];
	$analisis  = $_POST['analisis'];
	if($des==$descripcion and $tpp==$tipoproyecto and $ciudad==$ciu)
	{
	   echo 'error1';
	}
	else
	{
		$insa = mysql_query("insert into actividades(act_nombre,tpp_clave_int,uni_clave_int,act_analisis,est_clave_int,ciu_clave_int,act_usu_actualiz,act_fec_actualiz,act_usu_creacion,act_fec_creacion,pre_clave_int) values('".$desc."','".$tipoproyecto."','".$unidad."','".$analisis."','1','".$ciudad."','".$usuario."','".$fecha."','".$idUsuario."','".$fecha."','".$presupuesto."')");
	   
		
		if($insa>0)
		{
			$idac = mysql_insert_id();
			$upd = mysql_query("update tipoproyecto set est_clave_int = 1 where tpp_clave_int = '".$tipoproyecto."'");
			$upd1 = mysql_query("update ciudad set est_clave_int = 1 where ciu_clave_int = '".$ciudad."'");
			$upd2 = mysql_query("update unidades set est_clave_int = 1 where uni_clave_int = '".$unidad."'");
			
			$con = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des from  actividades a join actividadinsumos d on a.act_clave_int = d.act_clave_int join insumos i on d.ins_clave_int = i.ins_clave_int join tipoinsumos t on t.tpi_clave_int = i.tpi_clave_int join unidades  u on u.uni_clave_int  = i.uni_clave_int where a.act_clave_int = '".$actividad."'   order by tip,nom,nomu");
			
			while($dat = mysql_fetch_array($con))
			   {
				   $idc = $dat['idd'];
				   $nom = $dat['nom'];
				   $tipo = $dat['tip'];
				   $uni = $dat['nomu'];
				   $val = $dat['val'];
				   $val1 = $dat['val1'];
				   $des = $dat['des'];
				   $cod = $dat['cod'];
				   $est = $dat['est'];
				   $ren = $dat['ren'];
				   $ins = $dat['id'];
				   if($val1<=0){$valor = $val;}else{$valor=$val1;}
				   if($opcion=="GUARDARACTIVIDADUSAR")
				   {
					   //VALORES ACTUALES
					   $conactual = mysql_query("select pgi_rend_act,pgi_vr_act from pre_gru_cap_act_insumo where pre_clave_int = '".$presupuesto."' and gru_clave_int = '".$grupo."' and cap_clave_int = '".$capitulo."' and act_clave_int= '".$actividad."' and ins_clave_int ='".$ins."'");
					   $data = mysql_fetch_array($conactual);
					   $rena = $data['pgi_rend_act'];
					   $vala = $data['pgi_vr_act'];
					   if($rena>0){$ren=$rena;}
					   if($vala>0){$valor=$vala;}
				   }
				   
		    		$insi = mysql_query("insert into actividadinsumos(act_clave_int,ins_clave_int,ati_rendimiento,ati_valor,ati_usu_actualiz,ati_fec_actualiz) values ('".$idac."','".$ins."','".$ren."','".$valor."','".$usuario."','".$fecha."')");
					$upd4 = mysql_query("update insumos set est_clave_int = 1 where ins_clave_int = '".$ins."'");
			   }
			   echo "ok";
		}
		else
		{
			echo "error2";
		}
	}
  }
  //CODIGO BRAYAN
  else if($opcion=="GUARDARDETALLERENACTS")
  {
		$id = $_POST['id'];
		$cantidad = $_POST['cantidad'];
		$act = $_POST['act'];
		$pre = $_POST['pre'];
		$gru = $_POST['gru'];
		$cap= $_POST['cap'];
	  $upd = mysql_query("update pre_gru_cap_act_sub_insumo set pgi_rend_act ='".$cantidad."',pgi_usu_actualiz='".$usuario."',pgi_fec_actualiz='".$fecha."' where pgi_clave_int = '".$id."'");
	  if($upd>0)
		 {
			 $res = 1;
			 $coni = mysql_query("select pgca_clave_int from pre_gru_cap_actividad where  pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."' limit 1");
			 $dati = mysql_fetch_array($coni);			  
			 $idda = $dati['pgca_clave_int'];
			  $cons= mysql_query("select act_subanalisis,act_clave_int from pre_gru_cap_act_sub_insumo where pgi_clave_int = '".$id."'");
			 $dats = mysql_fetch_array($cons);			  
			 $idsu = $dats['act_subanalisis']; 
			 //$idda = $dats['act_clave_int'];  	
		  
		 }
		 else
		 {
			$res = 2;
		    $idda = 0;	
			$idsu = 0;
		 }
		  $datos[]  =array("res"=>$res, "idda"=>$idda,"idsu"=>$idsu);
	 echo json_encode($datos);
  }

  else if($opcion=="GUARDARDETALLERENACTSU")
  {
		$id = $_POST['id'];
		$cantidad = $_POST['cantidad'];
		$act = $_POST['act'];
		$pre = $_POST['pre'];
		$gru = $_POST['gru'];
		$cap= $_POST['cap'];
	  	
		$upd = mysql_query("update pre_gru_cap_act_sub_insumo set pgi_rend_sub_act ='".$cantidad."',pgi_usu_actualiz='".$usuario."',pgi_fec_actualiz='".$fecha."' where pre_clave_int = '".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."' and act_clave_int ='".$act."' and act_subanalisis = '".$id."'");
	  if($upd>0)
		 {
			 $res = 1;
			 $coni = mysql_query("select pgca_clave_int from pre_gru_cap_actividad where  pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."' limit 1");
			 $dati = mysql_fetch_array($coni);			  
			 $idda = $dati['pgca_clave_int']; 
			
		 }
		 else
		 {
			$res = 2;
		    $idda = 0;	
			
		 }
		  $datos[]  =array("res"=>$res, "idda"=>$idda);
	 echo json_encode($datos);
  }
    else if($opcion=="GUARDARDETALLEVALACTS")
  {
	$id = $_POST['id'];
	$valor = $_POST['valor'];
	$act = $_POST['act'];
	$pre = $_POST['pre'];
	$gru = $_POST['gru'];
	$cap= $_POST['cap'];
	  $upd = mysql_query("update pre_gru_cap_act_sub_insumo set pgi_vr_act ='".$valor."',pgi_usu_actualiz='".$usuario."',pgi_fec_actualiz='".$fecha."' where pgi_clave_int = '".$id."'");
	  if($upd>0)
		 {
			 $res = 1;
			 $coni = mysql_query("select pgca_clave_int from pre_gru_cap_actividad where  pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."' limit 1");
			 $dati = mysql_fetch_array($coni);			  
			 $idda = $dati['pgca_clave_int'];	
			 $cons= mysql_query("select act_subanalisis,act_clave_int from pre_gru_cap_act_sub_insumo where pgi_clave_int = '".$id."'");
			 $dats = mysql_fetch_array($cons);			  
			 $idsu = $dats['act_subanalisis']; 
			 //$idda = $dats['act_clave_int'];  				  		  
		 }
		 else
		 {
			 $res = 2;
		     $idda = 0;
			 $idsu = 0;	
		 }
		  $datos[]  =array("res"=>$res, "idda"=>$idda,"idsu"=>$idsu);
	 echo json_encode($datos);
  }
  else if($opcion=="GUARDARDETALLERENACT")
  {
     $id = $_POST['id'];
	 $act = $_POST['act'];
     $pre = $_POST['pre'];
	 $gru = $_POST['gru'];
     $cap= $_POST['cap'];
	 $ins = $_POST['ins'];
	 $cantidad = $_POST['cantidad'];

      $cona = mysql_query("select pre_apli_iva from presupuesto where pre_clave_int = '".$pre."'");
      $data = mysql_fetch_array($cona);
      $apli = $data['pre_apli_iva'];

	 $con = mysql_query("select * from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	 $num = mysql_num_rows($con);
	 if($num > 0)
	 {
	 	$upd = mysql_query("update pre_gru_cap_act_insumo set pgi_rend_act ='".$cantidad."',pgi_usu_actualiz='".$usuario."',pgi_fec_actualiz='".$fecha."' where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	 }
	 else
	 {
	 	$upd = mysql_query("insert into pre_gru_cap_act_insumo(pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int,ins_clave_int,pgi_rend_act,pgi_vr_act,pgi_usu_actualiz,pgi_fec_actualiz) values('".$pre."','".$gru."','".$cap."','".$act."','".$ins."','".$cantidad."','0','".$usuario."','".$fecha."')");
	 }
	 if($upd>0)
	 {
	  /*   if($cantidad<=0)
         {
             //consultar informacion de la tabla de partidad item asociada  al recurso en pre,gru,cap y act
             $conit = mysql_query("select par_clave_int,pai_cant_comprometida,pai_val_comprometido from partida_item where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
             $numit = mysql_num_rows($conit);
             if($numit>0)
             {
                 for($nt=0;$nt<$numit;$nt++)
                 {
                     $datit = mysql_fetch_array($conit);
                     $par = $datit['par_clave_int'];
                     $canc = $datit['pai_cant_comprometida'];
                     $valc = $datit['pai_val_comprometido'];
                     //consultar a la tabla partida observacion y restarle a pao_valor el valor comprometido de ese recurso en esa partida
                     $updpar = mysql_query("update partida_observacion set pao_valor = pao_valor - '".$valc."' where par_clave_int = '".$par."' and ins_clave_int = '".$ins."'");

                 }
                 $delpar = mysql_query("DELETE FROM partida_item WHERE pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
             }
         }*/
			
		/*	$cona = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$act."'   order by tip,tpl,nom,nomu");*/

         $cona = mysql_query("select d.pgi_clave_int idd,i.ins_clave_int id,pgi_rend_act,pgi_vr_act,pgi_imp_act,pgi_uti_act,pgi_iva_act,pgi_adm_act from  pre_gru_cap_act_insumo d join insumos i on (d.ins_clave_int = i.ins_clave_int) where d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."'  order by idd");
			$totalua = 0;
			while($dat = mysql_fetch_array($cona))
			{
			/*$idc = $dat['idd'];
			$val = $dat['val'];
			$val1 = $dat['val1'];		
			$ren = $dat['ren'];
			$tpl = $dat['tpl'];*/
			$idi = $dat['id'];
			
			/*$con1 = mysql_query("select pgi_rend_act,pgi_vr_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_gru_cap_act_insumo where act_clave_int = '".$act."' and pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and ins_clave_int = '".$idi."'");
			$dato1 = mysql_fetch_array($con1);*/
			$renact = $dat['pgi_rend_act'];
			$valact = $dat['pgi_vr_act'];
			$admact = $dat['pgi_adm_act'];
			$impact = $dat['pgi_imp_act'];
			$utiact = $dat['pgi_uti_act'];
			$ivaact = $dat['pgi_iva_act'];
			/*if($renact == '' || $renact ==0 ){ $renact = $ren; }
			if($val1<=0){$valor = $val;}else{$valor=$val1;}
			if($valact == '' || $valact == 0){ $valact = $valor; }*/
                $tadmact = ($valact * $admact) / 100;
                $timpact = ($valact * $impact) / 100;
                $tutiact = ($valact * $utiact) / 100;
                //$tivaact = ($tutiact * $ivaact) / 100;

                if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
                { $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}


                $valact = $valact +($tadmact+$timpact+$tutiact+$tivaact);
                $totalua = $totalua + ($valact * $renact);
			}
			$update =mysql_query("update pre_gru_cap_actividad set pgca_valor_acta =pgca_cant_proyectada * '".$totalua."' where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."'");
	 
		 if($update>0)
		 {
			 $coni = mysql_query("select pgca_clave_int from pre_gru_cap_actividad where  pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."' limit 1");
			 $dati = mysql_fetch_array($coni);			  
			 $idda = $dati['pgca_clave_int'];
		 }
		 else
		 {
		    $idda = 0;	
		 }
        $tot = $totalua;
        $res =  1;
	 }
	 else
	 {
	    $res =  2;
		$idda = 0;
		$tot = "No";
	 }
	 $datos[]  =array("res"=>$res, "idda"=>$idda,"tot"=>$tot,"upd"=>$upd);
	 echo json_encode($datos);
  }
  else if($opcion=="GUARDARDETALLEVALACT")
  {
     $id = $_POST['id'];
     $act = $_POST['act'];
     $pre = $_POST['pre'];
	 $gru = $_POST['gru'];
     $cap= $_POST['cap'];
	 $ins = $_POST['ins'];
	 $cantidad = $_POST['cantidad'];

      $cona = mysql_query("select pre_apli_iva from presupuesto where pre_clave_int = '".$pre."'");
      $data = mysql_fetch_array($cona);
      $apli = $data['pre_apli_iva'];
	 $con = mysql_query("select * from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	 $num = mysql_num_rows($con);
	 if($num > 0)
	 {
	 	$upd = mysql_query("update pre_gru_cap_act_insumo set pgi_vr_act ='".$cantidad."',pgi_usu_actualiz='".$usuario."',pgi_fec_actualiz='".$fecha."' where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	 }
	 else
	 {
	 	$upd = mysql_query("insert into pre_gru_cap_act_insumo(pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int,ins_clave_int,pgi_rend_act,pgi_vr_act,pgi_usu_actualiz,pgi_fec_actualiz) values('".$pre."','".$gru."','".$cap."','".$act."','".$ins."','0','".$cantidad."','".$usuario."','".$fecha."')");
	 }
	 if($upd>0)
	 {
        /* if($cantidad<=0)
         {
             //consultar informacion de la tabla de partidad item asociada  al recurso en pre,gru,cap y act
             $conit = mysql_query("select par_clave_int,pai_cant_comprometida,pai_val_comprometido from partida_item where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
             $numit = mysql_num_rows($conit);
             if($numit>0)
             {
                 for($nt=0;$nt<$numit;$nt++)
                 {
                     $datit = mysql_fetch_array($conit);
                     $par = $datit['par_clave_int'];
                     $canc = $datit['pai_cant_comprometida'];
                     $valc = $datit['pai_val_comprometido'];
                     //consultar a la tabla partida observacion y restarle a pao_valor el valor comprometido de ese recurso en esa partida
                     $updpar = mysql_query("update partida_observacion set pao_valor = pao_valor - '".$valc."' where par_clave_int = '".$par."' and ins_clave_int = '".$ins."'");

                 }
                 $delpar = mysql_query("DELETE FROM partida_item WHERE pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
             }
         }*/
			
			$cona = mysql_query("select d.pgi_clave_int idd,i.ins_clave_int id,pgi_rend_act,pgi_vr_act,pgi_imp_act,pgi_uti_act,pgi_iva_act,pgi_adm_act from  pre_gru_cap_act_insumo d join insumos i on (d.ins_clave_int = i.ins_clave_int) where d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and  d.act_clave_int = '".$act."'   order by idd");
			$totalua = 0;
			while($dat = mysql_fetch_array($cona))
			{
			$idc = $dat['idd'];
			$idi = $dat['id'];
			
			/*$con1 = mysql_query("select pgi_rend_act,pgi_vr_act,pgi_imp_act,pgi_uti_act,pgi_iva_act,pgi_adm_act from pre_gru_cap_act_insumo where act_clave_int = '".$act."' and pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and ins_clave_int = '".$idi."'");
			$dato1 = mysql_fetch_array($con1);*/
			$renact = $dat['pgi_rend_act'];
			$valact = $dat['pgi_vr_act'];
			$admact = $dat['pgi_adm_act'];
			$impact = $dat['pgi_imp_act'];
			$utiact = $dat['pgi_uti_act'];
			$ivaact = $dat['pgi_iva_act'];
			/*if($renact == '' || $renact ==0 ){ $renact = $ren; }
			if($val1<=0){$valor = $val;}else{$valor=$val1;}
			if($valact == '' || $valact == 0){ $valact = $valor; }*/
			
			$tadmact = ($valact*$admact)/100;
			$timpact = ($valact*$impact)/100;
			$tutiact = ($valact*$utiact)/100;
                if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
                { $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}
                //$tivaact = ($tutiact*$ivaact)/100;
                $valact = $valact +($tadmact+$timpact+$tutiact+$tivaact);
                $totalua = $totalua + ($valact * $renact);
			}
			$update =mysql_query("update pre_gru_cap_actividad set pgca_valor_acta = pgca_cant_proyectada * '".$totalua."' where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."'");
	 
		 if($update>0)
		 {
			 $coni = mysql_query("select pgca_clave_int from pre_gru_cap_actividad where  pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."' limit 1");
			 $dati = mysql_fetch_array($coni);
			  
			 $idda = $dati['pgca_clave_int'];  
		  
		 }
		 else
		 {
		    $idda = 0;	
		 }
        $tot = $totalua;
        $res =  1;
	 }
	 else
	 {
	    $res = 2;
		$idda = 0;
		$tot = "No";
	 }
	 $datos[]  =array("res"=>$res, "idda"=>$idda,"tot"=>$tot);
	 echo json_encode($datos);
  }
  else if($opcion=="CAMBIOALCANCE")
  {
     $id = $_POST['id'];
     $pre = $_POST['pre'];
	 $gru = $_POST['gru'];
	 $cap = $_POST['cap'];
	 $act = $_POST['act'];
	 $ins = $_POST['ins'];
	 $material = 0;
	 $equipo = 0;
	 $mano = 0;
      $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
      $data = mysql_fetch_array($cona);
      $apli = $data['apli'];
	 
	 $con = mysql_query("select pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	 $dato = mysql_fetch_array($con);
	 $rennew = $dato['pgi_rend_act'];
	 $valnew = $dato['pgi_vr_act'];	 
	$admact = $dato['pgi_adm_act'];
	$impact = $dato['pgi_imp_act'];
	$utiact = $dato['pgi_uti_act'];			
	$ivaact = $dato['pgi_iva_act'];
	
	$renold = $dato['pgi_rend_ini'];
	$valold = $dato['pgi_vr_ini'];
	$admold = $dato['pgi_adm_ini'];
	$impold = $dato['pgi_imp_ini'];
	$utiold = $dato['pgi_uti_ini'];			
	$ivaold = $dato['pgi_iva_ini'];

	$tadmold = ($valold*$admold)/100;
	$timpold = ($valold*$impold)/100;
	$tutiold = ($valold*$utiold)/100;
	if($apli==0) {$tivaold = ($tutiold*$ivaold)/100;}
      else {$tivaold = (($tadmold+$timpold+$tutiold)*$ivaold)/100;}

	$valoldi = $valold +($tadmold+$timpold+$tutiold+$tivaold);
	$totalcold = $renold*$valoldi;
	 
	 $con = mysql_query("select tpi_tipologia as tpl from insumos i join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int)  where i.ins_clave_int = '".$ins."'");
	 $dato = mysql_fetch_array($con);
	 $tpl = $dato['tpl'];
	 
//	 if($rennew==0){$rennew=$renold;} 
//	 if($valnew==0){$valnew=$totalcold;} 
	 

    $tadmact = ($valnew*$admact)/100;
    $timpact = ($valnew*$impact)/100;
    $tutiact = ($valnew*$utiact)/100;
    if($apli==0) {$tivaact = ($tutiact*$ivaact)/100;}
    else {$tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}
	  //$tivaact = ($tutiact*$ivaact)/100;
      $valnewa = $valnew +($tadmact+$timpact+$tutiact+$tivaact);
      $totalc = $rennew*$valnewa;
	 
	 
	 $total = $totalcold-$totalc;

      $incp = (($valnewa - $valnew)/$valnew)*100;
	 
	 if($tpl==1){$material=$totalc;}else if($tpl==2){$equipo = $totalc;}else if($tpl==3){$mano = $totalc;}
	 
	 $datos[] = array("totales"=>$total,"mate"=>$material,"equi"=>$equipo,"mano"=>$mano,"tpl"=>$tpl,"incp"=>number_format($incp,2,'.',',')."%");
	 echo json_encode($datos);
  }
   else if($opcion=="CAMBIOALCANCESUB")//CAMBIO ALCANCE EN SUBANALISIS
  {
     $id = $_POST['id'];
     $pre = $_POST['pre'];
	 $gru = $_POST['gru'];
	 $cap = $_POST['cap'];
	 $act = $_POST['act'];
	 $ins = $_POST['ins'];
	 $material = 0;
	 $equipo = 0;
	 $mano = 0;
	 
	 $con = mysql_query("select pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini from pre_gru_cap_act_sub_insumo where pgi_clave_int = '".$id."'");
	 $dato = mysql_fetch_array($con);
	 $rennew = $dato['pgi_rend_act'];
	 $valnew = $dato['pgi_vr_act'];	 
	$admact = $dato['pgi_adm_act'];
	$impact = $dato['pgi_imp_act'];
	$utiact = $dato['pgi_uti_act'];			
	$ivaact = $dato['pgi_iva_act'];
	
	$renold = $dato['pgi_rend_ini'];
	$valold = $dato['pgi_vr_ini'];
	$admold = $dato['pgi_adm_ini'];
	$impold = $dato['pgi_imp_ini'];
	$utiold = $dato['pgi_uti_ini'];			
	$ivaold = $dato['pgi_iva_ini'];
	$totalcold = $renold*$valold;
	$tadmold = ($totalcold*$admold)/100;
	$timpold = ($totalcold*$impold)/100;
	$tutiold = ($totalcold*$utiold)/100;
	$tivaold = ($tutiold*$ivaold)/100;
	$totalcold = $totalcold +($tadmold+$timpold+$tutiold+$tivaold);
	
	 
	 $con = mysql_query("select tpi_tipologia as tpl from insumos i join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int)  where i.ins_clave_int = '".$ins."'");
	 $dato = mysql_fetch_array($con);
	 $tpl = $dato['tpl'];
	 
//	 if($rennew==0){$rennew=$renold;} 
//	 if($valnew==0){$valnew=$totalcold;} 
	 
	$totalc = $rennew*$valnew;
	$tadmact = ($totalc*$admact)/100;
	$timpact = ($totalc*$impact)/100;
	$tutiact = ($totalc*$utiact)/100;
	$tivaact = ($tutiact*$ivaact)/100;
	$totalc = $totalc +($tadmact+$timpact+$tutiact+$tivaact); 
	 
	 $total = $totalcold-$totalc; 
	 
	 if($tpl==1){$material=$totalc;}else if($tpl==2){$equipo = $totalc;}else if($tpl==3){$mano = $totalc;}
	 
	 $datos[] = array("totales"=>$total,"mate"=>$material,"equi"=>$equipo,"mano"=>$mano,"tpl"=>$tpl);
	 echo json_encode($datos);
  }
   else if($opcion=="CAMBIOALCANCESUBA")//CAMBIO ALCANCE EN SUBANALISIS
  {
     $id = $_POST['id'];
     $pre = $_POST['pre'];
	 $gru = $_POST['gru'];
	 $cap = $_POST['cap'];
	 $act = $_POST['act'];
	 $cona = mysql_query("select pre_apli_iva from presupuesto where pre_clave_int = '".$pre."'");
	 $data = mysql_fetch_array($cona);
	 $apli = $data['pre_apli_iva'];	 
	 
	$consulta = mysql_query("SELECT i.act_clave_int idc,i.act_nombre nom,	u.uni_codigo cod,s.pgi_rend_sub_ini ri,s.pgi_rend_sub_act ra FROM actividades AS i JOIN pre_gru_cap_act_sub_insumo s ON s.act_subanalisis = i.act_clave_int JOIN tipoproyecto AS t ON t.tpp_clave_int = i.tpp_clave_int JOIN unidades AS u ON u.uni_clave_int = i.uni_clave_int WHERE s.pre_clave_int = '".$pre."' AND s.gru_clave_int = '".$gru."' AND s.cap_clave_int = '".$cap."' AND s.act_clave_int = '".$act."' and s.act_subanalisis = '".$id."' GROUP BY s.act_subanalisis ORDER BY i.act_nombre ASC");
	$nums = mysql_num_rows($consulta);
	$dats = mysql_fetch_array($consulta);
	$idc = $dats['idc'];
	$tipo = "";
	$ren = $dats['ri'];
	$renact = $dats['ra'];
	$tpl = 1;
	$material=0; $equipo = 0; $mano = 0;
	if($apli==0)
	{
		$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
		" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");
		
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totalcini = $totals + ($totads+$totims+$totuts+$totivs);
		
		$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
		",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
		" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");		
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totalc = $totals + ($totads+$totims+$totuts+$totivs);	
	}
	else
	{	   
		$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
		",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
		"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
		"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
		" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");							
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totalcini = $totals + ($totads+$totims+$totuts+$totivs);
		
		$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
		",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
		"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
		"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
		" from  pre_gru_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.gru_clave_int  = '".$gru."' and pa.cap_clave_int= '".$cap."' and a.act_clave_int = '".$act."' and pa.act_subanalisis = '".$idc."'");		
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totalc = $totals + ($totads+$totims+$totuts+$totivs);	
	}
	$totalc = $renact*$totalc;
	$totalcini = $ren*$totalcini;
	
	$totalact = ($totalcini)-($totalc);
	if($totalact == ''){ $totalact = 0; }	
	if($tpl==1){$material=$totalc;}else if($tpl==2){$equipo =  $totalc;}else if($tpl==3){$mano = $totalc;}				  
	 
	 $datos[] = array("totales"=>$totalact,"mate"=>$material,"equi"=>$equipo,"mano"=>$mano,"tpl"=>$tpl,"rena"=>$renact,"valact"=>$totalc,"ri"=>$ren,"ra"=>$renact);
	 echo json_encode($datos);
  }
  else if($opcion=="DATOSAIU")
  {
	  $idd = $_POST['idd'];//id
	  $pre = $_POST['pre'];
	  $gru = $_POST['gru'];
	  $cap = $_POST['cap'];
	  $id = $_POST['act'];
		$conact = mysql_query("select act_clave_int from pre_gru_cap_actividad where pgca_clave_int = '".$id."'");
	$datact = mysql_fetch_array($conact);
	$act = $datact['act_clave_int'];
	$ins = $_POST['ins'];
	
	$con = mysql_query("select * from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	$dat = mysql_fetch_array($con);
	$val = $dat['pgi_vr_act'];
	$adm = $dat['pgi_adm_act'];
	$imp = $dat['pgi_imp_act'];
	$uti = $dat['pgi_uti_act'];
	$iva = $dat['pgi_iva_act'];
     ?>
<div class="row" >
  <div class="col-m2"></div>
  <div class="col-md-2"><strong>ADM:</strong>
    <input  onKeyPress='return NumCheck(event, this)' type="text" id="adm<?php echo $pre.'_'.$gru.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="guardaraiu('<?php echo $idd;?>','<?php echo $pre;?>','<?php echo $gru;?>','<?php echo $cap;?>','<?php echo $act;?>','<?php echo $ins;?>')"  class="form-control input-sm" value="<?php echo $adm;?>" style="width:80px" />
  </div>
  <div class="col-md-2"><strong>IMP:</strong>
    <input onKeyPress='return NumCheck(event, this)' type="text" id="imp<?php echo $pre.'_'.$gru.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="guardaraiu('<?php echo $idd;?>','<?php echo $pre;?>','<?php echo $gru;?>','<?php echo $cap;?>','<?php echo $act;?>','<?php echo $ins;?>')"  class="form-control input-sm" value="<?php echo $imp;?>" style="width:80px" />
  </div>
  <div class="col-md-2"><strong>UTI:</strong>
    <input onKeyPress='return NumCheck(event, this)' type="text" id="uti<?php echo $pre.'_'.$gru.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="guardaraiu('<?php echo $idd;?>','<?php echo $pre;?>','<?php echo $gru;?>','<?php echo $cap;?>','<?php echo $act;?>','<?php echo $ins;?>')" class="form-control input-sm" value="<?php echo $uti;?>" style="width:80px" />
  </div>
  <div class="col-md-2"><strong>IVA:</strong>
    <input onKeyPress='return NumCheck(event, this)' type="text" id="iva<?php echo $pre.'_'.$gru.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="guardaraiu('<?php echo $idd;?>','<?php echo $pre;?>','<?php echo $gru;?>','<?php echo $cap;?>','<?php echo $act;?>','<?php echo $ins;?>')"  class="form-control input-sm" value="<?php echo $iva;?>"  style="width:80px" />
  </div>
  <div class="col-md-2"><span id="span<?php echo $pre.'_'.$gru.'_'.$cap.'_'.$act.'_'.$ins;?>"></span></div>
</div>
<?php
  }
  else if($opcion=="DATOSAIUE")
  {
      $idd = $_POST['idd'];//id
      $pre = $_POST['pre'];
      $idc = $_POST['idc'];
      $cap = $_POST['cap'];
      $act = $_POST['act'];
      /*$conact = mysql_query("select act_clave_int from pre_gru_cap_actividad where pgca_clave_int = '".$id."'");
      $datact = mysql_fetch_array($conact);
      $act = $datact['act_clave_int'];*/
      $ins = $_POST['ins'];

      $con = mysql_query("select * from pre_cap_act_insumo where pre_clave_int = '".$pre."'  and pev_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
      $dat = mysql_fetch_array($con);
      $val = $dat['pgi_vr_act'];
      $adm = $dat['pgi_adm_act'];
      $imp = $dat['pgi_imp_act'];
      $uti = $dat['pgi_uti_act'];
      $iva = $dat['pgi_iva_act'];
      $iddi = $dat['pgi_clave_int'];
      ?>
      <div class="row" >
          <div class="col-m2"></div>
          <div class="col-md-2"><strong>ADM:</strong>
              <input   onKeyPress='return NumCheck(event, this)' type="text" id="adme<?php echo $pre.'_'.$iddi.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="CRUDPRESUPUESTOINICIAL('ACTUALIZARRECURSOAIUE','<?php echo $act;?>','<?php echo $ins;?>','<?php echo $iddi;?>','<?php echo $cap;?>')"  class="form-control input-sm" value="<?php echo $adm;?>" style="width:80px" />
          </div>
          <div class="col-md-2"><strong>IMP:</strong>
              <input onKeyPress='return NumCheck(event, this)' type="text" id="impe<?php echo $pre.'_'.$iddi.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="CRUDPRESUPUESTOINICIAL('ACTUALIZARRECURSOAIUE','<?php echo $act;?>','<?php echo $ins;?>','<?php echo $iddi;?>','<?php echo $cap;?>')"   class="form-control input-sm" value="<?php echo $imp;?>" style="width:80px" />
          </div>
          <div class="col-md-2"><strong>UTI:</strong>
              <input onKeyPress='return NumCheck(event, this)' type="text" id="utie<?php echo $pre.'_'.$iddi.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="CRUDPRESUPUESTOINICIAL('ACTUALIZARRECURSOAIUE','<?php echo $act;?>','<?php echo $ins;?>','<?php echo $iddi;?>','<?php echo $cap;?>')"  class="form-control input-sm" value="<?php echo $uti;?>" style="width:80px" />
          </div>
          <div class="col-md-2"><strong>IVA:</strong>
              <input onKeyPress='return NumCheck(event, this)' type="text" id="ivae<?php echo $pre.'_'.$iddi.'_'.$cap.'_'.$act.'_'.$ins;?>" onchange="CRUDPRESUPUESTOINICIAL('ACTUALIZARRECURSOAIUE','<?php echo $act;?>','<?php echo $ins;?>','<?php echo $iddi;?>','<?php echo $cap;?>')"   class="form-control input-sm" value="<?php echo $iva;?>"  style="width:80px" />
          </div>
          <div class="col-md-2"><span id="spane<?php echo $pre.'_'.$iddi.'_'.$cap.'_'.$act.'_'.$ins;?>"></span></div>
      </div>
      <?php
  }
  else if($opcion=="GUARDARDETALLEAIU")
  {
	  
     $id = $_POST['id'];
	 $act = $_POST['act'];
     $pre = $_POST['pre'];
	 $gru = $_POST['gru'];
     $cap= $_POST['cap'];
	 $ins = $_POST['ins'];
	 $adm = $_POST['adm'];
	 $imp = $_POST['imp'];
	 $uti = $_POST['uti'];
	 $iva = $_POST['iva'];
	 
	 $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
	 $data = mysql_fetch_array($cona);
	 $apli = $data['apli'];
	 
	 $con = mysql_query("select * from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	 $num = mysql_num_rows($con);
	 if($num > 0)
	 {
	 	$upd = mysql_query("update pre_gru_cap_act_insumo set pgi_adm_act ='".$adm."',pgi_imp_act='".$imp."',pgi_uti_act = '".$uti."',pgi_iva_act='".$iva."',pgi_usu_actualiz='".$usuario."',pgi_fec_actualiz='".$fecha."' where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
	 }
	 else
	 {
	 	$upd = mysql_query("insert into pre_gru_cap_act_insumo(pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int,ins_clave_int,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act,pgi_usu_actualiz,pgi_fec_actualiz) values('".$pre."','".$gru."','".$cap."','".$act."','".$ins."','".$adm."','".$imp."','".$uti."','".$iva."','".$usuario."','".$fecha."')");
	 }
	 if($upd>0)
	 {
		 
		 //actualizar aiu donde aparezaca el recurso en este presupuesto
		 $updateinsumos = mysql_query("UPDATE pre_gru_cap_act_insumo SET pgi_adm_act = '".$adm."',pgi_imp_act = '".$imp."',pgi_uti_act = '".$uti."', pgi_iva_act = '".$iva."' WHERE pre_clave_int = '".$pre."' and ins_clave_int = '".$ins."'");
		 
		$updateinsumos2 = mysql_query("UPDATE pre_gru_cap_act_sub_insumo SET pgi_adm_act = '".$adm."',pgi_imp_act = '".$imp."',pgi_uti_act = '".$uti."', pgi_iva_act = '".$iva."' WHERE pre_clave_int = '".$pre."' and ins_clave_int = '".$ins."'");

		 
			$con = mysql_query("select d.pgi_clave_int idd, i.ins_clave_int idi,i.ins_nombre AS nom,t.tpi_nombre tip,u.uni_codigo AS cod,u.uni_nombre nomu,i.ins_valor AS val,d.pgi_rend_ini,d.pgi_vr_ini,d.pgi_adm_ini,d.pgi_imp_ini,d.pgi_uti_ini,d.pgi_iva_ini,d.pgi_rend_act,d.pgi_vr_act,d.pgi_adm_act,d.pgi_imp_act,d.pgi_uti_act,d.pgi_iva_act,t.tpi_tipologia AS tpl,d.pgi_creacion as cre from  insumos i JOIN pre_gru_cap_act_insumo d ON (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where d.pre_clave_int = '".$pre."' AND d.gru_clave_int = '".$gru."' AND d.cap_clave_int = '".$cap."' AND d.act_clave_int = '".$act."' and i.ins_clave_int = '".$ins."' order by tip,tpl,nom,nomu");
			
			$dat = mysql_fetch_array($con);		
			$val = $dat['val']; if($val=="null" || $val==NULL){$val=0;}
			$cod = $dat['cod'];
			$tpl = $dat['tpl'];
			$insu = $dat['idi'];
			$cre  = $dat['cre'];
				  
			$ren = $dat['pgi_rend_ini']; if($ren=="null" || $ren==NULL){$ren=0;}
			$valor = $dat['pgi_vr_ini']; if($valor=="null" || $valor==NULL){$valor=0;}
			$admini = $dat['pgi_adm_ini'];
			$impini = $dat['pgi_imp_ini'];
			$utiini = $dat['pgi_uti_ini'];			
			$ivaini = $dat['pgi_iva_ini'];
					
			
			$tadmini = ($valor*$admini)/100;
			$timpini = ($valor*$impini)/100;
			$tutiini = ($valor*$utiini)/100;
			if($apli==0) {$tivaini = ($tutiini*$ivaini)/100;}
			else {$tivaini = (($tadmini+$timpini+$tutiini)*$ivaini)/100;}
			$valori = $valor +($tadmini+$timpini+$tutiini+$tivaini);
			$totalcini = $ren*$valori;	//INICIAL

			$renact = $dat['pgi_rend_act'];
			$valact = $dat['pgi_vr_act'];
			$admact = $dat['pgi_adm_act'];
			$impact = $dat['pgi_imp_act'];
			$utiact = $dat['pgi_uti_act'];			
			$ivaact = $dat['pgi_iva_act'];	
			$material=0; $equipo = 0; $mano = 0;
			$mate = 0; $equi = 0; $man = 0;	
			
			
			$tadmact = ($valact*$admact)/100;
			$timpact = ($valact*$impact)/100;
			$tutiact = ($valact*$utiact)/100;
			if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
			{ $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}
            $valora = $valact +($tadmact+$timpact+$tutiact+$tivaact);
			$totalc = $renact*$valora;//ACTUAL
			$totalact = ($totalcini)-($totalc);
			if($totalact == ''){ $totalact = 0; }

			//porcentaje de aumento actual
           $incp = (($valora - $valact)/$valact)*100;
			
			if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
			$total = $material + $equipo + $mano;				   
			//BOTONES
			
			
			/*$cona = mysql_query("select d.ati_clave_int idd, i.ins_clave_int id,i.ins_nombre as nom,t.tpi_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,d.ati_rendimiento as ren,i.ins_valor as val,d.ati_valor as val1, i.ins_descripcion des,t.tpi_tipologia as tpl from  actividades a join actividadinsumos d on (a.act_clave_int = d.act_clave_int) join insumos i on (d.ins_clave_int = i.ins_clave_int) join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) join unidades u on (u.uni_clave_int = i.uni_clave_int) where a.act_clave_int = '".$act."'   order by tip,tpl,nom,nomu");
			$totalua = 0;
			while($dat = mysql_fetch_array($cona))
			{
			$idc = $dat['idd'];
			$val = $dat['val'];
			$val1 = $dat['val1'];		
			$ren = $dat['ren'];
			$tpl = $dat['tpl'];
			$idi = $dat['id'];
			
			
			$con1 = mysql_query("select pgi_rendimiento,pgi_vr_unitario,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_gru_cap_act_insumo where act_clave_int = '".$act."' and pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and ins_clave_int = '".$idi."'");
			$dato1 = mysql_fetch_array($con1);
			$renact = $dato1['pgi_rendimiento'];
			$valact = $dato1['pgi_vr_unitario'];
			$admact = $dato1['pgi_adm_act'];
			$impact = $dato1['pgi_imp_act'];
			$utiact = $dato1['pgi_uti_act'];			
			$ivaact = $dato1['pgi_iva_act'];
			if($renact == '' || $renact ==0 ){ $renact = $ren; }
			if($val1<=0){$valor = $val;}else{$valor=$val1;}
			if($valact == '' || $valact == 0){ $valact = $valor; }
			
			$tadmact = ($valact*$admact)/100;
			$timpact = ($valact*$impact)/100;
			$tutiact = ($valact*$utiact)/100;
			$tivaact = ($tutiact*$ivaact)/100;
			$valact = $valact +($tadmact+$timpact+$tutiact+$tivaact);
			$totalua = $totalua + ($valact * $renact);
			}*/
			$update =mysql_query("update pre_gru_cap_actividad set pgca_valor_acta ='".$totalua."' where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."'");
	 
		 if($update>0)
		 {
			 $coni = mysql_query("select pgca_clave_int from pre_gru_cap_actividad where  pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."' limit 1");
			 $dati = mysql_fetch_array($coni);
			  
			 $idda = $dati['pgca_clave_int'];  
		  
		 }
		 else
		 {
		    $idda = 0;	
		 }
        $tot = $totalua;
        $res =  1;
	 }
	 else
	 {
	    $res =  2;
		$idda = 0;
		$tot = "No";
	 }
	 $datos[]  =array("res"=>$res, "idda"=>$idda,"equi"=>$equi,"mano"=>$man,"mate"=>$mate,"alcance"=>$totalact,"acti"=>$act,"incp"=>number_format($incp,2,'.',',')."%");
	 echo json_encode($datos);
  
  }
  else if($opcion=="ASOCIARAPU")
  {
	  
	  $ida = $_POST['id'];
	  $gru = $_POST['gru'];
	  $cap = $_POST['cap'];
	  $idd = $_POST['idd'];
	   $coninfo = mysql_query("select act_nombre as des,tpp_clave_int as tpp,uni_clave_int uni,act_analisis as ana,a.est_clave_int as est,c.ciu_clave_int ciu,d.dep_clave_int dep,p.pai_clave_int pai ,a.act_usu_creacion usu from actividades a join ciudad c on c.ciu_clave_int = a.ciu_clave_int join departamento d on d.dep_clave_int = c.dep_clave_int join pais p on p.pai_clave_int = d.pai_clave_int  where act_clave_int = '".$ida."' limit 1");
	  $datinfo = mysql_fetch_array($coninfo);
	
	  $tpp = $datinfo['tpp'];
	  $uni = $datinfo['uni'];
	  $ana = $datinfo['ana'];
	  $des = utf8_decode($datinfo['des']);
	  $est = $datinfo['est'];
	  $ciu = $datinfo['ciu'];
	  $pai = $datinfo['pai'];
	  $dep = $datinfo['dep'];
	  $usu = $datinfo['usu'];
	  if($usu == $idUsuario){$per="si";}else if($percla==1){$per="si";}else{$per = "no";}
	  
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $ida;?>">
  <input type="hidden" id="gruedicion" value="<?php echo $gru;?>">
  <input type="hidden" id="capedicion" value="<?php echo $cap;?>">
  <input type="hidden" id="iddedicion" value="<?php echo $idd;?>">
  <input type="hidden" id="idpermiso" value="<?php echo $per;?>" title="<?php echo $percla."--".$usu."---".$idUsuario;?>">
  <div class="row">
    <div class="col-md-12"><strong>Descripción:<span class="symbol required"></span></strong>
      <input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" value="<?php echo $des;?>" placeholder="Escribe aqui la descripcion" disabled>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4"><strong>Tipo de Proyecto:<span class="symbol required"></span></strong>
      <select name="seltipoproyecto"id="seltipoproyecto" class="form-control input-sm" disabled>
        <option value="">--seleccione--</option>
        <?PHP
		 $con = mysql_query("select tpp_clave_int,tpp_nombre from tipoproyecto where est_clave_int != 2");
		 while($dat = mysql_fetch_array($con))
		 {
		    $idp = $dat['tpp_clave_int'];
			$nom = $dat['tpp_nombre'];
			?>
        <option <?php if($tpp==$idp){echo 'selected';}?>  value="<?php echo $idp;?>"><?php echo $nom;?></option>
        <?Php	
		 }
		 ?>
      </select>
    </div>
    <div class="col-md-4"><strong>Unidad de medida:<span class="symbol required"></span></strong>
      <select name="selunidad" id="selunidad" class="form-control input-sm" disabled>
        <option value="">--seleccione--</option>
        <?php 
		 $con = mysql_query("select uni_clave_int,uni_codigo,uni_nombre from unidades ");
		 while($dat = mysql_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
        <option <?php if($uni==$idu){echo 'selected';}?> value="<?Php echo $idu;?>"><?php echo $cod;?></option>
        <?php
		 }
		 ?>
      </select>
    </div>
    <div class="col-md-2"><strong>SubAnalisis:</strong><br>
      <label for="opcion3">
        <input type="radio" name="radanalisis" id="opcion3" class="flat-red"  value="Si" <?php if($ana=="Si" || $ana==""){echo 'checked';}?> disabled>
        Si</label>
      <label for="opcion4">
        <input type="radio" name="radanalisis" id="opcion4" class="flat-red" value="No" <?php if($ana=="No"){echo 'checked';}?> disabled>
        No</label>
    </div>
    <div class="col-md-2"><strong>Estado:</strong><br>
      <?php 
		if($metact==2 and $percla!=1)
		{
		?>
      <label for="opcion1">
        <input type="radio" name="radestado" id="opcion1" disabled class="flat-red" value="1" <?php if($est==1 || $est==""){echo 'checked';}?> />
        Activo</label>
      <label for="opcion2">
        <input type="radio" name="radestado" id="opcion2" disabled  class="flat-red" value="0" <?php if($est==0){echo 'checked';}?> />
        Inactivo </label>
      <label>
        <input type="radio" name="radestado" id="opcion5" class="flat-red" disabled value="3" <?php if($est==3){echo 'checked';}?> />
        Por Aprobar</label>
      <?php
		}
		else
		{
		?>
      <label for="opcion1">
        <input type="radio" name="radestado" id="opcion1" class="flat-red" checked value="1" <?php if($est==1 || $est==""){echo 'checked';}?> disabled>
        Activo</label>
      <label for="opcion2">
        <input type="radio" name="radestado" id="opcion2" class="flat-red" value="0" <?php if($est==0){echo 'checked';}?> disabled>
        Inactivo </label>
      <?php
			if($est==3)
			{
			?>
      <label>
        <input type="radio" name="radestado" id="opcion5" class="flat-red" value="3" checked>
        Por Aprobar</label>
      <?php
			}
        }
        ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4"><strong>Pais:<span class="symbol required"></span></strong>
      <select name="selpais" id="selpais" class="form-control input-sm" onChange="cargardepartamento('selpais','seldepartamento',1)" disabled>
        <option value="">--seleccione--</option>
        <?php
        $con = mysql_query("select pai_clave_int,pai_nombre from pais where est_clave_int = 1 order by pai_nombre");
        while($dat = mysql_fetch_array($con))
        {
        ?>
        <option <?php if($pai==$dat['pai_clave_int']){echo 'selected';}?> value="<?php echo $dat['pai_clave_int']?>"><?php echo $dat['pai_nombre'];?></option>
        <?php
        }
        ?>
      </select>
    </div>
    <div class="col-md-4"><strong>Departamento:<span class="symbol required"></span></strong>
      <select name="seldepartamento" id="seldepartamento" class="form-control input-sm" onChange="cargarciudad('seldepartamento','selciudad','selpais')" disabled>
        <option value="">--seleccione--</option>
        <?php
            $con = mysql_query("select dep_clave_int,dep_nombre from departamento where pai_clave_int ='".$pai."' and est_clave_int = 1");
            while($dat = mysql_fetch_array($con))
            {
            ?>
        <option <?php if($dep==$dat['dep_clave_int']){echo 'selected';}?> value="<?php echo $dat['dep_clave_int']?>"><?php echo $dat['dep_nombre'];?></option>
        <?php
            }
            ?>
      </select>
    </div>
    <div class="col-md-4"><strong>Ciudad:<span class="symbol required"></span></strong>
      <select name="selciudad" id="selciudad" class="form-control input-sm" disabled>
        <option value="">--seleccione--</option>
        <?php
	     $con = mysql_query("select ciu_clave_int,ciu_nombre from ciudad where dep_clave_int ='".$dep."' and est_clave_int = 1");
	 while($dat = mysql_fetch_array($con))
	 {
     ?>
        <option <?php if($ciu==$dat['ciu_clave_int']){echo 'selected';}?> value="<?Php echo $dat['ciu_clave_int'];?>"><?php echo $dat['ciu_nombre'];?></option>
        <?php
		}
	 ?>
      </select>
    </div>
  </div>
  <?php  
	  echo "<style onload=CRUDPRESUPUESTOINICIAL('AGREGADOS','".$ida."','','".$gru."','".$cap."')></style>";
	  ?>
  <div class="row">
    <hr class="divider">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs pull-left" id="nav1" title="ASIGNACION DE APU Y SUBANALISIS">
        <li class="active"> <a onclick="CRUDPRESUPUESTOINICIAL('AGREGADOS','<?PHP echo $ida;?>','','','');" data-toggle="tab" aria-expanded="true"><strong>APU</strong></a> </li>
        <li style="display:none"> <a onclick="CRUDPRESUPUESTOINICIAL('AGREGADOSSUB','<?PHP echo $ida;?>','','','');" data-toggle="tab" aria-expanded="true"><strong>SUB-ANALISIS</strong></a> </li>
      </ul>
    </div>
    <div class="col-md-12">
      <div id="pendientes"></div>
    </div>
  </div>
</form>
<?PHP  
  
  }
  else if($opcion=="AGREGARAPU")
  {
	  $act = $_POST['id'];
	  $pre = $_POST['pre'];
	  $gru = $_POST['gru'];
	  $cap = $_POST['cap'];
      $insumo = $_POST['insumo'];
	  $rendimiento = $_POST['rendimiento'];
	  $con = mysql_query("select ins_valor val from insumos where ins_clave_int = '".$insumo."' ");
	 $dat = mysql_fetch_array($con);
	 $valor = $dat['val']; if($valor=="" || $valor==NULL){$valor=0;}
	 
	 $conc = mysql_query("select pgca_cantidad as ca from pre_gru_cap_actividad where pre_clave_int = '".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' limit 1");
	 $datc = mysql_fetch_array($conc);
	 $cant = $datc['ca'];
	 
	  $ins = mysql_query("insert into actividadinsumos(act_clave_int,ins_clave_int,ati_rendimiento,ati_valor) values ('".$act."','".$insumo."','".$rendimiento."','".$valor."')");
	  if($ins>0)
	  {
			
		$insi = mysql_query("insert into pre_gru_cap_act_insumo(pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int,ins_clave_int,pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_usu_actualiz,pgi_fec_actualiz,pgi_cant_ini,pgi_cant_act) values ('".$pre."','".$gru."','".$cap."','".$act."','".$insumo."','".$rendimiento."','".$valor."','".$rendimiento."','".$valor."','".$usuario."','".$fecha."','".$cant."','".$cant."')");
		
			  
		  
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
   else if($opcion=="DELETEAPU")
  {
	  $ide= $_POST['id'];
	  $pre = $_POST['pre'];
	  $gru = $_POST['gru'];
	  $cap = $_POST['cap'];
	  $cona = mysql_query("select act_clave_int,ins_clave_int from actividadinsumos where ati_clave_int = '".$ide."' limit 1");
	  $data = mysql_fetch_array($cona); $act = $data['act_clave_int']; $insu = $data['ins_clave_int'];
	  $ins = mysql_query("delete from actividadinsumos where ati_clave_int = '".$ide."'");
	  if($ins>0)
	  {
		  $delete = mysql_query("delete from pre_gru_cap_act_insumo where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int ='".$act."'  and ins_clave_int = '".$insu."'");
		  
		  echo 1;
	  }
	  else
	  {
		  echo 2;
	  }
  }
  else if($opcion=="AGREGADOSSUB")
  {
	  
	  $id = $_POST['id'];
	  $coninfo = mysql_query("select act_usu_creacion,est_clave_int from actividades a  where act_clave_int = '".$id."' limit 1");
	  $datinfo = mysql_fetch_array($coninfo);
	  $usu = $datinfo['act_usu_creacion'];
	  $esta = $datinfo['est_clave_int'];
	  if($usu == $idUsuario){$per="si";}else{$per = "no";}
	  ?>
<script src="js/jssubeditaragregados.js"></script> 
<script src="js/jssubeditarporgregar.js"></script>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="col-md-6">
      <table id="tbactividadporagregar" class=" table table-bordered table-condensed compact" style="font-size:11px">
        <thead>
          <tr>
            <th class="dt-head-center" style="width:20px"></th>
            <th class="dt-head-center">NOMBRE</th>
            <th class="dt-head-center">TIPO</th>
            <th class="dt-head-center">UNIDAD</th>
            <th class="dt-head-center">CIUDAD</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th class="dt-head-center" style="width:20px"></th>
            <th class="dt-head-center"></th>
            <th class="dt-head-center"></th>
            <th class="dt-head-center"></th>
            <th class="dt-head-center"></th>
          </tr>
        </tfoot>
      </table>
    </div>
    <div class="col-md-6">
      <table id="tbactividadagregados" class=" table table-bordered table-condensed compact" style="font-size:11px">
        <thead>
          <tr>
            <th class="dt-head-center" style="width:20px"></th>
            <th class="dt-head-center">NOMBRE</th>
            <th class="dt-head-center">TIPO</th>
            <th class="dt-head-center">UNIDAD</th>
            <th class="dt-head-center">CIUDAD</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th class="dt-head-center" style="width:20px"></th>
            <th class="dt-head-center"></th>
            <th class="dt-head-center"></th>
            <th class="dt-head-center"></th>
            <th class="dt-head-center"></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<?PHP
  
  }
  else
  if($opcion=="AGREGADOS")
  {
	  $id = $_POST['id'];
	  $coninfo = mysql_query("select act_usu_creacion,est_clave_int from actividades a  where act_clave_int = '".$id."' limit 1");
	  $datinfo = mysql_fetch_array($coninfo);
	  $usu = $datinfo['act_usu_creacion'];
	  $esta = $datinfo['est_clave_int'];
	  if($usu == $idUsuario){$per="si";}else{$per = "no";}
	  ?>
<script src="js/jsinsumosagregadospi.js"></script> 
<script src="js/jsinsumoseditarporagregarpi.js"></script>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="col-md-6">
      <table id="tbinsumosporagregar" class=" table table-condensed compact table-hover" style="font-size:11px">
        <thead>
          <tr>
            <th class="dt-head-center"></th>
            <th class="dt-head-center">TIPO</th>
            <th class="dt-head-center">NOMBRE</th>
            <th class="dt-head-center">UNIDAD</th>
            <th class="dt-head-center">REND</th>
            <th class="dt-head-center">V/R UNITARIO</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </tfoot>
      </table>
    </div>
    <div class="col-md-6">
      <table id="tbinsumospendientes" style=" font-size:11px" class="table table-condensed compact table-hover">
        <thead>
          <tr>
            <th class="dt-head-center"></th>
            <th class="dt-head-center">TIPO</th>
            <th class="dt-head-center">NOMBRE</th>
            <th class="dt-head-center">UNIDAD</th>
            <th class="dt-head-center">REND</th>
            <th class="dt-head-center">V/R UNITARIO</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<?PHP
  }
  else if($opcion=="ACTUALIZARFACTURAS")
  {
	$consulta = mysql_query("select * from control_egreso where cpe_clave_int not in(SELECT c.cpe_clave_int FROM control_egreso c JOIN estados_contrato e ON UPPER(c.cpe_beneficiario) = UPPER(CONCAT_WS('-',e.esc_contratista,CAST(e.esc_contrato AS CHAR))) AND c.cpe_documento = e.esc_nit GROUP BY cpe_clave_int) and pre_clave_int in(select pre_clave_int from usuario_presupuesto where  usu_clave_int = '".$idUsuario."')");
	$numero = mysql_num_rows($consulta);
	if($numero>0){ $cla1 = "label-danger";}else {$cla1="label-default";}
	$text1  = "Hay ".$numero." Facturas sin relacionar";
	$consulta2 = mysql_query("select count(cpe_clave_int) as cant,pre_clave_int as pre from control_egreso where cpe_clave_int not in(SELECT c.cpe_clave_int FROM control_egreso c JOIN estados_contrato e ON UPPER(c.cpe_beneficiario) = UPPER(
			CONCAT_WS('-',e.esc_contratista,CAST(e.esc_contrato AS CHAR))) AND c.cpe_documento = e.esc_nit GROUP BY cpe_clave_int)  AND pre_clave_int in(select pre_clave_int from usuario_presupuesto where  usu_clave_int = '".$idUsuario."') group by pre");
	$numero2 = mysql_num_rows($consulta2);
	$li = "";
	if($numero2>0)
	{
		for($l=0;$l<$numero2;$l++)
			{
				$dat2 = mysql_fetch_array($consulta2);
				$pre = $dat2['pre'];
				$cant = $dat2['cant'];
		//DATOS PRESUPUESTO
				$conp = mysql_query("SELECT	p.pre_nombre nomp,c2.usu_email AS Dest,c2.usu_nombre as Cor,CONCAT(c.per_nombre,' ',c.per_apellido) as Cli,  ci.ciu_nombre as Ciu,p.pre_cliente as Cli2,p.pre_coordinador as Idc FROM	presupuesto p LEFT OUTER JOIN persona c ON c.per_clave_int = p.per_clave_int LEFT OUTER JOIN usuario c2 ON c2.usu_clave_int = p.pre_coordinador JOIN ciudad ci ON ci.ciu_clave_int = p.ciu_clave_int WHERE pre_clave_int = '".$pre."'");
				$datp = mysql_fetch_array($conp);
				$nomp = $datp['nomp'];
				$dest = $datp['Dest'];
				$cor = $datp['Cor'];
				$cli = $datp['Cli'];
				$cli2 = $datp['Cli2']; if($cli=="" || $cli==NULL || $cli=="null" || $cli==0){$cli=$cli2;}
				$ciudad = $datp['Ciu'];
				$idc = $datp['Idc'];
				$percent = ($cant*100)/$numero;
				if($percent<=25){ $pc = "green";}else if($percent<=50){$pc="aqua";}else 
				if($percent<=75){$pc="yellow";}else if($percent<=100){$pc = "red";}
				$convp = mysql_query("select pre_clave_int from usuario_presupuesto where pre_clave_int = '".$pre."' and usu_clave_int = '".$idUsuario."'");
						$numvp = mysql_num_rows($convp);
						if($numvp>0)
						{		
						//progress-bar-green red yellow( 1 % 5 green, 26 a 50 aqua, 51 as 75 yellow, 76 as 100 red)
				$li .="<li>";
				$li .="<a href='#'>";
				$li .="<h3>".$nomp;
				$li .="<small class='pull-right'>".$cant."</small>";
				$li .="</h3>";
				$li .="<div class='progress xs'>";
				$li .="<div class='progress-bar progress-bar-".$pc."' style='width: ".$percent."%' role='progressbar' aria-valuenow='".$percent."' aria-valuemin='0' aria-valuemax='100>'>";
				$li .="<span class='sr-only'>20% Complete</span>";
				$li .="</div>";
				$li .="</div>";
				$li .="</a>";
				$li .="</li>"; 
						}
			}
		}
		$datos[] = array("facturas"=>$numero,"textfactura"=>$text1,"clase"=>$cla1,"lista"=>$li);
		echo json_encode($datos);
  	}
	else if($opcion=="ASIGNARCAPITULOEVENTO")
	{
	   $pre = $_POST['pre'];
	   $cap = $_POST['cap'];
	   $con = mysql_query("select cap_nombre from capitulos where cap_clave_int  = '".$cap."' limit 1");
	   $dat = mysql_fetch_array($con);
	   $des = $dat['cap_nombre'];
	   $veri = mysql_query("select * from eventoscambio where pre_clave_int = '".$pre."' and UPPER(pev_descripcion) = UPPER('".$des."')");
	   $num = mysql_num_rows($veri);
	   if($num>0)
	   {
		   $res = "error1";
	   }
	   else
	   {
		   $ins = mysql_query("INSERT INTO eventoscambio(pre_clave_int,pev_descripcion,pev_usu_actualiz,pev_fec_actualiz) VALUES('".$pre."','".$des."','".$usuario."','".$fecha."')");
		   if($ins>0)
		   {
			   
				//RECALCULAR CODIGO DE CAPITULO ASIGNADO
				$recalculo = mysql_query("SELECT d.pgc_clave_int idd,g.gru_nombre, d.cap_clave_int,c.cap_nombre,g.gru_orden FROM 	pre_gru_capitulo d JOIN grupos g ON g.gru_clave_int = d.gru_clave_int JOIN capitulos c ON c.cap_clave_int = d.cap_clave_int
				where d.pre_clave_int = '".$pre."' order by g.gru_orden ASC, d.pgc_clave_int ASC");
				$numc = mysql_num_rows($recalculo);
				if($numc>0)
				{
					for($c=1;$c<=$numc;$c++)
					{
						$datr = mysql_fetch_array($recalculo);
						$idr = $datr['idd'];
						$updr = mysql_query("update pre_gru_capitulo set pgc_codigo = '".$c."' where pgc_clave_int = '".$idr."'");
					}
				}
				$recalculo2 = mysql_query("select pev_clave_int from eventoscambio where pre_clave_int  = '".$pre."'");
				$nume = mysql_num_rows($recalculo2);
				if($nume>0)
				{
					$f =$numc + 1;
					for($k=0;$k<$nume;$k++)
					{
					   $datr2 = mysql_fetch_array($recalculo2);
					   $pev = $datr2['pev_clave_int'];
					   $updr2 = mysql_query("update eventoscambio set pev_codigo = '".$f."' where pev_clave_int = '".$pev."'");
					   $f++;
					}
				}
			   $res = "ok";
		   }
		   else
		   {
			   $res = "error2";
		   }
	   }
	   $datos[]  = array("res"=>$res);
	   echo json_encode($datos);
	   
	}
	else if($opcion=="GUARDARCAPITULOEVENTO")
	{
	   $pre = $_POST['pre'];
	   $des = $_POST['nue'];
	  
	   $veri = mysql_query("select * from eventoscambio where pre_clave_int = '".$pre."' and UPPER(pev_descripcion) = UPPER('".$des."')");
	   $num = mysql_num_rows($veri);
	   if($num>0)
	   {
		   $res = "error1";
	   }
	   else
	   {
		   $ins = mysql_query("INSERT INTO eventoscambio(pre_clave_int,pev_descripcion,pev_usu_actualiz,pev_fec_actualiz) VALUES('".$pre."','".$des."','".$usuario."','".$fecha."')");
		   if($ins>0)
		   {
			   $recalculo = mysql_query("SELECT d.pgc_clave_int idd,g.gru_nombre, d.cap_clave_int,c.cap_nombre,g.gru_orden FROM 	pre_gru_capitulo d JOIN grupos g ON g.gru_clave_int = d.gru_clave_int JOIN capitulos c ON c.cap_clave_int = d.cap_clave_int
				where d.pre_clave_int = '".$pre."' order by g.gru_orden ASC, d.pgc_clave_int ASC");
				$numc = mysql_num_rows($recalculo);
				if($numc>0)
				{
					for($c=1;$c<=$numc;$c++)
					{
						$datr = mysql_fetch_array($recalculo);
						$idr = $datr['idd'];
						$updr = mysql_query("update pre_gru_capitulo set pgc_codigo = '".$c."' where pgc_clave_int = '".$idr."'");
					}
				}
				$recalculo2 = mysql_query("select pev_clave_int from eventoscambio where pre_clave_int  = '".$pre."'");
				$nume = mysql_num_rows($recalculo2);
				if($nume>0)
				{
					$f =$numc + 1;
					for($k=0;$k<$nume;$k++)
					{
					   $datr2 = mysql_fetch_array($recalculo2);
					   $pev = $datr2['pev_clave_int'];
					   $updr2 = mysql_query("update eventoscambio set pev_codigo = '".$f."' where pev_clave_int = '".$pev."'");
					$f++;
					}
				}
			   $res = "ok";
		   }
		   else
		   {
			   $res = "error2";
		   }
	   }
	   $datos[]  = array("res"=>$res);
	   echo json_encode($datos);
	   
	}
	else if($opcion=="ACTUALIZARCAPEXTRA")
	{
		$apro = $_POST['apro'];
		$fec = $_POST['fec'];
		$id = $_POST['id'];
		$update = mysql_query("update eventoscambio set pev_aprobo = '".$apro."',pev_fec_aprobo = '".$fec."',pev_fec_actualiz = '".$fecha."', pev_usu_actualiz = '".$usuario."' where pev_clave_int = '".$id."'");
		if($update>0)
		{
		   $res = "ok";
		}
		else
		{
		    $res = "error";
		}
		$datos[] = array("res"=>$res);
		echo json_encode($datos);
		
	}
	else if($opcion=="DELETEASIGNAREVENTO")
	{
		$id = $_POST['id'];
		$pre = $_POST['pre'];
		$dele = mysql_query("DELETE FROM eventoscambio WHERE pev_clave_int = '".$id."'");
		if($dele>0)
	    {
			$del1 = mysql_query("DELETE FROM pre_cap_actividad WHERE pev_clave_int = '".$id."'");
			$del2 = mysql_query("DELETE FROM pre_cap_act_insumo WHERE pev_clave_int = '".$id."'");
			$del3 = mysql_query("DELETE FROM pre_cap_act_sub_insumo WHERE pev_clave_int  = '".$id."'");
			$recalculo = mysql_query("SELECT d.pgc_clave_int idd,g.gru_nombre, d.cap_clave_int,c.cap_nombre,g.gru_orden FROM 	pre_gru_capitulo d JOIN grupos g ON g.gru_clave_int = d.gru_clave_int JOIN capitulos c ON c.cap_clave_int = d.cap_clave_int
				where d.pre_clave_int = '".$pre."' order by g.gru_orden ASC, d.pgc_clave_int ASC");
				$numc = mysql_num_rows($recalculo);
				if($numc>0)
				{
					for($c=1;$c<=$numc;$c++)
					{
						$datr = mysql_fetch_array($recalculo);
						$idr = $datr['idd'];
						$updr = mysql_query("update pre_gru_capitulo set pgc_codigo = '".$c."' where pgc_clave_int = '".$idr."'");
					}
				}
				$recalculo2 = mysql_query("select pev_clave_int from eventoscambio where pre_clave_int  = '".$pre."'");
				$nume = mysql_num_rows($recalculo2);
				if($nume>0)
				{
					$f =$numc + 1;
					for($k=0;$k<$nume;$k++)
					{
					   $datr2 = mysql_fetch_array($recalculo2);
					   $pev = $datr2['pev_clave_int'];
					   $updr2 = mysql_query("update eventoscambio set pev_codigo = '".$f."' where pev_clave_int = '".$pev."'");
					$f++;
					}
				}
			$res = "ok";
		}
		else
		{
			$res = "error";
	    }
		$datos[] = array("res"=>$res);
		echo json_encode($datos);
	}
	else if($opcion=="DELETEACTEVENTO")
	{
	   $id = $_POST['id'];
	   $idd = $_POST['idd'];
	   $pre = $_POST['pre'];
	   $cona = mysql_query("SELECT act_clave_int FROM pre_cap_actividad WHERE pce_clave_int ='".$idd."' LIMIT 1");
	   $data = mysql_fetch_array($cona);
	   $act = $data['act_clave_int'];
	   
	   $delete = mysql_query("DELETE FROM pre_cap_actividad WHERE pce_clave_int = '".$idd."'");
	   if($delete>0)
	   {
		   $del1 = mysql_query("DELETE FROM pre_cap_act_insumo WHERE pre_clave_int = '".$pre."' and act_clave_int = '".$act."' and pev_clave_int = '".$id."'");
		   $del2 = mysql_query("DELETE FROM pre_cap_act_sub_insumo WHERE pre_clave_int  = '".$pre."' and act_clave_int  ='".$act."' and pev_clave_int = '".$id."'");
		   $res = "ok";
	   }
	   else
	   {
		   $res = "error";
	   }
	   $datos[] = array("res"=>$res);
	   echo json_encode($datos);
	}
	else if($opcion=="ACTUALIZARACTEXTRA")
	{
	   $id = $_POST['id'];
	   $idd = $_POST['idd'];
	   $pre = $_POST['pre'];
	   $canti = $_POST['canti']; if($canti=="" || $canti==NULL){$canti=0;}
	   $cantp = $_POST['cantp']; if($cantp=="" || $cantp==NULL){$cantp=0;}
	   $cante = $_POST['cante']; if($cante=="" || $cante==NULL){$cante=0;}
	   $cona = mysql_query("SELECT act_clave_int FROM pre_cap_actividad WHERE pce_clave_int ='".$idd."' LIMIT 1");
	   $data = mysql_fetch_array($cona);
	   $act = $data['act_clave_int'];
	   
		$conc = mysql_query("select pev_codigo,pre_apli_iva apli from eventoscambio d join presupuesto p on p.pre_clave_int = d.pre_clave_int where p.pre_clave_int = '".$pre."'  and pev_clave_int = '".$id."'");
		$datc = mysql_fetch_array($conc);
		$codc = $datc['pev_codigo'];
		$apli = $datc['apli'];
	   
	   $update = mysql_query("UPDATE pre_cap_actividad SET pce_cantidad = '".$canti."',pce_cant_proyectada = '".$cantp."',pce_cant_ejecutada = '".$cante."', pce_usu_actualiz = '".$usuario."',pce_fec_actualiz = '".$fecha."' WHERE pce_clave_int = '".$idd."'");
	   if($update>0)
	   {
	      $upd1 = mysql_query("UPDATE pre_cap_act_insumo SET pgi_cant_ini= '".$canti."',pgi_cant_act = '".$cantp."' WHERE pre_clave_int = '".$pre."' and pev_clave_int = '".$id."' and act_clave_int =  '".$act."'");
		  $upd2 = mysql_query("UPDATE pre_cap_act_sub_insumo SET pgi_cant_ini= '".$canti."',pgi_cant_act = '".$cantp."' WHERE pre_clave_int = '".$pre."' and pev_clave_int = '".$id."' and act_clave_int =  '".$act."'");
		  $res  = "ok";
	   }
	   else
	   {
		  $res = "error";  
	   }
	   //
	  
	if($apli==0)
	 {  
		 //SUBANALISIS
		$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
		" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
	
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totals = $totals + ($totads+$totims+$totuts+$totivs);  
		   
		//consulta del total de la suma de las actividades asignadas a este capitulo
		$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
		",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
		" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
		$datsu = mysql_fetch_array($consu);
		 $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
		$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
		
		$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
		",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
		" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totals = $totals + ($totads+$totims+$totuts+$totivs);  
		
		$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
		",sum((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
		" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
		$datsu = mysql_fetch_array($consu);
		 $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
		$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
	 }
	 else
	 {						   
		 //SUBANALISIS
		$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini) tot".			
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100) totad".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100) totim".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100) totut".
		",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_adm_ini)/100)+".
		"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_imp_ini)/100)+".
		"(((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
		" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
	
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totals = $totals + ($totads+$totims+$totuts+$totivs);  
		   
		//consulta del total de la suma de las actividades asignadas a este capitulo
		$consu = mysql_query("select sum(pa.pgi_rend_ini*pa.pgi_vr_ini) tot".			
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100) totad".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100) totim".
		",sum(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100) totut".
		",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_adm_ini)/100)+".
		"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_imp_ini)/100)+".
		"(((pa.pgi_rend_ini*pa.pgi_vr_ini)*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
		" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
		$datsu = mysql_fetch_array($consu);
		 $totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
		$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
		
		$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act) tot".			
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100) totad".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100) totim".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100) totut".
		",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_adm_act)/100)+".
		"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_imp_act)/100)+".
		"(((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
		" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
		$datsu = mysql_fetch_array($consu);
		$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
		$totals = $totals + ($totads+$totims+$totuts+$totivs);  
		
		$consu = mysql_query("select sum(pa.pgi_rend_act*pa.pgi_vr_act) tot".			
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100) totad".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100) totim".
		",sum(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100) totut".
		",sum(((((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_adm_act)/100)+".
		"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_imp_act)/100)+".
		"(((pa.pgi_rend_act*pa.pgi_vr_act)*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
		" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$id."' and a.act_clave_int = '".$act."'");
		$datsu = mysql_fetch_array($consu);
		 $totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
		if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
		$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);					 
	 }		
		$preact = $apua*$cantp;//PRESUPUESTO ACTUAL				
		$cantf = $cantp - $cante;//CANTIDAD FALTANTE			      
		$total = $apu * $canti;//PRESUPUESTO INICIAL
		$alcance  = $total-$preact;	//CAMBIO EN EL ALCANCE	
		$datos[] = array("res"=>$res,"preact"=>$preact,"cantf"=>$cantf,"preini"=>$total,"alcanca"=>$alcance,"apua"=>$apua);//consultar datos de linea actualiza
	   echo json_encode($datos);
	}
	else if($opcion=="CALCULARTOTALES")
	{
	   $pre = $_POST['pre'];
	   $cap = $_POST['id'];
	   $con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
		$dat = mysql_fetch_array($con);   
		$adm = $dat['pre_administracion'];
		$iva = $dat['pre_iva'];
		$imp = $dat['pre_imprevisto'];
		$uti = $dat['pre_utilidades'];
		$apli = $dat['apli'];
		//totalpresupuestoenlazado
		$cone = mysql_query("select sum(enl_can_act*enl_val_act) as tote,sum(enl_can_ini*enl_val_ini) as toti FROM pre_enlazado WHERE pre_clave_int = '".$pre."'");
		$date = mysql_fetch_array($cone); 
		if($date['tote']=="" || $date['tote']==NULL){$tote = 0; } else { $tote = $date['tote'];}
		if($date['toti']=="" || $date['toti']==NULL){$toti = 0; } else { $toti = $date['toti'];}
		
		$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."'");		
	     $datsum = mysql_fetch_array($consu);
	    if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}	
		$totalc = $totalc + $toti;
			
		$totadm = ($totalc * $adm)/100;
		$totimp = ($totalc * $imp)/100;
		$totuti = ($totalc * $uti)/100;
		if($apli==0){ $totivai = ($totuti * $iva)/100;}else{ $totivai = (($totadm+$totimp+$totuti) * $iva)/100; }
		$totpre = $totalc + $totadm + $totimp + $totuti + $totivai;
		
		
		$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$pre."' limit 1");
		$dat = mysql_fetch_array($con);   
		$adma = $dat['pri_administracion'];
		$ivaa = $dat['pri_iva'];
		$impa = $dat['pri_imprevisto'];
		$utia = $dat['pri_utilidades'];
		
		$consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$pre."'");		
	    $datsum = mysql_fetch_array($consu);
	    if($datsum['totc']=="" || $datsum['totc']==NULL){$totalca=0;}else{$totalca=$datsum['totc'];}
		$totalca = $totalca + $tote;
		
		$totadma = ($totalca * $adma)/100;
		$totimpa = ($totalca * $impa)/100;
		$totutia = ($totalca * $utia)/100;
		if($apli==0){ $totivaa = ($totutia * $ivaa)/100; }else{ $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
		$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa ;
		 
			if($apli==0)
			{  
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);
			}
			else
			{						   
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apu = 0;}else {$apu  = $datsu['tot'];}
			$apu = $apu + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'  and pa.pev_clave_int= '".$cap."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".

			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."' and pa.pev_clave_int= '".$cap."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$apua = 0;}else {$apua  = $datsu['tot'];}
			$apua = $apua + ($totada+$totima+$totuta+$totiva) + ($totals);					 
			}	
			$alcancec= $apu - $apua;
			
			if($apli==0)
			{  
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
			$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
			$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);
			}
			else
			{						   
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
			$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".

			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$pre."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
			$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);					 
			}	
			$alcanceevento= $totale - $totalea;
			$totalobra = $totpre + $totale;
			$totalobraa = $totprea + $totalea;
			$alcanceobra = $totalobra - $totalobraa;
			$datos[] = array("divcapi"=>$apu,"divcapa"=>$apua,"divcapal"=>$alcancec,"totaleventoi"=>$totale,"totaleventoa"=>$totalea,"totaleventoal"=>$alcanceevento,"totalobraei"=>$totalobra,"totalobraea"=>$totalobraa,"totalobraeal"=>$alcanceobra);
			echo json_encode($datos);	
	}
	else if($opcion=="ACTUALIZARRECURSO")
	{
	   $pre = $_POST['pre'];
	   $cap = $_POST['cap'];
	   $act = $_POST['act'];
	   $ins = $_POST['ins'];
	   $ren = $_POST['rend']; if($ren=="" || $ren==NULL){$ren=0;}
	   $val = $_POST['val'];
	    $con  = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
		$dat = mysql_fetch_array($con);   
		$apli = $dat['apli'];
	   
	   $coni = mysql_query("select t.tpi_tipologia as tpl from insumos i join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) where i.ins_clave_int = '".$ins."' limit 1");
	   $dati = mysql_fetch_array($coni);
	   $tpl = $dati['tpl'];	  
	   $cona = mysql_query("select pce_clave_int from pre_cap_actividad where pre_clave_int = '".$pre."' and pev_clave_int ='".$cap."' and act_clave_int = '".$act."' limit 1");
	   $data = mysql_fetch_array($cona);
	   $idda = $data['pce_clave_int'];
	   $update = mysql_query("UPDATE pre_cap_act_insumo SET pgi_rend_act = '".$ren."', pgi_vr_act = '".$val."' WHERE pre_clave_int = '".$pre."' and pev_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int ='".$ins."'");
	   if($update>0)
	   {
		   $res = "ok";
	   }
	   else
	   {
		  $res = "error";  
	   }
		$con1 = mysql_query("select pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_cap_act_insumo where pre_clave_int = '".$pre."'  and pev_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
		$dato1 = mysql_fetch_array($con1);
		$ren = $dato1['pgi_rend_ini']; if($ren=="null" || $ren==NULL){$ren=0;}
		$valor = $dato1['pgi_vr_ini']; if($valor=="null" || $valor==NULL){$valor=0;}
		$admini = $dato1['pgi_adm_ini'];
		$impini = $dato1['pgi_imp_ini'];
		$utiini = $dato1['pgi_uti_ini'];			
		$ivaini = $dato1['pgi_iva_ini'];		
		$totalcini = $ren*$valor;
		$tadmini = ($totalcini*$admini)/100;
		$timpini = ($totalcini*$impini)/100;
		$tutiini = ($totalcini*$utiini)/100;
		if($apli==0) {$tivaini = ($tutiini*$ivaini)/100;}
		else {$tivaini = (($tadmini+$timpini+$tutiini)*$ivaini)/100;}
		$totalcini = $totalcini +($tadmini+$timpini+$tutiini+$tivaini);
		 
		$renact = $dato1['pgi_rend_act'];
		$valact = $dato1['pgi_vr_act'];
		$admact = $dato1['pgi_adm_act'];
		$impact = $dato1['pgi_imp_act'];
		$utiact = $dato1['pgi_uti_act'];			
		$ivaact = $dato1['pgi_iva_act'];				  
		
		$material=0; $equipo = 0; $mano = 0;
		$mate = 0; $equi = 0; $man = 0;				  
		
		$totalc = $renact*$valact;
		$tadmact = ($totalc*$admact)/100;
		$timpact = ($totalc*$impact)/100;
		$tutiact = ($totalc*$utiact)/100;
		if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
		{ $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}		
		$totalc = $totalc +($tadmact+$timpact+$tutiact+$tivaact);
		$totalact = ($totalcini)-($totalc);
		if($totalact == ''){ $totalact = 0; }	
	    if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
			$total = $material + $equipo + $mano;	
	   
	   $datos[] = array("res"=>$res,"idda"=>$idda,"mate"=>$mate,"equi"=>$equi,"man"=>$man,"alc"=>$totalact);
	   echo json_encode($datos);
	   
	}
    else if($opcion=="ACTUALIZARRECURSOAIUE")
    {
        $pre = $_POST['pre'];
        $cap = $_POST['cap'];
        $act = $_POST['act'];
        $ins = $_POST['ins'];
        $adm = $_POST['adm']; if($adm=="" || $adm==NULL){$adm=0;}
        $imp = $_POST['imp']; if($imp=="" || $imp==NULL){$imp=0;}
        $uti = $_POST['uti']; if($uti=="" || $uti==NULL){$uti=0;}
        $iva = $_POST['iva']; if($iva=="" || $iva==NULL){$iva=0;}
        //$val = $_POST['val'];
        $con  = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
        $dat = mysql_fetch_array($con);
        $apli = $dat['apli'];

        $coni = mysql_query("select t.tpi_tipologia as tpl from insumos i join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int) where i.ins_clave_int = '".$ins."' limit 1");
        $dati = mysql_fetch_array($coni);
        $tpl = $dati['tpl'];
        $cona = mysql_query("select pce_clave_int from pre_cap_actividad where pre_clave_int = '".$pre."' and pev_clave_int ='".$cap."' and act_clave_int = '".$act."' limit 1");
        $data = mysql_fetch_array($cona);
        $idda = $data['pce_clave_int'];
        $update = mysql_query("UPDATE pre_cap_act_insumo SET pgi_adm_act = '".$adm."', pgi_imp_act = '".$imp."', pgi_uti_act = '".$uti."', pgi_iva_act = '".$iva."' WHERE pre_clave_int = '".$pre."' and pev_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int ='".$ins."'");
        if($update>0)
        {
            $res = "ok";
        }
        else
        {
            $res = "error";
        }
        $con1 = mysql_query("select pgi_rend_ini,pgi_vr_ini,pgi_rend_act,pgi_vr_act,pgi_adm_ini,pgi_imp_ini,pgi_uti_ini,pgi_iva_ini,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_cap_act_insumo where pre_clave_int = '".$pre."'  and pev_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$ins."'");
        $dato1 = mysql_fetch_array($con1);
        $ren = $dato1['pgi_rend_ini']; if($ren=="null" || $ren==NULL){$ren=0;}
        $valor = $dato1['pgi_vr_ini']; if($valor=="null" || $valor==NULL){$valor=0;}
        $admini = $dato1['pgi_adm_ini'];
        $impini = $dato1['pgi_imp_ini'];
        $utiini = $dato1['pgi_uti_ini'];
        $ivaini = $dato1['pgi_iva_ini'];

        $tadmini = ($valor*$admini)/100;
        $timpini = ($valor*$impini)/100;
        $tutiini = ($valor*$utiini)/100;
        if($apli==0) {$tivaini = ($tutiini*$ivaini)/100;}
        else {$tivaini = (($tadmini+$timpini+$tutiini)*$ivaini)/100;}
        $totalcini = $valor +($tadmini+$timpini+$tutiini+$tivaini);
        $totalcini = $ren*$totalcini;

        $renact = $dato1['pgi_rend_act'];
        $valact = $dato1['pgi_vr_act'];
        $admact = $dato1['pgi_adm_act'];
        $impact = $dato1['pgi_imp_act'];
        $utiact = $dato1['pgi_uti_act'];
        $ivaact = $dato1['pgi_iva_act'];

        $material=0; $equipo = 0; $mano = 0;
        $mate = 0; $equi = 0; $man = 0;


        $tadmact = ($valact*$admact)/100;
        $timpact = ($valact*$impact)/100;
        $tutiact = ($valact*$utiact)/100;
        if($apli==0){$tivaact = ($tutiact*$ivaact)/100;}else
        { $tivaact = (($tadmact+$timpact+$tutiact)*$ivaact)/100;}
        $totalc = $valact +($tadmact+$timpact+$tutiact+$tivaact);
        $incpe = (($totalc - $valact)/$valact)*100;

        $totalc = $renact*$totalc;

        $totalact = ($totalcini)-($totalc);
        if($totalact == ''){ $totalact = 0; }
        if($tpl==1){$material=$totalcini; $mate = $totalc;}else if($tpl==2){$equipo = $totalcini; $equi = $totalc;}else if($tpl==3){$mano = $totalcini; $man = $totalc;}
        $total = $material + $equipo + $mano;



        $datos[] = array("res"=>$res,"idda"=>$idda,"mate"=>$mate,"equi"=>$equi,"man"=>$man,"alc"=>$totalact,"incpe"=>number_format($incpe,2,'.',',')."%");
        echo json_encode($datos);

    }
	else if($opcion=="GUARDARSUBACTUAL")
	{
		$pre = $_POST['pre'];
		$act = $_POST['act'];
		$sub = $_POST['sub'];
		$ins = $_POST['ins'];
		$cantidad = $_POST['cantidad'];
		$valor = $_POST['valor'];
		$conp = mysql_query("select est_clave_int,pre_apli_iva from presupuesto where pre_clave_int = '".$pre."'");
		$datp = mysql_fetch_array($conp);
		$apli = $datp['pre_apli_iva'];
		$update  = mysql_query("UPDATE pre_gru_cap_act_sub_insumo SET pgi_rend_act = '".$cantidad."',pgi_vr_act = '".$valor."', pgi_usu_actualiz = '".$usuario."',pgi_fec_actualiz = '".$fecha."' WHERE pre_clave_int = '".$pre."' and act_clave_int = '".$act."' and act_subanalisis  = '".$sub."' and ins_clave_int = '".$ins."'");
		if($update>0)
	    {			
			$res = "ok";
	    }
		else
		{
			$res = "error";
		}
		 
		$coni = mysql_query("select t.tpi_tipologia as tpl from  insumos i join tipoinsumos t on (t.tpi_clave_int = i.tpi_clave_int)  where i.ins_clave_int = '".$ins."'");
		$dati = mysql_fetch_array($coni);
		$tpl = $dati['tpl'];
		
		$con1 = mysql_query("select IFNULL(pgi_rend_act,0) AS ri,IFNULL(pgi_vr_act,0) AS vri,pgi_adm_act,pgi_imp_act,pgi_uti_act,pgi_iva_act from pre_gru_cap_act_sub_insumo where pre_clave_int = '".$pre."' and act_clave_int ='".$act."' and act_subanalisis = '".$sub."' and ins_clave_int = '".$ins."' limit 1");
		$dato1 = mysql_fetch_array($con1);
		$ren = $dato1['ri']; 
		$valor = $dato1['vri']; 	
		$admini = $dato1['pgi_adm_act'];
		$impini = $dato1['pgi_imp_act'];
		$utiini = $dato1['pgi_uti_act'];			
		$ivaini = $dato1['pgi_iva_act'];		
		$totalcini = $ren*$valor;
		$tadmini = ($totalcini*$admini)/100;
		$timpini = ($totalcini*$impini)/100;
		$tutiini = ($totalcini*$utiini)/100;
		if($apli==0){ $tivaini = ($tutiini*$ivaini)/100; } else { $tivaini = (($tutiini + $tadmini + $timpini )*$ivaini)/100; }
		$totalcini = $totalcini +($tadmini+$timpini+$tutiini+$tivaini);		
		$material=0; $equipo = 0; $mano = 0;
		$apu = $apu + $totalcini;
		if($tpl==1){$material=$totalcini; }else if($tpl==2){$equipo = $totalcini;}else if($tpl==3){$mano = $totalcini;}
		
		$datos[] = array("res"=>$res,"material"=>$material,"equipo"=>$equipo,"mano"=>$mano);
		echo json_encode($datos);	
	}
	else if($opcion=="DELETERECURSOACTAPU")
	{
		 $pre = $_POST['pre'];
		 $gru = $_POST['gru'];
		 $cap = $_POST['cap'];
		 $act = $_POST['act'];
		 $idd = $_POST['idd'];
		 $upd = mysql_query("update pre_gru_cap_act_insumo set pgi_rend_act = 0 where pgi_clave_int = '".$idd."'");
		 if($upd>0)
		 {
			 $res = "ok";
			 $coni = mysql_query("select pgca_clave_int from pre_gru_cap_actividad where  pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = 	'".$act."' limit 1");
			 $dati = mysql_fetch_array($coni);			  
			 $idda = $dati['pgca_clave_int'];			
		 }
		 else
		 {
			 $res = "error";
		 }
		 $datos[] = array("res"=>$res,"idda"=>$idda);
		 echo json_encode($datos);
	}
	else if($opcion=="ENLAZAR")
	{
	    $preenlazado = $_POST['preenlazado'];
		$presupuesto = $_POST['presupuesto'];
		$grupo = $_POST['gruenlazar'];
		$capitulo = $_POST['capenlazar'];
		$con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva apli from presupuesto where pre_clave_int = '".$preenlazado."' limit 1");
		$dat = mysql_fetch_array($con);   
		$adm = $dat['pre_administracion'];
		$iva = $dat['pre_iva'];
		$imp = $dat['pre_imprevisto'];
		$uti = $dat['pre_utilidades'];
		$apli = $dat['apli'];
        //TOTAL PRESUPUESTADO
		$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$preenlazado."'");		
	  $datsum = mysql_fetch_array($consu);
	  if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
		$totadm = ($totalc * $adm)/100;
		$totimp = ($totalc * $imp)/100;
		$totuti = ($totalc * $uti)/100;
		if($apli==0){ $totivai = ($totuti * $iva)/100;}else{ $totivai = (($totadm+$totimp+$totuti) * $iva)/100; }
		$totpre = $totalc + $totadm + $totimp + $totuti + $totivai;
		
		//TOTAL ACTUAL
		$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$preenlazado."' limit 1");
		$dat = mysql_fetch_array($con);   
		$adma = $dat['pri_administracion'];
		$ivaa = $dat['pri_iva'];
		$impa = $dat['pri_imprevisto'];
		$utia = $dat['pri_utilidades'];
		
		$consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$preenlazado."'");		
	  $datsum = mysql_fetch_array($consu);
	  if($datsum['totc']=="" || $datsum['totc']==NULL){$totalca=0;}else{$totalca=$datsum['totc'];}
		$totadma = ($totalca * $adma)/100;
		$totimpa = ($totalca * $impa)/100;
		$totutia = ($totalca * $utia)/100;
		if($apli==0){ $totivaa = ($totutia * $ivaa)/100; }else{ $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
		$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;
		
		if($apli==0)
			{  
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
			$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
			$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);
			}
			else
			{						   
			//SUBANALISIS
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			//consulta del total de la suma de las actividades asignadas a este capitulo
			$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
			",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
			",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
			"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			$datsu = mysql_fetch_array($consu);
			$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
			$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			$datsu = mysql_fetch_array($consu);
			$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
			$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
			$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
			",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
			",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
			"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
			" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			$datsu = mysql_fetch_array($consu);
			$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
			if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
			$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);					 
			}	
			$totalobra = $totpre + $totale;
			$totalobraa = $totprea + $totalea;
			//VERIFICAR SI YA ESTA ASIGNADO EL PRESUPUESTO SELECCIONADO
			$veri = mysql_query("SELECT * FROM pre_enlazado WHERE pre_clave_int = '".$presupuesto."' and gru_clave_int = '".$grupo."' and cap_clave_int = '".$capitulo."' and pre_enlazado = '".$preenlazado."'");
			$numv = mysql_num_rows($veri);
			if($numv>0)
			{
			   	$cone = mysql_query("UPDATE pre_enlazado SET enl_val_ini = '".$totalobra."',enl_val_act = '".$totalobraa."' WHERE pre_clave_int = '".$presupuesto."' and gru_clave_int = '".$grupo."' and cap_clave_int = '".$capitulo."' and pre_enlazado = '".$preenlazado."'");
			}
			else
			{
				$cone = mysql_query("INSERT INTO pre_enlazado(pre_clave_int,gru_clave_int,cap_clave_int,pre_enlazado,enl_can_ini,enl_can_act,enl_ejecutada,enl_val_ini,enl_val_act) VALUES('".$presupuesto."','".$grupo."','".$capitulo."','".$preenlazado."','1','1','0','".$totalobra."','".$totalobraa."')");
				//CONFIRMAR CON LINEA GLOBAL SI EL TOTAL INCLUYEN LOS EVENTOS DE CAMBIO
			
			}
			if($cone>0)
			{
			   $res = "ok";
			}
			else
			{
			   $res = "error";
			}
			$datos[] = array("res"=>$res);
			echo json_encode($datos);
		
		//CALCULO DEL VALOR TOTAL DEL PRESUPUESTO SELECCIONADO
    }
	else if($opcion=="ACTUALIZARPRESUPUESTOSENLAZADOS")
	{
		
	    $preenlazado = $_POST['preenlazado'];//solo se envia el presupuesto modificado y se actualizar en todos donde este enlazado		
		$veri = mysql_query("select * from pre_enlazado where pre_enlazado = '".$preenlazado."'");
		$numv = mysql_num_rows($veri);
		if($numv>0)
		{
		
			$con  = mysql_query("select pre_administracion,pre_imprevisto,pre_utilidades,pre_iva,pre_apli_iva apli from presupuesto where pre_clave_int = '".$preenlazado."' limit 1");
			$dat = mysql_fetch_array($con);   
			$adm = $dat['pre_administracion'];
			$iva = $dat['pre_iva'];
			$imp = $dat['pre_imprevisto'];
			$uti = $dat['pre_utilidades'];
			$apli = $dat['apli'];
			
			$consu = mysql_query("SELECT sum(pgca_valor_act) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$preenlazado."'");		
	  $datsum = mysql_fetch_array($consu);
	  if($datsum['totc']=="" || $datsum['totc']==NULL){$totalc=0;}else{$totalc=$datsum['totc'];}
			$totadm = ($totalc * $adm)/100;
			$totimp = ($totalc * $imp)/100;
			$totuti = ($totalc * $uti)/100;
			if($apli==0){ $totivai = ($totuti * $iva)/100;}else{ $totivai = (($totadm+$totimp+$totuti) * $iva)/100; }
			$totpre = $totalc + $totadm + $totimp + $totuti + $totivai;		
		
			$con  = mysql_query("select pri_administracion,pri_imprevisto,pri_utilidades,pri_iva from presupuestoinicial where pre_clave_int = '".$preenlazado."' limit 1");
			$dat = mysql_fetch_array($con);   
			$adma = $dat['pri_administracion'];
			$ivaa = $dat['pri_iva'];
			$impa = $dat['pri_imprevisto'];
			$utia = $dat['pri_utilidades'];
		    $consu = mysql_query("SELECT sum(pgca_valor_acta) as totc FROM pre_gru_cap_actividad WHERE pre_clave_int ='".$preenlazado."'");		
	  $datsum = mysql_fetch_array($consu);
	  if($datsum['totc']=="" || $datsum['totc']==NULL){$totalca=0;}else{$totalca=$datsum['totc'];}
			$totadma = ($totalca * $adma)/100;
			$totimpa = ($totalca * $impa)/100;
			$totutia = ($totalca * $utia)/100;
			if($apli==0){ $totivaa = ($totutia * $ivaa)/100; }else{ $totivaa = (($totadma + $totimpa + $totutia) * $ivaa)/100; }
			$totprea = $totalca + $totadma + $totimpa + $totutia + $totivaa;
			
			if($apli==0)
				{  
					//SUBANALISIS
					$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
					",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
					",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
					",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
					",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
					" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
					
					$datsu = mysql_fetch_array($consu);
					$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
					if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
					$totals = $totals + ($totads+$totims+$totuts+$totivs);  
			
					//consulta del total de la suma de las actividades asignadas a este capitulo
					$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
					",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
					",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
					",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
					",sum(((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100)*pa.pgi_iva_ini)/100 totiv".
					" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
					$datsu = mysql_fetch_array($consu);
					$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
					if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
					$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
					
					$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
					",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
					",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
					",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
					",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
					" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
					$datsu = mysql_fetch_array($consu);
					$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
					if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
					$totals = $totals + ($totads+$totims+$totuts+$totivs);  
					
					$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
					",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
					",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
					",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
					",sum(((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100)*pa.pgi_iva_act)/100 totiv".
					" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
					$datsu = mysql_fetch_array($consu);
					$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
					if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
					$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);
				}
				else
				{						   
				//SUBANALISIS
				$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini)) tot".			
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
				",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
				"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
				"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_rend_sub_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
				" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
			
				$datsu = mysql_fetch_array($consu);
				$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
				if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
				$totals = $totals + ($totads+$totims+$totuts+$totivs);  
				
				//consulta del total de la suma de las actividades asignadas a este capitulo
				$consu = mysql_query("select sum((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini)) tot".			
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100) totad".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100) totim".
				",sum((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100) totut".
				",sum((((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_adm_ini)/100)+".
				"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_imp_ini)/100)+".
				"((((pa.pgi_rend_ini*pa.pgi_vr_ini*pgi_cant_ini))*pa.pgi_uti_ini)/100))*pa.pgi_iva_ini)/100 totiv".
				" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
				$datsu = mysql_fetch_array($consu);
				$totad = $datsu['totad']; $totim = $datsu['totim']; $totut = $datsu['totut']; $totiv = $datsu['totiv'];
				if($datsu['tot']=="" || $datsu['tot']==NULL){$totale = 0;}else {$totale  = $datsu['tot'];}
				$totale = $totale + ($totad+$totim+$totut+$totiv) + ($totals);
				
				$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act)) tot".			
				",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
				",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
				",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
				",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
				"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
				"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_rend_sub_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
				" from  pre_cap_act_sub_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
				$datsu = mysql_fetch_array($consu);
				$totads = $datsu['totad']; $totims = $datsu['totim']; $totuts = $datsu['totut']; $totivs = $datsu['totiv'];
				if($datsu['tot']=="" || $datsu['tot']==NULL){$totals = 0;}else {$totals  = $datsu['tot'];}
				$totals = $totals + ($totads+$totims+$totuts+$totivs);  
				
				$consu = mysql_query("select sum((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act)) tot".			
				",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100) totad".
				",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100) totim".
				",sum((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100) totut".
				",sum((((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_adm_act)/100)+".
				"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_imp_act)/100)+".
				"((((pa.pgi_rend_act*pa.pgi_vr_act*pgi_cant_act))*pa.pgi_uti_act)/100))*pa.pgi_iva_act)/100 totiv".
				" from  pre_cap_act_insumo pa join actividades a on a.act_clave_int  = pa.act_clave_int where pa.pre_clave_int  = '".$preenlazado."'");
				$datsu = mysql_fetch_array($consu);
				$totada = $datsu['totad']; $totima = $datsu['totim']; $totuta = $datsu['totut']; $totiva = $datsu['totiv'];
				if($datsu['tot']=="" || $datsu['tot']==NULL){$totalea = 0;}else {$totalea  = $datsu['tot'];}
				$totalea = $totalea + ($totada+$totima+$totuta+$totiva) + ($totals);					 
			}	
			$totalobra = $totpre + $totale;
			$totalobraa = $totprea + $totalea;
			
			//VERIFICAR SI YA ESTA ASIGNADO EL PRESUPUESTO SELECCIONADO
			
			$cone = mysql_query("UPDATE pre_enlazado SET enl_val_ini = '".$totalobra."',enl_val_act = '".$totalobraa."' WHERE pre_enlazado = '".$preenlazado."'");
			
			if($cone>0)
			{
			   $res = "ok";
			}
			else
			{
			   $res = "error2";
			}
		}
		else
		{
			 $res = "error1";
		}
			$datos[] = array("res"=>$res);
			echo json_encode($datos);
	
	}
	else if($opcion=="DELETEENLAZAR")
	{
	   	$ide = $_POST['ide'];
		$dele = mysql_query("DELETE FROM pre_enlazado WHERE enl_clave_int = '".$ide."'");
		if($dele>0)
		{
		   $res = 1;
		}
		else
		{
		   $res = 2;
		}
		$datos[] = array("res"=>$res);
		echo json_encode($datos);
	}
	else if($opcion=="ACTUALIZARPARTIDAS")
	{
        $pre=$_POST['pre'];
        $cap=$_POST['cap'];
        $gru = $_POST['gru'];
        $act = $_POST['act'];
        $ins = $_POST['ins'];
        $cona = mysql_query("select pre_apli_iva apli from presupuesto where pre_clave_int = '".$pre."' limit 1");
        $data = mysql_fetch_array($cona);
        $apli = $data['apli'];


        //CANTIDAD DE LA ACTIVIDAD
        $conac = mysql_query("select pgca_cantidad,pgca_cant_proyectada from pre_gru_cap_actividad where pre_clave_int = '".$pre."' and gru_clave_int ='".$gru."' and cap_clave_int ='".$cap."' and act_clave_int = '".$act."'");
        $datac = mysql_fetch_array($conac);
        $cantinicial = $datac['pgca_cantidad'];
        $cantactual = $datac['pgca_cant_proyectada'];

        if($ins>0)
         {
             $con = mysql_query("select d.pgi_clave_int idd, i.ins_clave_int idi,i.ins_valor AS val,d.pgi_rend_ini,d.pgi_vr_ini,d.pgi_adm_ini,d.pgi_imp_ini,d.pgi_uti_ini,d.pgi_iva_ini,d.pgi_rend_act,d.pgi_vr_act,d.pgi_adm_act,d.pgi_imp_act,d.pgi_uti_act,d.pgi_iva_act,d.pgi_creacion as cre from  insumos i JOIN pre_gru_cap_act_insumo d ON (d.ins_clave_int = i.ins_clave_int) where d.pre_clave_int = '" . $pre . "' AND d.gru_clave_int = '" . $gru . "' AND d.cap_clave_int = '" . $cap . "' AND d.act_clave_int = '" . $act . "' and i.ins_clave_int = '".$ins."'  order by idd ASC");
         }
         else {
             $con = mysql_query("select d.pgi_clave_int idd, i.ins_clave_int idi,i.ins_valor AS val,d.pgi_rend_ini,d.pgi_vr_ini,d.pgi_adm_ini,d.pgi_imp_ini,d.pgi_uti_ini,d.pgi_iva_ini,d.pgi_rend_act,d.pgi_vr_act,d.pgi_adm_act,d.pgi_imp_act,d.pgi_uti_act,d.pgi_iva_act,d.pgi_creacion as cre from  insumos i JOIN pre_gru_cap_act_insumo d ON (d.ins_clave_int = i.ins_clave_int)  where d.pre_clave_int = '" . $pre . "' AND d.gru_clave_int = '" . $gru . "' AND d.cap_clave_int = '" . $cap . "' AND d.act_clave_int = '" . $act . "'  order by idd ASC");
         }
        $num = mysql_num_rows($con);
        $cana = 0;
        if($num>0) {
            $res = "si";
            while ($dat = mysql_fetch_array($con)) {
                $idc = $dat['idd'];
                $insu = $dat['idi'];
                /*$val = $dat['val'];
                if ($val == "null" || $val == NULL) { $val = 0; }

                $cre = $dat['cre'];
                $ren = $dat['pgi_rend_ini'];
                if ($ren == "null" || $ren == NULL) { $ren = 0; }
                $valor = $dat['pgi_vr_ini'];
                if ($valor == "null" || $valor == NULL) { $valor = 0; }
                $admini = $dat['pgi_adm_ini'];
                $impini = $dat['pgi_imp_ini'];
                $utiini = $dat['pgi_uti_ini'];
                $ivaini = $dat['pgi_iva_ini'];
                $tadmini = ($valor * $admini) / 100;
                $timpini = ($valor * $impini) / 100;
                $tutiini = ($valor * $utiini) / 100;
                if ($apli == 0) { $tivaini = ($tutiini * $ivaini) / 100; }
                else { $tivaini = (($tadmini + $timpini + $tutiini) * $ivaini) / 100; }
                $valori = $valor + ($tadmini + $timpini + $tutiini + $tivaini);
                $totalcini = $ren * $valori;//total inicial del recurso
                $totinicial = $totalcini  * $cantinicial;

                $renact = $dat['pgi_rend_act'];
                $valact = $dat['pgi_vr_act'];
                $admact = $dat['pgi_adm_act'];
                $impact = $dat['pgi_imp_act'];
                $utiact = $dat['pgi_uti_act'];
                $ivaact = $dat['pgi_iva_act'];
                $tadmact = ($valact * $admact) / 100;
                $timpact = ($valact * $impact) / 100;
                $tutiact = ($valact * $utiact) / 100;
                if ($apli == 0) { $tivaact = ($tutiact * $ivaact) / 100; }
                else { $tivaact = (($tadmact + $timpact + $tutiact) * $ivaact) / 100; }

                $valora = $valact + ($tadmact + $timpact + $tutiact + $tivaact);
                $totalc = $renact * $valora;//total actual
                $totalactual = $totalc * $cantactual;

                $totalact = ($totalcini) - ($totalc);
                if ($totalact == '') {
                    $totalact = 0;
                }

                $incp = (($valora - $valact) / $valact) * 100;
                */
                //
                //CANTIDADES TOTALES DE CADA RECURSO Y TOTALES EN ITEM SELECCIONADO
              /*  $coni = mysql_query("SELECT IFNULL(Id,Id1) Id,IFNULL(Ins,Ins1) Ins,IFNULL(Cod,Cod1) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini * pgi_cant_ini) AS Cant,sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."'  and i.ins_clave_int = '".$ins."'
								GROUP BY Id,Ins,Val,Cod
							) T1
							LEFT OUTER JOIN
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_ini*d.pgi_rend_ini * pgi_cant_ini) AS Cant1,sum(pgi_rend_sub_ini*d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and i.ins_clave_int = '".$ins."'
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2
							ON T1.Id = T2.Id1
							UNION
							SELECT IFNULL(Id1,Id) Id,IFNULL(Ins1,Ins) Ins,IFNULL(Cod1,Cod) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini * pgi_cant_ini) AS Cant,sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and i.ins_clave_int = '".$ins."'
								GROUP BY Id,Ins,Val,Cod
							) T1
							RIGHT OUTER JOIN
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_ini*d.pgi_rend_ini * pgi_cant_ini) AS Cant1,sum(pgi_rend_sub_ini*d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and i.ins_clave_int = '".$ins."'
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2
							ON T1.Id = T2.Id1
							ORDER BY Ins");
                $dati = mysql_fetch_array($coni);
                $canti = $dati['Suma'];
                $valu  = $dati['Val'];*/
                //CANTIDAD DISPONIBLE PARA COMPROMETER
                $coni = mysql_query("SELECT IFNULL(Id,Id1) Id,IFNULL(Ins,Ins1) Ins,IFNULL(Cod,Cod1) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini * pgi_cant_ini) AS Cant,sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id,Ins,Val,Cod
							) T1
							LEFT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_ini*d.pgi_rend_ini * pgi_cant_ini) AS Cant1,sum(pgi_rend_sub_ini*d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1
							
							UNION
							SELECT IFNULL(Id1,Id) Id,IFNULL(Ins1,Ins) Ins,IFNULL(Cod1,Cod) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_ini AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_ini * pgi_cant_ini) AS Cant,sum(d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id,Ins,Val,Cod
							) T1
							RIGHT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_ini AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_ini*d.pgi_rend_ini * pgi_cant_ini) AS Cant1,sum(pgi_rend_sub_ini*d.pgi_rend_ini*d.pgi_cant_ini*d.pgi_vr_ini) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1											
							ORDER BY Ins");
                $dati = mysql_fetch_array($coni);
                //INICIALES
                $cantia = $dati['Suma'];//cantidad total recurso a comprometer
                $valua  = $dati['Val'];//
                $valortotal  = $cantia * $valua;//valor total a comprometer

                //CANTIDAD DISPONIBLE PARA COMPROMETER ACTUAL
                $coni = mysql_query("SELECT IFNULL(Id,Id1) Id,IFNULL(Ins,Ins1) Ins,IFNULL(Cod,Cod1) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_act AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_act * pgi_cant_act) AS Cant,sum(d.pgi_rend_act*d.pgi_cant_act*d.pgi_vr_act) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id,Ins,Val,Cod
							) T1
							LEFT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_act AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_act*d.pgi_rend_act * pgi_cant_act) AS Cant1,sum(pgi_rend_sub_act*d.pgi_rend_act*d.pgi_cant_act*d.pgi_vr_act) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1
							
							UNION
							SELECT IFNULL(Id1,Id) Id,IFNULL(Ins1,Ins) Ins,IFNULL(Cod1,Cod) Cod,IFNULL(Val1,Val) Val,Cant1,IFNULL(Cant,0)+IFNULL(Cant1,0) as Suma,(IFNULL(Val1,Val)*(IFNULL(Cant,0)+IFNULL(Cant1,0))) as Tot,IFNULL(tot1,0)+IFNULL(tot2,0) as Suma2
							FROM
							(
								SELECT i.ins_clave_int AS Id,i.ins_nombre AS Ins,d.pgi_vr_act AS Val,u.uni_codigo AS Cod,								                                sum(d.pgi_rend_act * pgi_cant_act) AS Cant,sum(d.pgi_rend_act*d.pgi_cant_act*d.pgi_vr_act) as tot1
								FROM insumos i
								JOIN pre_gru_cap_act_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id,Ins,Val,Cod
							) T1
							RIGHT OUTER JOIN 
							(
								SELECT i.ins_clave_int AS Id1,i.ins_nombre AS Ins1,d.pgi_vr_act AS Val1,u.uni_codigo AS Cod1,
								sum(pgi_rend_sub_act*d.pgi_rend_act * pgi_cant_act) AS Cant1,sum(pgi_rend_sub_act*d.pgi_rend_act*d.pgi_cant_act*d.pgi_vr_act) as tot2
								FROM insumos i
								JOIN pre_gru_cap_act_sub_insumo d ON d.ins_clave_int = i.ins_clave_int
								JOIN unidades u ON u.uni_clave_int = i.uni_clave_int
								WHERE d.pre_clave_int = '".$pre."' and d.gru_clave_int = '".$gru."' and d.cap_clave_int = '".$cap."' and d.act_clave_int = '".$act."' and i.ins_clave_int = '".$insu."'
								GROUP BY Id1,Ins1,Val1,Cod1
							) T2 
							ON T1.Id = T2.Id1											
							ORDER BY Ins");
                $dati = mysql_fetch_array($coni);
                //ACTUALES
                $cantiact = $dati['Suma'];//cantidad total recurso a comprometer actu
                $valuact  = $dati['Val'];//
                $valortotalact  = $cantiact * $valuact;//valor total a comprometer actual
                //NO SE PUEDE COMPROMETER MAS DE LO INICIAL AUNQUE EL ACTUAL SE MAYOR SOLO SE MODIFICA SI EL ACTUAL ES MENOR SI ES IGUAL O MAYOR SE COMPROMETE PROPORCIONAL

                ///TOTAL COMPROMETIDO
                $cont = mysql_query("select sum(pai_cant_comprometida) cant, sum(pai_val_comprometido) tot from partida_item where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$insu."'");
                $datt = mysql_fetch_array($cont);
                $totalpa = $datt['tot'];
                $cantpa = $datt['cant'];

                //CALCLAR PORCENTAJE DE INCREMENTO O DESCREMENTO
                if($totalpa<=0 and ($valortotal>0 || $valortotalact>0)){
                    $dif = 0;
                    $por = 0;
                    $resultado = "A1";
                }
                else
                if($valortotal>$valortotalact and $totalpa==$valortotal){
                    //calcular porcentaje de decrementoy restarlo en cada uno de lo
                    $dif = $valortotal - $valortotalact;
                    $por = $dif/$valortotal *100;
                    $resultado=  "D1";
                }
                else if($totalpa<$valortotalact and $valortotalact<$valortotal){
                    $dif = $valortotalact - $totalpa;
                    $por = $dif/$totalpa *100;
                    $resultado = "I1";
                }
                else if($totalpa>$valortotalact and $valortotalact<$valortotal){
                    //calcular porcentaje de decrementoy restarlo en cada uno de lo
                    $dif = $totalpa - $valortotalact;
                    $por = $dif/$totalpa *100;
                    $resultado=  "D2";
                }
                else if($valortotal>=$valortotalact and $totalpa<$valortotal)
                {
                    $dif = $valortotalact - $totalpa;
                    $por = ($dif/$totalpa) *100;
                    $resultado = "I2";
                }
                else if($valortotalact>$valortotal and $totalpa<$valortotal)
                {
                    $dif = $valortotal - $totalpa;
                    $por = ($dif/$totalpa) *100;
                    $resultado = "I2";
                }


                //VERIFICAR SI EL RECURSO ESTA COMPROMETIDO EN LAS PARTIDASD ITEM Y EN PARTIDA OBSERVACION
                $conpa = mysql_query("select par_clave_int,pai_clave_int,pai_cant_comprometida,pai_val_comprometido from partida_item where pre_clave_int = '".$pre."' and gru_clave_int = '".$gru."' and cap_clave_int = '".$cap."' and act_clave_int = '".$act."' and ins_clave_int = '".$insu."'");
                $numpa = mysql_num_rows($conpa);
                if($numpa>0)
                {
                    for($np=0;$np<$numpa;$np++)
                    {
                        $datp = mysql_fetch_array($conpa);
                        $partida = $datp['par_clave_int'];
                        $pai = $datp['pai_clave_int'];
                        $cantpartida = $datp['pai_cant_comprometida'];
                        $valpartida = $datp['pai_val_comprometido'];
                        if($resultado=="A1" and $valortotal<=$valortotalact and $valpartida<=0)
                        {
                            $cantcomprometer = $cantia;
                            $valcomprometer = $valortotal;
                            $valincremento = $valortotal;
                            $updp = mysql_query("UPDATE partida_item set pai_cant_comprometida = '".$cantcomprometer."',pai_val_comprometido = '".$valcomprometer."' where pai_clave_int= '".$pai."'");
                            if($updp>0)
                            {
                                $cana++;
                                $updpo = mysql_query("UPDATE partida_observacion  SET pao_valor = pao_valor + (".$valincremento.") WHERE par_clave_int =  '".$partida."' and ins_clave_int = '".$insu."'");
                                $datos[] = array("cana"=>$cana,"valortotal"=>$valortotal,"valortotalact"=>$valortotalact,"por"=>$por,"resultado"=>$resultado,"totalpa"=>$totalpa);

                            }
                        }
                        else
                        if($resultado=="A1" and $valortotalact<$valortotal and $valpartida<=0)
                        {
                            $cantcomprometer = $cantiact;
                            $valcomprometer = $valortotalact;
                            $valincremento = $valortotalact;
                            $updp = mysql_query("UPDATE partida_item set pai_cant_comprometida = '".$cantcomprometer."',pai_val_comprometido = '".$valcomprometer."' where pai_clave_int= '".$pai."'");
                            if($updp>0)
                            {
                                $cana++;
                                $updpo = mysql_query("UPDATE partida_observacion  SET pao_valor = pao_valor + (".$valincremento.") WHERE par_clave_int =  '".$partida."' and ins_clave_int = '".$insu."'");
                                $datos[] = array("cana"=>$cana,"valortotal"=>$valortotal,"valortotalact"=>$valortotalact,"por"=>$por,"resultado"=>$resultado,"totalpa"=>$totalpa);

                            }
                        }
                        else
                        if($resultado=="D1" || $resultado=="D2"){
                            $cantcomprometer = $cantpartida - (($por/100)*$cantpartida);
                            $valcomprometer = $valpartida - (($por/100)*$valpartida);
                            $valdisminuir = (($por/100) * $valpartida);
                            $updp = mysql_query("UPDATE partida_item set pai_cant_comprometida = '".$cantcomprometer."',pai_val_comprometido = '".$valcomprometer."' where pai_clave_int= '".$pai."'");
                            if($updp>0)
                            {
                                 $cana++;
                                 $updpo = mysql_query("UPDATE partida_observacion  SET pao_valor = pao_valor - (".$valdisminuir.") WHERE par_clave_int =  '".$partida."' and ins_clave_int = '".$insu."'");
                                 $datos[] = array("cana"=>$cana,"valortotal"=>$valortotal,"valortotalact"=>$valortotalact,"por"=>$por,"resultado"=>$resultado,"totalpa"=>$totalpa);

                            }
                        }
                        else if($resultado=="I1" || $resultado=="I2")
                        {
                            $cantcomprometer = $cantpartida + (($por/100)*$cantpartida);
                            $valcomprometer = $valpartida + (($por/100)*$valpartida);
                            $valincremento = (($por/100) * $valpartida);
                            $updp = mysql_query("UPDATE partida_item set pai_cant_comprometida = '".$cantcomprometer."',pai_val_comprometido = '".$valcomprometer."' where pai_clave_int= '".$pai."'");
                            if($updp>0)
                            {
                                $cana++;
                                $updpo = mysql_query("UPDATE partida_observacion  SET pao_valor = pao_valor + (".$valincremento.") WHERE par_clave_int =  '".$partida."' and ins_clave_int = '".$insu."'");
                                $datos[] = array("cana"=>$cana,"valortotal"=>$valortotal,"valortotalact"=>$valortotalact,"por"=>$por,"resultado"=>$resultado,"totalpa"=>$totalpa);

                            }
                        }
                        else
                        {
                            $datos[] = array("cana"=>$cana,"valortotal"=>$valortotal,"valortotalact"=>$valortotalact,"por"=>$por,"resultado"=>$resultado,"totalpa"=>$totalpa);

                        }
                    }

                }
                else
                {
                    //no hay partidas pero se debe comprometer
                    $veripar = mysql_query("select * from partidas where pre_clave_int = '".$pre."' and par_tipo = 1");
                    $numpar = mysql_num_rows($veripar);
                    if($numpar>0)
                    {
                        $datpar = mysql_fetch_array($veripar);
                        $idpar = $datpar['par_clave_int'];
                    }
                    else
                    {
                        $inspar = mysql_query("INSERT INTO partidas(par_nombre,pre_clave_int,par_usu_actualiz,par_fec_actualiz,par_observacion,par_tipo) VALUES('OTROS','".$pre."','".$usuario."','".$fecha."','','1')");
                        $idpar = mysql_insert_id();
                    }
                    if($idpar>0) {
                        if ($valortotal <= $valortotalact) {
                            $valorcomp = $valortotal;
                            $cantdis = $cantia;
                        } else {
                            $valorcomp = $valortotalact;
                            $cantdis = $cantiact;
                        }
                        if ($valortotal > 0 and $idpar>0)
                        {
                            $sql = mysql_query("insert into partida_item(par_clave_int,pre_clave_int,gru_clave_int,cap_clave_int,act_clave_int,ins_clave_int,pai_usu_actualiz,pai_fec_actualiz,pai_val_comprometido,pai_cant_comprometida) values('" . $idpar . "','" . $pre . "','" . $gru . "','" . $cap . "','" . $act . "','" . $insu . "','" . $usuario . "','" . $fecha . "','" . $valorcomp . "','" . $cantdis . "')");
                            if ($sql > 0) {
                                $verio = mysql_query("select * from partida_observacion where par_clave_int = '" . $idpar . "' and ins_clave_int ='" . $insu . "'");
                                $numvo = mysql_num_rows($verio);
                                if ($numvo > 0) {
                                    $datoa = mysql_fetch_array($verio);
                                    $disponible3 = $datoa['pao_valor'] + $valorcomp;
                                    $sql2 = mysql_query("update partida_observacion set pao_valor='" . $disponible3 . "',pao_usu_actualiz ='" . $usuario . "',pao_fec_actualiz = '" . $fecha . "' where par_clave_int = '" . $idpar . "' and ins_clave_int ='" . $insu . "'");
                                    if ($sql2 > 0) {
                                        $cana++;
                                        $datos[] = array("cana" => $cana, "valortotal" => $valortotal, "valortotalact" => $valortotalact, "por" => $por, "opcion" => 2);
                                    } else {
                                        //$res = "error2";// ERROR AL ACTUALIZAR VALOR COMPROMETIDO
                                    }

                                } else {
                                    $sql2 = mysql_query("insert into partida_observacion(par_clave_int,ins_clave_int,pao_valor,pao_usu_actualiz,pao_fec_actualiz) values ('" . $idpar . "','" . $insu . "','" . $valorcomp . "','" . $usuario . "','" . $fecha . "')");
                                    if ($sql2 > 0) {
                                        $cana++;
                                        $datos[] = array("cana" => $cana, "valortotal" => $valortotal, "valortotalact" => $valortotalact, "por" => $por, "opcion" => 2);
                                    } else {
                                        //$res = "error3";// ERROR AL INSERTAR VALOR COMPROMETIDO
                                    }
                                }
                            } else {
                                // $res = "error4";//ERROR AL ASIGNAR ITEM SELECCIONADO
                            }
                        }
                        else
                        {// no inserta valores debido a que el inicial es igual a cero o se agregado desde control
                            $cana++;
                            $cana++;
                            $datos[] = array("cana" => $cana, "valortotal" => $valortotal, "valortotalact" => $valortotalact, "por" => $por, "opcion" => 2);

                        }
                    }
                }
            }
        }
        else
        {
            $cana = 0;
            $datos[] = array("cana"=>$cana);
        }

        echo json_encode($datos);
    }
?>
